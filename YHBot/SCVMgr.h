#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <queue>

#include "MapInfo.h"
#include "me.h"
#include "MyUnit.h"

using namespace BWAPI;
using namespace std;

class spyRoute{
	queue<BWTA::BaseLocation*> route;
	BWTA::BaseLocation* oppStartBase;

public:
	spyRoute() { oppStartBase = nullptr; };
	~spyRoute() {};

	void addRoute(BWTA::BaseLocation* base){
		route.push(base);
	}

	queue<BWTA::BaseLocation*> getRoute(){
		return route;
	}

	BWTA::BaseLocation* getNextRoute(){
		return route.front();
	}

	void removeRoute(){
		route.pop();
	}

	void setOppStartBase(BWTA::BaseLocation* base){
		oppStartBase = base;
	}

	BWTA::BaseLocation* getOppStartBase(){
		return oppStartBase;
	}
};

class SCVMgr
{
	map<Unit, int> resourceCount;

	vector<SCV*> mineralSCV;
	vector<SCV*> gasSCV;
	
	SCV* spySCV;
	spyRoute spyPlan;

	MapInfo* mapInfo;
	me* me;

	SCVMgr();
	~SCVMgr();
public:
	static SCVMgr* getInstance();

	void run();
	void trainSpy();
	void infiltration();
	int plan();	

	Unit getIdleMineral();
	Unit getIdleGas();
	SCV* getWorker(bool except = false);
	bool getProperBasicWork(SCV* scv);

	vector<SCV*>& getMineralScv(){ return mineralSCV; }
};

