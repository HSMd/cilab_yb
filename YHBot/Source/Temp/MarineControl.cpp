#include <list>
#include <vector>
#include <map>
#include <math.h>
#include "MarineControl.h"
#include "CombatInfo.h"
#include "UtilMgr.h"
#include "../Single.h"

#define MIM 256
#define MT 16

HMarine::HMarine(Unit _self) : MyUnit(_self) {
	state = STATE::IDLE;
	attackTarget = NULL;
	kPos = Position(0, 0);
	rPos = Position(0, 0);
}

HMarine::~HMarine() {}

void HMarineControl::run(HMarine* unit) {
	if (unit->getState() == HMarine::STATE::IDLE) {
		HMarineControl::getInstance()->goodLuck(unit);
		//printf("탱크 할일 지정\n");
	}
	else if (unit->getState() == HMarine::STATE::WAIT) {
		HMarineControl::getInstance()->WAIT_Control(unit);
	}
	else if (unit->getState() == HMarine::STATE::DEFENCE) {
		HMarineControl::getInstance()->DEFENCE_Control(unit);
	}
	else if (unit->getState() == HMarine::STATE::RETURN) {
		HMarineControl::getInstance()->RETURN_Control(unit);
	}
	else if (unit->getState() == HMarine::STATE::ATTACK) {
		HMarineControl::getInstance()->ATTACK_Control(unit);
	}
	else if (unit->getState() == HMarine::STATE::CRAZY) {
		HMarineControl::getInstance()->CRAZY_Control(unit);
	}
	else if (unit->getState() == HMarine::STATE::GAMIKAZE) {
		HMarineControl::getInstance()->BC_ATTACK_Control(unit);
	}
	else {
		HMarineControl::getInstance()->goodLuck(unit);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Tank Control

HMarineControl* HMarineControl::s_HMarineControl;
HMarineControl* HMarineControl::create() {
	if (s_HMarineControl)
	{
		return s_HMarineControl;
	}
	s_HMarineControl = new HMarineControl();
	return s_HMarineControl;
}
HMarineControl* HMarineControl::getInstance() {
	return s_HMarineControl;
}
HMarineControl::HMarineControl() {

	for (int x = 0; x < MIM; x++) {
		for (int y = 0; y < MIM; y++) {
			bionicIM[y][x] = Position(x * MT, y * MT);
			bionicIMvalue[y][x] = 0;
			bionicIMvalueBackup[y][x] = 0;
			if (BWAPI::Broodwar->isWalkable((WalkPosition)bionicIM[y][x])) {
				bionicIMvalue[y][x] = 0;
				bionicIMvalueBackup[y][x] = 0;
			}
			else {
				bionicIMvalue[y][x] = -1;
				bionicIMvalueBackup[y][x] = -1;
			}
		}
	}
	int range = 1;
	for (int x = 0; x < MIM; x++) {
		for (int y = 0; y < MIM; y++) {
			bool check = false;
			bool check2 = false;
			for (int i = -range; i <= range; i++) {
				for (int j = -range; j <= range; j++) {
					int _x = x + i;
					int _y = y + j;
					if (_x < 0 || _y < 0 || _x > MIM - 1 || _y > MIM - 1) continue;
					if (bionicIMvalue[_y][_x] == -1) {
						check = true;
					}
				}
			}
			if (check && bionicIMvalue[y][x] >= 0) {
				bionicIMvalue[y][x] = bionicIMvalue[y][x] + 20;
				bionicIMvalueBackup[y][x] = 20;
			}
		}
	}
}

HMarineControl::~HMarineControl() {}

void HMarineControl::onFrame() {
	updateBIM();
}

void HMarineControl::goodLuck(HMarine* unit) {
	if (!unit->getSelf()->isCompleted())
		return;

	if (twoFactoryScene) {
		unit->setState(HMarine::STATE::CRAZY);
	}
}

void HMarineControl::WAIT_Control(HMarine* unit) {
	if (BWAPI::Broodwar->getFrameCount() % 72 == 0) {
		unit->getSelf()->attack(attackPosition);
	}
}
void HMarineControl::DEFENCE_Control(HMarine* unit) {

}
void HMarineControl::RETURN_Control(HMarine* unit) {

}
void HMarineControl::MOVE_Control(HMarine* unit) {

}
void HMarineControl::CRAZY_Control(HMarine* unit) {

	if (VultureControl::getInstance()->getNearUnitDistance(unit->getSelf()->getPosition()) < 64) {
		kitingMove(unit);
	}
	else {
		if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
			if (unit->attackTarget == NULL) {
				if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
					float minDist = 99999;
					int minID = 0;
					for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
						float dist = getDistance((*q).second.second, unit->getSelf()->getPosition());
						if (minDist > dist) {
							minDist = dist;
							minID = (*q).first;
						}
					}
					Unit iu = getUnitforID(minID);
					if (iu == NULL) {
						if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
							unit->getSelf()->attack(unit->rPos, false);
					}
					else {
						if (BWAPI::Broodwar->getFrameCount() % 40 == 0) {
							unit->getSelf()->attack(iu, false);
						}
					}
				}
			}
			else {
				if (!unit->getSelf()->isAttacking())
				{
					if (unit->getSelf()->getHitPoints() >= unit->getSelf()->getType().maxHitPoints() * 0.7)
					{
						if (unit->getSelf()->getStimTimer() == 0)
						{
							unit->getSelf()->useTech(TechTypes::Stim_Packs);
						}
					}
					unit->getSelf()->attack(unit->attackTarget, false);
				}
			}
		}
	}
}
void HMarineControl::ATTACK_Control(HMarine* unit) {

}

void HMarineControl::BC_ATTACK_Control(HMarine* unit) {

}

float HMarineControl::getTargetUnitValue(UnitType ut) {
	if (ut.isFlyer())
		return -1;
	if (ut == UnitTypes::Protoss_Zealot)
		return 1;
	else if (ut == UnitTypes::Protoss_Dragoon)
		return 1;
	else if (ut == UnitTypes::Protoss_High_Templar)
		return 0.3;
	else if (ut == UnitTypes::Protoss_Dark_Templar)
		return 0.4;
	else if (ut == UnitTypes::Protoss_Probe)
		return 0.4;
	else if (ut == UnitTypes::Protoss_Reaver)
		return 0.5;
	else if (ut == UnitTypes::Protoss_Archon)
		return 0.1;
	else if (ut == UnitTypes::Protoss_Photon_Cannon)
		return 0.4;
	else if (ut == UnitTypes::Protoss_Shuttle)
		return 1;
	else if (ut == UnitTypes::Protoss_Observer)
		return 1;
	else {
		return 0;
	}
}

Unit HMarineControl::getUnitforID(int id) {
	for (auto q = BWAPI::Broodwar->enemy()->getUnits().begin(); q != BWAPI::Broodwar->enemy()->getUnits().end(); q++) {
		if ((*q)->exists() && (*q)->getID() == id) {
			return (*q);
		}
	}
	return NULL;
}

void HMarineControl::drawHMarineControlUnit(HMarine* unit) {
	if (unit->attackTarget != NULL) {
		BWAPI::Broodwar->drawLineMap(unit->getSelf()->getPosition(), unit->attackTarget->getPosition(), Colors::Blue);
	}
	BWAPI::Broodwar->drawCircleMap(unit->getSelf()->getPosition(), 5, Colors::Blue, true);
}

void HMarineControl::drawHMarineControl() {
	/*if (BWAPI::Broodwar->getFrameCount() > 24) {
	for (int x = 0; x < MIM - 2; x += 2) {
	for (int yy = 0; yy < MIM - 2; yy += 2) {
	if (bionicIMvalue[yy][x] == 0) {
	BWAPI::Broodwar->drawCircleMap(bionicIM[yy][x], 2, Colors::Green, false);
	}
	else if (bionicIMvalue[yy][x] == -1) {
	BWAPI::Broodwar->drawCircleMap(bionicIM[yy][x], 2, Colors::Red, false);
	}
	else if (bionicIMvalue[yy][x] == -2) {
	BWAPI::Broodwar->drawCircleMap(bionicIM[yy][x], 2, Colors::Blue, false);
	}
	else if (bionicIMvalue[yy][x] >= 30) {
	BWAPI::Broodwar->drawCircleMap(bionicIM[yy][x], 2, Colors::Red, false);
	}
	else if (bionicIMvalue[yy][x] >= 20) {
	BWAPI::Broodwar->drawCircleMap(bionicIM[yy][x], 2, Colors::Orange, false);
	}
	else if (bionicIMvalue[yy][x] >= 10) {
	BWAPI::Broodwar->drawCircleMap(bionicIM[yy][x], 2, Colors::Yellow, false);
	}
	else if (bionicIMvalue[yy][x] >= 5) {
	BWAPI::Broodwar->drawCircleMap(bionicIM[yy][x], 2, Colors::Green, false);
	}
	else if (bionicIMvalue[yy][x] >= 1) {
	BWAPI::Broodwar->drawCircleMap(bionicIM[yy][x], 2, Colors::Cyan, false);
	}
	else {
	BWAPI::Broodwar->drawCircleMap(bionicIM[yy][x], 2, Colors::Blue, false);
	}
	}
	}
	}*/
}

Unit HMarineControl::getTargetUnit(HMarine* unit) {
	std::list< pair<int, pair<UnitType, Position>>> nearEnemies;
	nearEnemies.clear();

	for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
		int range = (int)(((*q).second.first.groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange()) * 1.5);
		float dist = getDistance(unit->getSelf()->getPosition(), (*q).second.second);
		if (dist < range) {
			nearEnemies.push_back(*q);
		}
	}

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		int range = (int)(((*q).second.first.groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange()) * 1.5);
		float dist = getDistance(unit->getSelf()->getPosition(), (*q).second.second);
		if (dist < range) {
			nearEnemies.push_back(*q);
		}
	}

	int retInt = -1;
	float maxValue = 0;
	float utC = 7.0, bloodC = 5.0, distC = 3.0, damageC = 1.0, cannonC = 10.0;
	for (auto q = nearEnemies.begin(); q != nearEnemies.end(); q++) {
		float value = getTargetUnitValue((*q).second.first)*utC;
		if (getUnitforID((*q).first) != NULL) {
			Unit target = getUnitforID((*q).first);
			if (target->isVisible() == false) continue;
			if (target == NULL || target->exists() == false) continue;
			value += (((float)(*q).second.first.maxShields() + (*q).second.first.maxHitPoints()) - (target->getHitPoints() + target->getShields())) / (float)((*q).second.first.maxShields() + (*q).second.first.maxHitPoints()) *bloodC;
			int range = (*q).second.first.groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange();
			value += (range - getDistance(unit->getSelf()->getPosition(), (*q).second.second)) / range * distC;

			float cu = getDistance(VultureControl::getInstance()->getNearestCannonPos((*q).second.second), (*q).second.second);
			if (cu < UnitTypes::Protoss_Photon_Cannon.groundWeapon().maxRange() * 1.5f) {
				value = -100.0f;
			}

			// 예상 피해 value +=
			if (value > maxValue) {
				maxValue = value;
				retInt = (*q).first;
			}
		}
	}

	if (retInt == -1) return NULL;
	else {
		return getUnitforID(retInt);
	}

	return NULL;
}

void HMarineControl::updateBIM() {
	for (int x = 0; x < MIM; x++) {
		for (int y = 0; y < MIM; y++) {
			bionicIMvalue[y][x] = bionicIMvalueBackup[y][x];
		}
	}

	for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
		Position p = (*q).second.second;
		int _x = p.x / MT;
		int _y = p.y / MT;
		int eRange = (*q).second.first.groundWeapon().maxRange() + MT * 2;
		if ((*q).second.first == UnitTypes::Protoss_Probe)
			eRange = (*q).second.first.groundWeapon().maxRange() * 2;
		if ((*q).second.first == UnitTypes::Protoss_Zealot)
			eRange = (*q).second.first.groundWeapon().maxRange() * 8;
		for (int i = -eRange / MT; i <= eRange / MT; i++) {
			for (int j = -eRange / MT; j <= eRange / MT; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx > MIM - 1 || _yy > MIM - 1) {
					continue;
				}
				int dist = (int)getDistance(Position(_xx * MT, _yy * MT), p);
				if (dist < eRange) {
					bionicIMvalue[_yy][_xx] += (int)(((float)eRange - dist) / (float)eRange * 100);
				}
			}
		}
	}

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		if ((*q).second.first == UnitTypes::Protoss_Photon_Cannon) {
			Position p = (*q).second.second;
			int _x = p.x / MT;
			int _y = p.y / MT;
			int eRange = (*q).second.first.groundWeapon().maxRange() + MT * 3;
			for (int i = -eRange / MT; i <= eRange / MT; i++) {
				for (int j = -eRange / MT; j <= eRange / MT; j++) {
					int _xx = _x + i;
					int _yy = _y + j;
					if (_xx < 0 || _yy < 0 || _xx > MIM || _yy > MIM) {
						continue;
					}
					int dist = (int)getDistance(Position(_xx * MT, _yy * MT), p);
					if (dist < eRange) {
						bionicIMvalue[_yy][_xx] += (eRange - dist) / eRange * 100;
					}
				}
			}
		}
	}

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		Position p = (*q).second.second;
		int _x = p.x / MT;
		int _y = p.y / MT;
		if (_x < 0 || _y < 0 || _x > MIM - 1 || _y > MIM - 1) continue;
		bionicIMvalue[_y][_x] = -2;
		for (int i = -((*q).second.first.width() / 2) / MT - 1; i <= ((*q).second.first.width() / 2) / MT + 1; i++) {
			for (int j = -((*q).second.first.height() / 2) / MT; j <= ((*q).second.first.height() / 2) / MT + 1; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx >MIM || _yy > MIM) {
					continue;
				}
				bionicIMvalue[_yy][_xx] = -2;
			}
		}
	}

	for (auto q = CombatInfo::getInstance()->myBuildingsInfo.begin(); q != CombatInfo::getInstance()->myBuildingsInfo.end(); q++) {
		Position p = (*q).second.second;
		int _x = p.x / MT;
		int _y = p.y / MT;
		if (_x < 0 || _y < 0 || _x > MIM - 1 || _y > MIM - 1) continue;
		bionicIMvalue[_y][_x] = -2;
		for (int i = -((*q).second.first.width() / 2) / MT - 1; i <= ((*q).second.first.width() / 2) / MT + 1; i++) {
			for (int j = -((*q).second.first.height() / 2) / MT; j <= ((*q).second.first.height() / 2) / MT + 1; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx >MIM || _yy > MIM) {
					continue;
				}
				bionicIMvalue[_yy][_xx] = -2;
			}
		}
	}

	for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
		int _x = (*q)->getSelf()->getPosition().x / MT;
		int _y = (*q)->getSelf()->getPosition().y / MT;
		// 384
		for (int i = -12; i <= 12; i++) {
			for (int j = -12; j <= 12; j++) {
				int x = _x + i;
				int y = _y + j;
				if (x < 0 || y < 0 || x > MIM || y > MIM) continue;
				float dist = getDistance(Position(_x, _y), Position(x, y));
				if (dist < 384) {
					if (bionicIMvalue[y][x] >= 0) {
						bionicIMvalue[y][x] -= dist / MT;
						if (bionicIMvalue[y][x] < 0)
						{
							bionicIMvalue[y][x] = 0;
						}
					}
				}
			}
		}
	}
}

void HMarineControl::kitingMove(HMarine* unit) {
	if (unit->attackTarget == NULL) {
		if (CombatInfo::getInstance()->enemyStartingBase != NULL)
		{
			if(BWAPI::Broodwar->getFrameCount()%36==0)
				unit->getSelf()->attack(unit->rPos, false);
		}
	}
	else {
		Position p = unit->getSelf()->getPosition();
		Position targetPos;
		int minValue = 9999;
		int range = 3;
		for (int x = -range; x <= range; x++) {
			for (int y = -range; y <= range; y++) {
				int _x = x + p.x / MT;
				int _y = y + p.y / MT;
				if (_y < 0 || _x < 0 || _x > MIM - 1 || _y > MIM - 1) continue;
				if (BWAPI::Broodwar->getGroundHeight((TilePosition)Position(_x * MT, _y * MT)) != BWAPI::Broodwar->getGroundHeight((TilePosition)unit->getSelf()->getPosition())) {
					if (bionicIMvalue[_y][_x] < minValue && bionicIMvalue[_y][_x] >= 0) {
						minValue = bionicIMvalue[_y][_x];
					}
				}
				else if (bionicIMvalue[_y][_x] < minValue && bionicIMvalue[_y][_x] >= 0) {
					minValue = bionicIMvalue[_y][_x];
				}
			}
		}
		float minDisttoTarget = 99999;
		int fx = 0;
		int fy = 0;
		for (int x = -range; x <= range; x++) {
			for (int y = -range; y <= range; y++) {
				int _x = x + p.x / MT;
				int _y = y + p.y / MT;
				if (_y < 0 || _x < 0 || _x > MIM - 1 || _y > MIM - 1) continue;
				if (BWAPI::Broodwar->getGroundHeight((TilePosition)Position(_x * MT, _y * MT)) != BWAPI::Broodwar->getGroundHeight((TilePosition)unit->getSelf()->getPosition())) {
					if (bionicIMvalue[_y][_x] == minValue) {
						float dist = getDistance(Position(_x * MT, _y * MT), unit->attackTarget->getPosition());
						if (dist < minDisttoTarget) {
							minDisttoTarget = dist;
							targetPos = Position(_x * MT, _y * MT);
							fx = _x;
							fy = _y;
						}
					}
				}
				else if (bionicIMvalue[_y][_x] == minValue) {
					float dist = getDistance(Position(_x * MT, _y * MT), unit->attackTarget->getPosition());
					if (dist < minDisttoTarget) {
						minDisttoTarget = dist;
						targetPos = Position(_x * MT, _y * MT);
						fx = _x;
						fy = _y;
					}
				}
			}
		}
		unit->getSelf()->move(targetPos, false);
		unit->kPos = targetPos;
		bionicIMvalue[fy][fx] = -3;
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				int xxxx = fx + i;
				int yyyy = fy + j;
				if (xxxx < 0 || yyyy < 0 || xxxx> MIM - 1 || yyyy > MIM - 1) continue;
				bionicIMvalue[yyyy][xxxx] += 20;
			}
		}
	}
}