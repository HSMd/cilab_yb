#pragma once
#include "MyUnit.h"
#include "TerritoryMgr.h"
#include <map>

using namespace std;

class Invasion_pylon{

private:	
	//pylon ��ġ
	Unit pylon;
	
	vector<Position> pylonPosition;
	vector<Position> marinePosition;
	vector<SCV*> guardSCV;
	vector<Marine*> guardmarine;
	map<SCV*, Position> holdSCV;
	map<Marine*, Position> holdMarine;

public:
	Invasion_pylon(TerritoryMgr* tMgr);
	~Invasion_pylon();

	TerritoryMgr* tMgr;
	TerritoryMgr* territory;	

	void init();
	Marine* getIdleMarine();

	void scvControl(SCV* scv);
	bool ZealotDetect();

	//bool getPylonFlag(){ return PylonFlag; }
	//bool getZealotFlag(){ return ZealotFlag; }
	void getMarine();

	void SCVControl(SCV* scv, Unit unit);
	void MarineControl(Marine* marine);

	bool PylonDetect();
	
	bool DragoonDetect();

//	void run(TerritoryMgr *territory);
	void run2();
};