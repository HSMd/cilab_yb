#include "Work.h"
#include "../Single.h"
#include "UtilMgr.h"

bool scheduleWork::canFire(){
	
	if (immediatly){
		if (interrupt == nullptr){
			if (cmpType.isBuilding()){
				return cmpCount <= me->getImmediatelyBuilding(cmpType).size();
			}

			if (cmpType == UnitTypes::Terran_SCV){
				return cmpCount <= cmd->getScvCount();
			}

			return cmpCount <= me->getImmediatelyUnit(cmpType).size();
		}

		if (!interrupt->exists()){
			bw << "exist" << endl;
			return true;
		}

		if (!isContainTwoTerm(targetPos, interrupt->getTilePosition(), targetType.tileWidth(), targetType.tileHeight())){
			bw << "in" << endl;
			return true;
		}
	}
	else{
		if (interrupt == nullptr){
			if (cmpType.isBuilding()){
				return cmpCount <= me->getBuildings(cmpType).size();
			}

			if (cmpType == UnitTypes::Terran_SCV){
				return cmpCount <= cmd->getScvCount();
			}

			return cmpCount <= me->getUnits(cmpType).size();
		}

		if (!interrupt->exists()){
			bw << "exist" << endl;
			return true;
		}

		if (!isContainTwoTerm(targetPos, interrupt->getTilePosition(), targetType.tileWidth(), targetType.tileHeight())){
			bw << "in" << endl;
			return true;
		}
	}

	return false;
}