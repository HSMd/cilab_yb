#pragma once
#include "StateMgr.h"
#include "TerritoryMgr.h"
#include "Work.h"

#include "SimCityBuild.h"
#include "SimCityBuild2.h"
#include "TwoFactory.h"
#include "Bacanic.h"
#include "UpSimCityBuild2.h"
#include "FastTwoFactory.h"

class BuildMgr
{
	// Build
	SimCity_1 sim_1;
	SimCity_2 sim_2;

	TwoFactory tFac;
	Bacanic bacanic;
	UpSimCity_2 up;

	FastTwoFactory fast;

public:
	BuildMgr();
	~BuildMgr();

	void run(BUILD_FLAG build, COMBAT_FLAG combat);

	void onUnitCrate(Unit unit, BUILD_FLAG build);
	void onStructureCrate(Unit unit, BUILD_FLAG build);
	void onUnitComplete(Unit unit, BUILD_FLAG build);
	void onStructureComplete(Unit unit, BUILD_FLAG build);
};

