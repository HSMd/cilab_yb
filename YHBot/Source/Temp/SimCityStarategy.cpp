#include "SimCityStarategy.h"
#include "CombatInfo.h"
#include "../single.h"
#include "UtilMgr.h"


SimCityStrategy::SimCityStrategy(){

	this->tMgr = &cmd->getTerritoryMgr();
	safeFrame = 0;
	pylon = nullptr;
	Probe = nullptr;
	Cannon = nullptr;

	//flag false 추가
	setDefenseFlag(DEFENSE_Flag::IDLE, true);
	setDefenseFlag(DEFENSE_Flag::PROBE, false);
	setDefenseFlag(DEFENSE_Flag::PYLON, false);
	setDefenseFlag(DEFENSE_Flag::CANNON, false);

	init();
	VCannon.clear();
	VProbe.clear();
	VPylon.clear();
}

SimCityStrategy::~SimCityStrategy(){
}

Marine* SimCityStrategy::getIdleMarine(){

	for (auto m : me->getUnits(UnitTypes::Terran_Marine)){
		Marine* marine = dynamic_cast<Marine*>(m);
		if (marine->getState() == Marine::STATE::IDLE){
			marine->setState(Marine::STATE::WAIT);
			return marine;
		}
	}

	return nullptr;
}

void SimCityStrategy::init(){
	// 
	switch (mapInfo->getDir()){
		case 0:
			// pylonRush
			pylonPosition.push_back(tile2pos(TilePosition(93, 25)));
			pylonPosition.push_back(tile2pos(TilePosition(99, 10)));
			pylonPosition.push_back(tile2pos(TilePosition(100, 11)));
			pylonPosition.push_back(tile2pos(TilePosition(98, 12)) + Position(16, 0));

			marinePosition.push_back(tile2pos(TilePosition(100, 6)));
			marinePosition.push_back(tile2pos(TilePosition(101, 6)));
			marinePosition.push_back(tile2pos(TilePosition(103, 8)));
			marinePosition.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
			marinePosition.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));
			break;

		case 1:
			// pylonRush
			pylonPosition.push_back(tile2pos(TilePosition(100, 90)));
			pylonPosition.push_back(tile2pos(TilePosition(115, 98)) + Position(16, 16));
			pylonPosition.push_back(tile2pos(TilePosition(116, 98)) + Position(16, 0));
			pylonPosition.push_back(tile2pos(TilePosition(115, 97)) + Position(16, 16));

			marinePosition.push_back(tile2pos(TilePosition(121, 99)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(121, 100)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(119, 102)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(118, 102)) + Position(16, 16));
			break;

		case 2:
			// pylonRush
			pylonPosition.push_back(tile2pos(TilePosition(27, 37)));
			pylonPosition.push_back(tile2pos(TilePosition(11, 30)));
			pylonPosition.push_back(tile2pos(TilePosition(10, 30)) + Position(16, 16));
			pylonPosition.push_back(tile2pos(TilePosition(11, 31)));

			marinePosition.push_back(tile2pos(TilePosition(7, 26)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(11, 26)));
			marinePosition.push_back(tile2pos(TilePosition(14, 27)) + Position(0, 16));
			marinePosition.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 0));
			break;

		case 3:
			// pylonRush
			pylonPosition.push_back(tile2pos(TilePosition(36, 102)));
			pylonPosition.push_back(tile2pos(TilePosition(27, 118)));
			pylonPosition.push_back(tile2pos(TilePosition(28, 119)));
			pylonPosition.push_back(tile2pos(TilePosition(28, 118)) + Position(16, 0));

			marinePosition.push_back(tile2pos(TilePosition(23, 117)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(24, 121)));
			marinePosition.push_back(tile2pos(TilePosition(27, 123)));
			marinePosition.push_back(tile2pos(TilePosition(28, 123)));
			marinePosition.push_back(tile2pos(TilePosition(31, 121)) + Position(16, 16));
			break;
	}
}

void SimCityStrategy::run()
{

	UpdateUnit();

	Marine *marine = getIdleMarine();
	if (marine != nullptr)
	{
		guardmarine.push_back(marine);
	}

	blockFlag = isBlock();

	//입구막기가 안된 경우
	if (!blockFlag)
	{
		//포토캐논 공격
		if (getDefenseFlag(DEFENSE_Flag::CANNON))
		{
			int minCanhp = 9999;
			for (auto iter : VCannon)
			{
				if (minCanhp > iter->getHitPoints())
				{
					minCanhp = iter->getHitPoints();
					Cannon = iter;
				}
			}

			if (!guardmarine.empty())
			{
				for (auto marine : guardmarine)
				{
					if (marine->getSelf()->exists())
					{
						marine->setState(Marine::STATE::ATTACK);
					}
				}
			}

			for (int i = guardSCV.size(); i < VCannon.size() * 3; i++)
			{
				SCV* scv = tMgr->getAllTerritorys()[0]->getWorker(true);
				guardSCV.push_back(scv);
			}

			for (auto scv : guardSCV)
			{
				if (scv->getSelf()->exists())
				{
					scv->setState(SCV::STATE::ATTACK);
				}
			}
		}

		//파일런 공격
		else if (getDefenseFlag(DEFENSE_Flag::PYLON))
		{
			
			if (isInvasion())
			{
				Func = &SimCityStrategy::detectPylon1;
			}
			else
			{
				Func = &SimCityStrategy::detectPylon2;
			}
			((this->*Func))();
		}

		//질럿 공격
		else if (getDefenseFlag(DEFENSE_Flag::ZEALOT))
		{
			for (int i = 0; i < guardmarine.size();){
				if (i < 5){
					if (guardmarine[i]->getSelf()->exists()){
						holdMarine[guardmarine[i]] = marinePosition[i];
						guardmarine[i]->setState(Marine::STATE::ATTACK);
						i++;
					}
				}
				else{
					break;
				}
			}
			for (auto marine : guardmarine)
			{
				if (marine->getSelf()->exists())
				{
					marine->setState(Marine::STATE::ATTACK);
				}
			}
			for (auto scv : guardSCV)
			{
				if (scv->getSelf()->exists())
				{
					scv->setState(SCV::STATE::REPAIR);
				}
			}
		}

		//프로브 공격
		else if (getDefenseFlag(DEFENSE_Flag::PROBE))
		{
			int minhp = 9999;
			for (auto iter : VProbe)
			{
				if (minhp > iter->getHitPoints())
				{
					minhp = iter->getHitPoints();
					Probe = iter;
				}
			}
			if (guardmarine.empty())
			{
				if (guardSCV.size() < VProbe.size())
				{
					for (int i = guardSCV.size(); i < VProbe.size(); i++)
					{
						SCV *tscv = tMgr->getAllTerritorys()[0]->getWorker(true);
						guardSCV.push_back(tscv);
					}
				}

				for (auto scv : guardSCV)
				{
					if (scv->getSelf()->exists())
					{
						scv->setState(SCV::STATE::ATTACK);
					}
				}
			}
			else
			{
				for (auto marine : guardmarine)
				{
					if (marine->getSelf()->exists())
					{
						marine->setState(Marine::STATE::ATTACK);
					}
				}
			}
		}

		//다른 경우 - 평화
		else if (getDefenseFlag(DEFENSE_Flag::IDLE))
		{
			marineHold();
			for (auto scv : guardSCV)
			{
				if (scv->getSelf()->exists())
				{
					scv->setState(SCV::STATE::RETURN);
				}
			}
		}
	}
	//입구가 막힌 경우
	else
	{
		for (int i = 0; i < guardmarine.size();){
			if (i < 5){
				if (guardmarine[i]->getSelf()->exists()){
					holdMarine[guardmarine[i]] = marinePosition[i];
					guardmarine[i]->setState(Marine::STATE::ATTACK);
					i++;
				}
			}
			else{
				break;
			}
		}

		for (auto scv : guardSCV)
		{
			if (scv->getSelf()->exists())
			{
				if (pylon == nullptr && Probe == nullptr && Cannon == nullptr)
				{
					scv->setState(SCV::STATE::REPAIR);
				}
				else{
					scv->setState(SCV::STATE::ATTACK);
				}
				
			}
		}
	}

	UpdateMyUnit();

	if (VCannon.empty())
	{
		setDefenseFlag(DEFENSE_Flag::CANNON, false);
		Cannon = nullptr;
	}
	if (VPylon.empty())
	{
		setDefenseFlag(DEFENSE_Flag::PYLON, false);
		pylon = nullptr;
	}
	if (VZealot.empty())
	{
		setDefenseFlag(DEFENSE_Flag::ZEALOT, false);
	}
	if (VProbe.empty())
	{
		setDefenseFlag(DEFENSE_Flag::PROBE, false);
		Probe = nullptr;
	}

	
	for (auto scv : guardSCV)
	{
		if (scv->getSelf()->exists())
		{
			scvControl(scv);
		}
	}
	for (auto marine : guardmarine)
	{
		if (marine->getSelf()->exists())
		{
			MarineControl(marine);
		}
	}
	
}

void SimCityStrategy::scvControl(SCV* scv){
	if (scv == nullptr)
	{
		return;
	}

	Unit target = nullptr;
	
	switch (scv->getState())
	{
	case SCV::STATE::MOVE:
		if (isAlmostReachedDest(holdSCV[scv], scv->getSelf()->getPosition(), 10, 10)){
			scv->orderWait();
		}
		else{
			scv->getSelf()->move(holdSCV[scv]);
		}
		break;

	case SCV::STATE::ATTACK:
				
		if (Cannon != nullptr)
		{
			target = Cannon;
		}
		else if (pylon != nullptr)
		{
			target = pylon;
		}
		else if (Probe != nullptr)
		{
			if (getDistance(mapInfo->getMyStartBase()->getTilePosition(), Probe->getTilePosition()) <= 20)
			{
				target = Probe;
			}
			else
			{
				target == nullptr;
			}
		}

		for (auto eunit : scv->getSelf()->getUnitsInRadius(scv->getSelf()->getType().seekRange()+1, Filter::IsEnemy))
		{
			if (scv->getSelf()->isAttacking()){
				if (eunit->getTarget() == scv->getSelf())
				{
					if (scv->getSelf()->exists())
					{
						target = eunit;
						break;
					}
				}
			}
		}

		if (target != nullptr)
		{
			scv->getSelf()->attack(target);
		}
		else{
			if (!VPylon.empty())
			{
				for (auto p : VPylon)
				{
					scv->getSelf()->attack(p);
					break;
				}
			}
			else{
				if (scv->getSelf()->isCarryingMinerals()){
					scv->getSelf()->returnCargo(true);
				}
				else if (scv->getSelf()->isIdle() || scv->getSelf()->isGatheringGas()){
					scv->getSelf()->gather(scv->getTarget());
				}
			}
		}
		
		break;

	case SCV::STATE::RETURN:
		if (scv->getSelf()->isAttacking()){
			scv->getSelf()->stop();
		}
		if (scv->getSelf()->isCarryingMinerals()){
			scv->getSelf()->returnCargo(true);
		}
		else if (scv->getSelf()->isIdle() || scv->getSelf()->isGatheringGas()){
			scv->getSelf()->gather(scv->getTarget());
		}
		break;

	case SCV::STATE::REPAIR:
		if (!blockFlag){
			if (guardSCV[0]->getSelf()->exists())
			{
				if (guardSCV[0]->getSelf()->getHitPoints() < 60){
					scv->getSelf()->repair(guardSCV[0]->getSelf());
				}
				else{
					scv->getSelf()->move(holdSCV[scv]);
				}
			}
			else{
				scv->getSelf()->move(holdSCV[scv]);
			}
		}
		else{
			for (auto b : bw->self()->getUnits())
			{
				if (b->getType() == UnitTypes::Buildings)
				{
					if (b->getHitPoints() < b->getType().maxHitPoints())
					{
						if (b->exists())
						{
							scv->getSelf()->repair(b);
						}
					}
				}
			}
			if (scv->getSelf()->isIdle())
			{
				scv->getSelf()->gather(scv->getTarget());
			}
		}
		break;
	}
}

void SimCityStrategy::MarineControl(Marine* marine)
{
	if (marine == nullptr){
		return;
	}
	Unit t_marine = marine->getSelf();
	Unit target = nullptr;
	Unitset eunits = t_marine->getUnitsInRadius(bw->self()->sightRange(UnitTypes::Terran_Barracks), Filter::IsEnemy);
	
	switch (marine->getState()){
	
	case Marine::STATE::MOVE:
		t_marine->move(holdMarine[marine]);
		break;
	
	case Marine::STATE::HOLD:
		if (isAlmostReachedDest(t_marine->getPosition(), holdMarine[marine], 0, 0)){
			if (!t_marine->isHoldingPosition()){
				t_marine->holdPosition();
			}
		}
		else{
			t_marine->move(holdMarine[marine]);
		}
		break;

	case Marine::STATE::ATTACK:
		if (Cannon != nullptr)
		{
			target = Cannon;
		}
		else if (Probe != nullptr)
		{
			if (getDistance(mapInfo->getMyStartBase()->getTilePosition(), Probe->getTilePosition()) <= 20)
			{
				target = Probe;
			}
			else{
				target = nullptr;
			}
		}
		else if (pylon != nullptr)
		{
			target = pylon;
		}
		

		if (target != nullptr){
			if (getDistance(target->getTilePosition(), mapInfo->getMyStartBase()->getTilePosition()) >= 20){
				if (isAlmostReachedDest(t_marine->getPosition(), holdMarine[marine], 0, 0)){
					if (!t_marine->isHoldingPosition()){
						t_marine->holdPosition();
					}
				}
				else{
					t_marine->move(holdMarine[marine]);
				}
			}
			else{
				if (!marine->getSelf()->isAttacking()){
					marine->getSelf()->attack(target);
				}
			}
		}
		else{
			for (Unit u : bw->enemy()->getUnits()){
				if (u->getType() == UnitTypes::Protoss_Zealot){
					if (isAlmostReachedDest(BWTA::getNearestChokepoint(mapInfo->getMyStartBase()->getPosition())->getCenter(), u->getPosition(), 180, 180)){
						//질럿을 공격
						if (u != nullptr){
							if (!t_marine->isAttacking()){
								t_marine->attack(u);
								break;
							}
						}
					}
					else{
						if (isAlmostReachedDest(t_marine->getPosition(), holdMarine[marine], 0, 0)){
							if (!t_marine->isHoldingPosition()){
								t_marine->holdPosition();
								marine->setState(Marine::STATE::HOLD);
							}
						}
						else{
							t_marine->move(holdMarine[marine]);
						}
					}
					break;
				}
			}	
		}
		if (t_marine->getHitPoints() < marine->getSelf()->getType().maxHitPoints())
		{
			if (isAlmostReachedDest(t_marine->getPosition(), holdMarine[marine], 0, 0)){
				if (!t_marine->isHoldingPosition()){
					t_marine->holdPosition();
					marine->setState(Marine::STATE::HOLD);
					break;
				}
			}
			else{
				t_marine->rightClick(holdMarine[marine]);
			}
			
		}
		break;
	
	case Marine::STATE::WAIT:
		t_marine->move(holdMarine[marine]);
		
		break;

	case Marine::STATE::RETURN:
		if (isAlmostReachedDest(t_marine->getPosition(), holdMarine[marine], 0, 0)){
			t_marine->stop();
		}
		else{
			t_marine->move(holdMarine[marine]);
		}
		
		break;
	}
}

//입구막기를 방해한 경우
void SimCityStrategy::detectPylon1()
{
	bool zealot = ZealotDetect();

	// 발생했을 경우 3마리를 작업용으로 빼온다.
	for (int i = guardSCV.size(); i < 3; i++)
	{
		SCV* scv = tMgr->getAllTerritorys()[0]->getWorker(true);
		if (scv != nullptr)
		{
			guardSCV.push_back(scv);
		}
	}

	// 3마리가 위치가 할당이 안되있을 경우 할당해준다.
	for (int i = 0; i < 3; i++){
		SCV* scv = guardSCV[i];

		if (holdSCV[scv] == Position(0, 0)){
			holdSCV[scv] = pylonPosition[i];

			if (i == 0){
				scv->setState(SCV::STATE::MOVE);
			}
			else{
				scv->setState(SCV::STATE::ATTACK);
			}
		}
	}
	for (auto marine : guardmarine)
	{
		if (marine->getSelf()->exists())
		{
			marine->setState(Marine::STATE::ATTACK);
		}
	}

	if (zealot){
		holdSCV[guardSCV[0]] = pylonPosition[3];

		if (isAlmostReachedDest(holdSCV[guardSCV[0]], guardSCV[0]->getSelf()->getPosition(), 64, 64)){
			guardSCV[1]->setState(SCV::STATE::REPAIR);
			guardSCV[2]->setState(SCV::STATE::REPAIR);
		}

		if (!guardmarine.empty()){
			for (int i = 0; i < guardmarine.size();){
				if (i < 5){
					if (guardmarine[i]->getSelf()->exists()){
						holdMarine[guardmarine[i]] = marinePosition[i];
						guardmarine[i]->setState(Marine::STATE::ATTACK);
						i++;
					}
				}
				else{
					break;
				}
			}
		}
	}
	// SCV 의 행동을 수행
	for (auto scv : guardSCV)
	{
		if (scv->getSelf()->exists())
		{ 
			scvControl(scv);
		}
	}
	for (auto marine : guardmarine)
	{
		if (marine->getSelf()->exists())
		{
			
			MarineControl(marine);
		}
	}
}
//막 지었을 때 target이 없음
void SimCityStrategy::detectPylon2(){

	// 대충 지은 파일런
	for (int i = guardSCV.size(); i < 2; i++)
	{
		SCV* scv = tMgr->getAllTerritorys()[0]->getWorker(true);
		if (scv != nullptr)
		{
			guardSCV.push_back(scv);
		}
	}
	for (auto scv : guardSCV)
	{
		if (scv->getSelf()->exists())
		{
			scv->setState(SCV::STATE::ATTACK);
		}
	}
	for (auto marine : guardmarine)
	{
		if (marine->getSelf()->exists())
		{
			marine->setState(Marine::STATE::ATTACK); 
		}
	}
}

//적군 유닛을 저장
void SimCityStrategy::UpdateUnit()
{
	VCannon.clear();
	VPylon.clear();
	VProbe.clear();
	VZealot.clear();

	auto startbase = mapInfo->getMyStartBase()->getTilePosition();
	
	for (auto e : bw->enemy()->getUnits())
	{
		if (getDistance(e->getTilePosition(), startbase) <= 30)	
		{
			if (e->getType() == UnitTypes::Protoss_Probe)
			{
				VProbe.push_back(e);
				setDefenseFlag(DEFENSE_Flag::PROBE, true);
			}
			else if (e->getType() == UnitTypes::Protoss_Pylon)
			{
				VPylon.push_back(e);
				setDefenseFlag(DEFENSE_Flag::PYLON, true);
			}
			else if (e->getType() == UnitTypes::Protoss_Photon_Cannon)
			{
				VCannon.push_back(e);
				setDefenseFlag(DEFENSE_Flag::CANNON, true);
			}
			else if (e->getType() == UnitTypes::Protoss_Zealot)
			{
				VZealot.push_back(e);
				setDefenseFlag(DEFENSE_Flag::ZEALOT, true);
			}
		}
	}
}

//유닛이 죽으면 벡터에서 삭제
void SimCityStrategy::UpdateMyUnit()
{
	if (!guardSCV.empty())
	{
		for (auto iter = guardSCV.begin(); iter != guardSCV.end();)
		{
			SCV *iterscv = dynamic_cast<SCV*>(*iter);
			if (iterscv == nullptr || !iterscv->getSelf()->exists())
			{
				iter = guardSCV.erase(iter);
			}
			else{
				iter++;
			}
		}
	}

	if (!guardmarine.empty())
	{
		for (auto iter2 = guardmarine.begin(); iter2 != guardmarine.end();)
		{
			Marine *itermarine = dynamic_cast<Marine*>(*iter2);
			if (itermarine == nullptr || !itermarine->getSelf()->exists())
			{
				iter2 = guardmarine.erase(iter2);
			}
			else{
				iter2++;
			}
		}
	}
}

void SimCityStrategy::marineHold(){
	
	//마린 위치 잡기
	Marine* t_marine = getIdleMarine();
	if (t_marine != nullptr && guardmarine.size() <= 5){
		guardmarine.push_back(t_marine);
	}

	//마린이 고정할 위치를 지정
	if (!guardmarine.empty()){
		for (int i = 0; i < guardmarine.size(); i++){
			if (i < 5){
				if (guardmarine[i]->getSelf()->exists()){
					holdMarine[guardmarine[i]] = marinePosition[i];
					guardmarine[i]->setState(Marine::STATE::HOLD);
				}
			}
		}
	}
	for (auto m : guardmarine){
		if (m->getSelf()->exists())
		{
			MarineControl(m);
		}
	}
}

// FLAG FUNC
bool SimCityStrategy::isSafe(){

	if (PylonDetect()){
		safeFrame = 0;
		return false;
	}
	
	safeFrame++;
	if (3000 <= safeFrame){
		return true;
	}
	
	return true;
}

bool SimCityStrategy::PylonDetect(){

	auto startBase = mapInfo->getMyStartBase();

	for (auto unit : bw->enemy()->getUnits()){

		if (unit->getType() == UnitTypes::Protoss_Pylon && (int)getDistance(unit->getTilePosition(), startBase->getTilePosition()) <= 30){
			pylon = unit;
			cmd->getStateMgr().setCombatFlag(COMBAT_FLAG::INVASION_PYLON, true);

			return true;
		}
	}

	return false;
}

bool SimCityStrategy::isBlock()
{
	bool supply = false;

	for (auto building : me->getImmediatelyBuilding(UnitTypes::Terran_Supply_Depot))
	{
		switch (mapInfo->getDir())
		{
		case 0:
			if (building->getSelf()->getTilePosition() == TilePosition(100, 7))
			{
				supply = true;
			}
			break;

		case 1:
			if (building->getSelf()->getTilePosition() == TilePosition(118, 99))
			{
				supply = true;
			}
			break;

		case 2:
			if (building->getSelf()->getTilePosition() == TilePosition(8, 26))
			{
				supply = true;
			}
			break;

		case 3:
			if (building->getSelf()->getTilePosition() == TilePosition(28, 121))
			{
				supply = true;
			}
			break;
		}
	}
	return supply;
}

bool SimCityStrategy::isInvasion(){
	for (auto p : VPylon){

		switch (mapInfo->getDir()){
		case 0:
			if (isContainOneTerm(TilePosition(99, 6), p->getTilePosition(), 7)){
				pylon = p;
				return true;
			}

			break;

		case 1:
			if (isContainOneTerm(TilePosition(112, 97), p->getTilePosition(), 7)){
				pylon = p;
				return true;
			}

			break;

		case 2:
			if (isContainOneTerm(TilePosition(6, 26), p->getTilePosition(), 7)){
				pylon = p;
				return true;
			}

			break;

		case 3:
			if (isContainOneTerm(TilePosition(22, 116), p->getTilePosition(), 7)){
				pylon = p;
				return true;
			}

			break;
		}
	}

	return false;
}

bool SimCityStrategy::ZealotDetect(){

	auto startBase = mapInfo->getMyStartBase();

	for (auto unit : bw->enemy()->getUnits()){
		if (unit->getType() == UnitTypes::Protoss_Zealot && getDistance(unit->getTilePosition(), startBase->getTilePosition()) < 40){
			return true;
		}
	}

	return false;
}


void SimCityStrategy::end(){
	for (auto scv : guardSCV){
		if (scv != nullptr && scv->getSelf()->exists()){
			scv->setState(SCV::STATE::RETURN);
		}
	}
	for (auto marine : guardmarine){
		if (marine != nullptr && marine->getSelf()->exists()){
			marine->setState(Marine::STATE::IDLE);
		}
	}
	for (auto scv : guardSCV){
		scvControl(scv);
	}
	for (auto marine : guardmarine){
		MarineControl(marine);
	}
}
