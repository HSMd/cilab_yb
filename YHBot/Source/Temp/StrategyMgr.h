#pragma once
#include "StateMgr.h"
#include "TerritoryMgr.h"

#include "SimCityStrategy_1.h"
#include "SimCityStrategy_2.h"
#include "FactoryStrategy.h"

class StrategyMgr
{
	SimStrategy_1 simCity_1;
	SimStrategy_2 simCity_2;
	FactoryStrategy factoryDouble;

public:

	StrategyMgr();
	~StrategyMgr();

	void init();
	void run(BUILD_FLAG build, COMBAT_FLAG combat);
	void Probedetect();
	void draw(BUILD_FLAG build, COMBAT_FLAG combat);
};

