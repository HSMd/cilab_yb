#pragma once
#include <BWAPI.h>
#include "MyUnit.h"

using namespace BWAPI;
using namespace std;

class SCVControl {

private:
	int SCVIM[128][128];
public:

	SCVControl();
	~SCVControl();
	static SCVControl* create();
	static SCVControl* getInstance();
	static SCVControl* s_SCVControl;
	void run(SCV*);
	void onFrame();

	//control func
	void WAIT_Control(SCV* unit);
	void RETURN_Control(SCV* unit);
	void HELP_Control(SCV* unit);

	Unit getTargetUnit(Unit unit);
	Unit getUnitforID(int id);

	Position getBestPosition(Unit);
	Unit getBestUnitToHelp(Unit);

	Position attackPosition;

};