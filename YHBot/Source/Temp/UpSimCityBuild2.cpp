#include "UpSimCityBuild2.h"
#include "../Single.h"
#include "UtilMgr.h"

UpSimCity_2::UpSimCity_2(){
		
	this->tMgr = &cmd->getTerritoryMgr();
	out = nullptr;
	safe = false;
	frameCount = 0;

	init();

	build.push_back(scheduleWork(UnitTypes::Terran_Siege_Tank_Tank_Mode, 5, UnitTypes::Terran_SCV, 15));
	build.push_back(scheduleWork(UnitTypes::Terran_Vulture, 2, UnitTypes::Terran_Siege_Tank_Tank_Mode, 3, true));

	build.push_back(scheduleWork(UnitTypes::Terran_Command_Center, getPosition(cntPoses), 90, UnitTypes::Terran_Factory, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Engineering_Bay, enginePos, UnitTypes::Terran_Command_Center, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, *AUTO_POS, UnitTypes::Terran_Engineering_Bay, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Siege_Tank_Tank_Mode, 3, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, *AUTO_POS, UnitTypes::Terran_Factory, 2, true));

	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Factory, 2));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Factory, 2));
	build.push_back(scheduleWork(UnitTypes::Terran_Academy, *AUTO_POS, UnitTypes::Terran_Factory, 4, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Academy, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Academy, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Comsat_Station, *AUTO_POS, UnitTypes::Terran_Factory, 6, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Comsat_Station, *AUTO_POS, UnitTypes::Terran_Factory, 6, true));

	// 터렛
	build.push_back(scheduleWork(UnitTypes::Terran_Missile_Turret, getPosition(turretPoses), 20, UnitTypes::Terran_Engineering_Bay, 1));
	build.push_back(scheduleWork(UnitTypes::Terran_Missile_Turret, getPosition(turretPoses), UnitTypes::Terran_Siege_Tank_Tank_Mode, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Missile_Turret, front_trtPos, UnitTypes::Terran_Factory, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Missile_Turret, getPosition(turretPoses), UnitTypes::Terran_Factory, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Missile_Turret, getPosition(turretPoses), UnitTypes::Terran_Factory, 4, true));
}

UpSimCity_2::~UpSimCity_2(){

}

void UpSimCity_2::init(){

	auto startTerritory = mapInfo->getMyStartBase();
	auto multi_2 = mapInfo->getCandidateLocation();

	switch (mapInfo->getDir()){
	case 0:
		cntPoses.push(TilePosition(97, 1));
		turretPoses.push(TilePosition(103, 6));
		turretPoses.push(TilePosition(102, 16));
		turretPoses.push(multi_2->getTilePosition() + TilePosition(-2, 4));
		turretPoses.push(startTerritory->getTilePosition() + TilePosition(-2, 3));

		front_brkPos = TilePosition(89, 16);
		front_splyPos = TilePosition(93, 18);
		front_trtPos = TilePosition(94, 16);
		enginePos = TilePosition(106, 17);

		break;

	case 1:
		cntPoses.push(TilePosition(123, 96));
		turretPoses.push(TilePosition(119, 101));
		turretPoses.push(TilePosition(106, 99));
		turretPoses.push(multi_2->getTilePosition() + TilePosition(4, -2));
		turretPoses.push(startTerritory->getTilePosition() + TilePosition(-2, 0));

		front_brkPos = TilePosition(105, 92);
		front_splyPos = TilePosition(109, 91);
		front_trtPos = TilePosition(110, 94);
		enginePos = TilePosition(106, 102);

		break;

	case 2:
		cntPoses.push(TilePosition(16, 25));
		turretPoses.push(TilePosition(8, 24));
		turretPoses.push(TilePosition(18, 27));
		turretPoses.push(multi_2->getTilePosition() + TilePosition(-2, 3));
		turretPoses.push(startTerritory->getTilePosition() + TilePosition(4, 3));

		front_brkPos = TilePosition(18, 36);
		front_splyPos = TilePosition(21, 34);
		front_trtPos = TilePosition(16, 33);
		enginePos = TilePosition(18, 22);

		break;

	case 3:
		cntPoses.push(TilePosition(29, 124));
		turretPoses.push(TilePosition(21, 120));
		turretPoses.push(TilePosition(24, 110));
		turretPoses.push(multi_2->getTilePosition() + TilePosition(4, -1));
		turretPoses.push(startTerritory->getTilePosition() + TilePosition(4, 0));
		

		front_brkPos = TilePosition(32, 110);
		front_splyPos = TilePosition(32, 108);
		front_trtPos = TilePosition(30, 112);
		enginePos = TilePosition(17, 110);

		break;
	}
}

void UpSimCity_2::run(COMBAT_FLAG combat) {

	auto startTerritory = tMgr->getAllTerritorys()[0];
	auto centers = me->getBuildings(UnitTypes::Terran_Command_Center);
	auto factory = me->getBuildings(UnitTypes::Terran_Factory);
	auto barrack = me->getBuildings(UnitTypes::Terran_Barracks);
	auto tanks = me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode);
	auto engine = me->getBuildings(UnitTypes::Terran_Engineering_Bay);

	auto next = mapInfo->getCandidateLocation();
	auto& state = cmd->getStateMgr();

	// 공격
	auto control = UnitControl::getInstance();
	if (4 <= me->getBuildings(UnitTypes::Terran_Factory).size() && 5 <= control->tanks.size()){
		for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
			(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
			(*q)->rPos = CombatInfo::getInstance()->enemyStartingBase->getPosition();
			VultureControl::getInstance()->run((*q), true);
		}
		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			(*q)->setState(Tank::STATE::CRAZY);
			(*q)->rPos = CombatInfo::getInstance()->enemyStartingBase->getPosition();
			TankControl::getInstance()->run((*q));
		}
		state.setCombatFlag(COMBAT_FLAG::ATTACK, true);
	}

	// 커멘더 센터를 빨리 짓기위해 가스를 잠시 빼둠
	if (!flags[GAS_CONTROL_1]){

		cmd->getStateMgr().setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, true);

		startTerritory->setGasSCVCount(1);
		for (int i=0;i<2;i++){
			startTerritory->addSCV(startTerritory->getGasWorker());
		}

		flags[GAS_CONTROL_1] = true;
	}

	// 커멘더 센터를 짓고나서 넣어둠
	if (!flags[GAS_CONTROL_2] && 2 <= me->getImmediatelyBuilding(UnitTypes::Terran_Command_Center).size()){
		startTerritory->setGasSCVCount(3);
		flags[GAS_CONTROL_2] = true;
	}

	// SCV 생산
	if (tMgr->getAllTerritorys().size() == 1) {
		if (cmd->getUnitScheduleCount(UnitTypes::Terran_SCV) < 1) {
			cmd->addScvCount();
			cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_SCV, startTerritory->getCmdCenter()->getSelf()), 1, 1);
		}
	}
	else if (tMgr->getAllTerritorys().size() == 2){

		int require = 0;
		for (auto territory : tMgr->getAllTerritorys()) {
			require += territory->getRequireSCVCount();
		}

		if (1 < require && cmd->getUnitScheduleCount(UnitTypes::Terran_SCV) < 1) {
			cmd->addScvCount();
			cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_SCV), 1, 1);
		}
	}	

	// 시즈모드 업글
	if (!flags[UPGRADE_SIEGE_MODE] && 1 <= me->getBuildings(UnitTypes::Terran_Machine_Shop).size() &&
		1 <= me->getImmediatelyUnit(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()) {
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[0]->getSelf()->research(TechTypes::Tank_Siege_Mode)) {
			flags[UPGRADE_SIEGE_MODE] = true;
		}
	}

	// 마인 업글
	if (flags[SCV_MOVE2] && !flags[UPGRADE_MINE] && 2 <= me->getBuildings(UnitTypes::Terran_Factory).size() && 3 <= tanks.size() && tanks[0]->getSelf()->canSiege()) {
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[0]->getSelf()->research(TechTypes::Spider_Mines)) {
			flags[UPGRADE_MINE] = true;
		}
	}

	// 엔지니어링 베이를 초크포인트로 가져다 두기.
	if (!flags[PREPARE_EXPAND_MULTI2] == engine.size()){
		auto enginerringBay = dynamic_cast<Structure*>(engine.front());
		auto pos = BWTA::getNearestChokepoint(mapInfo->getCandidateLocation()->getTilePosition())->getCenter();
		enginerringBay->orderMove(pos2tile(pos));

		moveBuilding.push_back(enginerringBay);

		flags[PREPARE_EXPAND_MULTI2] = true;
	}

	// 커센 들어두기	
	if (!flags[LIFT_CENTER2] && 2 <= centers.size()){
		Unit unit = centers[1]->getSelf();
		if (unit->isIdle()){
			centers[1]->getSelf()->lift();
			flags[LIFT_CENTER2] = true;
		}		
	}

	// 탱크 생산
	if (flags[EXPAND_MULTI2] && cmd->getUnitScheduleCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) < 1) {
		cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_Siege_Tank_Tank_Mode, factory[0]->getSelf()), 1, 2);
	}

	// 벌쳐 생산
	if (flags[EXPAND_MULTI2] && cmd->getUnitScheduleCount(UnitTypes::Terran_Vulture) < 1) {
		cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_Vulture), 1);
	}

	// 안전하면 옮깁니다.
	if (flags[LIFT_CENTER2] && !flags[EXPAND_MULTI2] && 1 <= tanks.size() && tanks[0]->getSelf()->canSiege()){
		auto cnter = dynamic_cast<Structure*>(centers[1]);
		auto brk = dynamic_cast<Structure*>(barrack[0]);

		brk->orderMove(front_brkPos);
		cnter->orderMove(next->getTilePosition());

		landBuilding.push_back(cnter);
		landBuilding.push_back(brk);

		cmd->addStructureSchedule(scheduleWork(UnitTypes::Terran_Supply_Depot, front_splyPos, UnitTypes::Terran_Factory, 1), 1, 2);

		cmd->getStateMgr().setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, false);
		cmd->getStateMgr().setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2, true);

		flags[EXPAND_MULTI2] = true;
	}

	// 커센이 지어지면 SCV를 옮깁니다.	
	if (!flags[SCV_MOVE2] && 2 <= centers.size()) {
		if (centers[1]->getSelf()->getTilePosition() == next->getTilePosition()) {
			mapInfo->addTerritory(next);
			cmd->addStructureSchedule(scheduleWork(UnitTypes::Terran_Missile_Turret, front_splyPos, UnitTypes::Terran_Factory, 1), 1, 2);

			tMgr->addTerritory(new Territory(mapInfo->getNearBaseLocation()));
			tMgr->getAllTerritorys()[1]->init(next, centers[1]);
			tMgr->getAllTerritorys()[0]->moveTerritory(tMgr->getAllTerritorys()[1]);			

			state.setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2, true);
			flags[SCV_MOVE2] = true;
		}
	}

	// 서플라이 자동 생산.
	if (2 <= me->getBuildings(UnitTypes::Terran_Factory).size() && frameCount + 800 < bw->getFrameCount()){
		if (me->getBuildings(UnitTypes::Terran_Factory).size() <= 2) {
			if (cmd->getSupply() < 10 * 2) {
				frameCount = bw->getFrameCount();
				cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
			}
		}
		else{
			if (cmd->getSupply() < 20 * 2) {
				frameCount = bw->getFrameCount();
				cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 2, 1);
			}
		}
	}

	// 배럭 들어올려주기.
	if (!flags[OPEN_FRONT] && 4 <= me->getBuildings(UnitTypes::Terran_Factory).size()){

		auto brk = dynamic_cast<Structure*>(barrack[0]);
		brk->orderMove(startTerritory->getBaseTilePosition() + TilePosition(5, 0));
		moveBuilding.push_back(brk);

		flags[OPEN_FRONT] = true;
	}

	// 예약된 BUILD 들이 점화되면 수행된다.
	for (auto& work : build){
		auto targetType = work.getTargetType();

		if (!work.getFire() && work.canFire()){
			if (targetType.isBuilding()){
				cmd->addStructureSchedule(work, 1, 0);
				work.onFire();
			}
			else{
				cmd->addUnitSchedule(targetType, work.getCount(), 0);
				work.onFire();
			}
		}
	}	

	move();
	movePos();
	errorSCV();
	chkEnd();
}

TilePosition UpSimCity_2::getPosition(queue<TilePosition>& poses){

	if (poses.empty()){
		return *AUTO_POS;
	}

	TilePosition re = poses.front();
	poses.pop();

	return re;
}

void UpSimCity_2::move(){

	for (auto iter = moveBuilding.begin(); iter != moveBuilding.end(); ++iter)
	{
		auto building = (*iter);

		if (building->getState() == Structure::STATE::MOVE){
			if (5 < getDistance(building->getSelf()->getTilePosition(), building->getTargetPos())){
				building->getSelf()->move(tile2pos(building->getTargetPos()));
			}
			else{
				if (!safe && building->getSelf()->getType() == UnitTypes::Terran_Engineering_Bay){
					int cnt = 0;
					for (auto unit : bw->enemy()->getUnits()){
						if (getDistance(unit->getTilePosition(), mapInfo->getMyStartBase()->getTilePosition()) < 30){
							cnt++;
						}
					}

					if (cnt <= 3){
						safe = true;
					}
				}

				iter = moveBuilding.erase(iter);
				iter--;
			}
		}
	}
}

void UpSimCity_2::movePos(){

	for (auto iter = landBuilding.begin(); iter != landBuilding.end(); ++iter)
	{
		auto building = (*iter);

		if (building->getState() == Structure::STATE::MOVE){
			if (getDistance(building->getSelf()->getTilePosition(), building->getTargetPos()) < 3){
				building->orderLand(building->getTargetPos());
			}
			else{
				building->getSelf()->move(tile2pos(building->getTargetPos()));
			}
		}
		else if (building->getState() == Structure::STATE::LAND){

			if (building->getSelf()->isIdle()){
				building->orderLand(building->getTargetPos());
			}
			else{
				if (!building->getSelf()->isLifted()){
					building->orderIdle();
					iter = landBuilding.erase(iter);
					iter--;
				}
			}
		}
	}

	/*
	for (auto building : landBuilding){
		if (building->getState() == Structure::STATE::MOVE){
			if (getDistance(building->getSelf()->getTilePosition(), building->getTargetPos()) < 3){
				building->orderLand(building->getTargetPos());
			}
			else{
				building->getSelf()->move(tile2pos(building->getTargetPos()));
			}
		}
		else if (building->getState() == Structure::STATE::LAND){

			if (building->getSelf()->isIdle()){
				building->orderLand(building->getTargetPos());
			}
			else{
				if (!building->getSelf()->isLifted()){
					building->orderIdle();
				}
			}
		}
	}
	*/
}

void UpSimCity_2::errorSCV(){

	if (flags[EXPAND_MULTI2]){
		return;
	}

	auto startTerritory = tMgr->getAllTerritorys()[0];

	// 입구가 막혔을 경우 쫒겨난 유닛에 대한 대쳐.
	if (out == nullptr){
		for (auto scv : startTerritory->getWorkSCV()){
			if (scv->getState() != SCV::STATE::RETURN){
				continue;
			}

			int dis = getDistance(scv->getSelf()->getTilePosition(), mapInfo->getNearBaseLocation()->getTilePosition());
			if (dis < 9){
				out = scv;
			}
		}
	}
	else{
		auto b = me->getBuildings(UnitTypes::Terran_Barracks)[0];
		if (!flags[ENTRANCE_OPEN]){
			bw << "OPEN" << endl;
			outPos = b->getSelf()->getTilePosition();
			if (b->getSelf()->isIdle() && b->getSelf()->isCompleted())
				if (b->getSelf()->lift())
					flags[ENTRANCE_OPEN] = true;
		}
		else{
			if (!flags[ENTRANCE_CLOSE] && getDistance(out->getSelf()->getTilePosition(), startTerritory->getCmdCenter()->getSelf()->getTilePosition()) < 17){
				bw << "CLOSE" << endl;
				if (b->getSelf()->land(outPos))
					flags[ENTRANCE_CLOSE] = true;
			}
		}
	}
}

bool UpSimCity_2::chkEnd(){

	return false;
}

// DEFENCE

void UpSimCity_2::tankDefence(){
	
	for (auto pos : siegePoses){
		if (holdTanks[pos] == nullptr){
			holdTanks[pos] = getIdleTank();

			if (holdTanks[pos] == nullptr){
				break;
			}
		}
	}

	for (auto pos : siegePoses){
		auto tank = holdTanks[pos];

		if (tank != nullptr && !tank->getSelf()->exists()) {
			holdTanks[pos] = nullptr;
			continue;
		}

		if (tank == nullptr || tank->getSelf()->canUnsiege() || !tank->getSelf()->isCompleted())
			continue;

		if (getDistance(pos, tank->getSelf()->getTilePosition()) < 2){
			if (tank->getSelf()->canSiege()){
				tank->getSelf()->siege();
			}		
		}
		else{
			holdTanks[pos]->getSelf()->move(tile2pos(pos));
		}
	}
}

Tank* UpSimCity_2::getIdleTank(){

	for (auto u : me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
		Tank* tank = dynamic_cast<Tank*>(u);
		if (tank->getState() == Tank::STATE::IDLE){
			tank->setState(Tank::STATE::MOVE);
			return tank;
		}
	}

	return nullptr;
}