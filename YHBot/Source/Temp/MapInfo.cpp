#include "MapInfo.h"
#include "UtilMgr.h"
#include "CombatInfo.h"

MapInfo::MapInfo()
{
	myStartBase = nullptr;
	oppStartBase = nullptr;

	int dir = 0;
}

MapInfo::~MapInfo()
{
}

MapInfo* MapInfo::getInstance(){
	static MapInfo instance;
	return &instance;
}

void MapInfo::init(){

	myStartBase = BWTA::getStartLocation(Broodwar->self());
	otherStartBases = BWTA::getStartLocations();
	otherBases = BWTA::getBaseLocations();

	// Del myStartBase
	if (myStartBase->getTilePosition() == TilePosition(117, 7)){
		dir = 0;
	}
	else if (myStartBase->getTilePosition() == TilePosition(117, 117)){
		dir = 1;
	}
	else if (myStartBase->getTilePosition() == TilePosition(7, 6)){
		dir = 2;
	}
	else if (myStartBase->getTilePosition() == TilePosition(7, 116)){
		dir = 3;
	}

	for (auto iter = otherStartBases.begin(); iter != otherStartBases.end(); ++iter){
		if ((*iter)->getPosition() == myStartBase->getPosition()){
			otherStartBases.erase(iter);

			Broodwar << (*iter)->getTilePosition().x << " " << (*iter)->getTilePosition().y << endl;
			break;
		}
	}

	for (auto iter = otherBases.begin(); iter != otherBases.end(); ++iter){
		if ((*iter)->getPosition() == myStartBase->getPosition()){
			otherBases.erase(iter);
			break;
		}
		
	}	

	addTerritory(myStartBase);
	allBases = BWTA::getBaseLocations();

	// BWTA Study
	/*
	baseLocations = BWTA::getStartLocations();
	start = BWTA::getStartLocation(bw->self());

	set<BWTA::BaseLocation*>::iterator iter = baseLocations.begin();
	for (iter; iter != baseLocations.end(); ++iter){
	//if ((*iter)->getPosition().x == start->getPosition().x && (*iter)->getPosition().y == start->getPosition().y){
	if ((*iter)->getPosition() == start->getPosition()){
	baseLocations.erase(iter);
	break;
	}
	}

	near = *(baseLocations.begin());
	path = BWTA::getShortestPath(start->getTilePosition(), near->getTilePosition());

	bw << "PATH SIZE : " << path.size() << endl;

	int x = start->getPosition().x - near->getPosition().x;
	int y = start->getPosition().y - near->getPosition().y;
	bw << x << endl;
	bw << y << endl;


	//testPos.x = path[0].x;
	//testPos.y = path[0].y;
	*/
}

int MapInfo::getDir(){
	return dir;
}

BWTA::BaseLocation* MapInfo::getMyStartBase(){
	return myStartBase;
}

BWTA::BaseLocation* MapInfo::getOppStartBase(){
	return oppStartBase;
}

set<BWTA::BaseLocation*> MapInfo::getOtherStartBases(){
	return otherStartBases;
}

set<BWTA::BaseLocation*> MapInfo::getOtherBases(){
	return otherBases;
}

set<BWTA::BaseLocation*> MapInfo::getAllBases(){
	return allBases;
}


BWTA::BaseLocation* MapInfo::getNearLocation(BWTA::BaseLocation* src, set<BWTA::BaseLocation*>& dst){
	BWTA::BaseLocation* near = nullptr;
	float minDis = 9999999.0;

	for (auto iter = dst.begin(); iter != dst.end(); ++iter){

		float dis = getDistance((*iter), src);
		if (dis == 0){
			continue;
		}

		if (dis < minDis){
			minDis = dis;
			near = (*iter);
		}
	}

	return near;
}

BWTA::BaseLocation* MapInfo::getCandidateLocation(){

	BWTA::BaseLocation* near = nullptr;
	float minDis = 9999999.0;	

	for (auto iter = otherBases.begin(); iter != otherBases.end(); ++iter){
		bool same = false;
		for (auto territory : myTerritory){
			if ((*iter) == territory){
				same = true;
				break;
			}
		}

		if (same){
			continue;
		}

		float dis = getDistance((*iter), myStartBase);
		if (dis < minDis){
			minDis = dis;
			near = (*iter);
		}
	}

	return near;
}

BWTA::BaseLocation* MapInfo::getNearStartBaseLocation(){
	return getNearLocation(myStartBase, otherStartBases);
}

BWTA::BaseLocation* MapInfo::getNearBaseLocation(){
	return getNearLocation(myStartBase, otherBases);
}

vector<BWTA::BaseLocation*>& MapInfo::getMyTerritory(){
	return myTerritory;
}

void MapInfo::addTerritory(BWTA::BaseLocation* base){
	myTerritory.push_back(base);
}