#pragma once
#include "Work.h"
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"

class Bacanic
{
	enum FLAG{
		UPGRADE_STIM,
		UPGRADE_SIEGE_MODE,
		
		ENTRANCE_OPEN,
		ENTRANCE_CLOSE,

		ATTACK_ALL,
	};

	TilePosition moveBrkPos;
	TilePosition brkPos;
	TilePosition outPos;

	list<scheduleWork> build;	
	TerritoryMgr* tMgr;

	map<int, bool> flags;
	vector<Structure*> moveBuilding;
	SCV* out;	

	int frameCount;

public:
	Bacanic();
	~Bacanic();

	void run(COMBAT_FLAG combat);
	void errorSCV();
	void move();

	bool getIsEnd();
};

