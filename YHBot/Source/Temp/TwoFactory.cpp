#include "TwoFactory.h"
#include "../Single.h"
#include "UtilMgr.h"

TwoFactory::TwoFactory(){

	this->tMgr = &cmd->getTerritoryMgr();
	out = nullptr;
	moveBrk = nullptr;

	frameCount = 0;
	factoryFrameCount = 0;

	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 2, UnitTypes::Terran_Factory, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 2, UnitTypes::Terran_Factory, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Siege_Tank_Tank_Mode, 3, UnitTypes::Terran_SCV, 15));
	build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, *AUTO_POS, UnitTypes::Terran_Factory, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_SCV, 19, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Machine_Shop, *AUTO_POS, UnitTypes::Terran_Factory, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 2, UnitTypes::Terran_Siege_Tank_Tank_Mode, 2, true));
}

TwoFactory::~TwoFactory(){

}

void TwoFactory::chkStrategy(){

	auto startTerritory = tMgr->getAllTerritorys()[0];
	auto& state = cmd->getStateMgr();
	int frame = bw->getFrameCount();
	auto choke = getNearestChokepoint(startTerritory->getBaseTilePosition());

	for (auto unit : bw->enemy()->getUnits()){

		if (unit->getType() == UnitTypes::Protoss_Zealot){
			if (getDistance(pos2tile(choke->getCenter()), unit->getTilePosition()) < 20){
				state.setCombatFlag(COMBAT_FLAG::ZEALOT_RUSH, true);
				strategyFlag[COMBAT_FLAG::ZEALOT_RUSH] = frame;
			}
		}
		else if (unit->getType() == UnitTypes::Protoss_Dragoon){
			if (getDistance(pos2tile(choke->getCenter()), unit->getTilePosition()) < 20){
				state.setCombatFlag(COMBAT_FLAG::DRAGOON_RUSH, true);
				strategyFlag[COMBAT_FLAG::DRAGOON_RUSH] = frame;
			}
		}
	}

	if (strategyFlag[COMBAT_FLAG::ZEALOT_RUSH] + 24 * 7 < frame){
		state.setCombatFlag(COMBAT_FLAG::ZEALOT_RUSH, false);
	}

	if (strategyFlag[COMBAT_FLAG::DRAGOON_RUSH] + 24 * 7 < frame){
		state.setCombatFlag(COMBAT_FLAG::DRAGOON_RUSH, false);
	}
}


void TwoFactory::run(COMBAT_FLAG combat){

	auto startTerritory = tMgr->getAllTerritorys()[0];
	
	// SCV 스케쥴러에 추가합니다.
	if (cmd->getUnitScheduleCount(UnitTypes::Terran_SCV) < tMgr->getAllTerritorys().size()){
		for (auto territory : tMgr->getAllTerritorys()){
			int require = territory->getRequireSCVCount();

			if (0 < require && cmd->getReadyTrain(territory->getCmdCenter()->getSelf())){
				cmd->addScvCount();
				cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_SCV, territory->getCmdCenter()->getSelf()), 1, 1);
			}
		}
	}

	// 예약된 BUILD 들이 점화되면 수행된다.
	for (auto& work : build){

		auto targetType = work.getTargetType();

		if (!work.getFire() && work.canFire()){

			if (startTerritory->getCmdCenter()->getSelf()->getTrainingQueue().size() < 1){
				break;
			}

			if (targetType.isBuilding()){

				if (work.getTargetType() == UnitTypes::Terran_Barracks){
					//SCV* scv = startTerritory->getWorkSCV()[0];
					//work.setBuilder(scv);
				}

				cmd->addStructureSchedule(work, 1, 0);
				work.onFire();
			}
			else{

				cmd->addUnitSchedule(targetType, work.getCount(), 0);
				work.onFire();
			}
		}
	}

	// 벌쳐 업글
	if (!flags[UPGRADE_RACE_SPEED] && 2 <= me->getBuildings(UnitTypes::Terran_Machine_Shop).size() &&
		1 <= me->getBuildings(UnitTypes::Terran_Factory)[1]->getSelf()->getTrainingQueue().size()){
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[1]->getSelf()->upgrade(UpgradeTypes::Ion_Thrusters))
			flags[UPGRADE_RACE_SPEED] = true;
	}

	// 마인 업글
	if (!flags[UPGRADE_MINE] && 1 <= me->getImmediatelyUnit(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()){
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[0]->getSelf()->research(TechTypes::Spider_Mines))
			flags[UPGRADE_MINE] = true;
	}

	// 시즈모드 업글
	if (!flags[UPGRADE_SIEGE_MODE] && 3 <= me->getImmediatelyUnit(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()){
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[0]->getSelf()->research(TechTypes::Tank_Siege_Mode)){
			flags[UPGRADE_SIEGE_MODE] = true;
		}
	}

	// 엔지니어링 베이
	if (!flags[ENGINEERING_BAY] && flags[ADVANCE_ALL]){

		cmd->addStructureSchedule(UnitTypes::Terran_Engineering_Bay, *AUTO_POS);
		flags[ENGINEERING_BAY] = true;
	}

	// 수정
	// 탱크 2마리 유지
	if (flags[ADVANCE_ALL] && me->getImmediatelyUnit(UnitTypes::Terran_Siege_Tank_Tank_Mode).size() < me->getBuildings(UnitTypes::Terran_Factory).size() && cmd->getUnitScheduleCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) == 0){
		cmd->addUnitSchedule(UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, 2);
	}

	// 벌쳐 지속적인 생산
	if (cmd->getUnitScheduleCount(UnitTypes::Terran_Vulture) == 0 && 1 <= me->getBuildings(UnitTypes::Terran_Factory).size()){
		cmd->addUnitSchedule(UnitTypes::Terran_Vulture, 1, 0);
	}

	
	// 서플라이 자동 생산.
	int size = me->getBuildings(UnitTypes::Terran_Factory).size();
	if (2 <= size && frameCount + 800 < bw->getFrameCount()){
		if (size == 2){
			if (cmd->getSupply() < 6 * 2){
				frameCount = bw->getFrameCount();
				cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
			}			
		}
		else if (2 < size){
			if (cmd->getSupply() < 6 * 2){
				frameCount = bw->getFrameCount();
				cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 2, 1);
			}
		}
	}
	
	// 건물 들어올리는 것 ( 진출 타이밍 )
	if (!flags[ADVANCE_ALL] && 3 <= me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).size() &&
		2 <= me->getUnits(UnitTypes::Terran_Vulture).size()){

		auto b = me->getBuildings(UnitTypes::Terran_Barracks)[0];
		auto brk = dynamic_cast<Structure*>(b);
		if (b->getSelf()->exists() && b->getSelf()->isIdle() && b->getSelf()->canLift()){
			
			brk->getSelf()->lift();

			BaseLocation* enemyBase = CombatInfo::create()->enemyStartingBase;
			if (enemyBase != nullptr){
				auto choke = BWTA::getNearestChokepoint(enemyBase->getTilePosition());
				brk->orderMove(pos2tile(choke->getCenter()));
				moveBrk = brk;
				flags[ADVANCE_ALL] = true;

				cmd->getStateMgr().setCombatFlag(COMBAT_FLAG::ATTACK, true);
			}
		}	
	}

	if (!flags[GAS_CONTROL_1] && 400 <= cmd->getGas() && 3< me->getBuildings(UnitTypes::Terran_Factory).size()){

		cmd->getStateMgr().setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, true);

		startTerritory->setGasSCVCount(1);
		for (int i = 0; i<2; i++){
			startTerritory->addSCV(startTerritory->getGasWorker());
		}

		flags[GAS_CONTROL_1] = true;
	}

	// 팩토리 늘리기
	int facSize = me->getBuildings(UnitTypes::Terran_Factory).size();
	if (flags[ADVANCE_ALL] && 2 <= facSize && facSize < 3 && 300 <= cmd->getMineral() && 0 == cmd->getUnitScheduleCount(UnitTypes::Terran_Factory) &&
		factoryFrameCount + 600 < bw->getFrameCount()){
		cmd->addStructureSchedule(UnitTypes::Terran_Factory, *AUTO_POS, 1, 1);
		factoryFrameCount = bw->getFrameCount();
	}

	// 배럭 옮기기.
	if (moveBrk != nullptr){
		if (1 < getDistance(moveBrk->getSelf()->getTilePosition(), moveBrk->getTargetPos())){
			moveBrk->getSelf()->move(tile2pos(moveBrk->getTargetPos()));
		}
	}

	errorSCV();
}



void TwoFactory::errorSCV(){

	if (flags[EXTAND_FRONT]){
		return;
	}

	auto startTerritory = tMgr->getAllTerritorys()[0];

	// 입구가 막혔을 경우 쫒겨난 유닛에 대한 대쳐.
	if (out == nullptr){
		for (auto scv : startTerritory->getWorkSCV()){
			if (scv->getState() != SCV::STATE::RETURN){
				continue;
			}

			int dis = getDistance(scv->getSelf()->getTilePosition(), mapInfo->getNearBaseLocation()->getTilePosition());
			if (dis < 9){
				out = scv;
			}
		}
	}
	else{
		auto b = me->getBuildings(UnitTypes::Terran_Barracks)[0];
		if (!flags[ENTRANCE_OPEN]){
			bw << "OPEN" << endl;
			outPos = b->getSelf()->getTilePosition();
			if (b->getSelf()->isIdle() && b->getSelf()->isCompleted())
				if (b->getSelf()->lift())
					flags[ENTRANCE_OPEN] = true;
		}
		else{
			if (!flags[ENTRANCE_CLOSE] && getDistance(out->getSelf()->getTilePosition(), startTerritory->getCmdCenter()->getSelf()->getTilePosition()) < 17){
				bw << "CLOSE" << endl;
				if (b->getSelf()->land(outPos))
					flags[ENTRANCE_CLOSE] = true;
			}
		}
	}
}

bool TwoFactory::getIsEnd(){

	return false;
}