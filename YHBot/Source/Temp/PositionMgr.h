#pragma once
#include "BWAPI.h"

class PosMgr{

	BWAPI::TilePosition startSplyPos;
	BWAPI::TilePosition startOtherPos;

	BWAPI::TilePosition startFacPos_1;
	BWAPI::TilePosition startFacPos_2;

	BWAPI::TilePosition splyPos;
	BWAPI::TilePosition facPos;
	BWAPI::TilePosition otherPos;

	int facSize;
	int splySize;

	bool dir;

public:
	static PosMgr* getInstance();

	void init();
	BWAPI::TilePosition getSplyPos(bool add);
	BWAPI::TilePosition getSplyPos(bool add, int idx);
	BWAPI::TilePosition getFtryPos(bool add);
	BWAPI::TilePosition getOthrPos(bool add);
};