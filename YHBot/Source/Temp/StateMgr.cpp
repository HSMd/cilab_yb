#include "StateMgr.h"

#include "../Single.h"
#include "UtilMgr.h"

StateMgr::StateMgr(){
	build = BUILD_FLAG::SIM_CITY_1;
	//build = BUILD_FLAG::FAST_FACTORY_DOUBLE;
	combat = COMBAT_FLAG::NORMAL;

	combatFlag[COMBAT_FLAG::NORMAL] = true;
};

StateMgr::~StateMgr(){

};

void StateMgr::show(){

	int xpos = 50;

	switch (build){
	case BUILD_FLAG::SIM_CITY_1:
		bw->drawTextScreen(xpos, 40, "SIM_CITY 1");
		break;

	case BUILD_FLAG::FACTORY_DOUBLE:
		bw->drawTextScreen(xpos, 40, "TWO FACTORY");
		break;

	case BUILD_FLAG::SIM_CITY_2:
		bw->drawTextScreen(xpos, 40, "SIM_CITY 2");
		break;

	case BUILD_FLAG::FAST_FACTORY_DOUBLE:
		bw->drawTextScreen(xpos, 40, "FAST TWO FACTORY");
		break;

	case BUILD_FLAG::BACANIC:
		bw->drawTextScreen(xpos, 40, "BACANIC");
		break;
	}

	switch (combat){
	case COMBAT_FLAG::NORMAL:
		bw->drawTextScreen(xpos, 60, "NORMAL");
		break;

	case COMBAT_FLAG::ENEMY_SEARCH:
		bw->drawTextScreen(xpos, 60, "ENEMY_SEARCH");
		break;

	case COMBAT_FLAG::INVASION_PYLON:
		bw->drawTextScreen(xpos, 60, "INVASION_PYLON");
		break;

	case COMBAT_FLAG::ZEALOT_RUSH:
		bw->drawTextScreen(xpos, 60, "ZEALOT_RUSH");
		break;

	case COMBAT_FLAG::ATTACK:
		bw->drawTextScreen(xpos, 60, "ZEALOT_RUSH");
	}
}

void StateMgr::run(){

	combat = NORMAL;

	if (build == BUILD_FLAG::SIM_CITY_1){

		if (combatFlag[COMBAT_FLAG::ENEMY_SEARCH]){
			combat = ENEMY_SEARCH;
		}

		if (combatFlag[COMBAT_FLAG::INVASION_PYLON]){
			combat = INVASION_PYLON;
		}

		if (combatFlag[COMBAT_FLAG::ZEALOT_RUSH]){
			combat = ZEALOT_RUSH;
		}

		if (buildFlag[BUILD_FLAG::SIM_CITY_1]){
			if (oppFlag[OPP_FLAG::FAST_FORECOUNT]){
				//build = BUILD_FLAG::SIM_CITY_2;
				build = BUILD_FLAG::FACTORY_DOUBLE;
			}
			else{
				//build = BUILD_FLAG::SIM_CITY_2;
				build = BUILD_FLAG::FACTORY_DOUBLE;
			}
		}
	}
	else if (build == BUILD_FLAG::FACTORY_DOUBLE){

		if (combatFlag[COMBAT_FLAG::ZEALOT_RUSH]){
			combat = ZEALOT_RUSH;
		}

		if (combatFlag[COMBAT_FLAG::DRAGOON_RUSH]){
			combat = DRAGOON_RUSH;
		}

		if (combatFlag[COMBAT_FLAG::ATTACK]){
			combat = ATTACK;
		}
	}
	else if (build == BUILD_FLAG::SIM_CITY_2){

		if (combatFlag[COMBAT_FLAG::ZEALOT_RUSH]){
			combat = ZEALOT_RUSH;
		}

		if (combatFlag[COMBAT_FLAG::DRAGOON_RUSH]){
			combat = DRAGOON_RUSH;
		}

		if (combatFlag[COMBAT_FLAG::ATTACK]){
			combat = ATTACK;
		}
	}
}