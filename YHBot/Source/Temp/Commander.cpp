#include <algorithm>

#include "Commander.h"
#include "UtilMgr.h"
#include "Normal.h"
#include "Invasion_pylon.h"
#include "../Single.h"

SCV* t_scv = nullptr;

Commander::Commander()
{
	delayMineral = 0;
	delayGas = 0;
	scvCount = 4;
}

Commander::~Commander()
{
	BWTA::cleanMemory();
}

Commander* Commander::getInstance(){

	static Commander instance;
	return &instance;
}

///////////////////////////////////////////////////

// On Func // 
void Commander::init()
{
	// Hello World!
	bw->sendText("Hello I'm MJ_BOT Beta 1.0 ver");
	bw->sendText("Welcome CILab Oing");
	bw->sendText("please sal sal :D");

	bw << "The map is " << bw->mapName() << "!" << std::endl;

	bw->enableFlag(Flag::UserInput);

	// Speed 0 - 10
	bw->setLocalSpeed(10);
	bw->setCommandOptimizationLevel(0);

	BWTA::readMap();
	BWTA::analyze();

	mapInfo->getInstance()->init();
	pMgr->init();

	builder = new BuildMgr();
	strategist = new StrategyMgr();

	combatInfo = CombatInfo::create();
	unitControl = UnitControl::create();
	scouting = Scouting::create();
}

void Commander::start(){
}

void Commander::show(){

	// draw
	//drawTerrainData();

	// draw Text
	bw->drawTextScreen(200, 40, "ALL_SCV : %d", me->getUnits(UnitTypes::Terran_SCV).size());
	bw->drawTextScreen(200, 60, "SCV_1 : %d", territoryMgr.getAllTerritorys()[0]->getSCVCount());
	if (2 <= territoryMgr.getAllTerritorys().size()){
		bw->drawTextScreen(200, 80, "SCV_2 : %d", territoryMgr.getAllTerritorys()[1]->getSCVCount());
	}	
	bw->drawTextScreen(200, 100, "Schedule : %d", schedule.size());
	//bw->drawTextScreen(200, 100, "Delay_Mineral : %d", delayMineral);

	if (t_scv != nullptr){
		auto nearBase = mapInfo->getNearLocation(mapInfo->getMyStartBase(), mapInfo->getAllBases());
		//int dis = getDistance(t_scv->getSelf()->getTilePosition(), nearBase->getTilePosition());
		int dis = getDistance(t_scv->getSelf()->getTilePosition(), mapInfo->getMyStartBase()->getTilePosition());
		bw->drawTextScreen(200, 100, "Distance : %d", dis);

		if (t_scv->getSelf()->isCarryingMinerals()){
			bw->drawTextScreen(200, 120, "Carry", dis);
		}
		else{
			bw->drawTextScreen(200, 120, "Nope", dis);
		}
	}

	if (0 < schedule.size()){
		//bw->drawTextScreen(200, 100, "Next : %s", schedule.front()->getTargetType().c_str);
		//bw << schedule.front()->getTargetType() << endl;
	}

	state.show();
	
	combatInfo->drawCombatInfo(true);
	Me::getInstance()->drawMe();
	unitControl->drawUnitControl();	
	
	drawSignal();

	BUILD_FLAG build = state.getBuild();
	COMBAT_FLAG combat = state.getCombat();
	strategist->draw(build, combat);
}

void Commander::run()
{	
	/*
	if (t_scv == nullptr){
		t_scv = territoryMgr.getAllTerritorys()[0]->getWorker(true);
	}
	
	territoryMgr.run();

	runScheduleEX();

	return;
	*/
		
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&starti);

	//double elapsedTime1 = (clock[0].QuadPart - mapInfoClock.QuadPart) * 1000.0 / frequency.QuadPart;
	// update
	me->update();
	Me::getInstance()->run();

	QueryPerformanceCounter(&meClock);
	// StateMgr
	state.run();
	BUILD_FLAG build = state.getBuild();
	COMBAT_FLAG combat = state.getCombat();
	
	builder->run(build, combat);
	strategist->run(build, combat);
	QueryPerformanceCounter(&statClock);	

	// Territory run
	territoryMgr.run();
	QueryPerformanceCounter(&terrClock);

	// product
	runScheduleEX();
	QueryPerformanceCounter(&scheClock);

	// Combat Information
	combatInfo->updateEnemyInfo();
	combatInfo->updateMyInfo();
	QueryPerformanceCounter(&cbifClock);

	// Unit Control
	unitControl->run();
	QueryPerformanceCounter(&contClock);

	// Scouting
	scouting->run();
	QueryPerformanceCounter(&soutClock);

	//startegy
	
}

 ///// Check /////
bool Commander::enoughResource(UnitType target, int readyMineral){

	if (getSupply() < target.supplyRequired()){

		return false;
	}

	if (getMineral() < target.mineralPrice() - readyMineral || 
		getGas() < target.gasPrice() - 20) {

		return false;
	}

	return true;
}

bool Commander::enoughWhatBuild(UnitType target){

	auto rUnits = target.requiredUnits();
	for (auto require : rUnits){
		UnitType rType = require.first;

		if (rType.isBuilding()){
			if (me->getBuildings(require.first).size() < 1){
				return false;
			}
		}
		else{
			if (me->getUnits(require.first).size() < 1){
				return false;
			}
		}		
	}	

	return true;
}

bool Commander::canCrate(UnitType target){
	
	if (!enoughResource(target, 50)){
		return false;
	}	
	
	if (!enoughWhatBuild(target)){
		return false;
	}

	return true;
}

bool Commander::getReadyTrain(Unit unit){
	
	if (unit->exists()){
		if (unit->isIdle()){
			return true;
		}
		//else if (unit->getTrainingQueue().size() == 1 && 0 < unit->getRemainingTrainTime() && unit->getRemainingTrainTime() < 15){
		else if (unit->getTrainingQueue().size() == 1 && 0 < unit->getRemainingTrainTime() && unit->getRemainingTrainTime() < 15){
			return true;
		}		
	}

	return false;
}


///// Schedule Func //////
bool isUnit(BasicWork* work){

	if (!work->getTargetType().isBuilding()){
		if (UnitTypes::Terran_SCV == work->getTargetType()){
			return false;
		}
		return true;
	}
	else
		return false;
}

int Commander::getUnitScheduleCount(UnitType target){
	int count = 0;
	for (auto work : schedule){
		if (work->getTargetType() == target)
			count++;
	}

	return count;
}

bool Commander::addUnitSchedule(UnitType target, int count, int hurry){

	if (hurry == 2){
		for (int i = 0; i < count; i++)
			schedule.push_front(new UnitWork(target));
	}
	else if (hurry == 1){
		auto iter = find_if(schedule.begin(), schedule.end(), isUnit);

		for (int i = 0; i < count; i++)
			schedule.insert(iter, new UnitWork(target));
	}
	else{
		for (int i = 0; i < count; i++)
			schedule.push_back(new UnitWork(target));
	}

	return true;
}

bool Commander::addUnitSchedule(UnitWork& work, int count, int hurry){
	
	if (hurry == 2){
		for (int i = 0; i < count; i++)
			schedule.push_front(new UnitWork(work));
	}
	else if (hurry == 1){
		auto iter = find_if(schedule.begin(), schedule.end(), isUnit);

		for (int i = 0; i < count; i++)
			schedule.insert(iter, new UnitWork(work));
	}
	else{
		for (int i = 0; i < count; i++)
			schedule.push_back(new UnitWork(work));
	}
	
	return true;
}

bool Commander::addStructureSchedule(UnitType target, TilePosition pos, int count, int hurry){

	if (hurry == 2){
		for (int i = 0; i < count; i++)
			schedule.push_front(new BuildingWork(target, pos, 45));
	}
	else if (hurry == 1){
		auto iter = find_if(schedule.begin(), schedule.end(), isUnit);

		for (int i = 0; i < count; i++)
			schedule.insert(iter, new BuildingWork(target, pos, 45));
	}
	else{
		for (int i = 0; i < count; i++)
			schedule.push_back(new BuildingWork(target, pos, 45));
	}

	return true;
}

bool Commander::addStructureSchedule(scheduleWork& work, int count, int hurry){

	if (hurry == 2){
		for (int i = 0; i < count; i++)
			schedule.push_front(new BuildingWork(work));
	}
	else if (hurry == 1){
		auto iter = find_if(schedule.begin(), schedule.end(), isUnit);

		for (int i = 0; i < count; i++)
			schedule.insert(iter, new BuildingWork(work));
	}
	else{
		for (int i = 0; i < count; i++)
			schedule.push_back(new BuildingWork(work));
	}


	return true;
}

void Commander::runSchedule(){

	for (auto iter = schedule.begin(); iter != schedule.end();){
		BasicWork* work = (*iter);
		UnitType targetType = (*iter)->getTargetType();

		if (targetType.isBuilding() && !targetType.isAddon())
		{
			// Building
			
			if (!canCrate(targetType)){
				break;
			}
			
			/*
			if (!enoughWhatBuild(targetType)){
				iter++;
				continue;
			}

			if (!enoughResource(targetType, 45)){
				iter++;
				continue;
			}
			*/

			delayMineral += targetType.mineralPrice();
			delayGas += targetType.gasPrice();

			BuildingWork* bWork = dynamic_cast<BuildingWork*>(work);

			if (bWork->getBuilder() == nullptr){
				SCV* scv = territoryMgr.getNearTerritory(bWork->getTilePosition())->getWorker(false);

				if (bWork->getTilePosition() == TilePosition(0, 0)){
					TilePosition targetBuildLocation = Broodwar->getBuildLocation(bWork->getTargetType(), scv->getSelf()->getTilePosition());  // 여기서 한번 주금
					bWork->setTilePosition(targetBuildLocation);
					scv->orderBuild(bWork->getTargetType(), targetBuildLocation);
				}
				else{
					scv->orderBuild(bWork->getTargetType(), bWork->getTilePosition());
				}
			}
			else{
				if (!bWork->getBuilder()->getSelf()->isConstructing()){
					bWork->getBuilder()->orderBuild(bWork->getTargetType(), bWork->getTilePosition());
				}
				else{
					break;
				}
			}			

			iter = schedule.erase(iter);
			delete work;
		}
		else if (targetType.isAddon()){
			// Addon
			if (!enoughWhatBuild(targetType)){
				iter++;
				continue;
			}

			if (!enoughResource(targetType)){
				break;
			}

			auto& what = me->getBuildings(targetType.whatBuilds().first);
			Unit u = nullptr;

			for (auto unit : what){
				if (unit->getSelf()->canBuildAddon() && unit->getSelf()->exists()){
					u = unit->getSelf();

					break;
				}
			}			

			if (u == nullptr || !u->isIdle())
			{
				iter++;
				continue;
			}

			if (u->buildAddon(targetType)){
				delayMineral += targetType.mineralPrice();
				delayGas += targetType.gasPrice();

				iter = schedule.erase(iter);
				delete work;
			}
			else{
				break;
			}
		}
		else{
			// Unit
			if (!enoughWhatBuild(targetType)){
				iter++;
				continue;
			}

			if (!enoughResource(targetType)){
				iter++;
				continue;
			}

			auto uWork = dynamic_cast<UnitWork*>(work);
			Unit trainer = nullptr;

			if (uWork->getTrainer() == nullptr){
				trainer = getProperBuilding(targetType);
			}
			else{
				trainer = uWork->getTrainer();
				if (!trainer->isIdle()){
					iter++;
					continue;
				}
			}
			
			if (trainer == nullptr){
				iter++;
				continue;
			}

			if (trainer->train(targetType)){

				//bw << "Success" << endl;

				iter = schedule.erase(iter);
				delete work;
			}
			else{
				//bw << "Fail" << endl;

				iter++;
				continue;
			}
		}
	}
}

void Commander::runScheduleEX(){
	
	for (auto iter = schedule.begin(); iter != schedule.end(); ++iter){
		
		BasicWork* work = (*iter);
		UnitType targetType = (*iter)->getTargetType();

		// 이전에 필요한 것들이 없다면 넘겨라.
		if (!enoughWhatBuild(targetType)){
			continue;
		}

		if (targetType.isBuilding() && !targetType.isAddon()){

			if (runBuildingSchedule(work, iter))
				break;
		}
		else if (targetType.isAddon()){
			
			if (runAddonSchedule(work, iter))
				break;
		}
		else{

			if (runUnitSchedule(work, iter))
				break;
		}
	}
}

bool Commander::runUnitSchedule(BasicWork* work, list<BasicWork*>::iterator& iter){

	auto targetType = work->getTargetType();	
	auto uWork = dynamic_cast<UnitWork*>(work);
	Unit trainer = nullptr;
	
	// 트레이너가 있으나 돈이 부족할 경우.
	if (!enoughResource(targetType)){
		return false;
	}

	// work 에서 트레이너가 선택 되어 있지 않을 경우 트레이너를 구함
	if (uWork->getTrainer() == nullptr){
		trainer = getProperBuilding(targetType);
	}
	// work 에서 트레이너가 선택 되어 있으나 트레이너가 생산 준비가 되지 않았을 경우.
	else{
		trainer = uWork->getTrainer();
		if (!getReadyTrain(trainer)){
			trainer = nullptr;
		}
	}

	// 트레이너가 없을 경우.
	if (trainer == nullptr){
		return false;
	}	

	// 유닛을 뽑을 트레이너가 정해져 있지만 train을 실패했을 경우.
	if (trainer->train(targetType)){

		iter = schedule.erase(iter);
		iter--;

		delete work;
	}

	return false;
}

bool Commander::runBuildingSchedule(BasicWork* work, list<BasicWork*>::iterator& iter){

	// Building
	auto targetType = work->getTargetType();
	BuildingWork* bWork = dynamic_cast<BuildingWork*>(work);

	if (!enoughResource(targetType, bWork->getPre())){
		return false;
	}
	
	if (targetType == UnitTypes::Terran_Command_Center){
		bw << targetType << endl;
	}	
	
	SCV* scv = nullptr;
	// Work에 빌더가 없을 경우.
	if (bWork->getBuilder() == nullptr){		

		// 타겟 포지션이 자동일 경우.
		if (bWork->getTilePosition() == *AUTO_POS){
			//scv = territoryMgr.getNearTerritory(bWork->getTilePosition())->getWorker(false);
			//scv = territoryMgr.getAllTerritorys().back()->getWorker(false);

			TilePosition targetBuildLocation;

			if (targetType == UnitTypes::Terran_Supply_Depot){
				targetBuildLocation = pMgr->getSplyPos(true);
				if (targetBuildLocation == *AUTO_POS) {
					scv = territoryMgr.getAllTerritorys()[0]->getWorker(false); 
					targetBuildLocation = Broodwar->getBuildLocation(bWork->getTargetType(), scv->getSelf()->getTilePosition());  // 199/200에서 디짐 1 && 165/166에서 디짐 131/132에서 디짐 20번 정도 디짐
				}
			}
			else if (targetType == UnitTypes::Terran_Factory){
				targetBuildLocation = pMgr->getFtryPos(true);
				bw << targetBuildLocation << endl;
				//targetBuildLocation = Broodwar->getBuildLocation(bWork->getTargetType(), scv->getSelf()->getTilePosition());
			}
			else if (targetType == UnitTypes::Terran_Refinery){
				scv = territoryMgr.getAllTerritorys().back()->getWorker(false);
				targetBuildLocation = Broodwar->getBuildLocation(bWork->getTargetType(), scv->getSelf()->getTilePosition());		// 여기서 한번 죽었음요 (다크왔을때)
			}
			else{
				//targetBuildLocation = Broodwar->getBuildLocation(bWork->getTargetType(), scv->getSelf()->getTilePosition());
				targetBuildLocation = pMgr->getOthrPos(true);
			}			

			if (scv == nullptr) {
				scv = territoryMgr.getNearTerritory(bWork->getTilePosition())->getWorker(targetBuildLocation, false);
			}

			bWork->setTilePosition(targetBuildLocation);
		}
		else{
			scv = territoryMgr.getNearTerritory(bWork->getTilePosition())->getWorker(bWork->getTilePosition(), false);
			//scv = territoryMgr.getNearTerritory(bWork->getTilePosition())->getWorker(false);
		}
	}
	else{

		//bw << "Builder" << endl;

		// Work에 빌더가 있을 경우
		scv = bWork->getBuilder();

		// 만약 빌더가 죽었다면.
		if (!scv->getSelf()->exists()){
			bw << "DIE" << endl;
			scv = territoryMgr.getNearTerritory(bWork->getTilePosition())->getWorker(false);
			if (scv == nullptr){
				return false;
			}
		}

		// 빌더가 건설 중일경우
		if (scv->getSelf()->isConstructing()){
			return true;
		}

		// 타겟 포지션이 자동일 경우.
		if (bWork->getTilePosition() == *AUTO_POS){
			TilePosition targetBuildLocation = Broodwar->getBuildLocation(bWork->getTargetType(), scv->getSelf()->getTilePosition());
			bWork->setTilePosition(targetBuildLocation);
		}
	}

	
	if (scv == nullptr){
		return false;
	}

	scv->orderBuild(bWork->getTargetType(), bWork->getTilePosition());

	delayMineral += targetType.mineralPrice();
	delayGas += targetType.gasPrice();

	iter = schedule.erase(iter);
	iter--;

	delete work;

	return false;
}

bool Commander::runAddonSchedule(BasicWork* work, list<BasicWork*>::iterator& iter){

	auto targetType = work->getTargetType();

	if (!enoughResource(targetType)){
		return true;
	}

	auto building = getProperBuilding(targetType);
	if (building == nullptr)
	{
		return false;
	}

	if (building->buildAddon(targetType)){
		delayMineral += targetType.mineralPrice();
		delayGas += targetType.gasPrice();

		iter = schedule.erase(iter);
		iter--;
		delete work;
	}
	else{
		return true;
	}

	return false;
}

Unit Commander::getProperBuilding(UnitType target){
	
	auto what = me->getBuildings(target.whatBuilds().first);

	for (auto b : what){
		Unit trainer = b->getSelf();

		if (target.isAddon()){
			if (trainer->canBuildAddon() && trainer->exists() && trainer->isIdle()){
				return trainer;
			}
		}
		else
		{
			if (getReadyTrain(trainer)){
				return trainer;
			}
		}
	}

	return nullptr;
}


///// Create & Complete /////
void Commander::structureCreate(Unit unit)
{
	me->addBuildingImmediately(unit);

	if (50 < bw->getFrameCount()){
		delayMineral -= unit->getType().mineralPrice();
		delayGas -= unit->getType().gasPrice();
	}

	builder->onStructureCrate(unit, state.getBuild());
}

void Commander::structureComplete(Unit unit)
{
	me->addBuilding(unit);

	if (unit->getType() == UnitTypes::Terran_Command_Center){

		territoryMgr.addTerritory(unit);
	}

	if (unit->getType() == UnitTypes::Terran_Refinery){

		territoryMgr.gasCompleted(unit);
	}

	territoryMgr.completeSCVReturn(unit);
	builder->onStructureComplete(unit, state.getBuild());
}

void Commander::unitCreate(Unit unit)
{
	me->addUnitImmediately(unit);

	if (UnitTypes::Terran_SCV == unit->getType()){

		territoryMgr.addSCV(unit);
	}

	builder->onUnitCrate(unit, state.getBuild());
}

void Commander::unitComplete(Unit unit)
{
	me->addUnit(unit);
	me->addAllUnit(unit);

	builder->onUnitComplete(unit, state.getBuild());
}


/////
int Commander::getMineral(){
	return me->minerals() - delayMineral;
};

int Commander::getGas(){
	return me->gas() - delayGas;
};

int Commander::getSupply(){
	return me->player()->supplyTotal() - me->player()->supplyUsed();
};

void Commander::drawSignal() {
	if (1) {
		int clockSize = 7;
		int startX = 360;
		int startY = 50;
		int height = 10;
		int index = 0;
		/*BWAPI::Broodwar->drawTextScreen(BWAPI::Position(width * index, height * index), "%cbuildPlacerClock : %d", BWAPI::Text::Align_Right, buildPlacerClock - start);
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(width * index, height * index), "%cbuildOrderClock : %d", BWAPI::Text::Align_Right, buildOrderClock - start);
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(width * index, height * index), "%cworkerManagerClock : %d", BWAPI::Text::Align_Right, workerManagerClock - start);
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(width * index, height * index), "%cmicroManagerClock : %d", BWAPI::Text::Align_Right, microManagerClock - start);
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(width * index, height * index), "%cscoutingRemodelClock : %d", BWAPI::Text::Align_Right, scoutingRemodelClock - start);
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(width * index, height * index), "%ccommanderClock : %d", BWAPI::Text::Align_Right, commanderClock - start);
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(width * index, height * index), "%cconfidenceClock : %d", BWAPI::Text::Align_Right, confidenceClock - start);*/
		double elapsedTime1;
		double elapsedTime2;
		double elapsedTime3;
		double elapsedTime4;
		double elapsedTime5;
		double elapsedTime6;
		double elapsedTime7;
		double sum = 0;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX, startY + height * index), "ME");
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX, startY + height * index), "STATE");
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX, startY + height * index), "TERRYTORY");
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX, startY + height * index), "SCHEDULE");
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX, startY + height * index), "COMBATINFO");
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX, startY + height * index), "CONTROL");
		index++;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX, startY + height * index), "SCOUTING");
		index = 0;
		int addWidth = 70;
		statClock, builClock, straClock, terrClock, scheClock, cbifClock, contClock, soutClock;
		sum += elapsedTime1 = (meClock.QuadPart - starti.QuadPart) * 1000.0 / frequency.QuadPart;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX + addWidth, startY + height * index), ": %lf", elapsedTime1);
		index++;
		sum += elapsedTime2 = (statClock.QuadPart - meClock.QuadPart) * 1000.0 / frequency.QuadPart;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX + addWidth, startY + height * index), ": %lf", elapsedTime2);
		index++;
		sum += elapsedTime3 = (terrClock.QuadPart - statClock.QuadPart) * 1000.0 / frequency.QuadPart;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX + addWidth, startY + height * index), ": %lf", elapsedTime3);
		index++;
		sum += elapsedTime4 = (scheClock.QuadPart - terrClock.QuadPart) * 1000.0 / frequency.QuadPart;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX + addWidth, startY + height * index), ": %lf", elapsedTime4);
		index++;
		sum += elapsedTime5 = (cbifClock.QuadPart - scheClock.QuadPart) * 1000.0 / frequency.QuadPart;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX + addWidth, startY + height * index), ": %lf", elapsedTime5);
		index++;
		sum += elapsedTime6 = (contClock.QuadPart - cbifClock.QuadPart) * 1000.0 / frequency.QuadPart;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX + addWidth, startY + height * index), ": %lf", elapsedTime6);
		index++;
		sum += elapsedTime7 = (soutClock.QuadPart - contClock.QuadPart) * 1000.0 / frequency.QuadPart;
		BWAPI::Broodwar->drawTextScreen(BWAPI::Position(startX + addWidth, startY + height * index), ": %lf", elapsedTime7);

		sum *= 100;
		elapsedTime1 *= 10;
		elapsedTime2 *= 10;
		elapsedTime3 *= 10;
		elapsedTime4 *= 10;
		elapsedTime5 *= 10;
		elapsedTime6 *= 10;
		elapsedTime7 *= 10;
		index = 0;
		addWidth = 150;
		int paddingY = 2;
		/*BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime8 / sum) * 100, startY * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime9 / sum) * 100, startY * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime1 / sum) * 100, startY * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime2 / sum) * 100, startY * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime3 / sum) * 100, startY * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime4 / sum) * 100, startY * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime5 / sum) * 100, startY * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime6 / sum) * 100, startY * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime7 / sum) * 100, startY * (index + 1) - paddingY), BWAPI::Colors::White, true);
		*/
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY + height * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime1), startY + height * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY + height * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime2), startY + height * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY + height * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime3), startY + height * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY + height * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime4), startY + height * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY + height * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime5), startY + height * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY + height * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime6), startY + height * (index + 1) - paddingY), BWAPI::Colors::White, true);
		index++;
		BWAPI::Broodwar->drawBoxScreen(BWAPI::Position(startX + addWidth, startY + height * index + paddingY), BWAPI::Position(startX + addWidth + (elapsedTime7), startY + height * (index + 1) - paddingY), BWAPI::Colors::White, true);


	}
}