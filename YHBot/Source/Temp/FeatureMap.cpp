#include "FeatureMap.h"
#include "UtilMgr.h"
#include <BWAPI.h>
#include <BWTA.h>

#include <tchar.h>
#include <Windows.h>

#define SLOT_NAME _T("\\\\.\\mailslot\\mailbox")
#define DIR_LEN MAX_PATH + 1

using namespace BWAPI;

Data data;

bool isRun = true;
bool canSend = false;
bool expand = false;

void setRun(bool run){
	isRun = run;
}

int ipc(){

	// MailSlot
	HANDLE hMailSlot;
	DWORD bytesWritten;

	data.end = false;

	hMailSlot = CreateFile(
		SLOT_NAME,
		GENERIC_WRITE,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL
		);

	if (hMailSlot == INVALID_HANDLE_VALUE){
		_fputts(_T("Unable to create Mailslot! \n"), stdout);
		return 1;
	}

	while (isRun){

		//_fputts(_T("MY CMD>"), stdout);
		//_fgetts(message, sizeof(message) / sizeof(TCHAR), stdin);

		if (!canSend)
			continue;

		if (!WriteFile(hMailSlot, &data, sizeof(Data), &bytesWritten, NULL)){
			_fputts(_T("Unable to write!!"), stdout);
			CloseHandle(hMailSlot);
			return 1;
		}

		Sleep(1000 / 30);
	}

	data.end = true;
	WriteFile(hMailSlot, &data, sizeof(Data), &bytesWritten, NULL);

	CloseHandle(hMailSlot);

	return 0;
}

void setVision(){

	// VISION

	//memset(data.vision, 0, sizeof(int) * MAP_SIZE * MAP_SIZE);

	for (int y = 0; y < MAP_SIZE; y++){
		for (int x = 0; x < MAP_SIZE; x++){
			if (BWAPI::Broodwar->isVisible(TilePosition(x, y))){
				data.vision[y][x] = VISION_CLEAR;
			}
		}
	}
}

int getProgress(Unit unit){

	int re = 0;

	if (unit->getType().isBuilding()){
		if (unit->isCompleted()){
			// 진행률
			re = (int)(100 * (float)unit->getHitPoints() / unit->getType().maxHitPoints());
		}
		else{
			// 미완성
			re = 100 - (int)(((float)unit->getRemainingBuildTime() / unit->getType().buildTime()) * 100);
		}
	}
	else{
		re = (int)(100 * (float)unit->getHitPoints() / unit->getType().maxHitPoints());
	}

	return re;
}

void setData(){

	ZeroMemory(&data, sizeof(data));

	setVision();

	// 미네랄과 가스
	auto bases = BWTA::getBaseLocations();
	for (auto base : bases){
		for (auto mineral : base->getMinerals()){
			int ypos = mineral->getTilePosition().y;
			int xpos = mineral->getTilePosition().x;

			data.players[ypos][xpos] = NEUTRALITY;
			data.unitType[ypos][xpos] = mineral->getType();
			data.progress[ypos][xpos] = getProgress(mineral);
		}

		/*
		// 가스 에러 모르겠음.
		for (auto gas : base->getGeysers()){
		int ypos = gas->getTilePosition().y;
		int xpos = gas->getTilePosition().x;

		data.players[ypos][xpos] = NEUTRALITY;
		data.unitType[ypos][xpos] = gas->getType();
		data.progress[ypos][xpos] = getProgress(gas);
		}
		*/
	}

	// 우리편 유닛
	for (auto unit : BWAPI::Broodwar->self()->getUnits()){

		if (!unit->exists()){
			continue;
		}

		if (!unit->getType().isBuilding() && !unit->isCompleted()){
			continue;
		}

		int ypos = unit->getTilePosition().y;
		int xpos = unit->getTilePosition().x;

		data.players[ypos][xpos] = SELF;
		data.unitType[ypos][xpos] = unit->getType();
		data.progress[ypos][xpos] = getProgress(unit);
	}

	// 적군 유닛
	for (auto unit : BWAPI::Broodwar->enemy()->getUnits()){

		if (!unit->exists()){
			continue;
		}

		if (!unit->getType().isBuilding() && !unit->isCompleted()){
			continue;
		}

		int ypos = unit->getTilePosition().y;
		int xpos = unit->getTilePosition().x;

		data.players[ypos][xpos] = ENEMY;
		data.unitType[ypos][xpos] = unit->getType();
		data.progress[ypos][xpos] = getProgress(unit);
	}

	data.expand = expand;
	TilePosition screenTilePos = pos2tile(BWAPI::Broodwar->getScreenPosition());
	if (EXPAND_END_WIDTH < screenTilePos.x){
		data.screenX = EXPAND_END_WIDTH;
	}
	else{
		data.screenX = screenTilePos.x;
	}

	if (EXPAND_END_HEIGHT < screenTilePos.y){
		data.screenY = EXPAND_END_HEIGHT;
	}
	else{
		data.screenY = screenTilePos.y;
	}
}