#include "Normal.h"
#include "../single.h"
#include "UtilMgr.h"

Normal::Normal(){
	
	switch (mapInfo->getDir()){
	case 0:
		marinePos.push_back(tile2pos(TilePosition(100, 6)));
		marinePos.push_back(tile2pos(TilePosition(101, 6)));// +Position(0, 16));
		marinePos.push_back(tile2pos(TilePosition(103, 8)));
		marinePos.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
		marinePos.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));
		break;
	case 1:
		marinePos.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 0));
		marinePos.push_back(tile2pos(TilePosition(118, 101))+ Position(16,16));
		marinePos.push_back(tile2pos(TilePosition(118, 102)));//(119, 102)));
		marinePos.push_back(tile2pos(TilePosition(121, 99)));
		marinePos.push_back(tile2pos(TilePosition(114, 101)));
		break;
	case 2:
		marinePos.push_back(tile2pos(TilePosition(7, 26)) + Position(16,16));
		marinePos.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
		marinePos.push_back(tile2pos(TilePosition(11, 26)));
		marinePos.push_back(tile2pos(TilePosition(14, 27)) + Position(0,16));
		marinePos.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 7));
		break;
	case 3:
		marinePos.push_back(tile2pos(TilePosition(23, 117)) + Position(16,16));
		marinePos.push_back(tile2pos(TilePosition(24, 121)));
		marinePos.push_back(tile2pos(TilePosition(27, 123)));
		marinePos.push_back(tile2pos(TilePosition(28, 123)));
		marinePos.push_back(tile2pos(TilePosition(31, 121)) + Position(16,16));

		break;
	}
}

Normal::~Normal(){}

void Normal::run(){
	//마린 불러오기
	Marine* t_marine = getIdleMarine();
	if (t_marine != nullptr && marine.size()<=5){
		marine.push_back(t_marine);
	}

	//마린이 고정할 위치를 지정
	if (marine.size() > 0){
		for (int i = 0; i < marine.size(); i++){
			if (i < 5){
				if (marine[i]->getSelf()->exists()){
					HoldMarine[marine[i]] = marinePos[i];
					marine[i]->setState(Marine::STATE::HOLD);
					MarineControl(marine[i]);
				}
			}
		}
	}
	/*if (build == BUILD_FLAG::SIM_CITY_1 && combat == COMBAT_FLAG::NORMAL){
		for (vector<Marine*>::iterator iter = marine.begin(); iter != marine.end();){
			Marine* temp = (*iter);
			if (temp->getSelf()->exists()){
				temp->orderHold();
				MarineControl(temp);
				iter++;
			}
			else{
				iter = marine.erase(iter);
			}
		}
	}*/
}

Marine* Normal::getIdleMarine(){
	for (auto marine : me->getUnits(UnitTypes::Terran_Marine)){
		auto m = dynamic_cast<Marine *>(marine);
		if (m->getState() == Marine::STATE::IDLE){
			if (m->getSelf()->exists()){
				m->orderWait();
				return m;
			}
		}
	}
	return nullptr;
}
void Normal::HoldMarinePosition(){
//	if (HoldMarine[] != nullptr){

//	}
}

void Normal::MarineControl(Marine* marine){
	if (!marine->getSelf()->exists())
	{
		return;
	}
	Unit t_marine = marine->getSelf();

	if (marine->getState() == Marine::STATE::HOLD){
		if (isAlmostReachedDest(t_marine->getPosition(), HoldMarine[marine], 5, 5)){
			t_marine->holdPosition();
			
		}
		else{
			t_marine->move(HoldMarine[marine]);
		}
		
	/*	for (auto iter = HoldMarine.begin(); iter != HoldMarine.end();){
			if (t_marine->getDistance(tile2pos(HoldMarine[marine])) > 0){
				t_marine->move(tile2pos(HoldMarine[marine]));
			}
			else if (t_marine->getDistance(tile2pos(HoldMarine[marine])) == 0){
				t_marine->stop();
			}
		}*/
	}
}
