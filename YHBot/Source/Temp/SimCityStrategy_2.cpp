#include "SimCityStrategy_2.h"
#include "../Single.h"
#include "Scout.h"
#include "UtilMgr.h"

SimStrategy_2::SimStrategy_2(){

	frameCount = 0;
	tankPosition.clear();

	if (mapInfo->getDir() == 0) {
		tankPosition.push_back(tile2pos(TilePosition(106, 15)));
		tankPosition.push_back(tile2pos(TilePosition(101, 5)));
		tankPosition.push_back(tile2pos(TilePosition(102, 19)));
		tankPosition.push_back(tile2pos(TilePosition(92, 11)));
		tankPosition.push_back(tile2pos(TilePosition(115, 10)));
	}
	else if (mapInfo->getDir() == 1){
		tankPosition.push_back(tile2pos(TilePosition(109, 101)));
		tankPosition.push_back(tile2pos(TilePosition(119, 103)));
		tankPosition.push_back(tile2pos(TilePosition(105, 99)));
		tankPosition.push_back(tile2pos(TilePosition(113, 93)));
		tankPosition.push_back(tile2pos(TilePosition(115, 117)));
	}
	else if (mapInfo->getDir() == 2) {
		tankPosition.push_back(tile2pos(TilePosition(15, 26)));
		tankPosition.push_back(tile2pos(TilePosition(11, 25)));
		tankPosition.push_back(tile2pos(TilePosition(21, 30)));
		tankPosition.push_back(tile2pos(TilePosition(15, 36)));
		tankPosition.push_back(tile2pos(TilePosition(9, 10)));
	}
	else if (mapInfo->getDir() == 3) {
		tankPosition.push_back(tile2pos(TilePosition(20, 115)));
		tankPosition.push_back(tile2pos(TilePosition(22, 119)));
		tankPosition.push_back(tile2pos(TilePosition(23, 112)));
		tankPosition.push_back(tile2pos(TilePosition(33, 114)));
		tankPosition.push_back(tile2pos(TilePosition(9, 120)));
	}

}

SimStrategy_2::~SimStrategy_2(){

}

void SimStrategy_2::run(COMBAT_FLAG combat){

	if (combat == COMBAT_FLAG::NORMAL){
		//printf("NORMAL\n");
		// 탱크 배치.
		normal();		
	}
	else if (combat == COMBAT_FLAG::ZEALOT_RUSH){
		//printf("ZEALOT\n");
		// 컴퓨터가 주로 오는 전략.
		// 앞마당에 있는 SCV들을 동원 및 탱크와 벌쳐를 이용해서 싸움.
		// 탱크는 적 유닛이 다가올 경우 퉁퉁포로 변경.

		guardZealotRush();		
	}
	else if (combat == COMBAT_FLAG::DRAGOON_RUSH){
		//printf("DRAGOON\n");
		// 엔지니어링 베이로 적 드라군과 질럿 등 유닛 수 확인후 확장. ( 3마리 이하 )
		// 앞마당전 - NORMAL때 박아둔 탱크가 해결.
		// 앞마당후 - 들어오면 미친놈. ( NORMAL 탱크가 해결 )
		// 탱크가 가장 아플 수 있는 애를 때림.
		guardDragoonRush();
	}
	else if (combat == COMBAT_FLAG::ATTACK){
		//printf("ATTACK\n");
		attack();
	}
}

void SimStrategy_2::normal(){

	auto& state = cmd->getStateMgr();

	for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
		if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
			if (tIndex < tankPosition.size()) {
				(*q)->rPos = tankPosition[tIndex++];
			}
			else {
				(*q)->rPos = tankPosition[0];
			}
		}
	}

	if (BWAPI::Broodwar->getFrameCount() % 24 == 0) {
		float range = 32;
		if (UnitControl::getInstance()->tanks.size() > 5) range = 40;
		else range = 32;
		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
			if (dist < range) {
				if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
					if ((*q)->getSelf()->isHoldingPosition() == false)
						(*q)->getSelf()->holdPosition();
					if ((*q)->getSelf()->isSieged() == false) {
						if ((*q)->getSelf()->canSiege()) {
							(*q)->getSelf()->siege();
						}
					}
				}
			}
			else {
				(*q)->getSelf()->move((*q)->rPos, false);
			}
		}
	}
	/*if (state.getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
		
	}
	else if (state.getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2)){
		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
				if (tIndex < tankPosition.size()) {
					(*q)->rPos = tankPosition[tIndex++];
				}
				else {
					(*q)->rPos = tankPosition[0];
				}
			}
		}

		if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
			float range = 4;
			if (UnitControl::getInstance()->tanks.size() > 5) range = 50;
			else range = 4;
			for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
				(*q)->getSelf()->move((*q)->rPos, false);
				float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
				if (dist < range) {
					if ((*q)->getSelf()->isHoldingPosition() == false)
						(*q)->getSelf()->holdPosition();
				}
				if ((*q)->getSelf()->isSieged() == false) {
					if ((*q)->getSelf()->canSiege()) {
						(*q)->getSelf()->siege();
					}
				}
			}
		}
	}
	else if (state.getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_3)){
		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
				if (tIndex < tankPosition.size()) {
					(*q)->rPos = tankPosition[tIndex++];
				}
				else {
					(*q)->rPos = tankPosition[0];
				}
			}
		}

		if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
			float range = 4;
			if (UnitControl::getInstance()->tanks.size() > 5) range = 50;
			else range = 4;
			for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
				(*q)->getSelf()->move((*q)->rPos, false);
				float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
				if (dist < range) {
					if ((*q)->getSelf()->isHoldingPosition() == false)
						(*q)->getSelf()->holdPosition();
				}
				if ((*q)->getSelf()->isSieged() == false) {
					if ((*q)->getSelf()->canSiege()) {
						(*q)->getSelf()->siege();
					}
				}
			}
		}
	}*/
}

void SimStrategy_2::guardZealotRush(){

}

void SimStrategy_2::guardDragoonRush(){

}

void SimStrategy_2::attack(){

}

void SimStrategy_2::end(){
}

void SimStrategy_2::draw(COMBAT_FLAG combat) {
	//printf("haha\n");
	for (int x = 0; x < 5; x++) {
		if(x==0)
			BWAPI::Broodwar->drawCircleMap(tankPosition[x], 8, Colors::Red, false);
		else if (x == 1)
			BWAPI::Broodwar->drawCircleMap(tankPosition[x], 8, Colors::Orange, false);
		else if (x == 2)
			BWAPI::Broodwar->drawCircleMap(tankPosition[x], 8, Colors::Yellow, false);
		else if (x == 3)
			BWAPI::Broodwar->drawCircleMap(tankPosition[x], 8, Colors::Green, false);
		else if (x == 4)
			BWAPI::Broodwar->drawCircleMap(tankPosition[x], 8, Colors::Blue, false);
	
	}

	for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
		BWAPI::Broodwar->drawLineMap((*q)->getSelf()->getPosition(), (*q)->rPos, Colors::Brown);
	}
	if (combat == COMBAT_FLAG::NORMAL) {
		Position p1, p2;
		
		// 특정 위치에서 대기.
		switch (mapInfo->getDir())
		{
		case 0:
			p1.x = tile2pos(TilePosition(99, 6)).x;
			p1.y = tile2pos(TilePosition(99, 6)).y;
			p2.x = tile2pos(TilePosition(99 + 7, 6 + 7)).x;
			p2.y = tile2pos(TilePosition(99 + 7, 6 + 7)).y;
			bw->drawBoxMap(p1,p2,Colors::Orange,false);
			break;

		case 1:
			p1.x = tile2pos(TilePosition(112, 97)).x;
			p1.y = tile2pos(TilePosition(112, 97)).y;
			p2.x = tile2pos(TilePosition(112 + 7, 97 + 7)).x;
			p2.y = tile2pos(TilePosition(112 + 7, 97 + 7)).y;
			bw->drawBoxMap(p1, p2, Colors::Orange, false);
			break;

		case 2:
			p1.x = tile2pos(TilePosition(6, 24)).x;
			p1.y = tile2pos(TilePosition(6, 24)).y;
			p2.x = tile2pos(TilePosition(6 + 7, 24 + 7)).x;
			p2.y = tile2pos(TilePosition(6 + 7, 24 + 7)).y;
			bw->drawBoxMap(p1, p2, Colors::Orange, false);
			break;

		case 3:
			p1.x = tile2pos(TilePosition(22, 116)).x;
			p1.y = tile2pos(TilePosition(22, 116)).y;
			p2.x = tile2pos(TilePosition(22 + 7, 116 + 7)).x;
			p2.y = tile2pos(TilePosition(22 + 7, 116 + 7)).y;
			bw->drawBoxMap(p1, p2, Colors::Orange, false);
			break;

		}

	}
	else if (combat == COMBAT_FLAG::ZEALOT_RUSH) {

	}
	else if (combat == COMBAT_FLAG::INVASION_PYLON) {

		// 기존 코드를 이용.
	}
	else if (combat == COMBAT_FLAG::ENEMY_SEARCH) {

		// 정찰 왔을 경우에 대한 대처. - 1마리당 1SCV
	}
}

