#pragma once
#include <BWAPI.h>
#include "MyUnit.h"

using namespace BWAPI;

class MedicControl {

private:
	bool flag_Physical = false;
	Unitset bionicUnits;
public:

	MedicControl();
	~MedicControl();
	static MedicControl* create();
	static MedicControl* getInstance();
	static MedicControl* s_MedicControl;
	void run(Medic*);
	void onFrame();

	//control func
	void WAIT_Control(Medic* unit);
	void DEFENCE_Control(Medic* unit);
	void RETURN_Control(Medic* unit);
	void SIMSITY_Control(Medic* unit);
	void CRAZY_Control(Medic* unit);
	void HEAL_Control(Medic* unit);

	Unit getHealUnit(Medic* unit);
	float getHealUnitValue(UnitType ut);
	Unit getUnitforID(int id);
	Position getNearestBionicUnitPos(Medic* unit);
	Unit getNearestBionicUnit(Medic* unit);

	//other func
	void goodLuck(Medic *unit);
	void supportingMove(Medic* unit);
	void drawMedicControlUnit(Medic* unit);
	void drawMedicControl();

	Position attackPosition;
	bool twoFactoryScene = true;
};