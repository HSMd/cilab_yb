#include "Bacanic.h"
#include "../Single.h"
#include "UtilMgr.h"

Bacanic::Bacanic(){

	this->tMgr = &cmd->getTerritoryMgr();
	out = nullptr;
	moveBrkPos = *AUTO_POS;
	frameCount = 0;
		
	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 1, UnitTypes::Terran_SCV, 10, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Academy, 1, UnitTypes::Terran_Factory, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 3, UnitTypes::Terran_Academy, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Medic, 2, UnitTypes::Terran_Academy, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, *AUTO_POS, UnitTypes::Terran_Marine, 3, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Siege_Tank_Tank_Mode, 2, UnitTypes::Terran_Factory, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Comsat_Station, 1, UnitTypes::Terran_Medic, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Barracks, *AUTO_POS, UnitTypes::Terran_Siege_Tank_Tank_Mode, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, *AUTO_POS, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Barracks, *AUTO_POS, UnitTypes::Terran_Barracks, 2));
	build.push_back(scheduleWork(UnitTypes::Terran_Barracks, *AUTO_POS, UnitTypes::Terran_Barracks, 3));
	build.push_back(scheduleWork(UnitTypes::Terran_Barracks, *AUTO_POS, UnitTypes::Terran_Barracks, 4));
}

Bacanic::~Bacanic(){

}

void Bacanic::run(COMBAT_FLAG combat){

	//UnitTypes::Terran_Comsat_Station
	if (moveBrkPos == *AUTO_POS){
		moveBrkPos = pMgr->getFtryPos(true);
	}

	auto startTerritory = tMgr->getAllTerritorys()[0];
	
	// SCV 스케쥴러에 추가합니다.
	if (cmd->getUnitScheduleCount(UnitTypes::Terran_SCV) < tMgr->getAllTerritorys().size()){
		for (auto territory : tMgr->getAllTerritorys()){
			int require = territory->getRequireSCVCount();

			if (0 < require && cmd->getReadyTrain(territory->getCmdCenter()->getSelf())){
				cmd->addScvCount();
				if (flags[ATTACK_ALL]){
					cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_SCV, territory->getCmdCenter()->getSelf()), 1, 0);
				}
				else{
					cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_SCV, territory->getCmdCenter()->getSelf()), 1, 1);
				}
			}
		}
	}

	// 스팀팩
	if (!flags[UPGRADE_STIM] && 0 < me->getBuildings(UnitTypes::Terran_Academy).size()){
		if (me->getBuildings(UnitTypes::Terran_Academy)[0]->getSelf()->research(TechTypes::Stim_Packs))
			flags[UPGRADE_STIM] = true;
	}

	// 시즈모드 업글
	if (!flags[UPGRADE_SIEGE_MODE] && 1 <= me->getImmediatelyUnit(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()){
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[0]->getSelf()->research(TechTypes::Tank_Siege_Mode)){
			flags[UPGRADE_SIEGE_MODE] = true;
		}
	}

	// 진출타이밍
	if (!flags[ATTACK_ALL] && 1 <= me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()){
		auto b = me->getBuildings(UnitTypes::Terran_Barracks)[0];
		auto brk = dynamic_cast<Structure*>(b);

		if (me->getBuildings(UnitTypes::Terran_Barracks)[0]->getSelf()->lift()){

			//auto pos = me->getBuildings(UnitTypes::Terran_Barracks)[0]->getSelf()->getTilePosition() + TilePosition(4, 0);
			brk->orderMove(moveBrkPos);
			moveBuilding.push_back(brk);

			cmd->getStateMgr().setCombatFlag(COMBAT_FLAG::ATTACK, true);
			flags[ATTACK_ALL] = true;
		}
	}

	// 탱크 계속 생산
	if (flags[ATTACK_ALL] && cmd->getUnitScheduleCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) < 1){
		cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_Siege_Tank_Tank_Mode), 1);
	}

	// 마린 계속 생산
	if (flags[ATTACK_ALL] && cmd->getUnitScheduleCount(UnitTypes::Terran_Marine) < 1){
		cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_Marine), 1);
	}	

	// 서플라이 자동 생산
	if (flags[ATTACK_ALL] && frameCount + 800 < bw->getFrameCount() &&
		cmd->getSupply() < 6 * 2){

		frameCount = bw->getFrameCount();
		cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
	}

	// 예약된 BUILD 들이 점화되면 수행된다.
	for (auto& work : build){
		auto targetType = work.getTargetType();

		if (!work.getFire() && work.canFire()){
			if (targetType.isBuilding()){

				if (targetType == UnitTypes::Terran_Barracks) {
					work.setTilePosition(pMgr->getFtryPos(true));
				}

				cmd->addStructureSchedule(work, 1, 0);
				work.onFire();
			}
			else{

				cmd->addUnitSchedule(targetType, work.getCount(), 0);
				work.onFire();
			}
		}
	}	

	move();

	//errorSCV();
}

void Bacanic::move(){

	for (auto building : moveBuilding){
		if (building->getState() == Structure::STATE::MOVE){
			if (getDistance(building->getSelf()->getTilePosition(), building->getTargetPos()) < 3){
				building->orderLand(building->getTargetPos());
			}
			else{
				building->getSelf()->move(tile2pos(building->getTargetPos()));
			}
		}
		else if (building->getState() == Structure::STATE::LAND){
			
			if (building->getSelf()->isIdle()){
				building->orderLand(building->getTargetPos());					
			}
			else{
				if (!building->getSelf()->isLifted()){
					building->orderIdle();
				}
			}
		}
	}
}

void Bacanic::errorSCV(){

	auto startTerritory = tMgr->getAllTerritorys()[0];

	// 입구가 막혔을 경우 쫒겨난 유닛에 대한 대쳐.
	if (out == nullptr){
		for (auto scv : startTerritory->getWorkSCV()){
			if (scv->getState() != SCV::STATE::RETURN){
				continue;
			}

			int dis = getDistance(scv->getSelf()->getTilePosition(), mapInfo->getNearBaseLocation()->getTilePosition());
			if (dis < 9){
				out = scv;
			}
		}
	}
	else{
		auto b = me->getBuildings(UnitTypes::Terran_Barracks)[0];
		if (!flags[ENTRANCE_OPEN]){
			//bw << "OPEN" << endl;
			outPos = b->getSelf()->getTilePosition();
			if (b->getSelf()->isIdle() && b->getSelf()->isCompleted())
				if (b->getSelf()->lift())
					flags[ENTRANCE_OPEN] = true;
		}
		else{
			if (!flags[ENTRANCE_CLOSE] && getDistance(out->getSelf()->getTilePosition(), startTerritory->getCmdCenter()->getSelf()->getTilePosition()) < 17){
				//bw << "CLOSE" << endl;
				if (b->getSelf()->land(outPos))
					flags[ENTRANCE_CLOSE] = true;
			}
		}
	}
}

bool Bacanic::getIsEnd(){

	return false;
}