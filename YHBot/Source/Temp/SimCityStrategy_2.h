#pragma once
#include "Work.h"
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"

class SimStrategy_2
{
	// 한번만 실행되어야 될 것들.
	enum FLAG{
	};

	map<int, bool> flags;
	int frameCount;
	vector<Position> tankPosition;
	// 자료구조.
	int tIndex = 0;

public:
	SimStrategy_2();
	~SimStrategy_2();

	void run(COMBAT_FLAG combat);
	void end();
	void draw(COMBAT_FLAG combat);
	void show();

	void normal();
	void guardZealotRush();
	void guardDragoonRush();
	void attack();
};

