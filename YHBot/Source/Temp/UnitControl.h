#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <list>

#include "VultureControl.h"
#include "TankControl.h"
#include "MedicControl.h"
#include "MarineControl.h"
#include "GoliathControl.h"
#include "SCVControl.h"

using namespace std;
using namespace BWAPI;
using namespace Filter;

enum STATE {
	START,
	
	TF_WAIT,	// 대기
	TF_MOVE,	
	TF_ATTACK,	// 진출, 앞마당
	TF_FINISH,	// 끝내기

	TC_DEFENSE,
	TC_ATTACK,

	KJ_WAIT,
	KJ_ATTACK,
};

class UnitControl
{
	//startStrategy* test2;

	UnitControl();
	~UnitControl();
	STATE state;
	COMBAT_FLAG combat_flag;
	static UnitControl *s_UnitControl;
	
public:

	static UnitControl* getInstance();
	static UnitControl* create();

	VultureControl*		m_VultureControl;
	TankControl*		m_TankControl;
	MedicControl*		m_MedicControl;
	HMarineControl*		m_HMarineControl;
	//MarineControl*		m_MarineControl;
	GoliathControl*		m_GoliathControl;
	SCVControl*			m_SCVControl;
	void setControlState(STATE s) { state = s; }
	STATE getControlState() { return state; }

	void init();
	void run();
	void onUnitDestroy(Unit);

	//Control Func
	void allUnitControl();
	void setVultureMine(VultureUnit*);
	void setUnitAttackPos();

	//Set STATE Func
	void setUnitSTATE();
	void setVultureSTATE();
	void setTankSTATE();
	void setMedicSTATE();
	void setMarineSTATE();
	void setGoliathSTATE();
	bool stateFlag = false;

	//REAL Control
	void Control_Vulture(VultureUnit *u);
	void Control_Tank(Tank *u);
	void Control_Medic(Medic* u);
	void Control_Marine(HMarine* u);
	void Control_Goliath(Goliath* u);
	void Control_SCV(SCV* u);

	//Unit Vector
	list<VultureUnit*> vultures;
	list<Tank*> tanks;
	list<Medic*> medics;
	list<HMarine*> marines;
	list<Goliath*> goliaths;
	list<SCV*> scvs;
	void pushUnitList();
	list<Position> frontMineVec;

	//Mine Func
	Position getMultiMine(VultureUnit*);
	Position getFrontMine(VultureUnit*);
	Position getRecallMine(VultureUnit*);
	Position getKJMine(VultureUnit*);
	Position kjMinePos[4];
	int kjIndex = 0;
	bool kjFrontDone = false;

	//draw Func
	void drawUnitControl();

	//Etc Func
	bool isAlreadyPush(int id, UnitType ut);

	bool TF_MINE = false;
	Position enemyPos;
	int enemyRange;

	Position attackPos;

	Position TF_MinePos[8];
	Position TF_MinePosition;

	char* getString() {
		char * ret;
		if (state == STATE::TC_ATTACK) {
			ret = "TC_ATTACK";
		}
		else if (state == STATE::TC_DEFENSE) {
			ret = "TC_DEFENSE";
		}
		else if (state == STATE::TF_WAIT) {
			ret = "TF_WAIT";
		}
		else if (state == STATE::TF_MOVE) {
			ret = "TF_MOVE";
		}
		else if (state == STATE::TF_ATTACK) {
			ret = "TF_ATTACK";
		}
		else if (state == STATE::TF_FINISH) {
			ret = "TF_ATTACK";
		}
		else if (state == STATE::START) {
			ret = "START";
		}
		else {
			ret = "NONE";
		}
		return ret;
	}

	int getNearUnitCount(VultureUnit* u);

	Unitset getNearEnemyUnits();
	Unit getAttackUnit(Unitset, MyUnit*);
	float getUnitValue(UnitType, UnitType);
};

