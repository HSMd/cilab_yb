#include "UnitControl.h"
#include "UtilMgr.h"
#include "StateMgr.h"
#include "../Single.h"

UnitControl* UnitControl::s_UnitControl;
UnitControl::UnitControl() {
	init();
}

UnitControl::~UnitControl() {

}

UnitControl* UnitControl::create() {
	if (s_UnitControl) {
		return s_UnitControl;
	}
	s_UnitControl = new UnitControl();
	return s_UnitControl;
}

UnitControl* UnitControl::getInstance() {
	return s_UnitControl;
}

void UnitControl::init() {
	m_VultureControl = VultureControl::create();
	m_TankControl = TankControl::create();
	m_MedicControl = MedicControl::create();
	m_HMarineControl = HMarineControl::create();
	//m_MarineControl = HMarineControl::create();
	m_GoliathControl = GoliathControl::create();

	frontMineVec.clear();
	vultures.clear();
	tanks.clear();
	medics.clear();
	marines.clear();
	goliaths.clear();

	state = STATE::START;
}

void UnitControl::run() {
	pushUnitList();
	//setUnitAttackPos();

	VultureControl::getInstance()->onFrame();
	TankControl::getInstance()->onFrame();
	MedicControl::getInstance()->onFrame();
	//HMarineControl::getInstance()->onFrame();
	HMarineControl::getInstance()->onFrame();
	GoliathControl::getInstance()->onFrame();

	//setUnitSTATE();		// 전체 유닛 state 설정
	//allUnitControl();	// 전체 유닛 컨트롤
}

void UnitControl::drawUnitControl() {

	BWAPI::Broodwar->drawTextScreen(Position(480, 300), getString());

	VultureControl::getInstance()->drawVultrueControl();
	TankControl::getInstance()->drawTankControl();
	MedicControl::getInstance()->drawMedicControl();
	//HMarineControl::getInstance()->drawHMarineControl();

	if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
		for (int x = 0; x < 4; x++) {
			BWAPI::Broodwar->drawCircleMap(kjMinePos[x], 6, Colors::Red, true);
		}
	}
	
	
	for (auto v = vultures.begin(); v != vultures.end(); v++) {
		if ((*v)->getSelf()->exists() && (*v) != NULL)
			VultureControl::getInstance()->drawVultrueControlUnit((*v));
	}
	for (auto t = tanks.begin(); t != tanks.end(); t++) {
		if((*t)->getSelf()->exists() && (*t) != NULL)
			TankControl::getInstance()->drawTankControlUnit((*t));
	}
	for (auto m = medics.begin(); m != medics.end(); m++) {
		MedicControl::getInstance()->drawMedicControlUnit((*m));
	}
	for (auto m = marines.begin(); m != marines.end(); m++) {
		HMarineControl::getInstance()->drawHMarineControlUnit((*m));
	}
	for (auto g = goliaths.begin(); g != goliaths.end(); g++) {
		GoliathControl::getInstance()->drawGoliathControlUnit((*g));
	}

	for (auto q = frontMineVec.begin(); q != frontMineVec.end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q), 8, Colors::Red, false);
		BWAPI::Broodwar->drawCircleMap((*q), 6, Colors::Red, false);
	}
}

void UnitControl::allUnitControl() {
	
	for (auto v = vultures.begin(); v != vultures.end(); v++) {
		Control_Vulture((*v));
	}
	for (auto t = tanks.begin(); t != tanks.end(); t++) {
		Control_Tank((*t));
	}
	for (auto t = medics.begin(); t != medics.end(); t++) {
		Control_Medic((*t));
	}
	for (auto t = marines.begin(); t != marines.end(); t++) {
		Control_Marine((*t));
	}
	for (auto g = goliaths.begin(); g != goliaths.end(); g++) {
		Control_Goliath((*g));
	}

}

void UnitControl::Control_Vulture(VultureUnit* unit) {
	VultureControl::getInstance()->run(unit, false);
}

void UnitControl::Control_Tank(Tank* unit) {
	TankControl::getInstance()->run(unit);
}

void UnitControl::Control_Goliath(Goliath* unit) {
	GoliathControl::getInstance()->run(unit);
}

void UnitControl::Control_Medic(Medic* unit) {
	MedicControl::getInstance()->run(unit);
}
//void UnitControl::Control_Marine(HMarine* unit) {
	//HMarineControl::getInstance()->run(unit);

//}
void UnitControl::Control_Marine(HMarine* unit) {
	HMarineControl::getInstance()->run(unit);

}

bool UnitControl::isAlreadyPush(int id, UnitType ut) {
	if (ut == UnitTypes::Terran_Vulture) {
		for (auto q = vultures.begin(); q != vultures.end(); q++) {
			if ((*q)->getSelf()->getID() == id) {
				return true;
			}
		}
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode || ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		for (auto q = tanks.begin(); q != tanks.end(); q++) {
			if ((*q)->getSelf()->getID() == id) {
				return true;
			}
		}
	}
	else if (ut == UnitTypes::Terran_Medic) {
		for (auto q = medics.begin(); q != medics.end(); q++) {
			if ((*q)->getSelf()->getID() == id) {
				return true;
			}
		}
	}
	else if (ut == UnitTypes::Terran_Marine) {
		for (auto q = marines.begin(); q != marines.end(); q++) {
			if ((*q)->getSelf()->getID() == id) {
				return true;
			}
		}
	}
	else if (ut == UnitTypes::Terran_Goliath) {
		for (auto q = goliaths.begin(); q != goliaths.end(); q++) {
			if ((*q)->getSelf()->getID() == id) {
				return true;
			}
		}
	}
	
	return false;
}

void UnitControl::onUnitDestroy(Unit unit) {

	if (unit->getType() == UnitTypes::Terran_Vulture) {
		auto iter = vultures.begin();
		auto iter2 = vultures.begin();
		for (iter; iter != vultures.end(); iter = iter2) {
			iter2++;
			if((*iter)->getSelf()->getID() == unit->getID()){
				vultures.erase(iter);
			}
		}
	}
	else if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		auto iter = tanks.begin();
		auto iter2 = tanks.begin();
		for (iter; iter != tanks.end(); iter = iter2) {
			iter2++;
			if ((*iter)->getSelf()->getID() == unit->getID()) {
				tanks.erase(iter);
			}
		}
	}
	else if (unit->getType() == UnitTypes::Terran_Medic) {
		auto iter = medics.begin();
		auto iter2 = medics.begin();
		for (iter; iter != medics.end(); iter = iter2) {
			iter2++;
			if ((*iter)->getSelf()->getID() == unit->getID()) {
				medics.erase(iter);
			}
		}
	}
	else if(unit->getType() == UnitTypes::Terran_Marine) {
		auto iter = marines.begin();
		auto iter2 = marines.begin();
		for (iter; iter != marines.end(); iter = iter2) {
			iter2++;
			if ((*iter)->getSelf()->getID() == unit->getID()) {
				marines.erase(iter);
			}
		}
	}
	else if (unit->getType() == UnitTypes::Terran_Goliath) {
		auto iter = goliaths.begin();
		auto iter2 = goliaths.begin();
		for (iter; iter != goliaths.end(); iter = iter2) {
			iter2++;
			if ((*iter)->getSelf()->getID() == unit->getID()) {
				goliaths.erase(iter);
			}
		}
	}
	else if (unit->getType() == UnitTypes::Terran_SCV) {
		auto iter = scvs.begin();
		auto iter2 = scvs.begin();
		for (iter; iter != scvs.end(); iter = iter2) {
			iter2++;
			if ((*iter)->getSelf()->getID() == unit->getID()) {
				scvs.erase(iter);
			}
		}
	}
}

void UnitControl::setUnitSTATE() {
	setVultureSTATE();
	setTankSTATE();
	setMedicSTATE();
	setMarineSTATE();
	setGoliathSTATE();
}

void UnitControl::setVultureSTATE() {						// 벌쳐들의 state를 정의하시오 (4점)
	if (state == STATE::TC_DEFENSE) {
		int squUnitCount = 0;
		int defUnitCount = 0;
		int scoUnitCount = 0;

		for (auto q = vultures.begin(); q != vultures.end(); q++) {
			(*q)->attackTarget = VultureControl::getInstance()->getTargetUnit((*q), false);
			if ((*q)->getState() == VultureUnit::STATE::NM_SCOUT) scoUnitCount++;
			if ((*q)->getState() == VultureUnit::STATE::NM_DEFENSE) defUnitCount++;
			if ((*q)->getState() == VultureUnit::STATE::NM_CRAZY) {
				if ((*q)->attackTarget == NULL)
					(*q)->setState(VultureUnit::STATE::IDLE);
				else if ((*q)->attackTarget->getType() == UnitTypes::Protoss_Dragoon) {
					//(*q)->setState(VultureUnit::STATE::NM_RETURNTOBASE);
				}
			}
		}

		for (auto q = vultures.begin(); q != vultures.end(); q++) {
			if ((*q)->getSelf()->isCompleted() == false) continue;
			// 근처에 적군이 있으면 크레이지 모드
			if ((*q)->getState() == VultureUnit::STATE::IDLE) {
				if ((*q)->attackTarget != NULL) {
					(*q)->setState(VultureUnit::STATE::NM_CRAZY);
				}
				// 근처에 적군이 없으면 
				else {
					// 마인이 있으면 마인 심으러 보냄
					if ((*q)->getSelf()->getSpiderMineCount() > 0) {
						setVultureMine((*q));
					}
					// 마인이 없으면
					else {
						// 정찰기가 4개 미만이면 정찰보냄
						if (scoUnitCount < 4) {
							(*q)->setState(VultureUnit::STATE::NM_SCOUT);
						}
						// 정찰기가 4개 이상이면 안돼 돌아가
						else {
							(*q)->setState(VultureUnit::STATE::NM_RETURNTOBASE);
						}
					}
				}
			}
		}
	}
	if (state == STATE::TC_ATTACK) {
		for (auto q = vultures.begin(); q != vultures.end(); q++) {
			(*q)->attackTarget = VultureControl::getInstance()->getTargetUnit((*q), false);
			(*q)->setState(VultureUnit::STATE::NM_CRAZY);
		}
	}
	if (state == STATE::TF_WAIT) {
		for (auto q = vultures.begin(); q != vultures.end(); q++) {
			(*q)->attackTarget = VultureControl::getInstance()->getTargetUnit((*q), false);
			(*q)->setState(VultureUnit::STATE::KJ_WAIT);
		}
	}
	if (state == STATE::TF_ATTACK) {
		for (auto q = vultures.begin(); q != vultures.end(); q++) {
			if ((*q)->getSelf()->isCompleted() == false) continue;

			// 마인이 있으면
			if ((*q)->getSelf()->getSpiderMineCount() > 0) {
				Position p = getKJMine((*q));
				if (p.x < 0) {		// 박을 곳에 다 박음
					(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
				}
				else {													//멀티에 아직 박을데가 있음
					if (getNearUnitCount((*q)) < 3) {
						((*q))->setState(VultureUnit::STATE::KJ_MINE);
						((*q))->minePos = p;
					}
					else {
						(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
					}
				}
			}
			// 마인이 없으면
			else {
				(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
			}
		}
	}
	if (state == STATE::KJ_WAIT) {
		for (auto q = vultures.begin(); q != vultures.end(); q++) {
			(*q)->attackTarget = VultureControl::getInstance()->getTargetUnit((*q), false);
			(*q)->setState(VultureUnit::STATE::KJ_WAIT);
		}
	}
	else if (state == STATE::KJ_ATTACK) {
		for (auto q = vultures.begin(); q != vultures.end(); q++) {
			if ((*q)->getSelf()->isCompleted() == false) continue;

			// 마인이 있으면
			if ((*q)->getSelf()->getSpiderMineCount() > 0) {
				Position p = getKJMine((*q));
				if (p.x < 0) {		// 박을 곳에 다 박음
					(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
				}
				else {													//멀티에 아직 박을데가 있음
					if (getNearUnitCount((*q)) < 3) {
						((*q))->setState(VultureUnit::STATE::KJ_MINE);
						((*q))->minePos = p;
					}
					else {
						(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
					}
				}
			}
			// 마인이 없으면
			else {
				(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
			}
		}
	}
}
void UnitControl::setTankSTATE() {						// 탱크들의 state를 정의하시오 (3점)
	if (state == STATE::START) {
		for (auto q = tanks.begin(); q != tanks.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Tank::STATE::WAIT);
			}
		}
	}
	else if (state == STATE::TF_WAIT) {
		for (auto q = tanks.begin(); q != tanks.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Tank::STATE::WAIT);
			}
		}
	}
	else if (state == STATE::TF_ATTACK) {		//진출하고, 앞마당 먹고, 탱크 두대가 올때까지
		for (auto q = tanks.begin(); q != tanks.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Tank::STATE::CRAZY);
			}
		}
	}
	else if(state == STATE::TF_FINISH){			//본진 떄리기

	}
	else if (state == STATE::TC_DEFENSE) {
		for (auto q = tanks.begin(); q != tanks.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				/*if (!(*q)->defenceUnit || (*q)->isMove) {
					(*q)->setState(Tank::STATE::TC_DEFENCE);
				}
				if ((*q)->DDeokUnit) {
					(*q)->setState(Tank::STATE::CRAZY);
					//printf("Crazy DDeok Unit\n");
				}
				else if ((*q)->getState() == Tank::STATE::DEFENCE) {
					//printf("DEFENCE\n");
				}
				else if ((*q)->isMove) {
					//printf("무빙무빙\n");
				}
				else {
					//printf("ERROR\n");
				}*/
				if (cmd->getStateMgr().getCombatFlag(GUARD_SIM_CITY_1)) {
					combat_flag = GUARD_SIM_CITY_1;
					if (!(*q)->defenceUnit || (*q)->isMove) {
						(*q)->setState(Tank::STATE::TC_DEFENCE1);
					}
					if ((*q)->DDeokUnit) {
						(*q)->setState(Tank::STATE::CRAZY);
						//printf("Crazy DDeok Unit\n");
					}
					else if ((*q)->getState() == Tank::STATE::DEFENCE) {
						//printf("DEFENCE\n");
					}
					else if ((*q)->isMove) {
						//printf("무빙무빙\n");
					}
					else {
						//printf("ERROR\n");
					}
					//printf("GUARD_SIM_CITY_1\n");
				}
				else if (cmd->getStateMgr().getCombatFlag(GUARD_SIM_CITY_2)) {
					if (combat_flag != GUARD_SIM_CITY_2){
						//초기화
						//printf("초기화\n");
						for (auto u = tanks.begin(); u != tanks.end(); u++) {
							if ((*u)->getSelf()->isCompleted()) {
								(*u)->isMove = false;
								(*u)->DDeokUnit = false;
								(*u)->isSiege = false;
								(*u)->defenceUnit = false;
								(*u)->setState(Tank::STATE::IDLE);
								(*u)->attackTarget = NULL;
								if ((*u)->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
									(*u)->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
								}						
							}
						}
					}
					//printf("GUARD_SIM_CITY_2\n");
					combat_flag = GUARD_SIM_CITY_2;
					if (!(*q)->defenceUnit || (*q)->isMove) {
						(*q)->setState(Tank::STATE::TC_DEFENCE2);
					}
					if ((*q)->DDeokUnit) {
						(*q)->setState(Tank::STATE::CRAZY);
						//printf("Crazy DDeok Unit\n");
					}
					else if ((*q)->getState() == Tank::STATE::DEFENCE) {
						//printf("DEFENCE\n");
					}
					else if ((*q)->isMove) {
						//printf("무빙무빙\n");
					}
					else {
						//printf("ERROR\n");
					}
				}
				else if (cmd->getStateMgr().getCombatFlag(GUARD_SIM_CITY_3)) {
					if (!(*q)->defenceUnit || (*q)->isMove) {
						(*q)->setState(Tank::STATE::TC_DEFENCE3);
					}
					if ((*q)->DDeokUnit) {
						(*q)->setState(Tank::STATE::CRAZY);
						//printf("Crazy DDeok Unit\n");
					}
					else if ((*q)->getState() == Tank::STATE::DEFENCE) {
						//printf("DEFENCE\n");
					}
					else if ((*q)->isMove) {
						//printf("무빙무빙\n");
					}
					else {
						//printf("ERROR\n");
					}
					printf("GUARD_SIM_CITY_3\n");
				}
				else {
					printf("ERROR\n");
				}
			}
		}
	}
	else if (state == STATE::TC_ATTACK) {
		for (auto q = tanks.begin(); q != tanks.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Tank::STATE::CRAZY);
			}
		}
	}
	if (state == STATE::KJ_WAIT) {
		for (auto q = tanks.begin(); q != tanks.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Tank::STATE::KJ_WAIT);
			}
		}
	}
	else if (state == STATE::KJ_ATTACK) {
		for (auto q = tanks.begin(); q != tanks.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Tank::STATE::KJ_ATTACK);
			}
		}
	}
} 
void UnitControl::setMedicSTATE() {						// 메딕들의 state를 정의하시오 (3점)
	if (state == STATE::START) {
		for (auto q = medics.begin(); q != medics.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Medic::STATE::WAIT);
			}
		}
	}
	else if (state == STATE::TF_WAIT) {
		for (auto q = medics.begin(); q != medics.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Medic::STATE::WAIT);
			}
		}
	}
	else if (state == STATE::TF_ATTACK) {
		for (auto q = medics.begin(); q != medics.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Medic::STATE::CRAZY);
			}
		}
	}
	else if (state == STATE::TC_ATTACK) {
		for (auto q = medics.begin(); q != medics.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Medic::STATE::CRAZY);
			}
		}
	}
	else if (state == STATE::TC_DEFENSE) {
		for (auto q = medics.begin(); q != medics.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				(*q)->setState(Medic::STATE::CRAZY);
			}
		}
	}
}
void UnitControl::setMarineSTATE() {					// 마린들의 state를 정의하시오 (3점)
	if (state == STATE::START) {
		for (auto q = marines.begin(); q != marines.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				//(*q)->setState(HMarine::STATE::WAIT);
				(*q)->setState(HMarine::STATE::WAIT);
			}
		}
	}
	else if (state == STATE::KJ_WAIT) {
		for (auto q = marines.begin(); q != marines.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				//(*q)->setState(HMarine::STATE::WAIT);
				(*q)->setState(HMarine::STATE::WAIT);
			}
		}
	}
	else if (state == STATE::KJ_ATTACK) {
		for (auto q = marines.begin(); q != marines.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				//(*q)->setState(HMarine::STATE::CRAZY);
				(*q)->setState(HMarine::STATE::CRAZY);
			}
		}
	}
	else if (state == STATE::TF_WAIT) {
		for (auto q = marines.begin(); q != marines.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				//(*q)->setState(HMarine::STATE::WAIT);
				(*q)->setState(HMarine::STATE::WAIT);
			}
		}
	}
	else if (state == STATE::TF_ATTACK) {
		for (auto q = marines.begin(); q != marines.end(); q++) {
			if ((*q)->getSelf()->isCompleted()) {
				//(*q)->setState(HMarine::STATE::CRAZY);
				(*q)->setState(HMarine::STATE::CRAZY);
			}
		}
	}
	
}

void UnitControl::setGoliathSTATE() {					// 마린들의 state를 정의하시오 (3점)

	for (auto g = goliaths.begin(); g != goliaths.end(); g++) {
		if ((*g)->getSelf()->isCompleted()) {
			(*g)->setState(Goliath::STATE::CRAZY);
		}
	}

	/*if (state == STATE::START) {
		
	}
	else if (state == STATE::TF_WAIT) {
		
	}
	else if (state == STATE::TF_ATTACK) {
		
	}
	else if (state == STATE::TC_ATTACK) {

	}
	else if (state == STATE::TC_DEFENSE) {

	}*/
}

void UnitControl::setUnitAttackPos() {
	if (!stateFlag) {
		attackPos = CombatInfo::getInstance()->attackPos;
	}
	m_VultureControl->attackPosition = attackPos;
	m_TankControl->attackPosition = attackPos;
	m_MedicControl->attackPosition = attackPos;
	m_HMarineControl->attackPosition = attackPos;
	//m_MarineControl->attackPosition = attackPos;
	m_GoliathControl->attackPosition = attackPos;
}

void UnitControl::pushUnitList() {
	for (auto q = BWAPI::Broodwar->self()->getUnits().begin(); q != BWAPI::Broodwar->self()->getUnits().end(); q++) {
		if ((*q)->isCompleted()) {
			if ((*q)->getType() == UnitTypes::Terran_Vulture) {
				if (!isAlreadyPush((*q)->getID(), (*q)->getType())) {
					VultureUnit* unit = new VultureUnit((*q));
					vultures.push_back(unit);
				}
			}
			else if ((*q)->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || (*q)->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
				if (!isAlreadyPush((*q)->getID(), (*q)->getType())) {
					Tank* unit = new Tank((*q));
					tanks.push_back(unit);
				}
			}
			else if ((*q)->getType() == UnitTypes::Terran_Medic) {
				if (!isAlreadyPush((*q)->getID(), (*q)->getType())) {
					Medic* unit = new Medic((*q));
					medics.push_back(unit);
				}
			}
			else if ((*q)->getType() == UnitTypes::Terran_Marine) {
				if (!isAlreadyPush((*q)->getID(), (*q)->getType())) {
					//HMarine* unit = new HMarine((*q));
					HMarine* unit = new HMarine((*q));
					marines.push_back(unit);
				}
			}
			else if ((*q)->getType() == UnitTypes::Terran_Goliath) {
				if (!isAlreadyPush((*q)->getID(), (*q)->getType())) {
					Goliath* unit = new Goliath((*q));
					goliaths.push_back(unit);
				}
			}
			else if ((*q)->getType() == UnitTypes::Terran_SCV) {
				if (!isAlreadyPush((*q)->getID(), (*q)->getType())) {
					SCV* unit = new SCV((*q));
					scvs.push_back(unit);
				}
			}
		}
	}
}

Position UnitControl::getFrontMine(VultureUnit* unit) {
	Position ret = Position(-1, -1);

	if (frontMineVec.size() <= 18) {
		int mv = 0;
		int n = 12;
		enemyPos = VultureControl::getInstance()->getHongMinePos();
		enemyRange = 40;
		//printf("벌쳐 %d기 출동!~~\n", mv);
		int count = 0;
		float PI = 3.141592f;
		for (count; count < n; count++) {
			int CX = enemyPos.x;
			int CY = enemyPos.y;
			int dx = (int)(50 * cos(2 * PI / 12 * count));
			int dy = (int)(50 * sin(2 * PI / 12 * count));
			Position minPos = Position(CX + dx, CY + dy);
			Position cannonPos = VultureControl::getInstance()->getNearestCannonPos(minPos);
			count++;
			float dist = getDistance(minPos, cannonPos);
			if (dist < UnitTypes::Protoss_Photon_Cannon.groundWeapon().maxRange() + 40) continue;
			if (!BWAPI::Broodwar->isWalkable((WalkPosition)minPos)) continue;
			if (BWTA::getNearestBaseLocation(minPos) == CombatInfo::getInstance()->myStartingBase) continue;

			bool check = false;
			for (auto f = frontMineVec.begin(); f != frontMineVec.end(); f++) {
				if ((*f) == minPos) {
					check = true;
					break;
				}
			}
			for (auto q = vultures.begin(); q != vultures.end(); q++) {
				if ((*q)->getState() == VultureUnit::STATE::NM_MINE_FRONT) {
					if ((*q)->minePos == minPos) {
						check = true;
						break;
					}
				}
			}
			if (!check)
				frontMineVec.push_back(minPos);
		}
	}

	auto iter = frontMineVec.begin();
	auto iter2 = frontMineVec.begin();
	for (iter; iter != frontMineVec.end(); iter = iter2) {
		iter2++;
		for (auto b = CombatInfo::getInstance()->enemyBase.begin(); b != CombatInfo::getInstance()->enemyBase.end(); b++) {
			if (BWTA::getRegion((*iter)) == BWTA::getRegion((*b).first->getPosition())) {
				frontMineVec.erase(iter);
			}

		}
	}
	
	float minDist = 9999.f;
	for (auto f = frontMineVec.begin(); f != frontMineVec.end(); f++) {
		float dist = getDistance(unit->getSelf()->getPosition(), (*f));
		if (dist < minDist) {
			minDist = dist;
			ret = (*f);
		}
	}

	iter = frontMineVec.begin();
	iter2 = frontMineVec.begin();

	for (iter; iter != frontMineVec.end(); iter = iter2) {
		iter2++;
		if ((*iter) == ret) {
			frontMineVec.erase(iter);
		}
	}

	return ret;
}
Position UnitControl::getMultiMine(VultureUnit* unit) {
	Position ret = Position(-1, -1);
	for (auto q = BWTA::getBaseLocations().begin(); q != BWTA::getBaseLocations().end(); q++) {
		if (VultureControl::getInstance()->checkMultiMine[(*q)->getPosition()] == false) {
			ret = (*q)->getPosition();
			VultureControl::getInstance()->checkMultiMine[(*q)->getPosition()] = true;
			break;
		}
	}
	if (ret.x == -1) VultureControl::getInstance()->allMulitMine = true;
	return ret;
}
Position UnitControl::getRecallMine(VultureUnit* unit) {
	
	Position ret = Position(-1, -1);

	return ret;
}
Position UnitControl::getKJMine(VultureUnit* unit) {
	Position ret = Position(-1, -1);
	if (CombatInfo::getInstance()->enemyStartingBase != NULL && CombatInfo::getInstance()->enemyFrontYard != NULL) {
		if (BWTA::getRegion(unit->getSelf()->getPosition()) == BWTA::getRegion(CombatInfo::getInstance()->enemyFrontYard->getPosition())) {
			for (int x = 0; x < 4; x++) {
				bool check = false;
				for (auto q = BWAPI::Broodwar->self()->getUnits().begin(); q != BWAPI::Broodwar->self()->getUnits().end(); q++) {
					if ((*q)->getType() == UnitTypes::Terran_Vulture_Spider_Mine) {
						if ((*q)->getPosition() == kjMinePos[x]) {
							check = true;
							break;
						}
					}
				}
				for (auto q = vultures.begin(); q != vultures.end(); q++) {
					if ((*q)->minePos == kjMinePos[x]) {
						check = true;
						break;
					}
				}
				if (!check) {
					return kjMinePos[x];
				}
			}
		}
		else if(BWTA::getRegion(unit->getSelf()->getPosition()) == BWTA::getRegion(CombatInfo::getInstance()->enemyStartingBase->getPosition())){
			float minDist = 9999;
			for (int x = 0; x < 128; x++) {
				for (int y = 0; y < 128; y++) {
					if (VultureControl::getInstance()->vultureIM[y][x] == -3) {
						float dist = getDistance(Position(x * 32, y * 32), unit->getSelf()->getPosition());
						bool check = false;
						for (auto q = BWAPI::Broodwar->self()->getUnits().begin(); q != BWAPI::Broodwar->self()->getUnits().end(); q++) {
							if ((*q)->getType() == UnitTypes::Terran_Vulture_Spider_Mine) {
								if ((*q)->getPosition() == Position(x * 32, y * 32)) {
									check = true;
									break;
								}
							}
						}
						if (dist < minDist && !check) {
							minDist = dist;
							ret = Position(x * 32, y * 32);
						}
					}
				}
			}
		}
	}

	return ret;
}

void UnitControl::setVultureMine(VultureUnit* unit) {
	if (VultureControl::getInstance()->allMulitMine == false) {
		Position p = getMultiMine(unit);
		if (VultureControl::getInstance()->allMulitMine) {		//멀티에 다 박았음
			
		}
		else {													//멀티에 아직 박을데가 있음
			(unit)->setState(VultureUnit::STATE::NM_MINE_FRONT);
			(unit)->minePos = p;
			//printf("멀티에 박아요\n");
			return;
		}
	}
	if(BWAPI::Broodwar->self()->supplyUsed() > 340) {
		Position p = getRecallMine(unit);
		if (p.x == -1 && p.y == -1) {							//본진엔 더이상 필요 없음
			return;
		}
		else {													//본진에 필요함
			(unit)->setState(VultureUnit::STATE::NM_MINE_FRONT);
			(unit)->minePos = p;
			//printf("본진에 박아요\n");
		}
	}
	else {
		Position p = getFrontMine(unit);						//맵에 뿔려주세용
		if (p.x <= 0) {
			unit->minePos = p;
			unit->setState(VultureUnit::STATE::IDLE);
		}
		else {
			unit->minePos = p;
			unit->setState(VultureUnit::STATE::NM_MINE_FRONT);
		}
		
		//printf("전맵에 박아요\n");
	}

	

	// 적위치에 마인 박는거도 해야함











	if (BWAPI::Broodwar->getFrameCount() % 100 == 101) {
		//printf("마인 심으러 가자~~\n");
		int mv = 0;
		int n = 6;
		if (VultureControl::getInstance()->twoFactoryScene) {
			/*if (CombatInfo::getInstance()->enemyGroup.size() > 0) {
			printf("유닛한테 마인 심기 꺄르륵\n");
			int minDist = 99999;
			enemyPos = Position(0, 0);
			enemyRange = 0;
			for (auto q = CombatInfo::getInstance()->enemyGroundGroup.begin(); q != CombatInfo::getInstance()->enemyGroundGroup.end(); q++) {
			float dist = getDistance((*q).first, CombatInfo::getInstance()->myGroup.front().first);
			if (dist < minDist) {
			minDist = dist;
			enemyRange = (*q).second.first;
			enemyPos = (*q).first;
			}
			}
			if (enemyPos.x == 0) goto out;
			for (auto v = vultures.begin(); v != vultures.end(); v++) {
			if ((*v)->getSelf()->isCompleted() && (*v)->getSelf()->getSpiderMineCount() > 0) {
			float dist = getDistance((*v)->getSelf()->getPosition(), TF_MinePosition);
			if (1){//dist < UnitTypes::Terran_Vulture.groundWeapon().maxRange() * 2) {
			mv++;
			}
			}
			}
			//	printf("벌쳐 %d기 출동!~~\n", mv);
			int count = 0;
			float PI = 3.141592f;
			for (auto v = vultures.begin(); v != vultures.end(); v++) {
			if ((*v)->getSelf()->isCompleted() && (*v)->getSelf()->getSpiderMineCount() > 0) {
			if (1){//dist < UnitTypes::Terran_Vulture.groundWeapon().maxRange() * 2) {
			int CX = enemyPos.x;
			int CY = enemyPos.y;
			int dx = (int)(enemyRange * cos(2 * PI / mv*count));
			int dy = (int)(enemyRange * sin(2 * PI / mv*count));
			Position minPos = Position(CX + dx, CY + dy);
			Position cannonPos = VultureControl::getInstance()->getNearestCannonPos(minPos);
			count++;
			float dist = getDistance(minPos, cannonPos);
			if (dist < UnitTypes::Protoss_Photon_Cannon.groundWeapon().maxRange() + 40) continue;
			if (!BWAPI::Broodwar->isWalkable((WalkPosition)minPos)) continue;

			(*v)->setState(VultureUnit::STATE::TF_MINE_FRONT);
			(*v)->minePos = minPos;
			//printf("나는 마인 박으라고 했다.\n");
			}
			}
			}
			}
			else */ {
				printf("특정 지정 위치에 마인 심기 꺄르르르륵\n");

			}
		}
	}
}

int UnitControl::getNearUnitCount(VultureUnit* u) {
	int ret = 0;
	for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
		float dist = getDistance((*q).second.second, u->getSelf()->getPosition());

		if (dist < (*q).second.first.groundWeapon().maxRange() + u->getSelf()->getType().groundWeapon().maxRange()) {
			ret++;
		}
	}
	return ret;
}

Unitset UnitControl::getNearEnemyUnits() {
	Unitset ret;
	ret.clear();
	for (auto v = vultures.begin(); v != vultures.end(); v++) {
		for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
			float dist = getDistance((*q).second.second, (*v)->getSelf()->getPosition());
			if (dist < (*q).second.first.groundWeapon().maxRange() + (*v)->getSelf()->getType().groundWeapon().maxRange()) {
				Unit u = VultureControl::getInstance()->getUnitforID((*q).first);
				ret.insert(u);
			}
		}
		for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
			float dist = getDistance((*q).second.second, (*v)->getSelf()->getPosition());
			if (dist < (*q).second.first.groundWeapon().maxRange() + (*v)->getSelf()->getType().groundWeapon().maxRange()) {
				Unit u = VultureControl::getInstance()->getUnitforID((*q).first);
				ret.insert(u);
			}
		}
	}
	for (auto t = tanks.begin(); t != tanks.end(); t++) {
		for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
			float dist = getDistance((*q).second.second, (*t)->getSelf()->getPosition());
			if (dist < (*q).second.first.groundWeapon().maxRange() + (*t)->getSelf()->getType().groundWeapon().maxRange()) {
				Unit u = VultureControl::getInstance()->getUnitforID((*q).first);
				ret.insert(u);
			}
		}
		for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
			float dist = getDistance((*q).second.second, (*t)->getSelf()->getPosition());
			if (dist < (*q).second.first.groundWeapon().maxRange() + (*t)->getSelf()->getType().groundWeapon().maxRange()) {
				Unit u = VultureControl::getInstance()->getUnitforID((*q).first);
				ret.insert(u);
			}
		}
	}
	for (auto t = marines.begin(); t != marines.end(); t++) {
		for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
			float dist = getDistance((*q).second.second, (*t)->getSelf()->getPosition());
			if (dist < (*q).second.first.groundWeapon().maxRange() + (*t)->getSelf()->getType().groundWeapon().maxRange()) {
				Unit u = VultureControl::getInstance()->getUnitforID((*q).first);
				ret.insert(u);
			}
		}
		for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
			float dist = getDistance((*q).second.second, (*t)->getSelf()->getPosition());
			if (dist < (*q).second.first.groundWeapon().maxRange() + (*t)->getSelf()->getType().groundWeapon().maxRange()) {
				Unit u = VultureControl::getInstance()->getUnitforID((*q).first);
				ret.insert(u);
			}
		}
	}

	return ret;
	/*for (auto g = goliaths.begin(); g != goliaths.end(); g++) {
	}*/
}

Unit UnitControl::getAttackUnit(Unitset us, MyUnit* unit) {
	Unit ret = NULL;
	int retInt = -1;
	float maxValue = 0;
	float utC = 20.0, bloodC = 5.0, distC = 3.0, damageC = 1.0, cannonC = 10.0;
	for (auto q = us.begin(); q != us.end(); q++) {
		if ((*q) == NULL) continue;
		if ((*q)->getType().isBuilding() && (*q)->getType() != UnitTypes::Protoss_Photon_Cannon) continue;
		float value = getUnitValue(unit->getSelf()->getType(), (*q)->getType())*utC;
		if ((*q) != NULL) {
			Unit target = (*q);
			if (target->isVisible() == false) continue;
			if (target == NULL || target->exists() == false) continue;
			value += (((float)(*q)->getType().maxShields() + (*q)->getType().maxHitPoints()) - (target->getHitPoints() + target->getShields())) / (float)((*q)->getType().maxShields() + (*q)->getType().maxHitPoints()) *bloodC;
			int range = (*q)->getType().groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange();
			value += (range - getDistance(unit->getSelf()->getPosition(), (*q)->getPosition())) / range * distC;

			// 예상 피해 value +=

			if (value > maxValue) {
				maxValue = value;
				ret = target;
			}
		}
	}

	return ret;
}

float UnitControl::getUnitValue(UnitType ut, UnitType enemyUt) {
	float ret = 0.f;
	if (ut == UnitTypes::Terran_Marine) {
		ret = HMarineControl::getInstance()->getTargetUnitValue(enemyUt);
	}
	else if (ut == UnitTypes::Terran_Vulture) {
		ret = VultureControl::getInstance()->getTargetUnitValue(enemyUt, UnitControl::getInstance()->vultures.size(),  CombatInfo::getInstance()->getEnemyBuildingNumber(UnitTypes::Protoss_Photon_Cannon));
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		ret = TankControl::getInstance()->getTargetUnitValue(enemyUt);
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		ret = TankControl::getInstance()->getTargetUnitValueforSiege(enemyUt);
	}
	else if (ut == UnitTypes::Terran_Goliath) {
		ret = 0;
	}
	return ret;
}