#pragma once
#include "MyUnit.h"
#include <map>

using namespace std;

class Normal{
private:
	vector<Marine*> marine;
	vector<Position> marinePos;
	map<Marine*, Position> HoldMarine;
public:
	Normal();
	~Normal();
	
	void getMarine();
	Marine* getIdleMarine();
	
	void MarineControl(Marine* marine);
	void HoldMarinePosition();
	void run();
};