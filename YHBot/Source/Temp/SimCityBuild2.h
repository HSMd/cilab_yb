#pragma once
#include "Work.h"
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"

class SimCity_2
{
	enum FLAG{
		EXTAND_FRONT,
		EXTAND_MORE,

		UPGRADE_SIEGE_MODE,
		UPGRADE_MINE,
		UPGRADE_VEHICLE_WEAPONS,

		ENTRANCE_OPEN,
		ENTRANCE_CLOSE,

		SCV_MOVE,
		NO_SCV,
	};

	BaseLocation* base2;
	BaseLocation* base3;

	list<scheduleWork> build;	
	TerritoryMgr* tMgr;

	SCV* out;
	vector<SCV*> splySCV;

	map<int, bool> flags;
	vector<Structure*> moveBuilding;

	int frameCount;

	TilePosition front_brkPos;
	TilePosition front_splyPos;
	TilePosition cntPos;
	TilePosition cntPos2;
	TilePosition front_trtPos;
	TilePosition outPos;

	vector<TilePosition> multiSplyPos;
	queue<TilePosition> trtPoses;	

	// DEFENCE 
	vector<TilePosition> siegePoses;
	map<TilePosition, Tank*> holdTanks;
	vector<Tank*> subTanks; // 3번째 확장에 배치될 애들.

public:
	SimCity_2();
	~SimCity_2();

	void run(COMBAT_FLAG combat);
	void move();

	void errorSCV();
	bool chkEnd();

	TilePosition getTurretPos();

	// DEFENCE
	void tankDefence();
	Tank* getIdleTank();

};

