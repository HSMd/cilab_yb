#include "TerritoryMgr.h"

#include "UtilMgr.h"
#include "../Single.h"

TerritoryMgr::TerritoryMgr()
{
	myTerritorys.push_back(new Territory());
}


TerritoryMgr::~TerritoryMgr()
{
}


void TerritoryMgr::run(){
	
	for (auto territory : myTerritorys)
	{
		if (territory->isIdle()){
			territory->run();
		}		
	}
}

Territory* TerritoryMgr::getNearTerritory(TilePosition dst){

	float minDis = 999999.0;
	Territory* re = nullptr;

	for (auto& territory : myTerritorys){
		if (!territory->isIdle())
			continue;

		float distance = getDistance(territory->getCmdTilePosition(), dst);

		if (distance < minDis){
			minDis = distance;
			re = territory;
		}
	}

	return re;
}

void TerritoryMgr::completeSCVReturn(Unit unit){
	for (auto& territory : myTerritorys){
		for (auto& scv : territory->getWorkSCV()){
			if (scv->getTargetPos() == unit->getTilePosition() && scv->getTargetType() == unit->getType()){
				scv->orderReturn();
			}
		}
	}
}

void TerritoryMgr::addTerritory(Unit unit){

	if (bw->getFrameCount() < 50){
		myTerritorys[0]->init(mapInfo->getMyStartBase(), me->getBuildings(UnitTypes::Terran_Command_Center).back());
		return;
	}

	auto& otherBases = mapInfo->getAllBases();
	BWTA::BaseLocation* base = nullptr;

	
	for (auto other : otherBases){
		if (other->getTilePosition() == unit->getTilePosition()){
			base = other;
			break;
		}
	}

	if (base != nullptr){
		bw << "Complete" << endl;
		myTerritorys.push_back(new Territory(base, me->getBuildings(UnitTypes::Terran_Command_Center).back()));
	}
}

void TerritoryMgr::addTerritory(Territory* territory){

	myTerritorys.push_back(territory);
}

void TerritoryMgr::addSCV(Unit unit){

	//MyUnit* scv = me->getUnits(UnitTypes::Terran_SCV).back();
	MyUnit* scv = me->getImmediatelyUnit(UnitTypes::Terran_SCV).back();

	if (bw->getFrameCount() < 50){
		myTerritorys[0]->addSCV(scv);
		return;
	}

	Territory* territory = getNearTerritory(unit->getTilePosition());
	if (territory->getRequireSCVCount() <= 0){
		for (auto t : myTerritorys){
			if (!t->isIdle()){
				continue;
			}

			if (0 < t->getRequireSCVCount()){
				territory = t;
				break;
			}
		}
	}

	territory->addSCV(scv);
}

void TerritoryMgr::gasCompleted(Unit unit){

	for (auto territory : myTerritorys){
		if (territory->getGasPosition() == unit->getTilePosition()){
			territory->gasCompleted(unit);

			break;
		}
	}
}