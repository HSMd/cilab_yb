#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <list>
#include "MyUnit.h"

using namespace std;
using namespace BWAPI;
using namespace Filter;

class Scouting
{
	Scouting();
	~Scouting();

	bool chkFinScv;
	static Scouting *s_Scouting;

	BWAPI::Position scoutPosition[128][128];
	int				scoutValue[128][128];

	bool unLookEnemy = true;
	bool checkStarting[4];
	int targetIndex = 0;

	BWTA::BaseLocation* enemyStartingBase;	
	BWTA::BaseLocation* targetBase;
	BWAPI::Position movePosition[4];

public:
	static Scouting* getInstance();
	static Scouting* create();
	
	void setScoutUnit(SCV *);
	SCV * spy = NULL;
	
	// OnFunc
	void init();
	void run();

	// Scouting Func
	void SCV_Scout(SCV *u);
	void AIR_Scout(MyUnit * u);

	// Scout Info Func
	void checkEnemyBase(MyUnit* s_Unit);
	BWTA::BaseLocation* getTargetBase();
	void goTarget(MyUnit *u, BWAPI::Position p);
	void arriveTarget();
	void goOutofOnesAwayEnemyStarting(SCV* u);
	void setMovePosition(BWAPI::Position p, MyUnit *u);
	map<BWTA::BaseLocation*, bool> startingBaseCheck;
	
	int scoutFrame = 0;
};

