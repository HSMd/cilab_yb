#pragma once
#include <BWAPI.h>

#include <map>
#include <list>

#include "MyUnit.h"

using namespace BWAPI;
using namespace std;

class VultureControl{
private:
	bool mineBackEnough = false;
	bool mineFrontEnough = false;
	bool mineMultiEnough = false;
	int firstFrontMine = 0;
	int firstBackMine = 0;
	bool flag_Physical = false;
	Position firstMinePosition[4][3];
	bool enemyStartingFirstCheck = true;
	list<Position> frontMineVec;

public:
	VultureControl();
	~VultureControl();
	static VultureControl* create();
	static VultureControl* getInstance();
	static VultureControl* s_VultureControl;

	map<Position, bool> checkMultiMine;
	bool allMulitMine = false;

	Position hongMinePos[100];
	float hongMineValue[100];
	std::map<Position, bool> hongCheck;
	std::map<Unit, Position> hongMine2Pos;
	Position getHongMinePos();
	int smIndex = 65;

	void run(VultureUnit*, bool);
	void onFrame();
	int vultureIM[128][128];
	int ScoutIM[128][128];

	//control func
	void MB_Control(VultureUnit* unit);
	void MF_Control(VultureUnit* unit);
	void WAIT_Control(VultureUnit* unit);
	void ATTACK_Control(VultureUnit* unit);
	void DEFENSE_Control(VultureUnit* unit);
	void SQUEEZE_Control(VultureUnit* unit);
	void RTB_Control(VultureUnit* unit);
	void MM_Control(VultureUnit* unit);
	void CRAZY_Control(VultureUnit* unit, bool kiting);
	void SCOUT_Control(VultureUnit* unit);

	void TF_JUNJIN_Control(VultureUnit* unit, bool kiting);
	void TF_CRAZY_Control(VultureUnit* unit);
	void TF_SQUEEZE_Control(VultureUnit* unit);
	void TF_ATTACK_Control(VultureUnit* unit);
	void TF_FM_Control(VultureUnit* unit);

	void KJ_WAIT_Control(VultureUnit* unit);
	void KJ_ATTACK_Control(VultureUnit* unit);
	void KJ_MINE_Control(VultureUnit* unit);

	bool twoFactoryScene = true;
	bool twoFactoryJunjin = false;
	Unit getTargetUnit(VultureUnit* unit, bool cannon);
	Position twoFactoryVultureAttackPos;
	float getTargetUnitValue(UnitType ut, int v, int c);
	Unit getUnitforID(int id);
	float getExpectDamage(Position p, int range);
	Position getMulitMinePos(int);
	Position getFrontMinePos(int);
	Position getBackMinePos(int);
	Position getTFMinPos(VultureUnit* unit);
	float getNearUnitDistance(Position);
	void kitingMove(VultureUnit* unit);

	//other func
	void goodLuck(VultureUnit *unit);
	void drawVultrueControlUnit(VultureUnit* unit);
	void drawVultrueControl();
	void updateIM();
	void updateHong();
	void setFrontMineVector();
	void setStartMinePos();
	int range = 1;

	bool isSafeZone(VultureUnit* unit, Unit target);
	Position getNearestTankPos(VultureUnit* unit);
	bool back_all = false;
	Position getNearestCannonPos(Position p);
	Unit getNearestCannon(Position p);
	int mineIndex = 0;

	BWTA::BaseLocation* sb;
	list<BWTA::BaseLocation*> scoutBase;
	int scoutIndex;
	void setScoutBaseLocationVec();

	Position attackPosition;
};