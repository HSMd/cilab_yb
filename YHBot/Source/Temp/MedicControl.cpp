#include <list>
#include <vector>
#include <map>
#include <math.h>
#include "MedicControl.h"
#include "CombatInfo.h"
#include "UtilMgr.h"
#include "../Single.h"

Medic::Medic(Unit _self) : MyUnit(_self) {
	state = STATE::IDLE;
	healTarget = NULL;
	rPos = Position(0, 0);
}

Medic::~Medic() {}

void MedicControl::run(Medic* unit) {
	if (unit->getState() == Medic::STATE::IDLE) {

	}
	else if (unit->getState() == Medic::STATE::WAIT) {
		MedicControl::getInstance()->WAIT_Control(unit);
	}
	else if (unit->getState() == Medic::STATE::SIMSITY) {
		MedicControl::getInstance()->DEFENCE_Control(unit);
	}
	else if (unit->getState() == Medic::STATE::RETURN) {
		MedicControl::getInstance()->RETURN_Control(unit);
	}
	else if (unit->getState() == Medic::STATE::HEAL) {
		MedicControl::getInstance()->HEAL_Control(unit);
	}
	else if (unit->getState() == Medic::STATE::CRAZY) {
		MedicControl::getInstance()->CRAZY_Control(unit);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Medic Control

MedicControl* MedicControl::s_MedicControl;
MedicControl* MedicControl::create() {
	if (s_MedicControl)
	{
		return s_MedicControl;
	}
	s_MedicControl = new MedicControl();
	return s_MedicControl;
}
MedicControl* MedicControl::getInstance() {
	return s_MedicControl;
}
MedicControl::MedicControl() {
	
}

MedicControl::~MedicControl() {}

void MedicControl::onFrame() {
	//updateIM();
	bionicUnits.clear();
	Unitset myUnits = BWAPI::Broodwar->self()->getUnits();
	for (auto q = myUnits.begin(); q != myUnits.end(); q++) {
		if ((*q)->getType() == UnitTypes::Terran_Marine || (*q)->getType() == UnitTypes::Terran_Medic || (*q)->getType() == UnitTypes::Terran_SCV) {
			bionicUnits.insert((*q));
		}
	}

}

void MedicControl::goodLuck(Medic* unit) {
	
}

void MedicControl::WAIT_Control(Medic* unit) {

}
void MedicControl::DEFENCE_Control(Medic* unit) {

}
void MedicControl::RETURN_Control(Medic* unit) {

}
void MedicControl::SIMSITY_Control(Medic* unit) {

}
void MedicControl::CRAZY_Control(Medic* unit) {
	unit->healTarget = getHealUnit(unit);

	if (unit->healTarget == NULL) {
		if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
			unit->getSelf()->move(CombatInfo::getInstance()->myUnitPos, false);
		}
	}
	else {
		if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
			unit->getSelf()->useTech(TechTypes::Healing, unit->healTarget);
		}
	}
	
}
void MedicControl::HEAL_Control(Medic* unit) 
{
	unit->healTarget = getHealUnit(unit);
	if (unit->healTarget != NULL)
	{
		if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
			unit->getSelf()->useTech(TechTypes::Healing, unit->healTarget);
		}
	}
	else
	{
		Unit nearunit = getNearestBionicUnit(unit);
		if (nearunit == nullptr)
		{
			if (BWAPI::Broodwar->getFrameCount() % 24 == 0) {
				unit->getSelf()->move(mapInfo->getMyStartBase()->getPosition());
			}
		}
		else {
			if (BWAPI::Broodwar->getFrameCount() % 24 == 0) {
				unit->getSelf()->rightClick(nearunit);
			}
		}
	}
}

float MedicControl::getHealUnitValue(UnitType ut) 
{
	if (ut == UnitTypes::Terran_Marine)
	{
		return 5;
	}
	else if (ut == UnitTypes::Terran_Medic)
	{
		return 3;
	}
	else if (ut == UnitTypes::Terran_SCV)
	{ 
		return 4;
	}
	else if (ut == UnitTypes::Terran_Firebat)
	{ 
		return 5;
	}
	else if (ut == UnitTypes::Terran_Ghost)
	{ 
		return 5;
	}
	return -10;
}

Unit MedicControl::getUnitforID(int id) {
	for (auto q = BWAPI::Broodwar->enemy()->getUnits().begin(); q != BWAPI::Broodwar->enemy()->getUnits().end(); q++) {
		if ((*q)->exists() && (*q)->getID() == id) {
			return (*q);
		}
	}
	return NULL;
}

void MedicControl::drawMedicControlUnit(Medic* unit) {
	if (unit->healTarget != NULL) {
		BWAPI::Broodwar->drawLineMap(unit->getSelf()->getPosition(), unit->healTarget->getPosition(), Colors::Yellow);
	}
	if (unit->getState() == Medic::STATE::CRAZY) {
		BWAPI::Broodwar->drawCircleMap(unit->getSelf()->getPosition(), 5, Colors::Orange, true);
	}
	else {
		BWAPI::Broodwar->drawCircleMap(unit->getSelf()->getPosition(), 5, Colors::Yellow, true);
	}
	
}

void MedicControl::drawMedicControl() {
	/*for (auto q = bionicUnits.begin(); q != bionicUnits.end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 9, Colors::Yellow, false);
	}*/
}

void MedicControl::supportingMove(Medic* unit) 
{

}

Unit MedicControl::getHealUnit(Medic* unit) 
{
	Unit ret = NULL;
	
	int maxValue = 0;
	int value = 0;

	for (auto q = bionicUnits.begin(); q != bionicUnits.end(); q++)
	{
		if ((*q)->getID() == unit->getSelf()->getID()) continue;
		if ((*q)->isCompleted() == false) continue;
		if (!isAlmostReachedDest((*q)->getPosition(), unit->getSelf()->getPosition(), unit->getSelf()->getType().seekRange(), unit->getSelf()->getType().seekRange()))	continue;
	
		value = getHealUnitValue((*q)->getType());
		
		int score = ((*q)->getType().maxHitPoints() - (*q)->getHitPoints()) * value;
		
		if (score > maxValue) 
		{
			ret = (*q);
			maxValue = score;
		}
	}
	return ret;
}

Position MedicControl::getNearestBionicUnitPos(Medic* unit) 
{
	Position ret = Position(0,0);

	float minDist = 9999.0f;

	for (auto q = bionicUnits.begin(); q != bionicUnits.end(); q++) {
		if ((*q)->getID() == unit->getSelf()->getID()) continue;
		if ((*q)->getType() != UnitTypes::Terran_Marine) continue;
		
		float dist = getDistance((*q)->getPosition(), unit->getSelf()->getPosition());
		
		if (dist < minDist)
		{
			dist = minDist;
			ret = (*q)->getPosition();
		}
	}

	return ret;
}

Unit MedicControl::getNearestBionicUnit(Medic* unit)
{
	Unit ret = nullptr;

	float minDist = 9999.0f;

	for (auto q = bionicUnits.begin(); q != bionicUnits.end(); q++)
	{
		if ((*q)->getID() == unit->getSelf()->getID()) continue;
		if ((*q)->getType() != UnitTypes::Terran_Marine) continue;
	
		float dist = getDistance((*q)->getPosition(), unit->getSelf()->getPosition());
		
		if (dist < minDist) 
		{
			dist = minDist;
			ret = (*q);
		}
	}
	return ret;
}