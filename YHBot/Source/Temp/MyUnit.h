#pragma once
#include <BWAPI.h>
#include "UtilMgr.h"

using namespace BWAPI;

class MyUnit
{
protected:
	Unit self;

public:
	MyUnit(Unit _self){
		self = _self;
		team = 0;
	};
	~MyUnit(){};

	virtual void run(){};
	Unit getSelf(){ return self; };
	Position rPos;
	int team;
};

class Structure : public MyUnit{

public:
	enum STATE{
		IDLE,
		LIFT,
		LAND,
		MOVE,
	};

private:
	STATE state;
	TilePosition targetPos;

public:
	Structure(Unit _self) : MyUnit(_self) { state = IDLE; targetPos = _self->getTilePosition(); };
	~Structure() {};

	void run() {};
	void orderIdle(){ state = IDLE; };
	void orderLift(){ state = LIFT; self->lift(); };
	void orderMove(TilePosition pos){ targetPos = pos; state = MOVE; self->lift(); }
	bool orderLand(TilePosition pos){ targetPos = pos; state = LAND; return self->land(pos); }

	STATE getState() { return state; }
	TilePosition getTargetPos(){ return targetPos; };
};

class SCV : public MyUnit{
public:
	enum STATE{
		IDLE,
		MINERAL,
		GAS,
		MOVE,
		RETURN,
		WAIT,
		BUILD,
		ATTACK,
		ETC,
		
		//BAEK
		HELP,

		// Woo
		REPAIR
	};

private:
	STATE state;
	Unit target;
	Unit attackTarget;
	Unit helpTarget;
	UnitType targetType;
	TilePosition targetPos;
	Position movePos;

	bool error;

public:
	SCV(Unit _self) : MyUnit(_self) {
		state = STATE::IDLE;
		error = false;
	};
	~SCV();

	void run() {};

	// Order
	void orderIDLE() { self->stop(); state = STATE::IDLE; };
	void orderMineral(Unit target){ this->target = target; state = STATE::MINERAL; }
	void orderGas(Unit target){ this->target = target; state = STATE::GAS; }
	void orderETC(){ self->stop(); state = STATE::ETC; }
	void orderAttack(Unit target) { attackTarget = target; state = STATE::ATTACK; };
	void orderMove(TilePosition pos){ self->stop(); targetPos = pos; state = STATE::MOVE; }
	void orderMove(Position pos){ self->stop(); movePos = pos; state = STATE::MOVE; }
	void orderWait() { self->stop(); state = STATE::WAIT; }
	void orderBuild(UnitType type, TilePosition pos){ self->stop(); this->targetType = type, this->targetPos = pos; state = STATE::BUILD; }
	void orderReturn(){ state = STATE::RETURN; }

	// Woo
	void orderRepair(Unit unit){ this->target = unit; state = STATE::REPAIR; }

	void setError(bool err) { error = err; };
	bool getError() { return error; };

	void setTargetPos(TilePosition pos){ this->targetPos = pos; };

	void setState(STATE state) { this->state = state; };
	STATE getState() { return state; }
	TilePosition getTargetPos() { return targetPos; }
	Unit getTarget() { return target; }
	UnitType getTargetType(){ return targetType; }

	//BAEK
	Unit getHelpTarget() { return helpTarget; }
	Unit setHelpTarget(Unit unit) { this->helpTarget = unit; }

};

/////////////////////////////////

class Marine : public MyUnit{

public:
	enum STATE{
		IDLE,
		ATTACK,
		HOLD,
		MOVE,
		RETURN,
		WAIT,
		CRAZY,
		ETC,

		//BAEK
		PROTECT,

		//
		NORMALHOLD,
		ATTACKHOLD,
	};

	Marine(Unit _self) : MyUnit(_self) {
		state = STATE::IDLE;
	};

	~Marine() {};

private:
	STATE state;
	Unit attackTarget;
	UnitType targetType;
	TilePosition targetPos;
	Unit protectTarget;

public:
	void orderIDLE(){ state = STATE::IDLE; }
	void orderMove(TilePosition tpos){ this->targetPos = tpos; state = STATE::MOVE; }
	void orderAttack(Unit unit){ this->attackTarget = unit; state = STATE::ATTACK; }
	void orderHold(){ self->stop(); state = STATE::HOLD; }
	void orderWait(){ self->stop(); state = STATE::WAIT; }
	void orderETC(){ self->stop(); state = STATE::ETC; }
	void orderReturn(){ state = STATE::RETURN; }
	void orderProtect(Unit unit) { this->protectTarget; state = STATE::PROTECT; }

	void setState(STATE state) { this->state = state; };
	STATE getState() { return state; }
	TilePosition getTargetPos() { return targetPos; }
	Unit getAttackTarget() { return attackTarget; }
	void setAttackTarget(Unit unit) { this->attackTarget = unit; }
	UnitType getTargetType(){ return targetType; }
	Unit getProtectTarget() { return protectTarget; }
	void setProtectTarget(Unit unit) { this->protectTarget = unit; }
};

class Solider : public MyUnit{
public:
	enum STATE{
		IDLE,
	};

private:
	STATE state;
	Unit target;
	UnitType targetType;
	TilePosition targetPos;

public:

	Solider(Unit _self) : MyUnit(_self) {
		state = STATE::IDLE;
	};

	~Solider() {};
};

class VultureUnit : public MyUnit {
public:
	enum STATE {
		IDLE,

		NM_MINE_BACK,
		NM_MINE_FRONT,
		NM_WAIT,
		NM_ATTACK,
		NM_DEFENSE,
		NM_SQUEEZE,
		NM_RETURNTOBASE,
		NM_MINE_MULTI,
		NM_CRAZY,
		NM_SCOUT,

		TF_JUNJIN,
		TF_MINE_FRONT,
		TF_SQUEEZE,
		TF_CRAZY,
		TF_ATTACK,

		KJ_WAIT,
		KJ_ATTACK,
		KJ_MINE,

	};

private:
	STATE state;
	Unit target;
	UnitType targetType;
	Position targetPos;

public:
	VultureUnit(Unit _self);
	~VultureUnit();

	bool safe;
	bool isMining;
	bool isMove;
	Position minePos;
	void setState(STATE state) { this->state = state; }
	STATE getState() { return state; }
	Position getTargetPos() { return targetPos; }
	Unit attackTarget;
	int spiderNumber;
	Unit cannon;
	char* getString() {
		if (state == NM_MINE_FRONT) {
			return "MINE_FRONT";
		}
		else if (state == NM_DEFENSE) {
			return "DEFENSE";
		}
		else if (state == NM_SQUEEZE) {
			return "SQUEEZE";
		}
		else if (state == NM_SCOUT) {
			return "SCOUT";
		}
		else if (state == NM_CRAZY) {
			return "CRAZY";
		}
		else if (state == KJ_ATTACK) {
			return "KJ_ATTACK";
		}
		else if (state == KJ_MINE) {
			return "KJ_MINE";
		}
		else if (state == KJ_WAIT) {
			return "KJ_WAIT";
		}
		else if (state == NM_RETURNTOBASE) {
			return "RETURN2BASE";
		}
		else {
			return "IDLE";
		}
	}
};

class Tank : public MyUnit {
public:
	enum STATE {
		IDLE,
		TC_DEFENCE1,
		TC_DEFENCE2,
		TC_DEFENCE3,
		KJ_WAIT,
		KJ_ATTACK,
		WAIT,
		DEFENCE,
		RETURN,
		MOVE,
		CRAZY,
		SIEGE,
	};

private:
	STATE state;
	Unit target;
	UnitType targetType;
	Position targetPos;

public:
	Tank(Unit _self);
	~Tank();

	bool isMove;
	bool isSiege;
	bool DDeokUnit;
	bool defenceUnit;
	void setState(STATE state) { this->state = state; }
	STATE getState() { return state; }
	Position getTargetPos() { return targetPos; }
	Unit attackTarget;
	bool isBackup;
	bool isJunjin;
	bool isInTeam;
	int frameCount;
};

class HMarine : public MyUnit {
public:
	enum STATE {
		IDLE,
		WAIT,
		DEFENCE,
		RETURN,
		MOVE,
		CRAZY,
		ATTACK,
		GAMIKAZE,
	};

private:
	STATE state;
	Unit target;
	UnitType targetType;
	Position targetPos;

public:
	HMarine(Unit _self);
	~HMarine();

	bool isMove;
	void setState(STATE state) { this->state = state; }
	STATE getState() { return state; }
	Position getTargetPos() { return targetPos; }
	Unit attackTarget;
	Position kPos;
};

class Medic : public MyUnit {
public:
	enum STATE {
		IDLE,
		WAIT,
		SIMSITY,
		RETURN,
		MOVE,
		HEAL,
		CRAZY,
	};

private:
	STATE state;
	Unit target;
	UnitType targetType;
	Position targetPos;

public:
	Medic(Unit _self);
	~Medic();

	void setState(STATE state) { this->state = state; }
	STATE getState() { return state; }
	Position getTargetPos() { return targetPos; }
	Unit healTarget;
};

class Goliath : public MyUnit {

public:
	enum STATE {
		IDLE,
		ATTACK,
		HOLD,
		MOVE,
		RETURN,
		WAIT,
		CRAZY,
		ETC,

		//BAEK
		PROTECT
	};


private:
	STATE state;
	Unit attackTarget = nullptr;
	UnitType attackTargetType;
	TilePosition targetPos;
	Unit protectTarget = nullptr;

public:
	Goliath(Unit _self) : MyUnit(_self) {
		state = STATE::IDLE;
	};
	~Goliath();

	void orderIDLE() { state = STATE::IDLE; }
	void orderMove(TilePosition tpos) { this->targetPos = tpos; state = STATE::MOVE; }
	void orderAttack(Unit unit) { this->attackTarget = unit; state = STATE::ATTACK; }
	void orderHold() { self->stop(); state = STATE::HOLD; }
	void orderWait() { self->stop(); state = STATE::WAIT; }
	void orderETC() { self->stop(); state = STATE::ETC; }
	void orderReturn() { state = STATE::RETURN; }
	void orderProtect(Unit unit) { this->protectTarget; state = STATE::PROTECT; }

	void setState(STATE state) { this->state = state; };
	STATE getState() { return state; }
	TilePosition getAttackTargetPos() { return targetPos; }
	Unit getAttackTarget() { return attackTarget; }
	void setAttackTarget(Unit unit) { this->attackTarget = unit; }
	UnitType getAttackTargetType() { return attackTargetType; }
	Unit getProtectTarget() { return protectTarget; }
	void setProtectTarget(Unit unit) { this->protectTarget = unit; }
};