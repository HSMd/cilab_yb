#pragma once
#include <BWAPI.h>
#include "MyUnit.h"



using namespace BWAPI;
using namespace std;

class GoliathControl {

private:
	int GoliathIM[128][128];
public:

	GoliathControl();
	~GoliathControl();
	static GoliathControl* create();
	static GoliathControl* getInstance();
	static GoliathControl* s_GoliathControl;
	void run(Goliath*);
	void onFrame();

	//control func
	void WAIT_Control(Goliath* unit);
	void ATTACK_Control(Goliath* unit);
	void RETURN_Control(Goliath* unit);
	void MOVE_Control(Goliath* unit);
	void CRAZY_Control(Goliath* unit);
	void PROTECT_Control(Goliath* unit);

	Unit getTargetUnit(Unit unit);
	Unit getUnitforID(int id);

	int getUnitGroundAttackRadius(Unit);
	int getUnitAirttackRadius(Unit);
	Unit getTargetToHelpFriend(Unit);
	vector<TilePosition> getNearTilePositions(TilePosition);

	int getAngleTwoPoints(Position, Position);
	int getAngleArea(int);

	void GoliathControl::drawGoliathControlUnit(Goliath*);
	//other func
	void goodLuck(Goliath *unit);
	void kitingMove(Goliath* unit);

	void updateIM();

	Unitset getSafeUnitsInSeekRange(Unit);
	Unitset getDangerUnitsInSeekRange(Unit);
	Unit getClosestTankToSave(Unit);
	int getWorstAreaScore(Goliath *unit);

	Position getBestPosition(Unit);

	map<UnitType, int> pt_weight;
	map<UnitType, int> tr_weight;

	Position attackPosition;

};