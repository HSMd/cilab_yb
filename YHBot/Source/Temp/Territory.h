#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include <map>
#include <queue>

#include "MyUnit.h"

using namespace BWAPI;
using namespace BWTA;
using namespace std;

class Territory
{
	bool idle;

	Structure* cmdCenter;
	int scvCount;
	int gasSCVCount;

	//Unitset minerals;
	vector<Unit> minerals;
	Unit gas;

	TilePosition basePosition;
	TilePosition gasPos;

	map<Unit, int> resourceCount;

	vector<SCV*> mineralSCV;
	vector<SCV*> gasSCV;
	vector<SCV*> workSCV;
	vector<SCV*> overSCV;

public:
	
	Territory();
	Territory(BaseLocation* base);
	Territory(BaseLocation* base, MyUnit* cmdCenter);
	~Territory();

	void init(BaseLocation* base, MyUnit* cmdCenter);
	void run();
	void show();

	// SCV Control //
	void resourceSCVControl(SCV* scv);
	void workSCVControl(SCV* scv);

	// Resource //
	Unit getIdleMineral();
	Unit getIdleGas();

	void orderMineral(SCV* scv, Unit target);
	void orderGas(SCV* scv, Unit target);

	bool getProperBasicWork(SCV* scv);
	void gasCompleted(Unit gas);

	void addSCV(MyUnit* newSCV, bool re=false);
	void addSCV(SCV* newSCV, bool re = false);

	void setGasSCVCount(int count){ gasSCVCount = count; };

	// Work //
	SCV* getWorker(bool except = false);
	SCV* getWorker(TilePosition targetPosition, bool except = false);
	SCV* getGasWorker();

	bool isIdle(){ return idle; };
	int getSCVCount() { return mineralSCV.size() + workSCV.size() + gasSCV.size() + overSCV.size(); };
	int getRequireSCVCount() { return scvCount - getSCVCount(); };
	TilePosition getGasPosition() { return gasPos; };
	TilePosition getCmdTilePosition() { return cmdCenter->getSelf()->getTilePosition(); };
	TilePosition getBaseTilePosition() { return basePosition; };
	vector<SCV*> getWorkSCV(){ return workSCV; };
	vector<SCV*> getMineralSCV(){ return mineralSCV; };
	void moveTerritory(Territory* territory);
	MyUnit* getCmdCenter(){ return cmdCenter; };
	void deleteScv(SCV* scv);
};

