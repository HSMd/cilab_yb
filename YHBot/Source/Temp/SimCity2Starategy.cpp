#include "SimCity2Starategy.h"
#include "../single.h"
#include "UtilMgr.h"


SimCity2Strategy::SimCity2Strategy(){

	this->tMgr = &cmd->getTerritoryMgr();
	init();
}

SimCity2Strategy::~SimCity2Strategy(){
}

Marine* SimCity2Strategy::getIdleMarine(){

	for (auto m : me->getUnits(UnitTypes::Terran_Marine)){
		Marine* marine = dynamic_cast<Marine*>(m);
		if (marine->getState() == Marine::STATE::IDLE){
			marine->setState(Marine::STATE::WAIT);
			return marine;
		}
	}

	return nullptr;
}

void SimCity2Strategy::init(){
	
	switch (mapInfo->getDir()){
	case 0:
		marinePosition.push_back(tile2pos(TilePosition(100, 6)));
		marinePosition.push_back(tile2pos(TilePosition(101, 6)));
		marinePosition.push_back(tile2pos(TilePosition(103, 8)));
		marinePosition.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
		marinePosition.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));
		break;

	case 1:
		marinePosition.push_back(tile2pos(TilePosition(121, 99)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 100)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(118, 102)) + Position(16, 16));
		break;

	case 2:
		marinePosition.push_back(tile2pos(TilePosition(7, 26)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(11, 26)));
		marinePosition.push_back(tile2pos(TilePosition(14, 27)) + Position(0, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 0));
		break;

	case 3:
		marinePosition.push_back(tile2pos(TilePosition(23, 117)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(24, 121)));
		marinePosition.push_back(tile2pos(TilePosition(27, 123)));
		marinePosition.push_back(tile2pos(TilePosition(28, 123)));
		marinePosition.push_back(tile2pos(TilePosition(31, 121)) + Position(16, 16));
		break;
	}
}

void SimCity2Strategy::run(){

	
	DragoonDetect();
	marineHold();

	if (cmd->getStateMgr().getCombatFlag(COMBAT_FLAG::DRAGOON_RUSH)){
		detectDragoon();
	}
}

void SimCity2Strategy::scvControl(SCV* scv){

	switch (scv->getState()){
	case SCV::STATE::REPAIR:
		for (auto building : bw->self()->getUnits()){
			if (building->getType() == UnitTypes::Terran_Barracks || building->getType() == UnitTypes::Terran_Supply_Depot){
				if (building->getHitPoints() < building->getType().maxHitPoints()){
					scv->getSelf()->repair(building);
					break;
				}
				else{
					if (scv->getSelf()->isCarryingMinerals()){
						scv->getSelf()->returnCargo(true);
					}
					else if (scv->getSelf()->isIdle() || scv->getSelf()->isGatheringGas()){
						scv->getSelf()->gather(scv->getTarget());
					}
				}
			}
		}
		break;
	}
}

void SimCity2Strategy::MarineControl(Marine* marine)
{
	if (marine == nullptr){
		return;
	}
	Unit t_marine = marine->getSelf();
	Unitset eunits = t_marine->getUnitsInRadius(bw->self()->sightRange(UnitTypes::Terran_Barracks), Filter::IsEnemy);


	switch (marine->getState()){

	case Marine::STATE::MOVE:
		t_marine->move(mapInfo->getMyStartBase()->getPosition());
		break;

	case Marine::STATE::HOLD:
		if (isAlmostReachedDest(t_marine->getPosition(), holdMarine[marine], 2, 2)){
			if (!t_marine->isHoldingPosition()){
				t_marine->holdPosition();
			}
		}
		else{
			t_marine->move(holdMarine[marine]);
		}
		break;
	}
}

//
void SimCity2Strategy::detectDragoon(){
	//드라군 공격인 경우
	bool dragoon = isInvasion();
	
	if (dragoon){
		// 발생했을 경우 2마리를 작업용으로 빼온다.
		if (guardSCV.empty()){
			for (int i = 0; i < 2; i++){
				SCV* scv = tMgr->getAllTerritorys()[0]->getWorker(true);
				guardSCV.push_back(scv);
			}
		}

		// 2마리가 위치가 할당이 안되있을 경우 할당해준다.
		for (int i = 0; i < 2; i++){
			SCV* scv = guardSCV[i];
			scv->setState(SCV::STATE::REPAIR);
		}
		
		for (auto marine : guardmarine){
			if (marine->getSelf()->exists() || marine != nullptr){
				
				if (0< marine->getSelf()->getHitPoints() && marine->getSelf()->getHitPoints() <= marine->getSelf()->getType().maxHitPoints() / 2 + 5){
					for (Unit my : bw->self()->getUnits())
					{
						if (my->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || my->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode)
						{
							marine->setState(Marine::STATE::HOLD);
							break;
						}
						else{
							marine->setState(Marine::STATE::MOVE);
						}
					}
					
				}
			}
		}
	}

	// SCV 의 행동을 수행
	for (auto scv : guardSCV){
		scvControl(scv);
	}
	for (auto marine : guardmarine){
		if (marine->getSelf()->exists()){
			MarineControl(marine);
		}
	}
}

void SimCity2Strategy::marineHold(){

	//마린 위치 잡기
	Marine* t_marine = getIdleMarine();
	if (t_marine != nullptr && guardmarine.size() <= 5){
		guardmarine.push_back(t_marine);
	}

	//마린이 고정할 위치를 지정
	if (!guardmarine.empty()){
		for (int i = 0; i < guardmarine.size(); i++){
			if (i < 5){
				if (guardmarine[i]->getSelf()->exists()){
					holdMarine[guardmarine[i]] = marinePosition[i];
					guardmarine[i]->setState(Marine::STATE::HOLD);
				}
			}
			else{
				break;
			}
		}
	}
	for (auto m : guardmarine){
		if (m->getSelf()->exists()){
			MarineControl(m);
		}
	}
}

// FLAG FUNC
bool SimCity2Strategy::DragoonDetect(){
	auto startBase = mapInfo->getMyStartBase();

	for (auto unit : bw->enemy()->getUnits()){
		if (unit->getType() == UnitTypes::Protoss_Dragoon){
			cmd->getStateMgr().setCombatFlag(COMBAT_FLAG::DRAGOON_RUSH, true);
			return true;
		}
	}

	return false;
}

bool SimCity2Strategy::isInvasion(){
	for (auto dragon : bw->enemy()->getUnits()){
		if (dragon->getType() == UnitTypes::Protoss_Dragoon){
			if (isAlmostReachedDest(BWTA::getNearestChokepoint(mapInfo->getMyStartBase()->getPosition())->getCenter(), dragon->getPosition(), 360, 360)){
				Dragoon = dragon;
				return true;
			}
		}
	}
	return false;
}


void SimCity2Strategy::end(){
	for (auto scv : guardSCV){
		if (scv != nullptr && scv->getSelf()->exists()){
			scv->setState(SCV::STATE::RETURN);
		}
	}
	for (auto marine : guardmarine){
		if (marine != nullptr && marine->getSelf()->exists()){
			marine->setState(Marine::STATE::IDLE);
		}
	}
	for (auto scv : guardSCV){
		scvControl(scv);
	}
	for (auto marine : guardmarine){
		if (marine->getSelf()->exists()){
			MarineControl(marine);
		}
	}
}