#pragma once
#include "Work.h"
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"

class UpSimCity_2
{
	enum FLAG{
		GAS_CONTROL_1,
		GAS_CONTROL_2,

		PREPARE_EXPAND_MULTI2,
		LIFT_CENTER2,
		EXPAND_MULTI2,
		SCV_MOVE2,

		UPGRADE_SIEGE_MODE,
		UPGRADE_MINE,
		UPGRADE_VEHICLE_WEAPONS,

		OPEN_FRONT,

		ENTRANCE_OPEN,
		ENTRANCE_CLOSE,
		
		NO_SCV,
	};

	TerritoryMgr* tMgr;
	map<int, bool> flags;
	list<scheduleWork> build;

	SCV* out;
	int frameCount;
	bool safe;

	queue<TilePosition> cntPoses;
	queue<TilePosition> turretPoses;
	
	vector<SCV*> splySCV;	
	vector<Structure*> moveBuilding;
	vector<Structure*> landBuilding;
	
	TilePosition front_brkPos;
	TilePosition front_splyPos;
	TilePosition front_trtPos;
	TilePosition enginePos;
	TilePosition outPos;
	
	// DEFENCE 
	vector<TilePosition> siegePoses;
	map<TilePosition, Tank*> holdTanks;
	vector<Tank*> subTanks; // 3번째 확장에 배치될 애들.

public:
	UpSimCity_2();
	~UpSimCity_2();

	void init();
	void run(COMBAT_FLAG combat);

	void move();
	void movePos();

	void errorSCV();
	bool chkEnd();

	TilePosition getPosition(queue<TilePosition>& poses);

	// DEFENCE
	void tankDefence();
	Tank* getIdleTank();

};

