#pragma once

#define MAP_SIZE 128
#define PIXEL 3

#define SELF       1
#define ENEMY      2
#define NEUTRALITY 3

#define VISION_INVISIBLE 0
#define VISION_CLEAR     1
#define VISION_DIM       2
#define VISION_VERY_DIM  3

#define EXPAND_WIDTH 32
#define EXPAND_HEIGHT 32

#define EXPAND_RATE MAP_SIZE / EXPAND_WIDTH

#define EXPAND_END_WIDTH MAP_SIZE - (EXPAND_WIDTH + 1)
#define EXPAND_END_HEIGHT MAP_SIZE - (EXPAND_HEIGHT + 1)

struct Data {
	bool end;
	bool expand;

	int screenX;
	int screenY;

	int vision[MAP_SIZE][MAP_SIZE];
	int players[MAP_SIZE][MAP_SIZE];
	int unitType[MAP_SIZE][MAP_SIZE];
	int progress[MAP_SIZE][MAP_SIZE];
};