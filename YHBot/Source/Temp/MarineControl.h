#pragma once
#include <BWAPI.h>
#include "MyUnit.h"

using namespace BWAPI;

class HMarineControl {

private:
	bool flag_Physical = false;
	void kitingMove(HMarine*);
public:
	Position bionicIM[256][256];
	int bionicIMvalue[256][256];
	int bionicIMvalueBackup[256][256];

	HMarineControl();
	~HMarineControl();
	static HMarineControl* create();
	static HMarineControl* getInstance();
	static HMarineControl* s_HMarineControl;
	void run(HMarine*);
	void onFrame();

	//control func
	void WAIT_Control(HMarine* unit);
	void DEFENCE_Control(HMarine* unit);
	void RETURN_Control(HMarine* unit);
	void MOVE_Control(HMarine* unit);
	void CRAZY_Control(HMarine* unit);
	void ATTACK_Control(HMarine* unit);
	void BC_ATTACK_Control(HMarine* unit);

	Unit getTargetUnit(HMarine* unit);
	float getTargetUnitValue(UnitType ut);
	Unit getUnitforID(int id);

	//other func
	void goodLuck(HMarine *unit);
	void drawHMarineControlUnit(HMarine* unit);
	void drawHMarineControl();
	void updateBIM();

	Position attackPosition;
	bool twoFactoryScene = true;
};


//
//#pragma once
//#include <BWAPI.h>
//#include "MyUnit.h"
//
//
//
//using namespace BWAPI;
//using namespace std;
//
//class MarineControl {
//
//private:
//	int MarineIM[128][128];
//public:
//
//	MarineControl();
//	~MarineControl();
//	static MarineControl* create();
//	static MarineControl* getInstance();
//	static MarineControl* s_MarineControl;
//	void run(Marine*);
//	void onFrame();
//
//	//control func
//	void WAIT_Control(Marine* unit);
//	void ATTACK_Control(Marine* unit);
//	void RETURN_Control(Marine* unit);
//	void MOVE_Control(Marine* unit);
//	void CRAZY_Control(Marine* unit);
//	void PROTECT_Control(Marine* unit);
//
//	Unit getTargetUnit(Unit unit);
//	Unit getUnitforID(int id);
//
//	int getUnitGroundAttackRadius(Unit);
//	int getUnitAirttackRadius(Unit);
//	Unit getTargetToHelpFriend(Unit);
//	vector<TilePosition> getNearTilePositions(TilePosition);
//
//	int getAngleTwoPoints(Position, Position);
//	int getAngleArea(int);
//
//	void MarineControl::drawMarineControlUnit(Marine*);
//	//other func
//	void goodLuck(Marine *unit);
//	void kitingMove(Marine* unit);
//
//	void updateIM();
//
//	Unitset getSafeUnitsInSeekRange(Unit);
//	Unitset getDangerUnitsInSeekRange(Unit);
//
//	int getWorstAreaScore(Marine *unit);
//
//	Position getBestPosition(Unit);
//
//	map<UnitType, int> pt_weight;
//	map<UnitType, int> tr_weight;
//
//	Position attackPosition;
//
//	Unit getClosestTankToSave(Unit);
//};