#pragma once
#include "Work.h"
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"

class SimCity_1
{
	enum Flag {
		spy,

		ENTRANCE_OPEN,
		ENTRANCE_CLOSE
	};

	vector<scheduleWork> build;
	TerritoryMgr* tMgr;

	map<int, bool> flags;
	map<int, int> strategyFlag;

	SCV* builder;
	SCV* out;
	TilePosition outPos;

	int frameCount;

public:
	SimCity_1();
	~SimCity_1();

	void chkStrategy();
	void run(COMBAT_FLAG combat);
	void errorSCV();

	bool chkEnd();
};

