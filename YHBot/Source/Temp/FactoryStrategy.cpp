#include "FactoryStrategy.h"
#include "../Single.h"
#include "Scout.h"
#include "UtilMgr.h"

FactoryStrategy::FactoryStrategy(){

	frameCount = 0;
	marinePosition.clear();
	tankPosition.clear();
	vulturePosition.clear();

	if (mapInfo->getDir() == 0) {
		marinePosition.push_back(tile2pos(TilePosition(100, 6)));
		marinePosition.push_back(tile2pos(TilePosition(101, 6)));
		marinePosition.push_back(tile2pos(TilePosition(103, 8)));
		marinePosition.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
		marinePosition.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));

		tankPosition.push_back(Position(3387, 409));
		tankPosition.push_back(Position(3343, 262)); 
		tankPosition.push_back(Position(3343, 517));
		tankPosition.push_back(Position(3292, 194));

		vulturePosition.push_back(Position(3338, 219));
		vulturePosition.push_back(Position(3399, 296));
		vulturePosition.push_back(Position(3434, 362));
		vulturePosition.push_back(Position(3263, 563));
	}
	else if (mapInfo->getDir() == 1) {
		marinePosition.push_back(tile2pos(TilePosition(121, 99)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 100)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(118, 102)) + Position(16, 16));

		tankPosition.push_back(Position(3629, 3259));
		tankPosition.push_back(Position(3486, 3232));
		tankPosition.push_back(Position(3869, 3259));
		tankPosition.push_back(Position(3928, 3201));

		vulturePosition.push_back(Position(3580, 3304));
		vulturePosition.push_back(Position(3791, 3320));
		vulturePosition.push_back(Position(3953, 3085));
		vulturePosition.push_back(Position(3959, 3141));
	}
	else if (mapInfo->getDir() == 2) {
		marinePosition.push_back(tile2pos(TilePosition(7, 26)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(11, 26)));
		marinePosition.push_back(tile2pos(TilePosition(14, 27)) + Position(0, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 0));

		tankPosition.push_back(Position(187, 838));
		tankPosition.push_back(Position(515, 883));
		tankPosition.push_back(Position(290, 797));
		tankPosition.push_back(Position(398, 799));

		vulturePosition.push_back(Position(545, 896));
		vulturePosition.push_back(Position(351, 798));
		vulturePosition.push_back(Position(114, 971));
		vulturePosition.push_back(Position(127, 852));
	}
	else if (mapInfo->getDir() == 3) {
		marinePosition.push_back(tile2pos(TilePosition(23, 117)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(24, 121)));
		marinePosition.push_back(tile2pos(TilePosition(27, 123)));
		marinePosition.push_back(tile2pos(TilePosition(28, 123)));
		marinePosition.push_back(tile2pos(TilePosition(31, 121)) + Position(16, 16));

		tankPosition.push_back(Position(719, 3871));
		tankPosition.push_back(Position(743, 3635));
		tankPosition.push_back(Position(838, 3979));
		tankPosition.push_back(Position(1050, 3944));

		vulturePosition.push_back(Position(700, 3802));
		vulturePosition.push_back(Position(969, 3965));
		vulturePosition.push_back(Position(765, 3961));
		vulturePosition.push_back(Position(658, 3880));
	}
}

FactoryStrategy::~FactoryStrategy(){

}

void FactoryStrategy::run(COMBAT_FLAG combat){

	scvrepair();

	if (combat == COMBAT_FLAG::NORMAL){
		// 특정 위치에서 대기.
		normal();
	}
	else if (combat == COMBAT_FLAG::ZEALOT_RUSH){
		
		// 입구를 좀 늦게 열고 탱크로 요격.
		guardZealotRush();		
	}
	else if (combat == COMBAT_FLAG::DRAGOON_RUSH){

		// 입구를 좀 늦게 열고 탱크로 요격. SCV가 두마리 건물 회복.
		// 일단 발생할 경우 없음.
		guardDragoonRush();
	}
	else if (combat == COMBAT_FLAG::ATTACK){

		attack();
	}
}

void FactoryStrategy::scvrepair()
{
	Unit build = nullptr;

	for (auto building : bw->self()->getUnits())
	{
		if ((building->getType() == UnitTypes::Terran_Barracks || building->getType() == UnitTypes::Terran_Supply_Depot) && isAlmostReachedDest(building->getPosition(), BWTA::getNearestChokepoint(mapInfo->getMyStartBase()->getPosition())->getCenter(), 250, 250))
		{
			int forbuildhp = building->getType().maxHitPoints() - building->getHitPoints();
			if (building->getHitPoints() <= building->getType().maxHitPoints() * 0.6)
			{
				build = building;
				break;
			}
		}
	}

	if (build != nullptr)
	{
		if (guardSCV.empty())
		{
			for (int i = 0; i < 2; i++)
			{
				SCV* scv = cmd->getTerritoryMgr().getAllTerritorys()[0]->getWorker(true);
				if (scv != nullptr)
				{
					guardSCV.push_back(scv);
				}
			}
		}
		for (auto scv : guardSCV)
		{
			if (scv->getSelf()->exists())
			{
				scv->getSelf()->repair(build);
			}
		}
	}
	else{
		if (!guardSCV.empty())
		{
			for (auto scv : guardSCV)
			{
				if (scv->getSelf()->exists())
				{
					scv->getSelf()->gather(scv->getTarget());
				}
			}
		}
	}

	for (auto scv = guardSCV.begin(); scv != guardSCV.end();)
	{
		if (!(*scv)->getSelf()->exists())
		{
			scv = guardSCV.erase(scv);
		}
		else
		{
			scv++;
		}
	}
}

void FactoryStrategy::normal(){
	for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
		if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
			if (mIndex < marinePosition.size()) {
				(*q)->rPos = marinePosition[mIndex++];
			}
			else {
				(*q)->rPos = marinePosition[0];
			}
		}
	}
	for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
		if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
			if (tIndex < tankPosition.size()) {
				(*q)->rPos = tankPosition[tIndex++];
			}
			else {
				(*q)->rPos = tankPosition[0];
			}
		}
	}
	for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
		if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
			if (vIndex < vulturePosition.size()) {
				(*q)->rPos = vulturePosition[vIndex++];
			}
			else {
				(*q)->rPos = vulturePosition[0];
			}
		}
	}

	if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
		float range = 4;
		if (UnitControl::getInstance()->marines.size() > 5) range = 30;
		for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
			(*q)->getSelf()->move((*q)->rPos, false);
			float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
			if (dist < range) {
				if ((*q)->getSelf()->isHoldingPosition() == false)
					(*q)->getSelf()->holdPosition();
			}
		}

		if (UnitControl::getInstance()->tanks.size() > 4) range = 30;
		else range = 4;
		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
			if (dist < range) {
				if ((*q)->getSelf()->isHoldingPosition() == false)
					(*q)->getSelf()->holdPosition();
				if ((*q)->getSelf()->isSieged() == false) {
					if ((*q)->getSelf()->canSiege()) {
						(*q)->getSelf()->siege();
					}
				}
			}
			else {
				(*q)->getSelf()->move((*q)->rPos, false);
			}
		}

		if (UnitControl::getInstance()->vultures.size() > 4) range = 30;
		else range = 4;
		for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
			(*q)->getSelf()->move((*q)->rPos, false);
			float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
			if (dist < range) {
				if ((*q)->getSelf()->isHoldingPosition() == false)
					(*q)->getSelf()->holdPosition();
			}
		}
	}
}

void FactoryStrategy::guardZealotRush(){
	
	auto state = cmd->getStateMgr();

	if (state.getCombatFlag(COMBAT_FLAG::ATTACK) == false){
		// 입구 닫혀있을 거임.
		// 때릴수 있는 애들을 때려.
		for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
			if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
				if (mIndex < marinePosition.size()) {
					(*q)->rPos = marinePosition[mIndex++];
				}
				else {
					(*q)->rPos = marinePosition[0];
				}
			}
		}
		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
				if (tIndex < tankPosition.size()) {
					(*q)->rPos = tankPosition[tIndex++];
				}
				else {
					(*q)->rPos = tankPosition[0];
				}
			}
		}
		for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
			if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
				if (vIndex < vulturePosition.size()) {
					(*q)->rPos = vulturePosition[vIndex++];
				}
				else {
					(*q)->rPos = vulturePosition[0];
				}
			}
		}

		if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
			float range = 4;
			if (UnitControl::getInstance()->marines.size() > 5) range = 30;
			for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
				(*q)->getSelf()->move((*q)->rPos, false);
				float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
				if (dist < range) {
					if ((*q)->getSelf()->isHoldingPosition() == false)
						(*q)->getSelf()->holdPosition();
				}
			}

			if (UnitControl::getInstance()->tanks.size() > 4) range = 30;
			else range = 4;
			for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
				float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
				if (dist < range) {
					if ((*q)->getSelf()->isHoldingPosition() == false)
						(*q)->getSelf()->holdPosition();
					if ((*q)->getSelf()->isSieged() == false) {
						if ((*q)->getSelf()->canSiege()) {
							(*q)->getSelf()->siege();
						}
					}
				}
				else {
					(*q)->getSelf()->move((*q)->rPos, false);
				}
			}

			if (UnitControl::getInstance()->vultures.size() > 4) range = 30;
			else range = 4;
			for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
				(*q)->getSelf()->move((*q)->rPos, false);
				float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
				if (dist < range) {
					if ((*q)->getSelf()->isHoldingPosition() == false)
						(*q)->getSelf()->holdPosition();
				}
			}
		}
	}
}

void FactoryStrategy::guardDragoonRush(){
	auto state = cmd->getStateMgr();

	if (state.getCombatFlag(COMBAT_FLAG::ATTACK) == false){
		// 입구 닫혀있을 거임.
		// 때릴수 있는 애들을 때려.
		for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
			if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
				if (mIndex < marinePosition.size()) {
					(*q)->rPos = marinePosition[mIndex++];
				}
				else {
					(*q)->rPos = marinePosition[0];
				}
			}
		}
		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
				if (tIndex < tankPosition.size()) {
					(*q)->rPos = tankPosition[tIndex++];
				}
				else {
					(*q)->rPos = tankPosition[0];
				}
			}
		}
		for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
			if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
				if (vIndex < vulturePosition.size()) {
					(*q)->rPos = vulturePosition[vIndex++];
				}
				else {
					(*q)->rPos = vulturePosition[0];
				}
			}
		}

		if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
			float range = 4;
			if (UnitControl::getInstance()->marines.size() > 5) range = 30;
			for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
				(*q)->getSelf()->move((*q)->rPos, false);
				float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
				if (dist < range) {
					if ((*q)->getSelf()->isHoldingPosition() == false)
						(*q)->getSelf()->holdPosition();
				}
			}

			if (UnitControl::getInstance()->tanks.size() > 4) range = 30;
			else range = 4;
			for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
				(*q)->getSelf()->move((*q)->rPos, false);
				float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
				if (dist < range) {
					if ((*q)->getSelf()->isHoldingPosition() == false)
						(*q)->getSelf()->holdPosition();
					if ((*q)->getSelf()->isSieged() == false) {
						if ((*q)->getSelf()->canSiege()) {
							(*q)->getSelf()->siege();
						}
					}
				}
				else {
					(*q)->getSelf()->move((*q)->rPos, false);
				}
			}

			if (UnitControl::getInstance()->vultures.size() > 4) range = 30;
			else range = 4;
			for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
				(*q)->getSelf()->move((*q)->rPos, false);
				float dist = getDistance((*q)->getSelf()->getPosition(), (*q)->rPos);
				if (dist < range) {
					if ((*q)->getSelf()->isHoldingPosition() == false)
						(*q)->getSelf()->holdPosition();
				}
			}
		}
	}
}

void FactoryStrategy::attack(){
	// 가는즁
	Unitset nearUnits = UnitControl::getInstance()->getNearEnemyUnits();
	if (!flags[ATTACK_READY]) {

		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			
			if ((*q)->getSelf()->isSieged()) {
				(*q)->getSelf()->unsiege();
			}
		}
		teamAttackPosition.clear();
		teamAttackPosition.push_back(CombatInfo::getInstance()->getAttackPosition());
		flags[ATTACK_READY] = true;
	}
	if (!flags[ENEMY_SETTING]) {
		if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
			if (CombatInfo::getInstance()->getEnemyDir() == 0) {
				nextPosition = Position(2737, 924);
			}
			else if (CombatInfo::getInstance()->getEnemyDir() == 1) {
				nextPosition = Position(3075, 2732);
			}
			else if (CombatInfo::getInstance()->getEnemyDir() == 2) {
				nextPosition = Position(1029, 1375);
			}
			else if (CombatInfo::getInstance()->getEnemyDir() == 3) {
				nextPosition = Position(1334, 3195);
			}

			flags[ENEMY_SETTING] = true;
		}
	}
	if (!flags[WE_ARE_THE_ONE]) {
		if (flags[ENEMY_SETTING]) {
			float dist = getDistance(nextPosition, CombatInfo::getInstance()->myUnitPos);
			if (dist < 240) {
				for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
					float pdist = getDistance((*q)->getSelf()->getPosition(), teamAttackPosition[(*q)->team]);
					if (pdist > 200) {
						(*q)->isJunjin = true;
						(*q)->isInTeam = false;
					}
					else {
						(*q)->isInTeam = true;
					}
				}
				teamHongHong[0] = true;
				flags[WE_ARE_THE_ONE] = true;
				frameCount = 0;
			}
		}
	}
	if (!flags[WE_ARE_THE_ONE]){
		int zc = 0;
		for (auto n = nearUnits.begin(); n != nearUnits.end(); n++) {
			if((*n) == NULL) continue;
			if ((*n)->isDetected() == false) continue;
			if ((*n)->exists() == false) continue;
			if ((*n)->getType() == UnitTypes::Protoss_Zealot) {
				zc++;
			}
			else {
				zc--;
			}
		}
		teamAttackPosition[0] = CombatInfo::getInstance()->getFloydPosition2(CombatInfo::getInstance()->myUnitPos, nextPosition);
		if (zc > 0) {
			for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
				(*q)->rPos = teamAttackPosition[(*q)->team];
				(*q)->setState(HMarine::STATE::CRAZY);
				(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
				HMarineControl::getInstance()->run((*q));
			}
			for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
				(*q)->rPos = teamAttackPosition[(*q)->team];
				(*q)->setState(Tank::STATE::CRAZY);
				(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
				TankControl::getInstance()->run((*q));
			}
			for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
				(*q)->rPos = teamAttackPosition[(*q)->team];
				(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
				(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
				VultureControl::getInstance()->run((*q), true);
			}
		}
		else {
			for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
				(*q)->rPos = teamAttackPosition[(*q)->team];
				(*q)->setState(HMarine::STATE::CRAZY);
				(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
				HMarineControl::getInstance()->run((*q));
			}
			for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
				(*q)->rPos = teamAttackPosition[(*q)->team];
				(*q)->setState(Tank::STATE::CRAZY);
				(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
				TankControl::getInstance()->run((*q));
			}
			for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
				(*q)->rPos = teamAttackPosition[(*q)->team];
				(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
				(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
				VultureControl::getInstance()->run((*q), false);
			}
		}
	}
	// 적 앞 초크포인트 도착
	else if (flags[WE_ARE_THE_ONE]){
		frameCount++;
		// 조이기 정보 업데이트 해줍니다.
		// 팀별로 주위를 살핍니다.
		//프레임이 길었던 친구가 안전하면 앞으로 땡겨여
		int teamIndex = 0;


		//시즈 박고 있어여
		if (teamHongHong[teamIndex]) {
			// 주변에 공격 유닛이 없고, 때릴 게 없어여
			if (frameCount >= 24 * 6) {
				int count = 0;
				for (auto t = UnitControl::getInstance()->tanks.begin(); t != UnitControl::getInstance()->tanks.end(); t++) {
					if ((*t)->isInTeam == false) continue;
					for (auto q = BWAPI::Broodwar->enemy()->getUnits().begin(); q != BWAPI::Broodwar->enemy()->getUnits().end(); q++) {
						if ((*t)->getSelf()->isInWeaponRange((*q))) {
							count++;
						}
					}
				}
				if (count == 0) {
					teamHongHong[teamIndex] = false;
				}
			}
		}
		// 안박고 있어여
		else {
			float count = 0;
			for (auto t = UnitControl::getInstance()->tanks.begin(); t != UnitControl::getInstance()->tanks.end(); t++) {
				if ((*t)->isInTeam == false) continue;
				for (auto q = BWAPI::Broodwar->enemy()->getUnits().begin(); q != BWAPI::Broodwar->enemy()->getUnits().end(); q++) {
					
					if ((*t)->getSelf()->isInWeaponRange((*q))) {
						if ((*q)->getType().isBuilding() && (*q)->getType() != UnitTypes::Protoss_Photon_Cannon)
							count += 0.34f;
						else
							count += 1;
					}
				}
			}
			if (count >= 1) {
				teamHongHong[teamIndex] = true;
				frameCount = 0;
			}
		}
		
		int mineScore = 0;
		for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
			if ((*q).second.first == UnitTypes::Protoss_Photon_Cannon) {
				mineScore = 100;
				break;
			}
		}
		for (auto q = nearUnits.begin(); q != nearUnits.end(); q++) {
			if ((*q) == NULL) continue;
			if ((*q)->getType() == UnitTypes::Protoss_Photon_Cannon) {
				mineScore += 100;
				break;
			}
			else if ((*q)->getType().isBuilding()) {

			}
			else if((*q)->getType().isFlyer()){

			}
			else {
				mineScore += 1;
			}
		}

		
		teamAttackPosition[0] = CombatInfo::getInstance()->getAttackBase()->getPosition();
		for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
			(*q)->rPos = teamAttackPosition[(*q)->team];
			(*q)->setState(HMarine::STATE::CRAZY);
			(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
			HMarineControl::getInstance()->run((*q));
		}
		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			float pdist = getDistance((*q)->getSelf()->getPosition(), CombatInfo::getInstance()->myUnitPos);
			if (pdist > 200) {
				(*q)->isInTeam = false;
			}
			else {
				(*q)->isInTeam = true;
				//(*q)->rPos = getTankPos;
			}
			(*q)->rPos = teamAttackPosition[(*q)->team];
			// 탱크가 멀리 있습니다. 
			if (!(*q)->isInTeam) {
				// 그러면 빨리 텨오세요
				(*q)->setState(Tank::STATE::CRAZY);
				(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
				TankControl::getInstance()->run((*q));
				// 왔는지 확인할 게요
				
			}
			//탱크가 군집에 있습니다.
			else {
				// 지금 모두 시즈를 박으라 캅니다.
				if (teamHongHong[(*q)->team]) {
					// 시즈 안하고 노는 놈들은
					if ((*q)->getSelf()->isSieged() == false) {
						// 혼내줍니다.
						if (getDistance((*q)->getSelf()->getPosition(), BWTA::getNearestChokepoint((*q)->getSelf()->getPosition())->getCenter()) < 64) {
							if (BWAPI::Broodwar->getFrameCount() % 18 == 0)
								(*q)->getSelf()->move(teamAttackPosition[(*q)->team], false);
						}
						if ((*q)->getSelf()->canSiege()) {
							(*q)->getSelf()->siege();
						}
					}
					// 시즈 하고 있는 놈들은
					else {
						// 공격할 수 있으면 공격하시는데
						if ((*q)->getSelf()->getSpellCooldown() == 0) {
							(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
							// 공격할 놈이 있으면 떄리세여
							if((*q)->attackTarget != NULL)
								(*q)->getSelf()->attack((*q)->attackTarget, false);
						}
					}
				}
				// 전진 하시라 캅니다.
				else {
					// 전진 할 친구들은
					// 아닌 놈들 중에
					// 시즈 안하고 노는 놈들은
					if ((*q)->getSelf()->isSieged()) {
						(*q)->getSelf()->unsiege();
					}
					// 시즈 하고 있는 놈들은
					else {
						if (BWAPI::Broodwar->getFrameCount() % 24 == 0) {
							(*q)->getSelf()->attack(teamAttackPosition[(*q)->team], false);
						}
					}
				}

			}
			
		}
		int pCount = 0;
		for (auto q = nearUnits.begin(); q != nearUnits.end(); q++) {
			if ((*q) == NULL) continue;
			if ((*q)->getType() == UnitTypes::Protoss_Photon_Cannon) {
				pCount++;
			}
		}

		bool isBrave = true;
		if (pCount > UnitControl::getInstance()->vultures.size() * 6) {
			isBrave = false;
		}

		int zc = 0;
		for (auto n = nearUnits.begin(); n != nearUnits.end(); n++) {
			if ((*n) == NULL) continue;
			if ((*n)->isDetected() == false) continue;
			if ((*n)->exists() == false) continue;
			if ((*n)->getType() == UnitTypes::Protoss_Zealot) {
				zc++;
			}
			else {
				zc--;
			}
		}

		for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
			if ((*q)->isMining) {
				if (mineScore > 3) {
					(*q)->isMining = false;
					(*q)->minePos = Position(0, 0);
				}
				else {
					(*q)->setState(VultureUnit::STATE::KJ_MINE);
					VultureControl::getInstance()->run((*q), false);
				}
			}
			else {
				if (mineScore < 4) {
					if ((*q)->getSelf()->getSpiderMineCount() > 0) {
						Position p = UnitControl::getInstance()->getKJMine((*q));
						if (p.x != -1) {
							(*q)->minePos = p;
							(*q)->setState(VultureUnit::STATE::KJ_MINE);
							VultureControl::getInstance()->run((*q), false);
						}
						else {
							(*q)->rPos = teamAttackPosition[(*q)->team];
							(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
							(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
							if (zc > 0)
								VultureControl::getInstance()->run((*q), true);
							else
								VultureControl::getInstance()->run((*q), false);
						}
					}
					else {
						(*q)->rPos = teamAttackPosition[(*q)->team];
						(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
						(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
						if (zc > 0)
							VultureControl::getInstance()->run((*q), true);
						else
							VultureControl::getInstance()->run((*q), false);
					}
				}
				else {
					(*q)->rPos = teamAttackPosition[(*q)->team];
					(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
					(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
					if (zc > 0)
						VultureControl::getInstance()->run((*q), true);
					else
						VultureControl::getInstance()->run((*q), false);
				}
			}
			//(*q)->attackTarget = UnitControl::getInstance()->getAttackUnit(nearUnits, (*q));
			//(*q)->rPos = teamAttackPosition[(*q)->team];
			////시즈 박고 있을때
			//if (teamHongHong[(*q)->team]) {
			//	//쿨다운일때
			//	if ((*q)->getSelf()->getSpellCooldown() == 0) {
			//		if ((*q)->attackTarget != NULL) {
			//			(*q)->getSelf()->attack((*q)->attackTarget, false);
			//		}
			//		else {
			//			(*q)->getSelf()->attack((*q)->rPos, false);
			//		}
			//	}
			//	//쿨다운아닐때
			//	else {
			//		float dist = getDistance((*q)->getSelf()->getPosition(), VultureControl::getInstance()->getNearestTankPos((*q)));
			//		if (dist < 250) {
			//			if (BWAPI::Broodwar->getFrameCount() % 12 == 0)
			//				(*q)->getSelf()->holdPosition();
			//		}
			//		else {
			//			(*q)->rPos = VultureControl::getInstance()->getNearestTankPos((*q));
			//			(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
			//			VultureControl::getInstance()->run((*q), false);
			//		}
			//	}
			//}
			////시즈 ㄴㄴ일때
			//else {
			//	//공격
			//	if ((*q)->getSelf()->getSpellCooldown() == 0) {
			//		if ((*q)->attackTarget != NULL) {
			//			(*q)->getSelf()->attack((*q)->attackTarget, false);
			//		}
			//		else {
			//			(*q)->getSelf()->attack((*q)->rPos, false);
			//		}
			//	}
			//	//공격ㄴㄴ
			//	else {
			//		float dist = getDistance((*q)->getSelf()->getPosition(), VultureControl::getInstance()->getNearestTankPos((*q)));
			//		if (dist < 250) {
			//			if (BWAPI::Broodwar->getFrameCount() % 12 == 0)
			//				(*q)->getSelf()->holdPosition();
			//		}
			//		else {
			//			(*q)->rPos = VultureControl::getInstance()->getNearestTankPos((*q));
			//			(*q)->setState(VultureUnit::STATE::TF_JUNJIN);
			//			VultureControl::getInstance()->run((*q), false);
			//		}
			//	}
			//}
		}
	}
}

void FactoryStrategy::end(){

	// 자료구조 초기화 및 SCV를 TRUE로 가져왔을 경우 addSCV 해줄 것.
}

void FactoryStrategy::draw(COMBAT_FLAG combat) {
	
	
	if (combat == COMBAT_FLAG::NORMAL) {
		// 특정 위치에서 대기.
		for (int x = 0; x < marinePosition.size(); x++) {
			BWAPI::Broodwar->drawCircleMap(marinePosition[x], 8, Colors::Blue, false);
		}
		for (int x = 0; x < tankPosition.size(); x++) {
			BWAPI::Broodwar->drawCircleMap(tankPosition[x], 10, Colors::Brown, false);
		}
		for (int x = 0; x < vulturePosition.size(); x++) {
			BWAPI::Broodwar->drawCircleMap(vulturePosition[x], 10, Colors::Orange, false);
		}
	}
	else if (combat == COMBAT_FLAG::ZEALOT_RUSH) {

		// 입구를 좀 늦게 열고 탱크로 요격.
	}
	else if (combat == COMBAT_FLAG::DRAGOON_RUSH) {

		// 입구를 좀 늦게 열고 탱크로 요격. SCV가 두마리 건물 회복.
		// 일단 발생할 경우 없음.
	}
	else if (combat == COMBAT_FLAG::ATTACK) {
		if (flags[WE_ARE_THE_ONE]) {
			if (teamHongHong[0]) {
				for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
					BWAPI::Broodwar->drawCircleMap((*q)->getSelf()->getPosition(), 12, Colors::Orange, false);
				}
			}
			else {
				for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
					BWAPI::Broodwar->drawCircleMap((*q)->getSelf()->getPosition(), 12, Colors::Red, false);
				}
			}

		}
		if (flags[ENEMY_SETTING]) {
			for (int x = 0; x < teamAttackPosition.size(); x++) {
				if (BWAPI::Broodwar->getFrameCount() % 8 < 4) {
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 5, Colors::Red, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 8, Colors::Orange, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 11, Colors::Red, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 14, Colors::Orange, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 17, Colors::Red, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 20, Colors::Orange, false);
				}
				else {
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 5, Colors::Orange, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 8, Colors::Yellow, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 11, Colors::Orange, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 14, Colors::Yellow, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 17, Colors::Orange, false);
					BWAPI::Broodwar->drawCircleMap(teamAttackPosition[x], 20, Colors::Yellow, false);
				}
			}
		}
	}
}