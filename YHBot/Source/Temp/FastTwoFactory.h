#pragma once
#include "Work.h"
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"

class FastTwoFactory
{
	enum Flag{
		SPY,

		UPGRADE_ION,
		UPGRADE_MINE,

		ATTACK,
	};

	vector<scheduleWork> build;	
	TerritoryMgr* tMgr;

	map<int, bool> flags;

	SCV* out;
	int frameCount;

public:
	FastTwoFactory();
	~FastTwoFactory();

	void run(COMBAT_FLAG combat);
};

