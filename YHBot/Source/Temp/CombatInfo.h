#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <vector>
#include <stdio.h>
#include <list>
#include <map>

using namespace std;
using namespace BWAPI;
using namespace Filter;

struct Node {
	BWTA::Chokepoint* choke;
	BWTA::Region* region;
	int num;
	bool regionCheck;
	Position getCenter() {
		if (regionCheck) {
			return region->getCenter();
		}
		else {
			return choke->getCenter();
		}
	}
	bool isRegion() {
		return regionCheck;
	}
	int getNumber() {
		return num;
	}
	Node(BWTA::Region* r, int n) {
		num = n;
		regionCheck = true;
		region = r;
	}
	Node(BWTA::Chokepoint* c, int n) {
		num = n;
		regionCheck = false;
		choke = c;
	}
};

class CombatInfo
{
	//startStrategy* test2;
private:
	CombatInfo();
	~CombatInfo();
	static CombatInfo *s_CombatInfo;
	vector < pair<BWTA::Region*, vector<BWTA::Chokepoint*>>> nearRegionChokePoints;

	const int GROUP_R = 400;
	int enemyDir;
	int enemyPosVal[128][128];
	Position enemyPos[128][128];

	map<int, Node*> nodeIndex;

	int NodeGraph[102][102];
	int NodeGraph2[102][102];
	int NodeGraphBackup[102][102];

	Position MyIMPos[128][128];
	int MyIM[128][128];
	
	int graphTotalIndex = 0;

	int Floyd(Position currentPos, Position targetPos);
	int Floyd2(Position currentPos, Position targetPos);

	///////////////////////////////////////////////////////////// Vulture

	vector<Position> mines;

	/////////////////////////////////////////////////////////////

public:
	static CombatInfo* getInstance();
	static CombatInfo* create();
	void init();
	void updateEnemyInfo();
	void updateEnemyBase();
	void updateMyInfo();
	void updateMyBase();
	void drawCombatInfo(bool debug);

	BWTA::BaseLocation* enemyStartingBase = NULL;
	BWTA::BaseLocation* myStartingBase = NULL;
	BWTA::BaseLocation* enemyFrontYard = NULL;

	// 니꺼
	list<pair<int, pair<UnitType, Position>>> enemyUnitsInfo;				//[id, [UnitType, LastPosition]] (Units)
	list<pair<int, pair<UnitType, Position>>> enemyBuildingsInfo;			//[id, [UnitType, Position]] (Buildings)
	vector<pair<Position, pair<int, int>>> enemyGroup;						//[GroupPosition, [Range, GroupLevel]]
	vector<pair<Position, pair<int, int>>> enemyGroundGroup;				//[GroupPosition, [Range, GroupLevel]]
	vector<pair<BWTA::BaseLocation*, int>> enemyBase;						//[BaseLocation, baseLevel]
	map<BWTA::BaseLocation*, bool> checkEnemyBase;

	// 내꺼
	list<pair<int, pair<UnitType, Position>>> myUnitsInfo;				//[id, [UnitType, LastPosition]] (Units)
	list<pair<int, pair<UnitType, Position>>> myBuildingsInfo;			//[id, [UnitType, Position]] (Buildings)
	vector<pair<Position, pair<int, int>>> myGroup;						//[GroupPosition, [Range, GroupLevel]]
	vector<pair<BWTA::BaseLocation*, int>> myBase;						//[BaseLocation, baseLevel]

	Position attackPos;
	Position defensePos;
	Position myUnitPos;
	BWTA::BaseLocation* attackBase;

	int getEnemyUnitNumber(UnitType);										//적 유닛 개수
	int getEnemyBuildingNumber(UnitType);									//적 건물 개수
	vector<pair<Position, pair<int, int>>> getGroupArray(bool, bool);

	bool isGumNodes(Node*, Node*);							// 두 노드가 붙어있슴까? (Region)

	vector<BWTA::Chokepoint *> getNearestChokePoints(BWTA::Region*);		// 현재 Region or Position에 가까운 ChokePoint를 반환
	vector<BWTA::Chokepoint *> getNearestChokePoints(BWAPI::Position);		// 현재 Region or Position에 가까운 ChokePoint를 반환
	Position getRTB_MovePosition(Position currentPos, Position targetPos);	// 현재 Position에서 target Position으로 가는 RTB 상황에서 적들을 피해 돌아가기 위해 현재 이동해야할 Position을 반환 

	void onDestroy(Unit u);
	char* getString(int);

	int getEnemyDir() {
		return enemyDir;
	}

	Position getMyGroupPos();
	void updateMyIM();

	Position getFloydPosition(Position current, Position target);
	Position getFloydPosition2(Position current, Position target);

	Position getAttackPosition();
	Position getFinalAttackPosition();
	Position getDefensePosition();
	BWTA::BaseLocation* getAttackBase();
	Position returnBase;

	map<BWTA::BaseLocation*, bool> checkStarting;
	map<BWTA::BaseLocation*, bool> checkSeekStarting;
};

