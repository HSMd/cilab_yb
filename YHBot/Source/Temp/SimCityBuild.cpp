#include "SimCityBuild.h"
#include "../Single.h"
#include "Scout.h"
#include "UtilMgr.h"

SimCity_1::SimCity_1() {

	frameCount = 0;
	out = nullptr;
	builder = nullptr;

	this->tMgr = &cmd->getTerritoryMgr();

	int s1 = 8;
	int s2 = 14;
	int gas = 12;
	int b1 = 10;

	switch (mapInfo->getDir()) {
	case 0:
		build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, TilePosition(97, 5), UnitTypes::Terran_SCV, s1, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Barracks, TilePosition(102, 9), UnitTypes::Terran_SCV, b1, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Refinery, *AUTO_POS, UnitTypes::Terran_SCV, gas, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, TilePosition(100, 7), UnitTypes::Terran_SCV, s2, true));

		break;

	case 1:

		build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, TilePosition(120, 97), UnitTypes::Terran_SCV, s1, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Barracks, TilePosition(114, 101), UnitTypes::Terran_SCV, b1, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Refinery, *AUTO_POS, UnitTypes::Terran_SCV, gas, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, TilePosition(118, 99), UnitTypes::Terran_SCV, s2, true));

		break;

	case 2:
		build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, TilePosition(11, 26), UnitTypes::Terran_SCV, s1, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Barracks, TilePosition(4, 28), UnitTypes::Terran_SCV, b1, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Refinery, *AUTO_POS, UnitTypes::Terran_SCV, gas, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, TilePosition(8, 26), UnitTypes::Terran_SCV, s2, true));

		break;

	case 3:
		build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, TilePosition(23, 118), UnitTypes::Terran_SCV, s1, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Barracks, TilePosition(24, 120), UnitTypes::Terran_SCV, b1, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Refinery, *AUTO_POS, UnitTypes::Terran_SCV, gas, true));
		build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, TilePosition(28, 121), UnitTypes::Terran_SCV, s2, true));

		break;
	}

	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 1, UnitTypes::Terran_SCV, 12));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, 80, UnitTypes::Terran_SCV, 14, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Machine_Shop, *AUTO_POS, UnitTypes::Terran_Factory, 1, true));
}

SimCity_1::~SimCity_1() {

}

void SimCity_1::chkStrategy(){

	

	auto startTerritory = tMgr->getAllTerritorys()[0];
	auto& state = cmd->getStateMgr();
	int frame = bw->getFrameCount();

	for (auto unit : bw->enemy()->getUnits()){
		if (unit->getType() == UnitTypes::Protoss_Probe){
			if (getDistance(startTerritory->getBaseTilePosition(), unit->getTilePosition()) < 26){
				state.setCombatFlag(COMBAT_FLAG::ENEMY_SEARCH, true);
				strategyFlag[COMBAT_FLAG::ENEMY_SEARCH] = frame;
			}			
		}
		else if (unit->getType() == UnitTypes::Protoss_Pylon){
			auto choke = getNearestChokepoint(startTerritory->getBaseTilePosition());
			if (getDistance(pos2tile(choke->getCenter()), unit->getTilePosition()) < 10){
				state.setCombatFlag(COMBAT_FLAG::INVASION_PYLON, true);
				strategyFlag[COMBAT_FLAG::INVASION_PYLON] = frame;
			}
		}
		else if (unit->getType() == UnitTypes::Protoss_Zealot){
			if (getDistance(startTerritory->getBaseTilePosition(), unit->getTilePosition()) < 40){
				state.setCombatFlag(COMBAT_FLAG::ZEALOT_RUSH, true);
				strategyFlag[COMBAT_FLAG::ZEALOT_RUSH] = frame;
			}
		}
	}

	if (strategyFlag[COMBAT_FLAG::ENEMY_SEARCH] + 24 * 5 < frame){
		state.setCombatFlag(COMBAT_FLAG::ENEMY_SEARCH, false);
	}

	if (strategyFlag[COMBAT_FLAG::INVASION_PYLON] + 24 * 1 < frame){
		state.setCombatFlag(COMBAT_FLAG::INVASION_PYLON, false);
	}

	if (strategyFlag[COMBAT_FLAG::ZEALOT_RUSH] + 24 * 10 < frame){
		state.setCombatFlag(COMBAT_FLAG::ZEALOT_RUSH, false);
	}

	// 심시티 완성
	if (!state.getCombatFlag(COMBAT_FLAG::INVASION_PYLON) && 2 <= me->getImmediatelyBuilding(UnitTypes::Terran_Supply_Depot).size() && 1 <= me->getBuildings(UnitTypes::Terran_Barracks).size()){
		state.setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, true);
	}
}

void SimCity_1::run(COMBAT_FLAG combat) {

	chkStrategy();

	auto startTerritory = tMgr->getAllTerritorys()[0];	
	
	// SCV 스케쥴러에 추가합니다.
	if (cmd->getUnitScheduleCount(UnitTypes::Terran_SCV) < tMgr->getAllTerritorys().size()) {
		for (auto territory : tMgr->getAllTerritorys()) {
			int require = territory->getRequireSCVCount() + 1;

			if (1 < require && cmd->getReadyTrain(territory->getCmdCenter()->getSelf())) {
				cmd->addScvCount();
				cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_SCV, territory->getCmdCenter()->getSelf()), 1, 1);
			}
		}
	}


	// 예약된 BUILD 들이 점화되면 수행된다.	
	for (auto& work : build) {

		auto targetType = work.getTargetType();

		if (!work.getFire() && work.canFire()) {

			if (startTerritory->getCmdCenter()->getSelf()->getTrainingQueue().size() < 1) {
				break;
			}

			if (targetType.isBuilding()) {

				if (builder == nullptr) {
					builder = startTerritory->getWorker(false);
				}

				if (targetType == UnitTypes::Terran_Supply_Depot || targetType == UnitTypes::Terran_Barracks) {
					work.setBuilder(builder);
					
					if (builder->getSelf()->isConstructing()){
						continue;
					}
				}

				cmd->addStructureSchedule(work, 1, 0);
				work.onFire();
			}
			else {

				cmd->addUnitSchedule(targetType, work.getCount(), 0);
				work.onFire();
			}
		}
	}

	errorSCV();

	// 정찰 보내는 것.
	if (!flags[spy] && 11 <= me->getUnits(UnitTypes::Terran_SCV).size()) {
		SCV* spyScv = startTerritory->getWorker(true);
		Scouting::getInstance()->setScoutUnit(spyScv);
		flags[spy] = true;
	}

	if (chkEnd()) {
		if (flags[ENTRANCE_OPEN]) {
			if (flags[ENTRANCE_CLOSE]) {
				cmd->getStateMgr().setBuildFlag(BUILD_FLAG::SIM_CITY_1, true);
			}
		}
		else
			cmd->getStateMgr().setBuildFlag(BUILD_FLAG::SIM_CITY_1, true);
	}
}

void SimCity_1::errorSCV() {

	auto startTerritory = tMgr->getAllTerritorys()[0];

	// 입구가 막혔을 경우 쫒겨난 유닛에 대한 대쳐.
	if (out == nullptr) {
		for (auto scv : startTerritory->getWorkSCV()) {
			if (scv->getState() != SCV::STATE::RETURN) {
				continue;
			}

			int dis = getDistance(scv->getSelf()->getTilePosition(), mapInfo->getNearBaseLocation()->getTilePosition());
			if (dis < 11) {
				out = scv;
			}
		}
	}
	else {
		auto b = me->getBuildings(UnitTypes::Terran_Barracks)[0];
		if (!flags[ENTRANCE_OPEN]) {
			bw << "OPEN" << endl;
			outPos = b->getSelf()->getTilePosition();
			if (b->getSelf()->isIdle() && b->getSelf()->isCompleted())
				if (b->getSelf()->lift())
					flags[ENTRANCE_OPEN] = true;
		}
		else {
			if (!flags[ENTRANCE_CLOSE] && getDistance(out->getSelf()->getTilePosition(), startTerritory->getCmdCenter()->getSelf()->getTilePosition()) < 17) {
				bw << "CLOSE" << endl;
				if (b->getSelf()->land(outPos))
					flags[ENTRANCE_CLOSE] = true;
			}
		}
	}
}

bool SimCity_1::chkEnd() {

	for (auto& work : build) {
		if (!work.getFire() || me->getImmediatelyBuilding(UnitTypes::Terran_Factory).size() < 1) {
			return false;
		}
	}

	return true;
}