#pragma once
#include "Work.h"
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"

class FactoryStrategy
{
	// 한번만 실행되어야 될 것들.
	enum FLAG{
		ATTACK_READY,
		ENEMY_SETTING,
		WE_ARE_THE_ONE,
	};

	map<int, bool> flags;
	int frameCount;

	// 자료구조.
	vector<Position> marinePosition;
	vector<Position> tankPosition;
	vector<Position> vulturePosition;
	vector<Position> tankBackupPosition;
	vector<Position> teamAttackPosition;
	vector<SCV*> guardSCV;
	bool teamHongHong[10];
	int mIndex = 0;
	int tIndex = 0;
	int vIndex = 0;

	Position nextPosition;
	Unitset tankUnits;
public:
	FactoryStrategy();
	~FactoryStrategy();

	void run(COMBAT_FLAG combat);
	void end();

	void normal();
	void guardZealotRush();
	void guardDragoonRush();
	void attack();
	void scvrepair();
	void draw(COMBAT_FLAG combat);
};

