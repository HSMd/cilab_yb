#include "Scout.h"
#include "UtilMgr.h"
#include "../Single.h"
#include "VultureControl.h"

Scouting* Scouting::s_Scouting;

Scouting::Scouting() {
	init();
}

Scouting::~Scouting() {

}

Scouting* Scouting::create() {
	if (s_Scouting) {
		return s_Scouting;
	}

	s_Scouting = new Scouting();
	return s_Scouting;
}

Scouting* Scouting::getInstance() {
	return s_Scouting;
}

void Scouting::init() {

	chkFinScv = false;

	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			scoutPosition[y][x] = Position(x * 32, y * 32);
			scoutValue[y][x] = 0;
		}
	}

	targetBase = NULL;
	enemyStartingBase = NULL;

	for (int x = 0; x < 4; x++) {
		checkStarting[x] = false;
	}

	int nearIndex = -1;
	int index = 0;
	float min = 99999;
	for (auto s = BWTA::getStartLocations().begin(); s != BWTA::getStartLocations().end(); s++) {
		float dist = getDistance((*s)->getPosition(), mapInfo->getMyStartBase()->getPosition());
		if (dist < min) {
			min = dist;
			nearIndex = index;
		}
		index++;
	}

	checkStarting[nearIndex] = true;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Scout Func

void Scouting::SCV_Scout(SCV* s_Unit) {
	BWAPI::Broodwar->drawCircleMap(s_Unit->getSelf()->getPosition(), s_Unit->getSelf()->getType().width() / 2, BWAPI::Colors::Purple);
	scoutFrame++;
	if (unLookEnemy) {
		checkEnemyBase(s_Unit);
		if (targetBase == NULL) {
			targetBase = getTargetBase();
			if (targetBase == NULL) {
				cmd->getTerritoryMgr().getAllTerritorys()[0]->addSCV(spy, true);
				chkFinScv = true;
			}
		}
		else {
			if (s_Unit != NULL) {
				if (s_Unit->getSelf()->exists() && targetBase != NULL) {
					if (targetBase == NULL)printf("targetBase NULL point exception\n");
					goTarget(s_Unit, targetBase->getPosition());
				}
			}
		}
	}
	else {
		goOutofOnesAwayEnemyStarting(s_Unit);
	}
	//printf("SCV SCOUT FRAME %d\n", scoutFrame);
}

void Scouting::AIR_Scout(MyUnit* s_Unit) {

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Scout Info Func

void Scouting::checkEnemyBase(MyUnit* s_Unit) {																											// 적 본진 확인
	BWAPI::Unitset enemyUnits = BWAPI::Broodwar->enemy()->getUnits();

	for (auto u = enemyUnits.begin(); u != enemyUnits.end(); u++) {
		if ((*u)->getType() == BWAPI::UnitTypes::Protoss_Nexus) {
			float min = 99999;
			for (auto s = BWTA::getStartLocations().begin(); s != BWTA::getStartLocations().end(); s++) {
				float dist = getDistance((*s)->getPosition(), (*u)->getPosition());
				if (dist < min) {
					min = dist;
					enemyStartingBase = (*s);
					setMovePosition(enemyStartingBase->getPosition(), s_Unit);
					unLookEnemy = false;
				}
			}
		}
	}

	if (unLookEnemy) {																													// 스타팅 3개 확인했는데 발견 못했을 경우
		int count = 0;
		for (int x = 0; x < 4; x++) {
			if (checkStarting[x]) count++;
		}
		if (count == 3) {
			int index = 0;
			for (auto q = BWTA::getStartLocations().begin(); q != BWTA::getStartLocations().end(); q++) {
				if (!checkStarting[index++]) {
					enemyStartingBase = (*q);
				}
			}
		}
	}
}

BWTA::BaseLocation * Scouting::getTargetBase() {																							// 다음 스타팅 타겟 지점 받아옴

	if (targetIndex > 3) {
		return NULL;
	}
	BWTA::BaseLocation* ret;
	if (targetBase == NULL) {
		int index = 0;
		float min = 99999;
		bool alreadyScout = false;
		for (auto s = BWTA::getStartLocations().begin(); s != BWTA::getStartLocations().end(); s++) {
			if (!checkStarting[index]) {
				float dist = getDistance((*s)->getPosition(), spy->getSelf()->getPosition());
				if (dist < min) {
					min = dist;
					ret = (*s);
					targetIndex = index;
				}
			}
			index++;
		}
	}

	return ret;
}

void Scouting::goTarget(MyUnit *s_Unit, BWAPI::Position target) {																									// 타겟지점으로 이동
	s_Unit->getSelf()->move(target, false);
	float dist = getDistance(target, s_Unit->getSelf()->getPosition());
	if (dist < 300) {
		arriveTarget();
	}
	if (scoutFrame > 1200) {
		CombatInfo::getInstance()->checkSeekStarting[targetBase] = false;
		checkStarting[targetIndex] = true;
		targetBase = NULL;
	}
}

void Scouting::arriveTarget() {
	CombatInfo::getInstance()->checkSeekStarting[targetBase] = true;
	scoutFrame = 0;
	if (unLookEnemy) {
		checkStarting[targetIndex] = true;
		targetBase = NULL;
	}
	else {
		checkStarting[targetIndex] = true;
		targetIndex = (targetIndex + 1) % 4;
	}
}

void Scouting::goOutofOnesAwayEnemyStarting(SCV* s_Unit) {
	bool checkFin = true;
	for (int x = 0; x < 4; x++) {
		if (!CombatInfo::getInstance()->checkSeekStarting[targetBase]) {
			checkFin = false;
		}
	}

	if (checkFin) {
		//정찰 끝났음요
		if (!chkFinScv) {
			cmd->getTerritoryMgr().getAllTerritorys()[0]->addSCV(spy, true);
			chkFinScv = true;
		}

	}
	else {
		goTarget(s_Unit, movePosition[targetIndex]);
	}

	/*Position p, targetP;
	int minValue = 9999;
	int valueArr[5][5] = { 0 };
	memset(valueArr, -1, sizeof(int));
	for (int x = -1; x <= 1; x++) {
	for (int y = -1; y <= 1; y++) {
	int _x = x + (s_Unit->getSelf()->getPosition().x / 32);
	int _y = y + (s_Unit->getSelf()->getPosition().y / 32);
	if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
	Position pp = Position(_x * 32, _y * 32);
	BWAPI::Broodwar->drawCircleMap(pp, 10, Colors::Red, false);
	int value = VultureControl::getInstance()->ScoutIM[_y][_x];
	if (value >= 0) {
	for (int i = -1; i <= 1; i++) {
	for (int j = -1; j <= 1; j++) {
	int _xx = _x + i;
	int _yy = _y + j;
	if (_xx < 0 || _yy < 0 || _xx > 127 || _yy > 127) continue;
	pp = Position(_xx * 32, _yy * 32);
	BWAPI::Broodwar->drawCircleMap(pp, 10, Colors::Red, false);
	int vv = value + VultureControl::getInstance()->ScoutIM[_yy][_xx];
	if (vv < minValue && vv > 0 && vv) {
	valueArr[2 + y + j][2 + x + i] = vv;
	minValue = vv;
	}
	}
	}
	}
	}
	}
	float minDist = 9999;
	for (int x = -2; x <= 2; x++) {
	for (int y = -2; y <= 2; y++) {
	int _x = x + (s_Unit->getSelf()->getPosition().x / 32);
	int _y = y + (s_Unit->getSelf()->getPosition().y / 32);
	if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;

	if (valueArr[y+2][x+2] == minValue) {
	p = Position(_y * 32, _x * 32);
	float dist = getDistance(p, movePosition[targetIndex]);
	if (dist < minDist) {
	minDist = dist;
	BWAPI::Broodwar->drawCircleMap(p, 10, Colors::Purple, true);
	targetP = p;
	}
	}
	}
	}
	for (int x = 0; x < 4; x++) {
	BWAPI::Broodwar->drawCircleMap(movePosition[x], 10, Colors::Purple, true);
	}


	s_Unit->getSelf()->move(targetP, false);
	BWAPI::Broodwar->drawCircleMap(targetP, 10, Colors::Purple, true);

	if (getDistance(s_Unit->getSelf()->getPosition(), movePosition[targetIndex]) < 30) {
	targetIndex = (targetIndex + 1) % 4;
	}*/
}

void Scouting::setMovePosition(Position es, MyUnit* s_Unit) {																				// 적 본진 발견했을 경우 한바퀴 마실 돌기 위해서 포지션 정함
	movePosition[0] = Position(es.x + 240, es.y + 190);
	movePosition[1] = Position(es.x - 240, es.y + 190);
	movePosition[2] = Position(es.x - 240, es.y - 190);
	movePosition[3] = Position(es.x + 240, es.y - 190);
	int minIndex = 0;
	int min = 99999;
	for (int x = 0; x < 4; x++) {
		checkStarting[x] = false;
		float dist = getDistance(movePosition[x], s_Unit->getSelf()->getPosition());
		if (dist < min) {
			min = dist;
			minIndex = x;
		}
	}
	targetIndex = minIndex;
}

void Scouting::setScoutUnit(SCV* unit) {
	init();

	spy = unit;
	//spy = scvMgr->getWorker(true);
	//spy->orderSpy();
}

void Scouting::run() {
	if (spy != NULL && spy->getSelf()->exists() == false) {
		CombatInfo::getInstance()->enemyStartingBase = targetBase;
		spy = NULL;
	}
	if (spy != NULL) {
		if (spy->getSelf()->isCarryingMinerals()) {
			spy->getSelf()->returnCargo(true);
		}
		else {
			SCV_Scout(spy);
		}
	}
}