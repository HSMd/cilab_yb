#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <vector>
#include <stdio.h>
#include <list>
#include <map>

using namespace std;
using namespace BWAPI;
using namespace Filter;

struct Node {
	BWTA::Chokepoint* choke;
	BWTA::Region* region;
	int num;
	bool regionCheck;
	Position getCenter() {
		if (regionCheck) {
			return region->getCenter();
		}
		else {
			return choke->getCenter();
		}
	}
	bool isRegion() {
		return regionCheck;
	}
	int getNumber() {
		return num;
	}
	Node(BWTA::Region* r, int n) {
		num = n;
		regionCheck = true;
		region = r;
	}
	Node(BWTA::Chokepoint* c, int n) {
		num = n;
		regionCheck = false;
		choke = c;
	}
};

class CombatInfo
{
	//startStrategy* test2;
private:
};

