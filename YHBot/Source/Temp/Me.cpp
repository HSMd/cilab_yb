#include "Me.h"
#include "CombatInfo.h"
#include "UnitControl.h"
#include "UtilMgr.h"

Me::Me(){
	init();
}


Me::~Me()
{
	
}

void Me::init(){
	//addTerritory(mapInfo->getMyStartBase());
	
}

Me* Me::getInstance()
{
	static Me instance;
	return &instance;
}

BWAPI::Player Me::player()
{
	return Broodwar->self();
}

void Me::update(){
	
	updateUnit(UnitTypes::Terran_SCV , false);
	updateUnit(UnitTypes::Terran_SCV, true);

	updateUnit(UnitTypes::Terran_Marine, false);
	updateUnit(UnitTypes::Terran_Marine, true);

	updateUnit(UnitTypes::Terran_Vulture, false);
	updateUnit(UnitTypes::Terran_Vulture, true);

	updateUnit(UnitTypes::Terran_Siege_Tank_Tank_Mode, false);
	updateUnit(UnitTypes::Terran_Siege_Tank_Tank_Mode, true);
}

void Me::updateUnit(UnitType unitType, bool immediate){

	if (immediate){
		auto& units = getUnits(unitType);

		for (auto iter = units.begin(); iter != units.end();){
			MyUnit* unit = (*iter);
			if (!unit->getSelf()->exists()){
				iter = units.erase(iter);
				continue;
			}

			++iter;
		}
	}
	else{
		auto& units = getImmediatelyUnit(unitType);

		for (auto iter = units.begin(); iter != units.end();){
			MyUnit* unit = (*iter);
			if (!unit->getSelf()->exists()){
				iter = units.erase(iter);
				continue;
			}

			++iter;
		}
	}
}

int Me::minerals(){
	return Broodwar->self()->minerals();
}

int Me::gas(){
	return Broodwar->self()->gas();
}

void Me::addUnit(BWAPI::Unit unit)
{
	if (unit->getType() == UnitTypes::Terran_SCV){
		units[unit->getType()].push_back(new SCV(unit));
	}
	else if (unit->getType() == UnitTypes::Terran_Marine){
		units[unit->getType()].push_back(new Marine(unit));
	}
	else if (unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode){
		units[unit->getType()].push_back(new Tank(unit));
	}
	else{
		units[unit->getType()].push_back(new Solider(unit));
	}	
}

void Me::addUnitImmediately(BWAPI::Unit unit){

	if (unit->getType() == UnitTypes::Terran_SCV){
		createUnits[unit->getType()].push_back(new SCV(unit));
	}
	else if (unit->getType() == UnitTypes::Terran_Marine){
		createUnits[unit->getType()].push_back(new Marine(unit));
	}
	else if (unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode){
		createUnits[unit->getType()].push_back(new Tank(unit));
	}
	else{
		createUnits[unit->getType()].push_back(new Solider(unit));
	}
}

void Me::addAllUnit(Unit unit){

	allUnits.push_back(new MyUnit(unit));
}

void Me::addBuilding(BWAPI::Unit building)
{
	buildings[building->getType()].push_back(new Structure(building));
}

void Me::addBuildingImmediately(BWAPI::Unit building){
	createBuildings[building->getType()].push_back(new Structure(building));
}

vector<MyUnit*>& Me::getUnits(int type)
{
	return units[type];
}

vector<MyUnit*>& Me::getImmediatelyUnit(int type){
	return createUnits[type];
}

vector<MyUnit*>& Me::getImmediatelyBuilding(int type){
	return createBuildings[type];
}

vector<MyUnit*>& Me::getBuildings(int type){
	return buildings[type];
}

vector<MyUnit*>& Me::getAllUnits(){
	return allUnits;
}

void Me::run() {
	
}

void Me::drawMe() {
	//for (int x = 0; x < 127; x++) {
	//	for (int y = 0; y < 127; y++) {
	//		if (MyIM[y][x] > 50) {
	//			BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 2, Colors::Red, true);
	//		}
	//		else if (MyIM[y][x] > 40) {
	//			BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 2, Colors::Orange, true);
	//		}
	//		else if (MyIM[y][x] > 30) {
	//			BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 2, Colors::Yellow, true);
	//		}
	//		else if (MyIM[y][x] > 20) {
	//			BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 2, Colors::Green, true);
	//		}
	//		else if (MyIM[y][x] > 10) {
	//			BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 2, Colors::Teal, true);
	//		}
	//		else {
	//			BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 2, Colors::Blue, true);
	//		}
	//		//	BWAPI::Broodwar->drawTextMap(Position(x * 32, y * 32), CombatInfo::getInstance()->getString(MyIM[y][x]));
	//	}
	//}
}

