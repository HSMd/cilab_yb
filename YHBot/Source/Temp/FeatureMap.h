#pragma once

#include "BWAPI.h"
#include "SendDataType.h"

extern Data data;

extern bool run;
extern bool canSend;
extern bool expand;

int ipc();
void setVision();
int getProgress(BWAPI::Unit);
void setData();
void setRun(bool);