#include "PositionMgr.h"
#include "../Single.h"

using namespace BWAPI;

PosMgr* PosMgr::getInstance(){
	static PosMgr instance;
	return &instance;
}

void PosMgr::init(){
	facSize = -1;
	splySize = -1;
	dir = false;

	switch (mapInfo->getDir()){
	case 0:
		splyPos = TilePosition(125, 15);
		otherPos = TilePosition(115, 11);

		startFacPos_1 = TilePosition(106, 1);
		startFacPos_2 = TilePosition(107, 11);

		break;
	case 1:
		splyPos = TilePosition(120, 125);
		otherPos = TilePosition(123, 111);

		startFacPos_1 = TilePosition(114, 105);
		startFacPos_2 = TilePosition(104, 105);

		break;
	case 2:
		splyPos = TilePosition(0, 14);
		otherPos = TilePosition(0, 0);

		startFacPos_1 = TilePosition(11, 16);
		startFacPos_2 = TilePosition(19, 11);

		break;
	case 3:
		splyPos = TilePosition(0, 111);
		otherPos = TilePosition(0, 123);

		startFacPos_1 = TilePosition(14, 120);
		startFacPos_2 = TilePosition(13, 108);

		break;
	}
}

TilePosition PosMgr::getSplyPos(bool add, int idx) {

	TilePosition re = splyPos;
	TilePosition tmp;
	TilePosition more;

	if (add) {
		switch (mapInfo->getDir()) {
		case 0:
			tmp = TilePosition(0, 2);
			more = TilePosition(-3, 0);

			break;
		case 1:
			tmp = TilePosition(-3, 0);
			more = TilePosition(0, -2);

			break;
		case 2:
			tmp = TilePosition(0, 2);
			more = TilePosition(3, 0);

			break;
		case 3:
			tmp = TilePosition(0, -2);
			more = TilePosition(3, 0);

			break;
		}
	}

	for (int i=0;i<idx;i++) {
		re += more;
	}	

	if (bw->canBuildHere(re, UnitTypes::Terran_Supply_Depot)) {
		return re;
	}

	for (int i = 0; i < 10; i++) {
		re += tmp;
		if (bw->canBuildHere(re, UnitTypes::Terran_Supply_Depot)) {
			return re;
		}
	}

	return *AUTO_POS;
}

TilePosition PosMgr::getSplyPos(bool add){

	TilePosition re = splyPos;
	TilePosition tmp;
	TilePosition more;
	splySize++;

	if (add){
		switch (mapInfo->getDir()){
		case 0:
			tmp = TilePosition(0, 2);
			more = TilePosition(-3, 0);

			break;
		case 1:
			tmp = TilePosition(-3, 0);
			more = TilePosition(0, -2);

			break;
		case 2:
			tmp = TilePosition(0, 2);
			more = TilePosition(3, 0);

			break;
		case 3:
			tmp = TilePosition(0, -2);
			more = TilePosition(3, 0);

			break;
		}
	}

	if (splySize == 0){
		return splyPos;
	}

	if (splySize % 7 == 0){
		splyPos += more;
		dir = (dir + 1) % 2;
	}
	else{
		if (dir == true){
			tmp = tmp * -1;
		}

		splyPos += tmp;
	}
	
	

	/*
	if (bw->canBuildHere(re, UnitTypes::Terran_Supply_Depot)) {
		return re;
	}

	for (int i = 0; i < 10; i++) {
		re += tmp;
		if (bw->canBuildHere(re, UnitTypes::Terran_Supply_Depot)) {
			return re;
		}
	}

	re = splyPos + more;
	for (int i = 0; i < 10; i++) {
		re += tmp;
		if (bw->canBuildHere(re, UnitTypes::Terran_Supply_Depot)) {
			return re;
		}
	}
	*/

	return splyPos;
}

TilePosition PosMgr::getFtryPos(bool add){

	TilePosition re = facPos;
	TilePosition tmp;
	TilePosition more;
	facSize++;
	int size = facSize;

	if (size == 0){
		re = startFacPos_1;
		return re;
	}
	else if (size == 1){
		re = startFacPos_1 + TilePosition(0, 4);
		return re;
	}
	
	re = startFacPos_2;
	int cnt = 0;
	for (int i = 0; i < 2;i++){
		for (int j = 0; j < 3; j++){
			if (cnt == size -  2){
				return re;
			}

			re += TilePosition(0, 3);
			cnt++;
		}

		re = startFacPos_2 + TilePosition(4, 0);
	}

	return re;
}

TilePosition PosMgr::getOthrPos(bool add){
	TilePosition re = otherPos;

	if (add){
		switch (mapInfo->getDir()){
		case 0:
			otherPos += TilePosition(0, 1);

			break;
		case 1:
			otherPos += TilePosition(0, -1);

			break;
		case 2:
			otherPos += TilePosition(1, 0);

			break;
		case 3:
			otherPos += TilePosition(1, 0);

			break;
		}
	}

	return re;
}