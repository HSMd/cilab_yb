#include "Invasion_pylon.h"
#include "../single.h"
#include "UtilMgr.h"

Invasion_pylon::Invasion_pylon(TerritoryMgr* tMgr){

	this->tMgr = tMgr;
	
	init();
}

Invasion_pylon::~Invasion_pylon(){
	for (auto g : guardSCV){
		if (g->getSelf()->exists()){
			g->setState(SCV::STATE::IDLE);
		}
	}
}

Marine* Invasion_pylon::getIdleMarine(){
	for (auto m : me->getUnits(UnitTypes::Terran_Marine)){
		Marine* marine = dynamic_cast<Marine*>(m);

		if (marine->getState() == Marine::STATE::IDLE){
			marine->setState(Marine::STATE::WAIT);
			return marine;
		}
	}

	return nullptr;
}



void Invasion_pylon::init(){
	// 
	switch (mapInfo->getDir()){
		case 0:
			// pylonRush
			pylonPosition.push_back(tile2pos(TilePosition(93, 25)));
			pylonPosition.push_back(tile2pos(TilePosition(99, 10)));
			pylonPosition.push_back(tile2pos(TilePosition(100, 11)));
			pylonPosition.push_back(tile2pos(TilePosition(98, 12)) + Position(16, 0));

			marinePosition.push_back(tile2pos(TilePosition(100, 6)));
			marinePosition.push_back(tile2pos(TilePosition(101, 6)));// +Position(0, 16));
			marinePosition.push_back(tile2pos(TilePosition(103, 8)));
			marinePosition.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
			marinePosition.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));
			break;

		case 1:
			// pylonRush
			pylonPosition.push_back(tile2pos(TilePosition(100, 90)));
			pylonPosition.push_back(tile2pos(TilePosition(115, 98)) + Position(16, 16));
			pylonPosition.push_back(tile2pos(TilePosition(116, 98)) + Position(16, 0));
			pylonPosition.push_back(tile2pos(TilePosition(115, 97)) + Position(16, 16));

			marinePosition.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(118, 102)) + Position(0, 16));
			marinePosition.push_back(tile2pos(TilePosition(119, 102)));
			marinePosition.push_back(tile2pos(TilePosition(121, 99)));
			marinePosition.push_back(tile2pos(TilePosition(114, 101)));
			break;

		case 2:
			// pylonRush
			pylonPosition.push_back(tile2pos(TilePosition(27, 37)));
			pylonPosition.push_back(tile2pos(TilePosition(11, 30)));
			pylonPosition.push_back(tile2pos(TilePosition(10, 30)) + Position(16, 16));
			pylonPosition.push_back(tile2pos(TilePosition(11, 31)));

			marinePosition.push_back(tile2pos(TilePosition(7, 26)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(11, 26)));
			marinePosition.push_back(tile2pos(TilePosition(14, 27)) + Position(0, 16));
			marinePosition.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 7));
			break;

		case 3:
			// pylonRush
			pylonPosition.push_back(tile2pos(TilePosition(36, 102)));
			pylonPosition.push_back(tile2pos(TilePosition(27, 118)));
			pylonPosition.push_back(tile2pos(TilePosition(28, 119)));
			pylonPosition.push_back(tile2pos(TilePosition(28, 118)) + Position(16, 0));

			marinePosition.push_back(tile2pos(TilePosition(23, 117)) + Position(16, 16));
			marinePosition.push_back(tile2pos(TilePosition(24, 121)));
			marinePosition.push_back(tile2pos(TilePosition(27, 123)));
			marinePosition.push_back(tile2pos(TilePosition(28, 123)));
			marinePosition.push_back(tile2pos(TilePosition(31, 121)) + Position(16, 16));
			break;
	}
}

void Invasion_pylon::run2(){
	pylon = nullptr;

	for (auto e : bw->enemy()->getUnits()){
		if (e->getType() == UnitTypes::Protoss_Pylon){
			pylon = e;
			break;
		}
	}
		

	// 발생했을 경우 3마리를 작업용으로 빼온다.
	if (guardSCV.empty()){
		for (int i = 0; i < 3; i++){
			SCV* scv = tMgr->getAllTerritorys()[0]->getWorker(true);
			guardSCV.push_back(scv);
		}		
	}

	// 3마리가 위치가 할당이 안되있을 경우 할당해준다.
	for (int i = 0; i < 3; i++){
		SCV* scv = guardSCV[i];

		if (holdSCV[scv] == Position(0, 0)){
			holdSCV[scv] = pylonPosition[i];

			if (i == 0){																// 여기서 3번 뒤졌음  ps scv 개수가 넉넉하면 안쥬금
				scv->setState(SCV::STATE::MOVE);
			}
			else{
				scv->setState(SCV::STATE::ETC);
			}
		}				
	}
	// Marine 컨트롤 수행
	//컨트롤 하기 위해서 마린을 받아와 쌓아
	Marine* m = getIdleMarine();
	if (m != nullptr){
		guardmarine.push_back(m);
	}
	//파일런만 있으면 마린이 파일런 공격, 마린이 없으면 그 부분 지우기
	for (vector<Marine*>::iterator iter = guardmarine.begin(); iter != guardmarine.end();){
		Marine* m_temp = (*iter);
		if (m_temp->getSelf()->exists()){
			m_temp->setState(Marine::STATE::ATTACK);
			MarineControl(m_temp);
			iter++;
		}
		else{
			iter = guardmarine.erase(iter);
		}
	}

	// 질럿이 보여서 0번째 가드가 지정된 위치로 올 경우 1, 2 번쨰를 0번째 수리하게 한다.
	if (ZealotDetect()){
		holdSCV[guardSCV[0]] = pylonPosition[3];

		if (isAlmostReachedDest(holdSCV[guardSCV[0]], guardSCV[0]->getSelf()->getPosition(), 32, 32)){
			guardSCV[1]->setState(SCV::STATE::REPAIR);
			guardSCV[2]->setState(SCV::STATE::REPAIR);
		}

		//질럿이 공격 사정거리 안에 있으면  마린이 질럿을 공격, 그렇지 않으면 마린은 파일럿 공격, 파일런 없으면 위치 고정
		/*마린이 고정할 위치를 지정
		if (guardmarine.size() > 0){
			for (int i = 0; i < guardmarine.size(); i++){
				if (i < 5){
					if (guardmarine[i]->getSelf()->exists()){
						holdMarine[guardmarine[i]] = marinePosition[i];
						guardmarine[i]->setState(Marine::STATE::HOLD);
						MarineControl(guardmarine[i]);
					}
				}
			}
		}*/
	/*	int i = 0;
		for (vector<Marine*>::iterator iter = guardmarine.begin(); iter != guardmarine.end();){
			if (i < 5){
				Marine* m = (*iter);
				if (m->getSelf()->exists()){
					holdMarine[m] = marinePosition[i];
					m->setState(Marine::STATE::HOLD);
					MarineControl(guardmarine[i]);
					i++;
				}
				else{//dead
					iter = guardmarine.erase(iter);
				}
			}
		}*/
	}

	// SCV 의 행동을 수행
	for ( auto scv : guardSCV ){
		scvControl(scv);
	}
	for (auto marine : guardmarine){
		MarineControl(marine);
	}
	
	
	
}

void Invasion_pylon::scvControl(SCV* scv){

	switch (scv->getState()){
	case SCV::STATE::IDLE:
		break;

	case SCV::STATE::MOVE:
		if (isAlmostReachedDest(holdSCV[scv], scv->getSelf()->getPosition(), 10, 10)){
			scv->orderWait();
		}
		else{
			scv->getSelf()->move(holdSCV[scv]);
		}

		break;

	case SCV::STATE::WAIT:
		scv->getSelf()->move(holdSCV[scv]);

		break;

	case SCV::STATE::REPAIR:
		
		if (guardSCV[0]->getSelf()->exists())
		{
			if (guardSCV[0]->getSelf()->getHitPoints() < 60){
				scv->getSelf()->repair(guardSCV[0]->getSelf());
			}
			else{
				scv->getSelf()->move(holdSCV[scv]);
			}
		}
		else{
			//
			scv->getSelf()->move(holdSCV[scv]);
		}

		break;

	case SCV::STATE::ETC:

		if (pylon != nullptr){
			scv->getSelf()->attack(pylon);
		}
		else{
			if (scv->getSelf()->isCarryingMinerals()){
				scv->getSelf()->returnCargo(true);
			}
			else if (scv->getSelf()->isIdle() || scv->getSelf()->isGatheringGas()){
				scv->getSelf()->gather(scv->getTarget());
			}
		}

		break;
	}
}
void Invasion_pylon::MarineControl(Marine* marine){
	Unit t_marine = marine->getSelf();
	switch (marine->getState()){
	case Marine::STATE::MOVE:
		t_marine->move(holdMarine[marine]);
		break;
	case Marine::STATE::HOLD:
		if (isAlmostReachedDest(t_marine->getPosition(), holdMarine[marine], 0, 0)){
			t_marine->holdPosition();

		}
		else{
			t_marine->move(holdMarine[marine]);
		}
		break;
	case Marine::STATE::ATTACK:
		if (pylon != nullptr){
			if (!t_marine->isAttackFrame()){
				t_marine->attack(pylon);
			}
		}
		else{
			if (ZealotDetect()){
				if (t_marine->isUnderAttack()){
					if (isAlmostReachedDest(t_marine->getPosition(), holdMarine[marine], 0, 0)){
						t_marine->holdPosition();

					}
					else{
						t_marine->move(holdMarine[marine]);
					}
				}
				else{
					t_marine->patrol(guardSCV[0]->getSelf()->getPosition());
				}
			}
		}
		break;
	case Marine::STATE::WAIT:
		t_marine->move(holdMarine[marine]);
		break;
	}
}
bool Invasion_pylon::ZealotDetect(){

	bool zealotFlag = false;

	for (Unit u : Broodwar->enemy()->getUnits()){
		if (u->getType() == UnitTypes::Protoss_Zealot){
			zealotFlag = true;

			break;
		}
	}

	return zealotFlag;
}

