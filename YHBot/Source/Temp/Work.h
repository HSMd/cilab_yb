#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include "MyUnit.h"

using namespace std;
using namespace BWAPI;
using namespace Filter;

struct Resource{
	int mineral;
	int gas;
	int supply;
};

class BasicWork{
protected:
	UnitType targetType;

public:
	BasicWork(UnitType type){
		targetType = type;
	}

	~BasicWork() {};

	virtual const UnitType& getTargetType(){
		return targetType;
	}
};

class UnitWork : public BasicWork{

	Unit trainer;

public:
	UnitWork(UnitType type) : BasicWork(type){ trainer = nullptr; }
	UnitWork(UnitType type, Unit trainer) : BasicWork(type){ this->trainer = trainer; }

	Unit getTrainer() { return trainer; }
};

class BuildingWork : public BasicWork{

protected:
	TilePosition targetPos;
	SCV* builder;
	int pre;

public:

	BuildingWork(UnitType type, TilePosition pos, int pre) : BasicWork(type){
		targetPos = pos;
		builder = nullptr;
		this->pre = pre;
	};

	BuildingWork(UnitType type, SCV* scv, TilePosition pos, int pre) : BasicWork(type){
		targetPos = pos;
		builder = scv;
		this->pre = pre;
	};

	~BuildingWork(){};

	SCV* getBuilder(){
		return builder;
	}

	void setBuilder(SCV* builder){
		this->builder = builder;
	}

	const TilePosition& getTilePosition(){
		return targetPos;
	}

	void setTilePosition(TilePosition pos){
		this->targetPos = pos;
	}

	int getPre(){ return pre; };
};

///////////////////////

class scheduleWork : public BuildingWork{

	Unit interrupt;
	Unit trainer;
	UnitType cmpType;
	int count;
	int cmpCount;
	bool isFired;
	bool immediatly;

public:
	scheduleWork(UnitType targetType, int count, UnitType cmpType, int cmpCount = 0, bool immediatly = false) : BuildingWork(targetType, TilePosition(0, 0), 45){
		this->cmpType = cmpType;
		this->cmpCount = cmpCount;
		interrupt = nullptr;
		trainer = nullptr;
		isFired = false;
		this->immediatly = immediatly;
		this->count = count;
	};

	scheduleWork(UnitType targetType, TilePosition targetPos, UnitType cmpType) : BuildingWork(targetType, targetPos, 45){
		this->cmpType = cmpType;
		cmpCount = 0;
		interrupt = nullptr;
		trainer = nullptr;
		isFired = false;
		immediatly = false;
	};

	scheduleWork(UnitType targetType, TilePosition targetPos, UnitType cmpType, int cmpCount, bool immediatly = false) : BuildingWork(targetType, targetPos, 45){
		this->cmpType = cmpType;
		this->cmpCount = cmpCount;
		interrupt = nullptr;
		trainer = nullptr;
		isFired = false;
		this->immediatly = immediatly;
	};

	scheduleWork(UnitType targetType, TilePosition targetPos, int pre, UnitType cmpType, int cmpCount, bool immediatly = false) : BuildingWork(targetType, targetPos, pre){
		this->cmpType = cmpType;
		this->cmpCount = cmpCount;
		interrupt = nullptr;
		trainer = nullptr;
		isFired = false;
		this->immediatly = immediatly;
	};

	scheduleWork(UnitType targetType, TilePosition targetPos, Unit trainer, UnitType cmpType, int cmpCount, bool immediatly = false) : BuildingWork(targetType, targetPos, 45){
		this->cmpType = cmpType;
		this->cmpCount = cmpCount;
		interrupt = nullptr;
		this->trainer = trainer;
		isFired = false;
		this->immediatly = immediatly;
	};

	scheduleWork(UnitType targetType, TilePosition targetPos, UnitType cmpType, int cmpCount, SCV* scv, bool immediatly = false) : BuildingWork(targetType, scv, targetPos, 45){
		this->cmpType = cmpType;
		this->cmpCount = cmpCount;
		interrupt = nullptr;
		trainer = nullptr;
		isFired = false;
		this->immediatly = immediatly;
	};

	~scheduleWork(){};

	bool canFire();
	bool getFire() { return isFired; };
	void onFire() { isFired = true; };
	void closeFire() { isFired = false; };
	int getCount() { return count; };
	void setInterrupt(Unit inter) { interrupt = inter; };
	Unit getInterrupt() { return interrupt; };
	Unit getTrainer(){ return trainer; };
};