#include "Territory.h"

#include "../Single.h"
#include "UtilMgr.h"

//////////////

float getDistanceF(Position cmp1, Position cmp2){
	return sqrt((float)pow((cmp1.x - cmp2.x), 2) + (float)pow((cmp1.y - cmp2.y), 2));
}

bool orderByDistance(Unit a, Unit b){

	float dis1, dis2;

	switch (mapInfo->getDir()){
		case 0:
		case 1:

			dis1 = getDistanceF(mapInfo->getMyStartBase()->getPosition() + Position(16, 0), a->getPosition() + Position(-32, 0));
			dis2 = getDistanceF(mapInfo->getMyStartBase()->getPosition() + Position(16, 0), b->getPosition() + Position(-32, 0));

			break;
		
		case 2:
		case 3:

			dis1 = getDistanceF(mapInfo->getMyStartBase()->getPosition() + Position(-16, 0), a->getPosition() + Position(32, 0));
			dis2 = getDistanceF(mapInfo->getMyStartBase()->getPosition() + Position(-16, 0), b->getPosition() + Position(32, 0));

			break;
	}

	//dis1 = getDistanceF(mapInfo->getMyStartBase()->getPosition(), a->getPosition());
	//dis2 = getDistanceF(mapInfo->getMyStartBase()->getPosition(), b->getPosition());

	if (dis1 < dis2){
		return true;
	}

	return false;
}


//////////////

Territory::Territory()
{
	idle = false;
	cmdCenter = nullptr;
	gas = nullptr;
	gasSCVCount = 3;
}

Territory::Territory(BaseLocation* base){

	idle = false;
	cmdCenter = nullptr;
	gas = nullptr;
	basePosition = base->getTilePosition();
}


Territory::Territory(BaseLocation* base, MyUnit* cmdCenter){

	gas = nullptr;
	init(base, cmdCenter);
}

Territory::~Territory()
{

}

// //

void Territory::show(){

}

void Territory::init(BaseLocation* base, MyUnit* cmdCenter){

	auto gases = base->getGeysers();
	Unit g = *gases.begin();

	basePosition = base->getTilePosition();
	gasPos = g->getTilePosition();

	for (auto m : base->getMinerals()){
		this->minerals.push_back(m);
	}

	sort(minerals.begin(), minerals.end(), orderByDistance);

	this->cmdCenter = dynamic_cast<Structure*>(cmdCenter);
	this->scvCount = base->getMinerals().size() * 2 + base->getGeysers().size() * 3;

	idle = true;
}

void Territory::run(){

	// CheckGas
	if (gas != nullptr && resourceCount[gas] < gasSCVCount){
		int count = gasSCVCount - resourceCount[gas];

		for (int i = 0; i < count; i++){
			SCV* scv = getWorker(true);
			if (scv == nullptr){
				break;
			}

			orderGas(scv, gas);
			gasSCV.push_back(scv);
		}
	}

	// Work SCV RUN
	for (auto iter = workSCV.begin(); iter != workSCV.end();++iter){
		SCV* scv = (*iter);

		if (!scv->getSelf()->isCompleted()){
			continue;
		}

		if (!scv->getSelf()->exists()){
			// 누군가가 대신 해줘야됨.
			workSCV.erase(iter);
			iter--;
			continue;
		}

		workSCVControl(scv);

		if (scv->getState() == SCV::STATE::IDLE){
			iter = workSCV.erase(iter);
			iter--;

			addSCV(scv);
		}
	}

	// Mineral SCV RUN
	for (auto iter = mineralSCV.begin(); iter != mineralSCV.end(); ++iter){
		auto scv = (*iter);

		// Die
		if (!scv->getSelf()->isCompleted()){
			continue;
		}

		if (!scv->getSelf()->exists()){
			resourceCount[scv->getTarget()]--;
			mineralSCV.erase(iter);
			iter--;	
			continue;
		}

		if (scv->getState() == SCV::STATE::IDLE){
			getProperBasicWork(scv);
		}

		resourceSCVControl(scv);
	}

	/*
	// Mineral SCV RUN
	for (auto& scv : mineralSCV){
		// Die
		if (!scv->getSelf()->exists() || !scv->getSelf()->isCompleted()){
			continue;
		}

		if (scv->getState() == SCV::STATE::IDLE){
			getProperBasicWork(scv);
		}

		resourceSCVControl(scv);
	}
	*/

	// GAS SCV RUN
	for (auto iter = gasSCV.begin(); iter != gasSCV.end(); ++iter){
		auto scv = (*iter);

		// Die
		if (!scv->getSelf()->isCompleted()){
			continue;
		}

		if (!scv->getSelf()->exists()){
			resourceCount[scv->getTarget()]--;
			gasSCV.erase(iter);
			iter--;
			continue;
		}

		resourceSCVControl(scv);
	}

	/*
	for (auto& scv : gasSCV){

		// Die
		if (!scv->getSelf()->exists() || !scv->getSelf()->isCompleted()){
			continue;
		}

		resourceSCVControl(scv);
	}
	*/

	// Over SCV RUN
	for (auto iter = overSCV.begin(); iter != overSCV.end(); ++iter){
		auto scv = (*iter);

		// Die
		if (!scv->getSelf()->isCompleted()){
			continue;
		}

		if (!scv->getSelf()->exists()){
			resourceCount[scv->getTarget()]--;
			overSCV.erase(iter);
			iter--;
			continue;
		}

		if (scv->getState() == SCV::STATE::IDLE){
			getProperBasicWork(scv);
		}

		resourceSCVControl(scv);
	}

	/*
	for (auto& scv : overSCV){
		// Die
		if (!scv->getSelf()->exists() || !scv->getSelf()->isCompleted()){
			continue;
		}

		if (scv->getState() == SCV::STATE::IDLE){
			getProperBasicWork(scv);
		}

		resourceSCVControl(scv);
	}
	*/
}

void Territory::resourceSCVControl(SCV* scv){

	Unit self = scv->getSelf();
	Unit target = scv->getTarget();

	switch (scv->getState()){

	case SCV::STATE::IDLE:

		scv->setError(false);

		break;

	case SCV::STATE::MINERAL:
		
		if (self->isCarryingMinerals() && self->isCarryingGas()){
			self->returnCargo(true);
		}
		else if (self->isIdle() || self->isGatheringGas()){
			self->gather(target);		
		}

		break;

	case SCV::STATE::GAS:

		if (self->isCarryingGas()){
			self->returnCargo(true);
		}
		else{
			if (self->isIdle()){
				self->gather(target);
			}
		}

		break;

	case SCV::STATE::ETC:
		break;

	default:
		break;
	}
}

void Territory::workSCVControl(SCV* scv){

	Unit self = scv->getSelf();
	UnitType targetType = scv->getTargetType();
	TilePosition targetPos = scv->getTargetPos();

	switch (scv->getState()){

	case SCV::STATE::MOVE:
		//if (isAlmostReachedDest(tile2pos(targetPos), self->getPosition(), BWAPI::TILEPOSITION_SCALE * 4, BWAPI::TILEPOSITION_SCALE * 4)){
		if (getDistance(targetPos, self->getTilePosition()) < 1){
			scv->orderWait();
		}
		else{
			self->move(tile2pos(targetPos));
		}

		break;

	case SCV::STATE::WAIT:
		self->stop();
		
		break;

	case SCV::STATE::RETURN:

		if (self->isConstructing())
			break;

		if (getDistance(self->getTilePosition(), basePosition) < 13){
			scv->orderIDLE();
		}
		else{
			self->move(tile2pos(basePosition));
		}

		break;

	case SCV::STATE::BUILD:
		/*
		if (self->isCarryingMinerals()){
			self->returnCargo();
		}
		else{
			if (getDistance(self->getTilePosition(), targetPos) < 10){
				if (!self->isConstructing()){
					if (self->build(targetType, targetPos)){
						scv->setError(true);
					}
				}
			}
			else{
				self->move(tile2pos(targetPos));
			}
		}
		*/

		if (getDistance(self->getTilePosition(), targetPos) < 11){
			if (!self->isConstructing()){
				if (self->build(targetType, targetPos)){
					scv->setError(true);
				}
			}
		}
		else{
			self->move(tile2pos(targetPos));
		}

		break;

	default:
		break;
	}

	if (scv->getState() == SCV::STATE::WAIT){
		self->build(targetType);
	}
}

// Work //

SCV* Territory::getGasWorker(){
	SCV* re = nullptr;

	for (auto iter = gasSCV.begin(); iter != gasSCV.end(); ++iter){
		SCV* scv = (*iter);
		Unit unit = scv->getSelf();
		
		if (!unit->exists()){
			continue;
		}
		
		re = scv;
		resourceCount[re->getTarget()]--;
		gasSCV.erase(iter);
		re->orderETC();

		break;
	}

	return re;
}

SCV* Territory::getWorker(bool except){

	// True = 이 영토에서 제외
	// False = 작업하고 반환해야되니까 Work로

	SCV* re = nullptr;
	bool carry = false;
	vector<SCV*>::iterator delIter;


	for (auto iter = mineralSCV.begin(); iter != mineralSCV.end(); ++iter){
		SCV* scv = (*iter);

		if (!scv->getSelf()->exists() || !scv->getSelf()->isCompleted()){
			continue;
		}

		if (scv->getState() == SCV::STATE::MINERAL){
			if (re != nullptr){
				if (!scv->getSelf()->isCarryingMinerals()){
					re = scv;
					delIter = iter;
				}					
			}
			else{
				re = scv;
				delIter = iter;
			}			
		}
	}

	if (re != nullptr) {
		resourceCount[re->getTarget()]--;
		mineralSCV.erase(delIter);
		re->orderETC();

		if (!except) {
			workSCV.push_back(re);
		}
	}	

	return re;
}

SCV* Territory::getWorker(TilePosition targetPos, bool except){

	// True = 이 영토에서 제외
	// False = 작업하고 반환해야되니까 Work로
	SCV* re = nullptr;
	vector<SCV*>::iterator delIter;
	bool isMineralWorker = false;
	int minDis = 99999;

	/*
	for (auto iter = workSCV.begin(); iter != workSCV.end(); ++iter){
		SCV* scv = (*iter);
		
		if (scv->getSelf()->isConstructing()){
			continue;
		}

		if (scv->getState() == SCV::STATE::RETURN){
			int dis = getDistance(scv->getSelf()->getTilePosition(), targetPos);
			if (dis < minDis){
				minDis = dis;
				re = scv;
				delIter = iter;
			}
		}
	}
	*/

	for (auto iter = mineralSCV.begin(); iter != mineralSCV.end(); ++iter){
		SCV* scv = (*iter);

		if (!scv->getSelf()->exists() || !scv->getSelf()->isCompleted()){
			continue;
		}

		if (scv->getState() == SCV::STATE::MINERAL){

			int dis = getDistance(scv->getSelf()->getTilePosition(), targetPos);
			if (dis < minDis){
					isMineralWorker = true;

					minDis = dis;
					re = scv;
					delIter = iter;
			} 
		}
	}

	if (isMineralWorker){
		if (re != nullptr) {
			resourceCount[re->getTarget()]--;
			mineralSCV.erase(delIter);
			re->orderETC();

			if (!except) {
				workSCV.push_back(re);
			}
		}		
	}
	
	return re;
}

// Resource //

Unit Territory::getIdleMineral(){

	Unit re = nullptr;
	int minCount = 200;

	for (auto m : minerals){
		if (!m->exists()){
			continue;
		}

		if (resourceCount[m] < minCount){

			minCount = resourceCount[m];
			re = m;
		}
	}

	return re;
}

Unit Territory::getIdleGas(){
		
	if (gas != nullptr){
		if (resourceCount[gas] < 3){
			return gas;
		}
	}

	return nullptr;
}

void Territory::orderMineral(SCV* scv, Unit target){

	scv->orderMineral(target);
	resourceCount[target]++;
}

void Territory::orderGas(SCV* scv, Unit target){

	scv->orderGas(target);
	resourceCount[target]++;
}

bool Territory::getProperBasicWork(SCV* scv){
		
	// Mineral
	Unit target = getIdleMineral();
	if (target != nullptr){

		orderMineral(scv, target);

		return true;
	}

	return false;
}

void Territory::addSCV(MyUnit* newSCV, bool re){

	SCV* scv = dynamic_cast<SCV*>(newSCV);
	//scv->orderIDLE();
	
	if (0 < getRequireSCVCount()){
		if (re){
			scv->orderReturn();
			workSCV.push_back(scv);
		}
		else{
			getProperBasicWork(scv);
			mineralSCV.push_back(scv);
		}				
	}
	else{	
		overSCV.push_back(scv);
	}
}

void Territory::addSCV(SCV* newSCV, bool re){
	
	if (0 < getRequireSCVCount()){
		if (re){
			newSCV->orderReturn();
			workSCV.push_back(newSCV);
		}
		else{
			getProperBasicWork(newSCV);
			mineralSCV.push_back(newSCV);
		}				
	}
	else{
		overSCV.push_back(newSCV);
	}
	//newSCV->orderIDLE();
	//mineralSCV.push_back(newSCV);
}

void Territory::gasCompleted(Unit gas){

	this->gas = gas;
}

void Territory::moveTerritory(Territory* territory){

	for (auto iter = overSCV.begin(); iter != overSCV.end(); ++iter){
		territory->addSCV(*iter, true);
	}

	overSCV.clear();
}

void Territory::deleteScv(SCV* scv){


}