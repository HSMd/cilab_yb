#pragma once
#include "Work.h"
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"

class TwoFactory
{
	enum FLAG{
		UPGRADE_RACE_SPEED,
		UPGRADE_MINE,
		UPGRADE_SIEGE_MODE,
		UPGRADE_GONG,

		ENTRANCE_OPEN,
		ENTRANCE_CLOSE,

		ENGINEERING_BAY,
		ADVANCE_ALL,

		EXTAND_FRONT, 
		ADD_FACTORY,

		GAS_CONTROL_1,
	};

	TilePosition outPos;
	list<scheduleWork> build;	
	TerritoryMgr* tMgr;

	map<int, bool> flags;
	map<int, int> strategyFlag;

	SCV* out;
	Structure* moveBrk;

	int frameCount;
	int factoryFrameCount;

public:
	TwoFactory();
	~TwoFactory();

	void chkStrategy();
	void run(COMBAT_FLAG combat);
	void errorSCV();

	bool getIsEnd();
};

