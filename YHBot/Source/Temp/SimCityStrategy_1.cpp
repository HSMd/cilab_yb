#include "SimCityStrategy_1.h"
#include "../Single.h"
#include "Scout.h"
#include "UtilMgr.h"

SimStrategy_1::SimStrategy_1(){

	frameCount = 0;
	pylon = nullptr;

	switch (mapInfo->getDir()){
	case 0:
		// pylonRush
		pylonPosition.push_back(tile2pos(TilePosition(93, 25)));
		pylonPosition.push_back(tile2pos(TilePosition(99, 10)));
		pylonPosition.push_back(tile2pos(TilePosition(100, 11)));
		pylonPosition.push_back(tile2pos(TilePosition(98, 12)) + Position(16, 0));

		marinePosition.push_back(tile2pos(TilePosition(100, 6)));
		marinePosition.push_back(tile2pos(TilePosition(101, 6)));
		marinePosition.push_back(tile2pos(TilePosition(103, 8)));
		marinePosition.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
		marinePosition.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));

		break;

	case 1:
		// pylonRush
		pylonPosition.push_back(tile2pos(TilePosition(100, 90)));
		pylonPosition.push_back(tile2pos(TilePosition(115, 98)) + Position(16, 16));
		pylonPosition.push_back(tile2pos(TilePosition(116, 98)) + Position(16, 0));
		pylonPosition.push_back(tile2pos(TilePosition(115, 97)) + Position(16, 16));

		marinePosition.push_back(tile2pos(TilePosition(121, 99)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 100)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(118, 102)) + Position(16, 16));
		break;

	case 2:
		// pylonRush
		pylonPosition.push_back(tile2pos(TilePosition(27, 37)));
		pylonPosition.push_back(tile2pos(TilePosition(11, 30)));
		pylonPosition.push_back(tile2pos(TilePosition(10, 30)) + Position(16, 16));
		pylonPosition.push_back(tile2pos(TilePosition(11, 31)));

		marinePosition.push_back(tile2pos(TilePosition(7, 26)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(11, 26)));
		marinePosition.push_back(tile2pos(TilePosition(14, 27)) + Position(0, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 0));
		break;

	case 3:
		// pylonRush
		pylonPosition.push_back(tile2pos(TilePosition(36, 102)));
		pylonPosition.push_back(tile2pos(TilePosition(27, 118)));
		pylonPosition.push_back(tile2pos(TilePosition(28, 119)));
		pylonPosition.push_back(tile2pos(TilePosition(28, 118)) + Position(16, 0));

		marinePosition.push_back(tile2pos(TilePosition(23, 117)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(24, 121)));
		marinePosition.push_back(tile2pos(TilePosition(27, 123)));
		marinePosition.push_back(tile2pos(TilePosition(28, 123)));
		marinePosition.push_back(tile2pos(TilePosition(31, 121)) + Position(16, 16));
		break;
	}
}

SimStrategy_1::~SimStrategy_1(){

}

void SimStrategy_1::run(COMBAT_FLAG combat){

	if (combat == COMBAT_FLAG::NORMAL){
		// 특정 위치에서 대기.
		normal();
	}
	else if (combat == COMBAT_FLAG::ZEALOT_RUSH){

		guardZealotRush();		
	}
	else if (combat == COMBAT_FLAG::INVASION_PYLON){

		// 기존 코드를 이용.
		guardPylonRush();
	}
	else if (combat == COMBAT_FLAG::ENEMY_SEARCH){

		// 정찰 왔을 경우에 대한 대처. - 1마리당 1SCV
		updateCloseEnemy();
		guardEnemySearch();
	}
}

void SimStrategy_1::normal(){

	// 마린 1마리

	end();

	//마린 위치 대기
	int count = 0;
	for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
		if ((*q)->rPos.x == 0 && (*q)->rPos.y == 0) {
			if (count < marinePosition.size())
			{
				if (isAlmostReachedDest((*q)->getSelf()->getPosition(), marinePosition[count], 2, 2))
				{
					if (!(*q)->getSelf()->isHoldingPosition())
					{
						(*q)->getSelf()->holdPosition();
					}
				}
				else{
					(*q)->getSelf()->move(marinePosition[count]);
				}
			}
			else
			{
				if (!isAlmostReachedDest((*q)->getSelf()->getPosition(), marinePosition[0], 2, 2))
				{
					(*q)->getSelf()->move(marinePosition[0]);
				}
			}
		}
		count++;
	}
}

void SimStrategy_1::guardPylonRush(){

	isInvasion();

	// 기존 코드를 이용
	if (pylon != nullptr)
	{
		//SCV
		for (auto iter = guardSCV.begin(); iter != guardSCV.end();)
		{
			if (!(*iter)->getSelf()->exists())
			{
				iter = guardSCV.erase(iter);
			}
			else{
				iter++;
			}

		}

		for (int i = guardSCV.size(); i < 3; i++)
		{
			SCV* scv = cmd->getTerritoryMgr().getAllTerritorys()[0]->getWorker(true);
			if (scv != nullptr)
			{
				guardSCV.push_back(scv);
			}
		}

		for (int i = 0; i < 3; i++)
		{
			SCV *scv = guardSCV[i];
			if (scv != nullptr && holdSCV[scv] == Position(0, 0))
			{
				holdSCV[scv] == pylonPosition[i];

				if (i == 0)
				{
					scv->getSelf()->move(holdSCV[scv]);
				}
				else
				{
					if (pylon != nullptr)
					{
						if (!scv->getSelf()->isAttacking())
						{
							scv->getSelf()->attack(pylon);
						}
					}
				}
			}
		}
		//Marine
		for (auto marine : UnitControl::getInstance()->marines)
		{
			if ((*marine).getSelf()->exists())
			{
				if (!(*marine).getSelf()->isAttacking())
				{
					if (bw->getFrameCount() % 24 == 0)
					{
						(*marine).getSelf()->attack(pylon);
					}
				}
			}
		}
	}	
}

void SimStrategy_1::guardEnemySearch(){
		
	int size = getCloseEnemyCount(UnitTypes::Protoss_Probe) - scvs.size();
	auto startTerritory = cmd->getTerritoryMgr().getAllTerritorys()[0];

	if (closeEnemy.empty()){

		for (auto iter = scvs.begin(); iter != scvs.end(); ++iter){
			SCV* scv = (*iter);
			startTerritory->addSCV(scv, true);
		}

		closeEnemy.clear();
		scvs.clear();
	}

	// SCV 추가.
	for (int i = 0; i < size; i++){
		auto scv = startTerritory->getWorker(true);

		if (scv != nullptr){
			scvs.push_back(scv);
		}
	}
	
	// SCV 공격.
	for (auto iter = scvs.begin(); iter != scvs.end(); ++iter){
		auto scv = (*iter);

		if (!scv->getSelf()->exists()){
			iter = scvs.erase(iter);
			iter--;
			continue;
		}

		scv->getSelf()->attack(getAttackTarget());
	}
}

void SimStrategy_1::guardZealotRush(){

	auto state = cmd->getStateMgr();

	if (state.getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1) == false){

		// SCV 들여보내면서 3마리로 입구 막기.
		// 마린은 파일런을 공격.

	}
	else{

		// 탱크들이 가장 아프게 때릴수 있는 곳에 때림.

	}
}

void SimStrategy_1::isInvasion()
{
	for (auto p : bw->enemy()->getUnits())
	{
		if (p->getType() == UnitTypes::Protoss_Pylon)
		{
			switch (mapInfo->getDir())
			{
			case 0:
				if (isContainOneTerm(TilePosition(99, 6), p->getTilePosition(), 7)){
					pylon = p;
				}
				else if (getDistance(mapInfo->getMyStartBase()->getTilePosition(), p->getTilePosition()) <= 20)
				{
					pylon = p;
				}
				break;

			case 1:
				if (isContainOneTerm(TilePosition(113, 99), p->getTilePosition(), 7))
				{
					pylon = p;
				}
				else if (getDistance(mapInfo->getMyStartBase()->getTilePosition(), p->getTilePosition()) <= 20)
				{
					pylon = p;
				}
				break;

			case 2:
				if (isContainOneTerm(TilePosition(6, 24), p->getTilePosition(), 7)){
					pylon = p;
				}
				else if (getDistance(mapInfo->getMyStartBase()->getTilePosition(), p->getTilePosition()) <= 20)
				{
					pylon = p;
				}
				break;

			case 3:
				if (isContainOneTerm(TilePosition(22, 116), p->getTilePosition(), 7)){
					pylon = p;
				}
				else if (getDistance(mapInfo->getMyStartBase()->getTilePosition(), p->getTilePosition()) <= 20)
				{
					pylon = p;
				}
				break;
			}
		}
	}
}

void SimStrategy_1::end(){

	// 자료구조 초기화 및 SCV를 TRUE로 가져왔을 경우 addSCV 해줄 것.

	auto startTerritory = cmd->getTerritoryMgr().getAllTerritorys()[0];

	for (auto iter = scvs.begin(); iter != scvs.end(); ++iter){
		SCV* scv = (*iter);
		startTerritory->addSCV(scv, true);
	}
	for (auto iter = guardSCV.begin(); iter != guardSCV.end(); ++iter)
	{
		SCV* scv = (*iter);
		startTerritory->addSCV(scv,true);
	}
	scvs.clear();
	closeEnemy.clear();
	guardSCV.clear();
}

void SimStrategy_1::updateCloseEnemy(){
	
	auto base = mapInfo->getMyStartBase();
	auto enemyUnits = bw->enemy()->getUnits();

	for (auto iter = enemyUnits.begin(); iter != enemyUnits.end(); ++iter){
		Unit unit = *(iter);
		int dis = getDistance(base->getTilePosition(), unit->getTilePosition());

		if (!chkAlreadyEnemy(unit) && getDistance(base->getTilePosition(), unit->getTilePosition()) < 30){

			closeEnemy.push_back(unit);
		}
	}

	for (auto iter = closeEnemy.begin(); iter != closeEnemy.end(); ++iter){
		Unit unit = *(iter);
		int dis = getDistance(base->getTilePosition(), unit->getTilePosition());

		if (18 < dis || !unit->exists()){
			iter = closeEnemy.erase(iter);
			iter--;
			continue;
		}
	}
}

bool SimStrategy_1::chkAlreadyEnemy(Unit unit){

	for (auto close : closeEnemy){
		if (close->getID() == unit->getID()){
			return true;
		}
	}

	return false;
}

int SimStrategy_1::getCloseEnemyCount(UnitType type){

	int cnt = 0;
	
	for (auto close : closeEnemy){
		if (close->getType() == type){
			cnt++;
		}
	}

	return cnt;
}

Unit SimStrategy_1::getAttackTarget(){
	
	Unit re = nullptr;

	for (auto unit : closeEnemy){
		Unit enemyTarget = unit->getTarget();

		if (re == nullptr){
			re = unit;
			continue;
		}

		if (enemyTarget != nullptr && enemyTarget->isConstructing()){
			re = unit;
			break;
		}		
	}

	return re;
}

void SimStrategy_1::draw(COMBAT_FLAG combat) {
	
	
	if (combat == COMBAT_FLAG::NORMAL) {
		Position p1, p2;
		
		// 특정 위치에서 대기.
		switch (mapInfo->getDir())
		{
		case 0:
			p1.x = tile2pos(TilePosition(99, 6)).x;
			p1.y = tile2pos(TilePosition(99, 6)).y;
			p2.x = tile2pos(TilePosition(99 + 7, 6 + 7)).x;
			p2.y = tile2pos(TilePosition(99 + 7, 6 + 7)).y;
			bw->drawBoxMap(p1,p2,Colors::Orange,false);
			break;

		case 1:
			p1.x = tile2pos(TilePosition(112, 97)).x;
			p1.y = tile2pos(TilePosition(112, 97)).y;
			p2.x = tile2pos(TilePosition(112 + 7, 97 + 7)).x;
			p2.y = tile2pos(TilePosition(112 + 7, 97 + 7)).y;
			bw->drawBoxMap(p1, p2, Colors::Orange, false);
			break;

		case 2:
			p1.x = tile2pos(TilePosition(6, 24)).x;
			p1.y = tile2pos(TilePosition(6, 24)).y;
			p2.x = tile2pos(TilePosition(6 + 7, 24 + 7)).x;
			p2.y = tile2pos(TilePosition(6 + 7, 24 + 7)).y;
			bw->drawBoxMap(p1, p2, Colors::Orange, false);
			break;

		case 3:
			p1.x = tile2pos(TilePosition(22, 116)).x;
			p1.y = tile2pos(TilePosition(22, 116)).y;
			p2.x = tile2pos(TilePosition(22 + 7, 116 + 7)).x;
			p2.y = tile2pos(TilePosition(22 + 7, 116 + 7)).y;
			bw->drawBoxMap(p1, p2, Colors::Orange, false);
			break;

		}

	}
	else if (combat == COMBAT_FLAG::ZEALOT_RUSH) {

	}
	else if (combat == COMBAT_FLAG::INVASION_PYLON) {

		// 기존 코드를 이용.
	}
	else if (combat == COMBAT_FLAG::ENEMY_SEARCH) {

		// 정찰 왔을 경우에 대한 대처. - 1마리당 1SCV
	}
}

