#include "SCVControl.h"


void SCVControl::run(SCV* unit) {
	if (unit->getState() == SCV::STATE::IDLE) {
		//Nothing
	}
	else if (unit->getState() == SCV::STATE::HELP) {
		SCVControl::getInstance()->HELP_Control(unit);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// SCV Control

SCVControl* SCVControl::s_SCVControl;
SCVControl* SCVControl::create() {
	if (s_SCVControl)
	{
		return s_SCVControl;
	}
	s_SCVControl = new SCVControl();
	return s_SCVControl;
}
SCVControl* SCVControl::getInstance() {
	return s_SCVControl;
}
SCVControl::SCVControl() {

}

SCVControl::~SCVControl() {}

void SCVControl::onFrame() {


}


void SCVControl::WAIT_Control(SCV* unit) {

}

void SCVControl::HELP_Control(SCV* unit) {

	Unit targetUnit = getBestUnitToHelp(unit->getSelf());
	if (targetUnit != nullptr) {
		unit->getSelf()->attack(targetUnit);
	}
}

Unit SCVControl::getBestUnitToHelp(Unit unit) {
	Unit bestUnit = nullptr;

	//아군의 표적이 되는 유닛을 전부 불러온다음, 거리값 계산
	Unitset near_units = unit->getUnitsInRadius(100, Filter::IsOwned);

	list<Unit> targetUnits;

	for (auto u = near_units.begin(); u != near_units.end(); u++) {
		if ((*u)->getOrderTarget != nullptr) {
			targetUnits.push_back((*u)->getOrderTarget());
		}
		else if ((*u)->getTarget() != nullptr) {
			targetUnits.push_back((*u)->getTarget());
		}
	}

	int minDist = 99999;

	for (auto u = targetUnits.begin(); u != targetUnits.end(); u++) {
		if (getDistance(unit->getPosition(), (*u)->getPosition()) < minDist) {
			minDist = getDistance(unit->getPosition(), (*u)->getPosition());
			bestUnit = (*u);
		}
	}
}
