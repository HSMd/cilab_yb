#include <list>
#include <vector>
#include <math.h>
#include "VultureControl.h"
#include "CombatInfo.h"
#include "UnitControl.h"
#include "UtilMgr.h"
#include "../Single.h"


VultureUnit::VultureUnit(Unit _self) : MyUnit(_self) {
	state = VultureUnit::STATE::IDLE;
	attackTarget = NULL;
	isMining = false;
	spiderNumber = 3;
	isMove = false;
	safe = true;
	cannon = NULL;
	rPos = Position(0, 0);
}

VultureUnit::~VultureUnit() {}

void VultureControl::run(VultureUnit* unit, bool kiting) {
	if (unit->getState() == VultureUnit::STATE::IDLE) {
		//VultureControl::getInstance()->goodLuck(unit);
		//printf("벌처 할일 지정\n");
	}
	else if (unit->getState() == VultureUnit::STATE::NM_MINE_BACK) {
		VultureControl::getInstance()->MB_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::NM_MINE_FRONT) {
		VultureControl::getInstance()->MF_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::NM_WAIT) {
		VultureControl::getInstance()->WAIT_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::NM_ATTACK) {
		VultureControl::getInstance()->ATTACK_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::NM_DEFENSE) {
		VultureControl::getInstance()->DEFENSE_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::NM_SQUEEZE) {
		VultureControl::getInstance()->SQUEEZE_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::NM_RETURNTOBASE) {
		VultureControl::getInstance()->RTB_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::NM_MINE_MULTI) {
		VultureControl::getInstance()->MM_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::NM_CRAZY) {
		VultureControl::getInstance()->CRAZY_Control(unit, kiting);
	}
	else if (unit->getState() == VultureUnit::STATE::NM_SCOUT) {
		VultureControl::getInstance()->SCOUT_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::TF_JUNJIN) {
		VultureControl::getInstance()->TF_JUNJIN_Control(unit, kiting);
	}
	else if (unit->getState() == VultureUnit::STATE::TF_MINE_FRONT) {
		VultureControl::getInstance()->TF_FM_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::TF_SQUEEZE) {
		VultureControl::getInstance()->TF_SQUEEZE_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::TF_CRAZY) {
		VultureControl::getInstance()->TF_CRAZY_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::KJ_ATTACK) {
		VultureControl::getInstance()->KJ_ATTACK_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::KJ_MINE) {
		VultureControl::getInstance()->KJ_MINE_Control(unit);
	}
	else if (unit->getState() == VultureUnit::STATE::KJ_WAIT) {
		VultureControl::getInstance()->KJ_WAIT_Control(unit);
	}
	else {
		//VultureControl::getInstance()->goodLuck(unit);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Vulture Control

VultureControl* VultureControl::s_VultureControl;
VultureControl* VultureControl::create() {
	if (s_VultureControl)
	{
		return s_VultureControl;
	}
	s_VultureControl = new VultureControl();
	return s_VultureControl;
}
VultureControl* VultureControl::getInstance() {
	return s_VultureControl;
}

VultureControl::VultureControl() {
	int TT = 32;
	firstMinePosition[1][0] = Position(97 * TT, 86 * TT);
	firstMinePosition[1][1] = Position(95 * TT, 87 * TT);
	firstMinePosition[1][2] = Position(93 * TT, 88 * TT);
	firstMinePosition[0][0] = Position(88 * TT, 27 * TT);
	firstMinePosition[0][1] = Position(90 * TT, 28 * TT);
	firstMinePosition[0][2] = Position(92 * TT, 29 * TT);
	firstMinePosition[3][0] = Position(35 * TT, 100 * TT);
	firstMinePosition[3][1] = Position(37 * TT, 101 * TT);
	firstMinePosition[3][2] = Position(39 * TT, 102 * TT);
	firstMinePosition[2][0] = Position(30 * TT, 42 * TT);
	firstMinePosition[2][1] = Position(32 * TT, 41 * TT);
	firstMinePosition[2][2] = Position(34 * TT, 40 * TT);

	setStartMinePos();
	checkMultiMine.clear();
	frontMineVec.clear();
	scoutBase.clear();
	sb = NULL;
}

VultureControl::~VultureControl() {}

void VultureControl::onFrame() {

	updateIM();

	if (checkMultiMine.size() == 0 && CombatInfo::getInstance()->myStartingBase != NULL) {
		for (auto q = BWTA::getBaseLocations().begin(); q != BWTA::getBaseLocations().end(); q++) {
			if ((*q) == BWTA::getNearestBaseLocation(CombatInfo::getInstance()->myStartingBase->getPosition())) {
				checkMultiMine[(*q)->getPosition()] = true;
			}
			else {
				checkMultiMine[(*q)->getPosition()] = false;
			}
		}
	}


	if (CombatInfo::getInstance()->enemyFrontYard != NULL) {
		checkMultiMine[BWTA::getNearestBaseLocation(CombatInfo::getInstance()->enemyStartingBase->getPosition())->getPosition()] = true;
		checkMultiMine[BWTA::getNearestBaseLocation(CombatInfo::getInstance()->enemyFrontYard->getPosition())->getPosition()] = true;
	}

	if (twoFactoryScene) {
		if (CombatInfo::getInstance()->enemyStartingBase == NULL) {
			twoFactoryVultureAttackPos = CombatInfo::getInstance()->myUnitPos;
		}
		else {
			twoFactoryVultureAttackPos = CombatInfo::getInstance()->attackPos;
		}
	}

	if (twoFactoryScene && !twoFactoryJunjin) {
		if (BWTA::getNearestBaseLocation(CombatInfo::getInstance()->myUnitPos) == CombatInfo::getInstance()->enemyFrontYard) {
			twoFactoryJunjin = true;
		}
	}

	if (CombatInfo::getInstance()->enemyStartingBase != NULL && BWAPI::Broodwar->getFrameCount()%48==0) {
		updateHong();
	}

	if (sb != NULL && BWAPI::Broodwar->getFrameCount() % 30 == 0) {
		for (auto q = CombatInfo::getInstance()->enemyBase.begin(); q != CombatInfo::getInstance()->enemyBase.end(); q++) {
			if ((*q).first == sb) {
				sb = NULL;
			}
		}
	}

}

void VultureControl::goodLuck(VultureUnit* unit) {

	
	/*
	if (flag_Physical) {
		unit->setState(VultureUnit::STATE::CRAZY);
	}
	else if (unit->getSelf()->getSpiderMineCount() > 0 && (!mineBackEnough || !mineFrontEnough)) {	// 마인을 가지고 있음
		if (firstFrontMine < 3) {
			firstFrontMine++;
			unit->setState(VultureUnit::STATE::MINE_FRONT);
			printf("first mine %d\n", firstFrontMine);
		}
		else if (mineBackEnough == false && firstBackMine < 3) {
			firstBackMine++;
			unit->setState(VultureUnit::STATE::MINE_BACK);
			if(firstBackMine > 2)
				mineBackEnough = true;
			printf("first mine %d\n", firstBackMine);
		}
		else if (mineMultiEnough == false) {
			unit->setState(VultureUnit::STATE::CRAZY);
			//unit->setState(VultureUnit::STATE::MINE_MULTI);
		}
	}
	else {
		// TWO FACTORY

		// FRONT YARD SYMSITING

		// 
		unit->setState(VultureUnit::STATE::CRAZY);
	}*/
}


void VultureControl::MB_Control(VultureUnit* unit) {						// 마인 상대방 진출로에
	if (unit->isMining) {
		if (unit->spiderNumber != unit->getSelf()->getSpiderMineCount()) {
			unit->spiderNumber = unit->getSelf()->getSpiderMineCount();
			unit->setState(VultureUnit::STATE::IDLE);
			unit->isMining = false;
		}
		else {
			if (BWAPI::Broodwar->getFrameCount() % 80 == 0) {
				unit->getSelf()->useTech(TechTypes::Spider_Mines, unit->minePos);
			}
		}
	}
	else {
		unit->minePos = getBackMinePos(firstBackMine++);
		unit->getSelf()->useTech(TechTypes::Spider_Mines, unit->minePos);
		if (unit->getSelf()->isMoving()) {
			unit->isMining = true;
		}

	}
}
void VultureControl::MF_Control(VultureUnit* unit) {						// 마인 우리 병력 앞쪽에
	if (unit->attackTarget != NULL) {
		if (unit->attackTarget->getType() == UnitTypes::Protoss_Probe) {
			unit->setState(VultureUnit::STATE::NM_SQUEEZE);
		}
		else {
			unit->setState(VultureUnit::STATE::IDLE);
		}
		UnitControl::getInstance()->frontMineVec.push_back(unit->minePos);
		unit->isMining = false;
		return;
	}
	float dist = getDistance(unit->getSelf()->getPosition(), unit->minePos);
	if (dist < 120) {
		if (unit->isMining) {
			if (unit->spiderNumber != unit->getSelf()->getSpiderMineCount()) {
				unit->spiderNumber = unit->getSelf()->getSpiderMineCount();
				unit->setState(VultureUnit::STATE::IDLE);
				unit->isMining = false;
			}
			else if (BWAPI::Broodwar->isWalkable((WalkPosition)unit->minePos) == false) {
				//unit->spiderNumber = unit->getSelf()->getSpiderMineCount();
				unit->setState(VultureUnit::STATE::IDLE);
				unit->isMining = false;
			}
			else {
				if (BWAPI::Broodwar->getFrameCount() % 80 == 0) {
					unit->getSelf()->useTech(TechTypes::Spider_Mines, unit->minePos);
				}
			}
		}
		else {
			unit->getSelf()->useTech(TechTypes::Spider_Mines, unit->minePos);
			if (unit->getSelf()->isMoving()) {
				unit->isMining = true;
			}

		}
	}
	else {
		if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
			Position p = CombatInfo::getInstance()->getFloydPosition(unit->getSelf()->getPosition(), unit->minePos);
			unit->getSelf()->move(p, false);
		}
	}
}
void VultureControl::SCOUT_Control(VultureUnit* unit) {
	if (unit->attackTarget != NULL) {
		if (unit->attackTarget->getType() == UnitTypes::Protoss_Probe) {
			unit->setState(VultureUnit::STATE::NM_SQUEEZE);
		}
		return;
	}
	if (sb == NULL) {
		if (scoutBase.size() <= 0) {
			setScoutBaseLocationVec();
		}
		else {
			sb = scoutBase.front();
			scoutBase.pop_front();
		}
	}

	if (sb != NULL) {
		if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
			Position p = CombatInfo::getInstance()->getFloydPosition(unit->getSelf()->getPosition(), sb->getPosition());
			unit->getSelf()->move(p, false);
		}
		float dist = getDistance(unit->getSelf()->getPosition(), sb->getPosition());
		if (dist < 320) {
			sb = NULL;
		}
	}
}
void VultureControl::WAIT_Control(VultureUnit* unit) {						// 대기 

}
void VultureControl::ATTACK_Control(VultureUnit* unit) {					// 공격

}
void VultureControl::DEFENSE_Control(VultureUnit* unit) {					// 방어 (상대가 들어올 때)
	if (unit->attackTarget != NULL) {
		if (unit->attackTarget->getType() == UnitTypes::Protoss_Probe) {
			unit->setState(VultureUnit::STATE::NM_SQUEEZE);
		}
		else {
			unit->setState(VultureUnit::STATE::NM_CRAZY);
		}
		return;
	}
}
void VultureControl::SQUEEZE_Control(VultureUnit* unit) {					// 견제
	if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
		Unit targetUnit = getTargetUnit(unit, false);
		if (targetUnit == NULL) {
			if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
				unit->getSelf()->attack(attackPosition, false);
			}
			else{
				unit->setState(VultureUnit::STATE::IDLE);
			}
		}
		else {
			if (targetUnit->getType() != UnitTypes::Protoss_Probe) {
				if (getNearUnitDistance(unit->getSelf()->getPosition()) < 80) {
					kitingMove(unit);
				}
				else {
					unit->getSelf()->attack(targetUnit, false);
				}
			}
			else {
				unit->setState(VultureUnit::STATE::NM_RETURNTOBASE);
			}
		}
	}
	else {
		Unit targetUnit = getTargetUnit(unit, false);
		if (targetUnit != NULL)
			unit->getSelf()->move(targetUnit->getPosition(), false);
		else
			kitingMove(unit);
	}
}
void VultureControl::RTB_Control(VultureUnit* unit) {						// 작업 완료 후 병력 무리로 이동
	if (unit->attackTarget != NULL) {
		if (unit->attackTarget->getType() == UnitTypes::Protoss_Probe) {
			unit->setState(VultureUnit::STATE::NM_SQUEEZE);
		}
	}

	Position r2b = CombatInfo::getInstance()->returnBase;
	if (BWAPI::Broodwar->getFrameCount() % 36 == 0) {
		Position p = CombatInfo::getInstance()->getFloydPosition(unit->getSelf()->getPosition(), r2b);
		unit->getSelf()->move(p, false);
	}
	float dist = getDistance(unit->getSelf()->getPosition(), r2b);
	if (dist < 240) {
		unit->setState(VultureUnit::STATE::NM_DEFENSE);
	}
}
void VultureControl::MM_Control(VultureUnit* unit) {						// 마인 멀티에
	
}
void VultureControl::CRAZY_Control(VultureUnit* unit, bool kiting) {						// 피지컬 모드
	//unit->attackTarget = getTargetUnit(unit, false);

	if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
		if (unit->attackTarget == NULL) {
			float minDist = 99999;
			int minID = 0;
			for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
				float dist = getDistance((*q).second.second, unit->getSelf()->getPosition());
				if (minDist > dist) {
					minDist = dist;
					minID = (*q).first;
				}
			}
			Unit iu = getUnitforID(minID);
			if (iu == NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
					unit->getSelf()->attack(attackPosition, false);
			}
			else {
				if (BWAPI::Broodwar->getFrameCount() % 40 == 0) {
					unit->getSelf()->attack(iu, false);
				}
			}
		}
		else {
			if (getNearUnitDistance(unit->getSelf()->getPosition()) < 80) {
				if(kiting)
					kitingMove(unit);
			}
			else {
				unit->getSelf()->attack(unit->attackTarget, false);
			}
		}
	}
	else {
		if(kiting)
			kitingMove(unit);
	}
}

void VultureControl::TF_JUNJIN_Control(VultureUnit* unit, bool kiting) {
	int count = 0;
	int vcount = 0;

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		if ((*q).second.first == UnitTypes::Protoss_Photon_Cannon) {
			float dist = getDistance((*q).second.second, unit->getSelf()->getPosition());
			if (dist < UnitTypes::Terran_Vulture.groundWeapon().maxRange() * 2) {
				count++;
			}
		}
	}
	for (auto q = UnitControl::getInstance()->vultures.begin(); q != UnitControl::getInstance()->vultures.end(); q++) {
		float dist = getDistance((*q)->getSelf()->getPosition(), unit->getSelf()->getPosition());
		if (dist < UnitTypes::Terran_Vulture.groundWeapon().maxRange() * 2) {
			vcount++;
		}
	}

	if (unit->attackTarget == NULL) {
		float minDist = 99999;
		int minID = 0;
		for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
			float dist = getDistance((*q).second.second, unit->getSelf()->getPosition());
			if (minDist > dist) {
				minDist = dist;
				minID = (*q).first;
			}
		}
		Unit iu = getUnitforID(minID);
		if (iu == NULL) {
			if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
				unit->getSelf()->attack(unit->rPos, false);
		}
		else {
			if (BWAPI::Broodwar->getFrameCount() % 40 == 0) {
				unit->getSelf()->attack(iu, false);
			}
		}
	}
	else {
		if (count > 0) {																		//캐넌 있음
			if (vcount > count * 6) {																//캐넌보다 벌처가 훨씬 많음
				if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
					unit->getSelf()->attack(unit->rPos, false);
			}
			else {																					//캐넌보다 벌처가 훨씬 적음
				if (UnitControl::getInstance()->tanks.size() > 0) {										//탱크가 있음
					Position p = getNearestTankPos(unit);
					unit->cannon = getNearestCannon(unit->getSelf()->getPosition());
					if (unit->attackTarget == NULL) {														//주변에 캐넌땜에 공격할애가 없어
						if (unit->cannon == NULL) {
							if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
								unit->getSelf()->attack(twoFactoryVultureAttackPos, false);
						}
						else {
							if (getDistance(p, unit->getSelf()->getPosition()) > 100) {								// 탱크랑 멀어
								unit->getSelf()->move(p, false);
							}
							else {
								if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
									unit->getSelf()->attack(p, false);
							}
						}
					}
					else {				
																	//주변에 캐넌땜에 공격할애가 있어
						if (unit->cannon == NULL) {
							CRAZY_Control(unit, kiting);
						}
						else {
							if (getDistance(p, unit->getSelf()->getPosition()) > 100) {								// 탱크랑 멀어
								unit->getSelf()->move(p, false);
							}
							else {
								if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
									unit->getSelf()->attack(p, false);
							}
						}
					}
				}
				else {																					//탱크가 없음
					if (BWAPI::Broodwar->getFrameCount() % 40 == 0)		
						unit->getSelf()->attack(unit->rPos, false);
				}
			}
		}
		else {																					//캐넌 없음
			CRAZY_Control(unit, kiting);
		}
	}
	


	/*if (twoFactoryJunjin){
		if (getDistance(unit->getSelf()->getPosition(), CombatInfo::getInstance()->myUnitPos) > 500) {
			unit->attackTarget = getTargetUnit(unit);
			if (unit->attackTarget != NULL) {
				if (!isSafeZone(unit, unit->attackTarget) || back_all) {
					if (BWAPI::Broodwar->getFrameCount() % 4 == 0)
						unit->getSelf()->move(getNearestTankPos(unit), false);
				}
				else {
					if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
						Unit targetUnit = getTargetUnit(unit);
						if (targetUnit == NULL) {
							if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
								unit->getSelf()->attack(CombatInfo::getInstance()->attackPos, false);
							}
						}
						else {
							if (getNearUnitDistance(unit) < 90) {
								kitingMove(unit);
							}
							else {
								unit->getSelf()->attack(targetUnit, false);
							}
						}
					}
					else {
						kitingMove(unit);
					}
				}
			}
			else {
				if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
					unit->getSelf()->attack(twoFactoryVultureAttackPos, false);
			}
		}
		else {
			unit->setState(VultureUnit::STATE::IDLE);
		}
	}
	else {
		unit->attackTarget = getTargetUnit(unit);
		if (unit->attackTarget != NULL) {
			if (!isSafeZone(unit, unit->attackTarget) || back_all) {
				if (BWAPI::Broodwar->getFrameCount() % 4 == 0)
					unit->getSelf()->move(getNearestTankPos(unit), false);
			}
			else {
				if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
					Unit targetUnit = getTargetUnit(unit);
					if (targetUnit == NULL) {
						if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
							unit->getSelf()->attack(CombatInfo::getInstance()->attackPos, false);
						}
					}
					else {
						if (getNearUnitDistance(unit) < 90) {
							kitingMove(unit);
						}
						else {
							unit->getSelf()->attack(targetUnit, false);
						}
					}
				}
				else {
					kitingMove(unit);
				}
			}
		}
		else {
			if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
				unit->getSelf()->attack(twoFactoryVultureAttackPos, false);
		}
	}*/
}

void VultureControl::KJ_WAIT_Control(VultureUnit* unit) {
	if (BWAPI::Broodwar->getFrameCount() % 24 == 0) {
		unit->getSelf()->attack(attackPosition, false);
	}
}
void VultureControl::KJ_ATTACK_Control(VultureUnit* unit) {
	CRAZY_Control(unit, true);
}
void VultureControl::KJ_MINE_Control(VultureUnit* unit) {
	if (unit->isMining) {
		if (unit->spiderNumber != unit->getSelf()->getSpiderMineCount()) {
			unit->spiderNumber = unit->getSelf()->getSpiderMineCount();
			unit->setState(VultureUnit::STATE::IDLE);
			unit->isMining = false;
		}
		else if (BWAPI::Broodwar->isWalkable((WalkPosition)unit->minePos) == false) {
			//unit->spiderNumber = unit->getSelf()->getSpiderMineCount();
			unit->setState(VultureUnit::STATE::IDLE);
			unit->isMining = false;
		}
		else {
			if (BWAPI::Broodwar->getFrameCount() % 80 == 0) {
				unit->getSelf()->useTech(TechTypes::Spider_Mines, unit->minePos);
			}
		}
	}
	else {
		unit->getSelf()->useTech(TechTypes::Spider_Mines, unit->minePos);
		if (unit->getSelf()->isMoving()) {
			unit->isMining = true;
		}

	}
}





void VultureControl::TF_CRAZY_Control(VultureUnit* unit) {
	unit->attackTarget = getTargetUnit(unit, false);
	if (unit->attackTarget == NULL) {
		if (BWAPI::Broodwar->getFrameCount() % 40 == 0)
			unit->getSelf()->attack(twoFactoryVultureAttackPos, false);
	}
	else {
		CRAZY_Control(unit, false);
	}
}
void VultureControl::TF_SQUEEZE_Control(VultureUnit* unit) {

}
void VultureControl::TF_ATTACK_Control(VultureUnit* unit) {

}
void VultureControl::TF_FM_Control(VultureUnit* unit) {
	if (unit->isMining) {
		if (unit->spiderNumber != unit->getSelf()->getSpiderMineCount()) {
			unit->spiderNumber = unit->getSelf()->getSpiderMineCount();
			unit->setState(VultureUnit::STATE::TF_JUNJIN);
			unit->isMining = false;
		}
		else {
			if (BWAPI::Broodwar->getFrameCount() % 80 == 0) {
				unit->getSelf()->useTech(TechTypes::Spider_Mines, unit->minePos);
			}
			if (unit->attackTarget = getTargetUnit(unit, false)) {
				unit->setState(VultureUnit::STATE::TF_JUNJIN);
				unit->isMining = false;
			}
		}
	}
	else {
		unit->getSelf()->useTech(TechTypes::Spider_Mines, unit->minePos);
		if (unit->getSelf()->isMoving()) {
			unit->isMining = true;
		}

	}
}



bool VultureControl::isSafeZone(VultureUnit* unit, Unit target) {
	if (target == NULL) {
		float dist = getDistance(CombatInfo::getInstance()->attackPos, unit->getSelf()->getPosition());
		if (dist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().outerSplashRadius() + 100) return false;
		else true;
	}
	if (UnitControl::getInstance()->tanks.size() < 0) return true;
	else {
		int nVNum = 0;
		int nCNum = 0;
		float minDist = 9999;
		for (auto v = UnitControl::getInstance()->vultures.begin(); v != UnitControl::getInstance()->vultures.end(); v++) {
			if (getDistance((*v)->getSelf()->getPosition(), unit->getSelf()->getPosition()) < UnitTypes::Terran_Vulture.groundWeapon().maxRange() + 30) nVNum++;
		}
		for (auto e = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); e != CombatInfo::getInstance()->enemyBuildingsInfo.end(); e++) {
			if ((*e).second.first == UnitTypes::Protoss_Photon_Cannon) {
				float dist = getDistance((*e).second.second, unit->getSelf()->getPosition());
				if (dist < UnitTypes::Protoss_Photon_Cannon.groundWeapon().maxRange() + 80) nCNum++;
				if (minDist > dist) minDist = dist;
			}
		}
		if (nVNum > nCNum * 7) return true;
		if (minDist < UnitTypes::Protoss_Photon_Cannon.groundWeapon().maxRange() + 120) return false;
	}
	bool seige = false;
	for (auto t = UnitControl::getInstance()->tanks.begin(); t != UnitControl::getInstance()->tanks.end(); t++) {
		if (!seige && (*t)->isSiege) {
			seige = true;
		}
	}

	if (seige == false) return true;
	
	Position ntp = getNearestTankPos(unit);
	float dist = getDistance(ntp, unit->getSelf()->getPosition());
	int r = 32*3;
	if(dist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() + r){
		return true;
	}
	else {
		return false;
	}
}


Position VultureControl::getNearestTankPos(VultureUnit* unit) {
	float minDist = 99999;
	Position retPos = Position(0, 0);
	for (auto t = UnitControl::getInstance()->tanks.begin(); t != UnitControl::getInstance()->tanks.end(); t++) {

		float dist = getDistance(unit->getSelf()->getPosition(), (*t)->getSelf()->getPosition());
		if (minDist > dist) {
			minDist = dist;
			retPos = (*t)->getSelf()->getPosition();
		}
		
	}
	return retPos;
}

Unit VultureControl::getNearestCannon(Position p) {
	Unit ret = NULL;
	float minDist = 9999;

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		float dist = getDistance(p, (*q).second.second);
		if (dist < minDist) {
			minDist = dist;
			ret = getUnitforID((*q).first);
		}
	}
	return ret;
}

Position VultureControl::getNearestCannonPos(Position p) {
	Position ret = Position(0, 0);
	float minDist = 9999;

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		float dist = getDistance(p, (*q).second.second);
		if (dist < minDist) {
			minDist = dist;
			ret = (*q).second.second;
		}
	}
	return ret;
}

Unit VultureControl::getTargetUnit(VultureUnit* unit, bool cannon) {
	if (!cannon) {
		std::list< pair<int, pair<UnitType, Position>>> nearEnemies;
		nearEnemies.clear();

		for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
			int range = (int)(((*q).second.first.groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange()) * 1.5);
			float dist = getDistance(unit->getSelf()->getPosition(), (*q).second.second);
			if (dist < range) {
				nearEnemies.push_back(*q);
			}
		}

		for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
			int range = (int)(((*q).second.first.groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange()) * 1.5);
			float dist = getDistance(unit->getSelf()->getPosition(), (*q).second.second);
			if (dist < range) {
				nearEnemies.push_back(*q);
			}
		}

		int retInt = -1;
		float maxValue = 0;
		float utC = 7.0, bloodC = 5.0, distC = 3.0, damageC = 1.0, cannonC = 10.0;
		for (auto q = nearEnemies.begin(); q != nearEnemies.end(); q++) {
			float value = getTargetUnitValue((*q).second.first,0,0)*utC;
			if (getUnitforID((*q).first) != NULL) {
				Unit target = getUnitforID((*q).first);
				if (target->isVisible() == false) continue;
				if (target == NULL || target->exists() == false) continue;
				value += (((float)(*q).second.first.maxShields() + (*q).second.first.maxHitPoints()) - (target->getHitPoints() + target->getShields())) / (float)((*q).second.first.maxShields() + (*q).second.first.maxHitPoints()) *bloodC;
				int range = (*q).second.first.groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange();
				value += (range - getDistance(unit->getSelf()->getPosition(), (*q).second.second)) / range * distC;

				// 예상 피해 value +=

				if (value > maxValue) {
					maxValue = value;
					retInt = (*q).first;
				}
			}
		}

		if (retInt == -1) return NULL;
		else {
			return getUnitforID(retInt);
		}
	}
	else {
		std::list< pair<int, pair<UnitType, Position>>> nearEnemies;
		nearEnemies.clear();

		for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
			int range = (int)(((*q).second.first.groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange()) * 1.5);
			float dist = getDistance(unit->getSelf()->getPosition(), (*q).second.second);
			if (dist < range) {
				nearEnemies.push_back(*q);
			}
		}

		for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
			int range = (int)(((*q).second.first.groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange()) * 1.5);
			float dist = getDistance(unit->getSelf()->getPosition(), (*q).second.second);
			if (dist < range) {
				nearEnemies.push_back(*q);
			}
		}

		int retInt = -1;
		float maxValue = 0;
		float utC = 7.0, bloodC = 5.0, distC = 3.0, damageC = 1.0, cannonC = 10.0;
		for (auto q = nearEnemies.begin(); q != nearEnemies.end(); q++) {
			float value = getTargetUnitValue((*q).second.first,0,0)*utC;
			if (getUnitforID((*q).first) != NULL) {
				Unit target = getUnitforID((*q).first);
				if (target->isVisible() == false) continue;
				if (target == NULL || target->exists() == false) continue;
				value += (((float)(*q).second.first.maxShields() + (*q).second.first.maxHitPoints()) - (target->getHitPoints() + target->getShields())) / (float)((*q).second.first.maxShields() + (*q).second.first.maxHitPoints()) *bloodC;
				int range = (*q).second.first.groundWeapon().maxRange() + unit->getSelf()->getType().groundWeapon().maxRange();
				value += (range - getDistance(unit->getSelf()->getPosition(), (*q).second.second)) / range * distC;

				float cu = getDistance(getNearestCannonPos((*q).second.second), (*q).second.second);
				if (cu < UnitTypes::Protoss_Photon_Cannon.groundWeapon().maxRange() * 1.5f) {
					value = -100.0f;
				}
				// 예상 피해 value +=

				if (value > maxValue) {
					maxValue = value;
					retInt = (*q).first;
				}
			}
		}

		if (retInt == -1) return NULL;
		else {
			return getUnitforID(retInt);
		}
	}
}

float VultureControl::getTargetUnitValue(UnitType ut, int v, int c) {
	if (ut.isFlyer())
		return -1;
	if (ut == UnitTypes::Protoss_Zealot)
		return 1.0f;
	else if (ut == UnitTypes::Protoss_Dragoon)
		return 0.2f;
	else if (ut == UnitTypes::Protoss_High_Templar)
		return 0.60f;
	else if (ut == UnitTypes::Protoss_Dark_Templar)
		return 0.60f;
	else if (ut == UnitTypes::Protoss_Probe)
		return 0.75f;
	else if (ut == UnitTypes::Protoss_Reaver)
		return 0.1f;
	else if (ut == UnitTypes::Protoss_Archon)
		return 0.5f;
	else if (ut == UnitTypes::Protoss_Photon_Cannon) {
		if (c > v * 6) {
			return 0.1f;
		}
		else {
			return 0.5f;
		}
	}
	else {
		return -0;
	}
}

Unit VultureControl::getUnitforID(int id) {
	for (auto q = BWAPI::Broodwar->enemy()->getUnits().begin(); q != BWAPI::Broodwar->enemy()->getUnits().end(); q++) {
		if ((*q)->exists() && (*q)->getID() == id) {
			return (*q);
		}
	}
	return NULL;
}

float VultureControl::getExpectDamage(Position p, int range) {
	float ret = 0;
	for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++ ) {
		float dist = getDistance(p, (*q).second.second);
		if (dist < range) {
			
		}
	}

	return ret;
}

void VultureControl::drawVultrueControlUnit(VultureUnit* unit) {
	//BWAPI::Broodwar->drawLineMap(unit->getSelf()->getPosition(), Position((int)unit->getSelf()->getPosition().x + (int)unit->getSelf()->getVelocityX(),
	//	(int)unit->getSelf()->getPosition().y + (int)unit->getSelf()->getVelocityY()), Colors::White);
	//if (unit->attackTarget != NULL) {
	//	//BWAPI::Broodwar->drawLineMap(unit->getSelf()->getPosition(), unit->attackTarget->getPosition(), Colors::Yellow);
	//	BWAPI::Broodwar->drawLineMap(Position((int)unit->attackTarget->getVelocityX()+ (int)unit->attackTarget->getPosition().x,
	//		(int)unit->attackTarget->getVelocityY() + (int)unit->attackTarget->getPosition().y), unit->attackTarget->getPosition(), Colors::Red);
	//}
	//if (unit->safe) {
	//	BWAPI::Broodwar->drawCircleMap(unit->getSelf()->getPosition(), 5, Colors::Green, true);
	//}
	//else {
	//	BWAPI::Broodwar->drawCircleMap(unit->getSelf()->getPosition(), 5, Colors::Red, true);
	//}

	if (unit->getSelf()->isStuck()) {
		BWAPI::Broodwar->drawCircleMap(unit->minePos, 10, Colors::Red, true);
	}
	if (unit->getState() == VultureUnit::STATE::NM_MINE_FRONT) {
		BWAPI::Broodwar->drawCircleMap(unit->minePos, 7, Colors::Orange, true);
		BWAPI::Broodwar->drawLineMap(unit->getSelf()->getPosition(), unit->minePos, Colors::Orange);
	}
	BWAPI::Broodwar->drawCircleMap(unit->getSelf()->getPosition(), 5, Colors::Orange, true);
	BWAPI::Broodwar->drawTextMap(unit->getSelf()->getPosition(), unit->getString());
}

void VultureControl::drawVultrueControl() {

	/*for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		Position p1, p2;
		if ((*q).second.first == UnitTypes::Protoss_Gateway) {
			p1 = (*q).second.second;
			p2 = (*q).second.second;
			p1.x += 40;
			p1.y += 80;
			p2.y += 80;
			p2.x -= 40;

			if (BWAPI::Broodwar->isWalkable((WalkPosition)p1))
				BWAPI::Broodwar->drawCircleMap(p1, 5, Colors::Orange, true);
			else
				BWAPI::Broodwar->drawCircleMap(p1, 5, Colors::Brown, true);
			BWAPI::Broodwar->drawCircleMap(p2, 5, Colors::Orange, true);
		}
	}

	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {

		}
	}*/

	//마인 올멀티 
	/*if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
		for (auto q = checkMultiMine.begin(); q != checkMultiMine.end(); q++) {
			if (allMulitMine) {
				BWAPI::Broodwar->drawCircleMap((*q).first, 20, Colors::Purple, true);
			}
			else {
				if ((*q).second) {
					BWAPI::Broodwar->drawCircleMap((*q).first, 20, Colors::Blue, true);
				}
				else {
					BWAPI::Broodwar->drawCircleMap((*q).first, 20, Colors::Red, true);
				}
			}
		}
	}*/


	/*for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			if (vultureIM[y][x] == 0) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Green, false);
			}
			else if (vultureIM[y][x] == -1) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Red, false);
			}
			else if (vultureIM[y][x] == -3) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 6, Colors::Red, true);
			}
			else if (vultureIM[y][x] == -2) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Blue, false);
			}
			else if (vultureIM[y][x] >= 10) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Red, false);
			}
			else if (vultureIM[y][x] >= 5) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Orange, false);
			}
			else if (vultureIM[y][x] >= 1) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Yellow, false);
			}
		}
	}
	*/
	//int x = 0, y = 0;
	//x = BWAPI::Broodwar->getMousePosition().x + BWAPI::Broodwar->getScreenPosition().x;
	//y = BWAPI::Broodwar->getMousePosition().y + BWAPI::Broodwar->getScreenPosition().y;
	//printf("%d %d\n", x, y);

	/*for (int i = 0; i < smIndex; i++) {
		float PI = 3.141592;
		int count = 0;
		for (int j = 0; j < smIndex; j++) {
			if (hongMineValue[i] > hongMineValue[j]) {
				count++;
			}
		}
		if(count > 62)
			BWAPI::Broodwar->drawCircleMap(hongMinePos[i], 12, Colors::Red, true);
		else if (count > 57)							   
			BWAPI::Broodwar->drawCircleMap(hongMinePos[i], 12, Colors::Orange, true);
		else if (count > 50)							   
			BWAPI::Broodwar->drawCircleMap(hongMinePos[i], 12, Colors::Yellow, true);
		else if (count > 40)							   
			BWAPI::Broodwar->drawCircleMap(hongMinePos[i], 12, Colors::Green, true);
		else if (count > 30)							   
			BWAPI::Broodwar->drawCircleMap(hongMinePos[i], 12, Colors::Cyan, true);
		else if (count > 20)							   
			BWAPI::Broodwar->drawCircleMap(hongMinePos[i], 12, Colors::Blue, true);
		else if (count > 10)							   
			BWAPI::Broodwar->drawCircleMap(hongMinePos[i], 12, Colors::Purple, true);
		else 											   
			BWAPI::Broodwar->drawCircleMap(hongMinePos[i], 12, Colors::Black, true);
		for (int j = 0; j < 6; j++) {
			int CX = hongMinePos[i].x;
			int CY = hongMinePos[i].y;
			int dx = (int)(50 * cos(2 * PI / 6*j));
			int dy = (int)(50 * sin(2 * PI / 6*j));
			Position minPos = Position(CX + dx, CY + dy);
			if (hongCheck[hongMinePos[i]]) {
				BWAPI::Broodwar->drawCircleMap(minPos, 8, Colors::Purple, false);
				BWAPI::Broodwar->drawCircleMap(minPos, 6, Colors::Purple, false);
			}
			else{
				BWAPI::Broodwar->drawCircleMap(minPos, 8, Colors::Yellow, false);
				BWAPI::Broodwar->drawCircleMap(minPos, 6, Colors::Yellow, false);
			}
		}
	}*/

	/*Position p = Position(32 * 64, 32 * 64);
	Position p2[10];
	p2[0] = Position(p.x - 4, p.y - 20);
	p2[1] = Position(p.x - 15, p.y - 6);
	p2[2] = Position(p.x - 8, p.y + 16);
	p2[3] = Position(p.x + 9, p.y + 16);
	p2[4] = Position(p.x + 13, p.y - 7);

	for (int x = 0; x < 5; x++) {
		BWAPI::Broodwar->drawLineMap(p2[x], p, Colors::White);
	}

	BWAPI::Broodwar->drawCircleMap(p, 20, Colors::White, false);*/

	if (sb != NULL) {
		BWAPI::Broodwar->drawCircleMap(sb->getPosition(), 16, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap(sb->getPosition(), 15, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap(sb->getPosition(), 13, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap(sb->getPosition(), 12, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap(sb->getPosition(), 10, Colors::Orange, true);
	}
	
}

void VultureControl::kitingMove(VultureUnit* unit) {
	if (unit->attackTarget == NULL) {
		if (CombatInfo::getInstance()->enemyStartingBase != NULL)
			unit->getSelf()->move(unit->rPos, false);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
		//printf("닥치고 어택뜨으아아아아아앙\n");
	}
	else {
		Position p = unit->getSelf()->getPosition();
		Position targetPos;
		int minValue = 9999;
		for (int x = -5; x <= 5; x++) {
			for (int y = -5; y <= 5; y++) {
				int _x = x + p.x / 32;
				int _y = y + p.y / 32;
				if (_y < 0 || _x < 0 || _x > 127 || _y > 127) continue;
				if (BWAPI::Broodwar->getGroundHeight((TilePosition)Position(_x * 32, _y * 32)) != BWAPI::Broodwar->getGroundHeight((TilePosition)unit->getSelf()->getPosition())) {
					if (vultureIM[_y][_x] < minValue && vultureIM[_y][_x] >= 0) {
						minValue = vultureIM[_y][_x];
					}
				}
				else if (vultureIM[_y][_x] < minValue && vultureIM[_y][_x] >= 0) {
					minValue = vultureIM[_y][_x];
				}
			}
		}
		float minDisttoTarget = 99999;
		int fx = 0;
		int fy = 0;
		for (int x = -5; x <= 5; x++) {
			for (int y = -5; y <= 5; y++) {
				int _x = x + p.x / 32;
				int _y = y + p.y / 32;
				if (_y < 0 || _x < 0 || _x > 127 || _y > 127) continue;
				if (BWAPI::Broodwar->getGroundHeight((TilePosition)Position(_x * 32, _y * 32)) != BWAPI::Broodwar->getGroundHeight((TilePosition)unit->getSelf()->getPosition())) {
					if (vultureIM[_y][_x] == minValue) {
						float dist = getDistance(Position(_x * 32, _y * 32), unit->attackTarget->getPosition());
						if (dist < minDisttoTarget) {
							minDisttoTarget = dist;
							targetPos = Position(_x * 32, _y * 32);
							fx = _x;
							fy = _y;
						}
					}
				}
				else if (vultureIM[_y][_x] == minValue) {
					float dist = getDistance(Position(_x * 32, _y * 32), unit->attackTarget->getPosition());
					if (dist < minDisttoTarget) {
						minDisttoTarget = dist;
						targetPos = Position(_x * 32, _y * 32);
						fx = _x;
						fy = _y;
					}
				}
			}
		}
		BWAPI::Broodwar->drawLineMap(unit->getSelf()->getPosition(), targetPos, Colors::Yellow);
		unit->getSelf()->move(targetPos, false);
		vultureIM[fy][fx] = -3;
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				int xxxx = fx + i;
				int yyyy = fy + j;
				if (xxxx < 0 || yyyy < 0 || xxxx> 127 || yyyy > 127) continue;
				vultureIM[yyyy][xxxx] += 20;
			}
		}

		//printf("아~~~ 엄청난 피지컬로 컨트롤 하는 스무스한 무브!!!\n");
	}

}



void VultureControl::updateIM() {

	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			if (BWAPI::Broodwar->isWalkable((WalkPosition)(Position(32 * x, 32 * y)))) {
				vultureIM[y][x] = 0;
				ScoutIM[y][x] = 0;
			}
			else {
				vultureIM[y][x] = -1;
				ScoutIM[y][x] = -1;
			}
		}
	}

	//printf("VultureControl enemyUnitsIfnoSIze = %d\n", CombatInfo::getInstance()->enemyUnitsInfo.size());
	for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
		Position p = (*q).second.second;
		int _x = p.x / 32;
		int _y = p.y / 32;
		int eRange = (*q).second.first.groundWeapon().maxRange() + 140;
		int sRange = (*q).second.first.groundWeapon().maxRange();
		if ((*q).second.first == UnitTypes::Protoss_Probe) 
			eRange = (*q).second.first.groundWeapon().maxRange()*2;
		//printf("eRange %d\tUnitTypes %s\n", eRange, (*q).second.first.c_str());
		for (int i = -eRange / 32; i <= eRange / 32; i++) {
			for (int j = -eRange / 32; j <= eRange / 32; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx > 127 || _yy > 127) {
					continue;
				}
				int dist = (int)getDistance(Position(_xx * 32, _yy * 32), p);
				if (dist < eRange) {
					vultureIM[_yy][_xx] += (int)(((float)eRange - dist) / (float)eRange * 100);
				}
				if (dist < sRange) {
					ScoutIM[_yy][_xx] += (int)(((float)sRange - dist) / (float)sRange * 100);
				}

				//printf("erange : %d\tdist : %d\t (eRange-dist)/eRange*100 : %d\n", eRange, dist, (eRange - dist) / eRange * 100);
			}
		}
	}

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		if ((*q).second.first == UnitTypes::Protoss_Photon_Cannon) {
			Position p = (*q).second.second;
			int _x = p.x / 32;
			int _y = p.y / 32;
			int eRange = (*q).second.first.groundWeapon().maxRange() + 96;
			int sRange = (*q).second.first.groundWeapon().maxRange();
			for (int i = -eRange / 32; i <= eRange / 32; i++) {
				for (int j = -eRange / 32; j <= eRange / 32; j++) {
					int _xx = _x + i;
					int _yy = _y + j;
					if (_xx < 0 || _yy < 0 || _xx > 127 || _yy > 127) {
						continue;
					}
					int dist = (int)getDistance(Position(_xx * 32, _yy * 32), p);
					if (dist < eRange) {
						vultureIM[_yy][_xx] += (eRange - dist) / eRange * 100;
					}
					if (dist < sRange) {
						ScoutIM[_yy][_xx] += (int)(((float)sRange - dist) / (float)sRange * 100);
					}
				}
			}
		}
	}

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		Position p = (*q).second.second;
		int _x = p.x / 32;
		int _y = p.y / 32;
		if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
		vultureIM[_y][_x] = -2;
		ScoutIM[_y][_x] = -2;
		for (int i = -((*q).second.first.width() / 2) / 32 - 1; i <= ((*q).second.first.width() / 2) / 32 + 1; i++) {
			for (int j = -((*q).second.first.height() / 2) / 32; j <= ((*q).second.first.height() / 2) / 32 + 1; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx >127 || _yy > 127) {
					continue;
				}
				vultureIM[_yy][_xx] = -2;
				ScoutIM[_yy][_xx] = -2;
			}
		}
	}

	if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
		for (auto q = BWAPI::Broodwar->getMinerals().begin(); q != BWAPI::Broodwar->getMinerals().end(); q++) {
			Position p = (*q)->getPosition();
			int _x = p.x / 32;
			int _y = p.y / 32;
			if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
			vultureIM[_y][_x] = -2;
			ScoutIM[_y][_x] = -2;
			for (int i = -(UnitTypes::Resource_Mineral_Field.width() / 2) / 32; i <= (UnitTypes::Resource_Mineral_Field.width() / 2) / 32; i++) {
				for (int j = -((*q)->getType().height() / 2) / 32; j <= ((*q)->getType().height() / 2) / 32 + 1; j++) {
					int _xx = _x + i;
					int _yy = _y + j;
					if (_xx < 0 || _yy < 0 || _xx >127 || _yy > 127) {
						continue;
					}
					vultureIM[_yy][_xx] = -2;
					ScoutIM[_yy][_xx] = -2;
				}
			}
		}

		for (auto q = BWAPI::Broodwar->getGeysers().begin(); q != BWAPI::Broodwar->getGeysers().end(); q++) {
			Position p = (*q)->getPosition();
			int _x = p.x / 32;
			int _y = p.y / 32;
			if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
			vultureIM[_y][_x] = -2;
			ScoutIM[_y][_x] = -2;
			for (int i = -((*q)->getType().width() / 2) / 32; i <= ((*q)->getType().width() / 2) / 32; i++) {
				for (int j = -((*q)->getType().height() / 2) / 32; j <= ((*q)->getType().height() / 2) / 32; j++) {
					int _xx = _x + i;
					int _yy = _y + j;
					if (_xx < 0 || _yy < 0 || _xx >127 || _yy > 127) {
						continue;
					}
					vultureIM[_yy][_xx] = -2;
					ScoutIM[_yy][_xx] = -2;
				}
			}
		}
	}

	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			bool check = false;
			bool checkbuilding = false;
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					int _x = x + i;
					int _y = y + j;
					if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
					if (vultureIM[_y][_x] == -1) {
						check = true;
					}
					if (vultureIM[_y][_x] == -2) {
						checkbuilding = true;
					}
				}
			}
			if (check && vultureIM[y][x] >= 0) {
				vultureIM[y][x] = vultureIM[y][x] + 20;
				ScoutIM[y][x] = ScoutIM[y][x] + 20;
			}
			if (checkbuilding && vultureIM[y][x] >= 0) {
				vultureIM[y][x] = vultureIM[y][x] + 20;
				ScoutIM[y][x] = ScoutIM[y][x] + 20;
			}
			if (x == 0 || y == 0 || x == 127 || y == 127) {
				vultureIM[y][x] = vultureIM[y][x] + 20;
				ScoutIM[y][x] = ScoutIM[y][x] + 20;
			}
		}
	}

	for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
		int _x = (*q)->getSelf()->getPosition().x / 32;
		int _y = (*q)->getSelf()->getPosition().y / 32;
		// 384
		for (int i = -12; i <= 12; i++) {
			for (int j = -12; j <= 12; j++) {
				int x = _x + i;
				int y = _y + j;
				if (x < 0 || y < 0 || x > 127 || y > 127) continue;
				float dist = getDistance(Position(_x, _y), Position(x, y));
				if (dist < 384)
					if (vultureIM[y][x] >= 0) {
						vultureIM[y][x] -= dist / 2;
						if (vultureIM[y][x] < 0) vultureIM[y][x] = 0;

					}
			}
		}
	}

	for (auto q = BWAPI::Broodwar->self()->getUnits().begin(); q != BWAPI::Broodwar->self()->getUnits().end(); q++) {
		if ((*q)->getType() == UnitTypes::Terran_Vulture_Spider_Mine) {
			int _x = (*q)->getPosition().x / 32;
			int _y = (*q)->getPosition().y / 32;
			// 384
			for (int i = -3; i <= 3; i++) {
				for (int j = -3; j <= 3; j++) {
					int x = _x + i;
					int y = _y + j;
					if (x < 0 || y < 0 || x > 127 || y > 127) continue;
					float dist = getDistance(Position(_x, _y), Position(x, y));
					if (dist < 50){
						if (vultureIM[y][x] >= 0) {
							vultureIM[y][x] += (50 - dist);
						}
					}
				}
			}
		}
	}

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		if ((*q).second.first == UnitTypes::Protoss_Gateway) {
			Position p = (*q).second.second;
			int _x = p.x / 32;
			int _y = p.y / 32;
			if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
			vultureIM[_y][_x] = -2;
			ScoutIM[_y][_x] = -2;
			int i1 = 0, i2 = 0;
			if ((*q).second.second.x > 64 * 32) {
				i1 = 2;
				i2 = 1;
			}
			else {
				i1 = 1;
				i2 = 2;
			}
			for (int i = -((*q).second.first.width() / 2) / 32 - i1; i <= ((*q).second.first.width() / 2) / 32 + i2; i++) {
				for (int j = 0 ; j <= ((*q).second.first.height() / 2) / 32 + 2; j++) {
					int _xx = _x + i;
					int _yy = _y + j;
					if (_xx < 0 || _yy < 0 || _xx >127 || _yy > 127) {
						continue;
					}
					if (vultureIM[_yy][_xx] < 0) {
						continue;
					}
					else {
						bool check = false;
						for (int s = -1; s <= 1; s++) {
							for (int t = -1; t <= 1; t++) {
								int _xxx = _xx + s;
								int _yyy = _yy + t;
								if (_xxx < 0 || _yyy < 0 || _xxx >127 || _yyy > 127) {
									continue;
								}
								if (vultureIM[_yyy][_xxx] == -3) {
									check = true;
								}
							}
						}
						if (!check)
							vultureIM[_yy][_xx] = -3;
					}
				}
			}
		}
	}
}

Position VultureControl::getMulitMinePos(int ff) {
	for (auto q = checkMultiMine.begin(); q != checkMultiMine.end(); q++) {
		if (!(*q).second) {
			return (*q).first;
		}
	}
	return Position(-1,-1);
}

Position VultureControl::getTFMinPos(VultureUnit* unit) {
	/*Position ret;
	if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
		if (frontMineVec.size() <= 0) {
			setFrontMineVector();
		}
		if (frontMineVec.size() > 0) {
			for (auto q = frontMineVec.begin(); q != frontMineVec.end(); q++) {
				ret = (*q);
				break;
			}
		}
	}
	else {
		mineFrontEnough = true;
		ret = Position(-1, -1);
	}*/
}

Position VultureControl::getFrontMinePos(int index) {
	Position ret;
	if (index < 3) {
		return firstMinePosition[mapInfo->getDir()][index];
	}
	else if (1){//CombatInfo::getInstance()->enemyGroup.size() <= 0) {
		if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
			if (frontMineVec.size() <= 0) {
				setFrontMineVector();
			}
			if (frontMineVec.size() > 0) {
				for (auto q = frontMineVec.begin(); q != frontMineVec.end(); q++) {
					ret = (*q);
					break;
				}
			}
		}
		else {
			mineFrontEnough = true;
			ret = Position(-1, -1);
		}
	}

	return ret;
}

Position VultureControl::getBackMinePos(int index) {
	Position ret;
	if (index < 3) {
		return firstMinePosition[CombatInfo::getInstance()->getEnemyDir()][index];
	}

	return ret;
}

void VultureControl::setFrontMineVector() {
}

float VultureControl::getNearUnitDistance(Position p) {
	float minRet = 9999;
	for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
		if ((*q).second.first != UnitTypes::Protoss_Probe) {
			float dist = getDistance((*q).second.second, p);
			if (dist < minRet) {
				minRet = dist;
			}
		}
	}
	return minRet;
}

void VultureControl::setStartMinePos() {
	hongMinePos[0] = Position(271, 852);		hongMinePos[1] = Position(472, 1113);		hongMinePos[2] = Position(751, 1220);		hongMinePos[3] = Position(1867, 313);
	hongMinePos[4] = Position(1403, 306);		hongMinePos[5] = Position(1229, 538);		hongMinePos[6] = Position(1148, 894);		hongMinePos[7] = Position(1252, 1489);
	hongMinePos[8] = Position(943, 1201);		hongMinePos[9] = Position(1249, 1674);		hongMinePos[10] = Position(1232, 1912);		hongMinePos[11] = Position(1297, 2250);
	hongMinePos[12] = Position(677, 2054);		hongMinePos[13] = Position(419, 2264);		hongMinePos[14] = Position(928, 2183);		hongMinePos[15] = Position(1462, 2577);
	hongMinePos[16] = Position(1167, 2555);		hongMinePos[17] = Position(309, 2561);		hongMinePos[18] = Position(564, 2832);		hongMinePos[19] = Position(748, 3049);
	hongMinePos[20] = Position(1566, 2743);		hongMinePos[21] = Position(1719, 3030);		hongMinePos[22] = Position(1432, 3030);		hongMinePos[23] = Position(2055, 3420);
	hongMinePos[24] = Position(1145, 3314);		hongMinePos[25] = Position(1167, 3442);		hongMinePos[26] = Position(785, 3879);		hongMinePos[27] = Position(1071, 3655);
	hongMinePos[28] = Position(2237, 3688);		hongMinePos[29] = Position(2644, 3770);		hongMinePos[30] = Position(2009, 2799);		hongMinePos[31] = Position(1927, 2451);
	hongMinePos[32] = Position(2178, 2452);		hongMinePos[33] = Position(2050, 2273);		hongMinePos[34] = Position(2539, 2695);		hongMinePos[35] = Position(2851, 3496);
	hongMinePos[36] = Position(2987, 3149);		hongMinePos[37] = Position(2825, 2593);		hongMinePos[38] = Position(3189, 2937);		hongMinePos[39] = Position(3445, 2920);
	hongMinePos[40] = Position(3587, 2984);		hongMinePos[41] = Position(3768, 3248);		hongMinePos[42] = Position(2950, 2427);		hongMinePos[43] = Position(2890, 2118);
	hongMinePos[44] = Position(3401, 2027);		hongMinePos[45] = Position(3666, 1810);		hongMinePos[46] = Position(2879, 1720);		hongMinePos[47] = Position(3729, 1533);
	hongMinePos[48] = Position(3506, 1268);		hongMinePos[49] = Position(3390, 1086);		hongMinePos[50] = Position(2750, 1423);		hongMinePos[51] = Position(2387, 1222);
	hongMinePos[52] = Position(2141, 1656);		hongMinePos[53] = Position(1905, 1663);		hongMinePos[54] = Position(2053, 1875);		hongMinePos[55] = Position(1495, 1536);
	hongMinePos[56] = Position(1783, 1254);		hongMinePos[57] = Position(2046, 590);		hongMinePos[58] = Position(2939, 687);		hongMinePos[59] = Position(2971, 824);
	hongMinePos[60] = Position(2690, 1060);		hongMinePos[61] = Position(3038, 463);		hongMinePos[62] = Position(3299, 268);		hongMinePos[63] = Position(1889, 911);
	hongMinePos[64] = Position(2228, 926);

	hongCheck.clear();
	for (int x = 0; x < smIndex; x++) {
		hongMineValue[x] = 0;
		hongCheck[hongMinePos[x]] = false;
	}

	/*startMinePos[65] = Position(1495, 1536);
	startMinePos[66] = Position(1783, 1254);
	startMinePos[67] = Position(2046, 590);
	startMinePos[68] = Position(2939, 687);
	startMinePos[69] = Position(2971, 824);*/
}

void VultureControl::updateHong() {
	for (int x = 0; x < smIndex; x++) {
		hongMineValue[x] = 0;
		hongCheck[hongMinePos[x]] = false;
	}

	/*for (int x = 0; x < smIndex; x++) {
		if (hongCheck[hongMinePos[x]]) continue;
		for (auto q = CombatInfo::getInstance()->enemyBase.begin(); q != CombatInfo::getInstance()->enemyBase.end(); q++) {
			if ((*q).first == BWTA::getNearestBaseLocation(hongMinePos[x])) {
				hongMineValue[x] = -1;
				break;
			}
		}
	}*/

	hongMine2Pos.clear();
	for (auto q = BWAPI::Broodwar->self()->getUnits().begin(); q != BWAPI::Broodwar->self()->getUnits().end(); q++) {
		if ((*q)->getType() == UnitTypes::Terran_Vulture_Spider_Mine) {
			for (int x = 0; x < smIndex; x++) {
				float dist = getDistance((*q)->getPosition(), hongMinePos[x]);
				if (dist < 50) {
					hongMine2Pos[(*q)] = hongMinePos[x];
					hongCheck[hongMinePos[x]] = true;
					break;
				}
			}
		}
	}

	for (int x = 0; x < smIndex; x++) {
		if (hongCheck[hongMinePos[x]] || hongMineValue[x] < 0) continue;
		for (auto q = CombatInfo::getInstance()->enemyGroundGroup.begin(); q != CombatInfo::getInstance()->enemyGroundGroup.end(); q++) {
			float dist = getDistance((*q).first, hongMinePos[x]);
			if (dist < (*q).second.first * 2) {
				hongMineValue[x] = -1;
				break;
			}
		}
	}
	for (int x = 0; x < smIndex; x++) {
		if (hongCheck[hongMinePos[x]] || hongMineValue[x] < 0) continue;
		for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			float dist = getDistance((*q)->getSelf()->getPosition(), hongMinePos[x]);
			if (dist < 360) {
				hongMineValue[x] = -1;
				break;
			}
		}
	}

	for (int x = 0; x < smIndex; x++) {
		if (hongCheck[hongMinePos[x]] || hongMineValue[x] < 0) continue;
		Position p = getNearestCannonPos(hongMinePos[x]);
		if (p.x == 0 && p.y == 0) break;
		float dist = getDistance(p, hongMinePos[x]);
		if (dist < UnitTypes::Protoss_Photon_Cannon.groundWeapon().maxRange()*1.5f) {
			hongMineValue[x] = -1;
		}
	}

	/*for (int x = 0; x < smIndex; x++) {
		if (hongCheck[hongMinePos[x]] || hongMineValue[x] < 0) continue;
		for (auto q = CombatInfo::getInstance()->myGroup.begin(); q != CombatInfo::getInstance()->myGroup.end(); q++) {
			float dist = getDistance((*q).first, hongMinePos[x]);
			if (dist < 1) hongMineValue[x] += 1000;
			else hongMineValue[x] += 1000 / dist;
		}
	}*/

	for (int x = 0; x < smIndex; x++) {
		if (hongCheck[hongMinePos[x]] || hongMineValue[x] < 0) continue;
		for (auto q = CombatInfo::getInstance()->enemyGroup.begin(); q != CombatInfo::getInstance()->enemyGroup.end(); q++) {
			float dist = getDistance((*q).first, hongMinePos[x]);
			if (dist < 1) hongMineValue[x] += 1000;
			else hongMineValue[x] += 1000 / dist;
		}
	}

	for (int x = 0; x < smIndex; x++) {
		if (hongCheck[hongMinePos[x]] || hongMineValue[x] < 0) continue;
		for (auto q = CombatInfo::getInstance()->myBase.begin(); q != CombatInfo::getInstance()->myBase.end(); q++) {
			float dist = getDistance((*q).first->getPosition(), hongMinePos[x]);
			if (dist < 1) hongMineValue[x] += 1000;
			else hongMineValue[x] += 1000 / dist;
		}
	}

	for (int x = 0; x < smIndex; x++) {
		if (hongCheck[hongMinePos[x]] || hongMineValue[x] < 0) continue;
		for (auto q = CombatInfo::getInstance()->enemyBase.begin(); q != CombatInfo::getInstance()->enemyBase.end(); q++) {
			float dist = getDistance((*q).first->getPosition(), hongMinePos[x]);
			if (dist < 1) hongMineValue[x] += 1000;
			else hongMineValue[x] += 1000 / dist;
		}
	}
}

Position VultureControl::getHongMinePos() {
	Position ret = Position(0, 0);

	float maxV = 0;
	for (int x = 0; x < smIndex; x++) {
		if (hongMineValue[x] > maxV) {
			maxV = hongMineValue[x];
			ret = hongMinePos[x];
		}
	}

	return ret;
}

void VultureControl::setScoutBaseLocationVec() {
	bool index = false;
	for (auto q = BWTA::getBaseLocations().begin(); q != BWTA::getBaseLocations().end(); q++) {
		bool check = false;
		for (auto b = CombatInfo::getInstance()->myBase.begin(); b != CombatInfo::getInstance()->myBase.end(); b++) {
			if ((*q) == (*b).first) {
				check = true;
				break;
			}
		}
		for (auto b = CombatInfo::getInstance()->enemyBase.begin(); b != CombatInfo::getInstance()->enemyBase.end(); b++) {
			if ((*q) == (*b).first) {
				check = true;
				break;
			}
		}

		if(check==false){
			if (index) {
				scoutBase.push_back((*q));
			}
			else {
				scoutBase.push_front((*q));
			}
			index = !index;
		}
	}
}