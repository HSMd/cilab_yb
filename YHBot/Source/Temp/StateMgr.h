#pragma once

#include <map>

using namespace std;

enum BUILD_FLAG{
	SIM_CITY_1,
	SIM_CITY_2,
	FACTORY_DOUBLE,
	
	EXPAND_FORECOUNT,
	FAST_FACTORY_DOUBLE,
	BACANIC,
};

enum COMBAT_FLAG{
	NORMAL,

	ENEMY_SEARCH,
	INVASION_PYLON,   // 파일런 러쉬.
	ZEALOT_RUSH,      // 질럿 러쉬
	DRAGOON_RUSH,     // 드라군 
	DARKTEMPLER_RUSH, // 다크

	GUARD_SIM_CITY_1, // 탱크를 위함.
	GUARD_SIM_CITY_2, // 탱크를 위함.
	GUARD_SIM_CITY_3, // 탱크를 위함. 

	ATTACK,
};

enum OPP_FLAG{
	FAST_FORECOUNT,
};

class StateMgr
{
	map<BUILD_FLAG, bool> buildFlag;
	map<COMBAT_FLAG, bool> combatFlag;
	map<OPP_FLAG, bool> oppFlag;

	BUILD_FLAG build;
	COMBAT_FLAG combat;

public:
	StateMgr();
	~StateMgr();

	void show();
	void run();
	void chkCombatState();
	bool isSafe();

	BUILD_FLAG getBuild() { return build; }
	COMBAT_FLAG getCombat() { return combat; }

	// On Off Flags
	void setBuildFlag(BUILD_FLAG flag, bool fire) { buildFlag[flag] = fire; };
	void setCombatFlag(COMBAT_FLAG flag, bool fire) { combatFlag[flag] = fire; };
	void setOppFlag(OPP_FLAG flag, bool fire) { oppFlag[flag] = fire; };

	bool getCombatFlag(COMBAT_FLAG flag) { return combatFlag[flag]; };
};

