#pragma once
#include "Territory.h"

class TerritoryMgr
{
	vector<Territory*> myTerritorys;

public:
	TerritoryMgr();
	~TerritoryMgr();

	void run();

	Territory* getNearTerritory(TilePosition dst);
	void completeSCVReturn(Unit unit);
	void addTerritory(Unit unit);
	void addTerritory(Territory* territory);
	void addSCV(Unit unit);
	void gasCompleted(Unit unit);

	vector<Territory*> getAllTerritorys() { return myTerritorys; };
};

