#pragma once
#include "MyUnit.h"
#include "StateMgr.h"
#include "TerritoryMgr.h"
#include <map>

using namespace std;

class SimCityStrategy{

private:	
	//pylon 위치, cannon 위치, probe 위치
	enum DEFENSE_Flag{
		IDLE,
		PROBE,
		PYLON,
		CANNON,
		ZEALOT
	};
	Unit pylon;
	Unit Cannon;
	Unit Probe;

	vector<Unit> VPylon;
	vector<Unit> VCannon;
	vector<Unit> VProbe;
	vector<Unit> VZealot;

	//scv, 마린 위치
	vector<Position> pylonPosition;
	vector<Position> marinePosition;

	//할당할 유닛들 저장
	vector<SCV*> guardSCV;
	vector<Marine*> guardmarine;

	//유닛들의 타겟 위치 저장
	map<SCV*, Position> holdSCV;
	map<Marine*, Position> holdMarine;
	
	int safeFrame;
	bool blockFlag;

	map<DEFENSE_Flag, bool> defenseFlag;
	DEFENSE_Flag defense;

	void(SimCityStrategy::*Func)();

public:
	SimCityStrategy();
	~SimCityStrategy();

	TerritoryMgr* tMgr;

	// INIT FUNC
	void init();
	Marine* getIdleMarine();

	// CONTROL FUNC
	void scvControl(SCV* scv);
	void MarineControl(Marine* marine);

	// FLAG FUNC
	bool isSafe();
	bool PylonDetect();
	bool ZealotDetect();
	
	void setDefenseFlag(DEFENSE_Flag flag, bool check){ defenseFlag[flag] = check; };
	bool getDefenseFlag(DEFENSE_Flag flag) { return defenseFlag[flag]; };
	DEFENSE_Flag getDefenseState(){ return defense; }

	bool isInvasion();
	bool isBlock();

	// RUN FUNC
	void run();

	void detectPylon1(); // 입구 방해.
	void detectPylon2(); // 입구 이외에 대충 지은거.
	void UpdateUnit();	//우리 본진에 침투한 적의 유닛들
	void UpdateMyUnit();
	void marineHold();
	void end();

};