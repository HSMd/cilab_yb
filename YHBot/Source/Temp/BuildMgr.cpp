#include "BuildMgr.h"
#include "../Single.h"

BuildMgr::BuildMgr(){
}

BuildMgr::~BuildMgr(){

}

void BuildMgr::run(BUILD_FLAG build, COMBAT_FLAG combat){

	if (me->getBuildings(UnitTypes::Terran_Command_Center).size() < 1){
		return;
	}

	switch (build){

	case BUILD_FLAG::SIM_CITY_1:
		sim_1.run(combat);

		break;

	case BUILD_FLAG::SIM_CITY_2:
		up.run(combat);

		break;

	case BUILD_FLAG::FACTORY_DOUBLE:		
		tFac.run(combat);
		break;

	case BUILD_FLAG::FAST_FACTORY_DOUBLE:
		fast.run(combat);
		break;

	case BUILD_FLAG::BACANIC:
		bacanic.run(combat);

		break;
	}
}

void BuildMgr::onUnitCrate(Unit unit, BUILD_FLAG build){

	switch (build){

	case BUILD_FLAG::SIM_CITY_1:

		break;

	case BUILD_FLAG::SIM_CITY_2:

		break;

	case BUILD_FLAG::EXPAND_FORECOUNT:

		break;

	case BUILD_FLAG::FACTORY_DOUBLE:

		break;
	}
}

void BuildMgr::onStructureCrate(Unit unit, BUILD_FLAG build){

	switch (build){

	case BUILD_FLAG::SIM_CITY_1:

		break;

	case BUILD_FLAG::SIM_CITY_2:

		break;

	case BUILD_FLAG::EXPAND_FORECOUNT:

		break;

	case BUILD_FLAG::FACTORY_DOUBLE:

		break;
	}
}

void BuildMgr::onUnitComplete(Unit unit, BUILD_FLAG build){

	switch (build){

	case BUILD_FLAG::SIM_CITY_1:

		break;

	case BUILD_FLAG::SIM_CITY_2:

		break;

	case BUILD_FLAG::EXPAND_FORECOUNT:

		break;

	case BUILD_FLAG::FACTORY_DOUBLE:

		break;
	}
}

void BuildMgr::onStructureComplete(Unit unit, BUILD_FLAG build){

	switch (build){

	case BUILD_FLAG::SIM_CITY_1:

		break;

	case BUILD_FLAG::SIM_CITY_2:

		break;

	case BUILD_FLAG::EXPAND_FORECOUNT:

		break;

	case BUILD_FLAG::FACTORY_DOUBLE:

		break;
	}
}
