#include "CombatInfo.h"
#include "UtilMgr.h"
#include "VultureControl.h"
#include "UnitControl.h"
#include "../Single.h"

int nx[8] = { 1, 1, 0, -1, -1, -1, 0, 1 };
int ny[8] = { 0, 1, 1, 1, 0, -1, -1, -1 };
int regionCheck_x[4] = { 10, 0,-10, 0 };
int regionCheck_y[4] = { 0,-10, 0, 10 };
BWAPI::Color dcolor[8] = { BWAPI::Colors::Purple,  BWAPI::Colors::Blue,  BWAPI::Colors::Cyan, BWAPI::Colors::Teal,  BWAPI::Colors::Green,  BWAPI::Colors::Yellow,  BWAPI::Colors::Orange,  BWAPI::Colors::Red };

CombatInfo* CombatInfo::s_CombatInfo;
CombatInfo::CombatInfo() {
	init();
}

CombatInfo::~CombatInfo() {

}

Position getBoxPosition(bool first, UnitType ut, Position p) {
	Position ret;
	if (first) {
		ret.x = p.x - ut.width() / 2;
		ret.y = p.y - ut.width() / 2;
	}
	else {
		ret.x = p.x + ut.width() / 2;
		ret.y = p.y + ut.width() / 2;
	}
	return ret;
}

CombatInfo* CombatInfo::create() {
	if (s_CombatInfo) {
		return s_CombatInfo;
	}

	s_CombatInfo = new CombatInfo();
	return s_CombatInfo;
}

CombatInfo* CombatInfo::getInstance() {
	return s_CombatInfo;
}

void CombatInfo::init() {
	enemyUnitsInfo.clear();
	enemyBuildingsInfo.clear();
	enemyGroup.clear();
	enemyBase.clear();

	myUnitsInfo.clear();
	myBuildingsInfo.clear();
	myGroup.clear();
	myBase.clear();

	nearRegionChokePoints.clear();
	nodeIndex.clear();

	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			enemyPos[y][x] = Position(x * 32, y * 32);
			enemyPosVal[y][x] = 0;
			MyIMPos[y][x] = Position(x * 32, y * 32);
			MyIM[y][x] = 0;
		}
	}

	//printf("BWTA get RegionSize() = %d\n", BWTA::getRegions().size());
	for (auto q = BWTA::getRegions().begin(); q != BWTA::getRegions().end(); q++) {
		//printf("Index: %d Push Node Region\n", graphTotalIndex);
		nodeIndex.insert(map<int, Node*>::value_type(graphTotalIndex, (new Node(*q, graphTotalIndex))));
		graphTotalIndex++;
		//printf("Node Information: index : %d Region %d\n", nodeIndex[graphTotalIndex - 1]->num, nodeIndex[graphTotalIndex - 1]->isRegion());
		vector<BWTA::Chokepoint*> choke;
		choke.clear();

		for (auto c = BWTA::getChokepoints().begin(); c != BWTA::getChokepoints().end(); c++) {
			Position pos[4];
			for (int x = 0; x < 4; x++) {
				pos[x] = Position((*c)->getCenter().x + regionCheck_x[x], (*c)->getCenter().y + regionCheck_y[x]);
				if ((*q) == BWTA::getRegion(pos[x])) {
					choke.push_back((*c));
					break;
				}
			}
		}
		nearRegionChokePoints.push_back(pair<BWTA::Region*, vector<BWTA::Chokepoint*>>((*q), choke));
	}

	//printf("BWTA get Chokepoints size() = %d\n", BWTA::getChokepoints().size());
	for (auto c = BWTA::getChokepoints().begin(); c != BWTA::getChokepoints().end(); c++) {
		//printf("Index: %d Push Node ChokePoint\n", graphTotalIndex);
		nodeIndex.insert(map<int, Node*>::value_type(graphTotalIndex, (new Node(*c, graphTotalIndex))));
		graphTotalIndex++;
		//printf("Node Information: index : %d Region %d\n", nodeIndex[graphTotalIndex - 1]->num, nodeIndex[graphTotalIndex - 1]->isRegion());
	}

	for (int x = 0; x < graphTotalIndex; x++) {
		for (int y = 0; y < graphTotalIndex; y++) {
			if (nodeIndex[x] == NULL || nodeIndex[y] == NULL) {
				//printf("Null Pointer excetion where CombatInfo setting Node\n");
				continue;
			}
			if (x == y) {
				NodeGraph[y][x] = -2;
				NodeGraph2[y][x] = -2;
				NodeGraphBackup[y][x] = -2;
			}
			else if (isGumNodes(nodeIndex[x], nodeIndex[y])) {
				int dist = BWTA::getGroundDistance((TilePosition)(nodeIndex[x]->getCenter()), (TilePosition)(nodeIndex[y]->getCenter()));
				NodeGraph[y][x] = (int)dist;
				NodeGraph2[y][x] = (int)dist;
				NodeGraphBackup[y][x] = (int)dist;
			}
			else {
				NodeGraph[y][x] = 999999;
				NodeGraph2[y][x] = 999999;
				NodeGraphBackup[y][x] = 999999;
			}
		}
	}
	for (auto q = BWAPI::Broodwar->self()->getUnits().begin(); q != BWAPI::Broodwar->self()->getUnits().end(); q++) {
		if ((*q)->getType() == UnitTypes::Terran_Command_Center) {
			myStartingBase = BWTA::getNearestBaseLocation((*q)->getPosition());
		}
	}

	for (auto q = BWTA::getStartLocations().begin(); q != BWTA::getStartLocations().end(); q++) {
		if ((*q) != myStartingBase) {
			checkStarting[(*q)] = false;
			checkSeekStarting[(*q)] = false;
		}
		else {
			checkStarting[(*q)] = false;
			checkSeekStarting[(*q)] = true;
		}
	}
	

	attackPos = myStartingBase->getPosition();
	attackBase = NULL;
	myUnitPos = myStartingBase->getPosition();
	defensePos = myStartingBase->getPosition();
	
	if(mapInfo->getDir() == 0)
		returnBase = Position(90 * 32, 28 * 32);
	else if (mapInfo->getDir() == 1)
		returnBase = Position(95 * 32, 87 * 32);
	else if (mapInfo->getDir() == 2)
		returnBase = Position(32 * 32, 41 * 32);
	else if (mapInfo->getDir() == 3)
		returnBase = Position(37 * 32, 101 * 32);
}

void CombatInfo::updateMyInfo() {
	myBuildingsInfo.clear();
	myUnitsInfo.clear();

	for (auto q = BWAPI::Broodwar->self()->getUnits().begin(); q != BWAPI::Broodwar->self()->getUnits().end(); q++) {
		if ((*q)->getType() == UnitTypes::Terran_Vulture_Spider_Mine) continue;
		pair<UnitType, Position> info;
		info.first = (*q)->getType();
		info.second = (*q)->getPosition();

		if ((*q)->getType().isBuilding()) {
			myBuildingsInfo.push_back(pair<int, pair<UnitType, Position>>((*q)->getID(), info));
		}
		else {
			if ((*q)->isCompleted() == false) continue;
			myUnitsInfo.push_back(pair<int, pair<UnitType, Position>>((*q)->getID(), info));
		}
	}
	myGroup = getGroupArray(false, false);
}

void CombatInfo::updateEnemyInfo() {
	BWAPI::Unitset enemyUnits = BWAPI::Broodwar->enemy()->getUnits();
	//printf("CombatInfo enemyUnitset size %d\n", enemyUnits.size());
	for (auto q = enemyUnits.begin(); q != enemyUnits.end(); q++) {
		if (!(*q)->exists()) continue;		//덕상이가씀
		//printf("enemyUnits for loop\n");
		pair<UnitType, Position> info;
		info.first = (*q)->getType();
		info.second = (*q)->getPosition();
		if ((*q)->getType().isBuilding()) {			// buildings
			bool already = false;
			for (auto p = enemyBuildingsInfo.begin(); p != enemyBuildingsInfo.end(); p++) {
				if ((*p).first == (*q)->getID()) {
					already = true;
					break;
				}
			}
			if ((*q)->isVisible() && !already) {
				enemyBuildingsInfo.push_back(pair<int, pair<UnitType, Position>>((*q)->getID(), info));
			}
		}
		else {										// units
			bool already = false;
			for (auto p = enemyUnitsInfo.begin(); p != enemyUnitsInfo.end(); p++) {
				if ((*p).first == (*q)->getID()) {
					already = true;
					if((*q)->isVisible())
						(*p).second.second = (*q)->getPosition();
					break;
				}
			}
			if(already == false){
				enemyUnitsInfo.push_back(pair<int, pair<UnitType, Position>>((*q)->getID(), info));
			}
		}
	}

	if (enemyStartingBase == NULL) {
		//정찰 확인
		//printf("3개다 확인했는데 계속 들어오나 한번 봅시다.\n");
		for (auto p = enemyBuildingsInfo.begin(); p != enemyBuildingsInfo.end(); p++) {
			enemyStartingBase = BWTA::getNearestBaseLocation((*p).second.second);

			// 건물이 발견되면 가장 가까운 스타팅멀티를 적 본진으로 설정
			float minDist = 99999;
			for (auto q = BWTA::getStartLocations().begin(); q != BWTA::getStartLocations().end(); q++) {
				float dist = getDistance((*q)->getPosition(), (*p).second.second);
				if (dist < minDist) {
					minDist = dist;
					enemyStartingBase = (*q);
					checkStarting[(*q)] = true;
				}
			}
		}
		// 발견되지 않았을 때
		if (enemyStartingBase == NULL) {
			int seekCount = 0;
			for (auto q = BWTA::getStartLocations().begin(); q != BWTA::getStartLocations().end(); q++) {
				if (checkSeekStarting[(*q)]) {
					seekCount++;
				}
			}
			// 3개를 확인해 봤는데 없으면
			if (seekCount == 3) {
				for (auto q = BWTA::getStartLocations().begin(); q != BWTA::getStartLocations().end(); q++) {
					if (!checkSeekStarting[(*q)]) {
						//여기를 적의 스타팅멀티로 
						enemyStartingBase = (*q);
					}
				}
			}
		}

		if (enemyStartingBase != NULL) {
			if (enemyStartingBase->getTilePosition() == TilePosition(117, 7)) {
				enemyDir = 0;
				enemyFrontYard = BWTA::getNearestBaseLocation(Position(2880, 464));
				UnitControl::getInstance()->TF_MinePosition = Position(3020, 473);
				int mv = 0;
				int n = 4;
				Position enemyPos = Position(3008, 473);
				int enemyRange = 40;
				//printf("벌쳐 %d기 출동!~~\n", mv);
				int count = 0;
				float PI = 3.141592f;
				for (count; count < n; count++) {
					int CX = enemyPos.x;
					int CY = enemyPos.y;
					int dx = (int)(40 * cos(2 * PI / 4 * count));
					int dy = (int)(40 * sin(2 * PI / 4 * count));
					Position minPos = Position(CX + dx, CY + dy);
					Position cannonPos = VultureControl::getInstance()->getNearestCannonPos(minPos);
					UnitControl::getInstance()->kjMinePos[count] = minPos;
				}
			}
			else if (enemyStartingBase->getTilePosition() == TilePosition(117, 117)) {
				enemyDir = 1;
				enemyFrontYard = BWTA::getNearestBaseLocation(Position(3584, 2864));
				UnitControl::getInstance()->TF_MinePosition = Position(3544, 3001);
				int mv = 0;
				int n = 4;
				Position enemyPos = Position(3544, 3001);
				int enemyRange = 40;
				//printf("벌쳐 %d기 출동!~~\n", mv);
				int count = 0;
				float PI = 3.141592f;
				for (count; count < n; count++) {
					int CX = enemyPos.x;
					int CY = enemyPos.y;
					int dx = (int)(40 * cos(2 * PI / 4 * count));
					int dy = (int)(40 * sin(2 * PI / 4 * count));
					Position minPos = Position(CX + dx, CY + dy);
					Position cannonPos = VultureControl::getInstance()->getNearestCannonPos(minPos);
					UnitControl::getInstance()->kjMinePos[count] = minPos;
				}

			}
			else if (enemyStartingBase->getTilePosition() == TilePosition(7, 6)) {
				enemyDir = 2;
				enemyFrontYard = BWTA::getNearestBaseLocation(Position(512, 1264));
				UnitControl::getInstance()->TF_MinePosition = Position(454, 1103);
				int mv = 0;
				int n = 4;
				Position enemyPos = Position(454, 1103);
				int enemyRange = 40;
				//printf("벌쳐 %d기 출동!~~\n", mv);
				int count = 0;
				float PI = 3.141592f;
				for (count; count < n; count++) {
					int CX = enemyPos.x;
					int CY = enemyPos.y;
					int dx = (int)(40 * cos(2 * PI / 4 * count));
					int dy = (int)(40 * sin(2 * PI / 4 * count));
					Position minPos = Position(CX + dx, CY + dy);
					Position cannonPos = VultureControl::getInstance()->getNearestCannonPos(minPos);
					UnitControl::getInstance()->kjMinePos[count] = minPos;
				}
			}
			else if (enemyStartingBase->getTilePosition() == TilePosition(7, 116)) {
				enemyDir = 3;
				enemyFrontYard = BWTA::getNearestBaseLocation(Position(1216, 3632));
				UnitControl::getInstance()->TF_MinePosition = Position(1038, 3645);
				int mv = 0;
				int n = 4;
				Position enemyPos = Position(1038, 3645);
				int enemyRange = 40;
				//printf("벌쳐 %d기 출동!~~\n", mv);
				int count = 0;
				float PI = 3.141592f;
				for (count; count < n; count++) {
					int CX = enemyPos.x;
					int CY = enemyPos.y;
					int dx = (int)(40 * cos(2 * PI / 4 * count));
					int dy = (int)(40 * sin(2 * PI / 4 * count));
					Position minPos = Position(CX + dx, CY + dy);
					Position cannonPos = VultureControl::getInstance()->getNearestCannonPos(minPos);
					UnitControl::getInstance()->kjMinePos[count] = minPos;
				}
			}
		}
	}
	
	enemyGroup.clear();
	enemyGroup = getGroupArray(true, false);
	enemyGroundGroup = getGroupArray(true, true);
	//printf("CombatInfo enemyGroup SIZE %d\n", enemyGroup.size());
	for (int x = 0; x < graphTotalIndex; x++) {
		for (int y = 0; y < graphTotalIndex; y++) {
			NodeGraph[y][x] = NodeGraphBackup[y][x];
		}
	}

	for (auto q = enemyGroup.begin(); q != enemyGroup.end(); q++) {
		for (int x = 0; x < graphTotalIndex; x++) {
			float dist = getDistance((*q).first, nodeIndex[x]->getCenter());
			if(dist < 1000 && dist > 1){
				for (int y = 0; y < graphTotalIndex; y++) {
					if (NodeGraph[y][x] > 0 || NodeGraph[y][x] < 10000) {
						NodeGraph[y][x] += (*q).second.second * 500000 / dist;
						NodeGraph[x][y] += (*q).second.second * 500000 / dist;
					}
				}
			}
			else if (dist >= 0) {
				for (int y = 0; y < graphTotalIndex; y++) {
					if (NodeGraph[y][x] > 0 || NodeGraph[y][x] < 10000) {
						NodeGraph[y][x] += (*q).second.second * 500000;
						NodeGraph[x][y] += (*q).second.second * 500000;
					}
				}
			}
		}
	}
	
	mines.clear();
	for (auto q = BWAPI::Broodwar->self()->getUnits().begin(); q != BWAPI::Broodwar->self()->getUnits().end(); q++) {
		if ((*q)->getType() == UnitTypes::Terran_Vulture_Spider_Mine) {
			mines.push_back((*q)->getPosition());
		}
	}

	updateEnemyBase();
	updateMyIM();
	myUnitPos = getMyGroupPos();
	updateMyBase();

	if (enemyStartingBase != NULL) {
		attackBase = getAttackBase();
		defensePos = getDefensePosition();
		if (attackBase == NULL) {
			attackPos = Position(64 * 32, 64 * 32);
		}
		else {
			attackPos = getAttackPosition();
		}
	}

	if ((getEnemyBuildingNumber(UnitTypes::Protoss_Cybernetics_Core) == 0 && getEnemyBuildingNumber(UnitTypes::Protoss_Gateway > 2)) || getEnemyBuildingNumber(UnitTypes::Protoss_Nexus) > 1){
		cmd->getStateMgr().setOppFlag(OPP_FLAG::FAST_FORECOUNT, true);
	}
}

void CombatInfo::drawCombatInfo(bool debug) {
	if (debug) {

		for (auto q = BWTA::getStartLocations().begin(); q != BWTA::getStartLocations().end(); q++) {
			if (checkSeekStarting[(*q)]) {
				BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 30, Colors::Grey, true);
			}
			else {
				BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 30, Colors::White, true);
			}
		}

		//VultureControl::getInstance()->drawVultrueControl();

		for (auto q = enemyUnitsInfo.begin(); q != enemyUnitsInfo.end(); q++) {
			BWAPI::Broodwar->drawBoxMap(getBoxPosition(true, (*q).second.first, (*q).second.second), getBoxPosition(false, (*q).second.first, (*q).second.second), Colors::Red, false);
		}
		for (auto q = enemyBuildingsInfo.begin(); q != enemyBuildingsInfo.end(); q++) {
			BWAPI::Broodwar->drawBoxMap(getBoxPosition(true, (*q).second.first, (*q).second.second), getBoxPosition(false, (*q).second.first, (*q).second.second), Colors::Orange, false);
		}
		for (auto q = myUnitsInfo.begin(); q != myUnitsInfo.end(); q++) {
			BWAPI::Broodwar->drawBoxMap(getBoxPosition(true, (*q).second.first, (*q).second.second), getBoxPosition(false, (*q).second.first, (*q).second.second), Colors::Green, false);
		}
		for (auto q = myBuildingsInfo.begin(); q != myBuildingsInfo.end(); q++) {
			BWAPI::Broodwar->drawBoxMap(getBoxPosition(true, (*q).second.first, (*q).second.second), getBoxPosition(false, (*q).second.first, (*q).second.second), Colors::Blue, false);
		}

		//printf("ENEMYGROUP SIZE %d\n", enemyGroup.size());
		for (auto q = enemyGroup.begin(); q != enemyGroup.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.second/100, BWAPI::Colors::Orange, true);
			BWAPI::Broodwar->drawCircleMap((*q).first, 8, BWAPI::Colors::Red, true);
			BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.first, BWAPI::Colors::Red, false);
		}

		for (auto q = enemyGroundGroup.begin(); q != enemyGroundGroup.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.second/100, BWAPI::Colors::Orange, true);
			BWAPI::Broodwar->drawCircleMap((*q).first, 8, BWAPI::Colors::Yellow, true);
			BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.first, BWAPI::Colors::Yellow, false);
		}

		for (auto q = myGroup.begin(); q != myGroup.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.second/100, BWAPI::Colors::Blue, true);
			BWAPI::Broodwar->drawCircleMap((*q).first, 8, BWAPI::Colors::Green, true);
			BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.first, BWAPI::Colors::Green, false);
		}


		/*
		for (auto q = nearRegionChokePoints.begin(); q != nearRegionChokePoints.end(); q++) {
			for (auto c1 = (*q).second.begin(); c1 != (*q).second.end(); c1++) {
				BWAPI::Broodwar->drawLineMap((*q).first->getCenter(), (*c1)->getCenter(), Colors::White);
				for (auto c2 = (*q).second.begin(); c2 != (*q).second.end(); c2++) {
					if ((*c1) == (*c2)) continue;
					BWAPI::Broodwar->drawLineMap((*c1)->getCenter(), (*c2)->getCenter(), BWAPI::Colors::Grey);
				}
			}
		}

		for (auto q = BWTA::getRegions().begin(); q != BWTA::getRegions().end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getCenter(), 10, Colors::White, true);
		}

		for (auto q = BWTA::getChokepoints().begin(); q != BWTA::getChokepoints().end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getCenter(), 10, Colors::Grey, true);
		}
		*/

		/*for (auto q = nearRegionChokePoints.begin(); q != nearRegionChokePoints.end(); q++) {
			for (auto p = (*q).second.begin(); p != (*q).second.end(); p++) {
				BWAPI::Broodwar->drawLineMap((*p)->getCenter(), (*q).first->getCenter(), BWAPI::Colors::White);
				BWAPI::Broodwar->drawCircleMap((*p)->getCenter(), 10, BWAPI::Colors::Grey, true);
			}
			BWAPI::Broodwar->drawCircleMap((*q).first->getCenter(), 15, BWAPI::Colors::White, true);
		}*/

		/*for (int x = 0; x < graphTotalIndex; x++) {
			for (int y = 0; y < graphTotalIndex; y++) {
				if (NodeGraph[y][x] < 99999 && NodeGraph[y][x] > 0) {
					BWAPI::Broodwar->drawLineMap(nodeIndex[y]->getCenter(), nodeIndex[x]->getCenter(), BWAPI::Colors::Cyan);
					BWAPI::Broodwar->drawCircleMap(nodeIndex[y]->getCenter(), 8, BWAPI::Colors::Cyan, true);
					BWAPI::Broodwar->drawCircleMap(nodeIndex[x]->getCenter(), 8, BWAPI::Colors::Cyan, true);
				}
			}
		}*/

		/*Position cPos = nodeIndex[6]->getCenter();
		Position tPos = nodeIndex[21]->getCenter();
		BWAPI::Broodwar->drawCircleMap(cPos, 60, BWAPI::Colors::Purple, true);
		BWAPI::Broodwar->drawCircleMap(tPos, 60, BWAPI::Colors::Red, true);
		int colorIndex = 0;
		int count = 0;
		int n = Floyd(cPos, tPos);
		BWAPI::Broodwar->drawLineMap(cPos, nodeIndex[n]->getCenter(), Colors::Purple);
		//printf("6 -> %d\n", n);
		while (true) {
			Color col;
			if (colorIndex == 0) col = Colors::Blue;
			else if (colorIndex == 1) col = Colors::Teal;
			else if (colorIndex == 2) col = Colors::Green;
			else if (colorIndex == 3) col = Colors::Yellow;
			else if (colorIndex == 4) col = Colors::Orange;
			else if (colorIndex == 5) col = Colors::Red;
			else if (colorIndex == 6) col = Colors::Red;
			cPos = nodeIndex[n]->getCenter();
			BWAPI::Broodwar->drawCircleMap(cPos, 30, col, true);
			n = Floyd(nodeIndex[n]->getCenter(), tPos);
			BWAPI::Broodwar->drawLineMap(cPos, nodeIndex[n]->getCenter(), col);
			if (colorIndex++ > 6) colorIndex = 6;
			if (cPos == tPos) break;
			if (count++ > 6) {
				break;
			}
		}
		*/

		/*
		for (auto q = enemyGroup.begin(); q != enemyGroup.end(); q++) {
			for (auto p = nearRegionChokePoints.begin(); p != nearRegionChokePoints.end(); p++) {
				if (BWTA::getRegion((*q).first) == (*p).first) {
					BWAPI::Broodwar->drawCircleMap((*p).first->getCenter(), 20, BWAPI::Colors::Red, true);
					break;
				}
			}
		}
		*/

		/*
		for (int x = 0; x < graphTotalIndex; x++) {
			for (int y = 0; y < graphTotalIndex; y++) {
				if (RegionGraph[y][x] >= 9999) {
					BWAPI::Broodwar->drawCircleMap(RegionIndex[x]->getCenter(), 10, Colors::Red, true);
					BWAPI::Broodwar->drawCircleMap(RegionIndex[y]->getCenter(), 10, Colors::Red, true);
					BWAPI::Broodwar->drawLineMap(RegionIndex[y]->getCenter(), RegionIndex[x]->getCenter(), Colors::Red);
				}
				else if (RegionGraph[y][x] > 0) {
					BWAPI::Broodwar->drawCircleMap(RegionIndex[x]->getCenter(), 10, Colors::White, true);
					BWAPI::Broodwar->drawCircleMap(RegionIndex[y]->getCenter(), 10, Colors::White, true);
					BWAPI::Broodwar->drawLineMap(RegionIndex[y]->getCenter(), RegionIndex[x]->getCenter(), Colors::White);
				}
			}
		}
		*/

		/*for (int x = 0; x < 128; x++) {
			for (int y = 0; y < 128; y++) {
				if (MinePosGoodValue[y][x] >= 10)
					BWAPI::Broodwar->drawCircleMap(MinePos[y][x], 4, Colors::Teal, false);
				else if (checkMinePos[y][x] == 0)
					BWAPI::Broodwar->drawCircleMap(MinePos[y][x], 4, Colors::Blue, false);
				else if (checkMinePos[y][x] == 1)
					BWAPI::Broodwar->drawCircleMap(MinePos[y][x], 4, Colors::Green, false);
				else
					BWAPI::Broodwar->drawCircleMap(MinePos[y][x], 4, Colors::Red, false);
			}
		}*/
		for (auto q = enemyBase.begin(); q != enemyBase.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 16, Colors::Red, false);
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 15, Colors::Red, false);
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 13, Colors::Red, false);
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 12, Colors::Red, false);
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 10, Colors::Red, true);
		}
		for (auto q = myBase.begin(); q != myBase.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 16, Colors::Green, false);
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 15, Colors::Green, false);
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 13, Colors::Green, false);
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 12, Colors::Green, false);
			BWAPI::Broodwar->drawCircleMap((*q).first->getPosition(), 10, Colors::Green, true);
		}
		if (enemyFrontYard != NULL) {
			BWAPI::Broodwar->drawCircleMap(enemyFrontYard->getPosition(), 20, Colors::Orange, false);
			//printf("앞마당 그릴꼬야\n");
		}
		
		if (enemyStartingBase != NULL && attackBase != NULL) {
			
		}
	}
}

int CombatInfo::getEnemyUnitNumber(UnitType ut) {
	int ret = 0;
	for (auto q = enemyUnitsInfo.begin(); q != enemyUnitsInfo.end(); q++) {
		if ((*q).second.first == ut) {
			ret++;
		}
	}
	return ret;
}

int CombatInfo::getEnemyBuildingNumber(UnitType ut) {
	int ret = 0;
	for (auto q = enemyBuildingsInfo.begin(); q != enemyBuildingsInfo.end(); q++) {
		if ((*q).second.first == ut) {
			ret++;
		}
	}
	return ret;
}

void CombatInfo::onDestroy(Unit u) {
	if (u->getType().isBuilding()){
		//printf("CombatInfo->onDestroy : enemyBuildings.size = %d\n", enemyBuildingsInfo.size());
		auto iter = enemyBuildingsInfo.begin();
		auto iter2 = enemyBuildingsInfo.begin();
		for (iter = enemyBuildingsInfo.begin(); iter != enemyBuildingsInfo.end(); iter = iter2) {
			iter2++;
			if ((*iter).first == u->getID()) {
				enemyBuildingsInfo.erase(iter);
			}
		}
	}
	else {
		//printf("CombatInfo->onDestroy : enemyUnits.size = %d\n", enemyUnitsInfo.size());
		auto iter = enemyUnitsInfo.begin();
		auto iter2 = enemyUnitsInfo.begin();
		for (iter = enemyUnitsInfo.begin(); iter != enemyUnitsInfo.end(); iter = iter2) {
			iter2++;
			if ((*iter).first == u->getID()) {
				enemyUnitsInfo.erase(iter);
			}
		}
	}
}

vector <pair<Position, pair<int, int>>> CombatInfo::getGroupArray(bool enemy, bool ground) {
	vector<pair<Position, pair<int, int>>> ret;
	vector<Position> posv;
	vector<int> unitCount;
	vector<int> unitPosX;
	vector<int> unitPosY;
	vector<int> unitValue;
	vector<int> maxUnitDist;

	list<pair<int, pair<UnitType, Position>>> unitInfo;
	posv.clear();
	ret.clear();
	maxUnitDist.clear();
	unitCount.clear();
	unitInfo.clear();
	unitValue.clear();
	int count = 0;
	int range = 0;

	if (enemy && !ground) {
		unitInfo = enemyUnitsInfo;
	}
	else if (enemy && ground) {
		for (auto q = enemyUnitsInfo.begin(); q != enemyUnitsInfo.end(); q++) {
			if ((*q).second.first.isFlyer()) continue;
			unitInfo.push_back((*q));
		}
		for (auto q = enemyBuildingsInfo.begin(); q != enemyBuildingsInfo.end(); q++) {
			if ((*q).second.first == UnitTypes::Protoss_Photon_Cannon) {
				unitInfo.push_back((*q));
			}
		}
	}
	else {
		unitInfo = myUnitsInfo;
	}

	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			enemyPosVal[y][x] = 0;
		}
	}
	//printf("enemyUntisInfo Size = %d\n", enemyUnitsInfo.size());
	for (auto q = unitInfo.begin(); q != unitInfo.end(); q++) {
		//printf("enemyUnitInfo for loop\n");
		if ((*q).second.first == UnitTypes::Protoss_Probe) continue;
		int _x = (*q).second.second.x;
		int _y = (*q).second.second.y;
		_x /= 32;
		_y /= 32;
		if (_x < 0 || _y < 0 || _x>127 || _y > 127) continue;
		enemyPosVal[_y][_x]++;
	}
	int R = 16;
	if (!enemy) {
		R = 40;
	}
	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			for (int k = 0; k < 8; k++) {
				int _x = x + nx[k];
				int _y = y + ny[k];
				if (_x < 0 || _x > 127 || _y < 0 || _y > 127) continue;
				if (enemyPosVal[_y][_x] > 0) {
					vector<pair<int, int>> queue;
					queue.push_back(pair<int,int>(_x, _y));
					count = 0;
					int pos_x = 0;
					int pos_y = 0;
					//printf("while loop\n");
					while (queue.size() > 0) {
						int xx = queue.front().first;
						int yy = queue.front().second;
						queue.erase(queue.begin());
						for (int i = -R; i <= R; i++) {
							for (int j = -R; j <= R; j++) {
								int _xx = _x + i;
								int _yy = _y + j;
								if (_xx < 0 || _xx > 127 || _yy < 0 || _yy > 127) continue;
								if (enemyPosVal[_yy][_xx] > 0) {
									queue.push_back(pair<int, int>(_xx, _yy));
									enemyPosVal[_yy][_xx] = -1;
									count++;
									pos_x += enemyPos[_yy][_xx].x;
									pos_y += enemyPos[_yy][_xx].y;
								}
							}
						}
					}
					if (count != 0) {
						posv.push_back(Position(pos_x /= count, pos_y /= count));
						unitCount.push_back(0);
						unitPosX.push_back(0);
						unitPosY.push_back(0);
						maxUnitDist.push_back(0);
						unitValue.push_back(0);
					}
				}
			}
		}
	}

	if (posv.size() <= 0) {
		//printf("posv size = 0");
		return ret;
	}
	else {
		for (auto q = unitInfo.begin(); q != unitInfo.end(); q++) {
			if ((*q).second.first == UnitTypes::Protoss_Probe || (*q).second.first == UnitTypes::Terran_SCV) continue;
			float minDist = 99999;
			int index = 0;
			int nearIndex = 0;
			Position nearPos;
			for (auto p = posv.begin(); p != posv.end(); p++) {
				float dist = getDistance((*p), (*q).second.second);
				if (dist < minDist) {
					minDist = dist;
					nearIndex = index;
					nearPos = (*p);
				}
				index++;
			}
			unitCount[nearIndex]++;
			unitPosX[nearIndex] += (*q).second.second.x;
			unitPosY[nearIndex] += (*q).second.second.y;
			if ((*q).second.first.groundWeapon().damageAmount() != 0)
				unitValue[nearIndex] += (*q).second.first.groundWeapon().damageAmount();
			else if ((*q).second.first.airWeapon().damageAmount())
				unitValue[nearIndex] += (*q).second.first.airWeapon().damageAmount();
			else
				unitValue[nearIndex] += 10;
			float mind = getDistance((*q).second.second, nearPos);
			if (maxUnitDist[nearIndex] < mind) {
				maxUnitDist[nearIndex] = mind;
			}
		}
	}

	for (int x = 0; x < unitCount.size(); x++) {
		if (unitCount[x] != 0) {
			unitPosX[x] /= unitCount[x];
			unitPosY[x] /= unitCount[x];
		}
		ret.push_back(pair<Position, pair<int, int>>(Position(unitPosX[x], unitPosY[x]), pair<int, int>(maxUnitDist[x], unitValue[x])));
		//printf("ret push back Position[%d %d] range %d level %d\n", unitPosX[x], unitPosY[x], maxUnitDist[x], unitCount[x]);
	}

	return ret;
}

char* CombatInfo::getString(int x) {
	char * str;
	int d, s;
	str = _fcvt(x, 0, &d, &s);
	if (x == 0) {
		str[0] = '0';
	}

	return str;
}
vector<BWTA::Chokepoint *> CombatInfo::getNearestChokePoints(BWTA::Region* currentRegion) {
	for (auto q = nearRegionChokePoints.begin(); q != nearRegionChokePoints.end(); q++) {
		if (currentRegion == (*q).first) {
			return (*q).second;
		}
	}

	vector<BWTA::Chokepoint*> ret;
	ret.clear();
	return ret;
}

vector<BWTA::Chokepoint *> CombatInfo::getNearestChokePoints(Position currentPos) {
	for (auto q = nearRegionChokePoints.begin(); q != nearRegionChokePoints.end(); q++) {
		if (BWTA::getRegion(currentPos) == (*q).first) {
			return (*q).second;
		}
	}

	vector<BWTA::Chokepoint*> ret;
	ret.clear();
	return ret;
}

Position CombatInfo::getRTB_MovePosition(Position currentPos, Position targetPos) {
	BWTA::Region* tergetRegion = BWTA::getRegion(targetPos);
	BWTA::Region* currentRegion = BWTA::getRegion(currentPos);

	/*vector<BWTA::Region*> enemyRegion
	for (auto q = nearRegionChokePoints.begin(); q != nearRegionChokePoints.end(); q++) {
		float dist = getDistance()
	}*/

	
}

bool CombatInfo::isGumNodes(Node* n1, Node* n2) {
	if (n1->isRegion() && n2->isRegion()) {
		return false;
	}
	else if (!n1->isRegion() && !n2->isRegion()) {
		for (auto q = nearRegionChokePoints.begin(); q != nearRegionChokePoints.end(); q++) {
			bool check = false;
			bool check2 = false;
			for (auto p = (*q).second.begin(); p != (*q).second.end(); p++) {
				if ((*p) == n1->choke) check = true;
				if ((*p) == n2->choke) check2 = true;
			}
			if (check && check2) {
				return true;
			}
		}
		return false;
	}
	else {
		BWTA::Region* r; BWTA::Chokepoint* c;		
		if (n1->isRegion()) {
			r = n1->region;
			c = n2->choke;
		}
		else {
			r = n2->region;
			c = n1->choke;
		}
		for (auto q = nearRegionChokePoints.begin(); q != nearRegionChokePoints.end(); q++) {
			if ((*q).first == r) {
				for (auto p = (*q).second.begin(); p != (*q).second.end(); p++) {
					if ((*p) == c) {
						return true;
					}
				}
			}
		}
		return false;
	}
	return false;
}

int CombatInfo::Floyd(Position currentPos, Position targetPos) {
	int via[102][102] = { 0 };
	int d[102][102] = { 0 };
	int currentIndex = 0;
	int targetIndex = 0;
	
	float minC = 99999;
	float minT = 99999;
	for (int x = 0; x < graphTotalIndex; x++) {
		float distC = getDistance(currentPos, nodeIndex[x]->getCenter());
		float distT = getDistance(targetPos, nodeIndex[x]->getCenter());
		if (distC < minC) {
			minC = distC;
			currentIndex = x;
		}
		if (distT < minT) {
			minT = distT;
			targetIndex = x;
		}
	}

	for (int x = 0; x < graphTotalIndex; x++) {
		for (int y = 0; y < graphTotalIndex; y++) {
			d[y][x] = NodeGraph[y][x];
			via[y][x] = -1;
		}
	}
	for (int k = 0; k < graphTotalIndex; k++) {
		for (int x = 0; x < graphTotalIndex; x++) {
			for (int y = 0; y < graphTotalIndex; y++) {
				if (d[y][x] < 0) continue;
				if (d[y][k] < 0 || d[k][x] < 0) continue;
				if (d[y][x] > d[y][k] + d[k][x]) {
					d[y][x] = d[y][k] + d[k][x];
					via[y][x] = k;
				}
			}
		}
	}
	if (targetIndex == via[currentIndex][targetIndex]) {
		int minIndex = 999999;
		int min = -1;
		for (int x = 0; x < graphTotalIndex; x++) {
			if (NodeGraph[currentIndex][x] < minIndex && NodeGraph[currentIndex][x] > 0) {
				minIndex = NodeGraph[currentIndex][x];
				min = x;
			}
		}
		return min;
	}
	else {
		while (true) {
			if (isGumNodes(nodeIndex[currentIndex], nodeIndex[targetIndex])) {
				return targetIndex;
			}
			else {
				if (via[currentIndex][targetIndex] < 0) return targetIndex;
				targetIndex = via[currentIndex][targetIndex];
			}
		}
	}
}

int CombatInfo::Floyd2(Position currentPos, Position targetPos) {
	int via[102][102] = { 0 };
	int d[102][102] = { 0 };
	int currentIndex = 0;
	int targetIndex = 0;

	float minC = 99999;
	float minT = 99999;
	for (int x = 0; x < graphTotalIndex; x++) {
		float distC = getDistance(currentPos, nodeIndex[x]->getCenter());
		float distT = getDistance(targetPos, nodeIndex[x]->getCenter());
		if (distC < minC) {
			minC = distC;
			currentIndex = x;
		}
		if (distT < minT) {
			minT = distT;
			targetIndex = x;
		}
	}

	for (int x = 0; x < graphTotalIndex; x++) {
		for (int y = 0; y < graphTotalIndex; y++) {
			d[y][x] = NodeGraph2[y][x];
			via[y][x] = -1;
		}
	}
	for (int k = 0; k < graphTotalIndex; k++) {
		for (int x = 0; x < graphTotalIndex; x++) {
			for (int y = 0; y < graphTotalIndex; y++) {
				if (d[y][x] < 0) continue;
				if (d[y][k] < 0 || d[k][x] < 0) continue;
				if (d[y][x] > d[y][k] + d[k][x]) {
					d[y][x] = d[y][k] + d[k][x];
					via[y][x] = k;
				}
			}
		}
	}
	if (targetIndex == via[currentIndex][targetIndex]) {
		int minIndex = 999999;
		int min = -1;
		for (int x = 0; x < graphTotalIndex; x++) {
			if (NodeGraph2[currentIndex][x] < minIndex && NodeGraph2[currentIndex][x] > 0) {
				minIndex = NodeGraph2[currentIndex][x];
				min = x;
			}
		}
		return min;
	}
	else {
		while (true) {
			if (isGumNodes(nodeIndex[currentIndex], nodeIndex[targetIndex])) {
				return targetIndex;
			}
			else {
				if (via[currentIndex][targetIndex] < 0) return targetIndex;
				targetIndex = via[currentIndex][targetIndex];
			}
		}
	}
}

void CombatInfo::updateEnemyBase() {
	checkEnemyBase.clear();
	enemyBase.clear();
	for (auto q = BWTA::getBaseLocations().begin(); q != BWTA::getBaseLocations().end(); q++) {
		checkEnemyBase[(*q)] = false;
	}
	for (auto q = enemyBuildingsInfo.begin(); q != enemyBuildingsInfo.end(); q++) {
		BWTA::BaseLocation* base = BWTA::getNearestBaseLocation((*q).second.second);
		if (checkEnemyBase[base] == false) {
			checkEnemyBase[base] = true;
			enemyBase.push_back(pair<BWTA::BaseLocation*, int>(BWTA::getNearestBaseLocation((*q).second.second), 1));
		}
	}
}

Position CombatInfo::getFloydPosition(Position c, Position t) {
	Position ret;
	int n = Floyd(c, t);
	ret = nodeIndex[n]->getCenter();
	float dist = getDistance(t, ret);
	if (BWTA::getRegion(ret) == BWTA::getRegion(t) || dist < 320) {
		return t;
	}
	return ret;
}

Position CombatInfo::getFloydPosition2(Position c, Position t) {
	Position ret;
	int n = Floyd2(c, t);
	ret = nodeIndex[n]->getCenter();
	return ret;
}
Position CombatInfo::getDefensePosition() {
	for (auto q = enemyGroup.begin(); q != enemyGroup.end(); q++) {
		for (auto p = myBase.begin(); p != myBase.end(); p++) {
			float dist = getDistance((*q).first, (*p).first->getPosition());
			if (dist < 620) {
				return (*q).first;
			}
		}
	}
	return returnBase;
}


Position CombatInfo::getAttackPosition() {
	Position ret = myStartingBase->getPosition();

	
	if (BWTA::getNearestBaseLocation(myUnitPos) == attackBase && enemyStartingBase != NULL) {
		float minDist = 9999;
		for (auto q = enemyBuildingsInfo.begin(); q != enemyBuildingsInfo.end(); q++) {
			float dist = getDistance((*q).second.second, myUnitPos);
			if (dist < minDist) {
				minDist = dist;
				ret = (*q).second.second;
			}
		}
	}
	else if(enemyStartingBase != NULL) {
		if (attackPos.x > 0) {
			if (BWAPI::Broodwar->self()->supplyUsed() > 300) {
				ret = getFloydPosition2(myUnitPos, attackBase->getPosition());
			}
			else {
				ret = getFloydPosition2(myUnitPos, attackBase->getPosition());
			}
			//if (getDistance(ret, myUnitPos) < 30) {
			//	ret = getFloydPosition2(myUnitPos, attackBase->getPosition());
			//}
		}
	}
	return ret;
}

Position CombatInfo::getMyGroupPos() {
	Position ret;
	int maxV = 0;
	for (auto q = myGroup.begin(); q != myGroup.end(); q++) {
		if ((*q).second.second > maxV) {
			maxV = (*q).second.second;
			ret = (*q).first;
		}
	}
	return ret;
}

void CombatInfo::updateMyIM() {
	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			MyIM[y][x] = 0;
		}
	}
	for (auto q = BWAPI::Broodwar->self()->getUnits().begin(); q != BWAPI::Broodwar->self()->getUnits().end(); q++) {
		if ((*q)->getType() == UnitTypes::Terran_SCV || (*q)->getType().isBuilding() || (*q)->isCompleted() == false) continue;
		Position p = (*q)->getPosition();
		int _x = p.x / 32;
		int _y = p.y / 32;
		int eRange = (*q)->getType().groundWeapon().maxRange();
		//printf("eRange %d\tUnitTypes %s\n", eRange, (*q).second.first.c_str());
		int v = (*q)->getType().groundWeapon().damageAmount() * 10;
		if ((*q)->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || (*q)->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) v *= 3;
		for (int i = -eRange / 32; i <= eRange / 32; i++) {
			for (int j = -eRange / 32; j <= eRange / 32; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx > 127 || _yy > 127) {
					continue;
				}
				int dist = (int)getDistance(Position(_xx * 32, _yy * 32), p);
				if (dist < eRange && eRange > 0 && v > 0) MyIM[_yy][_xx] += (int)(((float)eRange - dist) / (float)eRange * v);
				//printf("erange : %d\tdist : %d\t (eRange-dist)/eRange*100 : %d\n", eRange, dist, (eRange - dist) / eRange * 100);
			}
		}
	}
}

BWTA::BaseLocation* CombatInfo::getAttackBase() {
	BWTA::BaseLocation* ret = NULL;
	
	if (enemyStartingBase == NULL) return ret;
	
	for (auto q = enemyBase.begin(); q != enemyBase.end(); q++) {
		if ((*q).first == enemyFrontYard) ret = (*q).first;
	}
	if (enemyBase.size() <= 0) {
		ret = enemyStartingBase;
	}
	
	if (ret != NULL) return ret;

	for (auto q = enemyBase.begin(); q != enemyBase.end(); q++) {
		if ((*q).first == enemyStartingBase) ret = (*q).first;
	}

	if (ret != NULL) return ret;

	for (auto q = enemyBase.begin(); q != enemyBase.end(); q++) {
		ret = (*q).first;
	}

	return ret;
}

void CombatInfo::updateMyBase() {

	myBase.clear();

	for (auto q = myBuildingsInfo.begin(); q != myBuildingsInfo.end(); q++) {
		bool check = false;
		BWTA::Region* region = BWTA::getRegion((*q).second.second);
		for (auto b = myBase.begin(); b != myBase.end(); b++) {
			if (BWTA::getRegion((*b).first->getPosition()) == region) {
				(*b).second++;
				check = true;
				break;
			}
		}
		if (!check) {
			for (auto p = BWTA::getBaseLocations().begin(); p != BWTA::getBaseLocations().end(); p++) {
				if (BWTA::getRegion((*p)->getPosition()) == region) {
					myBase.push_back(std::pair<BWTA::BaseLocation*, int>((*p), 1));
				}
			}
		}
	}
}