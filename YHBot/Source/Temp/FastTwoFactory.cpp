#include "FastTwoFactory.h"
#include "../Single.h"
#include "Scout.h"
#include "UtilMgr.h"

FastTwoFactory::FastTwoFactory(){

	frameCount = 0;
	out = nullptr;

	this->tMgr = &cmd->getTerritoryMgr();

	build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 50, UnitTypes::Terran_SCV, 9, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Barracks, *AUTO_POS, 30, UnitTypes::Terran_SCV, 11, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Refinery, *AUTO_POS, UnitTypes::Terran_Barracks, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, 60, UnitTypes::Terran_SCV, 14, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Machine_Shop, *AUTO_POS, UnitTypes::Terran_SCV, 14, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, 60, UnitTypes::Terran_SCV, 16, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, *AUTO_POS, UnitTypes::Terran_Factory, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Machine_Shop, *AUTO_POS, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true));

	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 1, UnitTypes::Terran_SCV, 12));
	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 1, UnitTypes::Terran_Factory, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 1, UnitTypes::Terran_Machine_Shop, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 1, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true));

	build.push_back(scheduleWork(UnitTypes::Terran_Siege_Tank_Tank_Mode, 3, UnitTypes::Terran_Factory, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Vulture, 2, UnitTypes::Terran_Siege_Tank_Tank_Mode, 3, true));
}

FastTwoFactory::~FastTwoFactory(){

}

void FastTwoFactory::run(COMBAT_FLAG combat){

	auto startTerritory = tMgr->getAllTerritorys()[0];

	if (!flags[ATTACK] && 3 <= me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()){
		flags[ATTACK] = true;
	}

	// SCV 스케쥴러에 추가합니다.
	if (cmd->getUnitScheduleCount(UnitTypes::Terran_SCV) < tMgr->getAllTerritorys().size()){
		for (auto territory : tMgr->getAllTerritorys()){
			int require = territory->getRequireSCVCount()-2;

			if (1 < require && cmd->getReadyTrain(territory->getCmdCenter()->getSelf())){
				cmd->addScvCount();
				cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_SCV, territory->getCmdCenter()->getSelf()), 1, 1);
			}
		}
	}

	// 탱크 생산
	if (flags[ATTACK] && me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).size() < 1 && cmd->getUnitScheduleCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) == 0){
		cmd->addUnitSchedule(UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, 1);
	}

	// 벌쳐 생산
	if (flags[ATTACK] && cmd->getUnitScheduleCount(UnitTypes::Terran_Vulture) == 0){
		cmd->addUnitSchedule(UnitTypes::Terran_Vulture, 1, 1);
	}	

	// 예약된 BUILD 들이 점화되면 수행된다.	
	for (auto& work : build){
		auto targetType = work.getTargetType();

		if (!work.getFire() && work.canFire()){

			if (targetType.isBuilding()){

				cmd->addStructureSchedule(work, 1, 0);
				work.onFire();
			}
			else{

				cmd->addUnitSchedule(targetType, work.getCount(), 0);
				work.onFire();
			}
		}
	}

	// 속도 업글
	if (!flags[UPGRADE_ION] && 3 <= me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()) {
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[0]->getSelf()->upgrade(UpgradeTypes::Ion_Thrusters)) {
			flags[UPGRADE_ION] = true;
		}
	}

	// 마인업글
	if (flags[UPGRADE_ION] && !flags[UPGRADE_MINE] && 3 <= me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()) {
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[1]->getSelf()->research(TechTypes::Spider_Mines)) {
			flags[UPGRADE_MINE] = true;
		}
	}
	
	// 서플 자동생산
	/*
	
	if (size < 3){
		if (frameCount + 800 < bw->getFrameCount() && cmd->getSupply() < 6 * 2){
			frameCount = bw->getFrameCount();
			cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
		}
	}
	else if(size < 5){
		if (frameCount + 800 < bw->getFrameCount() && cmd->getSupply() < 10 * 2){
			frameCount = bw->getFrameCount();
			cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
		}
	}
	else{
		if (frameCount + 800 < bw->getFrameCount() && cmd->getSupply() < 10 * 2){
			frameCount = bw->getFrameCount();
			cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 2, 1);
		}
	}
	*/
	int size = me->getBuildings(UnitTypes::Terran_Factory).size();
	if (2 <= size && frameCount + 800 < bw->getFrameCount()){
		if (size == 2){
			if (cmd->getSupply() < 6 * 2){
				frameCount = bw->getFrameCount();
				cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
			}			
		}
		else if (size <= 4){
			if (cmd->getSupply() < 10 * 2){
				frameCount = bw->getFrameCount();
				cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
			}
		}
		else{
			if (cmd->getSupply() < 10 * 2){
				frameCount = bw->getFrameCount();
				cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 2, 1);
			}
		}
	}

	/*
	if (2 <= me->getBuildings(UnitTypes::Terran_Factory).size() && frameCount + 800 < bw->getFrameCount()){
		if (cmd->getSupply() < 6 * 2) {
			frameCount = bw->getFrameCount();
			cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
		}
	}
	*/
	
	// 팩토리 늘리기
	if (2 <= me->getBuildings(UnitTypes::Terran_Factory).size() && 400 <= cmd->getMineral() && 0 == cmd->getUnitScheduleCount(UnitTypes::Terran_Factory)){
		cmd->addStructureSchedule(UnitTypes::Terran_Factory, *AUTO_POS, 1, 1);
	}


	// 정찰 보내는 것.
	if (!flags[SPY] && 16 <= me->getUnits(UnitTypes::Terran_SCV).size()){
		SCV* spyScv = startTerritory->getWorker(true);
		Scouting::getInstance()->setScoutUnit(spyScv);
		flags[SPY] = true;
	}
}