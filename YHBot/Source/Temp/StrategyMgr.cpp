#include "StrategyMgr.h"
#include "UtilMgr.h"
#include "../Single.h"


StrategyMgr::StrategyMgr() {
}

StrategyMgr::~StrategyMgr() {
}

void StrategyMgr::run(BUILD_FLAG build, COMBAT_FLAG combat) {

	switch (build){
	case BUILD_FLAG::SIM_CITY_1:

		simCity_1.run(combat);
		break;

	case BUILD_FLAG::SIM_CITY_2:

		simCity_2.run(combat);
		break;

	case BUILD_FLAG::FACTORY_DOUBLE:
		simCity_1.end();
		factoryDouble.run(combat);
		break;
	}
}

/*
void StrategyMgr::Probedetect()
{
	Unit enemy = nullptr;

	for (auto e : bw->enemy()->getUnits())
	{
		if (e->getType() == UnitTypes::Protoss_Probe)
		{
			if (getDistance(mapInfo->getMyStartBase()->getTilePosition(), e->getTilePosition()) <= 25)
			{
				enemy = e;
				break;
			}
		}
	}
	if (enemy != nullptr)
	{
		if (tscv == nullptr)
		{
			tscv = cmd->getTerritoryMgr().getAllTerritorys()[0]->getWorker(true);
		}
		else
		{
			tscv->getSelf()->attack(enemy);
		}
	}
	else
	{
		if (tscv != nullptr)
		{
			tscv->setState(SCV::STATE::RETURN);
			tscv->getSelf()->gather(tscv->getTarget());
		}
	}
	
}
*/


void StrategyMgr::draw(BUILD_FLAG build, COMBAT_FLAG combat) {
	switch (build) {
	case BUILD_FLAG::SIM_CITY_1:

		simCity_1.draw(combat);
		break;

	case BUILD_FLAG::SIM_CITY_2:
		simCity_2.draw(combat);
		break;

	case BUILD_FLAG::FACTORY_DOUBLE:

		factoryDouble.draw(combat);
		break;
	}
}