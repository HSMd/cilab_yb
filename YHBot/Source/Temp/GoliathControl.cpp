#include "GoliathControl.h"
#include "CombatInfo.h"
#include "UnitControl.h"

void GoliathControl::run(Goliath* unit) {

	if (unit->getState() == Goliath::STATE::IDLE) {
		//GoliathControl::getInstance()->goodLuck(unit);
		GoliathControl::getInstance()->CRAZY_Control(unit);
	}
	else if (unit->getState() == Goliath::STATE::WAIT) {
		GoliathControl::getInstance()->WAIT_Control(unit);
	}
	else if (unit->getState() == Goliath::STATE::ATTACK) {
		GoliathControl::getInstance()->ATTACK_Control(unit);
	}
	else if (unit->getState() == Goliath::STATE::RETURN) {
		GoliathControl::getInstance()->RETURN_Control(unit);
	}
	else if (unit->getState() == Goliath::STATE::MOVE) {
		GoliathControl::getInstance()->MOVE_Control(unit);
	}
	else if (unit->getState() == Goliath::STATE::CRAZY) {
		GoliathControl::getInstance()->CRAZY_Control(unit);
	}
	else if (unit->getState() == Goliath::STATE::PROTECT) {
		GoliathControl::getInstance()->PROTECT_Control(unit);
	}
	else {
		//GoliathControl::getInstance()->goodLuck(unit);
		GoliathControl::getInstance()->CRAZY_Control(unit);
	}

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Goliath Control

GoliathControl* GoliathControl::s_GoliathControl;
GoliathControl* GoliathControl::create() {
	if (s_GoliathControl)
	{
		return s_GoliathControl;

	}
	s_GoliathControl = new GoliathControl();
	return s_GoliathControl;
}
GoliathControl* GoliathControl::getInstance() {
	return s_GoliathControl;
}
GoliathControl::GoliathControl() {
	//Ground Units
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Archon, 5));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Dark_Archon, 5));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Dark_Templar, 5));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Dragoon, 5));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_High_Templar, 5));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Probe, 1));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Reaver, 5));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Scarab, 5));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Zealot, 5));

	//Air Units
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Arbiter, 10));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Carrier, 7));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Corsair, 7));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Interceptor, 6));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Observer, 2));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Scout, 6));
	pt_weight.insert(std::pair<UnitType, int>(UnitTypes::Protoss_Shuttle, 7));

	//GroundUnits
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Firebat, 5));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Ghost, 5));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Goliath, 5));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Medic, 6));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_SCV, 1));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Siege_Tank_Tank_Mode, 5));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Siege_Tank_Siege_Mode, 5));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Vulture, 5));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Vulture_Spider_Mine, 0));
	//Air Units
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Battlecruiser, 10));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Dropship, 5));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Nuclear_Missile, 7));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Science_Vessel, 5));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Valkyrie, 2));
	tr_weight.insert(std::pair<UnitType, int>(UnitTypes::Terran_Wraith, 5));

}

GoliathControl::~GoliathControl() {}

void GoliathControl::onFrame() {
	//updateIM();
}


void GoliathControl::goodLuck(Goliath* unit) {
	if (!unit->getSelf()->isCompleted()) return;
	unit->setState(Goliath::STATE::CRAZY);
}

void GoliathControl::WAIT_Control(Goliath* unit) {					// 대기 

}
void GoliathControl::ATTACK_Control(Goliath* unit) {					// 공격

}
void GoliathControl::RETURN_Control(Goliath* unit) {					// 작업 완료 후 병력 무리로 이동
	//Protecting Tank Mode
	if (unit->getProtectTarget() != nullptr && unit->getProtectTarget()->exists()) {

		//탱크로 돌아가는 위치(탱크와 조금 떨어진 위치임)
		Position posNearTank = Position(
			unit->getSelf()->getPosition().x + ((unit->getProtectTarget()->getPosition().x - unit->getSelf()->getPosition().x) * (0.5)),
			unit->getSelf()->getPosition().y + ((unit->getProtectTarget()->getPosition().y - unit->getSelf()->getPosition().y) * (0.5))
			);
		unit->getSelf()->move(posNearTank);
	}
	else {

	}
}
void GoliathControl::MOVE_Control(Goliath* unit) {					// 이동
	if (unit->getProtectTarget() != nullptr) {
		RETURN_Control(unit);
	}
}
void GoliathControl::CRAZY_Control(Goliath* unit) {					// 피지컬 모드

	//Retrives all defence tanks;
	//list<Tank*> tanks = UnitControl::getInstance()->tanks;
	//list<Unit> defence_tank_units;

	//for (auto t = tanks.begin(); t != tanks.end(); t++) {
	//	if ((*t)->getState() == Tank::STATE::DEFENCE) {
	//		defence_tank_units.push_back((*t)->getSelf());
	//	}
	//}

	//
	//auto it = std::find(defence_tank_units.begin(), defence_tank_units.end(), unit->getProtectTarget());
	//Broodwar << "size: " << defence_tank_units.size() << std::endl;

	//if (it != defence_tank_units.end())
	//{
	//	Broodwar << "true" << std::endl;
	//	unit->setProtectTarget(nullptr);
	//}

	unit->setAttackTarget(getTargetUnit(unit->getSelf()));
	int worstSocre = getWorstAreaScore(unit);

	//unit->setProtectTarget(getClosestTankToSave(unit->getSelf()));
	//if (unit->getProtectTarget() != nullptr) {
	//	Broodwar->drawLineMap(unit->getSelf()->getPosition(), unit->getProtectTarget()->getPosition(), Colors::Grey);
	//	PROTECT_Control(unit);
	//	return;
	//}
	//if (unit->getAttackTarget() != nullptr) 
	//	Broodwar->drawLineMap(unit->getSelf()->getPosition(), unit->getAttackTarget()->getPosition(), Colors::Purple);
	//만약 주변에 적이 있는지 확인

	if (worstSocre * (-1) > 0) {
		//쿨 타임
		Unit unitToHelpFriend = getTargetToHelpFriend(unit->getSelf());

		if (unitToHelpFriend != nullptr) {
			unit->setAttackTarget(unitToHelpFriend);
		}

		if (unit->getAttackTarget() != nullptr) {
			//Get Self Cooltime to Attack Enemy(Ground Units, Air Units)
			int cooldown_for_enemy;
			cooldown_for_enemy = unit->getAttackTarget()->isFlying() ? unit->getSelf()->getAirWeaponCooldown() : unit->getSelf()->getGroundWeaponCooldown();

			if (cooldown_for_enemy == 0 && (!unit->getSelf()->isAttackFrame())) {
				//Ready To Attack

				//만약 체력이 너무 작을 경우 피하지 안하고 끝까지 싸우다 전사함 (도망칠 가치 없음)
				if (unit->getSelf()->getHitPoints() > 10) {
					if (!unit->getSelf()->isAttackFrame()) {
						Position bp = getBestPosition(unit->getSelf());
						if (bp != Position(-1, -1)) {
							unit->getSelf()->move(bp);
						}
					}

				}
				else {
					if (!unit->getSelf()->isAttackFrame()) {
						//unit->getSelf()->attack(unit->getAttackTarget());
						unit->getSelf()->attack(unit->getAttackTarget());
					}
				}
			}
			else {
				//Not Ready To Attack
				int cooldown_for_enemy;
				cooldown_for_enemy = unit->getAttackTarget()->isFlying() ? unit->getSelf()->getAirWeaponCooldown() : unit->getSelf()->getGroundWeaponCooldown();


				if (!unit->getSelf()->isAttackFrame() && cooldown_for_enemy != 0) {
					Position bp = getBestPosition(unit->getSelf());
					if (bp != Position(-1, -1)) {
						unit->getSelf()->move(bp);
					}
				}
			}

		}

	}
	else {
		if (unit->getAttackTarget() != nullptr) {

			int cooldown_for_enemy;
			cooldown_for_enemy = unit->getAttackTarget()->isFlying() ? unit->getSelf()->getAirWeaponCooldown() : unit->getSelf()->getGroundWeaponCooldown();


			if (cooldown_for_enemy != 0 && (!unit->getSelf()->isAttackFrame())) {
				unit->getSelf()->attack(unit->getAttackTarget());
			}

		}
		else {
			if (!unit->getSelf()->isAttackFrame() || !unit->getSelf()->isStartingAttack()) {
				unit->getSelf()->attack(attackPosition);
			}


		}
	}

}

void GoliathControl::PROTECT_Control(Goliath* unit) {

	int worstScore = getWorstAreaScore(unit);

	if (unit->getProtectTarget() != nullptr) {
		Unit targetToHelpTank = getTargetUnit(unit->getProtectTarget());
		if (unit->getSelf()->getDistance(unit->getProtectTarget()) <= getUnitGroundAttackRadius(unit->getProtectTarget())) {
			if (worstScore * (-1) > 0) {
				Position bp = getBestPosition(unit->getSelf());
				if (bp != Position(-1, -1)) {
					unit->getSelf()->move(bp);
				}
			}
			else {
				unit->getSelf()->attack(targetToHelpTank);
			}
		}
		else {
			RETURN_Control(unit);
		}
	}
	else {
		//공격해야 할 대상 없을 때 -> 그냥 내 주변에 공격할 대상이 있는지 검색
		if (unit->getSelf()->getDistance(unit->getProtectTarget()) >= unit->getProtectTarget()->getType().sightRange() + 50 - 20) {
			//Get Return Position
			RETURN_Control(unit);
		}
		else {

			Unit targetToHelpFriend = getTargetToHelpFriend(unit->getSelf());
			if (targetToHelpFriend != nullptr) {
				unit->setAttackTarget(targetToHelpFriend);
			}

			if (unit->getAttackTarget() != nullptr) {
				//Get Self Cooltime to Attack Enemy(Ground Units, Air Units)
				int cooldown_for_enemy;
				cooldown_for_enemy = unit->getAttackTarget()->isFlying() ? unit->getSelf()->getAirWeaponCooldown() : unit->getSelf()->getGroundWeaponCooldown();

				if (cooldown_for_enemy == 0) {
					//Ready To Attack

					//만약 체력이 너무 작을 경우 피하지 안하고 끝까지 싸우다 전사함 (도망칠 가치 없음)
					if (unit->getSelf()->getHitPoints() < 20) {
						if (!unit->getSelf()->isAttackFrame()) {

							Position bp = getBestPosition(unit->getSelf());
							if (bp != Position(-1, -1)) {
								unit->getSelf()->move(bp);
							}
						}
					}
					else {
						unit->getSelf()->attack(unit->getAttackTarget());
					}
				}
				else {
					//Not Ready To Attack
					if (!unit->getSelf()->isAttackFrame()) {
						//unit->getSelf()->move(Position(nearTiles.at((worstArea + 4) % 8).x * TILE_SIZE, nearTiles.at((worstArea + 4) % 8).y * TILE_SIZE));
						Position bp = getBestPosition(unit->getSelf());
						if (bp != Position(-1, -1)) {
							unit->getSelf()->move(bp);
						}
					}
				}
			}
		}
	}
}


Unit GoliathControl::getTargetUnit(Unit unit) {
	Unitset enemies = unit->getUnitsInRadius(getUnitGroundAttackRadius(unit), Filter::IsEnemy);
	if (!enemies.empty()) {
		int max_score = 0;
		Unit targetUnit = nullptr;

		for (auto& e : enemies) {

			if (pt_weight.count(e->getType())) {

				int score = pt_weight[e->getType()] * 10000 - (unit->getDistance(e));

				if (score > max_score) {
					max_score = score;
					targetUnit = e;
				}
			}
		}
		return targetUnit;
	}
	return nullptr;
}


void GoliathControl::updateIM() {


}

Unitset GoliathControl::getDangerUnitsInSeekRange(Unit unit) {
	Unitset enemies = unit->getUnitsInRadius((UnitTypes::Terran_Goliath).sightRange(), (Filter::IsEnemy || Filter::IsAlly) && !(Filter::IsOwned));
	Unitset dangerUnits;

	if (!enemies.empty()) {
		for (auto& e : enemies) {
			int safety_zone_radius = getUnitGroundAttackRadius(e) + 50; //My Variable; 

			Unit canUnderAttackUnit = e->getClosestUnit(Filter::GetType == UnitTypes::Terran_Goliath, safety_zone_radius);
			if (canUnderAttackUnit == unit) {
				dangerUnits.insert(e);
				//Broodwar << e->getType() << std::endl;
			}

		}
	}
	return dangerUnits;
}

Unitset GoliathControl::getSafeUnitsInSeekRange(Unit unit) {
	Unitset myUnits = unit->getUnitsInRadius((UnitTypes::Terran_Goliath).sightRange(), Filter::IsOwned && Filter::IsCompleted);
	Unitset safeUnits;

	if (!myUnits.empty()) {
		for (auto& u : myUnits) {
			if (tr_weight.count(u->getType())) {
				if (u->getDistance(unit) <= getUnitGroundAttackRadius(unit)) {
					safeUnits.insert(u);
				}
			}
		}
	}
	return safeUnits;
}



Unit GoliathControl::getTargetToHelpFriend(Unit unit) {
	Unitset friends = unit->getUnitsInRadius((UnitTypes::Terran_Goliath).sightRange(), Filter::IsOwned);
	Unit bestAnemy = nullptr;

	if (!friends.empty()) {
		for (auto& f : friends) {
			if (f->getType() == UnitTypes::Terran_Goliath) {
				if (f->isAttacking()) {
					if (pt_weight.count(f->getOrderTarget()->getType())) {
						if (bestAnemy == nullptr) {
							bestAnemy = f->getOrderTarget();
						}
						else {
							if (pt_weight[bestAnemy->getType()] < pt_weight[f->getOrderTarget()->getType()]) {
								//더 좋은 타겟 발견
								bestAnemy = f->getOrderTarget();
							}
						}
					}
				}
			}
		}
	}

	return bestAnemy;
}

void GoliathControl::drawGoliathControlUnit(Goliath* unit) {
	BWAPI::Broodwar->drawCircleMap(unit->getSelf()->getPosition(), 8, Colors::Purple, true);
}

vector<TilePosition> GoliathControl::getNearTilePositions(TilePosition t_position) {
	vector<TilePosition> nearTilePosition;
	//snail 
	nearTilePosition.push_back(TilePosition(t_position.x, t_position.y - 5));
	nearTilePosition.push_back(TilePosition(t_position.x + 5, t_position.y - 5));
	nearTilePosition.push_back(TilePosition(t_position.x + 5, t_position.y));
	nearTilePosition.push_back(TilePosition(t_position.x + 5, t_position.y + 5));
	nearTilePosition.push_back(TilePosition(t_position.x, t_position.y + 5));
	nearTilePosition.push_back(TilePosition(t_position.x - 5, t_position.y + 5));
	nearTilePosition.push_back(TilePosition(t_position.x - 5, t_position.y));
	nearTilePosition.push_back(TilePosition(t_position.x - 5, t_position.y - 5));

	return nearTilePosition;
}

int GoliathControl::getAngleTwoPoints(Position p1, Position p2) {
	float angle_footage = atan2((float)p2.y - p1.y, (float)p2.x - p1.x) * (180 / 3.1415f);
	float angle;
	if (angle_footage < 0) {
		angle = (180 - angle_footage * (-1)) + 180;
	}
	else {
		angle = angle_footage;
	}

	return (((((int)angle + 360) % 360) + 270) % 360);
}

int GoliathControl::getAngleArea(int angle) {
	int areaNum;
	if (337 < angle || angle <= 22) {
		areaNum = 0;
	}
	else if (22 < angle && angle <= 67) {
		areaNum = 1;
	}
	else if (67 < angle && angle <= 112) {
		areaNum = 2;
	}
	else if (112 < angle && angle <= 157) {
		areaNum = 3;
	}
	else if (157 < angle && angle <= 202) {
		areaNum = 4;
	}
	else if (202 < angle && angle <= 247) {
		areaNum = 5;
	}
	else if (247 < angle && angle <= 292) {
		areaNum = 6;
	}
	else if (292 < angle && angle <= 337) {
		areaNum = 7;
	}
	return areaNum;
}

int GoliathControl::getUnitGroundAttackRadius(Unit unit) {
	UnitType unitType = unit->getType();
	Player p = unit->getPlayer(); //Player unit Owned

	int attack_radius = unitType.groundWeapon().maxRange();

	if (unitType == UnitTypes::Terran_Goliath) {
		if (p->getUpgradeLevel(UpgradeTypes::U_238_Shells)) {
			attack_radius = (int)(unitType.groundWeapon()).maxRange() * 1.25;
		}
	}
	else if (unitType == UnitTypes::Protoss_Dragoon) {
		if (p->getUpgradeLevel(UpgradeTypes::Singularity_Charge)) {
			attack_radius = (int)(unitType.groundWeapon()).maxRange() * 1.5;
		}
	}
	else if (unitType == UnitTypes::Terran_Goliath) {
		if (p->getUpgradeLevel(UpgradeTypes::Charon_Boosters)) {
			//attack_radius = (int)(unitType.groundWeapon()).maxRange() * 1.6;
		}
	}
	return attack_radius;
}

int GoliathControl::getUnitAirttackRadius(Unit unit) {
	UnitType unitType = unit->getType();
	Player p = unit->getPlayer(); //Player unit Owned

	int attack_radius = unitType.airWeapon().maxRange();

	if (unitType == UnitTypes::Terran_Goliath) {
		if (p->getUpgradeLevel(UpgradeTypes::Charon_Boosters)) {
			attack_radius = (int)(unitType.airWeapon()).maxRange() * 1.6;
		}
	}
	return attack_radius;
}


Position GoliathControl::getBestPosition(Unit unit) {
	Position temp_pos;
	int sum_x = 0;
	int sum_y = 0;
	int gasu = 0;
	bool enemy = false;
	bool reaver = false;
	//Just For Vector Test
	Unitset enemies = Broodwar->getUnitsInRadius(unit->getPosition(), unit->getType().sightRange(), !Filter::IsOwned);
	for (auto& e : enemies) {
		if (e->getType() == UnitTypes::Protoss_Reaver) {
			reaver = true;
		}
		if (pt_weight[e->getType()] > 3) {
			gasu++;
		}
		if (e->getType() == UnitTypes::Protoss_Shuttle) continue;
		enemy = true;
		//Get Score Vector Position
		int dis = getDistance(unit->getPosition(), e->getPosition());
		if (dis == 0) {
			dis = 1;
		}

		float _x = 0;
		float _y = 0;
		_x = (e->getPosition().x - unit->getPosition().x) / (float)dis * ((pt_weight[e->getType()]) / (float)10);
		_y = (e->getPosition().y - unit->getPosition().y) / (float)dis * ((pt_weight[e->getType()]) / (float)10);

		sum_x += (-1) * unit->getType().sightRange() * _x * (1 - (getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange()));
		sum_y += (-1) * unit->getType().sightRange() * _y * (1 - (getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange()));
		Position scorePosition = Position(

			unit->getPosition().x + (-1) * unit->getType().sightRange() * _x * (1 - (getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange())),
			unit->getPosition().y + (-1) * unit->getType().sightRange() * _y * (1 - (getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange()))
			);
		Broodwar->drawLineMap(unit->getPosition(), scorePosition, Colors::Red);
	}

	Unitset enemies2 = (Unitset)(Broodwar->getUnitsInRadius(unit->getPosition(), unit->getType().sightRange(), Filter::IsOwned));
	for (auto& e : enemies2) {
		if (e == unit || e->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode || e->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) continue;
		//Get Score Vector Position
		if (tr_weight[e->getType()] > 3) {
			gasu++;
		}
		int dis = getDistance(unit->getPosition(), e->getPosition());
		if (dis == 0) {
			dis = 1;
		}
		float _x = 0;
		float _y = 0;

		Position scorePosition;
		if (reaver) {
			_x = (e->getPosition().x - unit->getPosition().x) / (float)dis * ((pt_weight[e->getType()]) / (float)10);
			_y = (e->getPosition().y - unit->getPosition().y) / (float)dis * ((pt_weight[e->getType()]) / (float)10);

			sum_x += (-1.5) * unit->getType().sightRange() * _x * (1 - (getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange()));
			sum_y += (-1.5) * unit->getType().sightRange() * _y * (1 - (getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange()));
			scorePosition = Position(

				unit->getPosition().x + (-1.5) * unit->getType().sightRange() * _x * (1 - (getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange())),
				unit->getPosition().y + (-1.5) * unit->getType().sightRange() * _y * (1 - (getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange()))
				);
		}
		else {
			_x = (e->getPosition().x - unit->getPosition().x) / (float)dis * ((tr_weight[e->getType()]) / (float)10);
			_y = (e->getPosition().y - unit->getPosition().y) / (float)dis * ((tr_weight[e->getType()]) / (float)10);
			sum_x += unit->getType().sightRange() * _x * ((getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange()));
			sum_y += unit->getType().sightRange() * _y * ((getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange()));

			scorePosition = Position(
				unit->getPosition().x + unit->getType().sightRange() * _x * ((getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange())),
				unit->getPosition().y + unit->getType().sightRange() * _y * ((getDistance(unit->getPosition(), e->getPosition()) / (float)unit->getType().sightRange()))
				);
		}



		if (pt_weight[e->getType()] > 3) {
			gasu++;
		}
		Broodwar->drawLineMap(unit->getPosition(), scorePosition, Colors::Blue);
	}
	if (gasu) {
		Broodwar->drawTextMap(unit->getPosition(), "%d", gasu);
		temp_pos = Position(
			unit->getPosition().x + (sum_x / gasu) * 2,
			unit->getPosition().y + (sum_y / gasu) * 2
			);
	}
	if (temp_pos != Position(0, 0)) {
		Broodwar->drawLineMap(unit->getPosition(), temp_pos, Colors::Green);
		return temp_pos;
	}
	else {
		return Position(-1, -1);
	}
	//if (getDistance(temp_pos, unit->getPosition()) > 50) {
	//}
}

int GoliathControl::getWorstAreaScore(Goliath* unit) {
	int eval[8] = { 0 };

	Unitset dangerUnits = getDangerUnitsInSeekRange(unit->getSelf());
	if (dangerUnits.size() != 0) {
		for (auto& e : dangerUnits) {
			int pos = getAngleArea(getAngleTwoPoints(e->getPosition(), unit->getSelf()->getPosition()));
			eval[pos] += pt_weight[e->getType()] * (-1);

			Broodwar->drawLineMap(e->getPosition(), unit->getSelf()->getPosition(), Colors::Red);
		}
	}
	Unitset safeUnits = getSafeUnitsInSeekRange(unit->getSelf());
	if (safeUnits.size() != 0) {
		for (auto& s : safeUnits) {
			if (tr_weight.count(s->getType()) && s != unit->getSelf()) {
				int pos = getAngleArea(getAngleTwoPoints(s->getPosition(), unit->getSelf()->getPosition()));
				eval[pos] += tr_weight[s->getType()] * (1);
			}
		}
	}

	vector<TilePosition> nearTiles = getNearTilePositions(TilePosition(unit->getSelf()->getTilePosition().x + 1, unit->getSelf()->getTilePosition().y + 1));

	int worstScore = 0;
	int worstArea = NULL;

	unit->setAttackTarget(getTargetUnit(unit->getSelf()));


	for (int i = 0; i < 8; i++) {
		if (worstScore > eval[i]) {
			worstArea = i;
			worstScore = eval[i];
		}
	}
	return worstScore;
}

Unit GoliathControl::getClosestTankToSave(Unit unit) {
	Unitset units = unit->getUnitsInRadius((UnitTypes::Terran_Siege_Tank_Siege_Mode).groundWeapon().maxRange(), Filter::IsOwned);

	Unit tankToHelp = nullptr;
	int max_score = 0;

	//Retrives all defence tanks;
	list<Tank*> tanks = UnitControl::getInstance()->tanks;
	list<Unit> defence_tank_units;

	for (auto t = tanks.begin(); t != tanks.end(); t++) {
		if ((*t)->getState() == Tank::STATE::DEFENCE) {
			defence_tank_units.push_back((*t)->getSelf());
		}
	}

	for (auto& u : units) {
		auto it = std::find(defence_tank_units.begin(), defence_tank_units.end(), u);
		if (it != defence_tank_units.end())
		{

			continue;
		}

		if (u->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode || u->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
			Unitset closeUnits = u->getUnitsInRadius((u->getType()).sightRange(), Filter::IsAlly || Filter::IsEnemy);

			float score = 0;
			for (auto& enemy : closeUnits) {
				if (pt_weight.count(enemy->getType())) {
					score += (pt_weight[enemy->getType()] * (((u->getType()).sightRange()) - (u->getDistance(enemy))) * (0.1));
				}


			}
			if (score > max_score) {
				max_score = score;
				tankToHelp = u;
			}
			Broodwar->drawTextMap(Position(u->getPosition().x + TILE_SIZE, u->getPosition().y - TILE_SIZE), "%.1f", score);
		}

	}

	if (max_score == 0) {
		//도와야할 탱크 없을 시 그냥 가까운탱크 지정
		tankToHelp = unit->getClosestUnit(Filter::GetType == UnitTypes::Terran_Siege_Tank_Siege_Mode || Filter::GetType == UnitTypes::Terran_Siege_Tank_Tank_Mode, (UnitTypes::Terran_Siege_Tank_Siege_Mode).sightRange() + 50);
	}
	return tankToHelp;
}

