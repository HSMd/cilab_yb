#include "SimCityBuild2.h"
#include "../Single.h"
#include "UtilMgr.h"

SimCity_2::SimCity_2(){
		
	this->tMgr = &cmd->getTerritoryMgr();
	out = nullptr;
	frameCount = 0;

	switch (mapInfo->getDir()){
	case 0:
		front_brkPos = TilePosition(89, 16);
		front_splyPos = TilePosition(93, 18);
		front_trtPos = TilePosition(94, 16);

		cntPos = TilePosition(97, 1);
		cntPos2 = TilePosition(118, 26);

		trtPoses.push(front_trtPos);
		trtPoses.push(TilePosition(103, 17));		
		trtPoses.push(mapInfo->getMyStartBase()->getTilePosition() + TilePosition(-2, -1));

		multiSplyPos.push_back(TilePosition(110, 66));
		multiSplyPos.push_back(TilePosition(113, 66));
		multiSplyPos.push_back(TilePosition(116, 1));
		multiSplyPos.push_back(TilePosition(115, 2));

		break;

	case 1:
		front_brkPos = TilePosition(105, 92);
		front_splyPos = TilePosition(109, 91);
		front_trtPos = TilePosition(110, 94);

		cntPos = TilePosition(123, 96);
		cntPos2 = TilePosition(106, 120);

		trtPoses.push(front_trtPos);
		trtPoses.push(TilePosition(109, 101));		
		trtPoses.push(mapInfo->getMyStartBase()->getTilePosition() + TilePosition(-2, 2));

		multiSplyPos.push_back(TilePosition(66, 114));
		multiSplyPos.push_back(TilePosition(68, 116));
		multiSplyPos.push_back(TilePosition(66, 118));

		break;

	case 2:
		front_brkPos = TilePosition(18, 36);
		front_splyPos = TilePosition(21, 34);
		front_trtPos = TilePosition(16, 33);

		cntPos = TilePosition(16, 25);
		cntPos2 = TilePosition(21, 10);

		trtPoses.push(front_trtPos);
		trtPoses.push(TilePosition(16, 26));		
		trtPoses.push(mapInfo->getMyStartBase()->getTilePosition() + TilePosition(4, -1));

		multiSplyPos.push_back(TilePosition(55, 11));
		multiSplyPos.push_back(TilePosition(57, 13));
		multiSplyPos.push_back(TilePosition(59, 15));

		break;

	case 3:
		front_brkPos = TilePosition(32, 110);
		front_splyPos = TilePosition(32, 108);
		front_trtPos = TilePosition(30, 112);

		cntPos = TilePosition(29, 124);
		cntPos2 = TilePosition(4, 99);

		trtPoses.push(TilePosition(20, 111));
		trtPoses.push(front_trtPos);
		trtPoses.push(mapInfo->getMyStartBase()->getTilePosition() + TilePosition(4, 5));

		multiSplyPos.push_back(TilePosition(11, 68));
		multiSplyPos.push_back(TilePosition(13, 70));
		multiSplyPos.push_back(TilePosition(15, 72));

		break;
	}

	// 임시로.
	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 1, UnitTypes::Terran_Factory, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Marine, 2, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Siege_Tank_Tank_Mode, 7, UnitTypes::Terran_SCV, 15));

	// 건물들
	build.push_back(scheduleWork(UnitTypes::Terran_Command_Center, cntPos, UnitTypes::Terran_SCV, 18));
	build.push_back(scheduleWork(UnitTypes::Terran_Supply_Depot, *AUTO_POS, UnitTypes::Terran_SCV, 22, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Engineering_Bay, *AUTO_POS, UnitTypes::Terran_SCV, 26, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Siege_Tank_Tank_Mode, 3, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Command_Center, cntPos2, UnitTypes::Terran_Factory, 2, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Refinery, *AUTO_POS, UnitTypes::Terran_Command_Center, 3, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Armory, *AUTO_POS, UnitTypes::Terran_Command_Center, 3, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Academy, *AUTO_POS, UnitTypes::Terran_Armory, 1, true));		
	build.push_back(scheduleWork(UnitTypes::Terran_Machine_Shop, *AUTO_POS, UnitTypes::Terran_Engineering_Bay, true));

	// 추가됨.
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Academy, 1));
	build.push_back(scheduleWork(UnitTypes::Terran_Comsat_Station, *AUTO_POS, UnitTypes::Terran_Academy, 1));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Academy, 1));
	build.push_back(scheduleWork(UnitTypes::Terran_Comsat_Station, *AUTO_POS, UnitTypes::Terran_Academy, 1));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Factory, 4, true));
	build.push_back(scheduleWork(UnitTypes::Terran_Factory, *AUTO_POS, UnitTypes::Terran_Academy, 4, true));
	
	// 터렛
	build.push_back(scheduleWork(UnitTypes::Terran_Missile_Turret, getTurretPos(), UnitTypes::Terran_Siege_Tank_Tank_Mode, 2));
	build.push_back(scheduleWork(UnitTypes::Terran_Missile_Turret, getTurretPos(), UnitTypes::Terran_Siege_Tank_Tank_Mode, 3));
	build.push_back(scheduleWork(UnitTypes::Terran_Missile_Turret, getTurretPos(), UnitTypes::Terran_Siege_Tank_Tank_Mode, 4));

	// DEFENCE
	switch (mapInfo->getDir()){
	case 0:
		siegePoses.push_back(TilePosition(100, 18));
		siegePoses.push_back(TilePosition(101, 19));
		siegePoses.push_back(TilePosition(93, 15));
		siegePoses.push_back(mapInfo->getMyStartBase()->getTilePosition() + TilePosition(0, 3));
		siegePoses.push_back(TilePosition(92, 14));

		break;

	case 1:
		siegePoses.push_back(TilePosition(105, 97));
		siegePoses.push_back(TilePosition(106, 98));
		siegePoses.push_back(TilePosition(112, 93));
		siegePoses.push_back(mapInfo->getMyStartBase()->getTilePosition() + TilePosition(-1, 0));
		siegePoses.push_back(TilePosition(113, 94));

		break;

	case 2:
		siegePoses.push_back(TilePosition(21, 30));
		siegePoses.push_back(TilePosition(20, 29));
		siegePoses.push_back(TilePosition(15, 36));
		siegePoses.push_back(mapInfo->getMyStartBase()->getTilePosition() + TilePosition(5, 3));
		siegePoses.push_back(TilePosition(14, 35));

		break;

	case 3:
		siegePoses.push_back(TilePosition(25, 110));
		siegePoses.push_back(TilePosition(23, 109));
		siegePoses.push_back(TilePosition(33, 114));
		siegePoses.push_back(mapInfo->getMyStartBase()->getTilePosition() + TilePosition(5, 0));
		siegePoses.push_back(TilePosition(34, 114));

		break;
	}
}

SimCity_2::~SimCity_2(){

}

void SimCity_2::run(COMBAT_FLAG combat) {

	auto startTerritory = tMgr->getAllTerritorys()[0];

	if (me->getBuildings(UnitTypes::Terran_Command_Center).size() == 1) {
		if (cmd->getUnitScheduleCount(UnitTypes::Terran_SCV) < 1) {
			cmd->addScvCount();
			cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_SCV, startTerritory->getCmdCenter()->getSelf()), 1, 1);
		}
	}
	else {
		//SCV 스케쥴러에 추가합니다.
		if (cmd->getUnitScheduleCount(UnitTypes::Terran_SCV) < tMgr->getAllTerritorys().size()) {
			for (auto territory : tMgr->getAllTerritorys()) {
				if (!territory->isIdle()) {
					continue;
				}

				int require = territory->getRequireSCVCount();

				if (1 < require && cmd->getReadyTrain(territory->getCmdCenter()->getSelf()) && cmd->getUnitScheduleCount(UnitTypes::Terran_SCV) < 1) {
					cmd->addScvCount();
					cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_SCV), 1, 1);
				}
			}
		}
	}

	// 두번째 확장시.
	if (!flags[SCV_MOVE] && 2 <= tMgr->getAllTerritorys().size()) {
		if (me->getBuildings(UnitTypes::Terran_Command_Center)[1]->getSelf()->getTilePosition() == tMgr->getAllTerritorys()[1]->getBaseTilePosition()) {
			bw << "EXPAND" << endl;
			auto next = mapInfo->getCandidateLocation();

			tMgr->getAllTerritorys()[1]->init(next, me->getBuildings(UnitTypes::Terran_Command_Center).back());
			tMgr->getAllTerritorys()[0]->moveTerritory(tMgr->getAllTerritorys()[1]);

			mapInfo->addTerritory(next);

			flags[SCV_MOVE] = true;
		}
	}

	// 세번째 확장시.
	if (flags[EXTAND_MORE] && !flags[NO_SCV]) {
		auto multi = tMgr->getAllTerritorys()[2];

		if (multi->getBaseTilePosition() == me->getBuildings(UnitTypes::Terran_Command_Center)[2]->getSelf()->getTilePosition()) {
			auto next = mapInfo->getCandidateLocation();
			multi->init(next, multi->getCmdCenter());
			mapInfo->addTerritory(next);

			flags[NO_SCV] = true;
		}
	}

	// 세번째 탱크들 보내기.
	/*
	if (flags[NO_SCV]) {
		bool safe = true;
		for (auto pos : siegePoses) {
			auto tank = holdTanks[pos];

			if (tank == nullptr) {
				safe = false;
				break;
			}
		}

		if (safe) {
			for (int i = 0; i < 3 - subTanks.size(); i++) {
				auto tank = getIdleTank();
				if (tank != nullptr)
					subTanks.push_back(tank);
				else
					break;
			}

			for (auto& tank : subTanks) {
				auto multi = mapInfo->getMyTerritory()[2];

				if (5 < getDistance(multi->getTilePosition(), tank->getSelf()->getTilePosition())) {
					tank->getSelf()->move(multi->getPosition());
				}
			}
		}
	}
	*/

	// 시즈모드 업글
	if (!flags[UPGRADE_SIEGE_MODE] && 1 <= me->getBuildings(UnitTypes::Terran_Machine_Shop).size() &&
		1 <= me->getImmediatelyUnit(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()) {
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[0]->getSelf()->research(TechTypes::Tank_Siege_Mode)) {
			flags[UPGRADE_SIEGE_MODE] = true;
		}
	}

	// 마인 업글
	if (!flags[UPGRADE_MINE] && 2 <= me->getImmediatelyBuilding(UnitTypes::Terran_Machine_Shop).size()) {
		if (me->getBuildings(UnitTypes::Terran_Machine_Shop)[0]->getSelf()->research(TechTypes::Spider_Mines)) {
			flags[UPGRADE_MINE] = true;
		}
	}

	// 탱크 공업 1
	if (!flags[UPGRADE_VEHICLE_WEAPONS] && 1 <= me->getBuildings(UnitTypes::Terran_Armory).size()) {
		if (me->getBuildings(UnitTypes::Terran_Armory).back()->getSelf()->upgrade(UpgradeTypes::Terran_Vehicle_Weapons)) {
			flags[UPGRADE_VEHICLE_WEAPONS] = true;
		}
	}

	// 탱크 생산
	if (2 <= me->getBuildings(UnitTypes::Terran_Factory).size() && cmd->getUnitScheduleCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) < 2) {
		cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_Siege_Tank_Tank_Mode), 1);
	}

	// 벌쳐 생산
	if (2 <= me->getBuildings(UnitTypes::Terran_Factory).size() && cmd->getUnitScheduleCount(UnitTypes::Terran_Vulture) < 2) {
		cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_Vulture), 1);
	}

	// 골리앗 생산 해봄ㅋ
	//if (2 <= me->getBuildings(UnitTypes::Terran_Factory).size() && cmd->getUnitScheduleCount(UnitTypes::Terran_Goliath) < 2) {
	//	cmd->addUnitSchedule(UnitWork(UnitTypes::Terran_Goliath), 1);
	//}

	// 서플라이 자동 생산.
	if (me->getBuildings(UnitTypes::Terran_Factory).size() <= 2 && frameCount + 800 < bw->getFrameCount()){
		if (me->getBuildings(UnitTypes::Terran_Factory).size() <= 2) {
			if (cmd->getSupply() < 10 * 2) {
				frameCount = bw->getFrameCount();
				cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
			}
		}
		
		/*
		if(cmd->getSupply() < me->getBuildings(UnitTypes::Terran_Factory).size() * 4) {

		frameCount = bw->getFrameCount();
		cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 1);
		*/
	}

	if (2 < me->getBuildings(UnitTypes::Terran_Factory).size() && frameCount + 400 < bw->getFrameCount()){
		if (cmd->getSupply() < 20 * 2) {
			frameCount = bw->getFrameCount();
			cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, *AUTO_POS, 1, 2);
		}
	}

	// 예약된 BUILD 들이 점화되면 수행된다.
	for (auto& work : build){
		auto targetType = work.getTargetType();

		if (!work.getFire() && work.canFire()){
			if (targetType.isBuilding()){
				if (cmd->getProperBuilding(UnitTypes::Terran_Siege_Tank_Tank_Mode) == nullptr){
					cmd->addStructureSchedule(work, 1, 0);
					work.onFire();
				}				
			}
			else{

				cmd->addUnitSchedule(targetType, work.getCount(), 0);
				work.onFire();
			}
		}
	}	

	// 앞마당 확장!! 두둥!!
	if (!flags[EXTAND_FRONT] && 2 <= me->getBuildings(UnitTypes::Terran_Command_Center).size()){

		if (0 < me->getBuildings(UnitTypes::Terran_Barracks).size() && me->getBuildings(UnitTypes::Terran_Barracks)[0]->getSelf()->isIdle()){
			auto cnter = dynamic_cast<Structure*>(me->getBuildings(UnitTypes::Terran_Command_Center)[1]);
			auto brk = dynamic_cast<Structure*>(me->getBuildings(UnitTypes::Terran_Barracks)[0]);

			moveBuilding.push_back(cnter);
			moveBuilding.push_back(brk);

			cnter->orderMove(mapInfo->getNearBaseLocation()->getTilePosition());
			brk->orderMove(front_brkPos);
			cmd->addStructureSchedule(scheduleWork(UnitTypes::Terran_Supply_Depot, front_splyPos, UnitTypes::Terran_Factory, 1), 1, 2);
			//cmd->addStructureSchedule(scheduleWork(UnitTypes::Terran_Missile_Turret, front_trtPos, UnitTypes::Terran_Factory, 1), 1, 2);

			tMgr->addTerritory(new Territory(mapInfo->getNearBaseLocation()));
			//tMgr->getAllTerritorys()[0]->moveTerritory(tMgr->getAllTerritorys()[1]);

			flags[EXTAND_FRONT] = true;
		}		
	}

	// 세번째 멀티!! 두둥!!
	if (!flags[EXTAND_MORE] && 3 <= me->getBuildings(UnitTypes::Terran_Command_Center).size()) {

		auto next = mapInfo->getCandidateLocation();
		auto cnter = dynamic_cast<Structure*>(me->getBuildings(UnitTypes::Terran_Command_Center)[2]);

		cnter->orderMove(next->getTilePosition());
		moveBuilding.push_back(cnter);
		
		tMgr->addTerritory(new Territory(next, me->getBuildings(UnitTypes::Terran_Command_Center).back()));
		flags[EXTAND_MORE] = true;
	}

	// 나온 탱크들 배치.
	if (0 < me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()){
		//tankDefence();
	}

	move();
	errorSCV();
	chkEnd();
}

void SimCity_2::move(){

	for (auto building : moveBuilding){
		if (building->getState() == Structure::STATE::MOVE){
			if (getDistance(building->getSelf()->getTilePosition(), building->getTargetPos()) < 3){
				building->orderLand(building->getTargetPos());
			}
			else{
				building->getSelf()->move(tile2pos(building->getTargetPos()));
			}
		}
		else if (building->getState() == Structure::STATE::LAND){

			if (building->getSelf()->isIdle()){
				building->orderLand(building->getTargetPos());
			}
			else{
				if (!building->getSelf()->isLifted()){
					building->orderIdle();
				}
			}
		}
	}
}

void SimCity_2::errorSCV(){

	if (flags[EXTAND_FRONT]){
		return;
	}

	auto startTerritory = tMgr->getAllTerritorys()[0];

	// 입구가 막혔을 경우 쫒겨난 유닛에 대한 대쳐.
	if (out == nullptr){
		for (auto scv : startTerritory->getWorkSCV()){
			if (scv->getState() != SCV::STATE::RETURN){
				continue;
			}

			int dis = getDistance(scv->getSelf()->getTilePosition(), mapInfo->getNearBaseLocation()->getTilePosition());
			if (dis < 9){
				out = scv;
			}
		}
	}
	else{
		auto b = me->getBuildings(UnitTypes::Terran_Barracks)[0];
		if (!flags[ENTRANCE_OPEN]){
			bw << "OPEN" << endl;
			outPos = b->getSelf()->getTilePosition();
			if (b->getSelf()->isIdle() && b->getSelf()->isCompleted())
				if (b->getSelf()->lift())
					flags[ENTRANCE_OPEN] = true;
		}
		else{
			if (!flags[ENTRANCE_CLOSE] && getDistance(out->getSelf()->getTilePosition(), startTerritory->getCmdCenter()->getSelf()->getTilePosition()) < 17){
				bw << "CLOSE" << endl;
				if (b->getSelf()->land(outPos))
					flags[ENTRANCE_CLOSE] = true;
			}
		}
	}
}

bool SimCity_2::chkEnd(){

	return false;
}

TilePosition SimCity_2::getTurretPos(){

	if (!trtPoses.empty()){
		auto re = trtPoses.front();
		trtPoses.pop();		

		return re;
	}

	return *AUTO_POS;
}

// DEFENCE

void SimCity_2::tankDefence(){
	
	for (auto pos : siegePoses){
		if (holdTanks[pos] == nullptr){
			holdTanks[pos] = getIdleTank();

			if (holdTanks[pos] == nullptr){
				break;
			}
		}
	}

	for (auto pos : siegePoses){
		auto tank = holdTanks[pos];

		if (tank != nullptr && !tank->getSelf()->exists()) {
			holdTanks[pos] = nullptr;
			continue;
		}

		if (tank == nullptr || tank->getSelf()->canUnsiege() || !tank->getSelf()->isCompleted())
			continue;

		if (getDistance(pos, tank->getSelf()->getTilePosition()) < 2){
			if (tank->getSelf()->canSiege()){
				tank->getSelf()->siege();
			}		
		}
		else{
			holdTanks[pos]->getSelf()->move(tile2pos(pos));
		}
	}
}

Tank* SimCity_2::getIdleTank(){

	for (auto u : me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
		Tank* tank = dynamic_cast<Tank*>(u);
		if (tank->getState() == Tank::STATE::IDLE){
			tank->setState(Tank::STATE::MOVE);
			return tank;
		}
	}

	return nullptr;
}