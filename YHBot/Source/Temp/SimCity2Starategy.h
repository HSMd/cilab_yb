#pragma once
#include "MyUnit.h"
#include "StateMgr.h"
#include "TerritoryMgr.h"
#include <map>

using namespace std;

class SimCity2Strategy{

private:
	Unit Dragoon;
	vector<Position> marinePosition;

	vector<SCV*> guardSCV;
	vector<Marine*> guardmarine;

	map<Marine*, Position> holdMarine;
	map<Marine*, Position> RetreatMarine;

	void(SimCity2Strategy::*Func)();

public:
	SimCity2Strategy();
	~SimCity2Strategy();

	TerritoryMgr* tMgr;

	// INIT FUNC
	void init();
	Marine* getIdleMarine();

	// CONTROL FUNC
	void scvControl(SCV* scv);
	void MarineControl(Marine* marine);

	// FLAG FUNC
	bool DragoonDetect();

	bool isInvasion();

	// RUN FUNC
	void run();
	void detectDragoon();

	void marineHold();
	void end();
};