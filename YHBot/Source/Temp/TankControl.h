#pragma once
#include <BWAPI.h>
#include "MyUnit.h"
#include "SimCityBuild2.h"

using namespace BWAPI;

class TankControl {

private:
	bool flag_Physical = false;
	int TankIM[128][128];
	vector<TilePosition> sim1Poses;
	vector<TilePosition> sim2Poses;
	vector<TilePosition> sim3Poses;
	map<TilePosition, Tank*> defenceTanks;
	map<TilePosition, Tank*> defence1Tanks;
public:
	
	TankControl();
	~TankControl();

	SimCity_2 *sim2;

	static TankControl* create();
	static TankControl* getInstance();
	static TankControl* s_TankControl;
	void run(Tank*);
	void onFrame();

	//control func
	void WAIT_Control(Tank* unit);
	void TC_DEFENCE1_Control(Tank* unit);
	void TC_DEFENCE2_Control(Tank* unit);
	void TC_DEFENCE3_Control(Tank* unit);
	void KJ_WAIT_Control(Tank* unit);
	void KJ_ATTACK_Control(Tank* unit);
	void DEFENCE_Control(Tank* unit);
	void RETURN_Control(Tank* unit);
	void MOVE_Control(Tank* unit);
	void CRAZY_Control(Tank* unit);
	void SIEGE_Control(Tank* unit);

	Unit getTargetUnit(Tank* unit);
	float getTargetUnitValue(UnitType ut);
	float getTargetUnitValueforSiege(UnitType ut);
	float getUnitValueinSplash(Unit target);
	bool oneShotBonus(Unit target, Tank* unit);
	Unit getUnitforID(int id);
	float getExpectDamage(Position p, int range);

	//other func
	void goodLuck(Tank *unit);
	void kitingMove(Tank* unit);
	void checkSiege(Tank* unit);
	void drawTankControlUnit(Tank* unit);
	void drawTankControl();
	void updateIM();

	Position attackPosition;
};