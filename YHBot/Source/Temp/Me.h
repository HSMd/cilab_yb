#pragma once

#include <BWAPI.h>
#include <BWTA.h>

#include <memory>
#include <vector>
#include "MyUnit.h"

using namespace std;

class Me
{
	map<int, vector <MyUnit*>> units;
	map<int, vector <MyUnit*>> buildings;	

	map<int, vector <MyUnit*>> createUnits;
	map<int, vector <MyUnit*>> createBuildings;

	vector<MyUnit*> allUnits;
	
	Me();
	Me(const Me& other);
	~Me();	
	

public:
	BWAPI::Player player();
	static Me* getInstance();

	int minerals();
	int gas();

	void init();
	void update();
	void run();
	void drawMe();

	void addUnit(BWAPI::Unit unit);
	void addUnitImmediately(BWAPI::Unit unit);
	void addBuildingImmediately(BWAPI::Unit building);
	void addBuilding(BWAPI::Unit building);

	vector<MyUnit*>& getUnits(int type);
	vector<MyUnit*>& getImmediatelyUnit(int type);

	vector<MyUnit*>& getBuildings(int type);
	vector<MyUnit*>& getImmediatelyBuilding(int type);	

	void addAllUnit(Unit unit);
	vector<MyUnit*>& getAllUnits();

	void updateUnit(UnitType unitType, bool immediate);
};

