#pragma once
#include "Work.h"
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"

class SimStrategy_1
{
	// 한번만 실행되어야 될 것들.
	enum FLAG{
	};

	map<int, bool> flags;
	int frameCount;

	// 자료구조.
	vector<SCV*> scvs;
	vector<Unit> closeEnemy;

	Unit pylon;
	vector<SCV*> guardSCV;

	vector<Position> pylonPosition;
	vector<Position> marinePosition;

	map<HMarine*, Position> holdMarine;
	map<SCV*, Position> holdSCV;

public:
	SimStrategy_1();
	~SimStrategy_1();

	void run(COMBAT_FLAG combat);
	void end();

	void normal();
	void guardPylonRush();
	void guardEnemySearch();
	void guardZealotRush();
	
	void chkCombat();

	void isInvasion();
	

	bool chkAlreadyEnemy(Unit unit); 
	void updateCloseEnemy();
	int getCloseEnemyCount(UnitType type);
	Unit getAttackTarget();
	void draw(COMBAT_FLAG combat);

	void show();
};

