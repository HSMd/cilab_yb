#include <list>
#include <vector>
#include <map>
#include <math.h>
#include "TankControl.h"
#include "CombatInfo.h"
#include "UtilMgr.h"
#include "../Single.h"

Tank::Tank(Unit _self) : MyUnit(_self) {
	state = STATE::IDLE;
	attackTarget = NULL;
	isMove = false;
	isSiege = false;
	DDeokUnit = false;
	defenceUnit = false;
	rPos = Position(0, 0);
	isJunjin = false;
	isBackup = false;
	isInTeam = false;
	frameCount = 0;
}

Tank::~Tank() {}

void TankControl::run(Tank* unit) {
	if (unit->getState() == Tank::STATE::IDLE) {
		TankControl::getInstance()->goodLuck(unit);
		//printf("탱크 할일 지정\n");
	}
	else if (unit->getState() == Tank::STATE::TC_DEFENCE1) {
		TankControl::getInstance()->TC_DEFENCE1_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::TC_DEFENCE2) {
		TankControl::getInstance()->TC_DEFENCE2_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::TC_DEFENCE3) {
		TankControl::getInstance()->TC_DEFENCE3_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::KJ_WAIT) {
		TankControl::getInstance()->KJ_WAIT_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::KJ_ATTACK) {
		TankControl::getInstance()->KJ_ATTACK_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::WAIT) {
		TankControl::getInstance()->WAIT_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::DEFENCE) {
		TankControl::getInstance()->DEFENCE_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::RETURN) {
		TankControl::getInstance()->RETURN_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::MOVE) {
		TankControl::getInstance()->MOVE_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::CRAZY) {
		TankControl::getInstance()->CRAZY_Control(unit);
	}
	else if (unit->getState() == Tank::STATE::SIEGE) {
		TankControl::getInstance()->SIEGE_Control(unit);
	}
	else {
		TankControl::getInstance()->goodLuck(unit);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Tank Control

TankControl* TankControl::s_TankControl;
TankControl* TankControl::create() {
	if (s_TankControl)
	{
		return s_TankControl;
	}
	s_TankControl = new TankControl();
	return s_TankControl;
}
TankControl* TankControl::getInstance() {
	return s_TankControl;
}
TankControl::TankControl() {
	// DEFENCE
	switch (mapInfo->getDir()){
	case 0:
		sim1Poses.push_back(TilePosition(106, 15));
		sim1Poses.push_back(TilePosition(101, 5));
		sim1Poses.push_back(TilePosition(0, 0));                                // chkFullUnit Debug

		sim2Poses.push_back(TilePosition(103, 18));
		sim2Poses.push_back(TilePosition(106, 15));
		sim2Poses.push_back(TilePosition(92, 11));
		sim2Poses.push_back(TilePosition(115, 10));
		sim2Poses.push_back(TilePosition(97, 15));
		sim2Poses.push_back(TilePosition(0, 0));
		
		sim3Poses.push_back(TilePosition(113, 53));
		sim3Poses.push_back(TilePosition(113, 55));
		sim3Poses.push_back(TilePosition(115, 54));
		
		break;

	case 1:
		sim1Poses.push_back(TilePosition(110, 101));
		sim1Poses.push_back(TilePosition(108, 103));
		sim1Poses.push_back(TilePosition(0, 0));

		sim2Poses.push_back(TilePosition(105, 99));
		sim2Poses.push_back(TilePosition(106, 100));
		sim2Poses.push_back(TilePosition(112, 93));
		sim2Poses.push_back(TilePosition(115, 117));
		sim2Poses.push_back(TilePosition(109, 95));
		sim2Poses.push_back(TilePosition(0, 0));
		
		sim3Poses.push_back(TilePosition(77, 115));
		sim3Poses.push_back(TilePosition(76, 117));
		sim3Poses.push_back(TilePosition(77, 117));
		
		break;

	case 2:
		sim1Poses.push_back(TilePosition(15, 26));
		sim1Poses.push_back(TilePosition(10, 24));
		sim1Poses.push_back(TilePosition(0, 0));

		sim2Poses.push_back(TilePosition(21, 30));
		sim2Poses.push_back(TilePosition(20, 29));
		sim2Poses.push_back(TilePosition(15, 36));
		sim2Poses.push_back(TilePosition(9, 10));
		sim2Poses.push_back(TilePosition(12, 35));
		sim2Poses.push_back(TilePosition(0, 0));

		sim3Poses.push_back(TilePosition(49, 10));
		sim3Poses.push_back(TilePosition(48, 11));
		sim3Poses.push_back(TilePosition(50, 11));

		break;

	case 3:
		sim1Poses.push_back(TilePosition(20, 115));
		sim1Poses.push_back(TilePosition(23, 122));
		sim1Poses.push_back(TilePosition(0, 0));

		sim2Poses.push_back(TilePosition(25, 110));
		sim2Poses.push_back(TilePosition(22, 109));
		sim2Poses.push_back(TilePosition(33, 114));
		sim2Poses.push_back(TilePosition(9, 120));
		sim2Poses.push_back(TilePosition(34, 115));
		sim2Poses.push_back(TilePosition(0, 0));

		sim3Poses.push_back(TilePosition(13, 72));
		sim3Poses.push_back(TilePosition(12, 75));
		sim3Poses.push_back(TilePosition(14, 76));

		break;
	}
}

TankControl::~TankControl() {}

void TankControl::onFrame() {
	updateIM();
}

void TankControl::goodLuck(Tank* unit) {
	if (!unit->getSelf()->isCompleted()) return;
	//checkSiege(unit);
	///printf("%d : goodLuck\n", unit->isSiege);
	//if (unit->isSiege) unit->setState(Tank::STATE::SIEGE);
	//if (!unit->isSiege) unit->setState(Tank::STATE::CRAZY);
}
void TankControl::TC_DEFENCE1_Control(Tank* unit) {              // 트리플 수비 SIM1
	// 죽은 유닛 지우기
	for (auto pos : sim1Poses) {
		if (defence1Tanks[pos] != nullptr) {
			if (!defence1Tanks[pos]->getSelf()->exists()) {
				defence1Tanks[pos] = nullptr;
			}
		}
	}
	// 유닛 넣기
	for (auto pos : sim1Poses) {
		if (defence1Tanks[pos] == nullptr) {
			if (!unit->defenceUnit) {
				defence1Tanks[pos] = unit;
				unit->defenceUnit = true;
			}

			if (defence1Tanks[pos] == nullptr) {
				break;
			}
		}
	}

	// 시즈 업글 x
	if (!bw->self()->hasResearched(TechTypes::Tank_Siege_Mode)) {
		//printf("시즈업글 x\n");
		Unit targetUnit = getTargetUnit(unit);
		if (targetUnit != NULL) { // 적군 o
			if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
				unit->getSelf()->attack(targetUnit, false);
			}
			else {
				for (auto pos : sim1Poses) {
					if (defence1Tanks[pos] == unit) {
						if (unit != nullptr && !unit->getSelf()->exists()) {
							defence1Tanks[pos] = nullptr;
							continue;
						}
						if (unit == nullptr) {
							continue;
						}
						if (getDistance(pos, unit->getSelf()->getTilePosition()) < 2) {
							unit->isMove = false;
						}
						else {
							unit->getSelf()->move(tile2pos(pos));
							unit->isMove = true;
						}
					}
				}
			}
		}
		else { // 적군 x
			for (auto pos : sim1Poses) {
				if (defence1Tanks[pos] == unit) {
					if (unit != nullptr && !unit->getSelf()->exists()) {
						defence1Tanks[pos] = nullptr;
						continue;
					}
					if (unit == nullptr) {
						continue;
					}
					if (getDistance(pos, unit->getSelf()->getTilePosition()) < 2) {
						unit->isMove = false;
					}
					else {
						unit->getSelf()->move(tile2pos(pos));
						unit->isMove = true;
					}
				}
			}
		}
	}
	// 시즈 업글 o
	else {
		//printf("시즈업글 o\n");
		for (auto pos : sim1Poses) {
			if (defence1Tanks[pos] == unit) {
				if (unit != nullptr && !unit->getSelf()->exists()) {
					defence1Tanks[pos] = nullptr;
					continue;
				}

				if (unit == nullptr) {
					continue;
				}

				if (getDistance(pos, unit->getSelf()->getTilePosition()) < 2) {
					if (bw->self()->hasResearched(TechTypes::Tank_Siege_Mode)) {
						unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
						unit->setState(Tank::STATE::DEFENCE);
						unit->isMove = false;
					}
				}

				else {
					unit->getSelf()->move(tile2pos(pos));
					unit->isMove = true;
				}
			}

			else {
				continue;
			}
		}
	}
	// 수비유닛 꽉차 있는지 확인
	bool chkFullWork = true;
	for (auto pos : sim1Poses) {
		if (defence1Tanks[pos] == nullptr) {
			chkFullWork = false;
			break;
		}
	}
	if (chkFullWork) {
		unit->DDeokUnit = true;
	}
	else {
		unit->DDeokUnit = false;
	}
}
void TankControl::TC_DEFENCE2_Control(Tank* unit) {              // 트리플 수비 SIM2
	// 죽은 유닛 지우기
	for (auto pos : sim2Poses) {
		if (defenceTanks[pos] != nullptr) {
			if (!defenceTanks[pos]->getSelf()->exists()) {
				defenceTanks[pos] = nullptr;
				//printf("지운다\n");
			}
		}
	}
	// 유닛 넣기
	for (auto pos : sim2Poses) {
		if (defenceTanks[pos] == nullptr) {
			if (!unit->defenceUnit) {
				defenceTanks[pos] = unit;
				unit->defenceUnit = true;
				//printf("넣는다\n");
			}
			
			if (defenceTanks[pos] == nullptr) {
				break;
			}
		}
	}
	
	// 이동하거나 시즈
	for (auto pos : sim2Poses) {
		if (defenceTanks[pos] == unit) {
			if (unit != nullptr && !unit->getSelf()->exists()) {
				defenceTanks[pos] = nullptr;
				continue;
			}

			if (unit == nullptr) {
				continue;
			}

			if (getDistance(pos, unit->getSelf()->getTilePosition()) < 2) {
				if (bw->self()->hasResearched(TechTypes::Tank_Siege_Mode)) {
					unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
					unit->setState(Tank::STATE::DEFENCE);
					unit->isMove = false;
					//printf("시즈\n");
				}
			}

			else {
				unit->getSelf()->move(tile2pos(pos));
				unit->isMove = true;
			}
		}

		else {
			continue;
		}
	}

	// 수비유닛 꽉차 있는지 확인
	bool chkFullWork = true;
	for (auto pos : sim2Poses) {
		if (defenceTanks[pos] == nullptr) {
			chkFullWork = false;
			break;
		}
	}

	if (chkFullWork) {
		unit->DDeokUnit = true;
	}
	else {
		unit->DDeokUnit = false;
	}
}

void TankControl::TC_DEFENCE3_Control(Tank* unit) {              // 트리플 수비 SIM3
	// 죽은 유닛 지우기
	for (auto pos : sim2Poses) {
		if (defenceTanks[pos] != nullptr) {
			if (!defenceTanks[pos]->getSelf()->exists()) {
				defenceTanks[pos] = nullptr;
				//printf("지운다\n");
			}
		}
	}
	// 유닛 넣기
	for (auto pos : sim2Poses) {
		if (defenceTanks[pos] == nullptr) {
			if (!unit->defenceUnit) {
				defenceTanks[pos] = unit;
				unit->defenceUnit = true;
				//printf("넣는다\n");
			}

			if (defenceTanks[pos] == nullptr) {
				break;
			}
		}
	}

	// 이동하거나 시즈
	for (auto pos : sim2Poses) {
		if (defenceTanks[pos] == unit) {
			if (unit != nullptr && !unit->getSelf()->exists()) {
				defenceTanks[pos] = nullptr;
				continue;
			}

			if (unit == nullptr) {
				continue;
			}

			if (getDistance(pos, unit->getSelf()->getTilePosition()) < 2) {
				if (bw->self()->hasResearched(TechTypes::Tank_Siege_Mode)) {
					unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
					unit->setState(Tank::STATE::DEFENCE);
					unit->isMove = false;
				}
			}

			else {
				unit->getSelf()->move(tile2pos(pos));
				unit->isMove = true;
			}
		}

		else {
			continue;
		}
	}

	// 삼룡이 방어

	// 본진 수비유닛 꽉차 있는지 확인
	bool chkDEF = true;
	for (auto pos : sim3Poses) {
		if (defenceTanks[pos] == nullptr) {
			chkDEF = false;
		}
	}
	if (chkDEF) {
		// 죽은 유닛 지우기
		for (auto pos : sim3Poses) {
			if (defenceTanks[pos] != nullptr) {
				if (!defenceTanks[pos]->getSelf()->exists()) {
					defenceTanks[pos] = nullptr;
					//printf("지운다\n");
				}
			}
		}
		// 유닛 넣기
		for (auto pos : sim3Poses) {
			if (defenceTanks[pos] == nullptr) {
				if (!unit->defenceUnit) {
					defenceTanks[pos] = unit;
					unit->defenceUnit = true;
					//printf("넣는다\n");
				}

				if (defenceTanks[pos] == nullptr) {
					break;
				}
			}
		}

		// 이동하거나 시즈
		for (auto pos : sim3Poses) {
			if (defenceTanks[pos] == unit) {
				if (unit != nullptr && !unit->getSelf()->exists()) {
					defenceTanks[pos] = nullptr;
					continue;
				}

				if (unit == nullptr) {
					continue;
				}

				if (getDistance(pos, unit->getSelf()->getTilePosition()) < 2) {
					if (bw->self()->hasResearched(TechTypes::Tank_Siege_Mode)) {
						unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
						unit->setState(Tank::STATE::DEFENCE);
						unit->isMove = false;
					}
				}

				else {
					unit->getSelf()->move(tile2pos(pos));
					unit->isMove = true;
				}
			}

			else {
				continue;
			}
		}
	}


	// 수비유닛 꽉차 있는지 확인
	bool chkFullWork = true;
	//int cntUnit = 0;
	for (auto pos : sim2Poses) {
		if (defenceTanks[pos] == nullptr) {
			chkFullWork = false;
			break;
			//cntUnit++;
		}
	}

	for (auto pos : sim3Poses) {
		if (defenceTanks[pos] == nullptr) {
			chkFullWork = false;
			break;
		}
	}

	//printf("cntUnit : %d\n", cntUnit);
	if (chkFullWork) {
		unit->DDeokUnit = true;
	}
	else {
		unit->DDeokUnit = false;
	}
}

void TankControl::KJ_WAIT_Control(Tank* unit) {              // 올인 투팩 대기
	unit->attackTarget = getTargetUnit(unit);

	if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
		Unit targetUnit = getTargetUnit(unit);
		if (targetUnit == NULL) {
			if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
					unit->getSelf()->attack(attackPosition, false);
					//unit->isMove = false;
				}
			}
		}
		else {
			unit->getSelf()->attack(targetUnit, false);
			//unit->isMove = false;
		}
	}
	else {
		kitingMove(unit);
	}
}

void TankControl::KJ_ATTACK_Control(Tank* unit) {              // 올인 투팩 공격
	unit->attackTarget = getTargetUnit(unit);

	if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
		Unit targetUnit = getTargetUnit(unit);
		if (targetUnit == NULL) {
			if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
					unit->getSelf()->attack(attackPosition, false);
					//unit->isMove = false;
				}
			}
		}
		else {
			unit->getSelf()->attack(targetUnit, false);
			//unit->isMove = false;
		}
	}
	else {
		kitingMove(unit);
	}
	/*else {
		if (unit->getSelf()->getHitPoints() < 65 && !unit->isMove) {
			unit->getSelf()->move(CombatInfo::getInstance()->myStartingBase->getPosition(), false);
			unit->isMove = true;
		}
	}*/
}

void TankControl::WAIT_Control(Tank* unit) {                     // 대기 
	unit->attackTarget = getTargetUnit(unit);

	if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
		Unit targetUnit = getTargetUnit(unit);
		if (targetUnit == NULL) {
			if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
					unit->getSelf()->attack(attackPosition, false);
					//unit->isMove = false;
				}
			}
		}
		else {
			unit->getSelf()->attack(targetUnit, false);
			//unit->isMove = false;
		}
	}
	else {
		kitingMove(unit);
	}
}
void TankControl::DEFENCE_Control(Tank* unit) {                  // 방어
	//if (!Broodwar->self()->hasResearched(TechTypes::Tank_Siege_Mode)) return;
	unit->isMove = false;
	if (unit->isSiege == false && unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		unit->isSiege = true;
		unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
	}
	unit->attackTarget = getTargetUnit(unit);

	if (unit->getSelf()->getGroundWeaponCooldown() == 0 && unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		Unit targetUnit = getTargetUnit(unit);
		if (targetUnit == NULL) {
			
		}
		else {
			unit->getSelf()->attack(targetUnit, false);
			//BWAPI::Broodwar->drawCircleMap(targetUnit->getPosition(), 30, Colors::Brown);
		}
	}
}
void TankControl::RETURN_Control(Tank* unit) {                   // 작업 완료 후 병력 무리로 이동

}
void TankControl::MOVE_Control(Tank* unit) {					 // 이동
	unit->isMove = true;
}
void TankControl::CRAZY_Control(Tank* unit) {         			 // 피지컬 모드
	checkSiege(unit);
	if (unit->isSiege) {
		unit->setState(Tank::STATE::SIEGE);
	}

	if (unit->isSiege && unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
		//printf("시즈한당\n");
	}
	if (!unit->isSiege && unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
	}

	if (unit->getSelf()->getGroundWeaponCooldown() == 0) {
		if (unit->attackTarget == NULL) {
			if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 48 == 0) {
					unit->getSelf()->attack(unit->rPos, false);
				}
			}
		}
		else {
			unit->getSelf()->attack(unit->attackTarget, false);
			// BWAPI::Broodwar->drawCircleMap(targetUnit->getPosition(), 30, Colors::Teal);
		}
	}
	else {
		kitingMove(unit);
	}
}

void TankControl::SIEGE_Control(Tank* unit) {
	//printf("들어왔니..?\n");
	checkSiege(unit);
	if (!unit->isSiege) {
		unit->setState(Tank::STATE::CRAZY);
	}
	if (unit->isSiege && unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
		//printf("시즈한당\n");
	}

	if (!unit->isSiege && unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
	}

	unit->attackTarget = getTargetUnit(unit);

	if (unit->getSelf()->getGroundWeaponCooldown() == 0 && unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		Unit targetUnit = getTargetUnit(unit);
		if (targetUnit == NULL) {

		}
		else {
			unit->getSelf()->attack(targetUnit, false);
			//BWAPI::Broodwar->drawCircleMap(targetUnit->getPosition(), 30, Colors::Brown);
		}
	}
}

Unit TankControl::getTargetUnit(Tank* unit) {
	std::list< pair<int, pair<UnitType, Position>>> nearEnemies;
	std::map<int, float> value;
	nearEnemies.clear();

	for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
		if ((*q).second.first == UnitTypes::Protoss_Arbiter) continue;
		else if ((*q).second.first == UnitTypes::Protoss_Carrier) continue;
		else if ((*q).second.first == UnitTypes::Protoss_Observer) continue;
		else if ((*q).second.first == UnitTypes::Protoss_Interceptor) continue;
		else if ((*q).second.first == UnitTypes::Protoss_Corsair) continue;
		else if ((*q).second.first == UnitTypes::Protoss_Scout) continue;
		else if ((*q).second.first == UnitTypes::Protoss_Shuttle) continue;
		float myMaxRange = unit->getSelf()->getType().groundWeapon().maxRange();
		float myMinRange = unit->getSelf()->getType().groundWeapon().minRange();
		float dist = getDistance(unit->getSelf()->getPosition(), (*q).second.second);
		if (myMinRange < dist && dist < myMaxRange) {
			nearEnemies.push_back(*q);
		}
	}
	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		float myMaxRange = unit->getSelf()->getType().groundWeapon().maxRange();
		float myMinRange = unit->getSelf()->getType().groundWeapon().minRange();
		float dist = getDistance(unit->getSelf()->getPosition(), (*q).second.second);
		if (myMinRange < dist && dist < myMaxRange) {
			nearEnemies.push_back(*q);
		}
	}

	int retInt = -1;
	float maxValue = 0;
	for (auto q = nearEnemies.begin(); q != nearEnemies.end(); q++) {
		if (getUnitforID((*q).first) != NULL) {
			Unit target = getUnitforID((*q).first);
			if (target->isVisible() == false) continue;
			if (unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
				value[(*q).first] = getTargetUnitValueforSiege((*q).second.first);
				value[(*q).first] = getUnitValueinSplash(target);
			}
			else
				value[(*q).first] = getTargetUnitValue((*q).second.first);

			if (oneShotBonus(target, unit)) value[(*q).first] += 10;

			if (target == NULL || target->exists() == false) continue;
			value[(*q).first] += ((((float)(*q).second.first.maxShields() + (*q).second.first.maxHitPoints()) - (target->getHitPoints() + target->getShields())) / (float)((*q).second.first.maxShields() + (*q).second.first.maxHitPoints())) * 5;
			
			if (value[(*q).first] > maxValue) {
				maxValue = value[(*q).first];
				retInt = (*q).first;
			}
		}
		//printf("Value : %0.f\n", value[(*q).first]);
	}
	
	if (retInt == -1) return NULL;
	else {
		return getUnitforID(retInt);
	}
}

float TankControl::getTargetUnitValue(UnitType ut) {
	if (ut.isFlyer())
		return -1;
	if (ut == UnitTypes::Protoss_Zealot)
		return 0.5f;
	else if (ut == UnitTypes::Protoss_Dragoon)
		return 0.7f;
	else if (ut == UnitTypes::Protoss_High_Templar)
		return 0.6f;
	else if (ut == UnitTypes::Protoss_Dark_Templar)
		return 0.3f;
	else if (ut == UnitTypes::Protoss_Probe)
		return 0.7f;
	else if (ut == UnitTypes::Protoss_Reaver)
		return 0.4f;
	else if (ut == UnitTypes::Protoss_Archon)
		return 0.3f;
	else if (ut == UnitTypes::Protoss_Photon_Cannon)
		return 0.5f;
	else {
		return 0.0f;
	}
}

float TankControl::getTargetUnitValueforSiege(UnitType ut) {
	if (ut.isFlyer())
		return -1;
	if (ut == UnitTypes::Protoss_Zealot)
		return 0.3f;
	else if (ut == UnitTypes::Protoss_Dragoon)
		return 0.6f;
	else if (ut == UnitTypes::Protoss_High_Templar)
		return 1.0f;
	else if (ut == UnitTypes::Protoss_Dark_Templar)
		return 0.3f;
	else if (ut == UnitTypes::Protoss_Probe)
		return 0.4f;
	else if (ut == UnitTypes::Protoss_Reaver)
		return 0.8f;
	else if (ut == UnitTypes::Protoss_Archon)
		return 0.4f;
	else if (ut == UnitTypes::Protoss_Photon_Cannon)
		return 0.8f;
	else {
		return 0.0f;
	}
}

float TankControl::getUnitValueinSplash(Unit target) {
	float retValue = 0;
	for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
		float dist = getDistance(target->getPosition(), (*q).second.second);
		if (dist == 0) continue;
		if (dist < 40) {
			retValue += 2;
		}
		if (dist < 25) {
			retValue += 3;
		}
		if (dist < 10) {
			retValue += 5;
		}
	}
	for (auto q = CombatInfo::getInstance()->myUnitsInfo.begin(); q != CombatInfo::getInstance()->myUnitsInfo.end(); q++) {
		float dist = getDistance(target->getPosition(), (*q).second.second);
		if (dist < 40) {
			retValue -= 2;
		}
		if (dist < 25) {
			retValue -= 3;
		}
		if (dist < 10) {
			retValue -= 5;
		}
	}
	return retValue;
}

bool TankControl::oneShotBonus(Unit target, Tank* unit) {
	int HP = target->getHitPoints() + target->getShields();
	int armor = target->getType().armor();
	target->getType().size();
	if (unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		return 0;
	}
	else if (unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		return 0;
	}
	else 
		return 0;
}

Unit TankControl::getUnitforID(int id) {
	for (auto q = BWAPI::Broodwar->enemy()->getUnits().begin(); q != BWAPI::Broodwar->enemy()->getUnits().end(); q++) {
		if ((*q)->exists() && (*q)->getID() == id) {
			return (*q);
		}
	}
	return NULL;
}

void TankControl::drawTankControlUnit(Tank* unit) {
	BWAPI::Broodwar->drawCircleMap(unit->getSelf()->getPosition(), 7, Colors::Brown, true);

}

void TankControl::drawTankControl() {
	/*
	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			if (TankIM[y][x] == 0) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Green, false);
			}
			else if (TankIM[y][x] == -1) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Red, false);
			}
			else if (TankIM[y][x] == -2) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Blue, false);
			}
			else if (TankIM[y][x] >= 10) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Red, false);
			}
			else if (TankIM[y][x] >= 5) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Orange, false);
			}
			else if (TankIM[y][x] >= 1) {
				BWAPI::Broodwar->drawCircleMap(Position(x * 32, y * 32), 3, Colors::Yellow, false);
			}
		}
	}
	*/
}

void TankControl::kitingMove(Tank* unit) {
	if (unit->attackTarget == NULL) {
		if (CombatInfo::getInstance()->enemyStartingBase != NULL)
			unit->getSelf()->move(CombatInfo::getInstance()->enemyStartingBase->getPosition(), false);
	}
	else {
		// 탱쿠 주변으로
		/*for (auto q = UnitControl::getInstance()->tanks.begin(); q != UnitControl::getInstance()->tanks.end(); q++) {
			if (unit->getSelf()->getID() == (*q)->getSelf()->getID()) continue;
			int _x = (*q)->getSelf()->getPosition().x / 32;
			int _y = (*q)->getSelf()->getPosition().y / 32;

			for (int i = -12; i <= 12; i++) {
				for (int j = -12; j <= 12; j++) {
					int x = _x + i;
					int y = _y + j;
					if (x < 0 || y < 0 || x > 127 || y > 127) continue;
					float dist = getDistance(Position(_x, _y), Position(x, y));
					if (dist < 100)
						if (TankIM[y][x] >= 0) {
							TankIM[y][x] -= dist / 32;
							if (TankIM[y][x] < 0) TankIM[y][x] = 0;

						}
				}
			}
		}*/
		// 이동위치 지정 시작
		Position p = unit->getSelf()->getPosition();
		Position targetPos;
		int minValue = 9999;
		for (int x = -5; x <= 5; x++) {
			for (int y = -5; y <= 5; y++) {
				int _x = x + p.x / 32;
				int _y = y + p.y / 32;
				if (_y < 0 || _x < 0 || _x > 127 || _y > 127) continue;
				if (BWAPI::Broodwar->getGroundHeight((TilePosition)Position(_x * 32, _y * 32)) != BWAPI::Broodwar->getGroundHeight((TilePosition)unit->getSelf()->getPosition())) {
					if (TankIM[_y][_x] < minValue && TankIM[_y][_x] >= 0) {
						minValue = TankIM[_y][_x];
					}
				}
				else if (TankIM[_y][_x] < minValue && TankIM[_y][_x] >= 0) {
					minValue = TankIM[_y][_x];
				}
			}
		}
		float minDisttoTarget = 99999;
		int fx = 0;
		int fy = 0;
		for (int x = -5; x <= 5; x++) {
			for (int y = -5; y <= 5; y++) {
				int _x = x + p.x / 32;
				int _y = y + p.y / 32;
				if (_y < 0 || _x < 0 || _x > 127 || _y > 127) continue;
				if (BWAPI::Broodwar->getGroundHeight((TilePosition)Position(_x * 32, _y * 32)) != BWAPI::Broodwar->getGroundHeight((TilePosition)unit->getSelf()->getPosition())) {
					if (TankIM[_y][_x] == minValue) {
						float dist = getDistance(Position(_x * 32, _y * 32), unit->attackTarget->getPosition());
						if (dist < minDisttoTarget) {
							minDisttoTarget = dist;
							targetPos = Position(_x * 32, _y * 32);
							fx = _x;
							fy = _y;
						}
					}
				}
				else if (TankIM[_y][_x] == minValue) {
					float dist = getDistance(Position(_x * 32, _y * 32), unit->attackTarget->getPosition());
					if (dist < minDisttoTarget) {
						minDisttoTarget = dist;
						targetPos = Position(_x * 32, _y * 32);
						fx = _x;
						fy = _y;
					}
				}
			}
		}
		unit->getSelf()->move(targetPos, false);
		TankIM[fy][fx] = -3;
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				int xxxx = fx + i;
				int yyyy = fy + j;
				if (xxxx < 0 || yyyy < 0 || xxxx> 127 || yyyy > 127) continue;
				TankIM[yyyy][xxxx] += 20;
			}
		}

	}

}

void TankControl::checkSiege(Tank* unit) {
	if (!Broodwar->self()->hasResearched(TechTypes::Tank_Siege_Mode)) return;
	BWAPI::Unitset enemyUnits = BWAPI::Broodwar->enemy()->getUnits();
	for (auto q = enemyUnits.begin(); q != enemyUnits.end(); q++) {
		if ((*q)->getType().isBuilding()) continue;
		float dist = getDistance(unit->getSelf()->getPosition(), (*q)->getPosition());
		if ((*q)->getType() == UnitTypes::Protoss_Zealot) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Dark_Templar) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Arbiter) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Carrier) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Interceptor) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Corsair) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Observer) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Scout) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Shuttle) continue;
		else if (64.0 < dist && dist < 390.0) {
			unit->isSiege = true;
			break;
		}
		else // 공격범위안에 아무것도 없고 최소공격범위보다 가까이있을때 퉁퉁포로
			unit->isSiege = false;
	}
	bool checkUnit = false;
	if (unit->isSiege == true) checkUnit = true;
	Position posPhoton;
	bool checkPhoton = false;
	for (auto q = enemyUnits.begin(); q != enemyUnits.end(); q++) {
		if (!(*q)->getType().isBuilding()) continue;
		float dist = getDistance(unit->getSelf()->getPosition(), (*q)->getPosition());
		if (dist < 384 && (*q)->getType() == UnitTypes::Protoss_Photon_Cannon) {
			unit->isSiege = true;
			break;
		}
		else if (dist < 530 && (*q)->getType() == UnitTypes::Protoss_Photon_Cannon) {
			unit->isSiege = false;
			checkPhoton = true;
			posPhoton = (*q)->getPosition();
			break;
		}
		else if (dist < 384) {
			unit->isSiege = true;
		}
	}

	// 초크포인트 확인
	bool checkChoke = false;
	Chokepoint* cp = BWTA::getNearestChokepoint(unit->getSelf()->getPosition());
	if (cp->getWidth() < 80.0 && getDistance(unit->getSelf()->getPosition(), cp->getCenter()) < 95.0) {
		checkChoke = true;
	}
	else checkChoke = false;
	if (checkChoke == true) unit->isSiege = false;

	/*if (!checkChoke && unit->isSiege && (unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode)) {
		unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
	}
	if (!unit->isSiege && (unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
		unit->getSelf()->useTech(TechTypes::Tank_Siege_Mode);
	}*/
	// 포토 먼저 때리기
	//bw->drawCircleMap(unit->getSelf()->getPosition(), 530, Colors::Teal);
	if (!checkUnit && checkPhoton) unit->getSelf()->move(posPhoton);

	// 주변에 적 있는지 확인
	bool last_check = true;
	for (auto q = enemyUnits.begin(); q != enemyUnits.end(); q++) {
		float dist = getDistance(unit->getSelf()->getPosition(), (*q)->getPosition());
		if ((*q)->getType() == UnitTypes::Protoss_Zealot) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Dark_Templar) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Probe) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Arbiter) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Carrier) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Corsair) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Interceptor) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Observer) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Scout) continue;
		else if ((*q)->getType() == UnitTypes::Protoss_Shuttle) continue;
		else if (dist < 384.0) { 
			last_check = false;
			break;
		}
	}
	if (last_check && unit->isSiege && (unit->getSelf()->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
		unit->isSiege = false;
	}
	//printf("%d : checkSiege\n", unit->isSiege);
}

void TankControl::updateIM() {
	// Walkable check
	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			if (BWAPI::Broodwar->isWalkable((WalkPosition)(Position(32 * x, 32 * y)))) {
				TankIM[y][x] = 0;
			}
			else {
				TankIM[y][x] = -1;
			}
		}
	}

	// 상대 유닛
	for (auto q = CombatInfo::getInstance()->enemyUnitsInfo.begin(); q != CombatInfo::getInstance()->enemyUnitsInfo.end(); q++) {
		Position p = (*q).second.second;
		int _x = p.x / 32;
		int _y = p.y / 32;
		float eRange = (*q).second.first.groundWeapon().maxRange() + 140;
		if ((*q).second.first == UnitTypes::Protoss_Probe) eRange = (*q).second.first.groundWeapon().maxRange() * 2;
		for (int i = -eRange / 32; i <= eRange / 32; i++) {
			for (int j = -eRange / 32; j <= eRange / 32; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx > 127 || _yy > 127) {
					continue;
				}
				float dist = getDistance(Position(_xx * 32, _yy * 32), p);
				if (dist < eRange) TankIM[_yy][_xx] += (eRange - dist) / eRange * 100;
			}
		}
	}

	// 상대 건물
	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		if ((*q).second.first != UnitTypes::Protoss_Photon_Cannon) continue;
		Position p = (*q).second.second;
		int _x = p.x / 32;
		int _y = p.y / 32;
		float eRange = (*q).second.first.groundWeapon().maxRange() + 96;
		for (int i = -eRange / 32; i <= eRange / 32; i++) {
			for (int j = -eRange / 32; j <= eRange / 32; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx > 127 || _yy > 127) {
					continue;
				}
				float dist = getDistance(Position(_xx * 32, _yy * 32), p);
				if (dist < eRange) TankIM[_yy][_xx] += (eRange - dist) / eRange * 100;
			}
		}
	}

	for (auto q = CombatInfo::getInstance()->enemyBuildingsInfo.begin(); q != CombatInfo::getInstance()->enemyBuildingsInfo.end(); q++) {
		Position p = (*q).second.second;
		int _x = p.x / 32;
		int _y = p.y / 32;
		if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
		TankIM[_y][_x] = -2;
		for (int i = -((*q).second.first.width() / 2) / 32 - 1; i <= ((*q).second.first.width() / 2) / 32 + 1; i++) {
			for (int j = -((*q).second.first.height() / 2) / 32; j <= ((*q).second.first.height() / 2) / 32 + 1; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx >127 || _yy > 127) {
					continue;
				}
				TankIM[_yy][_xx] = -2;
			}
		}
	}

	// 내 유닛
	/*for (auto q = CombatInfo::getInstance()->myUnitsInfo.begin(); q != CombatInfo::getInstance()->myUnitsInfo.end(); q++) {
		Position p = (*q).second.second;
		int _x = p.x / 32;
		int _y = p.y / 32;
		if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
		TankIM[_y][_x] = -3;
	}*/

	// 내 건물
	for (auto q = CombatInfo::getInstance()->myBuildingsInfo.begin(); q != CombatInfo::getInstance()->myBuildingsInfo.end(); q++) {
		Position p = (*q).second.second;
		int _x = p.x / 32;
		int _y = p.y / 32;
		if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
		TankIM[_y][_x] = -3;
		for (int i = -((*q).second.first.width() / 2) / 32 - 1; i <= ((*q).second.first.width() / 2) / 32 + 1; i++) {
			for (int j = -((*q).second.first.height() / 2) / 32; j <= ((*q).second.first.height() / 2) / 32 + 1; j++) {
				int _xx = _x + i;
				int _yy = _y + j;
				if (_xx < 0 || _yy < 0 || _xx >127 || _yy > 127) {
					continue;
				}
				TankIM[_yy][_xx] = -3;
			}
		}
	}

	// 자원
	if (CombatInfo::getInstance()->enemyStartingBase != NULL) {
		for (auto q = BWAPI::Broodwar->getMinerals().begin(); q != BWAPI::Broodwar->getMinerals().end(); q++) {
			Position p = (*q)->getPosition();
			int _x = p.x / 32;
			int _y = p.y / 32;
			if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
			TankIM[_y][_x] = -2;
			for (int i = -(UnitTypes::Resource_Mineral_Field.width() / 2) / 32; i <= (UnitTypes::Resource_Mineral_Field.width() / 2) / 32; i++) {
				for (int j = -((*q)->getType().height() / 2) / 32; j <= ((*q)->getType().height() / 2) / 32 + 1; j++) {
					int _xx = _x + i;
					int _yy = _y + j;
					if (_xx < 0 || _yy < 0 || _xx >127 || _yy > 127) {
						continue;
					}
					TankIM[_yy][_xx] = -1;
				}
			}
		}

		for (auto q = BWAPI::Broodwar->getGeysers().begin(); q != BWAPI::Broodwar->getGeysers().end(); q++) {
			Position p = (*q)->getPosition();
			int _x = p.x / 32;
			int _y = p.y / 32;
			if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
			TankIM[_y][_x] = -2;
			for (int i = -((*q)->getType().width() / 2) / 32; i <= ((*q)->getType().width() / 2) / 32; i++) {
				for (int j = -((*q)->getType().height() / 2) / 32; j <= ((*q)->getType().height() / 2) / 32; j++) {
					int _xx = _x + i;
					int _yy = _y + j;
					if (_xx < 0 || _yy < 0 || _xx >127 || _yy > 127) {
						continue;
					}
					TankIM[_yy][_xx] = -1;
				}
			}
		}
	}

	// 주변 가산
	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			bool checkWalk = false;
			bool checkBuilding = false;
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					int _x = x + i;
					int _y = y + j;
					if (_x < 0 || _y < 0 || _x > 127 || _y > 127) continue;
					if (TankIM[_y][_x] == -1) {
						checkWalk = true;
					}
					if (TankIM[_y][_x] == -2) {
						checkBuilding = true;
					}
				}
			}
			if (checkWalk && TankIM[y][x] >= 0) {
				TankIM[y][x] = TankIM[y][x] + 20;
			}
			if (checkBuilding && TankIM[y][x] >= 0) {
				TankIM[y][x] = TankIM[y][x] + 20;
			}
			if (x == 0 || y == 0 || x == 127 || y == 127) TankIM[y][x] = TankIM[y][x] + 20;
		}
	}

	// 마린 주변으로
	for (auto q = UnitControl::getInstance()->marines.begin(); q != UnitControl::getInstance()->marines.end(); q++) {
		int _x = (*q)->getSelf()->getPosition().x / 32;
		int _y = (*q)->getSelf()->getPosition().y / 32;
		
		for (int i = -12; i <= 12; i++) {
			for (int j = -12; j <= 12; j++) {
				int x = _x + i;
				int y = _y + j;
				if (x < 0 || y < 0 || x > 127 || y > 127) continue;
				float dist = getDistance(Position(_x, _y), Position(x, y));
				if (dist < 100)
					if (TankIM[y][x] >= 0) {
						TankIM[y][x] -= dist / 32;
						if (TankIM[y][x] < 0) TankIM[y][x] = 0;

					}
			}
		}
	}

}