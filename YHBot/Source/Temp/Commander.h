#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include <time.h>
#include <Windows.h>
#include <WinBase.h>
#include <list>
#include "TerritoryMgr.h"
#include "BuildMgr.h"
#include "StrategyMgr.h"
#include "CombatInfo.h"
#include "UnitControl.h"
#include "Scout.h"

#include "Work.h"

using namespace std;
using namespace BWAPI;
using namespace Filter;

class Commander
{
private:

	int delayMineral;
	int delayGas;

	int scvCount;

	Resource resource;

	list<BasicWork*> schedule;
	TerritoryMgr territoryMgr;

	StateMgr state;
	BuildMgr* builder;
	StrategyMgr* strategist;
	CombatInfo* combatInfo;
	UnitControl* unitControl;
	Scouting* scouting;

	Commander();
	~Commander();

public:
	static Commander* getInstance();

	// OnFunc
	void init();
	void start();
	void show();
	void run();

	void structureCreate(Unit unit);
	void structureComplete(Unit unit);
	void unitCreate(Unit unit);
	void unitComplete(Unit unit);

	// Schedule Func
	int getUnitScheduleCount(UnitType target);
	bool addUnitSchedule(UnitType target, int count = 1, int hurry = 0);
	bool addUnitSchedule(UnitWork& work, int count = 1, int hurry = 0);
	bool addStructureSchedule(UnitType target, TilePosition pos, int count = 1, int hurry = 1);
	bool addStructureSchedule(scheduleWork& work, int count = 1, int hurry = 1);

	void runSchedule();
	void runScheduleEX();

	bool runUnitSchedule(BasicWork* work, list<BasicWork*>::iterator& iter);
	bool runBuildingSchedule(BasicWork* work, list<BasicWork*>::iterator& iter);
	bool runAddonSchedule(BasicWork* work, list<BasicWork*>::iterator& iter);

	// Build Func
	bool enoughResource(UnitType target, int readyMineral = 0);
	bool enoughWhatBuild(UnitType target);
	bool canCrate(UnitType target);
	bool getReadyTrain(Unit unit);

	int getMineral();
	int getGas();
	int getSupply();

	Unit getProperBuilding(UnitType target);
	StateMgr& getStateMgr(){ return state; };
	TerritoryMgr& getTerritoryMgr(){ return territoryMgr; };

	void addScvCount(){ scvCount++; };
	int getScvCount(){ return scvCount; };

	LARGE_INTEGER frequency, starti,
		meClock, statClock, builClock, straClock, terrClock, scheClock, cbifClock, contClock, soutClock;

	void drawSignal();
};

