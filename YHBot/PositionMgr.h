#pragma once
#include "BWAPI.h"

class PosMgr{

	BWAPI::TilePosition splyPos;
	BWAPI::TilePosition facPos;

public:
	void init();
	BWAPI::TilePosition getSplyPos(bool add);
};