#include "UtilMgr.h"



UtilMgr::UtilMgr()
{
}


UtilMgr::~UtilMgr()
{
}

UtilMgr* UtilMgr::getInstance(){
	static UtilMgr instance;
	return &instance;
}

void UtilMgr::drawTerrainData(){

	//we will iterate through all the base locations, and draw their outlines.
	for (const auto& baseLocation : BWTA::getBaseLocations()) {
		TilePosition p = baseLocation->getTilePosition();

		//draw outline of center location
		Position leftTop(p.x * TILE_SIZE, p.y * TILE_SIZE);
		Position rightBottom(leftTop.x + 4 * TILE_SIZE, leftTop.y + 3 * TILE_SIZE);
		Broodwar->drawBoxMap(leftTop, rightBottom, Colors::Blue);

		//draw a circle at each mineral patch
		for (const auto& mineral : baseLocation->getStaticMinerals()) {
			//Broodwar->drawCircleMap(mineral->getInitialPosition(), 30, Colors::Cyan);
			//Broodwar->drawBoxMap(leftTop, rightBottom, Colors::Blue);
			Position left(mineral->getPosition().x - TILE_SIZE, mineral->getPosition().y - TILE_SIZE / 2);
			Position right(left.x + 2 * TILE_SIZE, left.y + TILE_SIZE);
			Broodwar->drawBoxMap(left, right, Colors::Cyan);
		}

		//draw the outlines of Vespene geysers
		for (const auto& geyser : baseLocation->getGeysers()) {
			TilePosition p1 = geyser->getInitialTilePosition();
			Position leftTop1(p1.x * TILE_SIZE, p1.y * TILE_SIZE);
			Position rightBottom1(leftTop1.x + 4 * TILE_SIZE, leftTop1.y + 2 * TILE_SIZE);
			Broodwar->drawBoxMap(leftTop1, rightBottom1, Colors::Orange);
		}

		//if this is an island expansion, draw a yellow circle around the base location
		if (baseLocation->isIsland()) {
			Broodwar->drawCircleMap(baseLocation->getPosition(), 80, Colors::Yellow);
		}
	}

	//we will iterate through all the regions and ...
	for (const auto& region : BWTA::getRegions()) {
		// draw the polygon outline of it in green
		BWTA::Polygon p = region->getPolygon();
		for (size_t j = 0; j < p.size(); ++j) {
			Position point1 = p[j];
			Position point2 = p[(j + 1) % p.size()];
			Broodwar->drawLineMap(point1, point2, Colors::Green);
		}
		// visualize the chokepoints with red lines
		for (auto const& chokepoint : region->getChokepoints()) {
			Position point1 = chokepoint->getSides().first;
			Position point2 = chokepoint->getSides().second;
			Broodwar->drawLineMap(point1, point2, Colors::Red);
		}
	}
}

Position UtilMgr::tile2pos(TilePosition tile){
	return Position(tile.x * TILEPOSITION_SCALE, tile.y * TILEPOSITION_SCALE);
}

TilePosition UtilMgr::pos2tile(Position pos){
	return TilePosition(pos.x / TILEPOSITION_SCALE, pos.y / TILEPOSITION_SCALE);
}

float UtilMgr::getDistance(TilePosition cmp1, TilePosition cmp2){
	return getDistance(tile2pos(cmp1), tile2pos(cmp2));
}	

float UtilMgr::getDistance(BWTA::BaseLocation* cmp1, BWTA::BaseLocation* cmp2){
	return getDistance(cmp1->getPosition(), cmp2->getPosition());
}

float UtilMgr::getDistance(Position cmp1, Position cmp2){
	return sqrt((float)pow((cmp1.x - cmp2.x), 2) + (float)pow((cmp1.y - cmp2.y), 2));
}

bool UtilMgr::isAlmostReachedDest(Position dest, Position comparator, int xTerm, int yTerm){

	if (dest.x - (xTerm / 2) <= comparator.x && comparator.x <= dest.x + (xTerm / 2) &&
		dest.y - (yTerm / 2) <= comparator.y && comparator.y <= dest.y + (yTerm / 2))
	{
		return true;
	}

	return false;
}