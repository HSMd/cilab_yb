#pragma once

#include <BWAPI.h>
#include <queue>

using namespace std;
using namespace BWAPI;

class Work{
	UnitType targetType;
	TilePosition targetPos;
	int count;

	// 
	UnitType obj;
	int cmpCount;


public:
	Work(UnitType type, TilePosition pos, UnitType _obj, int _cmpCount, int _count) : targetType(type), targetPos(pos), obj(_obj), count(_count), cmpCount(_cmpCount) {};
	~Work(){};

	UnitType getTargetType(){ return targetType; };
	TilePosition getTargetPosition(){ return targetPos; };
	int getCount() { return count; };
	int getCmpCount() { return cmpCount; };
	bool isFire(); 
};

class StartBuild
{
	queue<Work> build;

public:
	StartBuild();
	~StartBuild();

	void run();
};

