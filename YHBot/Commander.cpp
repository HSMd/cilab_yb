#include "Commander.h"

auto& bw = Broodwar;

SCV* scvTest = nullptr;

Commander::Commander() : me(me::getInstance()), util(UtilMgr::getInstance()), scvMgr(SCVMgr::getInstance())
{
	delayMineral = 0;
	delayGas = 0;
	delayFrame = -1;
}

Commander::~Commander()
{
	BWTA::cleanMemory();
}

Commander* Commander::getInstance(){
	static Commander instance;
	return &instance;
}

void Commander::init()
{
	// Setting

	// Hello World!
	bw->sendText("Hello I'm meBot Beta 1.0 ver");
	bw << "The map is " << bw->mapName() << "!" << std::endl;


	bw->enableFlag(Flag::UserInput);

	// /speed 0 - 10
	bw->setLocalSpeed(10);
	bw->setCommandOptimizationLevel(0);

	BWTA::readMap();
	BWTA::analyze();

	MapInfo::getInstance()->init();
}

void Commander::start(){

	
}

void Commander::show(){

	// draw
	util->drawTerrainData();

	// draw Text
	bw->drawTextScreen(200, 40, "Command : %d", me->getBuildings(UnitTypes::Terran_Command_Center).size());
	bw->drawTextScreen(200, 60, "scv : %d", me->getUnitImmediatelyUnit(UnitTypes::Terran_SCV).size());
	bw->drawTextScreen(200, 80, "unitSchedule : %d", unitSchedule.size());
	//bw->drawTextScreen(200, 100, "structSchedule : %d", structSchedule.size());
	//bw->drawTextScreen(200, 120, "DelayMoney : %d", delayMineral);
	//bw->drawTextScreen(200, 100, "MineralSCV : %d", scvMgr->getMineralScv().size());

	if (scvTest != nullptr){
		bw->drawTextScreen(200, 100, "STATE : %d", scvTest->getState());
	}
}

void Commander::run()
{
	scvMgr->run();
	test.run();

	runSchedule();

	/*
	// update

	// Builder
	test.run();
	//test2.run();

	// behavior
	scvMgr->run();	

	// product
	if (delayFrame == 0){
		delayMineral -= UnitTypes::Terran_Refinery.mineralPrice();
		delayFrame = -1;
	}
	else if (0 < delayFrame){
		delayFrame -= 1;
	}

	runSchedule();
	*/
}


 // Create & Complete //
void Commander::structureCreate(Unit unit)
{
	me->addBuildingImmediately(unit);

	if (50 < bw->getFrameCount()){
		delayMineral -= unit->getType().mineralPrice();
		delayGas -= unit->getType().gasPrice();
	}
}

void Commander::structureComplete(Unit unit)
{
	me->addBuilding(unit);
	queue<structWork*> delSchedule;

	for (auto iter = progressSchedule.begin(); iter != progressSchedule.end(); ++iter){
		if ((*iter)->getTilePosition() == unit->getTilePosition()){
			//(*iter)->getBuilder()->setState(SCV::STATE::IDLE);
			//(*iter)->getBuilder()->reOrderMineral();
			Broodwar << "END!!!!!!!!!!!!!!!" << endl;
			//delSchedule.push((*iter));

			//progressSchedule.erase(iter);
		}
	}

	while (!delSchedule.empty()){
		structWork* work = delSchedule.front();

		//delete work;
		delSchedule.pop();
	}
}

void Commander::unitCreate(Unit unit)
{
	me->addUnitImmediately(unit);
}

void Commander::unitComplete(Unit unit)
{
	me->addUnit(unit);
}


// Build Func //
bool Commander::build(Unit builder, UnitType target, TilePosition pos)
{
	bool result = builder->build(target, pos);

	return result;
}

bool Commander::autoBuild(UnitType target, int center)
{
	/*
	Unit cmd = me->getBuildings(UnitTypes::Terran_Command_Center)[center];
	Unit builder = cmd->getClosestUnit(GetType == target.whatBuilds().first && (IsIdle || IsGatheringMinerals) && IsOwned);

	if (builder){
		TilePosition targetBuildLocation = bw->getBuildLocation(target, builder->getTilePosition());
		if (targetBuildLocation){
			return build(builder, target, targetBuildLocation);
		}
		else{
			bw << "not Found targetBuildLocation" << endl;
			return false;
		}
	}
	else{
		bw << "not Found builder" << endl;
		return false;
	}
	*/
	return true;
}

bool Commander::canCreate(UnitType target){
	int restSupply = me->player()->supplyTotal() - me->player()->supplyUsed();

	if ((me->player()->minerals() - delayMineral) < target.mineralPrice() ||
		(me->player()->gas() - delayGas) < target.gasPrice() ||
		restSupply < target.supplyRequired()){
		return false;
	}

	if (target == UnitTypes::Terran_Command_Center ||
		target == UnitTypes::Terran_Supply_Depot ||
		target == UnitTypes::Terran_Barracks ||
		target == UnitTypes::Terran_Refinery ||
		target == UnitTypes::Terran_Engineering_Bay){
		return true;
	}
	else if (me->getBuildings(target.whatBuilds().first).size() < 1){
		return false;
	}

	return true;
}


// 
int Commander::getUnitScheduleCount(UnitType target){
	int count = 0;
	for (UnitType type : unitSchedule){
		if (type == target)
			count++;
	}

	return count;
}

void Commander::addUnitSchedule(UnitType target, int count, bool hurry){
	
	if (hurry){
		for (int i = 0; i < count; i++)
			unitSchedule.push_front(target);
	}
	else{
		for (int i = 0; i < count; i++)
			unitSchedule.push_back(target);
	}
}

void Commander::addStructureSchedule(UnitType target, TilePosition pos, bool retry, int count, bool hurry){
	
	if (hurry){
		for (int i = 0; i < count; i++)
			structSchedule.push_front(new structWork(target, pos, bw->getFrameCount(), retry));
	}
	else{
		for (int i = 0; i < count; i++)
			structSchedule.push_back(new structWork(target, pos, bw->getFrameCount(), retry));
	}
}

void Commander::runSchedule(){

	while (!structSchedule.empty()){
		structWork* work = structSchedule.front();
		UnitType target = work->getTarget();

		if (!canCreate(target)){
			break;
		}

		if (target == UnitTypes::Terran_Refinery){
			delayFrame += 50;
		}

		SCV* builder = scvMgr->getWorker();
		scvTest = builder;
		if (work->getTilePosition().x == 0 && work->getTilePosition().y == 0){
			TilePosition targetBuildLocation = Broodwar->getBuildLocation(work->getTarget(), builder->getSelf()->getTilePosition());
			work->setTilePosition(targetBuildLocation);
			builder->orderBuild(work->getTarget(), targetBuildLocation);
		}
		else{
			builder->orderBuild(work->getTarget(), work->getTilePosition());
		}
		

		delayMineral += target.mineralPrice();
		delayGas += target.gasPrice();

		work->setBuilder(builder);

		/*
		if (work->getTilePosition().x == 0 && work->getTilePosition().y == 0){
			TilePosition targetBuildLocation = bw->getBuildLocation(target, builder->getTilePosition());
			builder->move(util->tile2pos(targetBuildLocation));
			work->setTilePosition(targetBuildLocation);
		}
		else{
			builder->move(util->tile2pos(work->getTilePosition()));
		}

		work->setBuilder(builder);
		progressSchedule.push_back(work);		

		//autoBuild(UnitTypes::Terran_Supply_Depot);
		*/

		progressSchedule.push_back(work);
		structSchedule.pop_front();
	}

	while (!unitSchedule.empty() && structSchedule.empty()){
		UnitType target = unitSchedule.front();

		if (!canCreate(target)){
			break;
		}

		int choiced = 0;
		me->getBuildings(target.whatBuilds().first)[choiced]->getSelf()->train(target);
		unitSchedule.pop_front();
	}
}

