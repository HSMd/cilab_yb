#pragma once

#include <BWAPI.h>
#include <BWTA.h>

#include <memory>
#include <vector>

#include "MyUnit.h"

using namespace std;

class me
{
	//MapInfo* mapInfo;

	map<int, vector <MyUnit*>> buildings;
	map<int, vector <MyUnit*>> units;

	map<int, vector <MyUnit*>> createUnits;
	map<int, vector <MyUnit*>> createBuildings;

	//vector<BWTA::BaseLocation*> myTerritory;
	
	me();
	me(const me& other);
	~me();	

public:
	BWAPI::Player player();

	static me* getInstance();

	void init();

	void addUnit(BWAPI::Unit unit);
	void addUnitImmediately(BWAPI::Unit unit);
	void addBuildingImmediately(BWAPI::Unit building);
	void addBuilding(BWAPI::Unit building);
	void addTerritory(BWTA::BaseLocation* base);

	vector<MyUnit*>& getUnits(int type);
	vector<MyUnit*>& getUnitImmediatelyUnit(int type);

	vector<MyUnit*>& getBuildings(int type);
	vector<MyUnit*>& getUnitImmediatelyBuilding(int type);

	//BWTA::BaseLocation* getMyStartLocation();
	//vector<BWTA::BaseLocation*>& getMyTerritory();
	
};

