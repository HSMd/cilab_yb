#include "StartBuild.h"
#include "Commander.h"
#include "BWAPI.h"

bool Work::isFire(){
	me* me = me::getInstance();

	return me->getUnits(obj).size() == cmpCount;
}

////////////////

StartBuild::StartBuild()
{
	build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(0, 0), UnitTypes::Terran_SCV, 8, 1));
	//build.push(Work(UnitTypes::Terran_Refinery, TilePosition(0, 0), UnitTypes::Terran_SCV, 12, 1));
	//build.push(Work(UnitTypes::Terran_Barracks, TilePosition(0, 0), UnitTypes::Terran_SCV, 13, 1));
	//build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(0, 0), UnitTypes::Terran_SCV, 15, 1));
}


StartBuild::~StartBuild()
{
}

void StartBuild::run(){

	Commander* cmd = Commander::getInstance();
	MapInfo* mapInfo = MapInfo::getInstance();
	me* me = me::getInstance();
		
	while (!build.empty()){
		auto& b = build.front();

		if (b.isFire()){
			cmd->addStructureSchedule(b.getTargetType(), b.getTargetPosition(), false, b.getCount());
			build.pop();
		}
		else{
			break;
		}
	}
	

	/*
	if (me->getUnits(UnitTypes::Terran_SCV).size() == 8 && !flag){
		cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(0, 0), false, 1);
		flag = true;
	}*/

	/*
	if (me->getUnits(UnitTypes::Terran_SCV).size() == 12 && me->getUnitImmediatelyBuilding(UnitTypes::Terran_Refinery).size() < 1){
		cmd->addStructureSchedule(UnitTypes::Terran_Refinery, TilePosition(0, 0), false, 1);
	}

	if (me->getUnits(UnitTypes::Terran_SCV).size() == 13 && me->getUnitImmediatelyBuilding(UnitTypes::Terran_Barracks).size() < 1){
		cmd->addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(0, 0), false, 1);
	}

	if (me->getUnits(UnitTypes::Terran_SCV).size() == 15 && me->getUnitImmediatelyBuilding(UnitTypes::Terran_Supply_Depot).size() < 1){
		cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(0, 0), false, 1);
	}
	*/

	/*
	switch (mapInfo->getDir()){

	case 0:
		cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(97, 5), false);
		cmd->addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(102, 9), false);
		cmd->addStructureSchedule(UnitTypes::Terran_Refinery, TilePosition(0, 0), false);

		break;

	case 1:
		cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(120, 97), false);
		cmd->addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(114, 101), false);
		cmd->addStructureSchedule(UnitTypes::Terran_Refinery, TilePosition(0, 0), false);

		break;

	case 2:
		cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(11, 26), false);
		cmd->addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(4, 28), false);
		cmd->addStructureSchedule(UnitTypes::Terran_Refinery, TilePosition(0, 0), false);

		break;

	case 3:
		cmd->addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(23, 118), false);
		cmd->addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(24, 120), false);
		cmd->addStructureSchedule(UnitTypes::Terran_Refinery, TilePosition(0, 0), false);

		break;

	default:
		Broodwar << "Map Info Dir ERROR!!\n" << endl;
		break;
	}
	*/
}