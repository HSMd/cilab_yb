#pragma once

#include <BWAPI.h>
#include <BWTA.h>
#include <math.h>

using namespace BWAPI;

class UtilMgr
{
public:

	UtilMgr();
	~UtilMgr();

public:
	static UtilMgr* getInstance();

	Position tile2pos(TilePosition tile);
	TilePosition pos2tile(Position pos);
	void drawTerrainData();
	float getDistance(TilePosition cmp1, TilePosition cmp2);
	float getDistance(Position cmp1, Position cmp2);
	float getDistance(BWTA::BaseLocation* cmp1, BWTA::BaseLocation* cmp2);
	bool isAlmostReachedDest(Position dest, Position comparator, int xTerm, int yTerm);
	int max(int a, int b);
};

