#pragma once
#include <BWAPI.h>

using namespace BWAPI;

class MyUnit
{
protected:
	Unit self;

public:
	MyUnit(Unit _self);
	~MyUnit();

	virtual void run();
	Unit getSelf(){ return self; };
};

class Structure : public MyUnit{

public:
	Structure(Unit _self) : MyUnit(_self) {};
	~Structure() {};

	void run(){
		// Nothing
	}
};

class SCV : public MyUnit{
public:
	enum STATE{
		IDLE,
		MINERAL,
		GAS,
		SPY,
		SPY_WAIT,
		REPAIR,
		ATTACK,
		BUILD		
	};

private:
	STATE state;
	Unit target;
	UnitType targetType;
	TilePosition targetPos;

public:
	SCV(Unit _self);
	~SCV();

	void run();
	void reOrderMineral() { state = STATE::MINERAL; };
	void orderMineral(Unit target){ this->target = target; state = STATE::MINERAL; }
	void orderGas(Unit target){ this->target = target; state = STATE::GAS; }
	void orderBuild(UnitType type, TilePosition pos){ this->targetType = type, this->targetPos = pos; state = STATE::BUILD; }
	void orderSpy(TilePosition pos){ this->targetPos = pos; state = STATE::SPY; }
	void orderSpyWait(){ state = STATE::SPY_WAIT; }

	void setState(STATE state) { this->state = state; }
	STATE getState() { return state; }
	
};