#pragma once

#include <map>

using namespace std;

enum BUILD_FLAG{
	SIM_CITY_1,
	SIM_CITY_2,
	FACTORY_DOUBLE,
	EXPAND_FORECOUNT,
};

enum COMBAT_FLAG{
	NORMAL,
	INVASION_PYLON,
	GUARD_SIM_CITY_2,
	ATTACK_TWO_FACTORY,
};

enum OPP_FLAG{
	FAST_FORECOUNT,
};

class StateMgr
{
	map<BUILD_FLAG, bool> buildFlag;
	map<COMBAT_FLAG, bool> combatFlag;
	map<OPP_FLAG, bool> oppFlag;

	BUILD_FLAG build;
	COMBAT_FLAG combat;

public:
	StateMgr();
	~StateMgr();

	void run();
	void chkCombatState();
	bool isSafe();

	BUILD_FLAG getBuild() { return build; }
	COMBAT_FLAG getCombet() { return combat; }

	// On Off Flags
	void setBuildFlag(BUILD_FLAG flag, bool fire) { buildFlag[flag] = fire; };
	void setCombatFlag(COMBAT_FLAG flag, bool fire) { combatFlag[flag] = fire; };
	void setOppFlag(OPP_FLAG flag, bool fire) { oppFlag[flag] = fire; };	
};

