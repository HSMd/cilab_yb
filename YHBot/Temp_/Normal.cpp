#include "Normal.h"
#include "../single.h"
#include "UtilMgr.h"

Normal::Normal(){
	switch (mapInfo->getDir()){
	case 0:
		HoldMarine[TilePosition(100, 6)] = nullptr;
		HoldMarine[TilePosition(103, 12)] = nullptr;
		HoldMarine[TilePosition(103, 8)] = nullptr;
		HoldMarine[TilePosition(101, 6)] = nullptr;
		HoldMarine[TilePosition(96, 5)] = nullptr;
		break;
	case 1:
		HoldMarine[TilePosition(118, 102)] = nullptr;
		HoldMarine[TilePosition(119, 101)] = nullptr;
		HoldMarine[TilePosition(119, 102)] = nullptr;
		HoldMarine[TilePosition(121, 99)] = nullptr;
		HoldMarine[TilePosition(114, 101)] = nullptr;
		break;
	case 2:
		HoldMarine[TilePosition(7, 26)] = nullptr;
		HoldMarine[TilePosition(6, 27)] = nullptr;
		HoldMarine[TilePosition(10, 25)] = nullptr;
		HoldMarine[TilePosition(11, 25)] = nullptr;
		HoldMarine[TilePosition(14, 26)] = nullptr;
		break;
	case 3:
		HoldMarine[TilePosition(23, 117)] = nullptr;
		HoldMarine[TilePosition(23, 120)] = nullptr;
		HoldMarine[TilePosition(23, 121)] = nullptr;
		HoldMarine[TilePosition(27, 123)] = nullptr;
		HoldMarine[TilePosition(28, 123)] = nullptr;
		break;
	}
}

Normal::~Normal(){}

void Normal::run(BUILD_FLAG build, COMBAT_FLAG combat){
	Solider* t_marine = getIdleMarine();
	if (t_marine != nullptr){
		Marine.push_back(t_marine);
	}
	
	if (build == BUILD_FLAG::SIM_CITY_1 && combat == COMBAT_FLAG::NORMAL){
		for (vector<Solider*>::iterator iter = Marine.begin(); iter != Marine.end();){
			Solider* temp = (*iter);
			if (temp->getSelf()->exists()){
				temp->orderHold();
				MarineControl(temp);
				iter++;
			}
			else{
				iter = Marine.erase(iter);
			}
		}
	}
}

Solider* Normal::getIdleMarine(){
	for (auto marine : me->getUnits(UnitTypes::Terran_Marine)){
		auto m = dynamic_cast<Solider *>(marine);
		if (m->getState() == Solider::STATE::IDLE){
			if (m->getSelf()->exists()){
				m->orderWait();
				return m;
			}
		}
	}
	return nullptr;
}
void Normal::HoldMarinePosition(){
	if (HoldMarine[] != nullptr){

	}
}
void Normal::MarineControl(Solider* marine){
	if (!marine->getSelf()->exists())
	{
		return;
	}
	Unit t_marine = marine->getSelf();

	if (marine->getState() == Solider::STATE::HOLD){
	/*	for (auto iter = HoldMarine.begin(); iter != HoldMarine.end();){
			if (t_marine->getDistance(tile2pos(HoldMarine[marine])) > 0){
				t_marine->move(tile2pos(HoldMarine[marine]));
			}
			else if (t_marine->getDistance(tile2pos(HoldMarine[marine])) == 0){
				t_marine->stop();
			}
		}*/
	}
}
