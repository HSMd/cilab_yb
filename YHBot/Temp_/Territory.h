#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include <map>
#include <queue>

#include "MyUnit.h"

using namespace BWAPI;
using namespace BWTA;
using namespace std;

class Territory
{
	MyUnit* cmdCenter;
	int scvCount;

	Unitset minerals;
	Unit gas;

	TilePosition gasPos;

	map<Unit, int> resourceCount;

	vector<SCV*> mineralSCV;
	vector<SCV*> gasSCV;
	vector<SCV*> workSCV;
	
public:
	Territory(BaseLocation* base, MyUnit* cmdCenter);
	Territory();
	~Territory();

	void init(BaseLocation* base, MyUnit* cmdCenter);
	void run();
	void show();
	
	// SCV Control //
	void resourceSCVControl(SCV* scv);
	void workSCVControl(SCV* scv);

	// Resource //
	Unit getIdleMineral();
	Unit getIdleGas();	

	void orderMineral(SCV* scv, Unit target);
	void orderGas(SCV* scv, Unit target);

	bool getProperBasicWork(SCV* scv);
	void gasCompleted(Unit gas);

	void addSCV(MyUnit* newSCV);
	void addSCV(SCV* newSCV);
		
	// Work //
	SCV* getWorker(bool except = false);

	int getRequireSCVCount() { return scvCount - (mineralSCV.size() + workSCV.size() + gasSCV.size()); };
	TilePosition getGasPosition() { return gasPos; };
	TilePosition getCmdTilePosition() { return cmdCenter->getSelf()->getTilePosition(); };
	vector<SCV*> getWorkSCV(){ return workSCV; };
	vector<SCV*> getMineralSCV(){ return mineralSCV; };
};

