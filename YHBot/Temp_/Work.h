#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include "MyUnit.h"

using namespace std;
using namespace BWAPI;
using namespace Filter;

class BasicWork{
	UnitType targetType;

public:
	BasicWork(UnitType type){
		targetType = type;
	}

	~BasicWork() {};

	virtual const UnitType& getTargetType(){
		return targetType;
	}
};

class UnitWork : public BasicWork{

public:
	UnitWork(UnitType type) : BasicWork(type){}
};

class BuildingWork : public BasicWork{
	TilePosition targetPos;
	SCV* builder;
	
public:

	BuildingWork(UnitType type, TilePosition pos) : BasicWork(type){
		targetPos = pos;
	};

	~BuildingWork(){};

	SCV* getBuilder(){
		return builder;
	}

	void setBuilder(SCV* builder){
		this->builder = builder;
	}

	const TilePosition& getTilePosition(){
		return targetPos;
	}

	void setTilePosition(TilePosition pos){
		this->targetPos = pos;
	}
};

/////////////////////

class structWork{
	UnitType target;
	SCV* builder;
	TilePosition pos;
	int startFrame;
	bool retry;

public:
	structWork(UnitType _target) : target(_target){

	}

	structWork(UnitType _target, TilePosition _pos, int _startFrame, bool _retry) : target(_target), pos(_pos), startFrame(_startFrame), retry(_retry){

	};

	const UnitType& getTarget(){
		return target;
	}

	SCV* getBuilder(){
		return builder;
	}

	void setBuilder(SCV* builder){
		this->builder = builder;
	}

	const TilePosition& getTilePosition(){
		return pos;
	}

	void setTilePosition(TilePosition pos){
		this->pos = pos;
	}

	const int& getStartFrame(){
		return startFrame;
	}

	bool& getRetry(){
		return retry;
	}
};