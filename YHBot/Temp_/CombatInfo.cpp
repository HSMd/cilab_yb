#include "CombatInfo.h"
#include "UtilMgr.h"
#include "../Single.h"

int nx[8] = { 1, 1, 0, -1, -1, -1, 0, 1 };
int ny[8] = { 0, 1, 1, 1, 0, -1, -1, -1 };

CombatInfo* CombatInfo::s_CombatInfo;
CombatInfo::CombatInfo() {
	init();
}

CombatInfo::~CombatInfo() {

}

Position getBoxPosition(bool first, UnitType ut, Position p) {
	Position ret;
	if (first) {
		ret.x = p.x - ut.width() / 2;
		ret.y = p.y - ut.width() / 2;
	}
	else {
		ret.x = p.x + ut.width() / 2;
		ret.y = p.y + ut.width() / 2;
	}
	return ret;
}

CombatInfo* CombatInfo::create() {
	if (s_CombatInfo) {
		return s_CombatInfo;
	}
	s_CombatInfo = new CombatInfo();
	return s_CombatInfo;
}

CombatInfo* CombatInfo::getInstance() {
	return s_CombatInfo;
}

void CombatInfo::init() {
	enemyUnitsInfo.clear();
	enemyBuildingsInfo.clear();
	enemyGroup.clear();
	enemyBase.clear();

	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			enemyPos[y][x] = Position(x * 32, y * 32);
			enemyPosVal[y][x] = 0;
		}
	}
}

void CombatInfo::updateEnemyInfo() {
	BWAPI::Unitset enemyUnits = BWAPI::Broodwar->enemy()->getUnits();
	for (auto q = enemyUnits.begin(); q != enemyUnits.end(); q++) {
		pair<UnitType, Position> info;
		info.first = (*q)->getType();
		info.second = (*q)->getPosition();
		if ((*q)->getType().isBuilding()) {			// buildings
			bool already = false;
			for (auto p = enemyBuildingsInfo.begin(); p != enemyBuildingsInfo.end(); p++) {
				if ((*p).first == (*q)->getID()) {
					already = true;
					break;
				}
			}
			if ((*q)->isVisible() && !already) {
				enemyBuildingsInfo.push_back(pair<int, pair<UnitType, Position>>((*q)->getID(), info));
			}
		}
		else {										// units
			bool already = false;
			for (auto p = enemyUnitsInfo.begin(); p != enemyUnitsInfo.end(); p++) {
				if ((*p).first == (*q)->getID()) {
					already = true;
					if((*q)->isVisible())
						(*p).second.second = (*q)->getPosition();
					break;
				}
			}
			if(already == false)
				enemyUnitsInfo.push_back(pair<int, pair<UnitType, Position>>((*q)->getID(), info));
		}
	}

	enemyGroup.clear();
	enemyGroup = getEnemyGroupArray();
}

void CombatInfo::drawCombatInfo(bool debug) {
	if(debug){
		for (auto q = enemyUnitsInfo.begin(); q != enemyUnitsInfo.end(); q++) {
			BWAPI::Broodwar->drawBoxMap(getBoxPosition(true, (*q).second.first, (*q).second.second), getBoxPosition(false, (*q).second.first, (*q).second.second), Colors::Red, false);
			BWAPI::Broodwar->drawTextMap((*q).second.second, (*q).second.first.c_str());
		}
		//printf("enemyUnitNumber : %d\n", enemyUnitsInfo.size());
		for (auto q = enemyBuildingsInfo.begin(); q != enemyBuildingsInfo.end(); q++) {
			BWAPI::Broodwar->drawBoxMap(getBoxPosition(true, (*q).second.first, (*q).second.second), getBoxPosition(false, (*q).second.first, (*q).second.second), Colors::Orange, false);
			BWAPI::Broodwar->drawTextMap((*q).second.second, (*q).second.first.c_str());
		}

		//printf("enemyBuildNumber : %d\n", enemyBuildingsInfo.size());
		/*
		for (int x = 0; x < 128; x++) {
			for (int y = 0; y < 128; y++) {
				BWAPI::Broodwar->drawTextMap(enemyPos[y][x], getString(enemyPosVal[y][x]));
			}
		}
		

		printf("ENEMYGROUP SIZE %d\n", enemyGroup.size());
		*/

		for (auto q = enemyGroup.begin(); q != enemyGroup.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q).first, 8, BWAPI::Colors::Red, true);
			BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.first, BWAPI::Colors::Red, false);
			BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.second, BWAPI::Colors::Orange, false);
		}
		
	}
}

int CombatInfo::getEnemyUnitNumber(UnitType ut) {
	int ret = 0;
	for (auto q = enemyUnitsInfo.begin(); q != enemyUnitsInfo.end(); q++) {
		if ((*q).second.first == ut) {
			ret++;
		}
	}
	return ret;
}

int CombatInfo::getEnemyBuildingNumber(UnitType ut) {
	int ret = 0;
	for (auto q = enemyBuildingsInfo.begin(); q != enemyBuildingsInfo.end(); q++) {
		if ((*q).second.first == ut) {
			ret++;
		}
	}
	return ret;
}

void CombatInfo::onDestroy(Unit u) {
	if (u->getType().isBuilding()){
		auto iter = enemyBuildingsInfo.begin();
		auto iter2 = enemyBuildingsInfo.begin();
		for (iter = enemyBuildingsInfo.begin(); iter != enemyBuildingsInfo.end(); iter = iter2) {
			iter2++;
			if ((*iter).second.first == u->getType()) {
				enemyBuildingsInfo.erase(iter);
			}
		}
	}
	else {
		auto iter = enemyUnitsInfo.begin();
		auto iter2 = enemyUnitsInfo.begin();
		for (iter = enemyUnitsInfo.begin(); iter != enemyUnitsInfo.end(); iter = iter2) {
			iter2++;
			if ((*iter).second.first == u->getType()) {
				enemyUnitsInfo.erase(iter);
			}
		}
	}
}

vector <pair<Position, pair<int,int>>> CombatInfo::getEnemyGroupArray() {
	vector<pair<Position, pair<int, int>>> ret;
	vector<Position> posv;
	vector<int> unitCount;
	vector<int> unitPosX;
	vector<int> unitPosY;
	vector<int> maxUnitDist;
	posv.clear();
	ret.clear();
	maxUnitDist.clear();
	unitCount.clear();
	int count = 0;
	int range = 0;
	
	

	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			enemyPosVal[y][x] = 0;
		}
	}
	for (auto q = enemyUnitsInfo.begin(); q != enemyUnitsInfo.end(); q++) {
		if ((*q).second.first == UnitTypes::Protoss_Probe) continue;
		int _x = (*q).second.second.x;
		int _y = (*q).second.second.y;
		_x /= 32;
		_y /= 32;
		enemyPosVal[_y][_x] ++;
	}
	int R = 16;
	for (int x = 0; x < 128; x++) {
		for (int y = 0; y < 128; y++) {
			for (int k = 0; k < 8; k++) {
				int _x = x + nx[k];
				int _y = y + ny[k];
				if (_x < 0 || _x > 128 || _y < 0 || _y > 128) continue;
				if (enemyPosVal[_y][_x] > 0) {
					vector<pair<int, int>> queue;
					queue.push_back(pair<int,int>(_x, _y));
					count = 0;
					int pos_x = 0;
					int pos_y = 0;
					while (queue.size() > 0) {
						int xx = queue.front().first;
						int yy = queue.front().second;
						queue.erase(queue.begin());
						for (int i = -R; i <= R; i++) {
							for (int j = -R; j <= R; j++) {
								int _xx = _x + i;
								int _yy = _y + j;
								if (_xx < 0 || _xx > 128 || _yy < 0 || _yy > 128) continue;
								if (enemyPosVal[_yy][_xx] > 0) {
									queue.push_back(pair<int, int>(_xx, _yy));
									enemyPosVal[_yy][_xx] = -1;
									count++;
									pos_x += enemyPos[_yy][_xx].x;
									pos_y += enemyPos[_yy][_xx].y;
								}
							}
						}
					}
					if (count != 0) {
						posv.push_back(Position(pos_x /= count, pos_y /= count));
						unitCount.push_back(0);
						unitPosX.push_back(0);
						unitPosY.push_back(0);
						maxUnitDist.push_back(0);
					}
				}
			}
		}
	}
	if (posv.size() <= 0) return ret;
	else {
		for (auto q = enemyUnitsInfo.begin(); q != enemyUnitsInfo.end(); q++) {
			if ((*q).second.first == UnitTypes::Protoss_Probe) continue;
			float minDist = 99999;
			int index = 0;
			int nearIndex = 0;
			Position nearPos;
			for (auto p = posv.begin(); p != posv.end(); p++) {
				float dist = getDistance((*p), (*q).second.second);
				if (dist < minDist) {
					minDist = dist;
					nearIndex = index;
					nearPos = (*p);
				}
				index++;
			}
			unitCount[nearIndex]++;
			unitPosX[nearIndex] += (*q).second.second.x;
			unitPosY[nearIndex] += (*q).second.second.y;
			float mind = getDistance((*q).second.second, nearPos);
			if (maxUnitDist[nearIndex] < mind) {
				maxUnitDist[nearIndex] = mind;
			}
		}
	}

	for (int x = 0; x < unitCount.size(); x++) {
		if (unitCount[x] != 0) {
			unitPosX[x] /= unitCount[x];
			unitPosY[x] /= unitCount[x];
		}
		ret.push_back(pair<Position, pair<int, int>>(Position(unitPosX[x], unitPosY[x]), pair<int, int>(maxUnitDist[x], unitCount[x])));
	}

	return ret;
}

char* CombatInfo::getString(int x) {
	char * str;
	int d, s;
	str = _fcvt(x, 0, &d, &s);
	if (x == 0) {
		str[0] = '0';
	}

	return str;
}