#pragma once
#include "StateMgr.h"
#include "MyUnit.h"
#include "TerritoryMgr.h"
#include <map>
using namespace std;

class Invasion_pylon{

private:
	bool PylonFlag;
	bool ZealotFlag;
	bool DragoonFlag;
	
	SCV* sentrySCV;
	SCV* guardSCV[2];
	vector<Solider*> Marine;
	map<Solider*, TilePosition> HoldUnits;
	Unit pylon;
public:
	Invasion_pylon();
	~Invasion_pylon();
	TerritoryMgr* territory;

	bool getPylonFlag(){ return PylonFlag; }
	bool getZealotFlag(){ return ZealotFlag; }
	void getMarine();

	void SCVControl(SCV* scv, Unit unit);
	void MarineControl(Solider* Marine, int n);
	Solider* getIdleMarine();

	bool PylonDetect();
	bool ZealotDetect();
	bool DragoonDetect();
	virtual void run(BUILD_FLAG build, COMBAT_FLAG combat, TerritoryMgr *territory);
};