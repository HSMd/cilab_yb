#include "Me.h"

Me::Me(){
}


Me::~Me()
{
	
}

void Me::init(){
	//addTerritory(mapInfo->getMyStartBase());
}

Me* Me::getInstance()
{
	static Me instance;
	return &instance;
}

BWAPI::Player Me::player()
{
	return Broodwar->self();
}

int Me::minerals(){
	return Broodwar->self()->minerals();
}

int Me::gas(){
	return Broodwar->self()->gas();
}

void Me::addUnit(BWAPI::Unit unit)
{
	if (unit->getType() == UnitTypes::Terran_SCV){
		units[unit->getType()].push_back(new SCV(unit));
	}
	else{
		units[unit->getType()].push_back(new Solider(unit));
	}
	
}

void Me::addUnitImmediately(BWAPI::Unit unit){

	if (unit->getType() == UnitTypes::Terran_SCV){
		createUnits[unit->getType()].push_back(new SCV(unit));
	}
	else{
		createUnits[unit->getType()].push_back(new Solider(unit));
	}
}

void Me::addAllUnit(Unit unit){

	allUnits.push_back(new MyUnit(unit));
}

void Me::addBuilding(BWAPI::Unit building)
{
	buildings[building->getType()].push_back(new Structure(building));
}

void Me::addBuildingImmediately(BWAPI::Unit building){
	createBuildings[building->getType()].push_back(new Structure(building));
}

vector<MyUnit*>& Me::getUnits(int type)
{
	return units[type];
}

vector<MyUnit*>& Me::getImmediatelyUnit(int type){
	return createUnits[type];
}

vector<MyUnit*>& Me::getImmediatelyBuilding(int type){
	return createBuildings[type];
}

vector<MyUnit*>& Me::getBuildings(int type){
	return buildings[type];
}

vector<MyUnit*>& Me::getAllUnits(){
	return allUnits;
}

