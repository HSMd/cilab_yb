#pragma once
#include <BWAPI.h>

using namespace BWAPI;

class MyUnit
{
protected:
	Unit self;

public:
	MyUnit(Unit _self){
		self = _self;
	};
	~MyUnit(){};

	virtual void run(){};
	Unit getSelf(){ return self; };
};

class Structure : public MyUnit{

public:
	enum STATE{
		IDLE,
		LIFT,
		LAND,
		MOVE
	};

private:
	STATE state;
	TilePosition targetPos;

public:
	Structure(Unit _self) : MyUnit(_self) {};
	~Structure() {};

	void run() {};
	void orderMove(TilePosition pos){ targetPos = pos; state = LIFT; }
};

class SCV : public MyUnit{
public:
	enum STATE{
		IDLE,
		MINERAL,
		GAS,
		MOVE,
		RETURN,
		WAIT,
		BUILD,
		REPAIR,
		ETC,
	};

private:
	STATE state;
	Unit target;
	UnitType targetType;
	TilePosition targetPos;

	bool buildComplete;

public:
	SCV(Unit _self) : MyUnit(_self) {
		state = STATE::IDLE;
		buildComplete = false;
	};
	~SCV();

	void run() {};

	// Order
	void orderIDLE() { self->stop(); state = STATE::IDLE; };
	void orderMineral(Unit target){ this->target = target; state = STATE::MINERAL; }
	void orderGas(Unit target){ this->target = target; state = STATE::GAS; }
	void orderETC(){ self->stop(); state = STATE::ETC; }
	void orderMove(TilePosition pos){ self->stop(); this->targetPos = pos; state = STATE::MOVE; }
	void orderWait() { self->stop(); state = STATE::WAIT; }
	void orderBuild(UnitType type, TilePosition pos){ self->stop(); this->targetType = type, this->targetPos = pos; state = STATE::BUILD; }
	void orderReturn(){ state = STATE::RETURN; }
	void orderRepair(Unit unit){ this->target = unit; state = STATE::REPAIR; }
	void setBuildComplete(bool complete) { buildComplete = complete; };
	bool getBuildComplete() { return buildComplete; };
	
	STATE getState() { return state; }
	TilePosition getTargetPos() { return targetPos; }
	Unit getTarget() { return target; }
	UnitType getTargetType(){ return targetType; }
};

/////////////////////////////////

class Solider : public MyUnit{

public:
	enum STATE{
		IDLE,
		WAIT,
		ATTACK,
		HOLD,
		MOVE,
		RETURN,
		ETC,
	};
	Solider(Unit _self) : MyUnit(_self) {
		state = STATE::IDLE;
	};
	~Solider() {};

private:
	STATE state;
	Unit target;
	UnitType targetType;
	TilePosition targetPos;

public:
	void orderIDLE(){ self->stop(); state = STATE::IDLE; }
	void orderMove(TilePosition tpos){ self->stop(); this->targetPos = tpos; state = STATE::MOVE; }
	void orderAttack(Unit unit){	this->target = unit; state = STATE::ATTACK;	}
	void orderHold(){ self->stop(); state = STATE::HOLD; }
	void orderWait(){ self->stop(); state = STATE::WAIT;}
	void orderETC(){ self->stop(); state = STATE::ETC;}
	void orderReturn(){ state = STATE::RETURN; }

	void setState(STATE state){ this->state = state; }
	STATE getState() { return state; }
	TilePosition getTargetPos() { return targetPos; }
	Unit getTarget() { return target; }
	UnitType getTargetType(){ return targetType; }
};