#include "Invasion_pylon.h"
#include "../single.h"
#include "UtilMgr.h"

Invasion_pylon::Invasion_pylon(){
	PylonFlag = ZealotFlag = DragoonFlag = false;
	guardSCV[0] = guardSCV[1] = sentrySCV = nullptr;
	switch (mapInfo->getDir()){
	case 0:
		HoldUnits[Marine[0]] = TilePosition(100, 6);
		HoldUnits[Marine[1]] = TilePosition(100, 6);
		HoldUnits[Marine[1]] = TilePosition(103, 12);
		HoldUnits[Marine[2]] = TilePosition(103, 8);
		HoldUnits[Marine[3]] = TilePosition(101, 6);
		HoldUnits[Marine[4]] = TilePosition(96, 5);
		break;
	case 1:
		HoldUnits[Marine[0]] = TilePosition(118, 102);
		HoldUnits[Marine[1]] = TilePosition(119, 101);
		HoldUnits[Marine[2]] = TilePosition(119, 102);
		HoldUnits[Marine[3]] = TilePosition(121, 99);
		HoldUnits[Marine[4]] = TilePosition(114, 101);
		break;
	case 2:
		HoldUnits[Marine[0]] = TilePosition(7, 26);
		HoldUnits[Marine[1]] = TilePosition(6, 27);
		HoldUnits[Marine[2]] = TilePosition(10, 25);
		HoldUnits[Marine[3]] = TilePosition(11, 25);
		HoldUnits[Marine[4]] = TilePosition(14, 26);
		break;
	case 3:
		HoldUnits[Marine[0]] = TilePosition(23, 117);
		HoldUnits[Marine[1]] = TilePosition(23, 120);
		HoldUnits[Marine[2]] = TilePosition(23, 121);
		HoldUnits[Marine[3]] = TilePosition(27, 123);
		HoldUnits[Marine[4]] = TilePosition(28, 123);
		break;
	}
}
Invasion_pylon::~Invasion_pylon(){

}

void Invasion_pylon::run(BUILD_FLAG build, COMBAT_FLAG combat, TerritoryMgr *territory){
	
	PylonFlag = PylonDetect();
	ZealotFlag = ZealotDetect();
	DragoonFlag = DragoonDetect();
	//Marine
	getMarine();
	int Marinesize = Marine.size();
	if (Marinesize > 0){
		bw << "marine" << endl;
		bw << "Marine size : " << Marinesize << endl;
	}

	if (DragoonFlag){

	}
	else if (ZealotFlag){
		if (sentrySCV == nullptr){
			sentrySCV = territory->getFirstTerritory()->getWorker(true);
		}
		switch (mapInfo->getDir()){
		case 0:
			sentrySCV->orderMove(TilePosition(99, 11));
			break;
		case 1:
			sentrySCV->orderMove(TilePosition(115, 98));
			break;
		case 2:
			sentrySCV->orderMove(TilePosition(11, 31));
			break;
		case 3:
			sentrySCV->orderMove(TilePosition(28, 118));
			break;
		}
		if (sentrySCV->getState() == SCV::STATE::MOVE){
			SCVControl(sentrySCV, NULL);
		}
		if (guardSCV[0] == nullptr){
			guardSCV[0] = territory->getFirstTerritory()->getWorker(true);
		}
	
		if (guardSCV[1] == nullptr){
			guardSCV[1] = territory->getFirstTerritory()->getWorker(true);
		}
		switch (mapInfo->getDir()){
		case 0:
			guardSCV[0]->orderMove(TilePosition(99, 10));
			guardSCV[1]->orderMove(TilePosition(100, 11));
			break;
		case 1:
			guardSCV[0]->orderMove(TilePosition(115, 99));
			guardSCV[1]->orderMove(TilePosition(116, 98));
			break;
		case 2:
			guardSCV[0]->orderMove(TilePosition(11, 30));
			guardSCV[1]->orderMove(TilePosition(10, 31));
			break;
		case 3:
			guardSCV[0]->orderMove(TilePosition(27, 118));
			guardSCV[1]->orderMove(TilePosition(28, 119));
			break;

		}	
		if (sentrySCV->getSelf()->getHitPoints() < 60){
			guardSCV[0]->orderRepair(sentrySCV->getSelf());
			guardSCV[1]->orderRepair(sentrySCV->getSelf());
		}
		SCVControl(guardSCV[0], NULL);
		SCVControl(guardSCV[1], NULL);

		if (Marinesize > 0){
			for (int i = 0; i < Marinesize; i++){
				Marine[i]->orderHold();
				MarineControl(Marine[i], i);
			}
			
		
		}
	}
	else if (PylonFlag){
		bw << "pylon" << endl;
		
		if (sentrySCV == nullptr){
			sentrySCV = territory->getFirstTerritory()->getWorker(true);
		}
		switch (mapInfo->getDir())
		{
		case 0:
			sentrySCV->orderMove(TilePosition(93, 25));
			break;
		case 1:
			sentrySCV->orderMove(TilePosition(100, 90));
			break;
		case 2:
			sentrySCV->orderMove(TilePosition(27, 37));
			break;
		case 3:
			sentrySCV->orderMove(TilePosition(36, 102));
			break;
		default:
			break;
		}
		if (sentrySCV->getState() == SCV::STATE::MOVE){
			SCVControl(sentrySCV, NULL);
		}
		if (guardSCV[0] == nullptr){
			guardSCV[0] = territory->getFirstTerritory()->getWorker(true);
			guardSCV[0]->orderETC();
		}
		if (guardSCV[1] == nullptr){
			guardSCV[1] = territory->getFirstTerritory()->getWorker(true);
			guardSCV[1]->orderETC();
		}
		if (guardSCV[0] != nullptr && guardSCV[1] != nullptr){
			SCVControl(guardSCV[0], pylon);
			SCVControl(guardSCV[1], pylon);
		}
		//Marine
		
		if (Marinesize > 0){
			for (int i = 0; i < Marinesize; i++){
				Marine[i]->orderAttack(pylon);
				MarineControl(Marine[i], 0);
			}
			
		}
	}
	else{//normal

	}
}

bool Invasion_pylon::PylonDetect(){
	for (Unit u : Broodwar->enemy()->getUnits()){
		if (u->getType() == UnitTypes::Protoss_Pylon){
	/*		if (u->isVisible()){
				pylon = u;
				PylonFlag = true;
				break;
			}
			else{
				pylon = nullptr;
				PylonFlag = false;
			}
		}
	}
	return PylonFlag*/
			switch (mapInfo->getDir()){
			case 0:
				if (isContainOneTerm(TilePosition(99, 6), u->getTilePosition(), 7)){
					pylon = u;
					PylonFlag = true;
				}
				else{
					pylon = nullptr;
					PylonFlag = false;
				}
				break;

			case 1:
				if (isContainOneTerm(TilePosition(112, 97), u->getTilePosition(), 7)){
					pylon = u;
					PylonFlag = true;
				}
				else{
					pylon = nullptr;
					PylonFlag = false;
				}
				break;

			case 2:
				if (isContainOneTerm(TilePosition(6, 26), u->getTilePosition(), 7)){
					pylon = u;
					PylonFlag = true;
				}
				else{
					pylon = nullptr;
					PylonFlag = false;
				}
				break;

			case 3:
				if (isContainOneTerm(TilePosition(22, 116), u->getTilePosition(), 7)){
					pylon = u;
					PylonFlag = true;
				}
				else{
					pylon = nullptr;
					PylonFlag = false;
				}
				break;
			}
		}
		else{
			PylonFlag = false;
		}
	}
	return PylonFlag;
}
bool Invasion_pylon::ZealotDetect(){
	//SCV* t_scv = sentrySCV;
	for (Unit u : Broodwar->enemy()->getUnits()){
		if (u->getType() == UnitTypes::Protoss_Zealot){
			/*TilePosition tpos = sentrySCV->getSelf()->getTilePosition();
			if (isContainOneTerm(TilePosition(tpos.x - 5, tpos.y - 5), u->getTilePosition(), 13)){
				ZealotFlag = true;
			}
			else{
				ZealotFlag = false;
			}*/
			if (u->isVisible()){
				ZealotFlag = true;
			}
			else{
				ZealotFlag = false;
			}
		}
		else{
			ZealotFlag;
		}
	}
	return ZealotFlag;
}
bool Invasion_pylon::DragoonDetect(){
	DragoonFlag = false;
	return DragoonFlag;
}
void Invasion_pylon::SCVControl(SCV* scv, Unit unit){
	Unit t_scv = scv->getSelf();
	TilePosition t_pos = scv->getTargetPos();
	Unit t_unit = scv->getTarget();
	switch (scv->getState()){
	case SCV::STATE::IDLE:

		break;
	case SCV::STATE::MOVE:
		t_scv->move(tile2pos(t_pos));
		if (t_scv->getDistance(tile2pos(t_pos)) == 0)	t_scv->stop();
		break;
	case SCV::STATE::ETC:
		if (!t_scv->isAttackFrame()){
				t_scv->attack(unit);
		}
		break;
	case SCV::STATE::REPAIR:
			t_scv->repair(t_unit);
		break;
	}
}
void Invasion_pylon::getMarine(){
	auto& marines = me->getUnits(UnitTypes::Terran_Marine);
	Solider *re = nullptr;
	for (auto& marine : marines){
		auto m = dynamic_cast<Solider *>(marine);
		if (m == nullptr || !m->getSelf()->exists()){
			continue;
		}
		if (m->getState() == Solider::STATE::IDLE){
			re = m;
			re->orderETC();
			Marine.push_back(re);
			break;
		}
	}
	for (auto iter = Marine.begin(); iter != Marine.end(); iter++){
		Solider* t_marine = (*iter);
		if (t_marine == nullptr){
			Marine.erase(iter);
		}
	}
}
Solider* getIdleMarine(){
	for (auto marine : me->getUnits(UnitTypes::Terran_Marine)){
		auto m = dynamic_cast<Solider *>(marine);
		if (m->getState() == Solider::STATE::IDLE){
			if (m->getSelf()->exists()){
				return m;
			}
		}
	}
	return nullptr;
}
void Invasion_pylon::MarineControl(Solider* Marine, int n){
	Unit t_unit = Marine->getTarget();
	TilePosition t_pos = Marine->getTargetPos();
	switch (Marine->getState()){
	case Solider::STATE::ATTACK:
		if (!Marine->getSelf()->isAttackFrame())
			Marine->getSelf()->attack(t_unit);
		break;
	case Solider::STATE::HOLD:
		t_pos = HoldUnits[Marine];
		Marine->getSelf()->move(tile2pos(t_pos));
		if (Marine->getSelf()->getDistance(tile2pos(t_pos)) == 0)
			Marine->getSelf()->holdPosition();
		break;
	case Solider::STATE::MOVE:
		Marine->getSelf()->move(tile2pos(t_pos));
		break;
	default:
		break;
	}
}
