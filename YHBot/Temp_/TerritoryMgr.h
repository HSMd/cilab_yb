#pragma once
#include "Territory.h"

class TerritoryMgr
{
	vector<Territory*> myTerritorys;

public:
	TerritoryMgr();
	~TerritoryMgr();

	void run();

	Territory* getNearTerritory(TilePosition dst);
	Territory* getFirstTerritory(){	return myTerritorys[0];}
	void completeSCVReturn(Unit unit);
	void addTerritory(Unit unit);
	void scvCompleted(Unit unit);
	void gasCompleted(Unit unit);
};

