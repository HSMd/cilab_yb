#include <algorithm>

#include "Commander.h"
#include "UtilMgr.h"
#include "../Single.h"
Commander::Commander()
{
	delayMineral = 0;
	delayGas = 0;
}

Commander::~Commander()
{	
	BWTA::cleanMemory();
}

///////////////////////////////////////////////////

  // On Func // 
void Commander::init()
{		
	// Setting

	// Hello World!
	bw->sendText("Hello I'm BangBot Beta 1.0 ver");
	bw << "The map is " << bw->mapName() << "!" << std::endl;

	bw->enableFlag(Flag::UserInput);

	// Speed 0 - 10
	bw->setLocalSpeed(10);
	bw->setCommandOptimizationLevel(0);

	BWTA::readMap();
	BWTA::analyze();

	mapInfo->getInstance()->init();
	//build.init(MapInfo::getInstance()->getDir());
}

void Commander::start(){

	//myTerritorys.push_back(new Territory());

	addUnitSchedule(UnitTypes::Terran_SCV, 10);
	
	switch (mapInfo->getDir()){
	case 0:
		addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(97, 5), 1, false);
		addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(102, 9), 1, false);
		addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(100, 7), 1, false);
		addUnitSchedule(UnitTypes::Terran_Marine, 5);
		/*build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(97, 5), UnitTypes::Terran_SCV, 7, 1));
		build.push(Work(UnitTypes::Terran_Barracks, TilePosition(102, 9), UnitTypes::Terran_SCV, 10, 1));
		build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(100, 7), UnitTypes::Terran_SCV, 12, 1));*/
		break;

	case 1:
		addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(120, 97), 1, false);
		addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(114, 101), 1, false);
		addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(118, 99), 1, false);
		/*build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(120, 97), UnitTypes::Terran_SCV, 7, 1));
		build.push(Work(UnitTypes::Terran_Barracks, TilePosition(114, 101), UnitTypes::Terran_SCV, 10, 1));
		build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(118, 99), UnitTypes::Terran_SCV, 12, 1));*/
		break;

	case 2:
		addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(11, 26), 1, false);
		addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(4, 28), 1, false);
		addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(8, 26), 1, false);
		/*build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(11, 26), UnitTypes::Terran_SCV, 7, 1));
		build.push(Work(UnitTypes::Terran_Barracks, TilePosition(4, 28), UnitTypes::Terran_SCV, 10, 1));
		build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(8, 26), UnitTypes::Terran_SCV, 12, 1));*/
		break;

	case 3:
		addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(23, 118), 1, false);
		addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(24, 120), 1, false);
		addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(28, 121), 1, false);
		/*build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(23, 118), UnitTypes::Terran_SCV, 7, 1));
		build.push(Work(UnitTypes::Terran_Barracks, TilePosition(24, 120), UnitTypes::Terran_SCV, 10, 1));
		build.push(Work(UnitTypes::Terran_Supply_Depot, TilePosition(28, 121), UnitTypes::Terran_SCV, 12, 1));*/
		break;
	}
	
	addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(0, 0), 1, false);
	addStructureSchedule(UnitTypes::Terran_Refinery, TilePosition(0, 0), 1, false);
	addUnitSchedule(UnitTypes::Terran_Marine,1);
	addUnitSchedule(UnitTypes::Terran_Marine, 1);
	addUnitSchedule(UnitTypes::Terran_Marine, 1);
	addUnitSchedule(UnitTypes::Terran_Marine, 1);
	addUnitSchedule(UnitTypes::Terran_Marine, 1);
	/*addStructureSchedule(UnitTypes::Terran_Barracks, TilePosition(0, 0), 1, false);
	addStructureSchedule(UnitTypes::Terran_Factory, TilePosition(0, 0), 2, false);	
	addStructureSchedule(UnitTypes::Terran_Machine_Shop, TilePosition(0, 0), 1, false);
	addStructureSchedule(UnitTypes::Terran_Supply_Depot, TilePosition(0, 0), 1, false);
	addUnitSchedule(UnitTypes::Terran_Vulture, 2);
	addUnitSchedule(UnitTypes::Terran_Siege_Tank_Tank_Mode, 5);*/
}

void Commander::show(){

	// draw
	drawTerrainData();

	// draw Text
	bw->drawTextScreen(200, 40, "ALL_SCV : %d", me->getUnits(UnitTypes::Terran_SCV).size());
	bw->drawTextScreen(200, 60, "Schedule : %d", schedule.size());
	
	//bw->drawTextScreen(200, 80, "TerritoryCount : %d", myTerritorys.size());
	//bw->drawTextScreen(200, 100, "ResourceSCV : %d", myTerritorys[0]->getMineralSCV().size()); 
	//bw->drawTextScreen(200, 120, "Schedule : %d", schedule.size());
}

void Commander::run()
{
	// StateMgr
	state.run();

	BUILD_FLAG build = state.getBuild();
	COMBAT_FLAG combat = COMBAT_FLAG::NORMAL;//state.getCombet();
	

	builder.run(build, combat);
	strategist.run(build, combat, &territoryMgr);
	// Territory run
	territoryMgr.run();
	
	/*
	for (auto territory : myTerritorys)
	{
		territory->run();
	}
	*/
	
	// product
	runSchedule();
}


 ///// Check /////
bool Commander::enoughResource(UnitType target, int readyMineral){
	if (target == UnitTypes::Terran_Refinery)
		readyMineral = 20;

	int restSupply = me->player()->supplyTotal() - me->player()->supplyUsed();
	if (restSupply < target.supplyRequired()){

		return false;
	}

	if (me->minerals() - delayMineral < target.mineralPrice() - readyMineral ||
		me->gas() - delayGas < target.gasPrice()) {

		return false;
	}

	return true;
}

bool Commander::enoughWhatBuild(UnitType target){

	auto rUnits = target.requiredUnits();
	for (auto require : rUnits){
		UnitType rType = require.first;

		if (rType.isBuilding()){
			if (me->getBuildings(require.first).size() < 1){
				return false;
			}
		}
		else{
			if (me->getUnits(require.first).size() < 1){
				return false;
			}
		}		
	}	

	return true;
}

bool Commander::canCrate(UnitType target){
	
	if (!enoughResource(target, 40)){
		return false;
	}	
	
	if (!enoughWhatBuild(target)){
		return false;
	}

	return true;
}


///// Schedule Func //////
bool isUnit(BasicWork* work){
	if (!work->getTargetType().isBuilding()){
		return true;
	}
	else
		return false;
}

int Commander::getUnitScheduleCount(UnitType target){
	int count = 0;
	for (auto work : schedule){
		if (work->getTargetType() == target)
			count++;
	}

	return count;
}

bool Commander::addUnitSchedule(UnitType target, int count, bool hurry){

	if (hurry){
		auto iter = find_if(schedule.begin(), schedule.end(), isUnit);

		for (int i = 0; i < count; i++)
			schedule.insert(iter, new UnitWork(target));
	}
	else{
		for (int i = 0; i < count; i++)
			schedule.push_back(new UnitWork(target));
	}

	return true;
}

bool Commander::addStructureSchedule(UnitType target, TilePosition pos, int count, bool hurry){
	
	if (hurry){
		auto iter = find_if(schedule.begin(), schedule.end(), isUnit);

		for (int i = 0; i < count; i++)
			//schedule.push_front(new BuildingWork(target, pos));
			schedule.insert(iter, new BuildingWork(target, pos));
	}
	else{
		for (int i = 0; i < count; i++)
			schedule.push_back(new BuildingWork(target, pos));
	}

	return true;
}

void Commander::runSchedule(){

	for (auto iter = schedule.begin(); iter != schedule.end();){
		BasicWork* work = (*iter);
		UnitType targetType = (*iter)->getTargetType();

		if (targetType.isBuilding() && !targetType.isAddon())
		{
			// Building
			if (!canCrate(targetType)){
				break;
			}

			delayMineral += targetType.mineralPrice();
			delayGas += targetType.gasPrice();

			BuildingWork* bWork = dynamic_cast<BuildingWork*>(work);
			SCV* builder = territoryMgr.getNearTerritory(bWork->getTilePosition())->getWorker(false);

			if (bWork->getTilePosition() == TilePosition(0, 0)){
				TilePosition targetBuildLocation = Broodwar->getBuildLocation(bWork->getTargetType(), builder->getSelf()->getTilePosition());
				bWork->setTilePosition(targetBuildLocation);
				builder->orderBuild(bWork->getTargetType(), targetBuildLocation);
			}
			else{
				builder->orderBuild(bWork->getTargetType(), bWork->getTilePosition());
			}

			iter = schedule.erase(iter);
			delete work;
		}
		else if (targetType.isAddon()){
			// Addon
			if (!enoughWhatBuild(targetType)){
				iter++;
				continue;
			}

			if (!enoughResource(targetType)){
				break;
			}

			auto& what = me->getBuildings(targetType.whatBuilds().first);
			Unit u = nullptr;

			for (auto unit : what){
				if (unit->getSelf()->canBuildAddon() && unit->getSelf()->exists()){
					u = unit->getSelf();

					break;
				}
			}			

			if (u == nullptr || !u->isIdle())
			{
				iter++;
				continue;
			}

			if (u->buildAddon(targetType)){
				delayMineral += targetType.mineralPrice();
				delayGas += targetType.gasPrice();

				iter = schedule.erase(iter);
				delete work;
			}
			else{
				break;
			}
		}
		else{
			// Unit
			if (!enoughWhatBuild(targetType)){
				iter++;
				continue;
			}

			if (!enoughResource(targetType)){
				break;
			}

			auto builder = getProperBuilding(targetType);
			if (builder == nullptr){
				iter++;
				continue;
			}

			if (builder->train(targetType)){

				//bw << "Success" << endl;

				iter = schedule.erase(iter);
				delete work;
			}
			else{
				//bw << "Fail" << endl;

				iter++;
				continue;
			}
		}
	}
}

Unit Commander::getProperBuilding(UnitType target){
	
	auto what = me->getBuildings(target.whatBuilds().first);

	for (auto b : what){
		Unit builder = b->getSelf();

		if (builder->isCompleted() && builder->canTrain() && builder->isIdle() && builder->exists()){
			return builder;
		}
	}

	return nullptr;
}

Territory* Commander::getNearTerritory(TilePosition dst){

	/*
	float minDis = 999999.0;
	Territory* re = nullptr;

	for (auto& territory : myTerritorys){
		float distance = getDistance(territory->getCmdTilePosition(), dst);

		if (distance < minDis){
			distance = minDis;
			re = territory;
		}
	}
	*/

	return nullptr;
}

///// Create & Complete /////
void Commander::structureCreate(Unit unit)
{
	me->addBuildingImmediately(unit);

	if (50 < bw->getFrameCount()){
		delayMineral -= unit->getType().mineralPrice();
		delayGas -= unit->getType().gasPrice();
	}

	territoryMgr.completeSCVReturn(unit);

	/*
	for (auto& territory : myTerritorys){
		for (auto& scv : territory->getWorkSCV()){
			if (scv->getTargetPos() == unit->getTilePosition() && scv->getTargetType() == unit->getType()){
				scv->orderReturn();
			}
		}
	}
	*/
}

void Commander::structureComplete(Unit unit)
{
	me->addBuilding(unit);

	if (unit->getType() == UnitTypes::Terran_Command_Center){

		territoryMgr.addTerritory(unit);
		/*
		if (bw->getFrameCount() < 50){

			myTerritorys[0]->init(mapInfo->getMyStartBase(), me->getBuildings(UnitTypes::Terran_Command_Center).back());
			return;
		}

		auto& otherBases = mapInfo->getAllBases();
		BWTA::BaseLocation* base = nullptr;

		for (auto other : otherBases){
			if (other->getTilePosition() == unit->getTilePosition()){
				base = other;
				break;
			}
		}

		if (base != nullptr){
			int scvCount = base->getMinerals().siege() * 2 + base->getGeysers().size() * 3;
			myTerritorys.push_back(new Territory(base, me->getBuildings(UnitTypes::Terran_Command_Center).back()));
		}
		*/
	}

	if (unit->getType() == UnitTypes::Terran_Refinery){

		territoryMgr.gasCompleted(unit);
		/*
		for (auto territory : myTerritorys){
			if (territory->getGasPosition() == unit->getTilePosition()){
				territory->gasCompleted(unit);
			}
		}
		*/
	}
}

void Commander::unitCreate(Unit unit)
{
	me->addUnitImmediately(unit);
}

void Commander::unitComplete(Unit unit)
{
	me->addUnit(unit);
	me->addAllUnit(unit);

	if (UnitTypes::Terran_SCV == unit->getType()){

		territoryMgr.scvCompleted(unit);

		/*
		MyUnit* scv = me->getUnits(UnitTypes::Terran_SCV).back();

		if (bw->getFrameCount() < 50){
			myTerritorys[0]->addSCV(scv);
			return;
		}

		for (auto territory : myTerritorys){
			if (territory->getRequireSCVCount() != 0){

				territory->addSCV(scv);

				return;
			}
		}
		*/
	}	
}
