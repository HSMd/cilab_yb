#include "Territory.h"

#include "../Single.h"
#include "UtilMgr.h"

//////////////

Territory::Territory()
{
	gas = nullptr;
}

Territory::Territory(BaseLocation* base, MyUnit* cmdCenter){

	init(base, cmdCenter);
}

Territory::~Territory()
{

}

// //

void Territory::show(){

}

void Territory::init(BaseLocation* base, MyUnit* cmdCenter){
	auto gases = base->getGeysers();
	Unit g = *gases.begin();
	gasPos = g->getTilePosition();

	this->minerals = base->getMinerals();
	this->cmdCenter = cmdCenter;
	this->scvCount = base->getMinerals().siege() * 2 + base->getGeysers().size() * 3;
}

void Territory::run(){

	// CheckGas
	if (gas != nullptr && resourceCount[gas] < 3){
		int count = 3 - resourceCount[gas];

		for (int i = 0; i < count; i++){
			SCV* scv = getWorker(true);
			orderGas(scv, gas);
			gasSCV.push_back(scv);
		}
	}

	// Work SCV RUN
	for (auto iter = workSCV.begin(); iter != workSCV.end();){
		SCV* scv = (*iter);

		if (!scv->getSelf()->exists()){

			iter++;
			continue;
		}

		if (scv->getState() == SCV::STATE::IDLE){
			iter = workSCV.erase(iter);
			addSCV(scv);

			continue;
		}

		workSCVControl(scv);
		++iter;
	}

	// Mineral SCV RUN
	for (auto& scv : mineralSCV){

		// Die
		if (!scv->getSelf()->exists()){
			continue;
		}

		if (scv->getState() == SCV::STATE::IDLE){
			getProperBasicWork(scv);
		}

		resourceSCVControl(scv);
	}

	// GAS SCV RUN
	for (auto& scv : gasSCV){

		// Die
		if (!scv->getSelf()->exists()){
			continue;
		}

		resourceSCVControl(scv);
	}
}

void Territory::resourceSCVControl(SCV* scv){

	Unit self = scv->getSelf();
	Unit target = scv->getTarget();

	switch (scv->getState()){

	case SCV::STATE::IDLE:
		scv->setBuildComplete(false);
		break;
		
	case SCV::STATE::MINERAL:

		if (self->isCarryingMinerals()){
			self->returnCargo(true);
		}
		else if (self->isIdle() || self->isGatheringGas()){
			self->gather(target);
		}

		break;

	case SCV::STATE::GAS:

		if (self->isIdle() || self->isCarryingMinerals()){
			self->gather(target);
		}
		else if (self->isCarryingGas()){
			self->returnCargo(true);
		}

		break;

	case SCV::STATE::ETC:

		break;

	default:
		break;
	}
}

void Territory::workSCVControl(SCV* scv){

	Unit self = scv->getSelf();
	UnitType targetType = scv->getTargetType();
	TilePosition targetPos = scv->getTargetPos();

	switch (scv->getState()){

	case SCV::STATE::MOVE:
		if (isAlmostReachedDest(tile2pos(targetPos), self->getPosition(), BWAPI::TILEPOSITION_SCALE * 4, BWAPI::TILEPOSITION_SCALE * 4)){
			scv->orderWait();
		}
		else{
			self->move(tile2pos(targetPos));
		}

		break;

	case SCV::STATE::WAIT:
		self->stop();
		
		break;

	case SCV::STATE::RETURN:
		if (self->isConstructing())
			break;

		if (isAlmostReachedDest(cmdCenter->getSelf()->getPosition(), self->getPosition(), BWAPI::TILEPOSITION_SCALE * 6, BWAPI::TILEPOSITION_SCALE * 6)){
			scv->orderIDLE();
		}
		else{
			self->move(cmdCenter->getSelf()->getPosition());
		}

		break;

	case SCV::STATE::BUILD:
		if (isAlmostReachedDest(tile2pos(targetPos), self->getPosition(), BWAPI::TILEPOSITION_SCALE * 8, BWAPI::TILEPOSITION_SCALE * 8)){
			if (!self->isConstructing()){
				if (self->build(targetType, targetPos)){
					scv->setBuildComplete(true);
				}
			}
		}
		else{
			self->move(tile2pos(targetPos));
		}

		break;

	default:
		break;
	}

	if (scv->getState() == SCV::STATE::WAIT){
		self->build(targetType);
	}
}

// Work //

SCV* Territory::getWorker(bool except){

	// True = 이 영토에서 제외
	// False = 작업하고 반환해야되니까 Work로

	SCV* re = nullptr;

	for (auto iter = mineralSCV.begin(); iter != mineralSCV.end(); ++iter){
		SCV* scv = (*iter);
		if (!scv->getSelf()->exists()){
			continue;
		}

		if (scv->getState() == SCV::STATE::MINERAL){

			if (scv->getSelf()->isGatheringGas()){
				bw << "???" << endl;
			}

			re = scv;

			resourceCount[scv->getTarget()]--;
			mineralSCV.erase(iter);
			re->orderETC();

			if (!except){
				workSCV.push_back(scv);
			}

			break;
		}
	}
	return re;
}

// Resource //

Unit Territory::getIdleMineral(){

	Unit re = nullptr;
	int minCount = 200;

	for (auto m : minerals){
		if (!m->exists()){
			continue;
		}

		if (resourceCount[m] < minCount){

			minCount = resourceCount[m];
			re = m;
		}
	}

	return re;
}

Unit Territory::getIdleGas(){
		
	if (gas != nullptr){
		if (resourceCount[gas] < 3){
			return gas;
		}
	}

	return nullptr;
}

void Territory::orderMineral(SCV* scv, Unit target){

	scv->orderMineral(target);
	resourceCount[target]++;
}

void Territory::orderGas(SCV* scv, Unit target){

	scv->orderGas(target);
	resourceCount[target]++;
}

bool Territory::getProperBasicWork(SCV* scv){
		
	// Mineral
	Unit target = getIdleMineral();
	if (target != nullptr){

		orderMineral(scv, target);

		return true;
	}

	return false;
}

void Territory::addSCV(MyUnit* newSCV){
	SCV* scv = dynamic_cast<SCV*>(newSCV);

	scv->orderIDLE();
	mineralSCV.push_back(scv);
}

void Territory::addSCV(SCV* newSCV){

	newSCV->orderIDLE();
	mineralSCV.push_back(newSCV);
}

void Territory::gasCompleted(Unit gas){

	this->gas = gas;
}

