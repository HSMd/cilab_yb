#pragma once
#include "StateMgr.h"

/*
class Work{
	UnitType targetType;
	TilePosition targetPos;
	bool hurry;
	int count;
	
	// 
	UnitType obj;
	int cmpCount;


public:
	Work(UnitType type, int _count) : targetType(type), count(_count){ hurry = false; };
	Work(UnitType type, int _count, bool hurry) : targetType(type), count(_count){ this->hurry = hurry; };
	Work(UnitType type, TilePosition pos, UnitType _obj, int _cmpCount, int _count) : targetType(type), targetPos(pos), obj(_obj), count(_count), cmpCount(_cmpCount) { hurry = true; };
	~Work(){};


	UnitType getTargetType(){ return targetType; };
	TilePosition getTargetPosition(){ return targetPos; };
	int getCount() { return count; };
	int getCmpCount() { return cmpCount; };
	bool getHurry() { return hurry; };
	bool isFire(); 	
};
*/

class BuildMgr
{

public:
	BuildMgr();
	~BuildMgr();

	void run(BUILD_FLAG build, COMBAT_FLAG combat);
};

