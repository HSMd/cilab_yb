#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include <list>
#include "TerritoryMgr.h"
#include "BuildMgr.h"
#include "StrategyMgr.h"
#include "Work.h"

using namespace std;
using namespace BWAPI;
using namespace Filter;

class Commander
{	
private:
	int delayMineral;
	int delayGas;

	list<BasicWork*> schedule;
	//vector<Territory*> myTerritorys;
	TerritoryMgr territoryMgr;
	
	StateMgr state;
	BuildMgr builder;
	StrategyMgr strategist;
public:
	Commander();
	~Commander();

	// OnFunc
	void init();
	void start();
	void show();
	void run();

	void structureCreate(Unit unit);
	void structureComplete(Unit unit);
	void unitCreate(Unit unit);
	void unitComplete(Unit unit);

	// Schedule Func
	int getUnitScheduleCount(UnitType target);
	bool addUnitSchedule(UnitType target, int count = 1, bool hurry = false);
	bool addStructureSchedule(UnitType target, TilePosition pos, int count = 1, bool hurry = true);
	void runSchedule();	
	Territory* getNearTerritory(TilePosition dst);

	// Build Func
	bool enoughResource(UnitType target, int readyMineral = 0);
	bool enoughWhatBuild(UnitType target);
	bool canCrate(UnitType target);
		
	Unit getProperBuilding(UnitType target);
};

