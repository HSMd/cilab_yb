#pragma once
#include "StateMgr.h"
#include "MyUnit.h"
#include <map>

using namespace std;

class Normal{
private:
	vector<Solider*> Marine;
	map<TilePosition, Solider*> HoldMarine;
public:
	Normal();
	~Normal();
	
	void getMarine();
	Solider* getIdleMarine();
	
	void MarineControl(Solider* marine);
	void HoldMarinePosition();
	void run(BUILD_FLAG build, COMBAT_FLAG combat);
};