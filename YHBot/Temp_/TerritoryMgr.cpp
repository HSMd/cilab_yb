#include "TerritoryMgr.h"

#include "UtilMgr.h"
#include "../Single.h"

TerritoryMgr::TerritoryMgr()
{
	myTerritorys.push_back(new Territory());
}


TerritoryMgr::~TerritoryMgr()
{
}


void TerritoryMgr::run(){
	
	for (auto territory : myTerritorys)
	{
		territory->run();
	}
}

Territory* TerritoryMgr::getNearTerritory(TilePosition dst){

	float minDis = 999999.0;
	Territory* re = nullptr;

	for (auto& territory : myTerritorys){
		float distance = getDistance(territory->getCmdTilePosition(), dst);

		if (distance < minDis){
			distance = minDis;
			re = territory;
		}
	}

	return re;
}
void TerritoryMgr::completeSCVReturn(Unit unit){
	for (auto& territory : myTerritorys){
		for (auto& scv : territory->getWorkSCV()){
			if (scv->getTargetPos() == unit->getTilePosition() && scv->getTargetType() == unit->getType()){
				scv->orderReturn();
			}
		}
	}
}

void TerritoryMgr::addTerritory(Unit unit){

	if (bw->getFrameCount() < 50){
		myTerritorys[0]->init(mapInfo->getMyStartBase(), me->getBuildings(UnitTypes::Terran_Command_Center).back());
		return;
	}

	auto& otherBases = mapInfo->getAllBases();
	BWTA::BaseLocation* base = nullptr;

	for (auto other : otherBases){
		if (other->getTilePosition() == unit->getTilePosition()){
			base = other;
			break;
		}
	}

	if (base != nullptr){
		int scvCount = base->getMinerals().siege() * 2 + base->getGeysers().size() * 3;
		myTerritorys.push_back(new Territory(base, me->getBuildings(UnitTypes::Terran_Command_Center).back()));
	}
}

void TerritoryMgr::scvCompleted(Unit unit){

	MyUnit* scv = me->getUnits(UnitTypes::Terran_SCV).back();

	if (bw->getFrameCount() < 50){
		myTerritorys[0]->addSCV(scv);
		return;
	}

	Territory* near = getNearTerritory(unit->getTilePosition());
	near->addSCV(scv);

	/*
	for (auto territory : myTerritorys){
		if (territory->getRequireSCVCount() != 0){

			territory->addSCV(scv);

			return;
		}
	}
	*/
}

void TerritoryMgr::gasCompleted(Unit unit){
	for (auto territory : myTerritorys){
		if (territory->getGasPosition() == unit->getTilePosition()){
			territory->gasCompleted(unit);
		}
	}
}