#pragma once
#include "StateMgr.h"
//#include "Invasion_pylon.h"
#include "Normal.h"
#include "TerritoryMgr.h"

class StrategyMgr
{
private:
//	Invasion_pylon invasion_pylon;
	Normal normal;

public:

	StrategyMgr();
	~StrategyMgr();
	void run(BUILD_FLAG build, COMBAT_FLAG combat, TerritoryMgr* t);
};

