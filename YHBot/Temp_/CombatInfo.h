#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <vector>
#include <list>


using namespace std;
using namespace BWAPI;
using namespace Filter;

class CombatInfo
{
	
private:
	CombatInfo();
	~CombatInfo();

	static CombatInfo *s_CombatInfo;

	const int GROUP_R = 400;
	int enemyPosVal[128][128];
	Position enemyPos[128][128];
	char* getString(int);

public:
	static CombatInfo* getInstance();
	static CombatInfo* create();

	void init();
	void updateEnemyInfo();
	void drawCombatInfo(bool debug);

	list<pair<int, pair<UnitType, Position>>> enemyUnitsInfo;				//[id, [UnitType, LastPosition]] (Units)
	list<pair<int, pair<UnitType, Position>>> enemyBuildingsInfo;			//[id, [UnitType, Position]] (Buildings)
	vector<pair<Position, pair<int, int>>> enemyGroup;						//[GroupPosition, [Range, GroupLevel]]
	vector<pair<BWTA::BaseLocation*, int>> enemyBase;						//[BaseLocation, baseLevel]

	int getEnemyUnitNumber(UnitType);										//적 유닛 개수
	int getEnemyBuildingNumber(UnitType);									//적 건물 개수
	vector<pair<Position, pair<int, int>>> getEnemyGroupArray();
	
	void onDestroy(Unit u);
};

