#include "StateMgr.h"

#include "../Single.h"
#include "UtilMgr.h"

StateMgr::StateMgr(){
	build = BUILD_FLAG::SIM_CITY_1;
	combat = COMBAT_FLAG::NORMAL;
};

StateMgr::~StateMgr(){

};

void StateMgr::chkCombatState(){

	// 화면에 보이는 적군이 없으면 NORMAL 온.
	// 작성해야됨.

	if (build == BUILD_FLAG::FACTORY_DOUBLE){
		// IF TANK 3 , VULTURE 3 ==> ATTACK!!
		if (3 <= me->getUnits(UnitTypes::Terran_Vulture).size() && 3 <= me->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).size()){
			combat = COMBAT_FLAG::ATTACK_TWO_FACTORY;
		}
	}
}

void StateMgr::run(){

	chkCombatState();

	if (build == BUILD_FLAG::SIM_CITY_1){
		if (isSafe()){
			// Safe Logic
			if (oppFlag[OPP_FLAG::FAST_FORECOUNT] == true){
				build = BUILD_FLAG::FACTORY_DOUBLE;
			}
			else{
				build = BUILD_FLAG::SIM_CITY_2;
			}
		}
		else{
			if (combatFlag[COMBAT_FLAG::INVASION_PYLON] == true){
				combat = COMBAT_FLAG::INVASION_PYLON;
			}
		}		 
	}

	/*
	if (build == BUILD_FLAG::SIM_CITY_2){
		if (isSafe()){
			//SafeLogic
		}
		else{

		}
	}
	*/
}

bool StateMgr::isSafe(){
	return buildFlag[build] == true && combatFlag[COMBAT_FLAG::NORMAL] == true;
}