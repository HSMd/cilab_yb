#include "SCVMgr.h"
#include "Commander.h"


SCVMgr::SCVMgr() : mapInfo(MapInfo::getInstance()), me(me::getInstance())
{
	spySCV = nullptr;
}

SCVMgr::~SCVMgr()
{
}

SCVMgr* SCVMgr::getInstance(){
	static SCVMgr instance;
	return &instance;
}

int SCVMgr::plan(){
	
	auto& territory = mapInfo->getMyTerritory();
	int count = 0;

	for (auto& t : territory){
		Unitset minerals = t->getMinerals();
		//Unitset gas = t->getGeysers();

		for (auto m : minerals){
			if (m->exists())
				count += 2;
		}		
	}

	auto gas = me->getBuildings(UnitTypes::Terran_Refinery);
	for (auto g : gas){
		if (g->getSelf()->exists())
			count += 3;
	}

	count = 7;

	int scvScheduleCount = Commander::getInstance()->getUnitScheduleCount(UnitTypes::Terran_SCV);
	int need = count - (me->getUnitImmediatelyUnit(UnitTypes::Terran_SCV).size() + scvScheduleCount);



	return (need < 0) ? 0 : need;
}

void SCVMgr::run(){
	
	//infiltration();

	// SCV RUN
	auto& scvs = me->getUnits(UnitTypes::Terran_SCV);

	for (auto& scv : scvs){
		auto iter = dynamic_cast<SCV*>(scv);

		if (iter == nullptr){
			Broodwar << "SCV NULL ERROR!!" << endl;
			continue;
		}

		if (iter->getState() == SCV::STATE::IDLE){
			Broodwar << "IDLE" << endl;
			getProperBasicWork(iter);
		}

		iter->run();
	}

	// SCV ����
	//Broodwar << "Plan() : " << plan() << endl;
	Commander::getInstance()->addUnitSchedule(UnitTypes::Terran_SCV, plan());
}

void SCVMgr::infiltration(){

	if (spySCV == nullptr){
		return;
	}

	if (spySCV->getState() == SCV::STATE::SPY_WAIT && !spyPlan.getRoute().empty()){
		BWTA::BaseLocation* next = spyPlan.getNextRoute();
		spySCV->orderSpy(next->getTilePosition());
		spyPlan.removeRoute();
	}
}

void SCVMgr::trainSpy(){
	spySCV = getWorker(true);
	spySCV->orderSpyWait();

	BWTA::BaseLocation* base = mapInfo->getNearStartBaseLocation();
	spyPlan.addRoute(base);
	
	for (int i=0;i<2;i++){
		base = mapInfo->getNearLocation(base, mapInfo->getOtherStartBases());
		spyPlan.addRoute(base);
	}
}

Unit SCVMgr::getIdleMineral(){

	auto& territory = mapInfo->getMyTerritory();
	Unit re = nullptr;
	int minCount = 100;

	for (auto& t : territory){
		Unitset minerals = t->getMinerals();

		for (auto m : minerals){
			if (resourceCount[m] < minCount){
				minCount = resourceCount[m];
				re = m;
			}
		}
	}

	return re;
}

Unit SCVMgr::getIdleGas(){

	Unit re = nullptr;

	auto gases = me->getBuildings(UnitTypes::Terran_Refinery);
	for (auto g : gases){
		if (resourceCount[g->getSelf()] < 3){
			return g->getSelf();
		}
	}

	return re;
}

SCV* SCVMgr::getWorker(bool except){
	
	SCV* re = nullptr;

	for (auto iter = mineralSCV.begin(); iter != mineralSCV.end(); ++iter){
		if ((*iter)->getState() == SCV::STATE::IDLE || (*iter)->getState() == SCV::STATE::MINERAL){
			re = (*iter);

			if (except){
				resourceCount[(*iter)->getSelf()]--;
				mineralSCV.erase(iter);				
			}
			
			break;
		}
	}

	/*
	for (auto scv : mineralSCV){
		if (scv->getState() == SCV::STATE::IDLE || scv->getState() == SCV::STATE::MINERAL){
			if (except)
				

			return scv;
		}
	}
	*/

	return re;
}

bool SCVMgr::getProperBasicWork(SCV* scv){

	// Gas
	Unit target = getIdleGas();
	if (target != nullptr){
		scv->orderGas(target);
		resourceCount[target]++;
		gasSCV.push_back(scv);

		return true;
	}

	// Mineral
	target = getIdleMineral();
	if (target != nullptr){
		scv->orderMineral(target);
		resourceCount[target]++;
		mineralSCV.push_back(scv);

		return true;
	}
	

	return false;
}
