#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include <queue>
#include <deque>

#include "me.h"
#include "MapInfo.h"
#include "UtilMgr.h"
#include "SCVMgr.h"
#include "StartBuild.h"
#include "startStrategy.h"

using namespace std;
using namespace BWAPI;
using namespace Filter;

class structWork{
	UnitType target;
	SCV* builder;
	TilePosition pos;
	int startFrame;
	bool retry;

public:
	structWork(UnitType _target, TilePosition _pos, int _startFrame, bool _retry) : target(_target), pos(_pos), startFrame(_startFrame), retry(_retry){

	};

	const UnitType& getTarget(){
		return target;
	}

	SCV* getBuilder(){
		return builder;
	}

	void setBuilder(SCV* builder){
		this->builder = builder;
	}

	const TilePosition& getTilePosition(){
		return pos;
	}

	void setTilePosition(TilePosition pos){
		this->pos = pos;
	}

	const int& getStartFrame(){
		return startFrame;
	}

	bool& getRetry(){
		return retry;
	}
};

class Commander
{
	me* me;
	UtilMgr* util;
	SCVMgr* scvMgr;

	deque<structWork*> structSchedule;
	deque<UnitType> unitSchedule;

	vector<structWork*> progressSchedule;

	int delayMineral;
	int delayGas;
	int delayFrame;

	//
	StartBuild test;
	startStrategy test2;

	Commander();
	~Commander();

public:
	static Commander* getInstance();

	// OnFunc
	void init();
	void start();
	void show();
	void run();
	void structureCreate(Unit unit);
	void structureComplete(Unit unit);
	void unitCreate(Unit unit);
	void unitComplete(Unit unit);

	// Build Func
	bool build(Unit Builder, UnitType target, TilePosition pos);
	bool autoBuild(UnitType target, int center = 0);
	bool canCreate(UnitType target);
	
	//
	int getUnitScheduleCount(UnitType target);
	void runUnitSchedule();
	


	// Schedule Func	
	void addUnitSchedule(UnitType target, int count = 1, bool hurry = false);
	void addStructureSchedule(UnitType target, TilePosition pos, bool retry, int count = 1, bool hurry = false);
	void runSchedule();

	/*
	void addStructureSchedule(UnitType target, TilePosition pos, bool retry, int count = 1, bool hurry = false);
	void addSchedule(UnitType unit, bool hurry = false);
	void runSchedule(deque<UnitType> &schedule);
	void runProgressSchedule();
	*/
};

