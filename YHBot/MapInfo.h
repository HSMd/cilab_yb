#pragma once

#include <BWAPI.h>
#include <BWTA.h>
#include <vector>

#include "UtilMgr.h"

using namespace BWAPI;
using namespace std;

class MapInfo
{
	UtilMgr* util;

	int dir;
	BWTA::BaseLocation* myStartBase;
	BWTA::BaseLocation* oppStartBase;
	set<BWTA::BaseLocation*> otherStartBases;
	set<BWTA::BaseLocation*> otherBases;

	vector<BWTA::BaseLocation*> myTerritory;

	MapInfo();
	~MapInfo();

public:
	static MapInfo* getInstance();	

	void init();
	bool comp(BWTA::BaseLocation* a, BWTA::BaseLocation* b);
	int getDir();

	BWTA::BaseLocation* getMyStartBase();
	BWTA::BaseLocation* getOppStartBase();
	set<BWTA::BaseLocation*> getOtherStartBases();
	set<BWTA::BaseLocation*> getOtherBases();

	BWTA::BaseLocation* getNearLocation(BWTA::BaseLocation* src, set<BWTA::BaseLocation*>& dst);
	BWTA::BaseLocation* getNearStartBaseLocation();
	BWTA::BaseLocation* getNearBaseLocation();

	vector<BWTA::BaseLocation*>& getMyTerritory();
	void addTerritory(BWTA::BaseLocation* base);
};

