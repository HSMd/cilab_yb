#include "me.h"

me::me(){
}


me::~me()
{
	
}

void me::init(){
	//addTerritory(mapInfo->getMyStartBase());
}

me* me::getInstance()
{
	static me instance;
	return &instance;
}

BWAPI::Player me::player()
{
	return BWAPI::Broodwar->self();
}

void me::addUnit(BWAPI::Unit unit)
{
	if (unit->getType() == UnitTypes::Terran_SCV){
		units[unit->getType()].push_back(new SCV(unit));
	}
	else{
		//
	}
	
}

void me::addUnitImmediately(BWAPI::Unit unit){

	if (unit->getType() == UnitTypes::Terran_SCV){
		createUnits[unit->getType()].push_back(new SCV(unit));
	}
	else{
		//
	}
}

void me::addBuilding(BWAPI::Unit building)
{
	buildings[building->getType()].push_back(new Structure(building));
}

void me::addBuildingImmediately(BWAPI::Unit building){
	createBuildings[building->getType()].push_back(new Structure(building));
}

vector<MyUnit*>& me::getUnits(int type)
{
	return units[type];
}

vector<MyUnit*>& me::getUnitImmediatelyUnit(int type){
	return createUnits[type];
}

vector<MyUnit*>& me::getUnitImmediatelyBuilding(int type){
	return createBuildings[type];
}

vector<MyUnit*>& me::getBuildings(int type)
{
	return buildings[type];
}

/*
BWTA::BaseLocation* me::getMyStartLocation(){
	//return mapInfo->getMyStartBase();
	return nullptr;
}


vector<BWTA::BaseLocation*>& me::getMyTerritory(){
	return myTerritory;
}

void me::addTerritory(BWTA::BaseLocation* base){

	auto& mienrals = base->getMinerals();
	

	myTerritory.push_back(base);
}
*/