#include "MyUnit.h"
#include "UtilMgr.h"

auto util = UtilMgr::getInstance();

MyUnit::MyUnit(Unit _self){
	self = _self;
}

MyUnit::~MyUnit(){

}

void MyUnit::run(){

}

// SCV

SCV::SCV(Unit _self) : MyUnit(_self){
	state = STATE::IDLE;
}

SCV::~SCV(){

}

void SCV::run(){
	switch (state){
	case STATE::IDLE:
	case STATE::SPY_WAIT:
		break;

	case STATE::MINERAL:
	case STATE::GAS:
		if (self->isIdle()){
			self->gather(target);
		}
		else if (self->isCarryingMinerals() || self->isCarryingGas()){
			self->returnCargo();
		}

		break;

	case STATE::BUILD:
		if (self->isMoving()){
			if (util->isAlmostReachedDest(util->tile2pos(targetPos), self->getPosition(), BWAPI::TILEPOSITION_SCALE * 5, BWAPI::TILEPOSITION_SCALE * 5)){
				self->build(targetType, targetPos);
				state = STATE::MINERAL;
			}
		}
		else{
			self->move(util->tile2pos(targetPos));
		}

		break;

	case STATE::SPY:
		if (self->isMoving()){
			if (util->isAlmostReachedDest(util->tile2pos(targetPos), self->getPosition(), BWAPI::TILEPOSITION_SCALE * 5, BWAPI::TILEPOSITION_SCALE * 5)){
				state = SPY_WAIT;
			}
		}
		else{
			self->move(util->tile2pos(targetPos));
		}

		break;

	default:
		break;
	}
}