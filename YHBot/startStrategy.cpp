#include "startStrategy.h"
#include "MapInfo.h"
#include "SCVMgr.h"

startStrategy::startStrategy()
{
	flag = false;
}


startStrategy::~startStrategy()
{
}


void startStrategy::run(){
	MapInfo* mapInfo = MapInfo::getInstance();
	SCVMgr* scvMgr = SCVMgr::getInstance();
	me* me = me::getInstance();

	if (me->getUnits(UnitTypes::Terran_SCV).size() == 9 && !flag){
		scvMgr->trainSpy();
		flag = true;
	}
}
