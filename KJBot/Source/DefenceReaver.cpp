#include "DefenceReaver.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

DefenceReaver::DefenceReaver(DEFENSE_TYPE type) : DefenceEvent(type)
{	
	
}

DefenceReaver::~DefenceReaver()
{

}

void DefenceReaver::chkFire(){

	auto shuttleCount = info->map->getCloseCount(UnitTypes::Protoss_Shuttle);
	auto reaverCount = info->map->getCloseCount(UnitTypes::Protoss_Reaver);
	if (reaverCount > 0 || (shuttleCount > 0 && reaverCount > 0))
	{
		fire = true;
	}
}

void DefenceReaver::update(){
	//sample
	auto shuttleCount = info->map->getCloseCount(UnitTypes::Protoss_Shuttle);
	auto reaverCount = info->map->getCloseCount(UnitTypes::Protoss_Reaver);

	if (!state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
		
	}
	else{
		if (shuttleCount == 1)
		{
			if (reaverCount > 0)
			{

			}
			else
			{

			}
		}
		else
		{

		}
	}
	
	/*
	auto probeCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Probe);

	if (probeCount == 1){
		getUnits(UnitTypes::Terran_Marine, 2);

		if (defenceUnits[UnitTypes::Terran_Marine].size() == 0){
			getUnits(UnitTypes::Terran_SCV, probeCount);
		}
		else{
			eraseUnits(UnitTypes::Terran_SCV);
		}
	}
	else{
		getUnits(UnitTypes::Terran_Marine, 2);
		getUnits(UnitTypes::Terran_SCV, probeCount);
	}
	*/
}

void DefenceReaver::run()
{	
	
	chkFire();
	turnOff();
	if (!fire){
		return;
	}	

	update();
	act();
}

void DefenceReaver::act(){

	auto& close = info->map->getCloseEnemy();
	Unit attackTarget = nullptr;
	for (auto enemy = close.begin(); enemy != close.end();)
	{
		if ((*enemy)->getType() != UnitTypes::Protoss_Shuttle || (*enemy)->getType() != UnitTypes::Protoss_Reaver)
		{
			enemy = close.erase(enemy);
		}
		else{
			enemy++;
		}
	}
		
	// Ÿ�� ����.
	for (auto enemy : close){
		
		if (attackTarget == nullptr){
			attackTarget = enemy;
		}
		else{
			if (enemy->getHitPoints() < attackTarget->getHitPoints()){
				attackTarget = enemy;
			}
		}
	}

	if (attackTarget == nullptr){
		return;
	}

	for (auto iter : defenceUnits){
		UnitType type = iter.first;
		
		for (auto unit : defenceUnits[type]){

			if (!unit->self->exists() || !unit->self->isCompleted()){
				continue;
			}
			if (unit->self->getType() == UnitTypes::Terran_Marine)
			{
				if (attackTarget->getOrderTarget() != unit->self)
				{
					if (!unit->self->isAttacking())
					{
						unit->self->attack(attackTarget);
					}
				}
				else{
					unit->self->move(info->map->getMyTerritorys()[0]->getPosition());
				}
			}
			else if (unit->self->getType() == UnitTypes::Terran_SCV)
			{
				if (defenceUnits[UnitTypes::Terran_Marine].size() > 0)
				{
					eraseUnits(UnitTypes::Terran_SCV);
				}
				else{
					if (!unit->self->isAttacking())
					{
						unit->self->attack(attackTarget);
					}
				}
			}
			/*if (CombatControl::getInstance()->isEnemyUnitComeTrue(unit))
			{
				unit->self->move(info->map->getMyTerritorys()[0]->getPosition());
			}
			else{
				if (!unit->self->isAttackFrame())
				{
					unit->self->attack(attackTarget);
				}
			}*/
		}
	}
}

void DefenceReaver::turnOff(){

	auto zealotCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Zealot);

	if (zealotCount == 0){
		eraseUnits(UnitTypes::Terran_Marine);
		eraseUnits(UnitTypes::Terran_SCV);

		fire = false;
	}	
}