#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "DefenceEvent.h"

using namespace std;


class DefenceZealot2 : public DefenceEvent
{	
	bool closeFind;
	
public:

	DefenceZealot2(DEFENSE_TYPE type);
	~DefenceZealot2();

	//이벤트 시작
	void chkFire();
	//이벤트 종료
	void turnOff();

	//유닛 가져오기(제한두고)
	void update();
	//run
	void run();

	//update후 코드
	void act();

	float TargetValue(UnitType ut);
};

