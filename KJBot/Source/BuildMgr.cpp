#include "BuildMgr.h"
#include "Instance.h"

BuildMgr* BuildMgr::getInstance(){

	static BuildMgr instance;
	return &instance;
}

void BuildMgr::init(){

	//example.init();
	//study.init();
	//oneFacDouble.init();
	//last.init();

	//bacanic.init();
	bacanic2.init();
}

void BuildMgr::show(){

	//example.show();
	//study.show();
	//oneFacDouble.show();
	//last.show();

	//bacanic.show();
	bacanic2.show();
}

void BuildMgr::draw(){

	//example.draw();
	//study.draw();
	//oneFacDouble.draw();
	//last.draw();

	//bacanic.draw();
	bacanic2.draw();
}

void BuildMgr::run(){

	//example.run();
	//study.run();
	//oneFacDouble.run();
	//last.run();

	//bacanic.run();
	bacanic2.run();
}