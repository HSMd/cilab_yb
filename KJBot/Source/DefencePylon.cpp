#include "DefencePylon.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

DefencePylon::DefencePylon(DEFENSE_TYPE type) : DefenceEvent(type)
{	
	
}

DefencePylon::~DefencePylon()
{

}

void DefencePylon::chkFire(){

	auto pylonCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Pylon);
	if (0 < pylonCount){
		fire = true;
	}
}

void DefencePylon::update(){

	auto pylonCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Pylon);
	auto photoCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Photon_Cannon);

	if (1 == pylonCount){
		getUnits(UnitTypes::Terran_Marine);
		int scvCount = 3 - defenceUnits[UnitTypes::Terran_Marine].size();

		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
			eraseUnits(UnitTypes::Terran_SCV);
		}
		else{
			getUnits(UnitTypes::Terran_SCV, scvCount);
		}		

		if (0 < photoCount){
			if (info->self->getUnitCount(UnitTypes::Terran_Marine) < 3 && scheduler->canAddUnitType(UnitTypes::Terran_Marine)){
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Marine), Scheduler::BUILD_SPEED::NORMAL);
			}

			getUnits(UnitTypes::Terran_SCV, scvCount + 3);
		}		
	}
	else{
		getUnits(UnitTypes::Terran_Marine);
		int scvCount = 4 - defenceUnits[UnitTypes::Terran_Marine].size();
		getUnits(UnitTypes::Terran_SCV, scvCount);

		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
			eraseUnits(UnitTypes::Terran_SCV);
		}

		if (0 < photoCount){
			if (info->self->getUnitCount(UnitTypes::Terran_Marine) < 4 && scheduler->canAddUnitType(UnitTypes::Terran_Marine)){
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Marine), Scheduler::BUILD_SPEED::NORMAL);
			}

			getUnits(UnitTypes::Terran_SCV, scvCount + 3);
		}
	}
}

void DefencePylon::run()
{	
	chkFire();
	turnOff();
	if (!fire){
		return;
	}	

	update();
	act();
}

void DefencePylon::act(){

	auto& close = info->map->getMyTerritorys()[0]->getCloseEnemy();
	Unit attackTarget = nullptr;

	// Ÿ�� ����.
	for (auto enemy : close){
		if (enemy->getType() == UnitTypes::Protoss_Pylon){
			if (attackTarget == nullptr){
				attackTarget = enemy;
			}
			else{
				//
				if (getDistance(posMgr->getTilePosition(POS::SIM1_SUPPLY2) + TilePosition(2, 1), enemy->getTilePosition()) < getDistance(posMgr->getTilePosition(POS::SIM1_SUPPLY2) + TilePosition(2, 1), attackTarget->getTilePosition())){
					attackTarget = enemy;
				}
			}
		}
		else if (enemy->getType() == UnitTypes::Protoss_Photon_Cannon){
			if (attackTarget == nullptr){
				attackTarget = enemy;
			}
			else{
				if (attackTarget->getType() == UnitTypes::Protoss_Pylon){
					attackTarget = enemy;
				}

				if (enemy->getHitPoints() < attackTarget->getHitPoints()){
					attackTarget = enemy;
				}
			}
		}
	}

	if (attackTarget == nullptr){
		return;
	}

	for (auto iter : defenceUnits){
		UnitType type = iter.first;
		
		for (auto unit : defenceUnits[type]){

			if (!unit->self->exists() || !unit->self->isCompleted()){
				continue;
			}

			if(!unit->self->isAttackFrame())
				unit->self->attack(attackTarget);
		}
	}
}

void DefencePylon::turnOff(){

	auto pylonCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Pylon);

	if (pylonCount == 0){
		eraseUnits(UnitTypes::Terran_Marine);
		eraseUnits(UnitTypes::Terran_SCV);

		fire = false;
	}	
}