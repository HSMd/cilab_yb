#include "OneFacDouble.h"
#include "Instance.h"
#include "Work.h"
#include "Utility.h"

void OneFacDouble::init(){

	auto front = info->map->getCandidateLocation();
	Territory* territory = new Territory(front);
	info->map->addMyTerritory(territory);

	performer = nullptr;
	spare = nullptr;
	frameCount = 0;
	
	progress = BUILD_START;
}

void OneFacDouble::show(){

}

void OneFacDouble::draw(){

}

void OneFacDouble::run(){

	// 빌드진행.
	buildProcess();

	// 자동 유닛.
	autoUnit();	

	// 서치.
	search();

	// 앞마당 확장
	expand();

	// 빌드 진행.
	runBuild();

	// SCV 생산.
	scvCreate();	

	// 건물 옮기기.
	move();
}

void OneFacDouble::autoUnit(){

	if (1 <= info->self->getBuildingCount(UnitTypes::Terran_Factory)){
		flags[AUTO_TANK] = true;
	}

	if (4 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		flags[AUTO_SUPPLY] = true;
	}

	if (flags[BUILD_MID_MOVE]){
		if (flags[INVASION]){
			flags[AUTO_MARINE] = true;
		}
		else if(4 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks), true){
			flags[AUTO_MARINE] = true;
		}		
	}

	if (flags[AUTO_TANK]){
		if (scheduler->canAddUnitType(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode), Scheduler::BUILD_SPEED::NORMAL);
		}
	}

	if (flags[AUTO_MARINE]){

		int marineCount = info->self->getUnitCount(UnitTypes::Terran_Marine, true);
		int medicCount = info->self->getUnitCount(UnitTypes::Terran_Medic, true);

		if (7 <= marineCount && medicCount * 4 < marineCount && scheduler->canAddUnitType(UnitTypes::Terran_Medic)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Medic, 2), Scheduler::BUILD_SPEED::SLOW);
		}

		if (scheduler->canAddUnitType(UnitTypes::Terran_Marine)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Marine), Scheduler::BUILD_SPEED::SLOW);
		}
	}

	if (flags[AUTO_SUPPLY]){
		if (Broodwar->self()->supplyTotal() < 200 * 2 && frameCount + 900 < bw->getFrameCount()){

			if (info->self->getBuildingCount(UnitTypes::Terran_Barracks) < 4){
				if (scheduler->getSupply() <= 5 * 2){
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
					frameCount = bw->getFrameCount();
				}
			}
			else{
				if (scheduler->getSupply() <= 8 * 2){
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
					frameCount = bw->getFrameCount();
				}
			}			
		}
	}
}

void OneFacDouble::search(){

	// 정찰.
	if (!flags[SEARCH] && 11 <= info->self->getUnitCount(UnitTypes::Terran_SCV) && 70 <= bw->self()->minerals()){

		auto worker = info->map->getMyTerritorys()[0]->getWorker(true);
		scouting->setSpy(worker);

		if (worker != nullptr)
			flags[SEARCH] = true;
	}
}

void OneFacDouble::scvCreate(){

	auto& cnters = info->self->getBuildings(UnitTypes::Terran_Command_Center);
	auto& scvs = info->self->getImmediatelyUnit(UnitTypes::Terran_SCV);

	// SCV 뽑기.
	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center)){
		for (auto territory : info->map->getMyTerritorys()){
			int require = territory->getRequireSCVCount();
			auto cnter = territory->getCmdCenter()->self;

			if (1 <= require && scheduler->getReadyTrain(cnter) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
				cnter->train(UnitTypes::Terran_SCV);
		}
	}
	else{		
		if (progress == BUILD_START){
			if (scvs.size() == 15){
				if (0 < info->self->getBuildingCount(UnitTypes::Terran_Command_Center, true)){
					/*
					if (flags[SCV_CONTROL_1]){
						if (0 < info->self->getUnitCount(UnitTypes::Terran_Marine, true) && scheduler->getReadyTrain(cnters.front()->self) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
							cnters.front()->self->train(UnitTypes::Terran_SCV);
					}
					else{
						if (scheduler->getReadyTrain(cnters.front()->self) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
							cnters.front()->self->train(UnitTypes::Terran_SCV);
					}					
					*/

					if (scheduler->getReadyTrain(cnters.front()->self) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			else if (scvs.size() == 16){

				if (flags[SCV_CONTROL_1]){
					if (0 < info->self->getUnitCount(UnitTypes::Terran_Marine, true) && scheduler->getReadyTrain(cnters.front()->self) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
				else if (flags[SCV_CONTROL_2]){
					if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true)){
						if (scheduler->getReadyTrain(cnters.front()->self) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
							cnters.front()->self->train(UnitTypes::Terran_SCV);
					}
					else if ((1 <= scheduler->getUnitTypeRunCount(UnitTypes::Terran_Supply_Depot) && 150 <= scheduler->getMineral()))
						if (scheduler->getReadyTrain(cnters.front()->self) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
							cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
				else{
					if (scheduler->getReadyTrain(cnters.front()->self) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			else{
				if (scheduler->getReadyTrain(cnters.front()->self) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
					cnters.front()->self->train(UnitTypes::Terran_SCV);
			}
		}
	}
}

void OneFacDouble::expand(){

	if (!flags[ADD_CENTER] && 2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center, true)){
		info->map->getMyTerritorys().back()->init(info->map->getMyTerritorys().back()->getBase());
		info->map->getMyTerritorys().back()->setCmdCenter(info->self->getImmediatelyBuilding(UnitTypes::Terran_Command_Center).back());
		info->map->getMyTerritorys().back()->setFitScvCount(7);

		info->map->getMyTerritorys().back()->getReady() = false;

		flags[ADD_CENTER] = true;
	}

	if (spare == nullptr){
		spare = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::FRONT_BUNKER));
	}
	else{
		if (!flags[IMM_SPARE]){

			if (spare->state == MyUnit::STATE::MINERAL || spare->state == MyUnit::STATE::IDLE){
				info->map->getMyTerritorys().front()->delWorker(spare);
				info->map->getMyTerritorys().back()->addScv(spare);

				flags[IMM_SPARE] = true;
			}
		}
		
		if (spare->self->isCarryingMinerals() && info->self->getBuildingCount(UnitTypes::Terran_Command_Center) < 2){
			spare->self->move(info->map->getMyTerritorys().back()->getPosition());
		}
	}	

	if (flags[ADD_CENTER] && 85 <= getProgress(info->self->getImmediatelyBuilding(UnitTypes::Terran_Command_Center).back()->self)){
		
		if (!flags[IMM_PERFORMER]){

			info->map->getMyTerritorys().front()->delWorker(performer);
			info->map->getMyTerritorys().back()->addScv(performer);

			flags[IMM_PERFORMER] = true;
		}

		info->map->getMyTerritorys().back()->getReady() = true;
		info->map->getMyTerritorys().front()->immigrant(info->map->getMyTerritorys().back(), Territory::WORK::OVER);
	}	
}

void OneFacDouble::runBuild(){

	// 저장된 빌드들을 수행.
	for (auto iter = builds.begin(); iter != builds.end(); ++iter){
		BuildWork& build = (*iter);
		auto work = build.getWork();

		if (!build.getFire() && build.run()){

			if (build.getWork()->pos == posMgr->getTilePosition(POS::FIRST_SUPPLY)){
				auto performer = info->map->getMyTerritorys()[0]->getLastWorker();
				work->setPerformer(performer);
			}

			if (build.getWork()->pos == info->map->getMyTerritorys().back()->getTilePosition()){
				performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::FRONT_BARRACK));
				work->setPerformer(performer);
			}

			scheduler->addSchedule(build.getWork(), build.getSpeed());

			iter = builds.erase(iter);
			iter--;
		}
	}
}

void OneFacDouble::buildProcess(){

	if (0 < info->self->getBuildingCount(UnitTypes::Terran_Factory, true)){
		progress = BUILD_MID;
	}

	if (progress == BUILD_START){
		buildStart();
	}
	else if (progress == BUILD_MID){
		buildMid();
	}
}

void OneFacDouble::buildStart(){

	if (!flags[BUILD_START]){

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 8, false, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::FIRST_SUPPLY)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 10, true, UnitTypes::Terran_Barracks, posMgr->getTilePosition(POS::FRONT_BARRACK), nullptr, 60));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 1, true, UnitTypes::Terran_Command_Center, info->map->getMyTerritorys().back()->getTilePosition(), nullptr, 80));

		flags[BUILD_START] = true;
	}

	if (!flags[BUILD_START_FIND] && !flags[BUILD_START_NOT]){

		if (scouting->getSearchCount() == 1){
			if (scouting->isFind() && 50 <= scheduler->getMineral()){
				flags[BUILD_START_FIND] = true;
			}
		}
		else if (1 < scouting->getSearchCount()){
			flags[BUILD_START_NOT] = true;
		}
	}

	if (!flags[BUILD_START_FIND_RUN] && flags[BUILD_START_FIND]){
		if (0 < info->enemy->getUnitCount(UnitTypes::Protoss_Zealot) || 2 <= info->enemy->getUnitCount(UnitTypes::Protoss_Gateway)){
		//if (false){
			bw << "First Supply" << endl;
			flags[SCV_CONTROL_1] = true;

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, 2, true, UnitTypes::Terran_Refinery, TilePosition(0, 0), nullptr, 50));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery, 1, true, UnitTypes::Terran_Marine, 1));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery, 1, true, UnitTypes::Terran_Bunker, posMgr->getTilePosition(POS::FRONT_BUNKER), nullptr));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Marine, 3));
		}
		else{
			bw << "First Refinery" << endl;
			flags[SCV_CONTROL_2] = true;

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 16, true, UnitTypes::Terran_Refinery, TilePosition(0, 0), nullptr, 30));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery, 1, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true), nullptr, 50));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, 2, true, UnitTypes::Terran_Bunker, posMgr->getTilePosition(POS::FRONT_BUNKER), nullptr, 60));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, 2, false, UnitTypes::Terran_Marine, 1));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Marine, 3));
		}

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Bunker, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 50));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Bunker, 1, true, UnitTypes::Terran_Machine_Shop));

		flags[BUILD_START_FIND_RUN] = true;
	}

	if (!flags[BUILD_START_NOT_RUN] && flags[BUILD_START_NOT]){

		bw << "First Supply" << endl;

		flags[SCV_CONTROL_1] = true;

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, 2, true, UnitTypes::Terran_Refinery, TilePosition(0, 0), nullptr, 50));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery, 1, true, UnitTypes::Terran_Marine, 1));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery, 1, true, UnitTypes::Terran_Bunker, posMgr->getTilePosition(POS::FRONT_BUNKER), nullptr));

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Bunker, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 50));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Bunker, 1, true, UnitTypes::Terran_Machine_Shop));

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Marine, 3));

		flags[BUILD_START_NOT_RUN] = true;
	}
}

void OneFacDouble::buildMid(){

	int closeDragoonCount = info->map->getCloseCount(UnitTypes::Protoss_Dragoon);

	if (1 < closeDragoonCount){
		flags[INVASION] = true;
	}

	if (!flags[BUILD_MID]){

		bw << "BUILD_MID" << endl;

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Academy, posMgr->getTilePosition(POS::ACADEMY), nullptr, 60));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 2, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));

		builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, TechTypes::Tank_Siege_Mode));

		flags[BUILD_MID] = true;
	}

	// 드라군 견제가 온다면 컴셋을 짓지 않는다.
	if (3 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		if (!flags[INVASION]){
			if (!flags[BUILD_MID_NO_INVASION]){

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 3, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station));

				flags[BUILD_MID_NO_INVASION] = true;
			}
		}
		else{
			if (!flags[BUILD_MID_INVASION]){

				if (1 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode, true)){
					builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 3, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));

					flags[BUILD_MID_INVASION] = true;
				}				
			}
		}
	}

	if (!flags[BUILD_MID_MOVE] && 3 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		auto& firstBrk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
		firstBrk->targetPos = posMgr->getProperPosition(UnitTypes::Terran_Factory, true);

		moveBuildings.push_back(firstBrk);
		flags[BUILD_MID_MOVE] = true;
	}

	if (!flags[BUILD_MID_STIM_PACKS] && state->getCombatFlag(COMBAT_FLAG::SIEGE_MODE) && info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode, true)){
		builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Barracks, 4, true, TechTypes::Stim_Packs));
		flags[BUILD_MID_STIM_PACKS] = true;
	}

	if (!flags[BUILD_MID_U_238_SHELLS]){
		if (2 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode, true) && state->getCombatFlag(COMBAT_FLAG::STIM_PACKS)){
			builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Barracks, 4, true, UpgradeTypes::U_238_Shells, 1, nullptr, Scheduler::BUILD_SPEED::FAST));
			flags[BUILD_MID_U_238_SHELLS] = true;
		}		
	}
}

void OneFacDouble::move(){

	for (auto iter = moveBuildings.begin(); iter != moveBuildings.end(); iter++){

		auto unit = (*iter);

		if (unit->self->isLifted()){
			if (5 < getDistance(unit->getTilePosition(), unit->targetPos)){
				unit->self->move((Position)unit->targetPos);
			}
			else{
				if (unit->self->isIdle())
					unit->self->land(unit->targetPos);
			}
		}
		else{
			if (getDistance(unit->getTilePosition(), unit->targetPos) < 5){
				if (unit->self->isIdle()){
					iter = moveBuildings.erase(iter);
					iter--;
				}
			}
			else{
				unit->self->lift();
			}
		}
	}
}