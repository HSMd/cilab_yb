#include "Scout.h"
#include "Utility.h"
#include "Instance.h"

Scout::Scout()
{
	find = false;
	isIn = false;
	canUseData = false;

	point = nullptr;
	target = nullptr;
	enemyFront = nullptr;

	searchCount = 0;
}

Scout::~Scout()
{

}

Scout* Scout::getInstance() {

	static Scout instance;
	return &instance;
}

void Scout::init(){

	// 들르는 순서 정리.
	auto myStartBase = BWTA::getStartLocation(bw->self());
	vector<BaseLocation*> startLocations;
	int maxDis = 0;

	BaseLocation* secondLocation;
	vector<BaseLocation*>::iterator delIter;

	for (auto location : BWTA::getStartLocations()){
		startLocations.push_back(location);
	}

	for (auto iter = startLocations.begin(); iter != startLocations.end(); iter++){
		BaseLocation* location = (*iter);

		if (location->getTilePosition() == myStartBase->getTilePosition()){
			iter = startLocations.erase(iter);
			iter--;

			continue;
		}

		auto dis = getDistance(myStartBase, location);
		if (maxDis < dis){
			maxDis = dis;
			delIter = iter;
			secondLocation = location;
		}
	}

	startLocations.erase(delIter);

	startBases.push_back(startLocations.front());
	startBases.push_back(secondLocation);
	startBases.push_back(startLocations.back());
}

void Scout::show(){

	//drawUnitText(performer->self, "Spy");
}

void Scout::draw(){

	for (auto point : chkPoints){

		if (!point->chk){
			bw->drawCircleMap(point->pos, 7, Colors::Purple, false);
		}
		else{
			bw->drawCircleMap(point->pos, 7, Colors::Purple, true);
		}

	}

	if (target != nullptr){
		auto chkPos = BWTA::getNearestChokepoint(target->getTilePosition())->getCenter();
		bw->drawCircleMap(chkPos, 60, Colors::Green, false);
	}
}

void Scout::run(){

	if (performer == nullptr){
		return;
	}

	if (performer->getType() == UnitTypes::Terran_SCV)
		scvScout();
}

BaseLocation* Scout::getCloseBase(TilePosition pos, int term){

	for (auto base : BWTA::getStartLocations()){
		if (info->map->getMyStartBase() == base)
			continue;

		if (getDistance(pos, base->getTilePosition()) < term){

			return base;
		}
	}

	return nullptr;
}

BaseLocation* Scout::chkEnemyStart(UnitType type){

	for (auto base : BWTA::getStartLocations()){
		if (info->map->getMyStartBase() == base)
			continue;

		for (auto unit : info->enemy->getBuildings(type)){
			if (getDistance(unit->getTilePosition(), base->getTilePosition()) < 35){
				// 건물을 찾음 ( 치트키던지 아니면 정찰이던지 - 앞마당에 있는 애 제외하고.)

				auto closeBase = getCloseBase(unit->getTilePosition(), 35);
				if (closeBase != nullptr){
					return closeBase;
				}
			}
		}
	}

	return nullptr;
}

void Scout::scvScout(){

	if (!performer->self->exists()){
		auto closeBase = getCloseBase(performer->getTilePosition());
		if (closeBase != nullptr){
			info->map->setEnemyStartBase(target);
		}

		return;
	}

	if (!find){
		/* 우리 본진 확인하고 나가고 플때,
		// 적 스타팅 위치 모를 때,
		if (!s_flags[judge_myhouse]){//정찰 시작시_ 내부 본진확인용
		innersearch();
		}
		else{
		}
		*/

		// 정찰 돌리는 부분.
		if (target == nullptr){
			changeTarget();
		}

		if (8 < getDistance(target->getTilePosition(), performer->getTilePosition())){
			performer->self->move(target->getPosition());

			if (checkChokepoint(target))
			{

				find = true;
				info->map->setEnemyStartBase(target);
				findEnemyStart(target);
				point = getNextPoint();
				s_flags[ENTER_BLOCK] = true;
				s_flags[SEARCHTIME_1_ON] = true;
				//bw << "SEARCHTIME_1_ON" << endl;  //♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠
				return;
			}
		}
		else{
			changeTarget();
		}
		// 정찰 발견하는 부분.
		auto nexusFind = chkEnemyStart(UnitTypes::Protoss_Nexus);
		auto gasFind = chkEnemyStart(UnitTypes::Protoss_Assimilator);

		if (nexusFind != nullptr){
			find = true;
			target = nexusFind;
		}

		if (gasFind != nullptr){
			find = true;
			target = gasFind;
		}

		if (find){
			info->map->setEnemyStartBase(target);
			findEnemyStart(target);
			point = getNextPoint();
			s_flags[SEARCHTIME_1_ON] = true;
			//bw << "SEARCHTIME_1_ON" << endl;  //♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠
		}
	}
	else{
		checkFrontNexus(); //앞마당체크
		// 정찰이 끝났을 경우
		if (!s_flags[ENTER_BLOCK]){
			if (point == nullptr){
				s_flags[SEARCHTIME_1_OFF] = true;
				//bw << "SEARCHTIME_1_OFF" << endl;  //♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠
				centerRotate();
				return;
			}

			// 회전시키는 부분.
			if (!isIn && 4 < getDistance(target->getTilePosition(), performer->getTilePosition())){
				performer->self->move(target->getPosition());
				isIn = true;
			}
			else{
				if (TILE_SIZE * 4 < getDistance(point->pos, performer->getPosition())){
					performer->self->move(point->pos);
				}
				else{
					point = getNextPoint();
				}
			}
		}

		else{ //입구가 막힌경우
			centerRotate();
			/*
			입구 막혀서 본진 리턴
			info->map->getMyTerritorys()[0]->addScv(performer);
			performer = nullptr;
			return;
			*/
		}
	}
}

bool Scout::checkChokepoint(BaseLocation* base){

	auto chkPos = BWTA::getNearestChokepoint(base->getTilePosition())->getCenter();

	for (auto unit : bw->enemy()->getUnits()){
		if (unit->getType().isBuilding()){
			continue;
		}

		if (unit->getType() == UnitTypes::Protoss_Probe){
			if (getDistance(chkPos, performer->getPosition()) > 250){ // 밖에 쪽으로 한번 움직였다가 
				s_flags[ENTER_PROBE] = true;
				//bw << "check" << endl;
				
			}
			if (s_flags[ENTER_PROBE] && getDistance(chkPos, performer->getPosition()) < 120 && getDistance(chkPos, unit->getPosition()) < 50){ 
				//받은 트루값으로 다시 들어갈려고하는데 또있다 그럼 입구 막는거 확실한거지 프로브로 
				//bw << "real block" << endl;
				return true;
			}
			continue;
			
		}

		if (getDistance(chkPos, unit->getPosition()) < 50){  //입구막힌거 예외 처리 
			return true;
		}
	}

	return false;
}

void Scout::no_Enemymove(BaseLocation* base){

	switch (enemyDirection){
	case 0:
		pos1 = (Position)TilePosition(16, 5);
		pos2 = (Position)TilePosition(4, 18);
		pos3 = (Position)TilePosition(16, 17);
		break;
	case 1:
		pos1 = (Position)TilePosition(112, 4);
		pos2 = (Position)TilePosition(128, 21);
		pos3 = (Position)TilePosition(111, 15);
		break;
	case 2:
		pos1 = (Position)TilePosition(12, 113);
		pos2 = (Position)TilePosition(21, 124);
		pos3 = (Position)TilePosition(12, 124);
		break;
	case 3:
		pos1 = (Position)TilePosition(109, 106);
		pos2 = (Position)TilePosition(122, 106);
		pos3 = (Position)TilePosition(109, 119);
		break;
	}

	s_flags[ROTATE_0] = false;
	s_flags[ROTATE_1] = false;
	s_flags[ROTATE_2] = false;
	s_flags[ROTATE_3] = false;

	//위치로 갔는지 확인
	if (getDistance(base->getPosition(), performer->getPosition()) < 100){
		s_flags[ROTATE_0] = true;
	}
	else if (getDistance(pos1, performer->getPosition()) < 64){
		s_flags[ROTATE_1] = true;
	}
	else if (getDistance(pos2, performer->getPosition()) < 64){
		s_flags[ROTATE_2] = true;
	}
	else if (getDistance(pos3, performer->getPosition()) < 64){
		s_flags[ROTATE_3] = true;
	}

	//다음 위치로 이동
	if (s_flags[ROTATE_0] == true){
		performer->self->rightClick(pos1);
	}
	else if (s_flags[ROTATE_1] == true){
		performer->self->rightClick(pos2);
	}
	else if (s_flags[ROTATE_2] == true){
		performer->self->rightClick(pos3);
	}
	else if (s_flags[ROTATE_3] == true){
		performer->self->rightClick(pos1);
	}
}

void Scout::changeTarget(){

	searchCount++;
	target = startBases.back();
	startBases.pop_back();
}

void Scout::centerRotate(){
	if (!s_flags[JUDGE_M]){  //한바퀴돌고 중간으로 찍기
		performer->self->move((Position)TilePosition(64, 64));
	}
	else{
		if (!s_flags[JUDGE_TIME]){  //가운데 돌리기 용도 

			if (searchCount == 1){
				centerMoveCount = 4;
			}
			else if (searchCount == 2){
				centerMoveCount = 2;
			}
			else{
				centerMoveCount = 1;
			}

			if (centerMoveJudge < centerMoveCount){
				if (centerMoveJudge % 2 == 0){
					performer->self->move((Position)TilePosition(64, 42));
					if (performer->self->getTilePosition().x - TilePosition(64, 42).x < 2 && performer->self->getTilePosition().y - TilePosition(64, 42).y < 2 && performer->self->getTilePosition().x - TilePosition(64, 42).x > -2 && performer->self->getTilePosition().y - TilePosition(64, 42).y > -2 && !s_flags[JUDGE_WATAGATA1]){
						centerMoveJudge++;
						s_flags[JUDGE_WATAGATA1] = true;
						s_flags[JUDGE_WATAGATA2] = false;
					}
				}
				else if (centerMoveJudge % 2 == 1){
					performer->self->move((Position)TilePosition(77, 80));
					if (performer->self->getTilePosition().x - TilePosition(77, 80).x < 1 && performer->self->getTilePosition().y - TilePosition(77, 80).y < 1 && performer->self->getTilePosition().x - TilePosition(77, 80).x > -2 && performer->self->getTilePosition().y - TilePosition(88, 80).y > -2 && !s_flags[JUDGE_WATAGATA2] && s_flags[JUDGE_WATAGATA1]){
						centerMoveJudge++;
						s_flags[JUDGE_WATAGATA2] = true;
						s_flags[JUDGE_WATAGATA1] = false;
					}
				}
			}
			else{
				s_flags[JUDGE_TIME] = true;
			}
		}
		else{
			if (!s_flags[JUDGE_SECONDMOVE]){
				performer->self->move(target->getPosition());
				if (performer->self->getTilePosition().x - TilePosition(target->getPosition()).x < 5 && performer->self->getTilePosition().y - TilePosition(target->getPosition()).y < 5 && performer->self->getTilePosition().x - TilePosition(target->getPosition()).x > -5 && performer->self->getTilePosition().y - TilePosition(target->getPosition()).y > -5){
					s_flags[JUDGE_SECONDMOVE] = true;
				}
			}
			else{
				no_Enemymove(target);  // 다시 본진으로 들어간다. 

				/*info->map->getMyTerritorys()[0]->addScv(performer);
				performer = nullptr;
				return;*/
			}
		}
	}

	//중앙에 왔을때 플래그값 트루로 주기
	if (getDistance(performer->getTilePosition(), TilePosition(64, 64)) < 6){
		s_flags[JUDGE_M] = true;
		s_flags[SEARCHTIME_2_ON] = true;
		//bw << "SEARCHTIME_2_ON" << endl; //♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠♠
		
	}
}

void Scout::endMission(){

	if (performer->getType() == UnitTypes::Terran_SCV){

		int minScvCount = 9999;
		Territory* territory;

		for (auto t : info->map->getMyTerritorys()){
			if (!t->getReady()){
				continue;
			}

			int count = t->getScvCount();

			if (count < minScvCount){
				count = minScvCount;
				territory = t;
			}
		}

		territory->addScv(performer);
		performer = nullptr;
	}
	else{

	}
}

void Scout::findEnemyStart(BaseLocation* base){

	auto enemyFront = BWTA::getNearestBaseLocation(base->getTilePosition());
	enemyDirection = info->map->getEnemyDirection();

	switch (enemyDirection){

	case 0:

		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-170, 300)));
		chkPoints.push_back(new ChkPoint((Position)TilePosition(0, 0)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(350, -170)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(330, 300)));

		if (searchCount == 1){
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-170, 300)));
			chkPoints.push_back(new ChkPoint((Position)TilePosition(0, 0)));
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(350, -170)));
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(330, 300)));
		}

		break;

	case 1:

		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-330, -100)));
		chkPoints.push_back(new ChkPoint((Position)TilePosition(126, 1)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(250, 300)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-350, 300)));

		if (searchCount == 1){
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-330, -100)));
			chkPoints.push_back(new ChkPoint((Position)TilePosition(126, 1)));
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(250, 300)));
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-350, 300)));
		}

		break;

	case 2:

		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(330, 100)));
		chkPoints.push_back(new ChkPoint((Position)TilePosition(1, 124)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-170, -150)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(330, -150)));

		if (searchCount == 1){
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(330, 100)));
			chkPoints.push_back(new ChkPoint((Position)TilePosition(1, 124)));
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-170, -150)));
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(330, -150)));
		}

		break;

	case 3:

		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(250, -330)));
		chkPoints.push_back(new ChkPoint((Position)TilePosition(126, 126)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-400, 170)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-330, -400)));

		if (searchCount == 1){
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(250, -330)));
			chkPoints.push_back(new ChkPoint((Position)TilePosition(126, 126)));
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-400, 170)));
			chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-330, -400)));
		}
		break;
	}



}

ChkPoint* Scout::getNextPoint(){

	if (chkPoints.empty()){
		return nullptr;
	}

	if (point != nullptr){
		point->chk = true;
	}

	for (auto p : chkPoints){
		if (!p->chk){
			return p;
		}
	}

	return nullptr;
}

void Scout::seeEverything(){

	auto chkPoint = BWTA::getNearestChokepoint(target->getTilePosition());
	auto center = TilePosition(128 / 2, 128 / 2);

	if (2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway) || 2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Nexus) ||
		0 < info->enemy->getBuildingCount(UnitTypes::Protoss_Cybernetics_Core)){
	}

	if (info->enemy->getAllUnits().size() == info->enemy->getUnitCount(UnitTypes::Protoss_Probe)){
		bool avoid = false;

		for (auto unit : bw->enemy()->getUnits()){
			if (unit->getType().isBuilding()){
				continue;
			}

			if (getDistance(performer->getTilePosition(), unit->getTilePosition()) < 6){
				avoid = true;
				break;
			}
		}

		if (avoid){
			if (getDistance(performer->getTilePosition(), target->getTilePosition()) <= 21){

				if (getDistance(performer->getPosition(), chkPoint->getCenter()) <= 60){
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), (Position)center));
				}
				else{
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), chkPoint->getCenter()));
				}
			}
			else{
				if (getDistance(center, performer->getTilePosition()) < 15){
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), target->getPosition()));
				}
				else{
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), (Position)center));
				}
			}
		}
		else{
			if (7 < getDistance(performer->getTilePosition(), target->getTilePosition())){
				performer->self->move(target->getPosition());
			}
		}
	}
	else{

		bool avoid = false;

		for (auto unit : bw->enemy()->getUnits()){
			if (unit->getType().isBuilding()){
				continue;
			}

			if (getDistance(performer->getTilePosition(), unit->getTilePosition()) < 13){
				avoid = true;
				break;
			}
		}

		if (avoid){
			if (getDistance(performer->getTilePosition(), target->getTilePosition()) <= 21){
				if (getDistance(performer->getPosition(), chkPoint->getCenter()) <= 60){
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), (Position)center));
				}
				else{
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), chkPoint->getCenter()));
				}
			}
			else{
				if (getDistance(center, performer->getTilePosition()) < 15){
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), target->getPosition()));
				}
				else{
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), (Position)center));
				}
			}
		}
		else{
			if (7 < getDistance(performer->getTilePosition(), target->getTilePosition())){
				performer->self->move(target->getPosition());
			}
		}
	}
}

void Scout::hinder(){

	bool avoid = false;
	auto chkPoint = BWTA::getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition());

	for (auto unit : bw->enemy()->getUnits()){
		if (unit->getType().isBuilding()){
			continue;
		}

		if (getDistance(unit->getPosition(), performer->getPosition()) < TILEPOSITION_SCALE * 6){
			avoid = true;
			break;
		}
	}

	if (!avoid){
		Unit attackTarget = nullptr;
		int minLife = 9999;

		for (auto unit : bw->enemy()->getUnits()){

			if (!unit->getType().isBuilding()){
				continue;
			}

			if (attackTarget == nullptr){
				attackTarget = unit;
			}
			else{
				if (attackTarget->getType() != UnitTypes::Protoss_Pylon){
					attackTarget = unit;
					minLife = attackTarget->getHitPoints();
				}
				else{
					if (unit->getHitPoints() < minLife){
						attackTarget = unit;
						minLife = attackTarget->getHitPoints();
					}
				}
			}
		}

		performer->self->attack(attackTarget);
	}
	else{
		if (getDistance(chkPoint->getCenter(), performer->getPosition()) < 150){

			endMission();
			return;
		}

		performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), chkPoint->getCenter()));
	}
}

void Scout::InnerSearch(){
	myDirection = info->map->getMyDirection();
	if (myDirection == 0){
		if (!s_flags[JUDGE_MYHOUSE2]){
			performer->self->move((Position)TilePosition(20, 10));
			if (performer->self->getTilePosition().x - TilePosition(20, 10).x < 2 && performer->self->getTilePosition().y - TilePosition(20, 10).y < 2 && performer->self->getTilePosition().x - TilePosition(20, 10).x > -2 && performer->self->getTilePosition().y - TilePosition(20, 10).y > -2){
				s_flags[JUDGE_MYHOUSE2] = true;
			}
		}
		else{
			performer->self->move((Position)TilePosition(23, 17));
			if (performer->self->getTilePosition().x - TilePosition(23, 17).x < 2 && performer->self->getTilePosition().y - TilePosition(23, 17).y < 2 && performer->self->getTilePosition().x - TilePosition(23, 17).x > -2 && performer->self->getTilePosition().y - TilePosition(23, 17).y > -2){
				s_flags[JUDGE_MYHOUSE] = true;
			}
		}
	}
	else if (myDirection == 1){
		if (!s_flags[JUDGE_MYHOUSE2]){
			performer->self->move((Position)TilePosition(123, 26));
			if (performer->self->getTilePosition().x - TilePosition(123, 26).x < 2 && performer->self->getTilePosition().y - TilePosition(123, 26).y < 2 && performer->self->getTilePosition().x - TilePosition(123, 26).x > -2 && performer->self->getTilePosition().y - TilePosition(123, 26).y > -2){
				s_flags[JUDGE_MYHOUSE2] = true;
			}
		}
		else{
			performer->self->move((Position)TilePosition(109, 16));
			if (performer->self->getTilePosition().x - TilePosition(109, 16).x < 2 && performer->self->getTilePosition().y - TilePosition(109, 16).y < 2 && performer->self->getTilePosition().x - TilePosition(109, 16).x > -2 && performer->self->getTilePosition().y - TilePosition(109, 16).y > -2){
				s_flags[JUDGE_MYHOUSE] = true;
			}
		}
	}
	else if (myDirection == 2){
		performer->self->move((Position)TilePosition(6, 102));
		if (!s_flags[JUDGE_MYHOUSE2]){
			performer->self->move((Position)TilePosition(6, 102));
			if (performer->self->getTilePosition().x - TilePosition(6, 102).x < 2 && performer->self->getTilePosition().y - TilePosition(6, 102).y < 2 && performer->self->getTilePosition().x - TilePosition(6, 102).x > -2 && performer->self->getTilePosition().y - TilePosition(6, 102).y > -2){
				s_flags[JUDGE_MYHOUSE2] = true;
			}
		}
		else{
			performer->self->move((Position)TilePosition(19, 108));
			if (performer->self->getTilePosition().x - TilePosition(19, 108).x < 2 && performer->self->getTilePosition().y - TilePosition(19, 108).y < 2 && performer->self->getTilePosition().x - TilePosition(19, 108).x > -2 && performer->self->getTilePosition().y - TilePosition(19, 108).y > -2){
				s_flags[JUDGE_MYHOUSE] = true;
			}
		}
	}
	else if (myDirection == 3){
		performer->self->move((Position)TilePosition(108, 120));
		if (!s_flags[JUDGE_MYHOUSE2]){
			performer->self->move((Position)TilePosition(108, 120));
			if (performer->self->getTilePosition().x - TilePosition(108, 120).x < 2 && performer->self->getTilePosition().y - TilePosition(108, 120).y < 2 && performer->self->getTilePosition().x - TilePosition(108, 120).x > -2 && performer->self->getTilePosition().y - TilePosition(108, 120).y > -2){
				s_flags[JUDGE_MYHOUSE2] = true;
			}
		}
		else{
			performer->self->move((Position)TilePosition(106, 108));
			if (performer->self->getTilePosition().x - TilePosition(106, 108).x < 2 && performer->self->getTilePosition().y - TilePosition(106, 108).y < 2 && performer->self->getTilePosition().x - TilePosition(106, 108).x > -2 && performer->self->getTilePosition().y - TilePosition(106, 108).y > -2){
				s_flags[JUDGE_MYHOUSE] = true;
			}
		}
	}
}


void Scout::checkFrontNexus(){

	auto enemychkPos = BWTA::getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter(); //적군 check포인트
	auto enemyfrontBase = BWTA::getNearestBaseLocation(enemychkPos);

	for (auto unit : bw->enemy()->getUnits()){
		if (unit->getType() == UnitTypes::Protoss_Nexus){
			if (getDistance(unit->getPosition(),enemyfrontBase->getPosition()) < 100){
				s_flags[JUDGE_FRONTNEXUS] = true;
				//bw << "JUDGE_FRONTNEXUS" << endl;
			}
		}
	}
		
	
}