#pragma once
#include <BWAPI.h>

class Commander{
	
	Commander();
	~Commander();
	bool buildDebug;

public:
	static Commander* getInstance();

	// OnFunc
	void init();
	void start();
	void show();
	void run();
	
	void structureCreate(BWAPI::Unit unit);
	void structureComplete(BWAPI::Unit unit);
	void unitCreate(BWAPI::Unit unit);
	void unitComplete(BWAPI::Unit unit);
	void destroy(BWAPI::Unit unit);
	void unitMorph(BWAPI::Unit unit);
};