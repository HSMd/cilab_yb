#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include <vector>
#include "MyUnit.h"

using namespace std;
using namespace BWAPI;
using namespace BWTA;


class ChkPoint{
public:
	Position pos;
	bool chk;

	ChkPoint(Position pos){
		this->pos = pos;
		chk = false;
	}
};

class Scout
{
	enum S_FLAGS{
		JUDGE_M,
		JUDGE_TIME,
		JUDGE_WATAGATA1,
		JUDGE_WATAGATA2,
		JUDGE_SECONDMOVE,
		JUDGE_ENEMY,
		JUDGE_MYHOUSE,
		JUDGE_MYHOUSE2,
		JUDGE_FRONTNEXUS,

		ROTATE_0,
		ROTATE_1,
		ROTATE_2,
		ROTATE_3,

		ENTER_BLOCK,
		ENTER_PROBE,
		ENEMY_ROTATE,

		SEARCHTIME_1_ON,
		SEARCHTIME_1_OFF,
		SEARCHTIME_2_ON,
	};

	Scout();
	~Scout();

	bool find;
	bool isIn;

	bool canUseData;

	int searchCount;
	int enemyDirection;
	int myDirection;

	int centerMoveCount;
	int centerMoveJudge = 0;

	Position pos1;
	Position pos2;
	Position pos3;

	MyUnit* performer;
	BaseLocation* target;
	BaseLocation* enemyFront;
	ChkPoint* point;
	
	map<S_FLAGS, bool> s_flags;

	vector<BaseLocation*> startBases;
	vector<ChkPoint*> chkPoints;

public:
	static Scout* getInstance();

	void init();
	void show();
	void draw();
	void run();

	void scvScout();

	void changeTarget();
	ChkPoint* getNextPoint();
	void endMission();
	

	bool checkChokepoint(BaseLocation* base);
	void no_Enemymove(BaseLocation* base);
	void seeEverything();
	void hinder();
	void centerRotate();

	BaseLocation* chkEnemyStart(UnitType type);
	BaseLocation* getCloseBase(TilePosition pos, int term = 35);

	void findEnemyStart(BaseLocation* base);
	void setSpy(MyUnit* unit){ performer = unit; };
	void InnerSearch();
	MyUnit* getSpy(){ return performer; };

	void checkFrontNexus();
	int getSearchCount(){ return searchCount; };
	bool getUseData(){ return canUseData; };
	bool isFind(){ return find; };
	bool checkEnterBlock(){ return s_flags[ENTER_BLOCK]; };
	bool searchTime_1_On(){ return s_flags[SEARCHTIME_1_ON]; };
	bool searchTime_2_On(){ return s_flags[SEARCHTIME_2_ON]; };
	bool checkEnemyRotate(){ return s_flags[SEARCHTIME_1_OFF]; };
	bool getcheckFrontNexus(){ return s_flags[JUDGE_FRONTNEXUS]; };
};