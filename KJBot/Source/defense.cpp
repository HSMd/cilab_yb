#include "defense.h"
#include "Utility.h"
#include "Instance.h"
#include "Information.h"
#include "Instance.h"
#include "CombatControl.h"

Defense::Defense()
{
	SCV.clear();
	Scan.clear();
	Marine.clear();
	
	repairtarget = nullptr;
	FrontSight = false;
}
Defense::~Defense()
{

}
Defense* Defense::getInstance() {

	static Defense instance;
	return &instance;
}
void Defense::show()
{

}
void Defense::init()
{
	switch (info->map->getMyDirection())
	{
	case 0:
		marinePosition.push_back(tile2pos(TilePosition(11, 26)));
		marinePosition.push_back(tile2pos(TilePosition(7, 26)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(14, 27)) + Position(0, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 0));
		break;
	case 1:
		marinePosition.push_back(tile2pos(TilePosition(103, 8)));
		marinePosition.push_back(tile2pos(TilePosition(100, 6)));
		marinePosition.push_back(tile2pos(TilePosition(101, 6)));
		marinePosition.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
		marinePosition.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));
		break;
	case 2:
		marinePosition.push_back(tile2pos(TilePosition(24, 121)));
		marinePosition.push_back(tile2pos(TilePosition(23, 117)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(27, 123)));
		marinePosition.push_back(tile2pos(TilePosition(28, 123)));
		marinePosition.push_back(tile2pos(TilePosition(31, 121)) + Position(16, 16));
		break;

	case 3:
		marinePosition.push_back(tile2pos(TilePosition(118, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 99)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 100)) + Position(16, 16));		
		break;
	}
}

void Defense::run()
{	
	//IdleMarine();
	getUnits(UnitTypes::Terran_Marine);
	getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode, 5);
}

void Defense::run2(){

	/*
	//temp state change
	auto combat = state->getCombat();
	state->setCombatFlag(COMBAT_FLAG::NORMAL, false);
	
	// 적군이 없을 경우.	
	if (combat == COMBAT_FLAG::NORMAL)
	{
		//scv
		blockdefense();

		for (auto marine : Marine)
		{
			Position p;
			if (FrontSight == false)
			{
				p = info->map->getMyTerritorys().back()->getPosition();

				marine->self->attack(p);
				if (getDistance(marine->getPosition(), p) <= 3)
				{
					FrontSight = true;
					if (!marine->self->isHoldingPosition())
					{
						marine->self->holdPosition();
					}
				}
			}
			if (FrontSight == true && (info->self->getImmediatelyUnit(UnitTypes::Terran_Siege_Tank_Siege_Mode).size() + info->self->getImmediatelyUnit(UnitTypes::Terran_Siege_Tank_Siege_Mode).size()) > 2){
				p = BWTA::getNearestChokepoint(info->self->getUnits(UnitTypes::Terran_Barracks)[0]->getPosition())->getCenter();
				marine->self->attack(p);
				if (getDistance(marine->getPosition(), p) <= 2)
				{
					if (!marine->self->isHoldingPosition())
					{
						marine->self->holdPosition();
					}
				}
			}
		}
	}

	else if (combat == COMBAT_FLAG::ENEMY_SEARCH){
		// 프로브 정찰 혹은 프로브 러쉬.
		probeSearch();
	}
	else if (combat == COMBAT_FLAG::INVASION_PYLON)
	{
		// 파일런 - 입구방해 혹은 포토러쉬 본진 게이트 등.
		pylonDefeat();
	}
	else{
		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
			blockdefense();
		}
	}

	if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2))
		//if (info->self->getBuildingCount(UnitTypes::Terran_Command_Center) >= 2 && info->self->getImmediatelyBuilding(UnitTypes::Terran_Factory).size() >= 2)
	{
		if (combat == COMBAT_FLAG::NORMAL)
		{
			Position p, cp;
			MyUnit *bp;
			bp = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
			cp = BWTA::getNearestChokepoint(info->self->getBuildings(UnitTypes::Terran_Command_Center).front()->getPosition())->getCenter();
			p.x = (bp->getPosition().x * 2 + cp.x) / 3;
			p.y = (bp->getPosition().y * 2 + cp.y) / 3;

			for (auto unit : Marine)
			{
				if (!bp->self->isLifted())
				{
					unit->self->move(p);
				}
				if (isAlmostReachedDest(p, unit->getPosition(), 70, 70))
				{
					if (!unit->self->isHoldingPosition())
					{
						unit->self->holdPosition();
					}
				}

			}

			//repair
			scvRepair();

		}
		else{
			//marine
			auto enemyclose = info->map->getCloseEnemy();
			float dis = 99999;

			for (auto unit : Marine)
			{

				Unit enemy = nullptr;
				for (auto close : enemyclose)
				{
					if (close == enemyclose.front())
					{
						enemy = close;
						dis = getDistance(close->getTilePosition(), unit->getTilePosition());
					}
					else
					{
						if (getDistance(close->getTilePosition(), unit->getTilePosition()) < dis)
						{
							enemy = close;
							dis = getDistance(close->getTilePosition(), unit->getTilePosition());
						}
					}
				}


				if (unit->self->isAttacking() == false)
				{
					unit->self->attack(enemy);
				}
				Position temp = Position(0, 0);
				if (getDistance(unit->self->getPosition(), BWTA::getNearestChokepoint(info->map->getMyStartBase()->getPosition())->getCenter()) < 3)
				{
					temp = info->map->getMyStartBase()->getPosition();
				}
				else{
					temp = BWTA::getNearestChokepoint(info->map->getMyStartBase()->getPosition())->getCenter();
				}
				if (enemy != nullptr && temp != Position(0, 0))
				{
					CombatControl::getInstance()->kitingMove(unit, enemy, temp);
				}

			}
		}
	}
	*/
}

void Defense::cleanGarbage(){

}

//건물 수리
void Defense::getUnits(UnitType type, int count){
	
	auto unitController = UnitControl::getInstance()->getInstance();
	
	while (true){
		MyUnit* marine = unitController->getUnit(type);
		if (marine == nullptr || (count != -1 && count <= guardUnits[type].size())){
			return;
		}

		guardUnits[type].push_back(marine);
	}
}

void Defense::IdleMarine()
{
	for (auto m : info->self->getUnits(UnitTypes::Terran_Marine))
	{
		bool already = false;
		for (auto mm : Marine)
		{
			if (m->self->getID() == mm->self->getID())
			{
				already = true;
			}
		}
		if (!already)
		{
			Marine.push_back(m);
		}
	}
}

void Defense::probeSearch()
{	
	// 프로브만 있을 경우.
	auto& close = info->map->getMyTerritorys()[0]->getCloseEnemy();
	int probeCount = info->map->getCloseCount(UnitTypes::Protoss_Probe);

	auto& scvs = guardUnits[UnitTypes::Terran_SCV];
	Unit target = nullptr;
	int scvGetSize = probeCount - scvs.size();
	int minHit = 9999;	

	if (close.size() == 0){
		
		for (auto unit : SCV){
			unit->state = MyUnit::STATE::MINERAL;
		}

		SCV.clear();

		return;
	}

	// 타겟을 선정한다.
	for (auto unit : close){
		if (unit->getHitPoints() < minHit){
			minHit = unit->getHitPoints();
			target = unit;
		}
	}

	// SCV 숫자 관리.
	if (0 < scvGetSize){
		for (int i = 0; i < scvGetSize; i++){
			auto worker = info->map->getMyTerritorys()[0]->getWorker();
			if (worker != nullptr){
				SCV.push_back(worker);
			}
		}
	}
	else{
		int over = 1.5 * probeCount;

		for (auto iter = SCV.begin(); over < SCV.size(); iter++){
			(*iter)->state = MyUnit::STATE::IDLE;

			iter = SCV.erase(iter);
			iter--;
		}
	}

	// SCV 공격.
	if (Marine.size() == 0)
	{
		for (auto iter = SCV.begin(); iter != SCV.end(); iter++){
			auto scv = (*iter);

			if (!scv->self->exists()){
				iter = SCV.erase(iter);
				iter--;
				continue;
			}

			if (target != nullptr){
				scv->self->attack(target);
			}
		}
	}
	else{
		for (auto iter = SCV.begin(); iter != SCV.end();){
			(*iter)->state = MyUnit::STATE::IDLE;

			iter = SCV.erase(iter);
		}
	}
	

	// Marine 공격
	for (auto unit : Marine){
		if (target != nullptr){
			if (unit->self->isAttacking() == false)
			{
				unit->self->attack(target);
			}
		}
	}
}

void Defense::pylonDefeat()
{
	// 파일런 - 입구방해 혹은 포토러쉬 본진 게이트 등.
	auto& close = info->map->getCloseEnemy();
	int probeCount = info->map->getCloseCount(UnitTypes::Protoss_Probe);
	int pylonCount = info->map->getCloseCount(UnitTypes::Protoss_Pylon);
	int photoCount = info->map->getCloseCount(UnitTypes::Protoss_Photon_Cannon);
	int gateCount = info->map->getCloseCount(UnitTypes::Protoss_Gateway);

	int scvGetSize;
	int minHit = 9999;

	if (close.size() == probeCount){

		probeSearch();
		return;
	}

	if (0 < pylonCount){
		// 파일런이 있을 경우.
		Unit pylon = nullptr;
		Unit probe = nullptr;

		for (auto unit : close){
			if (unit->getType() == UnitTypes::Protoss_Pylon){
				if (pylon == nullptr){
					pylon = unit;
				}				
			}
			else{
				if (unit->getHitPoints() < minHit){
					minHit = unit->getHitPoints();
					probe = unit;
				}
			}
		}
		
		if (probeCount + pylonCount == close.size()){
			// 단순 프로브랑 파일런만 있을 경우 ( probe를 보낸다 )
			scvGetSize = probeCount + (pylonCount * 2) - SCV.size();
		}
		else{
			// 포토 혹은 게이트를 만듬.
			scvGetSize = probeCount + (pylonCount * 4) - SCV.size();
		}		

		if (0 < scvGetSize){
			for (int i = 0; i < scvGetSize; i++){
				auto worker = info->map->getMyTerritorys()[0]->getWorker();
				if (worker != nullptr){
					SCV.push_back(worker);
				}
			}
		}

		auto probeTracer = SCV.front();
		if (probe != nullptr){
			probeTracer->self->attack(probe);
		}
		else{
			probeTracer->self->attack(pylon);
		}

		for (auto scv : SCV){
			if (scv == probeTracer)
				continue;

			scv->self->attack(pylon);
		}

		for (auto unit : info->self->getUnits(UnitTypes::Terran_Marine)){
			unit->self->attack(pylon);
		}
	}
	else{
		// 파일런이 없을 경우.
		auto close = info->map->getCloseEnemy();
		Unit target = nullptr;
		int minHit = 9999;

		for (auto unit : close){
			if (target == nullptr){
				target = unit;
				continue;
			}

			if (unit->getType() == UnitTypes::Protoss_Zealot){
				target = unit;
				break;
			}

			if (unit->getType() == UnitTypes::Protoss_Probe){
				target = unit;
				break;
			}
			else{
				if (unit->getHitPoints() < minHit){
					minHit = unit->getHitPoints();
					target = unit;
				}
			}
		}

		if (target == nullptr){
			return;
		}

		for (auto unit : info->self->getUnits(UnitTypes::Terran_Marine)){
			unit->self->attack(target);
		}

	/*	for (auto unit : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
			unit->self->attack(target);
		}

		for (auto unit : info->self->getUnits(UnitTypes::Terran_Vulture)){
			unit->self->attack(target);
		}*/
	}
}

void Defense::scvRepair()
{
	int maxValue = -1;
	MyUnit *temp = nullptr;

	for (auto building = info->self->getAllBuildings().begin(); building != info->self->getAllBuildings().end(); building++)
	{
		if ((*building)->self->getType().maxHitPoints() - (*building)->self->getHitPoints() > 0)
		{
			if ((*building)->self->getType().maxHitPoints() - (*building)->self->getHitPoints() > maxValue)
			{
				temp = *building;
			}
		}

	}

	if (temp != nullptr)
	{
		//SCV 개수 정하기 어떻게 해야하리
		for (int i = SCV.size(); i < 2; i++)
		{
			MyUnit *scv = info->map->getNearTerritory(temp->self->getTilePosition())->getWorker(false);   // scv -> NULL
			if (scv->self->exists())
			{
				SCV.push_back(scv);
			}
		}
		for (auto scv : SCV)
		{
			if (scv->self->exists())
			{
				if (scv->self->isRepairing() == false)
				{
					scv->self->repair(temp->self);
				}

			}
		}
	}
	else
	{
		if (SCV.empty() == false)
		{
			for (auto scv : SCV)
			{
				 scv->self->gather(scv->target);
			}
		}
	}
}


void Defense::blockdefense()
{
	//입구 막은거
	for (auto unit : info->self->getAllBuildings()){
		if (unit->getType() == UnitTypes::Terran_Barracks){
			if (unit->getTilePosition() == posMgr->getTilePosition(POS::SIM1_BARRACK)){
				if (getProgress(unit->self) < 90)
				{
					if (repairtarget == nullptr)
					{
						repairtarget = unit;
					}
					else{
						if (getProgress(unit->self) < getProgress(repairtarget->self))
						{
							repairtarget = unit;
						}
					}
				}
			}
		}
		else if (unit->getType() == UnitTypes::Terran_Supply_Depot){
			if (unit->getTilePosition() == posMgr->getTilePosition(POS::SIM1_SUPPLY))
			{
				if (getProgress(unit->self) < 95)
				{
					if (repairtarget == nullptr)
					{
						repairtarget = unit;
					}
					else{
						if (getProgress(unit->self) < getProgress(repairtarget->self))
						{
							repairtarget = unit;
						}
					}
				}
			}
			else if (unit->getTilePosition() == posMgr->getTilePosition(POS::SIM1_SUPPLY2))
			{
				if (getProgress(unit->self) < 95)
				{
					if (repairtarget == nullptr)
					{
						repairtarget = unit;
					}
					else{
						if (getProgress(unit->self) < getProgress(repairtarget->self))
						{
							repairtarget = unit;
						}
					}
				}
			}
		}
	}
	if (repairtarget != nullptr)
	{
		auto startTerritory = info->map->getMyTerritorys().front();

		for (int i = SCV.size(); i < 2; i++){
			auto scv = startTerritory->getWorker(false);
			SCV.push_back(scv);
		}
		for (auto s : SCV)
		{
			if (repairtarget != nullptr)
			{
				s->self->repair(repairtarget->self);
			}
		}

		if (!repairtarget->self->exists() || getProgress(repairtarget->self) >= 100)
		{
			repairtarget = nullptr;
		}
	}
	else{
		for (auto s = SCV.begin(); s != SCV.end();)
		{
			(*s)->state = MyUnit::STATE::IDLE;
			s = SCV.erase(s);
		}
	}

	//marine 뚜가 맞으면 빼줘
	for (auto marine : Marine)
	{
		if (getProgress(marine->self) < 70)
		{
			if (getDistance(marine->self->getPosition(), info->map->getMyStartBase()->getPosition()) < 15)
			{
				if (marine->self->isMoving())
				{
					marine->self->stop();
				}
			}
			else
			{
				marine->self->rightClick(info->map->getMyStartBase()->getPosition());
			}
			
		}
	/*	else{
			MarineHold();
		}*/
	}
}

void Defense::MarineHold()
{
	int count = 0;
	for (auto m = Marine.begin(); m != Marine.end(); m++)
	{
		if ((*m)->self->isCompleted() && (*m)->self->exists())
		{
			if (count < marinePosition.size())
			{
				if (isAlmostReachedDest((*m)->getPosition(), marinePosition[count], 2, 2))
				{
					if (!(*m)->self->isHoldingPosition())
					{
						(*m)->self->holdPosition();
					}
				}
				else
				{
					(*m)->self->move(marinePosition[count]);
				}
			}
		}
		count++;
	}	
}

//스캔 뿌리기
bool Defense::ScanControl(TilePosition tp)
{
	for (auto comsat : info->self->getBuildings(UnitTypes::Terran_Comsat_Station))
	{
		if (comsat->self->getEnergy() > 60)
		{
			comsat->self->useTech(TechTypes::Scanner_Sweep, tile2pos(tp));
			return true;
		}
	}
	return false;
}
