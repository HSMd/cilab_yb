#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "DefenceEvent.h"

using namespace std;

class repairTeam{
public:
	MyUnit* target;
	vector<MyUnit*> performers;
	int require;

	repairTeam(MyUnit* target){ this->target = target; require = 0; };
	void run();
};

class DefenceRepair : public DefenceEvent
{	
	vector<repairTeam*> rTeams;
	vector<MyUnit*> scvs;

	int repairCount;

public:

	DefenceRepair(DEFENSE_TYPE type);
	~DefenceRepair();

	void chkFire();
	void turnOff();

	void update();
	void run();

	void act();
	int CountRepairBuilding();

	bool needRepair(MyUnit* unit);
	bool alreadyHasTeam(MyUnit* unit);
};

