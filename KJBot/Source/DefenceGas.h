#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "DefenceEvent.h"

using namespace std;


class DefenceGas : public DefenceEvent
{	
	Unit target;

public:

	DefenceGas(DEFENSE_TYPE type);
	~DefenceGas();

	void chkFire();
	void turnOff();

	void update();
	void run();

	void act();
};

