#pragma once
#include <BWAPI.h>

using namespace BWAPI;

class MyUnit{
public:
	enum STATE{
		IDLE,
		MINERAL,
		GAS,
		BUILD,
		OTHER,
		OUT,
		HOLD,

		MOVE,
		MOVE_LAND,
	};

	STATE state;
	Unit self;
	Unit target;

	UnitType targetType;
	TilePosition targetPos;
	bool buildMoney;

	Unit attackTarget;
	Position targetPosition;
	int team;
	int maginNumber;
	int preEnergy;
	bool mining;
	
	int orderTimes[7];

	bool helpMe;

	MyUnit(Unit unit){ 
		self = unit; 
		state = STATE::IDLE;
		buildMoney = false;
		attackTarget = NULL;
		targetPosition = Position(-1, -1);
		team = 0;
		helpMe = false;
		maginNumber = 0;
		if (unit->getType() == UnitTypes::Terran_Vulture) {
			maginNumber = 3;
			mining = false;
		}
		isInTeam = false;
		preEnergy = unit->getEnergy();
		
		for (int i = 0; i < 7; i++){
			orderTimes[i] = 0;
		}
		//memcpy(orderTimes, 0, sizeof(orderTimes));
	};	

	~MyUnit(){};
	bool isInTeam;
	UnitType getType(){ return self->getType(); };
	TilePosition getTilePosition() { return self->getTilePosition(); };
	Position getPosition() { return self->getPosition(); };
	void updateOrderTime(){ memcpy(orderTimes, orderTimes + 1, sizeof(orderTimes) - sizeof(int)); orderTimes[6] = self->getOrderTimer(); };
};

