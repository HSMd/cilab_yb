#include "Map.h"
#include "Utility.h"
#include "Instance.h"
#include <stdlib.h>

Map::Map(){

	myStartBase = BWTA::getStartLocation(Broodwar->self());
	enemyStartBase = nullptr;
	
	startDirection = getDirection(myStartBase);
	Territory* territory = new Territory(myStartBase);
	territory->init(myStartBase);

	myTerritorys.push_back(territory);
	setBaseLocationInfo();
}

Map::~Map(){

}

void Map::show(int startX, int startY){
	
	int closeY = 20;
	int i = 1;

	// SCV 숫자 보여주기.
	for (auto t : myTerritorys){

		if (!t->getReady()){
			continue;
		}

		//Broodwar->drawTextScreen(startX, startY, "ALL_SCV : %d", t->getScvCount());
		t->show(startX, startY);
		startX += 100;
		closeY += 20;
		i++;
	}	

	Broodwar->drawTextScreen(50, 60, "Territory[%d] : %d", 0, myTerritorys[0]->getCloseEnemy().size());
	Broodwar->drawTextScreen(50, closeY, "CLOSE_SIZE : %d", info->map->getCloseEnemy().size());

	if (enemyStartBase != nullptr){
		BWAPI::Broodwar->drawCircleMap(enemyStartBase->getPosition(), 20 * TILEPOSITION_SCALE, Colors::Red, false);
	}
}

void Map::update(){

	updateBaseLocationInfo();

	for (auto territory : myTerritorys) {
		territory->update();
	}

	for (auto scv : info->self->getUnits(UnitTypes::Terran_SCV)){
		scv->updateOrderTime();
	}
}

void Map::run(){

	for (auto territory : myTerritorys){
		territory->run();
	}
}

void Map::draw(){

	// 추후 원을 그릴 예정.
	for (auto territory : myTerritorys){
		territory->draw();
	}

	for (auto bli = BLI.begin(); bli != BLI.end(); bli++) {
		(*bli).second->draw();
	}
	// 미네랄 옆 카운트.
	
}

BaseLocation* Map::getCandidateLocation(){

	BWTA::BaseLocation* near = nullptr;
	float minDis = 9999999.0;

	for (auto iter = getBaseLocations().begin(); iter != getBaseLocations().end(); ++iter){

		bool same = false;
		for (auto territory : myTerritorys){
			if ((*iter) == territory->getBase()){
				same = true;
				break;
			}
		}

		if (same){
			continue;
		}

		float dis = getDistance((*iter), myStartBase);
		if (dis < minDis){
			minDis = dis;
			near = (*iter);
		}
	}

	return near;
}

Territory* Map::getNearTerritory(TilePosition dst){

	float minDis = 999999.0;
	Territory* re = nullptr;

	for (auto& territory : myTerritorys){

		if (!territory->getReady()){
			continue;
		}

		float distance = getDistance(territory->getTilePosition(), dst);

		if (distance < minDis){
			minDis = distance;
			re = territory;
		}
	}

	return re;
}

void Map::addScv(Unit unit){

	auto near = getNearTerritory(unit->getTilePosition());
	if (near->getRequireSCVCount() <= 0){
		for (auto t : myTerritorys){
			if (!t->getReady()){
				continue;
			}

			if (0 < t->getRequireSCVCount()){
				near = t;
				break;
			}
		}
	}

	near->addScv(new MyUnit(unit));
}

int Map::getDirection(BaseLocation* base){

	vector<int> dis;
	dis.push_back(getDistance(base->getTilePosition(), TilePosition(0, 0)));
	dis.push_back(getDistance(base->getTilePosition(), TilePosition(128, 0)));
	dis.push_back(getDistance(base->getTilePosition(), TilePosition(0, 128)));
	dis.push_back(getDistance(base->getTilePosition(), TilePosition(128, 128)));

	int dir = 0;
	int cnt = 0;
	int minDis = 999999;

	for (auto d : dis){
		if (d < minDis){
			minDis = d;
			dir = cnt;
		}

		cnt++;
	}

	return dir;
}

void Map::setBaseLocationInfo() {
	BLI.clear();
	for (auto base : BWTA::getBaseLocations()) {
		BaseLocationInfo* bli = new BaseLocationInfo(base);

		bli->minerals = base->minerals();
		bli->mineralCount = base->getMinerals().size();
		bli->gases = base->gas();
		bli->myBuildings.clear();
		bli->enemyBuildings.clear();
		bli->myUnits.clear();
		bli->enemyUnits.clear();
		bli->value = bli->minerals * 1 + bli->gases * 3;
		BLI.insert(std::pair<BWTA::BaseLocation*, BaseLocationInfo*>(base, bli));
	}
}

void Map::updateBaseLocationInfo() {
	for (auto b = BLI.begin(); b != BLI.end(); b++) {
		b->second->minerals = 0;
		b->second->mineralCount = 0;
		b->second->gases = 0;
		b->second->workerCount = 0;
		b->second->myUnits.clear();
		b->second->myBuildings.clear();
		b->second->enemyUnits.clear();
		b->second->enemyBuildings.clear();
		b->second->mineralVec.clear();
		b->second->gasVec.clear();
	}

	for (auto u : info->enemy->getAllUnits()) {
		UnitInfo *ui = new UnitInfo();
		ui->id = u->getID();
		ui->pos = info->enemy->getPosition(u);
		ui->type = info->enemy->getType(u);
		if (ui->type == UnitTypes::Protoss_Photon_Cannon)
			BLI[BWTA::getNearestBaseLocation(ui->pos)]->enemyBuildings.push_back(ui);
		else
			BLI[BWTA::getNearestBaseLocation(ui->pos)]->enemyUnits.push_back(ui);
		if (ui->type == UnitTypes::Protoss_Probe || ui->type == UnitTypes::Zerg_Drone || ui->type == UnitTypes::Terran_SCV) {
			BLI[BWTA::getNearestBaseLocation(ui->pos)]->workerCount++;
		}
	}
	for (auto u : info->enemy->getAllBuildings()) {
		UnitInfo *ui = new UnitInfo();
		ui->id = u->getID();
		ui->pos = info->enemy->getPosition(u);
		ui->type = info->enemy->getType(u);
		BLI[BWTA::getNearestBaseLocation(ui->pos)]->enemyBuildings.push_back(ui);
	}

	for (auto u : info->self->getAllUnits()) {
		if (!u->self->exists()) continue;
		UnitInfo *ui = new UnitInfo();
		ui->id = u->self->getID();
		ui->pos = u->getPosition();
		ui->type = u->getType();
		BLI[BWTA::getNearestBaseLocation(ui->pos)]->myUnits.push_back(ui);
	}
	for (auto u : info->self->getAllBuildings()) {
		if (!u->self->exists()) continue;
		UnitInfo *ui = new UnitInfo();
		ui->id = u->self->getID();
		ui->pos = u->getPosition();
		ui->type = u->getType();
		BLI[BWTA::getNearestBaseLocation(ui->pos)]->myBuildings.push_back(ui);
	}

	for (auto m : info->resource->minerals) {
		BLI[BWTA::getNearestBaseLocation(info->resource->getPosition(m))]->minerals += info->resource->getAmount(m);
		BLI[BWTA::getNearestBaseLocation(info->resource->getPosition(m))]->mineralCount++;
		BLI[BWTA::getNearestBaseLocation(info->resource->getPosition(m))]->mineralVec.push_back(m);
	}

	for (auto m : info->resource->gases) {
		BLI[BWTA::getNearestBaseLocation(info->resource->getPosition(m))]->gases += info->resource->getAmount(m);
		BLI[BWTA::getNearestBaseLocation(info->resource->getPosition(m))]->gasVec.push_back(m);
	}

	for (auto b = BLI.begin(); b != BLI.end(); b++) {
		b->second->update();
	}
}

void Map::drawBaseLocationInfo() {
	for (auto b = BLI.begin(); b != BLI.end(); b++) {
		
	}
}

void BaseLocationInfo::update() {
	if (enemyBuildings.size() > myBuildings.size()) {
		player = 0;
	}
	else if (enemyBuildings.size() < myBuildings.size()) {
		player = 1;
	}
	else {
		player = -1;
	}

	int v = (minerals + gases * 2)/4;
	int d = 0;
	if (player == 1) {
		for (auto u : myUnits) {
			if (u->type == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
				d += 30;
			}
			else if (u->type == UnitTypes::Terran_Vulture_Spider_Mine) {
				d += 5;
			}
			else if (u->type == UnitTypes::Terran_SCV) {
				v += 8;
			}
			else {
				d += 5;
			}
		}
		for (auto u : myBuildings) {
			if (u->type == UnitTypes::Terran_Missile_Turret) {
				d += 30;
			}
			else if (u->type == UnitTypes::Terran_Factory) {
				v += 25;
			}
			else {
				v += 10;
			}
		}
	}
	else if (player == 0) {
		for (auto u : enemyUnits) {
			if (u->type == UnitTypes::Protoss_High_Templar) {
				d += 10;
			}
			else if (u->type == UnitTypes::Protoss_Reaver) {
				d += 30;
			}
			else if (u->type == UnitTypes::Protoss_Probe) {
				v += 8;
			}
			else {
				d += 5;
			}
		}
		for (auto u : enemyBuildings) {
			if (u->type == UnitTypes::Protoss_Photon_Cannon) {
				d += 30;
			}
			else if (u->type == UnitTypes::Protoss_Gateway) {
				v += 25;
			}
			else {
				v += 10;
			}
		}
	}

	value = v;
	defense = d;
}

void BaseLocationInfo::draw() {
	Position s = Position(base->getPosition().x - 30 , base->getPosition().y - 57);
	Position e = Position(base->getPosition().x + 30,  base->getPosition().y + 57);
	Position s2 = Position(base->getPosition().x - 28, base->getPosition().y - 55);
	Position e2 = Position(base->getPosition().x + 28, base->getPosition().y + 55);

	s.x += 50;	e.x += 50;	s2.x += 50;  e2.x += 50;
	s.y -= 50;	e.y -= 50;	s2.y -= 50;  e2.y -= 50;
	
	if (player == 1) {
		BWAPI::Broodwar->drawBoxMap(s, e, Colors::Green, true);
		BWAPI::Broodwar->drawBoxMap(s2, e2, Colors::Black, true);
		s.x += 4;
		s.y += 1;
		
		BWAPI::Broodwar->drawTextMap(s, "Owner");
		s.x += 25;
		s.y += 11;
		BWAPI::Broodwar->drawTextMap(s, "ME");
		s.x -= 25;
		s.y += 11;
	}
	else if (player == 0) {
		BWAPI::Broodwar->drawBoxMap(s, e, Colors::Red, true);
		BWAPI::Broodwar->drawBoxMap(s2, e2, Colors::Black, true);
		s.x += 4;
		s.y += 1;

		BWAPI::Broodwar->drawTextMap(s, "Owner");
		s.x += 25;
		s.y += 11;
		BWAPI::Broodwar->drawTextMap(s, "ENEMY");
		s.x -= 25;
		s.y += 11;
	}
	else {
		BWAPI::Broodwar->drawBoxMap(s, e, Colors::Yellow, true);
		BWAPI::Broodwar->drawBoxMap(s2, e2, Colors::Black, true);
		s.x += 4;
		s.y += 1;

		BWAPI::Broodwar->drawTextMap(s, "Owner");
		s.y += 11;
		s.x += 25;
		BWAPI::Broodwar->drawTextMap(s, "DINO");
		s.x -= 25;
		s.y += 11;
	}


	

	char buf[10];

	BWAPI::Broodwar->drawTextMap(s, "Minerals");
	s.y += 11;
	itoa(minerals, buf, 10);
	buf[strlen(buf)] = '\0';
	s.x += 25;
	BWAPI::Broodwar->drawTextMap(s, buf);
	s.x -= 25;
	s.y += 11;

	BWAPI::Broodwar->drawTextMap(s, "Gas");
	s.y += 11;
	itoa(gases, buf, 10);
	buf[strlen(buf)] = '\0';
	s.x += 25;
	BWAPI::Broodwar->drawTextMap(s, buf);
	s.x -= 25;
	s.y += 11;

	BWAPI::Broodwar->drawTextMap(s, "Value");
	s.y += 11;
	itoa(value, buf, 10);
	buf[strlen(buf)] = '\0';
	s.x += 25;
	BWAPI::Broodwar->drawTextMap(s, buf);
	s.x -= 25;
	s.y += 11;

	BWAPI::Broodwar->drawTextMap(s, "Defense");
	s.y += 11;
	itoa(defense, buf, 10);
	buf[strlen(buf)] = '\0';
	s.x += 25;
	BWAPI::Broodwar->drawTextMap(s, buf);
	s.x -= 25;
	s.y += 11;

}

void Map::destroy(Unit unit) {

	for (auto territory : myTerritorys) {		
		territory->destroy(unit);
	}
}

int Map::getCloseCount(UnitType type) {

	int sum = 0;
	for (auto territory : myTerritorys) {
		sum += territory->getCloseCount(type);
	}

	return sum;
}

vector<Unit>& Map::getCloseEnemy() {

	close.clear();

	for (auto territory : myTerritorys) {
		auto& t_close = territory->getCloseEnemy();

		if (!t_close.empty()){
			close.insert(close.end(), t_close.begin(), t_close.end());
		}		
	}

	return close;
}

bool Map::isDiagonal(){

	if (enemyStartBase != nullptr){
		if (startDirection == 0 && enemyDirection == 3)
			return true;
		else if (startDirection == 1 && enemyDirection == 2)
			return true;
		else if (startDirection == 2 && enemyDirection == 1)
			return true;
		else if (startDirection == 3 && enemyDirection == 0)
			return true;
	}

	return false;
}