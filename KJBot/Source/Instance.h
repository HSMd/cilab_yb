#pragma once
#include <BWAPI.h>
#include "Commander.h"
#include "MapInformation.h"
#include "Information.h"
#include "UnitControl.h"
#include "ScvControl.h"
#include "Scheduler.h"
#include "BuildMgr.h"
#include "StateMgr.h"
#include "defense.h"
#include "PositionMgr.h"
#include "Scout.h"
#include "Guard.h"

extern BWAPI::GameWrapper& bw;
extern Commander* cmd;
extern Information* info;
extern MapInformation* mInfo;
extern UnitControl* control;
extern BuildMgr* buildMgr;
extern StateMgr* state;
extern Defense* defense;
extern PositionMgr* posMgr;
extern Scout* scouting;
extern Guard* guard;

extern ScvControl* scvControl;
extern Scheduler* scheduler;