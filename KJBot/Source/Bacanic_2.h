#pragma once
#include "BWAPI.h"
#include "MyUnit.h"
#include "Work.h"

#include <map>
#include <vector>

using namespace BWAPI;
using namespace std;

class Bacanic2{

	enum FLAG{
		SEARCH,
		ADD_CENTER,
		INVASION,

		AUTO_MARINE,
		AUTO_MEDIC,
		AUTO_TANK,
		AUTO_VULTURE,
		AUTO_SUPPLY,

		SCV_CONTROL_1,
		SCV_CONTROL_2,
		GAS_CONTROL,
		ERROR_OUT,

		BUILD_START,   // 팩토리까지.
		BUILD_START_2, // 2센터 + 머신샵 까지.

		BUILD_MID,
		BUILD_MID_GAS,
		BUILD_MID_COMSAT,

		BUILD_MID_2,
		BUILD_TURRET,
		BUILD_LAST,
		BUILD_LAST_ADD,		
		BUILD_HELL,

		EXPAND_BRAVELY,
		EXPAND_CAREFULLY,
		EXPAND_MUST,
	};

	vector<BuildWork> builds;
	vector<MyUnit*> moveBuildings;
	vector<MyUnit*> guardTank;

	map<FLAG, bool> flags;

	MyUnit* performer;
	MyUnit* spare;

	FLAG progress;
	int frameCount;

	int expandMind;

public: 
	Bacanic2(){};
	~Bacanic2(){};

	void init();
	void show();
	void draw();
	void run();
	
	void scvCreate(); // SCV 생산.
	void runBuild();  // 빌드를 진행.
	void gasControl(); // 가스 컨트롤
	void simOut();     // 쫒겨난 유닛
	void search();    // 정찰
	void autoUnit();

	// 확장 판단.
	float getValue(UnitType type);
	bool canExpand();
	void expand();
	void expandBravely();

	// 빌드 코드들.
	void buildProcess();
	void buildStart();
	void buildMid();
	void buildLast();
};