#include "UnitControl.h"
#include "Information.h"
#include "Instance.h"
#include "Self.h"
#include "Utility.h"
#include "CombatControl.h"

void TeamControl::update() {
	//printf("Tank size  %d\n", tankSet.size());

	this->teamCenterPos = UnitControl::getInstance()->getTeamCenterPos(this);

	this->tankCenterPos = UnitControl::getInstance()->getTankCenterPos(this);

	this->bionicCenterPos = UnitControl::getInstance()->getBionicCenterPos(this);

	this->attackPosition = UnitControl::getInstance()->getAttackPosition(this);

	this->state = UnitControl::getInstance()->getTeamState(this);

	CombatControl::getInstance()->onFrame(this);
}

void TeamControl::resetTG() {

}

TeamControl::STATE UnitControl::getTeamState(TeamControl* t) {
	TeamControl::STATE ret = t->state;
	if (t->state == TeamControl::STATE::IDLE) {
		return TeamControl::STATE::BCN_START;
	}
	if (t->state == TeamControl::STATE::BCN_START) {
		if (info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true) > 1) {
			return TeamControl::STATE::BCN_CLOSED;
		}
		else {
			return ret;
		}
	}
	if (t->state == TeamControl::STATE::BCN_CLOSED) {

		//if (0 < info->self->getBuildingCount(UnitTypes::Terran_Barracks) && info->self->getBuildings(UnitTypes::Terran_Barracks).front()->self->isLifted()) {
		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2)){
			for (auto scv : t->scvSet) {
				info->map->getMyTerritorys().front()->addScv(scv);
				UnitControl::getInstance()->getUnit(scv);
			}

			t->scvSet.clear();

			return TeamControl::STATE::BCN_FRONT;
		}
		else {
			return ret;
		}
	}
	/*if (t->state == TeamControl::STATE::BCN_FRONT) {
	if (isReadyGoing()) {
	return TeamControl::STATE::BCN_GOING;
	}
	else {
	return ret;
	}
	}*/
	if (t->state == TeamControl::STATE::BCN_GOING) {
		if (isReadyTight(t->team) && t->team == 0) {
			return TeamControl::STATE::BCN_TIGHT;
		}
		else {
			return ret;
		}
	}
	if (t->state == TeamControl::STATE::BCN_TIGHT) {
		if (isReadyBJ()) {
			return TeamControl::STATE::BCN_BJ;
		}
		else if (isReadyFinish()) {
			finishNextPos = info->map->getEnemyStartBase()->getPosition();
			return TeamControl::STATE::BCN_FINISH;
		}
		else {
			return ret;
		}
	}
	if (t->state == TeamControl::STATE::BCN_FINISH) {
		if (isUnReadyFinish()) {
			return TeamControl::STATE::BCN_TIGHT;
		}
		else if (isReadyBJ()) {
			return TeamControl::STATE::BCN_BJ;
		}
		else {
			return ret;
		}
	}

	return ret;
}

UnitControl* UnitControl::s_UnitControl;
UnitControl::UnitControl() {
	scanTime = 0;
}

UnitControl::~UnitControl() {

}

UnitControl* UnitControl::create() {
	static UnitControl instance;
	return &instance;
}

UnitControl* UnitControl::getInstance() {
	static UnitControl instance;
	return &instance;
}

void UnitControl::init() {
	CombatControl::create();
	TCV.clear();
	TCV.push_back(new TeamControl(0));
	TCV.push_back(new TeamControl(1));

	TCV[1]->state = TeamControl::STATE::BCN_JOIN;
	totalTeamIndex = 2;

	for (auto base : BWTA::getBaseLocations()) {
		finishSearchPos[base->getPosition()] = false;
	}
	finishSearchPos[(Position)(TilePosition(23,3))] = false;
	finishSearchPos[(Position)(TilePosition(2,95))] = false;
	finishSearchPos[(Position)(TilePosition(125,30))] = false;
	finishSearchPos[(Position)(TilePosition(102,116))] = false;
}

void UnitControl::onFrame() {

	CombatControl::getInstance()->update();

	for (int x = 0; x < totalTeamIndex; x++) {
		TCV[x]->update();
		//printf("team -> %d : marine %d medic %d tank %d\n", TCV[x]->team, TCV[x]->marineSet.size(), TCV[x]->medicSet.size(), TCV[x]->tankSet.size());
	}

	setTeamDicision();


	Unitset comsats;
	Position comsatPos;
	comsats.clear();

	bool needComsat = false;
	bool needSearchComsat = false;
	bool dontshoot = false;
	scanTime++;

	for (auto dark : info->enemy->getUnits(UnitTypes::Protoss_Dark_Templar)) {
		if (dark->isDetected()) continue;

		for (auto turret : info->self->getBuildings(UnitTypes::Terran_Missile_Turret)) {
			float dist = getDistance(dark->getPosition(), turret->getPosition());
			if (dist < 6.5f * 32) {
				dontshoot = true;
			}
		}
		if (dontshoot) continue;
		for (auto unit : info->self->getAllUnits()) {
			float dist = getDistance(dark->getPosition(), unit->getPosition());
			if (dist < 100) {
				needComsat = true;
				comsatPos = dark->getPosition();
				break;
			}
		}
		if (needComsat) break;
	}

	if (info->map->getEnemyStartBase() != NULL && TCV[0]->state == TeamControl::STATE::BCN_FINISH) {
		int count = 0;
		for (auto b = info->map->BLI.begin(); b != info->map->BLI.end(); b++) {
			if (b->second->player == 0) {
				count++;
			}
		}
		if (count == 0) {
			needSearchComsat = true;
		}
		updateFinishSearch();
	}

	if (scanTime > 64) {
		for (auto comsat : info->self->getBuildings(UnitTypes::Terran_Comsat_Station)) {
			if (comsat->self->getEnergy() > 50) {
				if (needComsat) {
					comsat->self->useTech(TechTypes::Scanner_Sweep, comsatPos);
					scanTime = 0;
					break;
				}
			}
			else if ((comsat->self->getEnergy() > 100 && info->enemy->getBuildingCount(UnitTypes::Protoss_Templar_Archives) > 0) ||
				(comsat->self->getEnergy() > 50 && info->enemy->getBuildingCount(UnitTypes::Protoss_Templar_Archives) == 0)) {
				if (needSearchComsat) {
					comsat->self->useTech(TechTypes::Scanner_Sweep, finishNextPos);
					finishSearchPos[finishNextPos] = true;
					scanTime = 0;
					break;
				}
			}
		}
	}

}

void UnitControl::draw() {

	if (TCV[0]->state == TeamControl::STATE::BCN_FINISH) {
		BWAPI::Broodwar->drawCircleMap(finishNextPos, 30, Colors::Purple, false);
		BWAPI::Broodwar->drawCircleMap(finishNextPos, 31, Colors::Purple, false);
		BWAPI::Broodwar->drawCircleMap(finishNextPos, 32, Colors::Purple, false);
		BWAPI::Broodwar->drawCircleMap(finishNextPos, 33, Colors::Purple, false);
	}

	for (auto q = info->self->getUnits(UnitTypes::Terran_Marine).begin(); q != info->self->getUnits(UnitTypes::Terran_Marine).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Teal, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Medic).begin(); q != info->self->getUnits(UnitTypes::Terran_Medic).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Yellow, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Vulture).begin(); q != info->self->getUnits(UnitTypes::Terran_Vulture).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Orange, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode).begin(); q != info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Blue, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).begin(); q != info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Cyan, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Goliath).begin(); q != info->self->getUnits(UnitTypes::Terran_Goliath).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Purple, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Science_Vessel).begin(); q != info->self->getUnits(UnitTypes::Terran_Science_Vessel).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::White, true);
	}

	for (auto b = finishSearchPos.begin(); b != finishSearchPos.end(); b++) {
		BWAPI::Broodwar->drawCircleMap(b->first, 20, Colors::Yellow, false);
		BWAPI::Broodwar->drawCircleMap(b->first, 19, Colors::Yellow, false);
		if (b->second) {
			BWAPI::Broodwar->drawCircleMap(b->first, 22, Colors::Green, false);
			BWAPI::Broodwar->drawCircleMap(b->first, 23, Colors::Green, false);
		}
		else {
			BWAPI::Broodwar->drawCircleMap(b->first, 22, Colors::Red, false);
			BWAPI::Broodwar->drawCircleMap(b->first, 23, Colors::Red, false);
		}
	}

	Position sp = Position(480, 270);
	Position np = Position(sp.x - 50, 270);

	for (int x = 0; x < totalTeamIndex; x++) {
		BWAPI::Color color = Colors::Red;
		if (x == 1) color = Colors::Yellow;
		if (x == 2) color = Colors::Green;
		if (x == 3) color = Colors::Blue;

		//printf("marineSet size = %d\n", TCV[x]->marineSet.size());
		for (auto q = TCV[x]->scvSet.begin(); q != TCV[x]->scvSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Cyan, true);
		}
		for (auto q = TCV[x]->marineSet.begin(); q != TCV[x]->marineSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->medicSet.begin(); q != TCV[x]->medicSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->vultureSet.begin(); q != TCV[x]->vultureSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->tankSet.begin(); q != TCV[x]->tankSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->goliathSet.begin(); q != TCV[x]->goliathSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->vessleList.begin(); q != TCV[x]->vessleList.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}

		BWAPI::Broodwar->drawCircleMap(TCV[x]->teamCenterPos, 10, color, true);

		CombatControl::getInstance()->draw(TCV[x]);

		BWAPI::Broodwar->drawBoxScreen(Position(np.x - 5, np.y - 4), Position(sp.x + 80, sp.y + 16), color, true);
		BWAPI::Broodwar->drawBoxScreen(Position(np.x - 3, np.y - 2), Position(sp.x + 78, sp.y + 14), Colors::Black, true);
		BWAPI::Broodwar->drawTextScreen(np, "Team ");
		BWAPI::Broodwar->drawTextScreen(sp, getControlString(TCV[x]));
		np.y += 20;
		sp.y += 20;
	}

}

char* UnitControl::getControlString(TeamControl* t) {
	if (t->state == TeamControl::STATE::BCN_START) {
		return "BCN_START";
	}
	else if (t->state == TeamControl::STATE::BCN_FRONT) {
		return "BCN_FRONT";
	}
	else if (t->state == TeamControl::STATE::BCN_FINISH) {
		return "Bacanic_Finish";
	}
	else if (t->state == TeamControl::STATE::BCN_GOING) {
		return "Bacanic_Going";
	}
	else if (t->state == TeamControl::STATE::BCN_JOIN) {
		return "Bacanic_Join";
	}
	else if (t->state == TeamControl::STATE::BCN_CLOSED) {
		return "BCN_CLOSED";
	}
	else if (t->state == TeamControl::STATE::BCN_TIGHT) {
		return "Bacanic_Tight";
	}
	else if (t->state == TeamControl::STATE::BCN_BJ) {
		return "Bacanic_BJ";
	}
	else {
		return "NONE";
	}
}

void UnitControl::changeTeam(MyUnit* unit, int team) {

	// 바꿀 팀의 리스트가 있는지 확인해여
	if (team >= totalTeamIndex) {
		return;
	}

	//팀을 바꿔줘요
	if (unit->getType() == UnitTypes::Terran_Marine) {
		onUnitDestroy(unit->self);
		unit->team = team;
		TCV[team]->marineSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Medic) {
		auto iter = TCV[unit->team]->medicSet.begin();
		auto iter2 = TCV[unit->team]->medicSet.begin();
		for (iter; iter != TCV[unit->team]->medicSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->medicSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->medicSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Vulture) {
		auto iter = TCV[unit->team]->vultureSet.begin();
		auto iter2 = TCV[unit->team]->vultureSet.begin();
		for (iter; iter != TCV[unit->team]->vultureSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->vultureSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->vultureSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		auto iter = TCV[unit->team]->tankSet.begin();
		auto iter2 = TCV[unit->team]->tankSet.begin();
		for (iter; iter != TCV[unit->team]->tankSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->tankSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->tankSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Goliath) {
		auto iter = TCV[unit->team]->goliathSet.begin();
		auto iter2 = TCV[unit->team]->goliathSet.begin();
		for (iter; iter != TCV[unit->team]->goliathSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->goliathSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->goliathSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Science_Vessel) {
		auto iter = TCV[unit->team]->vessleList.begin();
		auto iter2 = TCV[unit->team]->vessleList.begin();
		for (iter; iter != TCV[unit->team]->vessleList.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->vessleList.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->vessleList.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_SCV) {
		auto iter = TCV[unit->team]->scvSet.begin();
		auto iter2 = TCV[unit->team]->scvSet.begin();
		for (iter; iter != TCV[unit->team]->scvSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->scvSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->scvSet.push_back(unit);
	}
}

MyUnit* UnitControl::getUnit(UnitType ut, Position pos) {
	MyUnit* ret = NULL;
	for (int x = 0; x < totalTeamIndex; x++) {
		if (ut == UnitTypes::Terran_Marine) {
			float minDist = 9999;

			for (auto u : TCV[x]->marineSet) {
				float dist = getDistance(u->getPosition(), pos);
				if (dist < minDist) {
					ret = u;
					minDist = dist;
				}
			}

			auto iter = TCV[x]->marineSet.begin();
			auto iter2 = TCV[x]->marineSet.begin();
			for (iter; iter != TCV[x]->marineSet.end(); iter = iter2) {
				if ((*iter) == ret) {
					ret = (*iter);
					TCV[x]->marineSet.erase(iter);
					return ret;
				}
				iter2++;
			}
		}
		else if (ut == UnitTypes::Terran_Medic) {
			float minDist = 9999;

			for (auto u : TCV[x]->medicSet) {
				float dist = getDistance(u->getPosition(), pos);
				if (dist < minDist) {
					ret = u;
					minDist = dist;
				}
			}

			auto iter = TCV[x]->medicSet.begin();
			auto iter2 = TCV[x]->medicSet.begin();
			for (iter; iter != TCV[x]->medicSet.end(); iter = iter2) {
				if ((*iter) == ret) {
					ret = (*iter);
					TCV[x]->medicSet.erase(iter);
					return ret;
				}
				iter2++;
			}
		}
		else if (ut == UnitTypes::Terran_Vulture) {
			float minDist = 9999;

			for (auto u : TCV[x]->vultureSet) {
				float dist = getDistance(u->getPosition(), pos);
				if (dist < minDist) {
					ret = u;
					minDist = dist;
				}
			}

			auto iter = TCV[x]->vultureSet.begin();
			auto iter2 = TCV[x]->vultureSet.begin();
			for (iter; iter != TCV[x]->vultureSet.end(); iter = iter2) {
				if ((*iter) == ret) {
					ret = (*iter);
					TCV[x]->vultureSet.erase(iter);
					return ret;
				}
				iter2++;
			}
		}
		else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode || ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
			float minDist = 9999;

			for (auto u : TCV[x]->tankSet) {
				float dist = getDistance(u->getPosition(), pos);
				if (dist < minDist) {
					ret = u;
					minDist = dist;
				}
			}

			auto iter = TCV[x]->tankSet.begin();
			auto iter2 = TCV[x]->tankSet.begin();
			for (iter; iter != TCV[x]->tankSet.end(); iter = iter2) {
				if ((*iter) == ret) {
					ret = (*iter);
					TCV[x]->tankSet.erase(iter);
					return ret;
				}
				iter2++;
			}

		}
		else if (ut == UnitTypes::Terran_Goliath) {
			float minDist = 9999;

			for (auto u : TCV[x]->goliathSet) {
				float dist = getDistance(u->getPosition(), pos);
				if (dist < minDist) {
					ret = u;
					minDist = dist;
				}
			}

			auto iter = TCV[x]->goliathSet.begin();
			auto iter2 = TCV[x]->goliathSet.begin();
			for (iter; iter != TCV[x]->goliathSet.end(); iter = iter2) {
				if ((*iter) == ret) {
					ret = (*iter);
					TCV[x]->goliathSet.erase(iter);
					return ret;
				}
				iter2++;
			}
		}
		else if (ut == UnitTypes::Terran_Science_Vessel) {
			float minDist = 9999;

			for (auto u : TCV[x]->vessleList) {
				float dist = getDistance(u->getPosition(), pos);
				if (dist < minDist) {
					ret = u;
					minDist = dist;
				}
			}

			auto iter = TCV[x]->vessleList.begin();
			auto iter2 = TCV[x]->vessleList.begin();
			for (iter; iter != TCV[x]->vessleList.end(); iter = iter2) {
				if ((*iter) == ret) {
					ret = (*iter);
					TCV[x]->vessleList.erase(iter);
					return ret;
				}
				iter2++;
			}
		}
		else if (ut == UnitTypes::Terran_SCV) {
			float minDist = 9999;

			for (auto u : TCV[x]->scvSet) {
				float dist = getDistance(u->getPosition(), pos);
				if (dist < minDist) {
					ret = u;
					minDist = dist;
				}
			}

			auto iter = TCV[x]->scvSet.begin();
			auto iter2 = TCV[x]->scvSet.begin();
			for (iter; iter != TCV[x]->scvSet.end(); iter = iter2) {
				if ((*iter) == ret) {
					ret = (*iter);
					TCV[x]->scvSet.erase(iter);
					return ret;
				}
				iter2++;
			}
		}
	}

	return ret;
}

MyUnit* UnitControl::getUnit(UnitType ut) {
	// 유닛을 빼갈 수 있게 도와줘요
	MyUnit* ret = NULL;
	for (int x = 0; x < totalTeamIndex; x++) {
		if (ut == UnitTypes::Terran_Marine) {
			auto iter = TCV[x]->marineSet.begin();
			auto iter2 = TCV[x]->marineSet.begin();
			for (iter; iter != TCV[x]->marineSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->marineSet.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_Medic) {
			auto iter = TCV[x]->medicSet.begin();
			auto iter2 = TCV[x]->medicSet.begin();
			for (iter; iter != TCV[x]->medicSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->medicSet.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_Vulture) {
			auto iter = TCV[x]->vultureSet.begin();
			auto iter2 = TCV[x]->vultureSet.begin();
			for (iter; iter != TCV[x]->vultureSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->vultureSet.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode || ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
			auto iter = TCV[x]->tankSet.begin();
			auto iter2 = TCV[x]->tankSet.begin();
			for (iter; iter != TCV[x]->tankSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->tankSet.erase(iter);
				return ret;
			}

		}
		else if (ut == UnitTypes::Terran_Goliath) {
			auto iter = TCV[x]->goliathSet.begin();
			auto iter2 = TCV[x]->goliathSet.begin();
			for (iter; iter != TCV[x]->goliathSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->goliathSet.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_Science_Vessel) {
			auto iter = TCV[x]->vessleList.begin();
			auto iter2 = TCV[x]->vessleList.begin();
			for (iter; iter != TCV[x]->vessleList.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->vessleList.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_SCV) {
			auto iter = TCV[x]->scvSet.begin();
			auto iter2 = TCV[x]->scvSet.begin();
			for (iter; iter != TCV[x]->scvSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->scvSet.erase(iter);
				return ret;
			}
		}
	}

	return ret;
}

void UnitControl::getUnit(MyUnit* unit) {
	// 유닛을 빼갈 수 있게 도와줘요

	UnitType ut = unit->getType();

	for (int x = 0; x < totalTeamIndex; x++) {
		if (ut == UnitTypes::Terran_Marine) {
			auto iter = TCV[x]->marineSet.begin();
			auto iter2 = TCV[x]->marineSet.begin();
			for (iter; iter != TCV[x]->marineSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->marineSet.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_Medic) {
			auto iter = TCV[x]->medicSet.begin();
			auto iter2 = TCV[x]->medicSet.begin();
			for (iter; iter != TCV[x]->medicSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->medicSet.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_Vulture) {
			auto iter = TCV[x]->vultureSet.begin();
			auto iter2 = TCV[x]->vultureSet.begin();
			for (iter; iter != TCV[x]->vultureSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->vultureSet.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode || ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
			auto iter = TCV[x]->tankSet.begin();
			auto iter2 = TCV[x]->tankSet.begin();
			for (iter; iter != TCV[x]->tankSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->tankSet.erase(iter);
					return;
				}
			}

		}
		else if (ut == UnitTypes::Terran_Goliath) {
			auto iter = TCV[x]->goliathSet.begin();
			auto iter2 = TCV[x]->goliathSet.begin();
			for (iter; iter != TCV[x]->goliathSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->goliathSet.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_Science_Vessel) {
			auto iter = TCV[x]->vessleList.begin();
			auto iter2 = TCV[x]->vessleList.begin();
			for (iter; iter != TCV[x]->vessleList.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->vessleList.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_SCV) {
			auto iter = TCV[x]->scvSet.begin();
			auto iter2 = TCV[x]->scvSet.begin();
			for (iter; iter != TCV[x]->scvSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->scvSet.erase(iter);
					return;
				}
			}
		}
	}
}

void UnitControl::returnUnit(MyUnit* unit) {
	// 팀을 먼저 분배해 줘요
	int teamIndex = 0;

	if (unit->getType() == UnitTypes::Terran_Marine) {
		TCV[teamIndex]->marineSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Medic) {
		TCV[teamIndex]->medicSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Vulture) {
		TCV[teamIndex]->vultureSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		TCV[teamIndex]->tankSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Goliath) {
		TCV[teamIndex]->goliathSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Science_Vessel) {
		TCV[teamIndex]->vessleList.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_SCV) {
		TCV[teamIndex]->scvSet.push_back(unit);
	}
}

void UnitControl::onUnitCreate(Unit unit) {
	if (unit == nullptr) return;
}

void UnitControl::onUnitComplete(Unit unit) {
	//printf("유닛 추가해줄거에염\n");

	if (unit == nullptr) return;
	// 유닛이 완성되면 팀에 넣어줘요

	//일단 0번팀에 넣어줘요
	UnitType ut = unit->getType();
	if (ut == UnitTypes::Terran_SCV) return;

	int teamIndex = 0;
	//조이기에 들어갔으면
	if (TCV[0]->state == TeamControl::STATE::BCN_TIGHT || TCV[0]->state == TeamControl::STATE::BCN_FINISH) {
		teamIndex = 1;
	}

	if (ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		for (int x = 0; x < totalTeamIndex; x++) {
			for (auto t = TCV[x]->tankSet.begin(); t != TCV[x]->tankSet.end(); t++) {
				if ((*t)->self->getID() == unit->getID()) {
					return;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		for (int x = 0; x < totalTeamIndex; x++) {
			for (auto t = TCV[x]->tankSet.begin(); t != TCV[x]->tankSet.end(); t++) {
				if ((*t)->self->getID() == unit->getID()) {
					return;
				}
			}
		}
	}

	if (ut == UnitTypes::Terran_Marine) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->marineSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Medic) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->medicSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Vulture) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->vultureSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->tankSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Goliath) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->goliathSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Science_Vessel) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->vessleList.push_back(m);
	}
}

void UnitControl::onUnitDestroy(Unit unit) {
	// 애가 뒤지면 리스트에서 없애줘요
	UnitType ut = unit->getType();
	if (ut == UnitTypes::Terran_SCV) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->scvSet.begin();
			for (iter; iter != TCV[x]->scvSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->scvSet.erase(iter);
					iter--;
				}
			}
		}
	}
	if (ut == UnitTypes::Terran_Marine) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->marineSet.begin();
			for (iter; iter != TCV[x]->marineSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->marineSet.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Medic) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->medicSet.begin();
			for (iter; iter != TCV[x]->medicSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->medicSet.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Vulture) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->vultureSet.begin();
			for (iter; iter != TCV[x]->vultureSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->vultureSet.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Tank_Mode || ut == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->tankSet.begin();
			for (iter; iter != TCV[x]->tankSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->tankSet.erase(iter);
					iter--;
				}
			}
		}

	}
	else if (ut == UnitTypes::Terran_Goliath) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->goliathSet.begin();
			for (iter; iter != TCV[x]->goliathSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->goliathSet.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Science_Vessel) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->vessleList.begin();
			for (iter; iter != TCV[x]->vessleList.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->vessleList.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_SCV) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->scvSet.begin();
			for (iter; iter != TCV[x]->scvSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->scvSet.erase(iter);
					iter--;
				}
			}
		}
	}
}

int UnitControl::getTeamTankDanger(TeamControl* t) {
	int ret = 0;
	Unitset enemies = CombatControl::getInstance()->getNearEnemyUnits(t);

	float mindist = 99999;
	for (auto q = (*t).tankSet.begin(); q != (*t).tankSet.end(); q++) {
		for (auto e = enemies.begin(); e != enemies.end(); e++) {
			float dist = getDistance((*q)->getPosition(), (*e)->getPosition());
			if (dist < mindist) {
				mindist = dist;
			}
		}
	}
	//printf("3 - %.2f // 2 - %.2f // 1 - %.2f\n", (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 1.0f), (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 2.0f), (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 3.0f));
	//printf("minDist : %.2f\n", mindist);
	if (mindist < (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 1.0f)) {
		ret = 3;
	}
	else if (mindist < (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 2.0f)) {
		ret = 2;
	}
	else if (mindist < (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 3.0f)) {
		ret = 1;
	}
	return ret;
}

void UnitControl::updateTeam() {


}


Position UnitControl::getTeamCenterPos(TeamControl* t) {

	int tx = 0;
	int ty = 0;

	int count = 0;

	for (auto q : t->marineSet) {
		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;
		count++;
	}
	for (auto q : t->medicSet) {
		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;
		count++;
	}
	for (auto q : t->tankSet) {
		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;

		count++;
	}
	if (count == 0) {
		return info->map->getMyStartBase()->getPosition();
	}
	else {
		return Position(tx / count, ty / count);
	}
}

Position UnitControl::getBionicCenterPos(TeamControl* t) {

	Position ret = Position(0, 0);

	int tx = 0;
	int ty = 0;

	int count = 0;

	for (auto q : t->marineSet) {
		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;

		count++;
	}
	if (count != 0) {
		ret = Position(tx / count, ty / count);
	}

	return ret;
}

Position UnitControl::getTankCenterPos(TeamControl* t) {

	Position ret = t->tankCenterPos;

	int tx = 0;
	int ty = 0;

	int count = 0;

	for (auto q : t->tankSet) {
		auto near = CombatControl::getInstance()->getMyNearUnit(t->tankSet, q, false);
		if (near != nullptr && 70 < getDistance(q->getPosition(), near->getPosition())){
			continue;
		}

		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;

		count++;
	}
	if (count != 0) {
		ret = Position(tx / count, ty / count);
	}
	else {
		if (t->tankSet.size() > 0) {
			float minDist = 999999;
			MyUnit * near;
			for (auto tank : t->tankSet) {
				float dist = getDistance(tank->getPosition(), t->attackPosition);
				if (minDist > dist) {
					near = tank;
					minDist = dist;
				}
			}

			near->isInTeam = true;
			ret = near->getPosition();
		}
	}

	return ret;
}

Position UnitControl::getAttackPosition(TeamControl* t) {
	Position p;
	if (info->map->getEnemyStartBase() != NULL) {
		p = info->map->getEnemyStartBase()->getPosition();
	}
	else {
		p = info->map->getMyStartBase()->getPosition();
	}
	 
	if (finishSearchPos[p] == false) {
		return p;
	}

	if (info->enemy->enemyGroup.size() > 0) {
		Position ap;
		float minDist = 9999.9f;
		for (auto q = info->enemy->enemyGroup.begin(); q != info->enemy->enemyGroup.end(); q++) {
			float dist = getDistance(t->tankCenterPos, (*q).first);
			if (minDist > dist) {
				minDist = dist;
				ap = (*q).first;
			}
		}
		p = ap;
	}
	else {
		BWTA::BaseLocation* base = NULL;
		int maxValue = 0;
		for (auto q = info->map->BLI.begin(); q != info->map->BLI.end(); q++) {
			if ((*q).second->player == 0) {
				int value = (*q).second->value;
				if (value > maxValue) {
					maxValue = value;
					base = (*q).first;
				}
			}
		}

		if (base == NULL) {
			if (info->enemy->getAllBuildings().size() == 0 && info->enemy->getUnitCount(UnitTypes::Protoss_Photon_Cannon) == 0) {
				p = finishNextPos;
				if (getDistance(p, t->teamCenterPos) < 100) {
					updateFinishSearch();
				}
			}
			else {
				for (auto b : info->enemy->getAllBuildings()) {
					p = info->enemy->getPosition(b);
					break;
				}
				for (auto b : info->enemy->getUnits(UnitTypes::Protoss_Photon_Cannon)) {
					p = info->enemy->getPosition(b);
					break;
				}
			}
		}
		else {
			if (info->enemy->getAllBuildings().size() != 0) {
				bool check = false;
				for (auto b : info->enemy->getAllBuildings()) {
					p = info->enemy->getPosition(b);
					check = true;
					break;
				}

				if (!check) {
					p = finishNextPos;
				}
			}
			else {
				p = base->getPosition();
			}
		}
	}


	return p;
}

void UnitControl::setTeamDicision() {
	for (auto marine : TCV[1]->marineSet) {
		float dist = getDistance(marine->getPosition(), (Position)CombatControl::getInstance()->tightPos);
		if (dist < 100) {
			onUnitDestroy(marine->self);
			changeTeam(marine, 0);
		}
	}
	for (auto medic : TCV[1]->medicSet) {
		float dist = getDistance(medic->getPosition(), (Position)CombatControl::getInstance()->tightPos);
		if (dist < 100) {
			onUnitDestroy(medic->self);
			changeTeam(medic, 0);
		}
	}
	for (auto tank : TCV[1]->tankSet) {
		float dist = getDistance(tank->getPosition(), (Position)CombatControl::getInstance()->tightPos);
		if (dist < 100) {
			onUnitDestroy(tank->self);
			changeTeam(tank, 0);
		}
	}

	/*if (changeTeamSetting) {
		if (TCV[1]->state == TeamControl::STATE::BCN_FRONT) {
			for (auto marine : TCV[2]->marineSet) {
				onUnitDestroy(marine->self);
				changeTeam(marine, 1);
			}
			for (auto medic : TCV[2]->medicSet) {
				onUnitDestroy(medic->self);
				changeTeam(medic, 1);
			}
			for (auto tank : TCV[2]->tankSet) {
				onUnitDestroy(tank->self);
				changeTeam(tank, 1);
			}
		}

		int t2ma = TCV[2]->marineSet.size();
		int t2me = TCV[2]->medicSet.size();
		int t2ta = TCV[2]->tankSet.size();

		if (t2ma == 0 && t2me == 0 && t2ta == 0) changeTeamSetting = false;
	}*/
}

// 충분한 병력이 모였는?
bool UnitControl::isReadyGoing() {
	bool ret = false;

	int marineCount = 0;
	int tankCount = 0;

	marineCount = info->self->getUnitCount(UnitTypes::Terran_Marine);
	tankCount = info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode) + info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode);

	if (marineCount > 8 && tankCount > 5) ret = true;
	return ret;
}

// 적 다리에 도착했는지?
bool UnitControl::isReadyTight(int teamNumber) {
	bool ret = false;

	float dist = getDistance((Position)CombatControl::getInstance()->tightPos, TCV[teamNumber]->teamCenterPos);

	if (dist < 120) {
		ret = true;
	}

	return ret;
}

//병준형을 견제하는 방법
bool UnitControl::isReadyBJ() {
	bool ret = false;

	int count = 0;
	for (auto base = info->map->BLI.begin(); base != info->map->BLI.end(); base++) {
		if (base->second->player == 0) {
			if (base->first != info->map->getEnemyStartBase()) {
				count++;
			}
		}
	}

	if (TCV.size() > 2) {
		// 이미 견제 팀이 만들어 진 경우
		ret = false;
	}
	if (count > 1) {
		ret = true;
	}

	return false;
}

// 용사여 적을 끝낼 준비가 되었나!!!!!
bool UnitControl::isReadyFinish() {
	bool ret = false;

	if (info->map->getEnemyStartBase() == NULL) {
		ret = false;
	}
	else {
		for (auto eb : info->enemy->getAllBuildings()) {
			if (BWTA::getRegion(info->enemy->getPosition(eb)) != BWTA::getRegion(info->map->getEnemyStartBase()->getPosition())) {
				ret = false;
				break;
			}
			ret = true;
		}

		if (ret) {
			// 여기서 유불리 판단 불리하면 false 유리하면 true
		}
	}
	//return ret;
	return true;
}

// 밀다가 불리할 때?
bool UnitControl::isUnReadyFinish() {
	bool ret = false;

	// 여기서 유불리 판단 불리하면 true 유리하면 false

	return ret;
}

void UnitControl::updateFinishSearch() {
	if (finishNextPos.x < 1) {
		finishNextPos = info->map->getEnemyStartBase()->getPosition();
		for (auto c : info->self->getBuildings(UnitTypes::Terran_Command_Center)) {
			finishSearchPos[c->getPosition()] = true;
		}
	}

	for (auto c : info->self->getBuildings(UnitTypes::Terran_Command_Center)) {
		finishSearchPos[c->getPosition()] = true;
	}

	float minDist = 99999;
	for (auto unit : info->self->getAllUnits()) {
		float dist = getDistance(unit->getPosition(), finishNextPos);
		if (dist < minDist) {
			minDist = dist;
		}
	}
	if (minDist < 60) {
		finishSearchPos[finishNextPos] = true;
		CombatControl::getInstance()->unduckWait = false;
	}

	if (finishSearchPos[finishNextPos]) {
		float dist = 99999;
		if (info->enemy->getAllBuildings().size() == 0) {
			float minDist = 9999;
			for (auto pos = finishSearchPos.begin(); pos != finishSearchPos.end(); pos++) {
				if (pos->second) continue;
				float di = getDistance(pos->first, TCV[0]->teamCenterPos);

				if (di < minDist) {
					minDist = di;
					finishNextPos = pos->first;
				}
			}
		}
	}

	bool checkall = true;
	for (auto pos = finishSearchPos.begin(); pos != finishSearchPos.end(); pos++) {
		if (pos->second == false) checkall = false;
	}

	if (checkall) {
		for (auto pos = finishSearchPos.begin(); pos != finishSearchPos.end(); pos++) {
			pos->second = false;
		}
		for (auto c : info->self->getBuildings(UnitTypes::Terran_Command_Center)) {
			finishSearchPos[c->getPosition()] = true;
		}
	}
}