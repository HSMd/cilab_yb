#include "DefenceZealot2.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

DefenceZealot2::DefenceZealot2(DEFENSE_TYPE type) : DefenceEvent(type)
{	
	closeFind = false;
}

DefenceZealot2::~DefenceZealot2()
{

}

void DefenceZealot2::chkFire(){

	if (0 < info->map->getCloseCount(UnitTypes::Protoss_Zealot) || 0 < info->map->getCloseCount(UnitTypes::Protoss_Dragoon) || 0 < info->map->getCloseCount(UnitTypes::Protoss_Dark_Templar)){
		closeFind = true;
	}

	if (!closeFind && state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
		fire = true;
	}

	/*
	int zealotCount = 0;
	for (auto territory : info->map->getMyTerritorys())
	{
		zealotCount += territory->getCloseCount(UnitTypes::Protoss_Zealot);
	}

	if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1) && (info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) + info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode) < 1)){
		if (0 < zealotCount){
			fire = true;
		}
	}
	*/
}

void DefenceZealot2::update(){

	getUnits(UnitTypes::Terran_Marine);

	/*
	//sample
	auto zealotCount = info->map->getMyTerritorys()[1]->getCloseCount(UnitTypes::Protoss_Zealot);

	if (zealotCount == 1){
		getUnits(UnitTypes::Terran_Marine);
	}

	//2마리 이상
	else
	{
		getUnits(UnitTypes::Terran_Marine);

		if (defenceUnits[UnitTypes::Terran_Marine].size() * 2 < zealotCount){
			if (scheduler->canAddUnitType(UnitTypes::Terran_Marine)){
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Marine), Scheduler::BUILD_SPEED::FAST);
			}
		}
	}
	//}
	*/
}

void DefenceZealot2::run()
{	
	
	chkFire();
	turnOff();
	if (!fire){
		return;
	}	
	update();
	act();
}

void DefenceZealot2::act(){

	auto& close = info->map->getCloseEnemy();
	
	Unit attackTarget = nullptr;

	// 타겟 선정. - 파일런, 게이트, 프로브, 포토
	for (auto enemy : close){
		if (attackTarget == nullptr)
		{
			if (enemy->getType() == UnitTypes::Protoss_Photon_Cannon)
			{
				if (!enemy->isCompleted() || !enemy->exists())
				{
					continue;
				}
				else{
					attackTarget = enemy;
					break;
				}
			}
			else{
				attackTarget = enemy;
			}
			
		}
		else{
			if (enemy->getType() == UnitTypes::Protoss_Photon_Cannon)
			{
				if (!enemy->isCompleted() || !enemy->exists())
				{
					continue;
				}
				else{
					attackTarget = enemy;
					break;
				}
			}
			else if (enemy->getType() == UnitTypes::Protoss_Pylon)
			{
				if (attackTarget->getType() == UnitTypes::Protoss_Pylon)
				{
					if (enemy->isCompleted() && enemy->exists())
					{
						if (enemy->getHitPoints() < attackTarget->getHitPoints())
						{
							attackTarget = enemy;
						}
					}
				}
				else{
					attackTarget = enemy;
				}
			}
			else if (enemy->getType() == UnitTypes::Protoss_Probe)
			{
				if (attackTarget->getType() == UnitTypes::Protoss_Pylon)
				{
					continue;
				}
				else if (attackTarget->getType() == UnitTypes::Protoss_Probe)
				{
					if (enemy->getHitPoints() < attackTarget->getHitPoints())
					{
						attackTarget = enemy;
					}
				}
			}
		}
	}

	if (attackTarget == nullptr){
		// 앞마당 홀드 위치로 가있으면 되고.
		for (auto iter : defenceUnits)
		{
			UnitType type = iter.first;

			for (auto unit : defenceUnits[type])
			{
				if (!unit->self->exists() || !unit->self->isCompleted()){
					continue;
				}
				if (unit->self->getType() == UnitTypes::Terran_Marine)
				{
					if (!isAlmostReachedDest(unit->self->getPosition(), info->map->getMyTerritorys()[1]->getPosition(), 3 * TILE_SIZE, 3 * TILE_SIZE))
					{
						if (bw->getFrameCount() % 24 == 0)
						{
							//unit->self->move(info->map->getMyTerritorys()[1]->getPosition());
							unit->self->move((Position)posMgr->getTilePosition(POS::FRONT_BARRACK));
						}
					}
				}
			}
		}
		
	}
	else{
		
		for (auto iter : defenceUnits)
		{
			UnitType type = iter.first;

			for (auto unit : defenceUnits[type])
			{
				if (attackTarget->getType() == UnitTypes::Protoss_Photon_Cannon)
				{
				/*	Position p;
					p.x = (info->map->getMyTerritorys()[0]->getPosition().x + info->self->getBuildings(UnitTypes::Terran_Barracks)[0]->getPosition().x) / 2;
					p.y = (info->map->getMyTerritorys()[0]->getPosition().y + info->self->getBuildings(UnitTypes::Terran_Barracks)[0]->getPosition().y) / 2;
					unit->self->move(p);*/
					closeFind = true;
				}
				else{
					if (!unit->self->exists() || !unit->self->isCompleted()){
						continue;
					}
					if (!unit->self->isAttacking())
					{
						unit->self->attack(attackTarget);
					}
				}
			}
		}
	}
	/*for (auto iter : defenceUnits){
		UnitType type = iter.first;
		
		for (auto unit : defenceUnits[type]){

			if (!unit->self->exists() || !unit->self->isCompleted()){
				continue;
			}
			if (unit->self->getType() == UnitTypes::Terran_Marine)
			{
				if (!unit->self->isHoldingPosition())
				{
					unit->self->move(unit->targetPosition);
				}
				if (isAlmostReachedDest(unit->self->getPosition(), unit->targetPosition, 3, 3))
				{
					if (!unit->self->isHoldingPosition())
					{
						unit->self->holdPosition();
					}
				}
			}
		}
	}*/
}

void DefenceZealot2::turnOff(){

	//auto zealotCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Zealot);
	/*
	int zealotCount = 0;
	for (auto territory : info->map->getMyTerritorys())
	{
		zealotCount += territory->getCloseCount(UnitTypes::Protoss_Zealot);
	}

	if (zealotCount == 0){
		eraseUnits(UnitTypes::Terran_Marine);
		eraseUnits(UnitTypes::Terran_SCV);

		fire = false;
	}
	*/

	if (closeFind || state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2)){
		eraseUnits(UnitTypes::Terran_Marine);
		fire = false;
	}
}

float DefenceZealot2::TargetValue(UnitType ut)
{
	float value = 0.0f;


	return value;
}