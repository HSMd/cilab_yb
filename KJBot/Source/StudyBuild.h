 #pragma once
#include "BWAPI.h"
#include "MyUnit.h"
#include "Work.h"

#include <map>
#include <vector>

using namespace BWAPI;
using namespace std;

class StudyBuild{
	enum FLAG{
		GAS_CONTROL_1,
		GAS_CONTROL_2,
		SCV_INSERT,

		SIEGE_MODE,

		EXPAND_CMD,
		EXPAND_FRONT,

		ERROR_OUT,
		OUT_SCV,

		SEARCH,
		ATTAK,

		JUDGE_2COMMAND,
		JUDGE_3FACTORY,
		JUDGE_2REFINERY		
	};

	int supplyFrame;

	vector<BuildWork> builds;
	vector<MyUnit*> moveBuildings;

	map<FLAG, bool> flags;

	MyUnit* performer;
	MyUnit* performer2;

public: 
	StudyBuild(){};
	~StudyBuild(){};

	void init();
	void show();
	void draw();
	void run();

	void scvCreate();
	void runBuildWork();

	void outScv();
	void search();
	void expandFront();
	void gasControl();

	void guardChk();
	void move();
};