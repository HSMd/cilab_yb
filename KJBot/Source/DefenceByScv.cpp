#include "DefenceByScv.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

DefenceByScv::DefenceByScv(DEFENSE_TYPE type) : DefenceEvent(type)
{	
	
}

DefenceByScv::~DefenceByScv()
{

}

void DefenceByScv::chkFire(){
	
	if (info->map->getMyTerritorys().back()->getReady()){

		int ourSize = (info->self->getUnitCount(UnitTypes::Terran_Marine) / 2) + ((info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) + info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode)) * 2) ;
		int enemySize = info->map->getCloseEnemy().size() - info->map->getCloseCount(UnitTypes::Protoss_Observer);

		int require = enemySize - ourSize;

		if (0 < require){
			fire = true;
		}
	}
}

void DefenceByScv::update(){
	
	int ourSize = (info->self->getUnitCount(UnitTypes::Terran_Marine) / 2) + ((info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) + info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode)) * 2);
	int enemySize = info->map->getCloseEnemy().size();

	int require = (enemySize - ourSize) * 3;

	//bw << "SCV Count : " << defenceUnits[UnitTypes::Terran_SCV].size() << ", Require : " << require << endl;

	int max = 8;
	if (max < require * 3){
		getUnits(UnitTypes::Terran_SCV, max);
	}
	else{
		getUnits(UnitTypes::Terran_SCV, require * 3);
	}	
}

void DefenceByScv::run()
{	
	chkFire();
	turnOff();
	if (!fire){
		return;
	}	

	update();
	act();
}

void DefenceByScv::act(){

	for (auto iter : defenceUnits){
		UnitType type = iter.first;

		for (auto unit : defenceUnits[type]){

			if (!unit->self->exists() || !unit->self->isCompleted()){
				continue;
			}

			if (!unit->self->isAttackFrame())
				unit->self->attack(unit->self->getClosestUnit(Filter::IsEnemy, 32 * 40));
		}
	}
}

void DefenceByScv::turnOff(){

	int enemySize = info->map->getCloseEnemy().size();

	if (enemySize == 0){
		eraseUnits(UnitTypes::Terran_SCV);

		fire = false;
	}	
}