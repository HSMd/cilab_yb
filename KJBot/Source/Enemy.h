#pragma once
#include <BWAPI.h>
#include <map>
#include <vector>

using namespace BWAPI;
using namespace std;

class BuildInfo {
private:
	enum BUILD {
		DOUBLE_NEXUS,
		ZEALOT_ALLIN,
		ZEALOT_DRAGOON_ALLIN,
		SHUTTLE_REAVER,
		SHUTTLE_DARK,
		SHUTTLE_ZEALOT,
		ONE_GATE_OBSERVER_DOUBLE,
		FAST_DARK,
		TWO_GATE_DOUBLE,
		ONE_GATE_DOUBLE,
		THREE_GATE_OBSERVER_DRAGOON,
		JUNJIN_GATE,
		PHOTO_RUSH,
	};
	
	map<BUILD, float> buildScore;

public:
	vector<std::pair<BUILD, vector<std::pair<UnitType, int>>>> buildFrame;
	vector<std::pair<BUILD, vector<std::pair<UnitType, float>>>> frameScore;
	vector<std::pair<UnitType, int>> enemyBuildFrame;
	void setBuildFrame();
	BuildInfo();
	std::pair<UnitType, bool> setEnemyBuildVector(std::pair<UnitType, bool>);
	void update();
	void draw();
	BUILD build;
};

class EnemyGroup {
private:
	enum TYPE {
		ALL_ZEALOT,
		MOST_ZEALOT,
		MOST_DRAGOON,
		BALANCE,
	};

public:
	Position groupPos;
	vector<Unit>& groupUnits;
	int range;
	int level;
	TYPE type;
	void setTYPE(TYPE t) {
		type = t;
	}
	TYPE getType() {
		return type;
	}
};

class Enemy{

	map<UnitType, vector<Unit>> units;
	map<UnitType, vector<Unit>> buildings;

	map<Unit, Position> unitPos;
	map<Unit, UnitType> unitType;

	vector<Unit> allUnits;
	vector<Unit> allBuildings;

public:
	Enemy();
	~Enemy();

	BuildInfo* bif;

	void show(int startX, int startY);
	void update();
	void draw();
	void destroy(Unit unit);
	void morph(Unit unit);
	void create(Unit unit);

	int getUnitCount(UnitType type);
	int getBuildingCount(UnitType type);

	vector<Unit>& getUnits(UnitType type);
	vector<Unit>& getBuildings(UnitType type);

	vector<Unit>& getAllUnits();
	vector<Unit>& getAllBuildings();

	vector<pair<Position, pair<int, int>>> enemyGroup;						//[GroupPosition, [Range, GroupLevel]]
	vector<pair<Position, pair<int, int>>> enemyGroundGroup;				//[GroupPosition, [Range, GroupLevel]]

	Position getPosition(Unit unit){ return unitPos[unit]; };
	UnitType getType(Unit unit) { return unitType[unit]; };

	bool isAlready(Unit unit, bool isBuilding);
	bool hasFlyer() {
		if (getUnits(UnitTypes::Protoss_Scout).size() > 0 || getUnits(UnitTypes::Protoss_Observer).size() > 0 || getUnits(UnitTypes::Protoss_Carrier).size() > 0 || getUnits(UnitTypes::Protoss_Shuttle).size() > 0 || getUnits(UnitTypes::Protoss_Arbiter).size() > 0) return true;
		else return false;
	};
	void delData(vector<Unit>& data, Unit unit);
};