#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "DefenceEvent.h"

using namespace std;


class DefenceReaver : public DefenceEvent
{	
	
public:

	DefenceReaver(DEFENSE_TYPE type);
	~DefenceReaver();

	//이벤트 시작
	void chkFire();
	//이벤트 종료
	void turnOff();

	//유닛 가져오기(제한두고)
	void update();
	//run
	void run();

	//update후 코드
	void act();
};

