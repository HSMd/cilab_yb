#include "Bacanic_2.h"
#include "Instance.h"
#include "Work.h"
#include "Utility.h"

void Bacanic2::init(){

	auto brkPos = posMgr->getTilePosition(POS::THIRD_FACTORY);
	posMgr->planUpload(UnitTypes::Terran_Barracks, brkPos);

	auto front = info->map->getCandidateLocation();
	Territory* territory = new Territory(front);
	info->map->addMyTerritory(territory);

	performer = nullptr;
	spare = nullptr;
	frameCount = 0;	
	
	progress = BUILD_START;
}

void Bacanic2::show(){

}

void Bacanic2::draw(){

}

void Bacanic2::run(){

	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true) && 1 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, true);
	}

	buildProcess();

	runBuild();

	scvCreate();

	autoUnit();

	search();

	gasControl();

	simOut();

	expand();
}

void Bacanic2::autoUnit(){

	if (flags[ADD_CENTER]){
		flags[AUTO_MARINE] = true;
	}
	
	if (flags[AUTO_TANK]){
		for (auto& fac : info->self->getBuildings(UnitTypes::Terran_Factory)){
			if (fac->self->getAddon() == nullptr){
				continue;
			}

			if (scheduler->getReadyTrain(fac->self) && scheduler->canAddUnitType(UnitTypes::Terran_Siege_Tank_Tank_Mode))
				fac->self->train(UnitTypes::Terran_Siege_Tank_Tank_Mode);
		}
	}

	if (4 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks)){
		flags[AUTO_MEDIC] = true;

		if (info->self->getUnitCount(UnitTypes::Terran_SCV) < 7){
			flags[AUTO_MEDIC] = false;
		}
	}

	if (flags[AUTO_MEDIC]){
		for (auto& ba : info->self->getBuildings(UnitTypes::Terran_Barracks)){
			if (scheduler->getReadyTrain(ba->self) && scheduler->canAddUnitType(UnitTypes::Terran_Medic))
				if (info->self->getUnitCount(UnitTypes::Terran_Medic, true) * 4 < info->self->getUnitCount(UnitTypes::Terran_Marine, true))
					ba->self->train(UnitTypes::Terran_Medic);
		}
	}

	if (flags[AUTO_MARINE]){
		for (auto& ba : info->self->getBuildings(UnitTypes::Terran_Barracks)){
			if (scheduler->getReadyTrain(ba->self) && scheduler->canAddUnitType(UnitTypes::Terran_Marine))
				ba->self->train(UnitTypes::Terran_Marine);
		}
	}

	if (flags[AUTO_SUPPLY]){
		if (0 != scheduler->getUnitTypeRunCount(UnitTypes::Terran_Supply_Depot) || !scheduler->canAddUnitType(UnitTypes::Terran_Supply_Depot))
			return;

		if (Broodwar->self()->supplyTotal() < 200 * 2 && frameCount + 900 < bw->getFrameCount()){

			if (info->self->getBuildingCount(UnitTypes::Terran_Machine_Shop, true) == 1){
				if (flags[ADD_CENTER]){
					if (scheduler->getSupply() <= 8 * 2){
						scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
						frameCount = bw->getFrameCount();
					}
				}
				else{
					if (scheduler->getSupply() <= 2 * 2){
						scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
						frameCount = bw->getFrameCount();
					}
				}				
			}
			else if (info->self->getBuildingCount(UnitTypes::Terran_Machine_Shop, true) == 2){
				if (flags[ADD_CENTER]){
					if (scheduler->getSupply() <= 9 * 2){
						scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
						scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
						frameCount = bw->getFrameCount();
					}
				}
				else{
					if (scheduler->getSupply() <= 8 * 2){
						scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
						frameCount = bw->getFrameCount();
					}
				}
			}
		}
	}
}

void Bacanic2::scvCreate(){

	auto& cnters = info->self->getBuildings(UnitTypes::Terran_Command_Center);
	auto& scvs = info->self->getImmediatelyUnit(UnitTypes::Terran_SCV);

	// SCV 뽑기.
	if (flags[ADD_CENTER]){
		for (auto territory : info->map->getMyTerritorys()){
			if (info->map->getMyTerritorys().back() == territory && 0 < territory->getCloseEnemy().size()){

				continue;
			}

			if (0 < scheduler->getUnitTypeCount(UnitTypes::Terran_Comsat_Station) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Comsat_Station)){
				return;
			}

			if (!territory->getReady()){
				continue;
			}

			int require = territory->getRequireSCVCount();
			auto cnter = territory->getCmdCenter()->self;

			if (1 <= require && scheduler->getReadyTrain(cnter))
				cnter->train(UnitTypes::Terran_SCV);
		}
	}
	else{
		if (0 < scheduler->getUnitTypeCount(UnitTypes::Terran_Comsat_Station) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Comsat_Station)) {
			return;
		}

		if (progress == BUILD_START){
			if (scvs.size() == 15){
				if (1 <= info->self->getBuildingCount(UnitTypes::Terran_Factory, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			if (scvs.size() == 20){
				if (3 <= info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			/*
			else if (scvs.size() == 20){
				if (3 <= info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			*/
			else{
				if (scheduler->getReadyTrain(cnters.front()->self))
					cnters.front()->self->train(UnitTypes::Terran_SCV);
			}
		}
		else if (progress == BUILD_MID){
			if (scvs.size() == 19){
				if (3 <= info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			else{
				if (scheduler->getReadyTrain(cnters.front()->self))
					cnters.front()->self->train(UnitTypes::Terran_SCV);
			}
		}
		else{
			if (flags[EXPAND_MUST]){
				if (0 < info->self->getBuildingCount(UnitTypes::Terran_Academy) && info->self->getBuildingCount(UnitTypes::Terran_Comsat_Station) < 1){
					cnters.front()->self->cancelTrain(2);
					return;
				}
			}			

			if (scheduler->getReadyTrain(cnters.front()->self))
				cnters.front()->self->train(UnitTypes::Terran_SCV);
		}
	}


	// 스케쥴러에 Run Scv 관리.
	for (auto run : scheduler->getRunning()){
		if (run->performer != nullptr){
			if (!run->performer->self->exists()){
				auto performer = info->map->getMyTerritorys().front()->getWorker(false, run->pos);
				run->setPerformer(performer);
			}
		}
	}

	// 확장 완료후 SCV가 본진에만 많을 경우 옮기는 코드.
	if (flags[ADD_CENTER]){
		if (14 < info->map->getMyTerritorys().front()->getScvCount() && info->map->getMyTerritorys().back()->getScvCount() < 4){
			int size = info->map->getMyTerritorys().front()->getScvCount() - 14;
			for (int i = 0; i < size; i++){
				auto worker = info->map->getMyTerritorys().front()->getWorker(true);
				info->map->getMyTerritorys().back()->addScv(worker);
			}
		}
	}
}

void Bacanic2::search(){

	// 정찰.
	if (!flags[SEARCH] && 11 <= info->self->getUnitCount(UnitTypes::Terran_SCV)){
		//if (!flags[SEARCH] && 5 <= info->self->getUnitCount(UnitTypes::Terran_SCV)){

		auto worker = info->map->getMyTerritorys()[0]->getWorker(true);
		scouting->setSpy(worker);

		if (worker != nullptr)
			flags[SEARCH] = true;
	}
}

void Bacanic2::gasControl(){

	auto startTerritory = info->map->getMyTerritorys().front();
	auto front = info->map->getMyTerritorys().back();

	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center, true)){
		flags[GAS_CONTROL] = true;
	}

	if (!flags[GAS_CONTROL]){
		if (88 <= bw->self()->gas()){
			startTerritory->setGasScvCount(2);
		}

		if (96 <= bw->self()->gas()){
			startTerritory->setGasScvCount(1);
		}
	}
	else{		
		startTerritory->setGasScvCount(3);

		if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Refinery)){
			if (20 < info->self->getUnitCount(UnitTypes::Terran_SCV)){
				if (270 < bw->self()->gas()){
					front->setGasScvCount(1);
				}
				else{
					front->setGasScvCount(2);
				}
			}
		}

		if (startTerritory->getReady()){
			if (350 < bw->self()->gas()){
				startTerritory->setGasScvCount(1);
			}

			if (4 < startTerritory->getRequireSCVCount())
				startTerritory->setGasScvCount(0);
		}

		if (front->getReady() && 4 < front->getRequireSCVCount()){
			front->setGasScvCount(0);
		}
	}
}

void Bacanic2::simOut(){
	
	if (!state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1) || flags[ERROR_OUT]){
		return;
	}

	auto startTerritory = info->map->getMyTerritorys().front();
	auto chkPoint = getNearestChokepoint(startTerritory->getTilePosition());
	auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
	bool safe = info->map->getCloseEnemy().size() == 0;

	static bool isRun = false;

	if (performer == nullptr){
		performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_SUPPLY2));
	}
	else{
		if (performer->self->exists() && !performer->self->isConstructing()){
			if (!brk->self->isLifted()){
				if (performer->self->exists() && getDistance(startTerritory->getTilePosition(), performer->getTilePosition()) <= 16){
					flags[ERROR_OUT] = true;
					return;
				}

				if (getDistance(chkPoint->getCenter(), performer->getPosition()) < 70){
					performer->state = MyUnit::STATE::OUT;
					isRun = true;

					if (brk->self->isIdle() && safe){
						brk->self->lift();
					}
				}
				else{
					if (isRun && brk->self->isIdle()){
						flags[ERROR_OUT] = true;
					}
				}
			}
			else{
				if (performer->self->exists() && getDistance(startTerritory->getTilePosition(), performer->getTilePosition()) <= 18){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}

				if (!performer->self->exists()){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}

				if (brk->self->isIdle()){
					performer->state = MyUnit::STATE::MINERAL;
				}
			}
		}
	}
}

// 확장 코드
float Bacanic2::getValue(UnitType type){

	if (type == UnitTypes::Protoss_Zealot){
		return 1;
	}

	if (type == UnitTypes::Protoss_Dragoon){
		return 2;
	}

	if (type == UnitTypes::Terran_Marine){
		return 0.5;
	}

	if (type == UnitTypes::Terran_Siege_Tank_Tank_Mode){
		return 2;
	}

	if (type == UnitTypes::Terran_Siege_Tank_Siege_Mode){
		return 2.5;
	}

	if (type == UnitTypes::Protoss_Dragoon){
		return 2;
	}

	if (type == UnitTypes::Protoss_Dark_Templar){
		return 2.5;
	}

	return 0.0f;
}

bool Bacanic2::canExpand(){
	
	// 시즈가 가능한지 여부 확인.
	if (!bw->self()->hasResearched(TechTypes::Tank_Siege_Mode)){
		return false;
	}

	int dark = info->map->getCloseCount(UnitTypes::Protoss_Dark_Templar);
	int dragoon = info->map->getCloseCount(UnitTypes::Protoss_Dragoon);
	int zealot = info->map->getCloseCount(UnitTypes::Protoss_Zealot);

	if (0 < dark && info->self->getBuildingCount(UnitTypes::Terran_Comsat_Station) < 1){
		bw << "Show Dark." << endl;
		return false;
	}

	if (flags[EXPAND_MUST]){
	//auto enemyBuild = state->getEnemyBuild();
	//if(enemyBuild == EXPECT_BUILD::ONEGATECORE || enemyBuild == EXPECT_BUILD::ONEGATECORE_DARK || enemyBuild == EXPECT_BUILD::MORE_TURRET){
		if (info->self->getBuildingCount(UnitTypes::Terran_Comsat_Station) < 1 || info->self->getPosUnit(posMgr->getTilePosition(POS::SIM1_TURRET), UnitTypes::Terran_Missile_Turret) != nullptr){
			bw << "He has DrakBuild" << endl;
			return false;
		}		
	}

	if (info->self->getUnitCount(UnitTypes::Terran_Marine) < 2){
		if (info->self->getBuildingCount(UnitTypes::Terran_Barracks) < 1){
			return false;
		}

		auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front()->self;
		if (scheduler->getReadyTrain(brk)){
			brk->train(UnitTypes::Terran_Marine);
		}

		return false;
	}

	if (info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) + info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode) < 2){
		return false;
	}

	auto chkPoint = BWTA::getNearestChokepoint(info->map->getMyStartBase()->getTilePosition());
	auto& close = info->map->getMyTerritorys().back()->getCloseEnemy();
	int enemyScore = 0;
	int myScore = -1;

	if (0 < close.size()){
		int enemy = 0;
		for (auto c : close){
			if (c->getType() == UnitTypes::Protoss_Dark_Templar && 0 < info->self->getBuildingCount(UnitTypes::Terran_Comsat_Station)){
				continue;
			}

			if (c->getType() == UnitTypes::Protoss_Probe || c->getType() == UnitTypes::Protoss_Observer){
				continue;
			}

			if (getDistance(c->getPosition(), chkPoint->getCenter()) < 32 * 6){
				enemy++;
				enemyScore += getValue(c->getType());
			}
		}

		if (0 < enemy){
			bw << "Many Close Enemy." << endl;
			return false;
		}

		// value
		for (auto unit : UnitControl::getInstance()->TCV[0]->tankSet){
			myScore += getValue(unit->getType());
		}

		for (auto unit : UnitControl::getInstance()->TCV[0]->marineSet){
			myScore += getValue(unit->getType());
		}

		auto gap = enemyScore - myScore;

		if (gap <= 1.5){
			return true;
		}
		else{
			bw << "Maybe Lose." << endl;
			return false;
		}
	}

	return true;
}

void Bacanic2::expand(){

	if (info->self->getBuildingCount(UnitTypes::Terran_Command_Center) < 2){
		return;
	}

	if (flags[ADD_CENTER]){
		info->map->getMyTerritorys().front()->immigrant(info->map->getMyTerritorys().back(), Territory::WORK::OVER);
		return;
	}

	if (flags[EXPAND_BRAVELY]){
		expandBravely();
	}
	else if (flags[EXPAND_CAREFULLY]){

	}
	else if (flags[EXPAND_MUST]){
		expandBravely();
	}
}

void Bacanic2::expandBravely(){

	bool allow = canExpand();
	auto brkPos = posMgr->getTilePosition(POS::THIRD_FACTORY);
	posMgr->planUpload(UnitTypes::Terran_Barracks, brkPos);

	MyUnit* cnter = info->self->getBuildings(UnitTypes::Terran_Command_Center).back();
	MyUnit* brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
	auto front = info->map->getMyTerritorys().back();

	if (!brk->self->isLifted()){
		if (allow){
			if (2 < getDistance(brk->getTilePosition(), brkPos)){
				brk->self->lift();
				state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2, true);
			}
		}
	}
	else{
		if (getDistance(brk->getTilePosition(), brkPos) < 5){
			if (brk->self->canLand()){
				brk->self->land(brkPos);
			}			
		}
		else{
			brk->self->move((Position)brkPos);
		}
	}

	if (flags[ADD_CENTER]){
		info->map->getMyTerritorys().front()->immigrant(front, Territory::WORK::OVER);
		return;
	}

	if (!cnter->self->isLifted()){
		if (3 < getDistance(cnter->getTilePosition(), front->getTilePosition())){
			cnter->self->lift();
		}
		else{
			if (cnter->self->isIdle()){
				front->setCmdCenter(cnter);
				front->init(front->getBase());
				front->getReady() = true;

				front->setGasScvCount(1);				
				front->setFitScvCount(12);

				for (int i = 0; i < 4; i++){
					auto unit = info->map->getMyTerritorys().front()->getWorker(true);
					front->addScv(unit);
				}

				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::SIM2_TURRET)), Scheduler::BUILD_SPEED::FAST);

				flags[ADD_CENTER] = true;
			}
		}
	}
	else{
		if (allow){
			if (getDistance(cnter->getTilePosition(), front->getTilePosition()) < 5){
				cnter->self->land(front->getTilePosition());
			}
			else{
				cnter->self->move(front->getPosition());
			}
		}
		else{
			if (90 < getProgress(cnter->self)){
				if (10 < getDistance(cnter->getTilePosition(), front->getTilePosition())){
					cnter->self->move(front->getPosition());
				}
				else{
					cnter->self->stop();
				}
			}
			else{
				cnter->self->move((Position)posMgr->getTilePosition(POS::SECOND_CENTER));
			}			
		}
	}
}


// Build 코드들
void Bacanic2::runBuild(){

	if (flags[ADD_CENTER]){
		if (info->self->getUnitCount(UnitTypes::Terran_SCV) < 18 && scheduler->getMineral() < 200){
			state->setCombatFlag(COMBAT_FLAG::MORE_SCV, true);
			return;
		}
		else{
			state->setCombatFlag(COMBAT_FLAG::MORE_SCV, false);
		}
	}
	else{
		if (info->self->getUnitCount(UnitTypes::Terran_SCV) < 7 && scheduler->getMineral() < 200){
			state->setCombatFlag(COMBAT_FLAG::MORE_SCV, true);
			return;
		}
		else{
			state->setCombatFlag(COMBAT_FLAG::MORE_SCV, false);
		}
	}

	// 저장된 빌드들을 수행.
	for (auto iter = builds.begin(); iter != builds.end(); ++iter){
		BuildWork& build = (*iter);
		auto work = build.getWork();

		if (!build.getFire() && build.run()){
			if (build.getWork()->type == UnitTypes::Terran_Supply_Depot && 2 * 10 < scheduler->getSupply()){
				iter = builds.erase(iter);
				iter--;

				continue;
			}

			if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_SUPPLY)){
				//auto performer = info->map->getMyTerritorys()[0]->getNearWorker((Position)posMgr->getTilePosition(POS::SIM1_SUPPLY));
				auto performer = info->map->getMyTerritorys()[0]->getLastWorker();
				work->setPerformer(performer);
			}
			else if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_BARRACK)){
				auto performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_SUPPLY));
				work->setPerformer(performer);
			}
			else if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_SUPPLY2)){
				performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_BARRACK));
				work->setPerformer(performer);
			}

			scheduler->addSchedule(build.getWork(), build.getSpeed());

			iter = builds.erase(iter);
			iter--;
		}
	}
}

void Bacanic2::buildProcess(){

	if (progress == BUILD_START){
		buildStart();
	}
	else if (progress == BUILD_MID){
		buildMid();
	}
	else if (progress == BUILD_LAST){
		buildLast();
	}
}

void Bacanic2::buildStart(){
	
	if (state->getEnemyBuild() == EXPECT_BUILD::FRONTGATE){
		if (info->self->getUnitCount(UnitTypes::Terran_Marine, true) < 5){
			flags[AUTO_MARINE] = true;
		}
		else{
			flags[AUTO_MARINE] = false;
		}
	}

	if (!flags[BUILD_START]){
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 8, false, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY), nullptr, 45));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, 1, true, UnitTypes::Terran_Barracks, posMgr->getTilePosition(POS::SIM1_BARRACK)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 12, true, UnitTypes::Terran_Refinery));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 10, true, UnitTypes::Terran_Marine, 2));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 14, true, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY2), nullptr));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 14, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 60));

		flags[BUILD_START] = true;
	}

	if (flags[BUILD_START] && builds.empty() && !flags[BUILD_START_2]){
		if ((0 < info->self->getBuildingCount(UnitTypes::Terran_Factory) && 20 <= getProgress(info->self->getBuildings(UnitTypes::Terran_Factory).front()->self))){
			auto enemyBuild = state->getEnemyBuild();

			if (enemyBuild == EXPECT_BUILD::RAW_NEXUS){
				// 생더블 혹은 앞마당을 갔을 경우.
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Siege_Tank_Tank_Mode));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Marine, 2));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Command_Center, posMgr->getTilePosition(POS::SECOND_CENTER), nullptr, 75));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Machine_Shop));
				builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Supply_Depot, 3, true, TechTypes::Tank_Siege_Mode));				
			}
			else if (enemyBuild == EXPECT_BUILD::TWOGATE || enemyBuild == EXPECT_BUILD::ONEGATECORE || enemyBuild == EXPECT_BUILD::TWOGATE_GATE || enemyBuild == EXPECT_BUILD::DEFAULT){
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Marine, 1));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Marine, 2));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Siege_Tank_Tank_Mode));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Command_Center, posMgr->getTilePosition(POS::SECOND_CENTER), nullptr, 75));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Machine_Shop));
				builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Supply_Depot, 3, true, TechTypes::Tank_Siege_Mode));
			}
			else{
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Marine));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Siege_Tank_Tank_Mode));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Marine, 2));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Command_Center, posMgr->getTilePosition(POS::SECOND_CENTER), nullptr, 75));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Machine_Shop));
				builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Supply_Depot, 3, true, TechTypes::Tank_Siege_Mode));
			}
			
			flags[AUTO_SUPPLY] = true;
			flags[AUTO_TANK] = true;

			flags[BUILD_START_2] = true;
		}		
	}

	if (2 <= info->map->getCloseCount(UnitTypes::Protoss_Zealot) && info->self->getBuildingCount(UnitTypes::Terran_Command_Center, true) < 2 && info->self->getUnitCount(UnitTypes::Terran_Marine, true) <= 2){
		auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
		if (scheduler->getReadyTrain(brk->self)){
			brk->self->train(UnitTypes::Terran_Marine);
		}
	}

	if (builds.empty() && 0 < info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode, true)){
	//if (builds.empty()){
		progress = BUILD_MID;
	}
}

void Bacanic2::buildMid(){

	auto enemyBuild = state->getEnemyBuild();

	if (!flags[BUILD_MID]){
		if (enemyBuild == EXPECT_BUILD::ONEGATECORE_DARK){

			// 아카데미 먼저 (영호 빌드). 다크 의심.
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Academy, posMgr->getTilePosition(POS::ACADEMY)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Comsat_Station));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy , 1, true, UnitTypes::Terran_Engineering_Bay, posMgr->getTilePosition(POS::ENGINEERING)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::SIM1_TURRET)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::START_TURRET)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::HILL_TURRET)));
			
			/*
			builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Academy, 1, true, TechTypes::Stim_Packs));
			builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Academy, 1, true, UpgradeTypes::U_238_Shells));
						
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, false, UnitTypes::Terran_Medic, 3));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 45));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 2, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 45));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 3, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 0));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -100));
			*/

			flags[EXPAND_MUST] = true;
		}
		else{		

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::START_TURRET)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::HILL_TURRET)));

			if (enemyBuild == EXPECT_BUILD::ONEGATECORE_REAVER){
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::FRONT_TURRET)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::AROUND1_TURRET)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::AROUND2_TURRET)));
			}
			
			if (enemyBuild == EXPECT_BUILD::MORE_TURRET) {
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::FRONT_TURRET)));
			}

			// 내빌드.			
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 2, true, UnitTypes::Terran_Engineering_Bay, posMgr->getTilePosition(POS::ENGINEERING)));			
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true, UnitTypes::Terran_Academy, posMgr->getTilePosition(POS::ACADEMY)));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, false, UnitTypes::Terran_Medic, 3));

			/*
			builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Academy, 1, true, TechTypes::Stim_Packs));
			builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Academy, 1, true, UpgradeTypes::U_238_Shells));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, false, UnitTypes::Terran_Medic, 3));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 45));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 2, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 45));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 3, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 0));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -100));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station, TilePosition(0, 0), nullptr, 45, Scheduler::BUILD_SPEED::FAST));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station, TilePosition(0, 0), nullptr, 45, Scheduler::BUILD_SPEED::FAST));
			*/

			flags[EXPAND_BRAVELY] = true;
		}

		flags[BUILD_MID] = true;
	}

	if (flags[BUILD_MID] && !flags[BUILD_MID_2]){
		if (0 < info->self->getBuildingCount(UnitTypes::Terran_Missile_Turret)){
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -50));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Machine_Shop));

			builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Siege_Tank_Tank_Mode, 4, true, UpgradeTypes::Terran_Infantry_Armor));
			builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Siege_Tank_Tank_Mode, 4, true, TechTypes::Stim_Packs));
			builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Siege_Tank_Tank_Mode, 4, true, UpgradeTypes::U_238_Shells));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 2, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 3, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -200));

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station, TilePosition(0, 0), nullptr, 45, Scheduler::BUILD_SPEED::FAST));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station, TilePosition(0, 0), nullptr, 45, Scheduler::BUILD_SPEED::FAST));

			flags[BUILD_MID_2] = true;
		}
	}


	if (flags[ADD_CENTER] && !flags[BUILD_MID_GAS]){
		if (8 < info->map->getMyTerritorys().back()->getScvCount()){
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Refinery, info->resource->getFrontGasTilePos(info->map->getMyTerritorys().back()->getBase()), nullptr, 45, Scheduler::BUILD_SPEED::FAST));
			flags[BUILD_MID_GAS] = true;
		}
	}

	if (0 < info->map->getCloseEnemy().size() && info->self->getUnitCount(UnitTypes::Terran_Marine, true) <= 3){
		auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
		if (scheduler->getReadyTrain(brk->self)){
			brk->self->train(UnitTypes::Terran_Marine);
		}
	}

	/*
	if (!flags[BUILD_MID_COMSAT]){
		if (0 < info->self->getBuildingCount(UnitTypes::Terran_Academy) && info->map->getCloseEnemy().size() == 0){
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, false, UnitTypes::Terran_Comsat_Station, TilePosition(0, 0), nullptr, 45, Scheduler::BUILD_SPEED::FAST));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, false, UnitTypes::Terran_Comsat_Station, TilePosition(0, 0), nullptr, 45, Scheduler::BUILD_SPEED::FAST));

			flags[BUILD_MID_COMSAT] = true;
		}
	}
	*/
}

void Bacanic2::buildLast(){

}