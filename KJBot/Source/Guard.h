#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "DefenceEvent.h"
#include "MyUnit.h"

using namespace std;

class Guard
{ 	
	vector<DefenceEvent*> events;

	Guard();
	~Guard();

public:
	static Guard* getInstance();
	void init();
	void show();
	void update();
	void run();	

	bool getFire(DEFENSE_TYPE type) { return events[type]->isFired(); };
	bool isError();
};
