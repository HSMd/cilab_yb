#include "Utility.h"
#include "Instance.h"

void drawTerrainData(){

	//we will iterate through all the base locations, and draw their outlines.
	for (const auto& baseLocation : BWTA::getBaseLocations()) {
		TilePosition p = baseLocation->getTilePosition();

		//draw outline of center location
		Position leftTop(p.x * TILE_SIZE, p.y * TILE_SIZE);
		Position rightBottom(leftTop.x + 4 * TILE_SIZE, leftTop.y + 3 * TILE_SIZE);
		Broodwar->drawBoxMap(leftTop, rightBottom, Colors::Blue);

		//draw a circle at each mineral patch
		for (const auto& mineral : baseLocation->getStaticMinerals()) {
			//Broodwar->drawCircleMap(mineral->getInitialPosition(), 30, Colors::Cyan);
			//Broodwar->drawBoxMap(leftTop, rightBottom, Colors::Blue);
			Position left(mineral->getPosition().x - TILE_SIZE, mineral->getPosition().y - TILE_SIZE / 2);
			Position right(left.x + 2 * TILE_SIZE, left.y + TILE_SIZE);
			Broodwar->drawBoxMap(left, right, Colors::Cyan);
		}

		//draw the outlines of Vespene geysers
		for (const auto& geyser : baseLocation->getGeysers()) {
			TilePosition p1 = geyser->getInitialTilePosition();
			Position leftTop1(p1.x * TILE_SIZE, p1.y * TILE_SIZE);
			Position rightBottom1(leftTop1.x + 4 * TILE_SIZE, leftTop1.y + 2 * TILE_SIZE);
			Broodwar->drawBoxMap(leftTop1, rightBottom1, Colors::Orange);
		}

		//if this is an island expansion, draw a yellow circle around the base location
		if (baseLocation->isIsland()) {
			Broodwar->drawCircleMap(baseLocation->getPosition(), 80, Colors::Yellow);
		}
	}

	//we will iterate through all the regions and ...
	for (const auto& region : BWTA::getRegions()) {
		// draw the polygon outline of it in green
		BWTA::Polygon p = region->getPolygon();
		for (size_t j = 0; j < p.size(); ++j) {
			Position point1 = p[j];
			Position point2 = p[(j + 1) % p.size()];
			Broodwar->drawLineMap(point1, point2, Colors::Green);
		}
		// visualize the chokepoints with red lines
		for (auto const& chokepoint : region->getChokepoints()) {
			Position point1 = chokepoint->getSides().first;
			Position point2 = chokepoint->getSides().second;
			Broodwar->drawLineMap(point1, point2, Colors::Red);
		}
	}
}

void drawUnit(Unit u, bool my, bool building) {

	Position p1, p2, p3, p4;
	p1 = Position(u->getPosition().x - u->getType().width() / 2, u->getPosition().y - u->getType().height() / 2);
	p2 = Position(u->getPosition().x + u->getType().width() / 2, u->getPosition().y + u->getType().height() / 2);
	p3 = Position(u->getPosition().x - u->getType().width() / 2 - 1, u->getPosition().y - u->getType().height() / 2 - 1);
	p4 = Position(u->getPosition().x + u->getType().width() / 2 + 1, u->getPosition().y + u->getType().height() / 2 + 1);
	if (!my) {
		p1 = Position(info->enemy->getPosition(u).x - info->enemy->getType(u).width() / 2,		info->enemy->getPosition(u).y - info->enemy->getType(u).height() / 2);
		p2 = Position(info->enemy->getPosition(u).x + info->enemy->getType(u).width() / 2, info->enemy->getPosition(u).y +		info->enemy->getType(u).height() / 2);
		p3 = Position(info->enemy->getPosition(u).x - info->enemy->getType(u).width() / 2 - 1, info->enemy->getPosition(u).y -	info->enemy->getType(u).height() / 2 - 1);
		p4 = Position(info->enemy->getPosition(u).x + info->enemy->getType(u).width() / 2 + 1, info->enemy->getPosition(u).y +	info->enemy->getType(u).height() / 2 + 1);
	}
	if (my) {
		BWAPI::Broodwar->drawBoxMap(p3, p4, Colors::Blue, false);
		if (building) {
			BWAPI::Broodwar->drawBoxMap(p1, p2, Colors::Cyan, false);
		}
		else {
			BWAPI::Broodwar->drawBoxMap(p1, p2, Colors::Green, false);
		}
	}
	else {
		BWAPI::Broodwar->drawBoxMap(p3, p4, Colors::Red, false);
		if (building) {
			BWAPI::Broodwar->drawBoxMap(p1, p2, Colors::Orange, false);
		}
		else {
			BWAPI::Broodwar->drawBoxMap(p1, p2, Colors::Yellow, false);
		}
	}
}

int getProgress(Unit unit) {

	int re = 0;

	if (unit->getType().isBuilding()){
		if (unit->isCompleted()){
			// �����
			re = (int)(100 * (float)unit->getHitPoints() / unit->getType().maxHitPoints());
		}
		else{
			// �̿ϼ�
			re = 100 - (int)(((float)unit->getRemainingBuildTime() / unit->getType().buildTime()) * 100);
		}
	}
	else{
		re = (int)(100 * (float)unit->getHitPoints() / unit->getType().maxHitPoints());
	}

	return re;
}

void drawTerritory(Territory* territory){

	BWAPI::Broodwar->drawCircleMap(territory->getPosition(), territory->getRadius() * TILEPOSITION_SCALE, Colors::Purple, false);
}

void drawUnitText(Unit u, string str){

	UnitType targetType = u->getType();
	Position pos = u->getPosition() + Position(targetType.width() / 2 + 5, targetType.height() / 2 - 15);

	BWAPI::Broodwar->drawTextMap(pos, str.c_str());
}

bool isSame(Unit a, Unit b){
	//return a->getID() == b->getID();
	return a == b;
}

Position tile2pos(TilePosition tile){
	return Position(tile.x * TILEPOSITION_SCALE, tile.y * TILEPOSITION_SCALE);
}

TilePosition pos2tile(Position pos){
	return TilePosition(pos.x / TILEPOSITION_SCALE, pos.y / TILEPOSITION_SCALE);
}

float getDistance(TilePosition cmp1, TilePosition cmp2){
	return sqrt((float)pow(cmp1.x - cmp2.x, 2) + (float)pow(cmp1.y - cmp2.y, 2));
}	

float getDistance(BWTA::BaseLocation* cmp1, BWTA::BaseLocation* cmp2){
	return getDistance(cmp1->getPosition(), cmp2->getPosition());
}

float getDistance(Position cmp1, Position cmp2){
	return sqrt((float)pow((cmp1.x - cmp2.x), 2) + (float)pow((cmp1.y - cmp2.y), 2));
}

bool isAlmostReachedDest(Position dest, Position comparator, int xTerm, int yTerm){

	if (dest.x - (xTerm / 2) <= comparator.x && comparator.x <= dest.x + (xTerm / 2) &&
		dest.y - (yTerm / 2) <= comparator.y && comparator.y <= dest.y + (yTerm / 2))
	{
		return true;
	}

	return false;
}

bool isContain(TilePosition cmp1, TilePosition cmp2, int term){

	if (cmp1.x <= cmp2.x && cmp2.x <= cmp1.x + term &&
		cmp1.y <= cmp2.y && cmp2.y <= cmp1.y + term){
		return true;
	}

	return false;
}

bool isContainOneTerm(TilePosition cmp1, TilePosition cmp2, int term){

	if (cmp1.x <= cmp2.x && cmp2.x <= cmp1.x + term &&
		cmp1.y <= cmp2.y && cmp2.y <= cmp1.y + term){
		return true;
	}

	return false;
}

bool isContainTwoTerm(TilePosition cmp1, TilePosition cmp2, int xTerm, int yTerm){

	if (cmp1.x <= cmp2.x && cmp2.x <= cmp1.x + xTerm &&
		cmp1.y <= cmp2.y && cmp2.y <= cmp1.y + yTerm){
		return true;
	}

	return false;
}