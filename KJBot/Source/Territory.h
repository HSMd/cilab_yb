#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include <map>
#include <vector>

#include "MyUnit.h"

using namespace BWAPI;
using namespace BWTA;
using namespace std;

class CountUnit{
public:
	Unit unit;
	int count;

	CountUnit(Unit unit){ this->unit = unit; this->count = 0; };
	CountUnit(Unit unit, int count){ this->unit = unit; this->count = count; };
	~CountUnit(){};
};

class Territory{

public:
	enum WORK{
		MINERAL,
		GAS,
		OVER,
	};

	enum M_DIR{
		LEFT,
		RIGHT,
		UP,
		DOWN,
	};

private:
	int fitnessScvCount;
	int gasScvCount;
	int radius;

	bool ready;

	BaseLocation* base;
	MyUnit* cmdCenter;
	vector<CountUnit*> minerals;
	CountUnit* gas;

	map<WORK, vector<MyUnit*>> scvs;
	vector<Unit> buildings;

	map<UnitType, int> closeCount;
	vector<Unit> closeEnemy;

public:
	Territory();
	Territory(BaseLocation* base);
	Territory(BaseLocation* base, MyUnit* cmdCenter);
	~Territory();

	void init(BaseLocation* base);
	void update();
	void run();
	void show(int startX, int startY);
	void draw();
	void destroy(Unit unit);

	bool isAlready(Unit unit);
	int getCloseCount(UnitType type);

	vector<Unit> getCloseEnemy(){ return closeEnemy; };

	// Tomorrow 
	void addScv(MyUnit* scv);
	MyUnit* getWorker(bool except = false, TilePosition pos = TilePosition(0, 0));
	MyUnit* getNearWorker(Position pos, bool except = false);
	void delWorker(MyUnit* worker);
	MyUnit* getWorkerEx(bool except = false);
	int getScore(MyUnit* unit);

	MyUnit* getGasWorker(bool except = false);
	MyUnit* getLastWorker(bool except = false);
	CountUnit* getIdleMineral();
	void subResource(Unit resource, bool isMineral = true);
	
	void orderMineral(MyUnit* scv, CountUnit* work, bool add = false);
	void orderGas(MyUnit* scv, CountUnit* work, bool add = false);

	static string getScvStateString(MyUnit* scv);
	int getScvCount(){ return scvs[WORK::MINERAL].size() + scvs[WORK::GAS].size() + scvs[WORK::OVER].size(); };
	int getRequireSCVCount() { return fitnessScvCount - getScvCount(); };
	int getRadius(){ return radius; };
	bool& getReady(){ return ready; };

	void setMineralDir();
	void setGasScvCount(int count) { gasScvCount = count; };
	void setGas(Unit gas){ this->gas = new CountUnit(gas); };
	Unit getGas(){ if (gas == nullptr) return nullptr; return gas->unit; };
	void setCmdCenter(MyUnit* cmd){ this->cmdCenter = cmd; };
	void setBase(BaseLocation* base){ this->base = base; };
	void setFitScvCount(int count){ fitnessScvCount = count; };

	MyUnit* getCmdCenter(){ return cmdCenter; };
	bool isRequireScv(){ return 1 < (fitnessScvCount - getScvCount()); };
	vector<MyUnit*>& getScvs(WORK work){ return scvs[work]; };
	TilePosition getTilePosition(){ return base->getTilePosition(); };
	Position getPosition(){ return base->getPosition(); };
	BaseLocation* getBase(){ return base; };
	vector<MyUnit*> getMineralScvs(){ return scvs[WORK::MINERAL]; };

	void immigrant(Territory* territory, WORK work);
	vector<MyUnit*>& getOverScvs(){ return scvs[WORK::OVER]; };

	static bool orderByDistance(CountUnit* aUnit, CountUnit* bUnit);
};