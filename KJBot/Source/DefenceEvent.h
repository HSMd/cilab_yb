#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "MyUnit.h"

using namespace std;

enum DEFENSE_TYPE{
	ENEMY_SEARCH,
	GAS_TERROR,
	PYLON_RUSH,
	ZEALOT_RUSH,
	ZEALOT_RUSH2,
	REPAIR_BUILDING,
	SCV_GUARD,
	TANK_REPAIR,
};

class DefenceEvent
{
protected:
	DEFENSE_TYPE eventType;
	map<UnitType, vector<MyUnit*>> defenceUnits;

	bool fire;
	
public:

	DefenceEvent(DEFENSE_TYPE state);
	~DefenceEvent();

	virtual void init();
	virtual void show();
	virtual void update() = 0;
	virtual void run() = 0;

	virtual void chkFire() = 0;
	virtual void turnOff() = 0;

	void getUnits(UnitType type, int count = 9999);
	void eraseUnits(UnitType type);
	int getCloseCount(UnitType type);
	DEFENSE_TYPE getType(){ return eventType; };

	bool isFired(){ return fire; };
};

