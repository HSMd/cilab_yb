#include "Resource.h"

ResourceInfo::ResourceInfo() {
	for (auto m : BWAPI::Broodwar->getMinerals()) {
		minerals.push_back(m);
		resourceAmount[m] = m->getResources();
		resourcePos[m] = m->getPosition();
		resourceType[m] = m->getType();
	}
	for (auto g : BWAPI::Broodwar->getGeysers()) {
		gases.push_back(g);
		resourceAmount[g] = g->getResources();
		resourcePos[g] = g->getPosition();
		resourceType[g] = g->getType();
	}
}

ResourceInfo::~ResourceInfo() {

}

void ResourceInfo::draw() {
	for (auto r : minerals) {
		Position start = Position(getPosition(r).x - getType(r).width() / 2, getPosition(r).y + getType(r).height() / 2);
		Position end = Position(getPosition(r).x + getType(r).width() / 2, getPosition(r).y + getType(r).height() / 2 + 5);
		float w = ((float)resourceAmount[r] / 1500.f) * (float)getType(r).width();
		Position rend = Position(getPosition(r).x - getType(r).width() / 2 + w, getPosition(r).y + getType(r).height() / 2 + 5);
		Position p[4];
		p[0] = Position(start.x, start.y);
		p[1] = Position(end.x, start.y);
		p[2] = Position(end.x, end.y);
		p[3] = Position(start.x, end.y);
		BWAPI::Broodwar->drawBoxMap(start, rend, Colors::Cyan, true);
		for (int x = 0; x < 4; x++) {
			BWAPI::Broodwar->drawLineMap(p[x], p[(x + 1) % 4], Colors::Black);
		}
		for (int x = start.x; x <= end.x; x += getType(r).width() / 10) {
			Position up = Position(x, start.y);
			Position down = Position(x, end.y);
			BWAPI::Broodwar->drawLineMap(up, down, Colors::Black);
		}
		
	}

	for (auto r : gases) {
		Position start = Position(getPosition(r).x - getType(r).width() / 2, getPosition(r).y + getType(r).height() / 2);
		Position end = Position(getPosition(r).x + getType(r).width() / 2, getPosition(r).y + getType(r).height() / 2 + 5);
		float w = ((float)resourceAmount[r] / 5000.f) * (float)getType(r).width();
		Position rend = Position(getPosition(r).x - getType(r).width() / 2 + w, getPosition(r).y + getType(r).height() / 2 + 5);
		Position p[4];
		p[0] = Position(start.x, start.y);
		p[1] = Position(end.x, start.y);
		p[2] = Position(end.x, end.y);
		p[3] = Position(start.x, end.y);
		BWAPI::Broodwar->drawBoxMap(start, rend, Colors::Green, true);
		for (int x = 0; x < 4; x++) {
			BWAPI::Broodwar->drawLineMap(p[x], p[(x + 1) % 4], Colors::Black);
		}
		for (int x = start.x; x <= end.x; x += getType(r).width() / 10) {
			Position up = Position(x, start.y);
			Position down = Position(x, end.y);
			BWAPI::Broodwar->drawLineMap(up, down, Colors::Black);
		}

	}
}

void ResourceInfo::update() {

	for (auto r : minerals) {
		if (r->isVisible()) {
			resourceAmount[r] = r->getResources();
			resourcePos[r] = r->getPosition();
			resourceType[r] = r->getType();
		}
	}

	for (auto r : gases) {
		if (r->isVisible()) {
			resourceAmount[r] = r->getResources();
			resourcePos[r] = r->getPosition();
			resourceType[r] = r->getType();
		}
	}
	// 양 이 0이면 지우기
}