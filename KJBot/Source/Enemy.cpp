#include "Enemy.h"
#include "Utility.h"
#include "Instance.h"



BuildInfo::BuildInfo() {
	buildScore[DOUBLE_NEXUS] = 0.0f;
	buildScore[ZEALOT_ALLIN] = 0.0f;
	buildScore[ZEALOT_DRAGOON_ALLIN] = 0.0f;
	buildScore[SHUTTLE_REAVER] = 0.0f;
	buildScore[SHUTTLE_DARK] = 0.0f;
	buildScore[SHUTTLE_ZEALOT] = 0.0f;
	buildScore[ONE_GATE_OBSERVER_DOUBLE] = 0.0f;
	buildScore[FAST_DARK] = 0.0f;
	buildScore[TWO_GATE_DOUBLE] = 0.0f;
	buildScore[ONE_GATE_DOUBLE] = 0.0f;
	buildScore[THREE_GATE_OBSERVER_DRAGOON] = 0.0f;
	buildScore[JUNJIN_GATE] = 0.0f;
	buildScore[PHOTO_RUSH] = 0.0f;
	
	setBuildFrame();
}

void BuildInfo::draw() {
	Position p = Position(30, 30);
	Position t = Position(200, 30);
	Position p1 = p;

	BWAPI::Broodwar->drawBoxScreen(Position(28, 28), Position(222, 220), Colors::Black, true);
	char buf[20];

	BWAPI::Broodwar->drawTextScreen(p, "DOUBLE_NEXUS");
	itoa((int)(buildScore[DOUBLE_NEXUS]*100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "ZEALOT_ALLIN");
	itoa((int)(buildScore[ZEALOT_ALLIN] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "ZEALOT_DRAGOON_ALLIN");
	itoa((int)(buildScore[ZEALOT_DRAGOON_ALLIN] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "SHUTTLE_REAVER");
	itoa((int)(buildScore[SHUTTLE_REAVER] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "SHUTTLE_DARK");
	itoa((int)(buildScore[SHUTTLE_DARK] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "SHUTTLE_ZEALOT");
	itoa((int)(buildScore[SHUTTLE_ZEALOT] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "ONE_GATE_OBSERVER_DOUBLE");
	itoa((int)(buildScore[ONE_GATE_OBSERVER_DOUBLE] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "FAST_DARK");
	itoa((int)(buildScore[FAST_DARK] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "TWO_GATE_DOUBLE");
	itoa((int)(buildScore[TWO_GATE_DOUBLE] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "ONE_GATE_DOUBLE");
	itoa((int)(buildScore[ONE_GATE_DOUBLE] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "THREE_GATE_OBSERVER_DRAGOON");
	itoa((int)(buildScore[THREE_GATE_OBSERVER_DRAGOON] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "JUNJIN_GATE");
	itoa((int)(buildScore[JUNJIN_GATE] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);

	p.y += 11; t.y += 11;

	BWAPI::Broodwar->drawTextScreen(p, "PHOTO_RUSH");
	itoa((int)(buildScore[PHOTO_RUSH] * 100), buf, 10);
	buf[strlen(buf)] = '\0';
	BWAPI::Broodwar->drawTextScreen(t, buf);
}

void BuildInfo::update() {
	
	for (auto b = buildScore.begin(); b != buildScore.end(); b++) {
		b->second = 0.0f;
		if (b->first == BUILD::JUNJIN_GATE) {
			for (auto e : info->enemy->getUnits(UnitTypes::Protoss_Gateway)) {
				if (info->map->getEnemyStartBase() == NULL) break;
				float dist = getDistance(info->enemy->getPosition(e), info->map->getEnemyStartBase()->getPosition());
				if (dist > 32 * 32) {
					b->second = 100.0f;
				}
			}
			for (auto e : info->enemy->getUnits(UnitTypes::Protoss_Pylon)) {
				if (info->map->getEnemyStartBase() == NULL) break;
				float dist = getDistance(info->enemy->getPosition(e), info->map->getEnemyStartBase()->getPosition());
				if (dist > 32 * 32) {
					b->second = 100.0f;
				}
			}
		}
		else {
			vector<std::pair<UnitType, int>> bframe;
			vector<std::pair<UnitType, float>> fscore;
			vector<std::pair<UnitType, int>> enemyBuild;

			for (auto bf = buildFrame.begin(); bf != buildFrame.end(); bf++) {
				if (b->first == bf->first) {
					bframe = bf->second;
					for (auto p = bframe.begin(); p != bframe.end(); p++) {
						enemyBuild.push_back(std::pair<UnitType, int>(p->first, -1));
					}
				}
			}
			for (auto fs = frameScore.begin(); fs != frameScore.end(); fs++) {
				if (b->first == fs->first) {
					fscore = fs->second;
				}
			}
			//printf("enemyBuildFrame vector size %d\n", enemyBuildFrame.size());
			for (auto p = enemyBuildFrame.begin(); p != enemyBuildFrame.end(); p++) {
				if (p->first == UnitTypes::Protoss_Pylon || p->first == UnitTypes::Protoss_Shield_Battery) continue;

				for (auto q = enemyBuild.begin(); q != enemyBuild.end(); q++) {
					if (q->first == p->first && q->second < 0) {
						q->second = p->second;
						break;
					}
				}

			}
			float score = 0;
			for (int x = 0; x < enemyBuild.size(); x++) {
				if (enemyBuild[x].second != -1) {
					int frameDiff = enemyBuild[x].second - bframe[x].second;
					float beta = 0;

					float SCORE_FRAME = 3600.0f;

					if (frameDiff < 0) beta = 0;
					else if (frameDiff > SCORE_FRAME) beta = 3.141592f / 2;
					else {
						beta = (float)frameDiff/ SCORE_FRAME * 3.141592f / 2;
					}
					float s = cos(beta);
					score += s * 100.0f * fscore[x].second;
				}
			}
			b->second = score;
		}

		//printf("score : %.2f\n", b->second);
	}

	BUILD maxBuild;
	float maxVal = 0;
	for (auto b = buildScore.begin(); b != buildScore.end(); b++) {
		if(maxVal < b->second){
			maxVal = b->second;
			maxBuild = b->first;
		}
	}

	/*if (maxBuild == BUILD::DOUBLE_NEXUS) {
		cout << "Max Score Build : DOUBLE_NEXUS  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::FAST_DARK) {
		cout << "Max Score Build : FAST_DARK  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::JUNJIN_GATE) {
		cout << "Max Score Build : JUNJIN_GATE  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::ONE_GATE_DOUBLE) {
		cout << "Max Score Build : ONE_GATE_DOUBLE  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::ONE_GATE_OBSERVER_DOUBLE) {
		cout << "Max Score Build : ONE_GATE_OBSERVER_DOUBLE  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::PHOTO_RUSH) {
		cout << "Max Score Build : PHOTO_RUSH  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::SHUTTLE_DARK) {
		cout << "Max Score Build : SHUTTLE_DARK  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::SHUTTLE_REAVER) {
		cout << "Max Score Build : SHUTTLE_REAVER  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::SHUTTLE_ZEALOT) {
		cout << "Max Score Build : SHUTTLE_ZEALOT  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::THREE_GATE_OBSERVER_DRAGOON) {
		cout << "Max Score Build : THREE_GATE_OBSERVER_DRAGOON  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::TWO_GATE_DOUBLE) {
		cout << "Max Score Build : TWO_GATE_DOUBLE  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::ZEALOT_ALLIN) {
		cout << "Max Score Build : ZEALOT_ALLIN  score : " << maxVal << endl;
	}
	else if (maxBuild == BUILD::ZEALOT_DRAGOON_ALLIN) {
		cout << "Max Score Build : ZEALOT_DRAGOON_ALLIN  score : " << maxVal << endl;
	}*/


	
}



Enemy::Enemy(){
	bif = new BuildInfo();
}

Enemy::~Enemy(){

}

void Enemy::show(int startX, int startY){
	
	//Broodwar << allUnits.size() << endl;

	Broodwar->drawTextScreen(startX, startY, "ALL_ENEMY : %d", allUnits.size());
	Broodwar->drawTextScreen(startX, startY + 20, "ALL_PROBE : %d", getUnits(UnitTypes::Protoss_Probe).size());
}

bool Enemy::isAlready(Unit unit, bool isBuilding){

	if (isBuilding){
		auto findUnits = getBuildings(unit->getType());

		for (auto iter = findUnits.begin(); iter != findUnits.end(); iter++){
			Unit find = (*iter);
			if (find == nullptr) 
				continue;

			if (isSame(find, unit))
				return true;
		}
	}
	else{
		auto findUnits = getUnits(unit->getType());

		for (auto iter = findUnits.begin(); iter != findUnits.end(); iter++){
			Unit find = (*iter);
			if (find == nullptr)
				continue;

			if (isSame(find, unit))
				return true;
		}
	}

	return false;
}

void Enemy::delData(vector <Unit>& data, Unit unit){

	for (auto iter = data.begin(); iter != data.end(); iter++) {
		Unit find = (*iter);

		if (isSame(find, unit)){
			data.erase(iter);
			break;
		}
	}
}

void Enemy::update(){

	auto enemyUnits = BWAPI::Broodwar->enemy()->getUnits();

	for (auto iter = enemyUnits.begin(); iter != enemyUnits.end(); iter++) {
		Unit unit = (*iter);
		UnitType unitType = unit->getType();

		if (unitType == UnitTypes::Unknown){
			continue;
		}

		if (unitType.isBuilding() && unitType != UnitTypes::Protoss_Photon_Cannon){
			bool already = isAlready(unit, DRAW_BUILDING);

			if (already)
				continue;
			else{
				//if (unitType == UnitTypes::Protoss_Gateway) printf("GATE WAY FRAME %d\n", BWAPI::Broodwar->getFrameCount());
				bif->enemyBuildFrame.push_back(std::pair<UnitType, int>(unit->getType(), BWAPI::Broodwar->getFrameCount()));
				bif->update();
				buildings[unitType].push_back(unit);
				allBuildings.push_back(unit);
			}
		}
		else{
			bool already = isAlready(unit, DRAW_UNIT);

			if (already)
				continue;
			else{
				units[unitType].push_back(unit);
				allUnits.push_back(unit);
			}
		}
	}

	for (auto unit : allUnits){
		if (unit->isVisible()){
			unitPos[unit] = unit->getPosition();
			unitType[unit] = unit->getType();
		}	
	}

	for (auto unit : allBuildings){
		if (unit->isVisible()){
			unitPos[unit] = unit->getPosition();
			unitType[unit] = unit->getType();
		}
	}

	if (BWAPI::Broodwar->getFrameCount() % 50 == 0)
		bif->update();

	
	if (BWAPI::Broodwar->enemy()->getUnits().size() > 0)
		enemyGroup = info->getGroupArray(true, false);
	else
		enemyGroup.clear();
	//enemyGroundGroup = info->getGroupArray(true, true);
}

void Enemy::draw(){

	//Broodwar << allUnits.size() << endl;

	for (auto unit : getAllUnits()){
		drawUnit(unit, DRAW_ENEMY, DRAW_UNIT);
	}

	for (auto unit : getAllBuildings()){
		drawUnit(unit, DRAW_ENEMY, DRAW_BUILDING);
	}

	for (auto q = enemyGroup.begin(); q != enemyGroup.end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.second / 100, BWAPI::Colors::Orange, true);
		BWAPI::Broodwar->drawCircleMap((*q).first, 8, BWAPI::Colors::Red, true);
		BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.first, BWAPI::Colors::Red, false);
	}

	for (auto q = enemyGroundGroup.begin(); q != enemyGroundGroup.end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.second / 100, BWAPI::Colors::Orange, true);
		BWAPI::Broodwar->drawCircleMap((*q).first, 8, BWAPI::Colors::Yellow, true);
		BWAPI::Broodwar->drawCircleMap((*q).first, (*q).second.first, BWAPI::Colors::Yellow, false);
		//printf("enemyGroup unit Value %d\n", (*q).second.second);
	}

	//bif->draw();
}

void Enemy::morph(Unit unit) {
	if (unit->getType() == UnitTypes::Protoss_Archon) {
		delData(allUnits, unit);
		delData(units[UnitTypes::Protoss_High_Templar], unit);
	}
}


void Enemy::destroy(Unit unit){

	UnitType type = unit->getType();

	if (type.isBuilding() && type != UnitTypes::Protoss_Photon_Cannon){
		delData(allBuildings, unit);
		delData(buildings[type], unit);
	}
	else{
		delData(allUnits, unit);
		delData(units[type], unit);
	}
}

vector<Unit>& Enemy::getUnits(UnitType type){
	
	return units[type];
}

vector<Unit>& Enemy::getBuildings(UnitType type){

	return buildings[type];
}

vector<Unit>& Enemy::getAllUnits(){

	return allUnits;
}

vector<Unit>& Enemy::getAllBuildings(){

	return allBuildings;
}

int Enemy::getUnitCount(UnitType type){

	return units[type].size();
}

int Enemy::getBuildingCount(UnitType type){

	return buildings[type].size();
}

void Enemy::create(Unit unit) {
	if (unit->getType().isBuilding()) {
		bif->enemyBuildFrame.push_back(std::pair<UnitType, int>(unit->getType(), BWAPI::Broodwar->getFrameCount()));
		bif->update();
	}
}

void BuildInfo::setBuildFrame() {	
	buildFrame.clear();
	frameScore.clear();

	vector<std::pair<UnitType, int>> frame;
	vector<std::pair<UnitType, float>> score;



	//Double Nexus
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 2629));							//min 2581
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::DOUBLE_NEXUS, frame));

	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 1.0f));
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::DOUBLE_NEXUS, score));



	//Zealot Allin request replay
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 1900));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 2117));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 4500));
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::ZEALOT_ALLIN, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.3f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.3f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.4f));
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::ZEALOT_ALLIN, score));



	//Zealot Dragoon Allin 
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 1905));						//min 1905
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 2117));						//min 2117
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Cybernetics_Core, 4969));				//min 1969
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::ZEALOT_DRAGOON_ALLIN, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.2f));						
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.3f));						
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Cybernetics_Core, 0.5f));				
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::ZEALOT_DRAGOON_ALLIN, score));



	//Shuttle Reaver 
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 1807));						//min 1905
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Cybernetics_Core, 2859));				//min 2117
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Robotics_Facility, 5246));				//min 1969
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Robotics_Support_Bay, 5401));			//min 1969
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::SHUTTLE_REAVER, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.1f));						//min 1905
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Cybernetics_Core, 0.2f));				//min 2117
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Robotics_Facility, 0.3f));				//min 1969
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Robotics_Support_Bay, 0.4f));			//min 1969
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::SHUTTLE_REAVER, score));



	//Shuttle Dark request replay
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 0));							//min 1905
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Cybernetics_Core, 0));					//min 2117
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Citadel_of_Adun, 0));					//min 1969
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Robotics_Facility, 0));					//min 1969
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Templar_Archives, 0));					//min 1969
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::SHUTTLE_DARK, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.1f));							
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Cybernetics_Core, 0.1f));					
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Citadel_of_Adun, 0.3f));					
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Robotics_Facility, 0.3f));				
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Templar_Archives, 0.2f));					
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::SHUTTLE_DARK, score));



	//Zealot Drop
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 1908));						//min 1877
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Cybernetics_Core, 3289));				//min 3289
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Robotics_Facility, 5317));				//min 5317
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 6229));						//min 6629
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::SHUTTLE_ZEALOT, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.1f));						
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Cybernetics_Core, 0.3f));				
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Robotics_Facility, 0.3f));			
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.3f));						
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::SHUTTLE_ZEALOT, score));



	//One Gateway Observer Double Nexus
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 1909));						//min 1779
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Cybernetics_Core, 3126));				//min 2907
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Robotics_Facility, 5418));				//min 5187
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 7148));							//min 6937
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::ONE_GATE_OBSERVER_DOUBLE, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.2f));						
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Cybernetics_Core, 0.2f));				
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Robotics_Facility, 0.2f));			
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.4f));						
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::ONE_GATE_OBSERVER_DOUBLE, score));



	//Fast Dark Templar
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 1980));						//min 1791
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Cybernetics_Core, 3324));				//min 2980
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Citadel_of_Adun, 4862));				//min 3968
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Templar_Archives, 5954));				//min 4977
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::FAST_DARK, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.1f));						//min 1791
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Cybernetics_Core, 0.2f));				//min 2980
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Citadel_of_Adun, 0.35f));				//min 3968
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Templar_Archives, 0.35f));				//min 4977
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::FAST_DARK, score));



	//Two Gateway Double Nexus
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 1857));						//min 1826
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Cybernetics_Core, 2991));				//min 2813
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 4767));						//min 4453
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 7208));							//min 6471
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::TWO_GATE_DOUBLE, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.1f));						//min 1826
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Cybernetics_Core, 0.2f));				//min 2813
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.3f));						//min 4453
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.4f));							//min 6471
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::TWO_GATE_DOUBLE, score));



	//Three Gateway Double Nexus Observer and Dragoon 
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 1827));						//min 1809
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Cybernetics_Core, 2890));				//min 2793
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 5496));							//min 5496
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::THREE_GATE_OBSERVER_DRAGOON, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.2f));						//min 1809
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Cybernetics_Core, 0.3f));				//min 2793
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.5f));						//min 5496
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::THREE_GATE_OBSERVER_DRAGOON, score));

	//One Gate way Double Nexus
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 2111));						//min 2111
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 4292));							//min 3276
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::ONE_GATE_DOUBLE, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 0.3f));						//min 2111
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.7f));						//min 3276
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::ONE_GATE_DOUBLE, score));



	//Junjin Gateway
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Gateway, 1883));						//min 1835
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::JUNJIN_GATE, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Gateway, 1.0f));						//min 1835
	frameScore.push_back(std::pair<BUILD, vector<std::pair<UnitType, float>>>(BUILD::JUNJIN_GATE, score));



	//Photo Cannon Rush
	frame.clear();
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Nexus, 0));
	frame.push_back(std::pair<UnitType, int>(UnitTypes::Protoss_Forge, 2378));							//min 2378
	buildFrame.push_back(std::pair<BUILD, vector<std::pair<UnitType, int>>>(BUILD::PHOTO_RUSH, frame));

	score.clear();
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Nexus, 0.0f));
	score.push_back(std::pair<UnitType, float>(UnitTypes::Protoss_Forge, 1.0f));							//min 2378
	frameScore.push_back(std::pair < BUILD, vector < std::pair < UnitType, float >> > (BUILD::PHOTO_RUSH, score));
}
