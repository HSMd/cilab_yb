#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "DefenceEvent.h"

using namespace std;


class TankRepair : public DefenceEvent
{	
	MyUnit* target;

public:

	TankRepair(DEFENSE_TYPE type);
	~TankRepair();

	void chkFire();
	void turnOff();

	void update();
	void run();

	void act();
};

