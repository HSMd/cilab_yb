#pragma once
#include "BWAPI.h"
#include "MyUnit.h"
#include "Work.h"

#include <map>
#include <vector>

using namespace BWAPI;
using namespace std;

class OneFacDouble{

	enum FLAG{
		SEARCH,
		ADD_CENTER,

		INVASION,

		AUTO_MARINE,
		AUTO_TANK,
		AUTO_SUPPLY,

		SCV_CONTROL_1,
		SCV_CONTROL_2,

		IMM_PERFORMER, // 
		IMM_SPARE,     //

		BUILD_START, // ���丮����.
		BUILD_START_FIND,
		BUILD_START_FIND_RUN,
		BUILD_START_NOT,
		BUILD_START_NOT_RUN,
		
		BUILD_MID,
		BUILD_MID_INVASION,
		BUILD_MID_NO_INVASION,
		BUILD_MID_MOVE,

		BUILD_MID_STIM_PACKS,
		BUILD_MID_U_238_SHELLS
	};

	vector<BuildWork> builds;
	vector<MyUnit*> moveBuildings;

	map<FLAG, bool> flags;

	MyUnit* performer;
	MyUnit* spare;

	FLAG progress;
	int frameCount;

public: 
	OneFacDouble(){};
	~OneFacDouble(){};

	void init();
	void show();
	void draw();
	void run();

	void autoUnit();
	void search();

	void scvCreate();
	void runBuild();

	void expand();
	void move();

	void buildProcess();
	void buildStart();
	void buildMid();
};