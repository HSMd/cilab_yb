#include "DefenceZealot.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

DefenceZealot::DefenceZealot(DEFENSE_TYPE type) : DefenceEvent(type)
{	
	
}

DefenceZealot::~DefenceZealot()
{

}

void DefenceZealot::chkFire(){

	auto zealotCount = info->map->getCloseCount(UnitTypes::Protoss_Zealot);

	if (!state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
		if (0 < zealotCount){
			fire = true;
		}
	}
}

void DefenceZealot::update(){
	//sample
	auto zealotCount = info->map->getCloseCount(UnitTypes::Protoss_Zealot);

	if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
		
	}
	else{
		if (zealotCount == 1){
			getUnits(UnitTypes::Terran_Marine);
		}

		//2마리 이상
		else
		{
			getUnits(UnitTypes::Terran_Marine);

			if (defenceUnits[UnitTypes::Terran_Marine].size() * 2 < zealotCount){
				if (scheduler->canAddUnitType(UnitTypes::Terran_Marine)){
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Marine), Scheduler::BUILD_SPEED::FAST);
				}
			}
		}
	}
	
	/*
	auto probeCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Probe);

	if (probeCount == 1){
		getUnits(UnitTypes::Terran_Marine, 2);

		if (defenceUnits[UnitTypes::Terran_Marine].size() == 0){
			getUnits(UnitTypes::Terran_SCV, probeCount);
		}
		else{
			eraseUnits(UnitTypes::Terran_SCV);
		}
	}
	else{
		getUnits(UnitTypes::Terran_Marine, 2);
		getUnits(UnitTypes::Terran_SCV, probeCount);
	}
	*/
}

void DefenceZealot::run()
{	
	
	chkFire();
	turnOff();
	if (!fire){
		return;
	}	

	update();
	act();
}

void DefenceZealot::act(){

	auto& close = info->map->getCloseEnemy();
	Unit attackTarget = nullptr;

	// 타겟 선정.
	for (auto enemy : close){
		if (attackTarget == nullptr){
			attackTarget = enemy;
		}
		else{
			if (enemy->getHitPoints() < attackTarget->getHitPoints()){
				attackTarget = enemy;
			}
		}
	}

	if (attackTarget == nullptr){
		return;
	}

	for (auto iter : defenceUnits){
		UnitType type = iter.first;
		
		for (auto unit : defenceUnits[type]){

			if (!unit->self->exists() || !unit->self->isCompleted()){
				continue;
			}
			if (unit->self->getType() == UnitTypes::Terran_Marine)
			{
				if (attackTarget->getOrderTarget() != unit->self)
				{
					if (!unit->self->isAttacking())
					{
						unit->self->attack(attackTarget);
					}
				}
				else{
					unit->self->move(info->map->getMyTerritorys()[0]->getPosition());
				}
			}
			else if (unit->self->getType() == UnitTypes::Terran_SCV)
			{
				if (defenceUnits[UnitTypes::Terran_Marine].size() > 0)
				{
					eraseUnits(UnitTypes::Terran_SCV);
				}
				else{
					if (!unit->self->isAttacking())
					{
						unit->self->attack(attackTarget);
					}
				}
			}
			/*if (CombatControl::getInstance()->isEnemyUnitComeTrue(unit))
			{
				unit->self->move(info->map->getMyTerritorys()[0]->getPosition());
			}
			else{
				if (!unit->self->isAttackFrame())
				{
					unit->self->attack(attackTarget);
				}
			}*/
		}
	}
}

void DefenceZealot::turnOff(){

	auto zealotCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Zealot);

	if (zealotCount == 0){
		eraseUnits(UnitTypes::Terran_Marine);
		eraseUnits(UnitTypes::Terran_SCV);

		fire = false;
	}	
}