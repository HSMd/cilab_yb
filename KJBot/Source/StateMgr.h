#pragma once

#include <BWAPI.h>

#include <map>
#include <vector>
#include "Territory.h"
#include "Scout.h"

using namespace BWAPI;
using namespace std;

enum COMBAT_FLAG{
	GUARD_SIM_CITY_1, 
	GUARD_SIM_CITY_2,
	GUARD_SIM_CITY_3, 
	INVASION_PYLON,
	FORWARD_MARINE,
	ATTACK,

	MORE_SCV,
	SIEGE_MODE,
	STIM_PACKS,
	U_238_SHELLS,
};


enum EXPECT_BUILD{
	DEFAULT,
	RAW_NEXUS,
	TWOGATE,
	ONEGATECORE,
	ONEGATECORE_GATE,
	ONEGATECORE_DOUBLE,
	ONEGATECORE_DARK,
	ONEGATECORE_REAVER,
	TWOGATE_CORE,
	TWOGATE_GATE,
	NONE,
	FRONTGATE,
	MORE_TURRET,
	GATE3,

};

class FlagData{

public:
	bool fire;
	int frameCount;

	FlagData(){
		fire = false;
		frameCount = 0;
	}
};

class StateMgr
{
	map<COMBAT_FLAG, FlagData> combatFlag;
	map<COMBAT_FLAG, int> combatLog;		
	map<EXPECT_BUILD, bool> expectbuild;

	COMBAT_FLAG combat;
	EXPECT_BUILD enemyBuild;

	StateMgr();
	~StateMgr();

public:
	static StateMgr* getInstance();
		
	void init();
	void show();
	void draw();
	void run();

	COMBAT_FLAG getCombat() { return combat; }

	void setCombatFlag(COMBAT_FLAG flag, bool fire) { combatFlag[flag].fire = fire; };
	void setOppFlag(EXPECT_BUILD build) { enemyBuild = build; };
	bool getCombatFlag(COMBAT_FLAG flag) { return combatFlag[flag].fire; };
	bool getExpectFlag(EXPECT_BUILD flag) { return expectbuild[flag]; };
	EXPECT_BUILD getEnemyBuild(){ return enemyBuild; };
	void updateEnemyBuildByScout();
	int checkFrontNexusTime();
};

