#include "Instance.h"

BWAPI::GameWrapper& bw = BWAPI::Broodwar;
MapInformation* mInfo = MapInformation::getInstance();
Information* info = Information::getInstance();
UnitControl* control = UnitControl::getInstance();
Commander* cmd = Commander::getInstance();
BuildMgr* buildMgr = BuildMgr::getInstance();
StateMgr* state = StateMgr::getInstance();

PositionMgr* posMgr = PositionMgr::getInstance();
Scout* scouting = Scout::getInstance();

ScvControl* scvControl = ScvControl::getInstance();
Scheduler* scheduler = Scheduler::getInstance();
Guard* guard = Guard::getInstance();

Defense* defense = Defense::getInstance();