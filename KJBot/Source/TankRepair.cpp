#include "TankRepair.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

TankRepair::TankRepair(DEFENSE_TYPE type) : DefenceEvent(type)
{	
	
}

TankRepair::~TankRepair()
{

}

void TankRepair::chkFire(){

	if (!state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2)){
		int count = 0;
		for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
			if (getProgress(tank->self) < 90){
				fire = true;
				return;
			}
		}

		for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)){
			if (getProgress(tank->self) < 90){
				fire = true;
				return;
			}
		}
	}
}

void TankRepair::update(){

	getUnits(UnitTypes::Terran_SCV, 2);
}

void TankRepair::run()
{	
	chkFire();
	turnOff();
	if (!fire){
		return;
	}	

	update();
	act();
}

void TankRepair::act(){
	
	target = nullptr;

	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
		if (getProgress(tank->self) < 90){
			if (target == nullptr){
				target = tank;
			}
			else{
				if (tank->self->getHitPoints() < target->self->getHitPoints()){
					target = tank;
				}
			}
		}
	}

	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)){
		if (getProgress(tank->self) < 90){
			if (target == nullptr){
				target = tank;
			}
			else{
				if (tank->self->getHitPoints() < target->self->getHitPoints()){
					target = tank;
				}
			}
		}
	}

	if (target == nullptr){

		for (auto unit : defenceUnits[UnitTypes::Terran_SCV]){
			if (!unit->self->exists() || !unit->self->isCompleted()){
				continue;
			}

			if (bw->getFrameCount() % 32 == 0){
				unit->self->move((Position)posMgr->getTilePosition(POS::SIM1_BARRACK));
			}
		}

		return;
	}

	for (auto unit : defenceUnits[UnitTypes::Terran_SCV]){
		if (!unit->self->exists() || !unit->self->isCompleted()){
			continue;
		}

		if (!unit->self->isRepairing())
			unit->self->repair(target->self);
	}
}

void TankRepair::turnOff(){

	/*
	if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2) && target == nullptr) {
		eraseUnits(UnitTypes::Terran_SCV);
		fire = false;
	}
	*/

	if (target == nullptr) {
		eraseUnits(UnitTypes::Terran_SCV);
		fire = false;
	}

	/*
	if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2) && target == nullptr){
		eraseUnits(UnitTypes::Terran_SCV);
		fire = false;
	}
	*/
}