#pragma once
#include "BWAPI.h"
#include "MyUnit.h"

using namespace BWAPI;

class Error{
	Unit interrupter;
public:
	enum{
		MY_BUILDING,
		MY_UNIT,
	};
};

class Work{
public:
	enum WORK_TYPE{
		UNIT,
		TECH,
		UPGRADE,
	};

	WORK_TYPE workType;
	MyUnit* performer;
	Unit target;
	TilePosition pos;
	int type;

	int count;
	int beforehand;
	bool reStruct;

public:
	// Unit Work
	Work(WORK_TYPE workType, int type, int count = 1, MyUnit* performer = nullptr){
		this->workType = workType;
		this->type = type;
		this->performer = performer;
		this->count = count;
		this->beforehand = 45;
		this->pos = TilePosition(0, 0);
		this->target = nullptr;
	}
	
	// Build Work
	Work(WORK_TYPE workType, int type, TilePosition pos, MyUnit* performer = nullptr, int beforehand = 45, bool reStruct = false){
		this->workType = workType;
		this->count = 1;
		this->type = type;
		this->performer = performer;
		this->beforehand = beforehand;
		this->reStruct = reStruct;
		this->pos = pos;
		this->target = nullptr;
	}

	void setPerformer(MyUnit* performer){ this->performer = performer; };
	void setTarget(Unit target){ this->target = target; };
};

class BuildWork {

	UnitType comparison;
	int cmpCount;
	bool immediatly;
	bool isFired;
	int speed;

	Work* work;

public:
	// Unit Work
	BuildWork(Work::WORK_TYPE workType, UnitType comparison, int cmpCount, bool immediatly, int type, int count = 1, MyUnit* performer = nullptr, int speed = 2){

		this->comparison = comparison;
		this->cmpCount = cmpCount;
		this->immediatly = immediatly;
		this->isFired = false;
		this->speed = speed;

		work = new Work(workType, type, count, performer);
	}

	// Building Work
	BuildWork(Work::WORK_TYPE workType, UnitType comparison, int cmpCount, bool immediatly, int type, TilePosition pos, MyUnit* performer = nullptr, int beforehand = 45, int speed = 2, bool reStruct = false){

		this->comparison = comparison;
		this->cmpCount = cmpCount;
		this->immediatly = immediatly;
		this->isFired = false;
		this->speed = speed;

		work = new Work(workType, type, pos, performer, beforehand, reStruct);
	}

	bool run();
	Work* getWork(){ return work; };
	void onFire(){ isFired = true; };
	bool getFire(){ return isFired; };
	int getSpeed(){ return speed; };
};