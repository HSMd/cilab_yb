#pragma once
#include <BWAPI.h>
#include "MyUnit.h"

using namespace BWAPI;

class ScvControl{

	ScvControl();
	~ScvControl();

public:
	static ScvControl * getInstance();	

	void run(MyUnit* unit);
};