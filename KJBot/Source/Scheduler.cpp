#include "Scheduler.h"
#include "Instance.h"
#include "Utility"

Scheduler::Scheduler(){

	runMineral = 0;
	runGas = 0;
}

Scheduler::~Scheduler(){

}

Scheduler* Scheduler::getInstance(){

	static Scheduler instance;
	return &instance;
}

void Scheduler::show(){

	runMineral = 0;
	runGas = 0;

	int x = 500, y = 120;
	bw->drawTextScreen(x, y, "Schedule Count : %d", schedule.size());

	int count = 0;
	for (auto s : schedule){
		if (count == 5)
			break;

		count++;
		y += 20;

		//string str = "";
		if (s->workType == Work::WORK_TYPE::UNIT){
			UnitType type = s->type;
			bw->drawTextScreen(x, y, "%s", type.c_str());
		}
		else if (s->workType == Work::WORK_TYPE::UPGRADE){
			UpgradeType type = s->type;
			bw->drawTextScreen(x, y, "%s", type.c_str());
		}
		else if (s->workType == Work::WORK_TYPE::TECH){
			TechType type = s->type;
			bw->drawTextScreen(x, y, "%s", type.c_str());
		}
	}

	y += 20;
	bw->drawTextScreen(x, y, "mineral : %d", getMineral());
	y += 20;
	bw->drawTextScreen(x, y, "gas : %d", getGas());

	y = 120;
	x -= 150;
	count = 0;

	bw->drawTextScreen(x, y, "running Count : %d", running.size());

	for (auto s : running){
		if (count == 5)
			break;

		count++;
		y += 20;

		if (s->workType == Work::WORK_TYPE::UNIT){
			UnitType type = s->type;
		bw->drawTextScreen(x, y, "%s", type.c_str());
		}
		else if (s->workType == Work::WORK_TYPE::UPGRADE){
			UpgradeType type = s->type;
			bw->drawTextScreen(x, y, "%s", type.c_str());
		}
		else if (s->workType == Work::WORK_TYPE::TECH){
			TechType type = s->type;
			bw->drawTextScreen(x, y, "%s", type.c_str());
		}
	}
}

void Scheduler::draw(){

}

void Scheduler::buildingCreate(Unit unit){
	
	usedMineral -= unit->getType().mineralPrice();
	usedGas -= unit->getType().gasPrice();

	for (auto run : running){
		if (run->pos == unit->getTilePosition()){
			run->setTarget(unit);
		}
	}
}

// Add Schedule
bool Scheduler::isUnit(Work* work){

	if (work->type != Work::WORK_TYPE::UNIT){
		return false;
	}

	UnitType type = work->type;

	if (!type.isBuilding()){
		if (UnitTypes::Terran_SCV == work->type){
			return false;
		}

		return true;
	}
	else
		return false;
}

void Scheduler::addSchedule(Work* work, int hurry){

	if (hurry == BUILD_SPEED::FAST){
		for (int i = 0; i < work->count; i++)
			schedule.push_front(work);
	}
	else if (hurry == BUILD_SPEED::NORMAL){
		auto iter = find_if(schedule.begin(), schedule.end(), Scheduler::isUnit);

		for (int i = 0; i < work->count; i++)
			schedule.insert(iter, work);
	}
	else{
		for (int i = 0; i < work->count; i++)
			schedule.push_back(work);
	}
}

void Scheduler::deleteSchedule(TilePosition pos){

	for (auto iter = schedule.begin(); iter != schedule.end(); ++iter){
		Work* work = (*iter);
		if (work->pos == pos){
			delete work;
			break;
		}	
	}
}

void Scheduler::deleteSchedule(UnitType type, bool all){

	for (auto iter = schedule.begin(); iter != schedule.end(); ++iter){
		Work* work = (*iter);
		if (work->type == type){
			delete work;
			if (!all){
				break;
			}
			else{
				iter = schedule.erase(iter);
				iter--;
			}
		}
	}
}

void Scheduler::deleteAllSchedule(){

	for (auto iter = schedule.begin(); iter != schedule.end(); ++iter){
		Work* work = (*iter);
		delete work;
		iter = running.erase(iter);
		--iter;
	}
}

// Run Function
void Scheduler::run(){

	for (auto iter = running.begin(); iter != running.end(); ++iter){
		Work* work = (*iter);
		Unit unit = (*iter)->target;
		
		if (unit != nullptr){
			if (unit->isCompleted()){
				if (work->performer->state == MyUnit::STATE::BUILD){
					// SCV 초기화

					bool hasNextWork = false;
					for (auto runWork : running){
						if (runWork != work && runWork->performer == work->performer){

							hasNextWork = true;
							break;
						}
					}

					for (auto runWork : schedule){
						if (runWork->performer == work->performer){

							hasNextWork = true;
							break;
						}
					}

					if (!hasNextWork){
						work->performer->state = MyUnit::STATE::IDLE;
						for (auto tcv : UnitControl::getInstance()->TCV) {
							for (auto scv : tcv->scvSet) {
								if (work->performer == scv) {
									scv->state = MyUnit::STATE::IDLE;
									scv->targetPos = TilePosition(-1, -1);
									break;
								}
							}
						}
					}
					else{
						work->performer->buildMoney = false;
					}

					delete work;
					iter = running.erase(iter);
					--iter;
				}
			}
			else{
				if (!work->performer->self->isConstructing()){

					bw << "gogo" << endl;

					work->performer->self->rightClick(work->target);
					work->performer->state = MyUnit::STATE::BUILD;
				}
			}
		}
		else{
			work->performer->targetType = work->type;
			work->performer->targetPos = work->pos;
			work->performer->state = MyUnit::STATE::BUILD;
		}
	}

	if (state->getCombatFlag(COMBAT_FLAG::MORE_SCV)){
		return;
	}

	for (auto iter = schedule.begin(); iter != schedule.end(); ++iter){

		Work* work = (*iter);

		if (work->workType == Work::WORK_TYPE::UNIT){
			UnitType targetType = work->type;

			// 이전에 필요한 것들이 없다면 넘겨라.
			if (!enoughWhatBuild(Work::WORK_TYPE::UNIT, targetType)){
				continue;
			}

			if (targetType.isBuilding() && !targetType.isAddon()){
				if (runBuildingSchedule(work, iter)){
					runMineral += targetType.mineralPrice();
					runGas += targetType.gasPrice();
					continue;
				}
			}
			else if (targetType.isAddon()){
				if (runAddonSchedule(work, iter)){
					runMineral += targetType.mineralPrice();
					runGas += targetType.gasPrice();
					continue;
				}
			}
			else{
				if (runUnitSchedule(work, iter)){
					runMineral += targetType.mineralPrice();
					runGas += targetType.gasPrice();
					continue;
				}
			}
		}
		else if (work->workType == Work::WORK_TYPE::TECH){
			TechType targetType = work->type;

			if (!enoughWhatBuild(Work::WORK_TYPE::TECH, targetType)){
				continue;
			}

			if (runTechSchedule(work, iter)){
				runMineral += targetType.mineralPrice();
				runGas += targetType.gasPrice();
				continue;
			}
		}
		else if (work->workType == Work::WORK_TYPE::UPGRADE){
			UpgradeType targetType = work->type;

			if (!enoughWhatBuild(Work::WORK_TYPE::UPGRADE, targetType)){
				continue;
			}

			if (runUpgradeSchedule(work, iter)){
				runMineral += targetType.mineralPrice();
				runGas += targetType.gasPrice();

				continue;
			}
		}
	}
}

bool Scheduler::runUnitSchedule(Work* work, list<Work*>::iterator& iter){
	
	UnitType targetType = work->type;
	MyUnit* performer = work->performer;

	if (performer != nullptr){
		if (!performer->self->exists()){
			iter = schedule.erase(iter);
			iter--;

			return false;
		}

		/*
		if (getReadyTrain(performer->self)){
			if (!enoughResource(Work::WORK_TYPE::UNIT, targetType))
				return true;
		}
		else{
			return false;
		}
		*/
	}
	else{

		/*
		if (!enoughResource(Work::WORK_TYPE::UNIT, targetType))
			return false;
		*/

		performer = getProperBuilding(Work::WORK_TYPE::UNIT, targetType);
		if (performer == nullptr)
			return false;
		
		/*
		if (!getReadyTrain(performer->self))
			return false;
		*/
	}

	if (getReadyTrain(performer->self)){
		if (!enoughResource(Work::WORK_TYPE::UNIT, targetType))
			return true;
	}
	else{
		return false;
	}

	// 실제로 병력을 뽑는 것을 수행.
	if (performer->self->train(targetType)){

		// Tomorrow
		//delete work;

		iter = schedule.erase(iter);
		iter--;
	}
	
	return false;
}

bool Scheduler::runAddonSchedule(Work* work, list<Work*>::iterator& iter){

	UnitType targetType = work->type;

	if (!enoughResource(Work::WORK_TYPE::UNIT, 45)){
		return true;
	}

	auto performer = getProperBuilding(Work::WORK_TYPE::UNIT, targetType);
	if (performer == nullptr)
	{
		return false;
	}

	if (performer->self->canBuildAddon() && performer->self->buildAddon(targetType)){
		usedMineral += targetType.mineralPrice();
		usedGas += targetType.gasPrice();

		iter = schedule.erase(iter);
		iter--;
		//delete work;
	}
	else{
		return true;
	}

	return false;
}

bool Scheduler::runBuildingSchedule(Work* work, list<Work*>::iterator& iter){
	
	// Building
	UnitType targetType = work->type;
	TilePosition targetPos = work->pos;
	MyUnit* performer = work->performer;	
	
	// 돈이 되는지 보는것.
	if (!enoughResource(Work::WORK_TYPE::UNIT, targetType, work->beforehand)){
		return false;
	}

	if (performer == nullptr){
		MyUnit* worker = nullptr;
		
		if (targetPos == TilePosition(0, 0)){				
			if (targetType == UnitTypes::Terran_Refinery){

				targetPos = posMgr->getGasPosition(info->map->getMyTerritorys()[info->self->getBuildingCount(UnitTypes::Terran_Refinery, true)]);

				worker = info->map->getNearTerritory(targetPos)->getNearWorker((Position)targetPos, false);
				if (worker == nullptr) {
					bw << "??" << endl;
					worker = info->map->getMyTerritorys()[0]->getNearWorker((Position)targetPos, false);
				}
			}
			else{
				//worker = info->map->getMyTerritorys()[0]->getWorker(false, targetPos);
				worker = info->map->getNearTerritory(targetPos)->getWorker(false);
				if (worker == nullptr){
					worker = info->map->getMyTerritorys()[0]->getWorker(false);
				}
				targetPos = bw->getBuildLocation(targetType, worker->getTilePosition());
			}
			
			work->pos = targetPos;
		}
		else{
			worker = info->map->getNearTerritory(targetPos)->getWorker(false, targetPos);
			if (worker == nullptr){
				worker = info->map->getMyTerritorys()[0]->getWorker(false, targetPos);
			}
		}

		worker->state = MyUnit::STATE::BUILD;
		work->performer = worker;
	}
	else{
		work->performer->state = MyUnit::STATE::BUILD;

		if (targetPos == TilePosition(0, 0)){
			targetPos = bw->getBuildLocation(targetType, performer->getTilePosition());
			work->pos = targetPos;
		}
	}

	posMgr->planUpload(targetType, targetPos);

	work->performer->targetType = targetType;
	work->performer->targetPos = targetPos;

	running.push_back(work);

	iter = schedule.erase(iter);
	iter--;	

	return false;
}

bool Scheduler::runTechSchedule(Work* work, list<Work*>::iterator& iter){

	TechType targetType = work->type;
	
	auto performer = getProperBuilding(Work::WORK_TYPE::TECH, targetType);
	if (performer == nullptr)
	{
		return false;
	}

	if (!enoughResource(Work::WORK_TYPE::TECH, targetType)){
		return true;
	}

	if (performer->self->research(targetType)){

		if (targetType == TechTypes::Tank_Siege_Mode){
			state->setCombatFlag(COMBAT_FLAG::SIEGE_MODE, true);
		}
		else if (targetType == TechTypes::Stim_Packs){
			state->setCombatFlag(COMBAT_FLAG::STIM_PACKS, true);
		}
		
		iter = schedule.erase(iter);
		iter--;
		//delete work;
	}
	else{
		return true;
	}

	return false;
}

bool Scheduler::runUpgradeSchedule(Work* work, list<Work*>::iterator& iter){
	
	UpgradeType targetType = work->type;

	auto performer = getProperBuilding(Work::WORK_TYPE::UPGRADE, targetType);
	if (performer == nullptr)
	{
		return false;
	}

	if (!enoughResource(Work::WORK_TYPE::UPGRADE, targetType)){
		return true;
	}

	if (performer->self->upgrade(targetType)){

		if (targetType == UpgradeTypes::U_238_Shells){
			state->setCombatFlag(COMBAT_FLAG::U_238_SHELLS, true);
		}

		iter = schedule.erase(iter);
		iter--;
		//delete work;
	}
	else{
		return true;
	}

	return false;
}

MyUnit* Scheduler::getProperBuilding(Work::WORK_TYPE workType, int targetType){
	
	if (workType == Work::WORK_TYPE::UNIT){
		UnitType target = targetType;
		auto what = info->self->getBuildings(target.whatBuilds().first);

		for (auto b : what){
			if (target.isAddon()){
				if (b->self->exists() && b->self->canBuildAddon() && b->self->isIdle()){
					
					return b;
				}
			}
			else
			{
				if (getReadyTrain(b->self)){
					return b;
				}
			}
		}
	}
	else if (workType == Work::WORK_TYPE::TECH){

		TechType target = targetType;
		auto what = info->self->getBuildings(target.whatResearches());

		for (auto b : what){
			if (b->self->exists() && b->self->isIdle()){
				return b;
			}
		}
	}
	else if (workType == Work::WORK_TYPE::UPGRADE){

		UpgradeType target = targetType;
		auto what = info->self->getBuildings(target.whatUpgrades());

		for (auto b : what){
			if (b->self->exists() && b->self->isIdle()){
				return b;
			}
		}
	}

	return nullptr;
}


// Check Func
bool Scheduler::enoughResource(Work::WORK_TYPE workType, int targetType, int readyMineral){

	if (workType == Work::WORK_TYPE::UNIT){
		UnitType target = targetType;

		if (getSupply() < target.supplyRequired()){
			return false;
		}

		if (getMineral() < target.mineralPrice() - readyMineral ||
			getGas() < target.gasPrice() - 20) {

			return false;
		}
	}
	else if (workType == Work::WORK_TYPE::TECH){
		TechType target = targetType;

		if (getMineral() < target.mineralPrice() - readyMineral ||
			getGas() < target.gasPrice() - 20) {

			return false;
		}
	}
	else if (workType == Work::WORK_TYPE::UPGRADE){
		UpgradeType target = targetType;

		if (getMineral() < target.mineralPrice() - readyMineral ||
			getGas() < target.gasPrice() - 20) {

			return false;
		}
	}	

	return true;
}

bool Scheduler::enoughWhatBuild(Work::WORK_TYPE workType, int targetType){
	
	if (workType == Work::WORK_TYPE::UNIT){
		UnitType target = targetType;
		auto rUnits = target.requiredUnits();

		for (auto require : rUnits){
			UnitType rType = require.first;

			if (rType.isBuilding()){
				if (info->self->getBuildingCount(require.first) < 1){
					return false;
				}
			}
			else{
				if (info->self->getUnitCount(require.first) < 1){
					return false;
				}
			}
		}
	}	
	else if (workType == Work::WORK_TYPE::TECH){
		TechType target = targetType;
		auto require = target.whatResearches();

		if (info->self->getBuildingCount(require) < 1){
			return false;
		}
	}
	else if (workType == Work::WORK_TYPE::UPGRADE){
		UpgradeType target = targetType;
		auto require = target.whatUpgrades();

		if (info->self->getBuildingCount(require) < 1){
			return false;
		}
	}

	return true;
}

bool Scheduler::getReadyTrain(Unit unit){

	bool itNeedAddon = false;
	auto requires = unit->getType().requiredUnits();

	for (auto unit : requires){
		if (unit.first.isAddon()){
			itNeedAddon = true;
			break;
		}
	}

	if (unit->exists()){
		if (itNeedAddon && unit->getAddon() == nullptr){
			return false;
		}

		if (unit->isIdle()){
			return true;
		}
		else if (unit->getTrainingQueue().size() == 1 && 0 < unit->getRemainingTrainTime() && unit->getRemainingTrainTime() < 15){
			return true;
		}
	}

	return false;
}

int Scheduler::getUnitTypeCount(UnitType target){

	int re = 0;

	for (auto s : schedule){
		if (target == s->type)
			re++;
	}
	
	return re;
}

int Scheduler::getUnitTypeRunCount(UnitType target){

	int re = 0;

	for (auto s : running){
		if (target == s->type)
			re++;
	}

	return re;
}

bool Scheduler::canAddUnitType(UnitType target){
	return 0 == getUnitTypeCount(target);
}

MyUnit* Scheduler::getPerformerInSchedule(TilePosition pos){

	for (auto run : running){
		if (run->pos == pos){
			return run->performer;
		}
	}

	for (auto run : schedule){
		if (run->pos == pos){
			return run->performer;
		}
	}

	return nullptr;
}
