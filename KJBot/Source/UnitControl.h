#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include "MyUnit.h"

using namespace std;
using namespace BWAPI;

class TeamControl {
private:



public:
	enum STATE {
		IDLE,
		STRONG_ATTACK,
		CRAZY,
		ATTACK,
		TIGHTING,
		DEFENSE,
		SQUEEZE,

		BCN_START,
		BCN_CLOSED,
		BCN_FRONT,
		BCN_GOING,
		BCN_TIGHT,
		BCN_JOIN,
		BCN_FINISH,
		BCN_BJ,
	};

	int team;
	TeamControl(int team) {
		this->team = team;
		vultureSet.clear();
		tankSet.clear();
		goliathSet.clear();
		marineSet.clear();
		state = STATE::IDLE;
		attackPosition = Position(-1, -1);
		tankDanger = 0;
		isReset = false;
		printf("TeamControl Init\n");
		goingSetting = false;
		goingDariSetting = false;
	}
	~TeamControl() {}

	STATE state;
	vector <MyUnit*> scvSet;
	vector <MyUnit*> vultureSet;
	vector <MyUnit*> tankSet;
	vector <MyUnit*> goliathSet;
	vector <MyUnit*> marineSet;
	vector <MyUnit*> vessleList;
	vector <MyUnit*> medicSet;

	MyUnit* fTank = NULL;
	MyUnit* fMarine = NULL;

	void update();

	Position tankCenterPos;
	Position teamCenterPos;
	Position bionicCenterPos;
	int tR;
	int tankDanger;
	float tankRage;
	Position attackPosition;
	void resetTG();
	bool isReset;
	bool goingSetting;
	bool goingDariSetting;
};

class UnitControl
{
private:
	UnitControl();
	~UnitControl();
	static UnitControl *s_UnitControl;

public:
	static UnitControl * create();
	static UnitControl * getInstance();
	void init();
	void onFrame();
	void draw();
	void onUnitCreate(Unit);
	void onUnitDestroy(Unit);
	void onUnitComplete(Unit);

	void updateTeam();
	void changeTeam(MyUnit*, int);
	MyUnit* getUnit(UnitType);
	MyUnit* getUnit(UnitType ut, Position pos);
	void getUnit(MyUnit* unit);
	void returnUnit(MyUnit*);

	int totalTeamIndex = 0;

	vector<TeamControl*> TCV;

	int getTeamTankDanger(TeamControl* t);

	Position getTeamCenterPos(TeamControl* t);
	Position getTankCenterPos(TeamControl* t);
	Position getBionicCenterPos(TeamControl* t);

	map<Position, bool> finishSearchPos;
	Position finishNextPos;
	void updateFinishSearch();

	TeamControl::STATE getTeamState(TeamControl* t);

	Position getAttackPosition(TeamControl *t);

	void setTeamDicision();

	bool isReadyGoing();
	bool isReadyTight(int teamNumber);
	bool isReadyBJ();
	bool isReadyFinish();
	bool isUnReadyFinish();

	char* getControlString(TeamControl* t);

	int scanTime;
	bool changeTeamSetting;
};
