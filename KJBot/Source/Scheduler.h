#pragma once
#include <BWAPI.h>
#include <list>

#include "MyUnit.h"
#include "Work.h"

using namespace BWAPI;
using namespace std;

struct Resource{
	int canUseMineral;
	int canUseGas;
};

class Scheduler{

public:
	enum BUILD_SPEED{
		FAST,
		NORMAL,
		SLOW,
	};

private:
	int usedMineral;
	int usedGas;

	int runMineral;
	int runGas;

	list<Work*> schedule;
	list<Work*> running;

	Scheduler();
	~Scheduler();

public:

	static Scheduler* getInstance();	
	
	void show();
	void draw();
	void buildingCreate(Unit unit);

	// Run Schedule
	void run();
	bool runUnitSchedule(Work* work, list<Work*>::iterator& iter);
	bool runAddonSchedule(Work* work, list<Work*>::iterator& iter);
	bool runBuildingSchedule(Work* work, list<Work*>::iterator& iter);
	bool runTechSchedule(Work* work, list<Work*>::iterator& iter);
	bool runUpgradeSchedule(Work* work, list<Work*>::iterator& iter);
	
	MyUnit* getPerformerInSchedule(TilePosition pos);

	// AddSchedule
	static bool isUnit(Work* work);
	void addSchedule(Work* work, int hurry = 2);
	void deleteSchedule(TilePosition pos);
	void deleteSchedule(UnitType type, bool all = false);
	void deleteAllSchedule();

	// Check Func
	bool enoughResource(Work::WORK_TYPE workType, int targetType, int readyMineral = 0);
	bool enoughWhatBuild(Work::WORK_TYPE workType, int targetType);
	bool getReadyTrain(Unit unit);	
	MyUnit* getProperBuilding(Work::WORK_TYPE workType, int targetType);

	void updateUsedMineral(UnitType type){ usedMineral += type.mineralPrice(); usedGas += type.gasPrice(); };
	void deleteUsedMineral(UnitType type){ usedMineral -= type.mineralPrice(); usedGas -= type.gasPrice(); };

	bool canAddUnitType(UnitType target);
	int getUnitTypeCount(UnitType target);
	int getUnitTypeRunCount(UnitType target);
	int getMineral(){ return Broodwar->self()->minerals() - usedMineral - runMineral; };
	int getGas(){ return Broodwar->self()->gas() - usedGas - runGas; };
	int getSupply(){ return (Broodwar->self()->supplyTotal() - Broodwar->self()->supplyUsed()); };
	list<Work*>& getRunning(){ return running; };
};