#include "Bacanic.h"
#include "Instance.h"
#include "Work.h"
#include "Utility.h"

MyUnit* t_worker = nullptr;

void Bacanic::init(){

	auto front = info->map->getCandidateLocation();
	Territory* territory = new Territory(front);
	info->map->addMyTerritory(territory);

	performer = nullptr;
	spare = nullptr;
	frameCount = 0;
	
	progress = BUILD_START;
}

void Bacanic::show(){

}

void Bacanic::draw(){

}

void Bacanic::run(){

	// 빌드진행.
	buildProcess();

	// 서치.
	search();

	// 앞마당 확장
	expand_2();
	//expand();

	// 빌드 진행.
	runBuild();

	// SCV 생산.
	scvCreate();
	
	// 자동 유닛.
	autoUnit();

	// 가스 컨트롤
	gasControl();

	// 심시티 짓고 나간 SCV 처리
	simOut();

	// 건물 옮기기.
	move();

	// 막기
	//guardSelf();
}

void Bacanic::deleteBuild(UnitType type){

	for (auto iter = builds.begin(); iter != builds.end(); ++iter){
		BuildWork build = (*iter);

		if (build.getWork()->type == type){
			iter = builds.erase(iter);
			break;
		}
	}
}

void Bacanic::autoUnit(){
	
	if (1 <= info->self->getBuildingCount(UnitTypes::Terran_Factory)){
		flags[AUTO_TANK] = true;

		if (info->self->getUnitCount(UnitTypes::Terran_SCV) < 10){
			flags[AUTO_TANK] = false;
		}
	}

	if (4 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		flags[AUTO_SUPPLY] = true;

		if (info->self->getUnitCount(UnitTypes::Terran_SCV) < 10){
			flags[AUTO_SUPPLY] = false;
		}
	}

	if (4 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks)){
		flags[AUTO_MEDIC] = true;

		if (info->self->getUnitCount(UnitTypes::Terran_SCV) < 10){
			flags[AUTO_MEDIC] = false;
		}
	}

	if (flags[ADD_CENTER]){
		flags[AUTO_MARINE] = true;

		if (info->self->getUnitCount(UnitTypes::Terran_SCV) < 18){
			flags[AUTO_MARINE] = false;
		}
	}

	if (flags[AUTO_TANK]){
		for (auto& fac : info->self->getBuildings(UnitTypes::Terran_Factory)){
			if (scheduler->getReadyTrain(fac->self) && scheduler->canAddUnitType(UnitTypes::Terran_Siege_Tank_Tank_Mode))
				fac->self->train(UnitTypes::Terran_Siege_Tank_Tank_Mode);
		}
	}

	if (flags[AUTO_MEDIC]){
		for (auto& ba : info->self->getBuildings(UnitTypes::Terran_Barracks)){
			if (scheduler->getReadyTrain(ba->self) && scheduler->canAddUnitType(UnitTypes::Terran_Medic))
				if (info->self->getUnitCount(UnitTypes::Terran_Medic, true) * 4 < info->self->getUnitCount(UnitTypes::Terran_Marine, true))
					ba->self->train(UnitTypes::Terran_Medic);
		}
	}

	if (flags[AUTO_MARINE]){
		for (auto& ba : info->self->getBuildings(UnitTypes::Terran_Barracks)){
			if (scheduler->getReadyTrain(ba->self) && scheduler->canAddUnitType(UnitTypes::Terran_Marine))
				ba->self->train(UnitTypes::Terran_Marine);
		}
	}

	if (flags[AUTO_SUPPLY]){

		if (0 != scheduler->getUnitTypeRunCount(UnitTypes::Terran_Supply_Depot))
			return;

		if (Broodwar->self()->supplyTotal() < 200 * 2 && frameCount + 900 < bw->getFrameCount()){

			if (scheduler->getSupply() <= 8 * 2){
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
				frameCount = bw->getFrameCount();
			}
		}
	}
}

void Bacanic::search(){

	// 정찰.
	if (!flags[SEARCH] && 11 <= info->self->getUnitCount(UnitTypes::Terran_SCV)){
	//if (!flags[SEARCH] && 5 <= info->self->getUnitCount(UnitTypes::Terran_SCV)){

		auto worker = info->map->getMyTerritorys()[0]->getWorker(true);
		scouting->setSpy(worker);

		if (worker != nullptr)
			flags[SEARCH] = true;
	}
}

void Bacanic::gasControl(){

	auto startTerritory = info->map->getMyTerritorys().front();
	auto front = info->map->getMyTerritorys().back();

	if (!flags[GAS_CONTROL]){
		if (88 <= bw->self()->gas()){
			startTerritory->setGasScvCount(2);
		}

		if (96 <= bw->self()->gas()){
			startTerritory->setGasScvCount(1);
		}
	}
	else{
		/*
		int gasCount = 3;
		startTerritory->setGasScvCount(3);
		*/

		startTerritory->setGasScvCount(3);

		if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Refinery)){
			if (20 < info->self->getUnitCount(UnitTypes::Terran_SCV)){
				if (270 < bw->self()->gas()){
					front->setGasScvCount(1);
				}
				else{
					front->setGasScvCount(2);
				}
			}
		}

		if (startTerritory->getReady()){
			if (350 < bw->self()->gas()){
				startTerritory->setGasScvCount(1);
			}

			if (4 < startTerritory->getRequireSCVCount())
				startTerritory->setGasScvCount(0);
		}

		if (front->getReady() && 4 < front->getRequireSCVCount()){
			front->setGasScvCount(0);
		}
	}	
}

void Bacanic::simOut(){

	if (!state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1) || flags[ERROR_OUT]){
		return;
	}

	auto startTerritory = info->map->getMyTerritorys().front();
	auto chkPoint = getNearestChokepoint(startTerritory->getTilePosition());
	auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
	bool safe = info->map->getCloseEnemy().size() == 0;

	static bool isRun = false;

	if (performer == nullptr){
		performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_SUPPLY2));
	}
	else{
		if (performer->self->exists() && !performer->self->isConstructing()){
			if (!brk->self->isLifted()){

				if (performer->self->exists() && getDistance(startTerritory->getTilePosition(), performer->getTilePosition()) <= 18){
					flags[ERROR_OUT] = true;
					return;
				}

				if (getDistance(chkPoint->getCenter(), performer->getPosition()) < 60){
					performer->state = MyUnit::STATE::OUT;
					isRun = true;

					if (brk->self->isIdle() && safe){
						brk->self->lift();
					}
				}
				else{
					if (isRun && brk->self->isIdle()){
						flags[ERROR_OUT] = true;
					}
				}
			}
			else{
				if (performer->self->exists() && getDistance(startTerritory->getTilePosition(), performer->getTilePosition()) <= 18){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}

				if (!performer->self->exists()){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}

				if (brk->self->isIdle()){
					performer->state = MyUnit::STATE::MINERAL;
				}
			}
		}
	}

	/*
	if (performer == nullptr){
		performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_SUPPLY2));
	}
	else{
		if (performer->self->exists() && !performer->self->isConstructing()){
			if (!brk->self->isLifted()){
				if (getDistance(chkPoint->getCenter(), performer->getPosition()) < 60){
					performer->state = MyUnit::STATE::OUT;

					if (brk->self->isIdle() && safe){
						brk->self->lift();
					}
				}
				else{
					if (brk->self->isIdle()){
						flags[ERROR_OUT] = true;
					}
				}
			}
			else{
				if (performer->self->exists() && getDistance(startTerritory->getTilePosition(), performer->getTilePosition()) <= 18){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}
				
				if (!performer->self->exists()){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}

				if (brk->self->isIdle()){
					performer->state = MyUnit::STATE::MINERAL;
				}
			}
		}
	}
	*/
}

void Bacanic::scvCreate(){

	auto& cnters = info->self->getBuildings(UnitTypes::Terran_Command_Center);
	auto& scvs = info->self->getImmediatelyUnit(UnitTypes::Terran_SCV);

	// SCV 뽑기.
	if (flags[ADD_CENTER]){
		for (auto territory : info->map->getMyTerritorys()){
			if (!territory->getReady()){
				continue;
			}

			int require = territory->getRequireSCVCount();
			auto cnter = territory->getCmdCenter()->self;

			if (1 <= require && scheduler->getReadyTrain(cnter) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
				cnter->train(UnitTypes::Terran_SCV);
		}
	}
	else{		
		if (progress == BUILD_START){

			/*
			if (0 < info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Assimilator) && 12 < info->self->getUnitCount(UnitTypes::Terran_SCV, true)){
				if (scheduler->getMineral() < 150){
					return;
				}				
			}
			*/

			if (scvs.size() == 15){
				if (1 <= info->self->getBuildingCount(UnitTypes::Terran_Factory, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			else if (scvs.size() == 19){
				if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			else if (scvs.size() == 20){
				if (3 <= info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}			
			else{
				if (scheduler->getReadyTrain(cnters.front()->self))
					cnters.front()->self->train(UnitTypes::Terran_SCV);
			}			
		}
		else{
			if (scheduler->getReadyTrain(cnters.front()->self))
				cnters.front()->self->train(UnitTypes::Terran_SCV);
		}
	}


	// SCV 관리
	for (auto run : scheduler->getRunning()){
		if (run->performer != nullptr){
			if (!run->performer->self->exists()){
				auto performer = info->map->getMyTerritorys().front()->getWorker(false, run->pos);
				run->setPerformer(performer);
			}
		}
	}

	if (flags[ADD_CENTER]){
		if (14 < info->map->getMyTerritorys().front()->getScvCount() && info->map->getMyTerritorys().back()->getScvCount() < 3){
			int size = info->map->getMyTerritorys().front()->getScvCount() - 14;
			for (int i = 0; i < size; i++){
				auto worker = info->map->getMyTerritorys().front()->getWorker(true);
				info->map->getMyTerritorys().back()->addScv(worker);
			}
		}
	}
}

void Bacanic::expand(){
	
	if (info->self->getBuildingCount(UnitTypes::Terran_Command_Center) < 2){
		return;
	}
	
	MyUnit* cnter = info->self->getBuildings(UnitTypes::Terran_Command_Center).back();
	MyUnit* brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
	auto front = info->map->getMyTerritorys().back();

	if (!brk->self->isLifted()){
		if (info->map->getCloseEnemy().size() - info->map->getCloseCount(UnitTypes::Protoss_Probe) == 0){
			if (5 < getDistance(brk->getTilePosition(), posMgr->getTilePosition(POS::THIRD_FACTORY))){
				state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2, true);
				brk->self->lift();
			}
			else{
				posMgr->planUpload(UnitTypes::Terran_Factory, posMgr->getTilePosition(POS::THIRD_FACTORY));
			}
		}
		else if (info->map->getCloseEnemy().size() - info->map->getCloseCount(UnitTypes::Protoss_Probe) <= 2){
			if (5 < getDistance(brk->getTilePosition(), posMgr->getTilePosition(POS::THIRD_FACTORY)) && 0 < info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
				state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2, true);
				brk->self->lift();
			}
			else{
				posMgr->planUpload(UnitTypes::Terran_Factory, posMgr->getTilePosition(POS::THIRD_FACTORY));
			}
		}
		else{
			if (5 < getDistance(brk->getTilePosition(), posMgr->getTilePosition(POS::THIRD_FACTORY)) && 2 <= (info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) + info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode))){
				state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2, true);
				brk->self->lift();
			}
			else{
				if (brk->self->isIdle()){
					posMgr->planUpload(UnitTypes::Terran_Factory, posMgr->getTilePosition(POS::THIRD_FACTORY));
				}
			}
		}		
	}
	else{
		if (getDistance(brk->getTilePosition(), posMgr->getTilePosition(POS::THIRD_FACTORY)) < 5){
			if (brk->self->isIdle())
				brk->self->land(posMgr->getTilePosition(POS::THIRD_FACTORY));
		}
		else{
			brk->self->move((Position)posMgr->getTilePosition(POS::THIRD_FACTORY));
		}
	}

	if (flags[ADD_CENTER]){
		info->map->getMyTerritorys().front()->immigrant(front, Territory::WORK::OVER);
		return;
	}

	if (!cnter->self->isLifted()){
		if (5 < getDistance(cnter->getTilePosition(), front->getTilePosition())){
			cnter->self->lift();
		}
		else{
			if (cnter->self->isIdle()){
				front->setCmdCenter(cnter);
				front->init(front->getBase());
				front->getReady() = true;

				front->setGasScvCount(1);
				front->setFitScvCount(12);

				for (int i = 0; i < 3; i++){
					auto unit = info->map->getMyTerritorys().front()->getWorker(true);
					front->addScv(unit);
				}				

				flags[ADD_CENTER] = true;
			}
		}
	}
	else{
		if (getDistance(cnter->getTilePosition(), front->getTilePosition()) < 5){
			if (0 < info->enemy->getUnitCount(UnitTypes::Protoss_Dark_Templar)){
				if (cnter->self->isIdle())
					cnter->self->land(front->getTilePosition());
			}
			else{
				if (cnter->self->isIdle())
					cnter->self->land(front->getTilePosition());
			}
		}
		else{
			if (info->map->getCloseEnemy().size() - info->map->getCloseCount(UnitTypes::Protoss_Probe) <= 2){

				cnter->self->move((Position)front->getTilePosition());
			}
			else{

				if (2 <= (info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) + info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode))){
					cnter->self->move((Position)front->getTilePosition());
				}
				else{
					if (6 < getDistance(cnter->getTilePosition(), front->getTilePosition())){
						cnter->self->move((Position)front->getTilePosition());
					}
					else{
						cnter->self->stop();
					}
				}
			}
		}	
	}
}

float Bacanic::getValue(UnitType type){

	if (type == UnitTypes::Protoss_Zealot){
		return 1;
	}

	if (type == UnitTypes::Protoss_Dragoon){
		return 2;
	}

	if (type == UnitTypes::Terran_Marine){
		return 0.5;
	}

	if (type == UnitTypes::Terran_Siege_Tank_Tank_Mode){
		return 2;
	}

	if (type == UnitTypes::Terran_Siege_Tank_Siege_Mode){
		return 2.5;
	}

	if (type == UnitTypes::Protoss_Dragoon){
		return 2;
	}

	return 0.0f;
}

bool Bacanic::canExpand(){

	// 시즈가 가능한지 여부 확인.
	if (!bw->self()->hasResearched(TechTypes::Tank_Siege_Mode)){
		return false;
	}

	int dark = info->map->getCloseCount(UnitTypes::Protoss_Dark_Templar);
	int dragoon = info->map->getCloseCount(UnitTypes::Protoss_Dragoon);
	int zealot = info->map->getCloseCount(UnitTypes::Protoss_Zealot);

	if (0 < dark && info->self->getBuildingCount(UnitTypes::Terran_Comsat_Station) < 1){
		bw << "show Dark" << endl;
		return false;
	}

	if (state->getEnemyBuild() == EXPECT_BUILD::ONEGATECORE_DARK && info->self->getBuildingCount(UnitTypes::Terran_Comsat_Station) < 1){
		return false;
	}

	auto chkPoint = BWTA::getNearestChokepoint(info->map->getMyStartBase()->getTilePosition());
	auto& close = info->map->getMyTerritorys().back()->getCloseEnemy();
	int enemyScore = 0;
	int myScore = -1;

	if (0 < close.size()){
		int enemy = 0;
		for (auto c : close){
			if (c->getType() == UnitTypes::Protoss_Dark_Templar && 0 < info->self->getBuildingCount(UnitTypes::Terran_Comsat_Station)){
				continue;
			}

			if (c->getType() == UnitTypes::Protoss_Probe || c->getType() == UnitTypes::Protoss_Observer){
				continue;
			}

			if (getDistance(c->getPosition(), chkPoint->getCenter()) < 32 * 6){
				enemy++;
				enemyScore += getValue(c->getType());
			}
		}

		if (0 < enemy){
			bw << "Many Close Enemy" << endl;
			return false;
		}

		// value
		for (auto unit : UnitControl::getInstance()->TCV[0]->tankSet){
			myScore += getValue(unit->getType());
		}

		for (auto unit : UnitControl::getInstance()->TCV[0]->marineSet){
			myScore += getValue(unit->getType());
		}

		auto gap = enemyScore - myScore;

		if (gap <= 1.5){
			return true;
		}
		else{
			return false;
		}
	}	

	return true;
}

void Bacanic::expand_2(){

	auto brkPos = posMgr->getTilePosition(POS::THIRD_FACTORY);
	posMgr->planUpload(UnitTypes::Terran_Barracks, brkPos);
	static bool canUseData = false;
	auto expand = canExpand();

	if (info->self->getBuildingCount(UnitTypes::Terran_Engineering_Bay) < 1){
		return;
	}

	MyUnit* engine = info->self->getBuildings(UnitTypes::Terran_Engineering_Bay).front();
	if (!engine->self->isLifted()){
		if (!flags[ADD_CENTER]){
			engine->self->lift();
		}
	}
	else{
		auto enginePos = posMgr->getTilePosition(POS::ENGINEERING);
		auto sight = posMgr->getTilePosition(POS::ENGINE_SIGHT_1);

		if (flags[ADD_CENTER]){
			// 확장 후 코드
			if (getDistance(enginePos, engine->getTilePosition()) < 5){
				engine->self->land(enginePos);
			}
			else{
				engine->self->move((Position)enginePos);
			}
		}
		else{
			// 확장 전 코드
			if (getDistance(engine->getTilePosition(), sight) <= 2){
				canUseData = true;
			}

			if (!canUseData){
				engine->self->move((Position)sight);
			}
			else{
				if (getProgress(engine->self) <= 90){
					if (expand){
						engine->self->move((Position)sight);
					}
					else{
						engine->self->move((Position)enginePos);
					}
				}
				else{
					engine->self->move((Position)sight);
				}
			}			
		}
	}

	if (info->self->getBuildingCount(UnitTypes::Terran_Command_Center) < 2){
		return;
	}

	MyUnit* cnter = info->self->getBuildings(UnitTypes::Terran_Command_Center).back();
	MyUnit* brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
	auto front = info->map->getMyTerritorys().back();

	// 배럭 코드.
	if (!brk->self->isLifted()){
		if (canUseData && expand){
			if (2 < getDistance(brk->getTilePosition(), brkPos)){
				brk->self->lift();
				state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2, true);
			}
		}		
	}
	else{
		if (getDistance(brk->getTilePosition(), brkPos) < 5){
			brk->self->land(brkPos);
		}
		else{
			brk->self->move((Position)brkPos);
		}
	}

	if (flags[ADD_CENTER]){
		info->map->getMyTerritorys().front()->immigrant(front, Territory::WORK::OVER);
		return;
	}

	// 커맨더 센터 코드.
	if (!cnter->self->isLifted()){
		if (3 < getDistance(cnter->getTilePosition(), front->getTilePosition())){
			cnter->self->lift();
		}
		else{
			if (cnter->self->isIdle()){
				front->setCmdCenter(cnter);
				front->init(front->getBase());
				front->getReady() = true;

				front->setGasScvCount(1);
				front->setFitScvCount(12);

				for (int i = 0; i < 4; i++){
					auto unit = info->map->getMyTerritorys().front()->getWorker(true);
					front->addScv(unit);
				}

				flags[ADD_CENTER] = true;
			}
		}
	}
	else{
		if (canUseData && expand){
			if (getDistance(cnter->getTilePosition(), front->getTilePosition()) < 5){
				cnter->self->land(front->getTilePosition());
			}
			else{
				cnter->self->move(front->getPosition());
			}
		}
		else{
			cnter->self->move((Position)posMgr->getTilePosition(POS::SECOND_CENTER));
		}
	}
}

void Bacanic::runBuild(){

	if (flags[ADD_CENTER]){
		if (info->self->getUnitCount(UnitTypes::Terran_SCV) < 18 && scheduler->getMineral() < 200){
			state->setCombatFlag(COMBAT_FLAG::MORE_SCV, true);
			return;
		}
		else{
			state->setCombatFlag(COMBAT_FLAG::MORE_SCV, false);
		}
	}
	else{
		if (info->self->getUnitCount(UnitTypes::Terran_SCV) < 8 && scheduler->getMineral() < 200){
			state->setCombatFlag(COMBAT_FLAG::MORE_SCV, true);
			return;
		}
		else{
			state->setCombatFlag(COMBAT_FLAG::MORE_SCV, false);
		}
	}

	// 저장된 빌드들을 수행.
	for (auto iter = builds.begin(); iter != builds.end(); ++iter){
		BuildWork& build = (*iter);
		auto work = build.getWork();

		if (!build.getFire() && build.run()){
			if (build.getWork()->type == UnitTypes::Terran_Supply_Depot && 2 * 10 < scheduler->getSupply()){
				iter = builds.erase(iter);
				iter--;

				continue;
			}

			if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_BARRACK)){
				auto performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_SUPPLY));
				work->setPerformer(performer);
			}
			else if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_SUPPLY2)){
				performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_BARRACK));
				work->setPerformer(performer);
			}

			scheduler->addSchedule(build.getWork(), build.getSpeed());

			iter = builds.erase(iter);
			iter--;
		}
	}
}

void Bacanic::guardSelf(){
	
	auto unitController = UnitControl::getInstance()->getInstance();

	if (5 < info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) + info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode)){
		if (0 < info->map->getCloseEnemy().size() - info->map->getCloseCount(UnitTypes::Protoss_Observer)){
			if (0 < info->map->getMyTerritorys().front()->getCloseCount(UnitTypes::Protoss_Shuttle) || 0 < info->map->getMyTerritorys().front()->getCloseCount(UnitTypes::Protoss_Reaver)){

			}
			else{
				for (auto iter = guardTank.begin(); iter != guardTank.end(); ++iter){
					MyUnit* tank = (*iter);
					//unitController->returnUnit(tank);
					
					iter = guardTank.erase(iter);
					iter--;
				}
			}
		}
		else{
			if (guardTank.empty()){
				auto tank = unitController->getUnit(UnitTypes::Terran_Siege_Tank_Siege_Mode, (Position)posMgr->getTilePosition(POS::START_TURRET));
				if (tank != nullptr){
					guardTank.push_back(tank);
				}
			}
		}
	}

	for (auto iter = guardTank.begin(); iter != guardTank.end(); ++iter){
		MyUnit* tank = (*iter);

		if (!tank->self->exists()){
			iter = guardTank.erase(iter);
			iter--;
		}

		if (getDistance(posMgr->getTilePosition(POS::START_TURRET), tank->getTilePosition()) < 2){
			if (tank->self->canSiege())
				tank->self->siege();
		}
		else{
			if (tank->self->isSieged()){
				tank->self->unsiege();
			}

			tank->self->move((Position)posMgr->getTilePosition(POS::START_TURRET));
		}
	}
}
///////////////////////

void Bacanic::buildProcess(){

	if (0 < info->self->getBuildingCount(UnitTypes::Terran_Factory, true)){
		//progress = BUILD_MID;
	}

	if (progress == BUILD_START){
		buildStart();
	}
	else if (progress == BUILD_MID){
		buildMid();
	}
}

void Bacanic::buildStart(){

	auto startTerritory = info->map->getMyTerritorys().front();
	int zealotCount = startTerritory->getCloseCount(UnitTypes::Protoss_Zealot);

	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true) && 1 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, true);
	}

	if (2 <= info->self->getBuildingCount(UnitTypes:: Terran_Command_Center, true)){
		flags[GAS_CONTROL] = true;
	}

	if (!flags[BUILD_START]){
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 8, false, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY), nullptr, 60));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, 1, true, UnitTypes::Terran_Barracks, posMgr->getTilePosition(POS::SIM1_BARRACK)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 12, true, UnitTypes::Terran_Refinery));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 10, true, UnitTypes::Terran_Marine));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 14, true, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY2), nullptr));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 12, true, UnitTypes::Terran_Marine));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 14, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 50));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Machine_Shop, TilePosition(0, 0), nullptr, 45, Scheduler::BUILD_SPEED::FAST));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Marine, 1));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Marine, 1));

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Command_Center, posMgr->getTilePosition(POS::SECOND_CENTER), nullptr, 60));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Siege_Tank_Tank_Mode, 2));

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true), nullptr, 20));
		builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Supply_Depot, 3, true, TechTypes::Tank_Siege_Mode));

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Engineering_Bay, posMgr->getTilePosition(POS::ENGINEERING)));

		flags[BUILD_START] = true;
	}

	if (builds.empty() && !flags[BUILD_START_2]){		

		//builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Engineering_Bay, posMgr->getTilePosition(POS::ENGINEERING)));
		//builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Marine));

		flags[BUILD_START_2] = true;
	}

	bool pylonRush = guard->getFire(DEFENSE_TYPE::PYLON_RUSH);
	bool gasRush = guard->getFire(DEFENSE_TYPE::GAS_TERROR);

	if (pylonRush){
		if (false){
			if (!flags[BUILD_START_PYLON_DOOR]){
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true), nullptr), Scheduler::BUILD_SPEED::FAST);
				flags[BUILD_START_PYLON_DOOR] = true;
			}			
		}

		flags[AUTO_MARINE] = true;
	}
	
	if (gasRush){
		flags[AUTO_MARINE] = true;
	}

	if (4 <= info->self->getUnitCount(UnitTypes::Terran_Marine)){
		flags[AUTO_MARINE] = false;
	}

	if (builds.empty()){
		progress = BUILD_MID;
	}
}

void Bacanic::buildMid(){

	int closeDragoonCount = info->map->getCloseCount(UnitTypes::Protoss_Dragoon);
	auto front = info->map->getMyTerritorys().back();

	if (1 < closeDragoonCount){
		flags[INVASION] = true;
	}

	if (!flags[BUILD_MID]){
		builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Engineering_Bay, 1, false, UpgradeTypes::Terran_Infantry_Armor));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, false, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)));

		flags[BUILD_MID] = true;
	}

	if (flags[ADD_CENTER]){
		if (!flags[BUILD_MID_2]){
			if (8 <= info->map->getMyTerritorys().back()->getScvCount()){
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Refinery, info->resource->getFrontGasTilePos(front->getBase())));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery, 2, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery, 2, true, UnitTypes::Terran_Machine_Shop));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery, 2, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true, UnitTypes::Terran_Academy, posMgr->getTilePosition(POS::ACADEMY)));
				builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Academy, 1, true, TechTypes::Stim_Packs));
				builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Academy, 1, true, UpgradeTypes::U_238_Shells));

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, false, UnitTypes::Terran_Medic, 3, nullptr, Scheduler::BUILD_SPEED::FAST));

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 45));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 2, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 45));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 3, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 0));

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station));

				flags[BUILD_MID_2] = true;
			}			
		}		

		if (!flags[BUILD_TURRET] && 0 < info->self->getBuildingCount(UnitTypes::Terran_Engineering_Bay) && info->map->getCloseEnemy().size() == 0){

			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::SIM2_TURRET)), Scheduler::BUILD_SPEED::FAST);
			//scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::HILL_TURRET)), Scheduler::BUILD_SPEED::FAST);
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::START_TURRET)), Scheduler::BUILD_SPEED::SLOW);

			flags[BUILD_TURRET] = true;
		}

		if (!flags[BUILD_LAST_ADD]){
			if (4 == info->self->getBuildingCount(UnitTypes::Terran_Barracks)){
				if (400 <= scheduler->getMineral()){
					builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));
					builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));

					flags[BUILD_LAST_ADD] = true;
				}
			}
		}		
	}
}

void Bacanic::move(){

	for (auto iter = moveBuildings.begin(); iter != moveBuildings.end(); iter++){

		auto unit = (*iter);

		if (unit->self->isLifted()){
			if (5 < getDistance(unit->getTilePosition(), unit->targetPos)){
				unit->self->move((Position)unit->targetPos);
			}
			else{
				if (unit->self->isIdle())
					unit->self->land(unit->targetPos);
			}
		}
		else{
			if (getDistance(unit->getTilePosition(), unit->targetPos) < 5){
				if (unit->self->isIdle()){
					iter = moveBuildings.erase(iter);
					iter--;
				}
			}
			else{
				unit->self->lift();
			}
		}
	}
}