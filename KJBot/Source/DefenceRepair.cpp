#include "DefenceRepair.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

void repairTeam::run(){
		
	if (!target->self->exists()){
		for (auto& scv : performers){
			scv->state = MyUnit::STATE::IDLE;
		}

		performers.clear();

		return;
	}

	if (95 < getProgress(target->self)){
		for (auto& scv : performers){
			scv->state = MyUnit::STATE::IDLE;
		}

		performers.clear();

		return;
	}

	if (target->getType().maxHitPoints() <= 700){
		if (getProgress(target->self) < 80){
			require = 3;
		}
		else{
			require = 2;
		}
	}
	else{
		if (getProgress(target->self) <= 70){
			require = 3;
		}
		else if (getProgress(target->self) <= 90){
			require = 2;
		}
	}

	if (performers.size() < require){
		int more = require - performers.size();

		if (info->map->getMyTerritorys().back()->getReady()){

			for (int i = 0; i < more; i++){
				MyUnit* worker = info->map->getNearTerritory(target->getTilePosition())->getNearWorker(target->getPosition());
				if (worker == nullptr){
					worker = info->map->getMyTerritorys()[0]->getNearWorker(target->getPosition());
				}

				if (worker == nullptr){
					break;
				}

				performers.push_back(worker);
			}			
		}
		else{
			for (int i = 0; i < more; i++){
				MyUnit* worker = info->map->getMyTerritorys()[0]->getNearWorker(target->getPosition());
				if (worker == nullptr){
					break;
				}

				performers.push_back(worker);
			}
		}
	}

	for (auto iter = performers.begin(); iter != performers.end(); iter++){
		auto scv = (*iter);

		if (!scv->self->exists()){
			iter = performers.erase(iter);
			iter--;

			continue;
		}

		if (scv->self->canRepair()){
			scv->self->repair(target->self);
		}
	}
}

DefenceRepair::DefenceRepair(DEFENSE_TYPE type) : DefenceEvent(type)
{	
	
}

DefenceRepair::~DefenceRepair()
{

}

void DefenceRepair::chkFire(){

	fire = true;

	/*
	repairCount = CountRepairBuilding();		
	if (0 < repairCount){
		fire = true;
	}
	*/
}

void DefenceRepair::update(){

	/*
	if (0 < repairCount){
		getUnits(UnitTypes::Terran_SCV, 3);
	}
	*/
}

void DefenceRepair::run()
{	
	chkFire();
	turnOff();
	if (!fire){
		return;
	}	

	update();
	act();
}

bool DefenceRepair::needRepair(MyUnit* unit){

	if (unit->getType().maxHitPoints() <= 700){
		if (getProgress(unit->self) < 80)
			return true;	
	}
	else{
		if (getProgress(unit->self) < 65){
			return true;
		}
	}

	return false;
}

bool DefenceRepair::alreadyHasTeam(MyUnit* unit){

	for (auto team : rTeams){
		if (team->target == unit)
			return true;
	}

	return false;
}

void DefenceRepair::act(){

	/*
	MyUnit* target = nullptr;
	auto allBuilding = info->self->getAllBuildings();
	for (auto iter = allBuilding.begin(); iter != allBuilding.end(); iter++)
	{
		MyUnit* build = (*iter);
		if (!needRepair(build)){
			continue;
		}

		if (target == nullptr){
			target = build;
		}
		else{
			if (build->self->getHitPoints() < target->self->getHitPoints()){
				target = build;
			}
		}
	}

	for (auto unit : defenceUnits[UnitTypes::Terran_SCV]){

		if (!unit->self->exists() || !unit->self->isCompleted()){
			continue;
		}

		if (unit->self->canRepair()){
			unit->self->repair(target->self);
		}
	}
	*/

	for (auto iter = rTeams.begin(); iter != rTeams.end(); iter++){
		auto team = (*iter);
		team->run();
	}

	auto& buildings = info->self->getAllBuildings();
	for (auto iter = buildings.begin(); iter != buildings.end(); iter++){
		auto b = (*iter);
		if (b == nullptr){
			continue;
		}

		if (needRepair(b)){
			if (!alreadyHasTeam(b)){
				bw << "New Team" << endl;
				rTeams.push_back(new repairTeam(b));
			}
			else{
				bw << "already Has Team" << endl;
			}
		}
	}	
}

void DefenceRepair::turnOff(){

	/*
	if (repairCount == 0){
		eraseUnits(UnitTypes::Terran_SCV);
		fire = false;
	}
	*/
}


int DefenceRepair::CountRepairBuilding()
{
	int re = 0;
	for (auto build : info->self->getAllBuildings())
	{
		//if (build->self->getHitPoints() < build->self->getType().maxHitPoints() * 0.9)
		if (needRepair(build))
		{
			re++;
		}
	}

	return re;
}