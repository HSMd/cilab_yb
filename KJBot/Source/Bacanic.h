#pragma once
#include "BWAPI.h"
#include "MyUnit.h"
#include "Work.h"

#include <map>
#include <vector>

using namespace BWAPI;
using namespace std;

class Bacanic{

	enum FLAG{

		SEARCH,
		ADD_CENTER,
		INVASION,

		AUTO_MARINE,
		AUTO_MEDIC,
		AUTO_TANK,
		AUTO_SUPPLY,

		SCV_CONTROL_1,
		SCV_CONTROL_2,
		GAS_CONTROL,
		ERROR_OUT,

		BUILD_START, // ��������.
		BUILD_START_2,
		BUILD_START_PYLON_DOOR,

		BUILD_MID,
		BUILD_MID_2,
		BUILD_TURRET,

		BUILD_LAST,
		BUILD_LAST_ADD,		

		BUILD_HELL,
	};

	vector<BuildWork> builds;
	vector<MyUnit*> moveBuildings;

	vector<MyUnit*> guardTank;

	map<FLAG, bool> flags;

	MyUnit* performer;
	MyUnit* spare;

	FLAG progress;
	int frameCount;

public: 
	Bacanic(){};
	~Bacanic(){};

	void init();
	void show();
	void draw();
	void run();
	
	void guardSelf();
	void deleteBuild(UnitType type);

	void autoUnit();
	void search();

	void scvCreate();
	void runBuild();

	void simOut();
	void gasControl();
	void move();
	
	float getValue(UnitType type);
	bool canExpand();
	void expand_2();
	void expand();

	void buildProcess();
	void buildStart();
	void buildMid();
};