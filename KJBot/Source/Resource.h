#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <vector>
#include "MyUnit.h"

using namespace BWAPI;
using namespace std;

class ResourceInfo {

	map<Unit, Position> resourcePos;
	map<Unit, UnitType> resourceType;
	map<Unit, int> resourceAmount;

public:
	enum TYPE {
		MINERAL,
		GAS
	};

	ResourceInfo();
	~ResourceInfo();

	vector<Unit> minerals;
	vector<Unit> gases;

	TilePosition getFrontGasTilePos(BWTA::BaseLocation* base) {
		for (auto gas : gases) {
			if (BWTA::getNearestBaseLocation(getPosition(gas)) == base) {
				return gas->getTilePosition();
			}
		}
		return TilePosition(0, 0);
	};

	Position getPosition(Unit u) { return resourcePos[u]; };
	UnitType getType(Unit u) { return resourceType[u]; };
	int getAmount(Unit u) { return resourceAmount[u]; };
	
	void update();
	void draw();
};