#include "ScvControl.h"
#include "Utility.h"
#include "Instance.h"

ScvControl::ScvControl(){

}

ScvControl::~ScvControl(){

}

ScvControl* ScvControl::getInstance() {

	static ScvControl instance;
	return &instance;
}

void ScvControl::run(MyUnit* unit){

	auto self = unit->self;

	switch (unit->state){

	case MyUnit::STATE::MINERAL:

		if (10 < (int)getDistance(self->getTilePosition(), unit->target->getTilePosition())){
			self->move(unit->target->getPosition());

			break;
		}

		if (self->isCarryingMinerals() || self->isCarryingGas()){
			self->returnCargo(true);
		}
		else if (self->isIdle() || self->isGatheringGas()){
			self->gather(unit->target);
		}

	
		/*
		if (self->isUnderAttack())
		{
			for (auto enemy : info->enemy->getAllUnits())
			{
				if (enemy->getOrderTarget() == self)
				{
					self->attack(enemy);
					break;
				}
			}
		}
		else{

			if (self->isCarryingMinerals() || self->isCarryingGas()){
				self->returnCargo(true);
			}
			else if (self->isIdle() || self->isGatheringGas()){
				self->gather(unit->target);
			}
		}
		*/

		break;
	case MyUnit::STATE::BUILD:

		if ((int)getDistance(self->getTilePosition(), unit->targetPos) < 8){
			if (!self->isConstructing()){
				if (unit->buildMoney == false){
					
					scheduler->updateUsedMineral(unit->targetType);
					unit->buildMoney = true;
				}

				if (self->build(unit->targetType, unit->targetPos)){
					//scv->setError(true);
					;
				}
				else{
					auto target = self->getClosestUnit(Filter::IsEnemy, 32 * 4);
					if (target == nullptr){
						if (3 < (int)getDistance(self->getTilePosition(), unit->targetPos)){
							self->move((Position)unit->targetPos);
						}
					}
					else{
						self->attack(target);
					}
					
				}
			}
		}
		else{
			self->rightClick(tile2pos(unit->targetPos));
		}

		break;
				
	case MyUnit::STATE::GAS:

		if (self->isCarryingGas()){
			self->returnCargo(true);
		}
		else{
			if (self->isIdle() || self->isGatheringMinerals()){
				self->gather(unit->target);
			}
		}
		
		break;

	case MyUnit::STATE::OUT:
	{
		auto startTerritory = info->map->getMyTerritorys().front();
		auto chkPoint = BWTA::getNearestChokepoint(startTerritory->getTilePosition());

		self->move(chkPoint->getCenter());
	}

		break;
	}		
}
