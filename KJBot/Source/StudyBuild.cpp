#include "StudyBuild.h"
#include "Instance.h"
#include "Work.h"
#include "Utility.h"

void StudyBuild::init(){

	performer = nullptr;
	supplyFrame = 0;

	int s1 = 8;
	int s2 = 15;
	int gas = 12;
	int b1 = 10;

	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s1, true, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY), nullptr, 50));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, b1, true, UnitTypes::Terran_Barracks, posMgr->getTilePosition(POS::SIM1_BARRACK), nullptr, 80));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, gas, true, UnitTypes::Terran_Refinery));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s2, true, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY2)));

	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 1, true, UnitTypes::Terran_Marine));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, 2, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, 2, true, UnitTypes::Terran_Machine_Shop));

	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Command_Center, posMgr->getTilePosition(POS::SECOND_CENTER), nullptr, 100));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Siege_Tank_Tank_Mode));
	builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, TechTypes::Tank_Siege_Mode));

	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, false, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::HILL_TURRET)));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, false, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::SIM1_TURRET)));

	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
}

void StudyBuild::show(){

}

void StudyBuild::draw(){

}

void StudyBuild::run(){
	auto startTerritory = info->map->getMyTerritorys().front();

	// 자주 사용하는 유닛타입.
	auto tankType = UnitTypes::Terran_Siege_Tank_Tank_Mode;
	auto barrakType = UnitTypes::Terran_Barracks;
	auto marineType = UnitTypes::Terran_Marine;

	/*
	if (state->getCombatFlag(COMBAT_FLAG::SIEGE_MODE) && !flags[SIEGE_MODE]){
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Engineering_Bay, posMgr->getTilePosition(POS::ENGINEERING)));

		flags[SIEGE_MODE] = true;
	}
	*/

	guardChk();    // 플래그 체크.
	outScv();	   // 쫒겨난애.
	search();      // 정찰.
	gasControl();  // 가스 컨트롤
	expandFront(); // 확장	
	scvCreate();   // SCV 생산
	runBuildWork(); // BUILD 진행
	move();         // MOVE

	// 머신샵이 지어지고난 이후부터 탱크 지속적인 생산.
	if (0 < info->self->getBuildingCount(UnitTypes::Terran_Machine_Shop)){
		if (scheduler->canAddUnitType(tankType)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, tankType), Scheduler::BUILD_SPEED::SLOW);
		}
	}

	// 팩 2개부턴.
	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Factory)){
		if (scheduler->canAddUnitType(UnitTypes::Terran_Vulture)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Vulture), Scheduler::BUILD_SPEED::SLOW);
		}
	}

	//// 자동 서플 생산.
	//if (supplyFrame + 24 * 5 < bw->getFrameCount() && scheduler->getSupply() < 6){
	//	scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot)), Scheduler::BUILD_SPEED::SLOW);
	//	supplyFrame = bw->getFrameCount();
	//}

	// 팩 늘리기.

	/*
	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center) && !flags[JUDGE_2COMMAND]){
		scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)), Scheduler::BUILD_SPEED::SLOW);
		scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)), Scheduler::BUILD_SPEED::NORMAL);
		scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Machine_Shop), Scheduler::BUILD_SPEED::NORMAL);
		
		flags[JUDGE_2COMMAND] = true;
	}

	if (3 <= info->self->getBuildingCount(UnitTypes::Terran_Factory) && !flags[JUDGE_3FACTORY]){
		scheduler->addSchedule(new Work(Work::WORK_TYPE::TECH, TechTypes::Spider_Mines), Scheduler::BUILD_SPEED::NORMAL);
		scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot), Scheduler::BUILD_SPEED::NORMAL);
		//scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery), Scheduler::BUILD_SPEED::NORMAL);
		//scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::SECOND_TURRET)), Scheduler::BUILD_SPEED::NORMAL);

		flags[JUDGE_3FACTORY] = true;
	}
	*/


	// 1차 입구 막힘을 선포.
	
	

	
}

void StudyBuild::guardChk(){

	int count = 0;
	state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, false);

	for (auto unit : info->self->getImmediatelyBuilding(UnitTypes::Terran_Supply_Depot)){
		if (unit->getTilePosition() == posMgr->getTilePosition(POS::SIM1_SUPPLY)){
			count++;
		}

		if (unit->getTilePosition() == posMgr->getTilePosition(POS::SIM1_SUPPLY2)){
			count++;
		}
	}

	for (auto unit : info->self->getImmediatelyBuilding(UnitTypes::Terran_Barracks)){
		if (unit->getTilePosition() == posMgr->getTilePosition(POS::SIM1_BARRACK)){
			count++;
		}
	}

	if (3 <= count){
		state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, true);
	}
}

void StudyBuild::outScv(){

	if (flags[ERROR_OUT]){
		return;
	}

	auto startTerritory = info->map->getMyTerritorys().front();
	auto chkPoint = BWTA::getNearestChokepoint(startTerritory->getTilePosition());
	auto next = info->map->getCandidateLocation();

	bool open = true;

	for (auto unit : info->map->getCloseEnemy()){
		if (getDistance(unit->getTilePosition(), next->getTilePosition()) <= 12){

			open = false;
		}
	}

	// 쫒겨난 애들이 있나.
	if (!flags[OUT_SCV] && state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){		
		if (performer->self->exists() && getDistance(chkPoint->getCenter(), performer->getPosition()) < 50){
			performer->state = MyUnit::STATE::OUT;
		}

		if (performer2->self->exists() && getDistance(chkPoint->getCenter(), performer2->getPosition()) < 50){
			performer2->state = MyUnit::STATE::OUT;
		}

		// 다 짓고 쫒겨난 애들.
		if (performer->state == MyUnit::STATE::OUT || performer2->state == MyUnit::STATE::OUT){
			if (open){
				if (info->map->getCloseEnemy().size() == 0){
					auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
					if(brk->self->isIdle())
						if (brk->self->lift())
						{
							flags[OUT_SCV] = true;
						}							
				}
				else{
					flags[ERROR_OUT] = true;
					return;
				}
			}
		}
	}	

	if (flags[OUT_SCV]){
		auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();

		if (brk->self->isLifted()){
			if (getDistance(performer->getTilePosition(), startTerritory->getTilePosition()) < 18 &&
				getDistance(performer2->getTilePosition(), startTerritory->getTilePosition()) < 18){

				if (brk->self->isIdle()){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}				
			}
			else{
				if (brk->self->isIdle()){
					if (performer->state == MyUnit::STATE::OUT){
						performer->state = MyUnit::STATE::MINERAL;
					}

					if (performer2->state == MyUnit::STATE::OUT){
						performer2->state = MyUnit::STATE::MINERAL;
					}
				}
			}
		}
		else{
			if (brk->self->isIdle()){
				flags[ERROR_OUT] = true;
			}
		}
	}
}

void StudyBuild::search(){

	// 정찰.
	if (!flags[SEARCH] && 12 <= info->self->getUnitCount(UnitTypes::Terran_SCV)){

		auto worker = info->map->getMyTerritorys()[0]->getWorker(true);
		scouting->setSpy(worker);

		if (worker != nullptr)
			flags[SEARCH] = true;
	}
}

void StudyBuild::gasControl(){

	auto startTerritory = info->map->getMyTerritorys().front();

	// 가스 컨트롤
	if (!flags[GAS_CONTROL_2]){
		for (auto run : scheduler->getRunning()){
			if (run->type == UnitTypes::Terran_Command_Center){
				flags[GAS_CONTROL_2] = true;
				break;
			}
		}
	}

	if (!flags[GAS_CONTROL_2]){
		if (88 <= bw->self()->gas()){
			startTerritory->setGasScvCount(2);
		}

		if (96 <= bw->self()->gas()){
			startTerritory->setGasScvCount(1);
		}
	}
	else{
		startTerritory->setGasScvCount(3);
	}
}

void StudyBuild::expandFront(){
	
	if (info->self->getBuildingCount(UnitTypes::Terran_Command_Center) < 2 || flags[FLAG::EXPAND_CMD]){
		return;
	}

	auto next = info->map->getCandidateLocation();
	auto close = info->map->getCloseEnemy();
	auto cnter = info->self->getBuildings(UnitTypes::Terran_Command_Center).back();
	int enemyCount = 0;
	int darkCount = 0;

	/*
	if (!flags[EXPAND_FRONT] && flags[FLAG::EXPAND_CMD]){
		if (0 == darkCount && close.size() < info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) * 2){			

			flags[EXPAND_FRONT] = true;
		}

		return;
	}
	*/

	//for (auto unit : info->map->getCloseEnemy()){
	for (auto unit : bw->enemy()->getUnits()){
		if (unit->getType() == UnitTypes::Protoss_Dark_Templar){
			darkCount++;
		}

		if (getDistance(unit->getTilePosition(), next->getTilePosition()) <= 12){
			enemyCount++;
		}
	}

	bool safe = false;

	if (0 < info->enemy->getUnits(UnitTypes::Protoss_Dark_Templar).size()){
		if (2 <= info->self->getUnitCount(UnitTypes::Terran_Missile_Turret) || 3 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
			safe = true;
		}
	} else{
		safe = (enemyCount < 3 && darkCount == 0) || 4 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode);
	}

	if (!flags[FLAG::EXPAND_CMD]){
		if (cnter->self->isLifted()){
			if (safe){
				if (6 < getDistance(cnter->getTilePosition(), next->getTilePosition())){
					cnter->self->move(next->getPosition());
				}
				else{
					if (cnter->self->isIdle()){
						cnter->self->land(next->getTilePosition());
					}

					if (!flags[EXPAND_FRONT]){
						auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
						brk->targetPos = posMgr->getTilePosition(POS::FRONT_BARRACK);
						moveBuildings.push_back(brk);
						scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::FRONT_SUPPLY)), Scheduler::BUILD_SPEED::FAST);
						scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::SIM2_TURRET)), Scheduler::BUILD_SPEED::FAST);

						flags[EXPAND_FRONT] = true;
					}					
				}
			}
			else{
				//if (getDistance(cnter->getTilePosition(), next->getTilePosition()) < 5){
				if (getProgress(cnter->self) < 80){
					cnter->self->move((Position)posMgr->getTilePosition(POS::SECOND_CENTER));
				}				
			}
		}
		else{
			if (getDistance(next->getTilePosition(), cnter->getTilePosition()) < 5){
				if (cnter->self->isIdle()){
					flags[FLAG::EXPAND_CMD] = true;
					info->map->addMyTerritory(new Territory(next, cnter));

					return;
				}
			}
			else{
				if (cnter->self->isIdle()){
					cnter->self->lift();
				}				
			}
		}
	}	

	/*
	if (flags[EXNAPD_FRONT_MORE]){
		return;
	}

	auto close = state->getCloseEnemy();
	auto next = info->map->getCandidateLocation();
	auto cnters = info->self->getBuildings(UnitTypes::Terran_Command_Center);
	auto secondCnter = cnters.back();

	if (!flags[EXNAPD_FRONT_MORE] && flags[FLAG::EXPAND_FRONT]){
		if (state->getCloseEnemy().size() < 2){
			auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();			
			brk->targetPos = posMgr->getTilePosition(POS::FRONT_BARRACK);
			moveBuildings.push_back(brk);
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::FRONT_SUPPLY)), Scheduler::BUILD_SPEED::FAST);
		
			info->map->addMyTerritory(new Territory(next, secondCnter));
			flags[EXNAPD_FRONT_MORE] = true;
		}

		return;
	}		

	

	if (secondCnter->self->isLifted()){
		if (close.size() - state->getCloseCount(UnitTypes::Protoss_Probe) == 0 || 3 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) || 1 <= info->self->getBuildingCount(UnitTypes::Terran_Missile_Turret)){
			// SAFE
			if (5 < getDistance(secondCnter->getTilePosition(), next->getTilePosition())){
				secondCnter->self->move(next->getPosition());
			}
			else{
				// more code
				secondCnter->self->land(next->getTilePosition());
			}
		}
		else{
			if (4 < close.size() - state->getCloseCount(UnitTypes::Protoss_Probe) || 2 < state->getCloseCount(UnitTypes::Protoss_Dragoon)){
				if (getProgress(secondCnter->self) < 90){
					secondCnter->self->move((Position)posMgr->getTilePosition(POS::SECOND_CENTER));
				}
				else{
					secondCnter->self->move(next->getPosition());
				}
			}
			else{
				secondCnter->self->move(next->getPosition());
			}
		}
	}
	else{
		if (getDistance(secondCnter->getTilePosition(), next->getTilePosition()) < 5){
			if (secondCnter->self->isIdle()){
				flags[FLAG::EXPAND_FRONT] = true;
				return;
			}
		}
		else{
			secondCnter->self->lift();
		}
	}
	*/
}

void StudyBuild::scvCreate(){

	auto startTerritory = info->map->getMyTerritorys().front();

	// 부족한 SCV를 뽑는다.
	if (flags[FLAG::EXPAND_CMD]){
		for (auto territory : info->map->getMyTerritorys()){

			int require = territory->getRequireSCVCount() - 1;
			
			if (territory->isRequireScv() && scheduler->canAddUnitType(UnitTypes::Terran_SCV)){
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV), Scheduler::BUILD_SPEED::NORMAL);
			}
		}
	}
	else{
		if (!flags[SCV_INSERT] && 10 == info->self->getUnitCount(UnitTypes::Terran_SCV)){
			if (startTerritory->getCmdCenter()->self->train(UnitTypes::Terran_SCV))
				flags[SCV_INSERT] = true;
		}

		if (20 < info->self->getUnitCount(UnitTypes::Terran_SCV, true)){
			if (200 < scheduler->getMineral() && scheduler->canAddUnitType(UnitTypes::Terran_SCV) && 0 < info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 1, startTerritory->getCmdCenter()), Scheduler::BUILD_SPEED::NORMAL);
			}
		}
		else{
			if (scheduler->canAddUnitType(UnitTypes::Terran_SCV)){
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 1, startTerritory->getCmdCenter()), Scheduler::BUILD_SPEED::NORMAL);
			}
		}		
	}
}

void StudyBuild::runBuildWork(){

	auto startTerritory = info->map->getMyTerritorys().front();

	// 저장된 빌드들을 수행.
	for (auto iter = builds.begin(); iter != builds.end(); ++iter){
		BuildWork& build = (*iter);

		if (!build.getFire() && build.run()){
			if (performer == nullptr){
				performer = info->map->getMyTerritorys()[0]->getWorker();
				build.getWork()->setPerformer(performer);
				startTerritory->getCmdCenter()->self->train(UnitTypes::Terran_SCV);
			}
			else{
				if (build.getWork()->type == UnitTypes::Terran_Barracks){
					build.getWork()->setPerformer(performer);
				}
				else if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_SUPPLY2)){
					performer2 = info->map->getMyTerritorys()[0]->getWorker();
					build.getWork()->setPerformer(performer2);
				}
			}

			scheduler->addSchedule(build.getWork());
			build.onFire();
		}
	}
}

void StudyBuild::move(){
	
	for (auto iter = moveBuildings.begin(); iter != moveBuildings.end(); iter++){

		auto unit = (*iter);

		if (unit->self->isLifted()){
			if (5 < getDistance(unit->getTilePosition(), unit->targetPos)){
				unit->self->move((Position)unit->targetPos); 
			}
			else{
				if (unit->self->isIdle())
					unit->self->land(unit->targetPos);
			}
		}
		else{
			if (getDistance(unit->getTilePosition(), unit->targetPos) < 5){
				if (unit->self->isIdle()){
					iter = moveBuildings.erase(iter);
					iter--;
				}
			}
			else{
				unit->self->lift();
			}
		}
	}
}