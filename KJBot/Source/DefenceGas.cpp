#include "DefenceGas.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

DefenceGas::DefenceGas(DEFENSE_TYPE type) : DefenceEvent(type)
{	
	target = nullptr;
}

DefenceGas::~DefenceGas()
{

}

void DefenceGas::chkFire(){

	/*
	if (info->self->getUnitCount(UnitTypes::Terran_Refinery, true) == 0){
		fire = true;
	}
	*/

	auto count = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Assimilator);
	if (0 < count){
		fire = true;
	}
}

void DefenceGas::update(){

	static bool flag = false;

	if (target == nullptr){
		for (auto unit : info->map->getMyTerritorys()[0]->getCloseEnemy()){
			if (unit->getType() == UnitTypes::Protoss_Assimilator){
				target = unit;
				break;
			}	
		}
	}

	if (target != nullptr){
		if (!target->exists()){
			scheduler->deleteUsedMineral(UnitTypes::Terran_Refinery);
			target = nullptr;
			flag = false;
		}
		else{
			auto startTerritory = info->map->getMyTerritorys().front();
			auto gasPos = posMgr->getGasPosition(startTerritory);
			scheduler->deleteSchedule(gasPos);

			getUnits(UnitTypes::Terran_Marine);

			if (target->isCompleted()){
				getUnits(UnitTypes::Terran_SCV, 3);
				/*
				if (scheduler->getPerformerInSchedule(posMgr->getGasPosition(info->map->getMyTerritorys().front())) != nullptr){
					getUnits(UnitTypes::Terran_SCV, 3);
				}
				else{
					getUnits(UnitTypes::Terran_SCV, 4);
				}
				*/
				
				if (getProgress(target) < 30 && !flag){
					scheduler->updateUsedMineral(UnitTypes::Terran_Refinery);
					flag = true;
				}
			}
			else{
				if (scheduler->getPerformerInSchedule(posMgr->getGasPosition(info->map->getMyTerritorys().front())) != nullptr){
					
				}
				else{
					getUnits(UnitTypes::Terran_SCV, 1);
				}				
			}
		}
	}

	if (target == nullptr){
		/*
		if (info->self->getBuildingCount(UnitTypes::Terran_Refinery, true) == 0 && scheduler->canAddUnitType(UnitTypes::Terran_Refinery) && 
			scheduler->getPerformerInSchedule(posMgr->getGasPosition(info->map->getMyTerritorys().front())) != nullptr){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Refinery, 1, defenceUnits[UnitTypes::Terran_SCV].front()), Scheduler::BUILD_SPEED::FAST);
		}
		*/

		eraseUnits(UnitTypes::Terran_SCV);
		eraseUnits(UnitTypes::Terran_Marine);
	}
}

void DefenceGas::run()
{	
	chkFire();
	turnOff();
	if (!fire){
		return;
	}	

	update();
	act();
}

void DefenceGas::act(){
	
	if (target == nullptr){
		return;
	}

	for (auto iter : defenceUnits){
		UnitType type = iter.first;
		
		for (auto unit : defenceUnits[type]){

			if (!unit->self->exists() || !unit->self->isCompleted()){
				continue;
			}

			if(!unit->self->isAttackFrame())
				unit->self->attack(target);
		}
	}
}

void DefenceGas::turnOff(){

	auto assCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Assimilator);
	auto count = info->self->getBuildingCount(UnitTypes::Terran_Refinery, true);

	if (assCount == 0){
		eraseUnits(UnitTypes::Terran_Marine);
		eraseUnits(UnitTypes::Terran_SCV);
	}

	if (0 < count){
		eraseUnits(UnitTypes::Terran_Marine);
		eraseUnits(UnitTypes::Terran_SCV);

		target = nullptr;
		fire = false;
	}	
}