#include "StateMgr.h"
#include "Instance.h"
#include "Utility.h"

StateMgr::StateMgr(){
	enemyBuild = EXPECT_BUILD::DEFAULT;
};

StateMgr::~StateMgr(){

};

StateMgr* StateMgr::getInstance(){

	static StateMgr instance;
	return &instance;
}

void StateMgr::init(){

}

void StateMgr::show(){

	switch (enemyBuild){
	case EXPECT_BUILD::DEFAULT:
		Broodwar->drawTextScreen(50, 100, "DEFAULT");
		break;

	case EXPECT_BUILD::RAW_NEXUS:
		Broodwar->drawTextScreen(50, 100, "RAW_NEXUS");
		break;

	case EXPECT_BUILD::TWOGATE:
		Broodwar->drawTextScreen(50, 100, "TWOGATE");
		break;

	case EXPECT_BUILD::ONEGATECORE:
		Broodwar->drawTextScreen(50, 100, "ONEGATECORE");
		break;

	case EXPECT_BUILD::ONEGATECORE_GATE:
		Broodwar->drawTextScreen(50, 100, "ONEGATECORE_GATE");
		break;

	case EXPECT_BUILD::ONEGATECORE_DOUBLE:
		Broodwar->drawTextScreen(50, 100, "ONEGATECORE_DOUBLE");
		break;

	case EXPECT_BUILD::ONEGATECORE_DARK:
		Broodwar->drawTextScreen(50, 100, "ONEGATECORE_DARK");
		break;

	case EXPECT_BUILD::ONEGATECORE_REAVER:
		Broodwar->drawTextScreen(50, 100, "ONEGATECORE_REAVER,");
		break;

	case EXPECT_BUILD::NONE:
		Broodwar->drawTextScreen(50, 100, "NONE");
		break;

	case EXPECT_BUILD::MORE_TURRET:
		Broodwar->drawTextScreen(50, 100, "MORE_TURRET");
		break;

	case EXPECT_BUILD::TWOGATE_GATE:
		Broodwar->drawTextScreen(50, 100, "TWOGATE_GATE");
		break;

	case EXPECT_BUILD::FRONTGATE:
		Broodwar->drawTextScreen(50, 100, "FRONTGATE");
		break;
	}
}

void StateMgr::draw(){

}

void StateMgr::run(){
	updateEnemyBuildByScout();
}

void StateMgr::updateEnemyBuildByScout(){
	if (scouting->isFind()){ 
		//bw << checkFrontNexusTime() << endl;
		if (2 == info->enemy->getBuildingCount(UnitTypes::Protoss_Nexus) || scouting->getcheckFrontNexus()){ //더블넥서스         //여기서 조건문을 앞마당 건물로 파악을 하는게 좋을듯
			
			if (enemyBuild == EXPECT_BUILD::ONEGATECORE){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_DOUBLE;

			}
			
			if (Scout::getInstance()->getSearchCount() == 1){ 
				if (enemyBuild == EXPECT_BUILD::DEFAULT){
					enemyBuild = EXPECT_BUILD::RAW_NEXUS;
				}
			}
			else if (Scout::getInstance()->getSearchCount() == 2){
				if (checkFrontNexusTime() > 60){
					if (enemyBuild == EXPECT_BUILD::DEFAULT){
						enemyBuild = EXPECT_BUILD::RAW_NEXUS;
					}
				}
				else{
					if (enemyBuild == EXPECT_BUILD::DEFAULT){
						enemyBuild = EXPECT_BUILD::ONEGATECORE_DOUBLE;
					}
				}
			}
			else if (Scout::getInstance()->getSearchCount() == 3){
				if (checkFrontNexusTime() > 80){
					if (enemyBuild == EXPECT_BUILD::DEFAULT){
						enemyBuild = EXPECT_BUILD::RAW_NEXUS;
					}
				}
				else{
					if (enemyBuild == EXPECT_BUILD::DEFAULT){
						enemyBuild = EXPECT_BUILD::ONEGATECORE_DOUBLE;
					}
				}
			}

		}

		if (1 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Cybernetics_Core)){
			//코어발견
			if (enemyBuild == EXPECT_BUILD::DEFAULT){
				enemyBuild = EXPECT_BUILD::ONEGATECORE;

			}
			if (enemyBuild == EXPECT_BUILD::TWOGATE){
				enemyBuild = EXPECT_BUILD::TWOGATE_CORE;

			}
		}

		if (2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway)){
			//투게이트발견
			if (enemyBuild == EXPECT_BUILD::DEFAULT){
				enemyBuild = EXPECT_BUILD::TWOGATE;

			}
			else if (enemyBuild == EXPECT_BUILD::ONEGATECORE){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_GATE;

			}
			if (2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway)){
				if (enemyBuild == EXPECT_BUILD::TWOGATE){
					enemyBuild = EXPECT_BUILD::TWOGATE_GATE;
				}
			}
		}

		if (0 < info->enemy->getBuildingCount(UnitTypes::Protoss_Citadel_of_Adun) || 0 < info->enemy->getBuildingCount(UnitTypes::Protoss_Templar_Archives) || 0 < info->enemy->getUnitCount(UnitTypes::Protoss_Dark_Templar)){
			//다크관련발견
			if (enemyBuild == EXPECT_BUILD::DEFAULT){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_DARK;

			}
			else if (enemyBuild == EXPECT_BUILD::ONEGATECORE){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_DARK;

			}
			else if (enemyBuild = EXPECT_BUILD::MORE_TURRET){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_DARK;
			}
			else if (enemyBuild == EXPECT_BUILD::ONEGATECORE_GATE){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_DARK;
			}
		}

		if (0 < info->enemy->getBuildingCount(UnitTypes::Protoss_Robotics_Facility) || 0 < info->enemy->getBuildingCount(UnitTypes::Protoss_Robotics_Support_Bay) || 0 < info->enemy->getUnitCount(UnitTypes::Protoss_Reaver)){
			//리버관련발견
			if (enemyBuild == EXPECT_BUILD::DEFAULT){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_REAVER;

			}
			else if (enemyBuild == EXPECT_BUILD::ONEGATECORE){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_REAVER;
			}
			else if (enemyBuild == EXPECT_BUILD::ONEGATECORE_GATE){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_REAVER;
			}
			else if (enemyBuild = EXPECT_BUILD::MORE_TURRET){
				enemyBuild = EXPECT_BUILD::ONEGATECORE_REAVER;
			}
		}

		if (scouting->checkEnemyRotate()){  //본진한바퀴돌고 판단
			if (0 == info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway)){
				if (enemyBuild == EXPECT_BUILD::DEFAULT){
					enemyBuild = EXPECT_BUILD::NONE;

				}
			}
			if (0 < info->enemy->getUnitCount(UnitTypes::Protoss_Zealot)){
				if (enemyBuild == EXPECT_BUILD::NONE){
					enemyBuild = EXPECT_BUILD::FRONTGATE;
				}
			}	
		}


		if (scouting->checkEnterBlock()){
			//입구막혔을때
			if (enemyBuild == EXPECT_BUILD::DEFAULT){
				enemyBuild = EXPECT_BUILD::MORE_TURRET;
			}
			if (scouting->getcheckFrontNexus()){
				if (enemyBuild = EXPECT_BUILD::MORE_TURRET){
					enemyBuild = EXPECT_BUILD::ONEGATECORE_DOUBLE;
				}
			}
		}
	}
}

int StateMgr::checkFrontNexusTime(){

	auto enemychkPos = BWTA::getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter(); //적군 check포인트
	auto enemyfrontBase = BWTA::getNearestBaseLocation(enemychkPos);
	
	for (auto unit : info->enemy->getBuildings(UnitTypes::Protoss_Nexus)){
		if (unit->getTilePosition() == enemyfrontBase->getTilePosition()){
			return unit->getHitPoints() *100 / unit->getType().maxHitPoints();
		}
	}
	return 0;
}


