#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include "MyUnit.h"
#include "UnitControl.h"
#define MINE_NONE 0
#define MINE_READY 1
#define MINE_DONE 2


using namespace std;
using namespace BWAPI;

class CombatControl
{
private:
	CombatControl();
	~CombatControl();
	static CombatControl *s_CombatControl;
	bool fighting;
	bool forward;
	int gameFrame;

	bool tHillGo;

public:
	static CombatControl * create();
	static CombatControl * getInstance();
	void init();
	void onFrame(TeamControl *t);
	void draw(TeamControl* t);
	void update();

	void onUnitCreate(Unit);
	void onUnitDestroy(Unit);
	void onUnitComplete(Unit);

	void onSTRONG_ATTACK(TeamControl* t);
	void onCRZAY(TeamControl* t);
	void onATTACK(TeamControl* t);
	void onTIGHTING(TeamControl* t);
	void onDEFENSE(TeamControl* t);
	void onSQUEEZE(TeamControl* t);

	void onSTART(TeamControl* t);
	void onCLOSED(TeamControl* t);
	void onFRONT(TeamControl* t);
	void onGOING(TeamControl* t);
	void onTIGHT(TeamControl* t);
	void onJOIN(TeamControl* t);
	void onBJ(TeamControl* t);
	void onFINISH(TeamControl* t);
	void onSAMPLE(TeamControl* t);
	void onSAMPLE2(TeamControl* t);
	void onUNDUK(TeamControl* t);

	vector<std::pair<Position, std::pair<float, int>>> minePos;
	vector<std::pair<Position, std::pair<float, int>>> turretPos;
	void setMinePos();

	void updateMinePosValue();
	vector<Position> getMinePos(int count);
	void setUseMineState(Position p, int state);

	float p1, p2, p3;

	int totalTeamIndex = 0;

	vector<Position> getVGPosition(Position tPos, Position ePos, int vgNum, int tR, float tankRange, int dist);

	Unit getTargetUnit(MyUnit* unit, Unitset enemyUnits);
	Unit getHealUnit(MyUnit* unit, TeamControl* t);
	Unit getRepairUnit(MyUnit* unit);
	Unit getTargetBuilding(MyUnit* unit);
	float getTargetUnitValue(UnitType my, UnitType enemy);
	Unitset getNearEnemyUnits(TeamControl* t);
	Unitset getNearEnemyBuildings(MyUnit* t);
	bool isOnlyZealot(TeamControl* t);

	Unit getMassedUnit(MyUnit* unit, Unitset enemies);
	vector<std::pair<Position, float>> getEMPPositions(TeamControl* t, int vCount, Unitset enemies);
	bool isNearChokePoint(MyUnit *);
	bool isAlone(MyUnit* unit, TeamControl* t);
	Position getNearestEnemyGroup(MyUnit*);
	Position getNearestMarinePos(MyUnit*, TeamControl * t);
	MyUnit* getFrontTank(TeamControl* t, Position target);
	MyUnit* getFrontMarine(TeamControl* t, Position target);

	void kitingMove(MyUnit*, Unit target, Position targetPos);

	TilePosition DefensePos[3];
	TilePosition firstGoPos;
	TilePosition tightPos;
	vector<TilePosition> defenseTankPos;
	TilePosition bangMission;
	bool defenseSetting = false;
	void settingDefenseObject();

	bool isEnemyUnitComeTrue(MyUnit*);
	void joiningTeam(MyUnit*, TeamControl* t);
	void setHelpMe(MyUnit*);

	vector<TilePosition> tightTankPos;
	vector<TilePosition> tightBunkerPos;
	TilePosition tightWaitBionicPos;
	TilePosition waitTankPos;
	TilePosition middlePos;
	TilePosition middleGoPos;
	TilePosition undunkTankPos;

	TilePosition threeDragonPos;

	map<TilePosition, int> tightValue;
	map<TilePosition, int> tightReady;

	TilePosition getTightTankPosition();
	TilePosition getTightBunkerPosition();
	TilePosition unduckWiiPos[4];

	bool isCanBuySteamPack(MyUnit*);
	float getNearestTankDistance(MyUnit*);
	float getNearestChokePointDistance(MyUnit*);

	vector<Position> marinePosition;
	vector<bool> MarineInPosition;
	MyUnit* repairscv;
	MyUnit* repairscv2;

	MyUnit* repairScvs[2];

	bool EnemyOnce;
	void setMarinePos();
	void marineHold(MyUnit *unit);
	void marineHold1(TeamControl *t);

	void setMarineHoldPosition(TeamControl *t);
	bool CheckEnemyOnce();

	// bang // 
	void bang(TeamControl* t);
	void bang2(TeamControl* t);
	void bang3(TeamControl* t);

	void close(TeamControl* t);

	bool canGoHill(BWTA::BaseLocation* base, TeamControl* t);
	void bang4(TeamControl* t); // �����ڵ�.
	void bangWait(TeamControl* t);

	void attackIgnorantly(TeamControl* t);

	bool isSiegeTiming(MyUnit* unit, TeamControl* t);
	MyUnit* getMyNearUnit(vector<MyUnit*>& units, MyUnit* unit, bool canAttack = true);
	Unit getNearEnemy(vector<Unit> units, Position pos);
	Unit getNearEnemy(Unitset units, Position pos);
	//vector<Unit> getCanAttackEnemy(MyUnit* unit);
	vector<Unit> getCanAttackEnemy(MyUnit* unit, int range = 0);
	vector<Unit> getAllCanAttackEnemy(vector<MyUnit*>& units, int range = 0);
	vector<Unit> getAllCantEnemy(vector<MyUnit*>& units);
	Unit getMassedUnit(MyUnit* unit, vector<Unit>& enemies);
	Unit getTargetUnit(MyUnit* unit, vector<Unit>& enemies);
	bool canAttack(MyUnit* unit);
	bool canStimPack(TeamControl *t, MyUnit* unit);

	vector<MyUnit*> unduckTanks;

	bool enemyfront = false;
	bool enemyBase = false;
	bool isEnemyFront();

	bool isUnduck(TeamControl* t);
	bool unduck = false;
	bool unduckSetting = false;
	bool unduckWait = false;

	BWTA::Chokepoint* enemyChoke = NULL;
	bool needComsat = false;
	bool waitSet = false;
	Position needComsatPos;

	Position getAvoidStormPos(MyUnit* unit, TeamControl* t);

	bool isClearing = false;
	bool reaver = false;
};
