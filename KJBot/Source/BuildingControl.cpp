#include "BuildingControl.h"
#include "Utility.h"

BuildingControl::BuildingControl(){

}

BuildingControl::~BuildingControl(){

}

BuildingControl* BuildingControl::getInstance() {

	static BuildingControl instance;
	return &instance;
}

void BuildingControl::run(MyUnit* unit){

	auto self = unit->self;

	switch (unit->state){
		case MyUnit::STATE::MOVE_LAND:

		if (!self->isLifted()){
			if (self->canLift()){
				self->lift();
			}
		}
		else{
			if (getDistance(self->getTilePosition(), unit->targetPos) < 3){
				self->land(unit->targetPos);
			}
			else{
				self->move((Position)unit->targetPos);
			}
		}

		break;
	}		
}
