#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "DefenceEvent.h"

using namespace std;


class DefenceSearch : public DefenceEvent
{	
	
public:

	DefenceSearch(DEFENSE_TYPE type);
	~DefenceSearch();

	void chkFire();
	void turnOff();

	void update();
	void run();

	void act();
};

