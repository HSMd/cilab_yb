#include "Work.h"
#include "Instance.h"

bool BuildWork::run(){

	int cmp = 0;

	if (comparison.isBuilding()){
		if (immediatly){
			cmp = info->self->getImmediatelyBuilding(comparison).size();
		}
		else{
			cmp = info->self->getBuildings(comparison).size();
		}
	}
	else{
		if (immediatly){
			cmp = info->self->getImmediatelyUnit(comparison).size();
			if (comparison == UnitTypes::Terran_Siege_Tank_Tank_Mode){
				cmp += info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode);
			}
		}
		else{
			cmp = info->self->getUnits(comparison).size();
			if (comparison == UnitTypes::Terran_Siege_Tank_Tank_Mode){
				cmp += info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode);
			}
		}
	}

	return cmpCount <= cmp;
}