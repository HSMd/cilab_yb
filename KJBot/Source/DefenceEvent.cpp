#include "DefenceEvent.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

DefenceEvent::DefenceEvent(DEFENSE_TYPE type)
{	
	eventType = type;
	fire = false;
}

DefenceEvent::~DefenceEvent()
{

}

void DefenceEvent::show()
{

}

void DefenceEvent::init()
{

}

void DefenceEvent::getUnits(UnitType type, int count){

	auto unitController = UnitControl::getInstance()->getInstance();

	for (auto iter = defenceUnits[type].begin(); iter != defenceUnits[type].end(); ++iter){
		auto unit = (*iter);
		if (!unit->self->exists()){
			iter = defenceUnits[type].erase(iter);
			iter--;
			continue;
		}
	}

	if (count < 1){
		return;
	}
	/*
	int over = (defenceUnits[type].size() - count) - 1;
	if (0 < over){
		if (type == UnitTypes::Terran_SCV){
			for (int i = 0; i < over; i++){
				defenceUnits[type].back()->state = MyUnit::STATE::MINERAL;
				defenceUnits[type].pop_back();
			}
		}
		else{
			for (int i = 0; i < over; i++){
				defenceUnits[type].back()->state = MyUnit::STATE::IDLE;
				UnitControl::getInstance()->returnUnit(defenceUnits[type].back());
				defenceUnits[type].pop_back();
			}
		}
	}
	*/

	while (true){
		if (count <= defenceUnits[type].size()){
			return;
		}

		MyUnit* unit = nullptr;
		if (type == UnitTypes::Terran_SCV){
			// ����
			unit = info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BARRACK))->getWorker();
			if (unit == nullptr){
				unit = info->map->getMyTerritorys()[0]->getWorker();
			}
		}
		else{
			unit = unitController->getUnit(type);
		}

		if (unit == nullptr){
			return;
		}

		defenceUnits[type].push_back(unit);
	}
}

void DefenceEvent::eraseUnits(UnitType type){

	auto& units = defenceUnits[type];

	for (auto unit : units){
		if (type == UnitTypes::Terran_SCV){
			unit->state = MyUnit::STATE::IDLE;
		}
		else{
			unit->state = MyUnit::STATE::IDLE;
			UnitControl::getInstance()->returnUnit(unit);
		}		
	}

	units.clear();
}

int DefenceEvent::getCloseCount(UnitType type){

	return info->map->getCloseCount(type);
}
