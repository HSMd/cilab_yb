#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "DefenceEvent.h"

using namespace std;


class DefencePylon : public DefenceEvent
{	
	
public:

	DefencePylon(DEFENSE_TYPE type);
	~DefencePylon();

	void chkFire();
	void turnOff();

	void update();
	void run();

	void act();
};

