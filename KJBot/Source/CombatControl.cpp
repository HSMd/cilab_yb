#include "CombatControl.h"
#include "Information.h"
#include "Instance.h"
#include "Self.h"
#include "Utility.h"
#include "math.h"

CombatControl* CombatControl::s_CombatControl;
CombatControl::CombatControl() {
	init();

	tHillGo = false;

	for (auto scv : repairScvs){
		scv = nullptr;
	}
}

CombatControl::~CombatControl() {

}

CombatControl* CombatControl::create() {
	static CombatControl instance;
	return &instance;
}

CombatControl* CombatControl::getInstance() {
	static CombatControl instance;
	return &instance;
}

void CombatControl::init() {
	p1 = 5.0f;
	p2 = 7.0f;
	p3 = 4.0f;

	tightPos = TilePosition(0, 0);
	//setMinePos();
	setMarinePos();

}

void CombatControl::onFrame(TeamControl* t) {
	gameFrame = BWAPI::Broodwar->getFrameCount();
	//updateMinePosValue();
	if (!defenseSetting) {
		settingDefenseObject();
	}
	if ((*t).state == TeamControl::STATE::BCN_START) {
		// 시작 후, 벙커 지어지기 전
		setMarineHoldPosition(t);
		onSTART(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_CLOSED) {
		// 벙커 지어진 후, 시즈 탱크가 나오기전
		setMarineHoldPosition(t);
		onCLOSED(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_FRONT) {
		// 탱크가 나온 후, 진출 전
		onFRONT(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_GOING) {
		// 진출 후, 앞마당 까지 도달하기 전
		//onGOING(t);
		//onSAMPLE(t);
		bang3(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_TIGHT) {
		// 앞마당 도달 후, 조이기
		onTIGHT(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_JOIN) {
		// 후속 병력 합류
		//onJOIN(t);
		onSAMPLE2(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_BJ) {
		// 멀티                                    견제
		onBJ(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_FINISH) {
		// 마무으리
		//onFINISH(t);
		//bang(t);
		/*
		if(gameFrame %6==0)
		bang2(t);
		*/

		bang4(t);

		/*
		static bool canGo = false;
		if (!canGo){
			canGo = canGoHill(info->map->getEnemyStartBase(), t);
		}

		if (!canGo){
			bang4(t);
		}
		else{
			if (10 <= t->marineSet.size() && 6 <= t->tankSet.size()){
				tHillGo = true;
				bang4(t);
			}
			else{
				bangWait(t);
			}
		}		
		*/

	}
}

////////////////////////////////
// BANG //
////////////////////////////////

MyUnit* CombatControl::getMyNearUnit(vector<MyUnit*>& units, MyUnit* unit, bool canAttack){
	
	MyUnit* re = nullptr;
	int minDis = 99999;

	for (auto u : units){
		if (canAttack && u->self->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode && u->self->getUnitsInRadius(u->self->getType().groundWeapon().maxRange(), Filter::IsEnemy).size() == 0){
			continue;
		}

		int dis = getDistance(unit->getTilePosition(), u->getTilePosition());
		if (dis < minDis){
			minDis = dis;
			re = u;
		}
	}

	return re;
}

Unit CombatControl::getNearEnemy(vector<Unit> units, Position pos){

	Unit re = nullptr;
	int minDis = 99999;

	for (auto u : units){
		
		if (u->getType() == UnitTypes::Protoss_Observer){
			continue;
		}

		int dis = getDistance(pos, u->getPosition());
		if (dis < minDis){
			minDis = dis;
			re = u;
		}
	}

	return re;
}

Unit CombatControl::getNearEnemy(Unitset units, Position pos){

	Unit re = nullptr;
	int minDis = 99999;

	for (auto u : units){

		if (u->getType() == UnitTypes::Protoss_Observer){
			continue;
		}

		int dis = getDistance(pos, u->getPosition());
		if (dis < minDis){
			minDis = dis;
			re = u;
		}
	}

	return re;
}
/*
vector<Unit> CombatControl::getCanAttackEnemy(MyUnit* unit){

	Unitset closeEnemy = unit->self->getUnitsInRadius(unit->self->getType().groundWeapon().maxRange(), Filter::IsEnemy);
	Unitset cantAttack = unit->self->getUnitsInRadius(unit->self->getType().groundWeapon().minRange(), Filter::IsEnemy);

	vector<Unit> canAttackEnemy;

	for (auto close : closeEnemy){

		bool can = true;
		for (auto no : cantAttack){
			if (no == close){
				can = false;
				break;
			}
		}

		if (!can){
			continue;
		}

		canAttackEnemy.push_back(close);
	}

	return canAttackEnemy;
}
*/
vector<Unit> CombatControl::getCanAttackEnemy(MyUnit* unit, int range){

	if (range == 0){
		range = unit->self->getType().groundWeapon().maxRange();
	}

	Unitset closeEnemy = unit->self->getUnitsInRadius(range, Filter::IsEnemy);
	Unitset cantAttack = unit->self->getUnitsInRadius(unit->self->getType().groundWeapon().minRange(), Filter::IsEnemy);

	vector<Unit> canAttackEnemy;

	for (auto close : closeEnemy){

		if (unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode){
			if (close->getType().isFlyer()){
				continue;
			}
		}

		if (!close->isDetected()){
			continue;
		}

		bool can = true;
		for (auto no : cantAttack){
			if (no == close){
				can = false;
				break;
			}
		}

		if (!can){
			continue;
		}

		canAttackEnemy.push_back(close);
	}

	return canAttackEnemy;
}

vector<Unit> CombatControl::getAllCantEnemy(vector<MyUnit*>& units){

	vector<Unit> cantAttackEnemy;

	for (auto unit : units){
		auto targets = unit->self->getUnitsInRadius(unit->self->getType().groundWeapon().minRange(), Filter::IsEnemy);

		for (auto target : targets){
			bool add = true;
			for (auto already : cantAttackEnemy){
				if (already == target){

					add = false;
					break;
				}
			}

			if (add){
				cantAttackEnemy.push_back(target);
			}
		}
	}

	return cantAttackEnemy;
}

vector<Unit> CombatControl::getAllCanAttackEnemy(vector<MyUnit*>& units, int range){

	vector<Unit> canAttackEnemy;

	for (auto unit : units){
		auto targets = getCanAttackEnemy(unit, range);
		
		bool add = true;
		for (auto target : targets){

			if (target->getType() == UnitTypes::Protoss_Observer){
				continue;
			}

			if ((unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) && target->getType().isFlyer()){
				continue;
			}

			for (auto already : canAttackEnemy){
				if (already == target){

					add = false;
					break;
				}
			}

			if (add){
				canAttackEnemy.push_back(target);
			}			
		}
	}

	return canAttackEnemy;
}

Unit CombatControl::getMassedUnit(MyUnit* unit, vector<Unit>& enemies) {
	// 밀집된 유닛 반환
	Unit ret = NULL;

	//시즈일떄
	if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		//시즈모드 일 때만
		float maxValue = -9999;

		for (auto e = enemies.begin(); e != enemies.end(); e++) {
			if ((*e)->isFlying()) continue;
			if ((*e)->getType() == UnitTypes::Unknown) continue;
			if ((*e)->getType() == UnitTypes::Protoss_High_Templar && (*e)->getEnergy() < 60) continue;
			//if ((*e)->getType() == UnitTypes::Protoss_Probe) continue;
			//공격할 수 있는 애들 중에

			int minR = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().minRange();
			int maxR = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange();
			if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
				maxR = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange();
			}
			float distance = getDistance((*e)->getPosition(), unit->getPosition());
			if (minR < distance && maxR > distance) {
				// 직접 타격하는 친구는 *8
				float value = getTargetUnitValue(unit->getType(), (*e)->getType()) * 8.0f;

				for (auto s = enemies.begin(); s != enemies.end(); s++) {
					float dist = getDistance((*e)->getPosition(), (*s)->getPosition());
					// 스플래시 해당 거리 따라 데미지에 맞춰 값 계산
					if (dist < 10) {
						value += getTargetUnitValue(UnitTypes::Terran_Siege_Tank_Siege_Mode, (*e)->getType()) * 4.0f;
					}
					else if (dist < 25) {
						value += getTargetUnitValue(UnitTypes::Terran_Siege_Tank_Siege_Mode, (*e)->getType()) * 2.0f;
					}
					else if (dist < 40) {
						value += getTargetUnitValue(UnitTypes::Terran_Siege_Tank_Siege_Mode, (*e)->getType()) * 1.0f;
					}
				}

				for (auto s : info->self->getAllUnits()) {
					float dist = getDistance((*e)->getPosition(), s->getPosition());
					// 스플래시 해당 거리 따라 데미지에 맞춰 값 계산
					if (dist < 10) {
						value -= 0.1f * 4.0f;
					}
					else if (dist < 25) {
						value -= 0.1f * 2.0f;
					}
					else if (dist < 40) {
						value -= 0.1f * 1.0f;
					}
				}
				if (maxValue < value) {
					ret = (*e);
					maxValue = value;
				}
			}
		}

		return ret;
	}

	return ret;
}

Unit CombatControl::getTargetUnit(MyUnit* unit, vector<Unit>& enemyUnits) {
		
	Unit ret = nullptr;
	float maxValue = -1;
	vector<Unit> candidateEnemys;

	if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		ret = getMassedUnit(unit, enemyUnits);
		return ret;
	}

	for (auto enemy : enemyUnits){
		if (getDistance(unit->getPosition(), enemy->getPosition()) < unit->getType().groundWeapon().maxRange() + 32 * 3){
			candidateEnemys.push_back(enemy);
		}
	}

	if (candidateEnemys.empty()){
		return getNearEnemy(enemyUnits, unit->getPosition());
	}
	else{
		for (auto enemy : candidateEnemys){
			auto value = getTargetUnitValue(unit->getType(), enemy->getType());
			if (maxValue < value){
				maxValue = value;
				ret = enemy;
			}
			else if(maxValue == value){
				if (enemy->getHitPoints() < ret->getHitPoints()){
					ret = enemy;
				}
			}
		}
	}

	return ret;
}

bool CombatControl::canAttack(MyUnit* unit){

	if ((!unit->self->isAttackFrame() && unit->self->isIdle()) || unit->self->isMoving() || unit->self->isHoldingPosition()){
		return true;
	}

	return false;
}

bool CombatControl::canStimPack(TeamControl *t, MyUnit* unit) {

	if (!unit->self->canUseTech(TechTypes::Stim_Packs) || unit->self->isStimmed() || getProgress(unit->self) < 50 || t->medicSet.size() * 4 < t->marineSet.size())
		return false;

	return true;
}

bool CombatControl::isSiegeTiming(MyUnit* unit, TeamControl* t){

	if (!unit->self->canSiege()){
		return false;
	}

	auto targets = getCanAttackEnemy(unit, unit->self->getType().groundWeapon().maxRange());
	int enemyUnitCount = 0;
	int enemyCount = 0;
	bool immediately = false;

	for (auto unit : bw->enemy()->getUnits()){
		if ((unit->getType() != UnitTypes::Protoss_Photon_Cannon && unit->getType().isBuilding()) || unit->getType() == UnitTypes::Protoss_Probe){
			continue;
		}

		if (unit->isVisible() && unit->isDetected() == false){
			enemyUnitCount++;
		}
	}

	for (auto unit : bw->enemy()->getUnits()){
		if (unit->isVisible() && unit->isDetected() == false){
			enemyCount++;;
		}
	}

	if (enemyUnitCount == 0){
		if (max(1, enemyCount / 4) <= targets.size()){
			return true;
		}
	}
	else{
		//if (t->tankSet.size() + t->marineSet.size() <= enemyUnitCount * 4){
		//	return true;
		//}		

		return true;
	}

	return false;
}

void CombatControl::bang(TeamControl* t){

	Unitset enemy = getNearEnemyUnits(t); // 우리팀과 적군 사거리 내의 모든 유닛.
	Position goPos = t->attackPosition;   // 진출 방향
	auto tankEnemys = getAllCanAttackEnemy(t->tankSet);
	auto tankCantEnemys = getAllCantEnemy(t->tankSet);
	auto marineEnemys = getAllCanAttackEnemy(t->marineSet);

	static int frameCount = 0;
	int unSigedSize = 0;
	int enemyUnitCount = 0;
	int maxDis = 210;
	static bool flag = true;
	
	for (auto unit : bw->enemy()->getUnits()){
		if ((unit->getType() != UnitTypes::Protoss_Photon_Cannon && unit->getType().isBuilding()) || unit->getType() == UnitTypes::Protoss_Probe){
			continue;
		}

		if (unit->isVisible()){
			enemyUnitCount++;
		}		
	}

	bw << tankEnemys.size() << endl;

	if (enemyUnitCount < 3){
		//maxDis = 300;
	}

	// 시즈인애랑 아닌애 구분.
	for (auto tmp : t->tankSet){
		if (!tmp->self->isSieged()){
			unSigedSize++;
		}
	}

	// TankLogic 모두 균등하게 있을 경우.
	if (tankEnemys.empty()){
		if (frameCount + 24 * 2 < bw->getFrameCount()){
			
			int siegeCount = 0;
			for (auto tank : t->tankSet){
				if (tank->self->isSieged()){
					tank->self->unsiege();
				}
				else{
					siegeCount++;
				}
			}

			if (siegeCount == t->tankSet.size()){

				flag = true;

				for (auto tank : t->tankSet){
					tank->self->move(goPos);
				}
			}
		}
	}
	else{
		frameCount = bw->getFrameCount();

		for (auto tank : t->tankSet) {
			vector<Unit> canAttackEnemys = getCanAttackEnemy(tank);
			Unitset notAttackEnemys = tank->self->getUnitsInRadius(tank->self->getType().groundWeapon().minRange(), Filter::IsEnemy);

			if (tank->self->isSieged()){
				if (t->marineSet.size() + unSigedSize < notAttackEnemys.size() * 1.5){
					tank->self->unsiege();
				}
				else{
					if (canAttackEnemys.size() == 0 && tank->maginNumber + (24 * 2) < bw->getFrameCount()){
						tank->self->unsiege();
					}
					else{
						if (canAttack(tank)){
							Unit target = getTargetUnit(tank, canAttackEnemys);
							tank->self->attack(target);
						}
					}
				}
			}
			else{
				if (t->marineSet.size() + unSigedSize < notAttackEnemys.size() * 1.5){
					if (canAttack(tank)){
						Unit target = getTargetUnit(tank, canAttackEnemys);
						tank->self->attack(target);
					}
					else{
						tank->self->rightClick(info->map->getMyStartBase()->getPosition());
					}
				}
				else{
					if (canAttackEnemys.size() == 0){
						tank->self->move(goPos);
					}
					else{
						auto chkPoint = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter();
						if (getDistance(chkPoint, tank->self->getPosition()) < 150){
							tank->self->move(goPos);
						}
						else{
							if (isSiegeTiming(tank, t)){
								tank->self->siege();

								flag = false;
								tank->maginNumber = bw->getFrameCount();
							}
						}						
					}
				}
			}
		}
	}

	BWAPI::Broodwar->drawCircleMap(t->tankCenterPos, 3 * TILEPOSITION_SCALE, Colors::Grey, true);

	if (5 < enemyUnitCount && flag == true){
		maxDis = 210;
	}

	// 마린 코드
	for (auto marine : t->marineSet){
		if (enemyUnitCount < 1){

			if (tankEnemys.size() == 0){
				auto target = getTargetUnit(marine, tankEnemys);

				if (canAttack(marine)){
					marine->self->attack(target);
				}
			}
			else{
				auto target = marine->self->getClosestUnit(Filter::IsEnemy, 32 * 20);
				if (target != nullptr){
					if (canAttack(marine)){

						marine->self->attack(target);
					}
				}
				else{
					marine->self->move(goPos);
				}
			}			
		}
		else{
			auto attackEnemy = getCanAttackEnemy(marine);
			auto target = getTargetUnit(marine, marineEnemys);

			if (0 < tankCantEnemys.size()){

				int minDis = 9999999;

				for (auto unit : tankCantEnemys){
					int dis = getDistance(unit->getTilePosition(), marine->getTilePosition());
					if (dis < minDis){
						minDis = dis;
						target = unit;
					}
				}

				if (canAttack(marine)){
					marine->self->attack(target);
				}

				return;
			}

			if (getDistance(t->tankCenterPos, marine->getPosition()) < maxDis - 40){
				if (target == nullptr){
					target = getTargetUnit(marine, tankEnemys);
					if (target == nullptr){
						marine->self->move(goPos);
					}
					else{
						if (canStimPack(t, marine)){
							marine->self->useTech(TechTypes::Stim_Packs);
						}

						if (canAttack(marine)){
							marine->self->attack(target);
						}						
					}					
				}
				else{
					if (canStimPack(t, marine)){
						marine->self->useTech(TechTypes::Stim_Packs);
					}

					if (canAttack(marine)){
						marine->self->attack(target);
					}
				}
			}
			else if (getDistance(t->tankCenterPos, marine->getPosition()) < maxDis){
				if (target == nullptr){
					target = getTargetUnit(marine, tankEnemys);
					if (target == nullptr){
						if (!marine->self->isHoldingPosition())
							marine->self->holdPosition();
					}
					else{
						if (canStimPack(t, marine)){
							marine->self->useTech(TechTypes::Stim_Packs);
						}

						if (canAttack(marine)){
							marine->self->attack(target);
						}
					}					
				}
				else{
					if (canStimPack(t, marine)){
						marine->self->useTech(TechTypes::Stim_Packs);
					}

					if (canAttack(marine)){
						marine->self->attack(target);
					}
				}
			}
			else{
				float mdist = getDistance(marine->getPosition(), t->attackPosition);
				float tdist = getDistance(t->tankCenterPos, t->attackPosition);

				if (mdist > tdist + 10) {
					marine->self->move(goPos);
				}
				else {
					if (canStimPack(t, marine)){
						marine->self->useTech(TechTypes::Stim_Packs);
					}

					if (!marine->self->isHoldingPosition())
						marine->self->holdPosition();
				}
			}
		}		
	}

	for (auto medic : t->medicSet) {

		Unit healTarget = getHealUnit(medic, t);

		if (healTarget == nullptr){
			if (getDistance(t->tankCenterPos, medic->getPosition()) < 190){
				medic->self->move(goPos);
			}
			else{
				float mdist = getDistance(medic->getPosition(), t->attackPosition);
				float tdist = getDistance(t->tankCenterPos, t->attackPosition);

				if (mdist > tdist + 10) {
					medic->self->move(t->tankCenterPos, false);
				}
				else {
					if (t->marineSet.size() == 0){
						medic->self->move(t->teamCenterPos);
					}
					else{
						auto near = getMyNearUnit(t->marineSet, medic, false);
						medic->self->move(near->getPosition());
					}
				}
			}
		}
		else{
			if (medic->self->canUseTech(TechTypes::Healing, healTarget)){
				medic->self->useTech(TechTypes::Healing, healTarget);
			}
			else{
				medic->self->move(healTarget->getPosition());
			}
		}
	}
}

void CombatControl::bang2(TeamControl* t){

	vector<MyUnit*> siegeMode;
	auto nearGroup = getNearestEnemyGroup(t->tankSet.front());

	Position goPos = t->attackPosition;   // 진출 방향
	auto tankEnemys = getAllCanAttackEnemy(t->tankSet);
	auto tankCantEnemys = getAllCantEnemy(t->tankSet);

	static int frameCount = 0;
	int unSigedSize = 0;
	static bool flag = true;
	int enemyUnitCount = 0;
	int enemyCount = 0;
	int maxRange = 220;

	if (isUnduck(t)) {
		onUNDUK(t);
		return;
	}

	for (auto unit : bw->enemy()->getUnits()){
		if ((unit->getType() != UnitTypes::Protoss_Photon_Cannon && unit->getType().isBuilding()) || unit->getType() == UnitTypes::Protoss_Probe){

			continue;
		}

		if (unit->isVisible()){
			enemyUnitCount++;
		}
	}

	for (auto unit : bw->enemy()->getUnits()){
		if (unit->isVisible()){
			enemyCount++;;
		}
	}

	// 시즈인애랑 아닌애 구분.
	for (auto tmp : t->tankSet){
		if (tmp->self->isSieged()){
			siegeMode.push_back(tmp);
		}
	}

	unSigedSize = t->tankSet.size() - siegeMode.size();
	int range = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() - 30;
	auto marineEnemy = getAllCanAttackEnemy(t->tankSet, range); // 팅 가능

	// TankLogic 모두 균등하게 있을 경우.
	if (tankEnemys.empty()){
		if (frameCount + 24 * 2 < bw->getFrameCount()){

			int siegeCount = 0;
			for (auto tank : t->tankSet){
				if (tank->self->isSieged()){
					tank->self->unsiege();
				}
				else{
					siegeCount++;
				}
			}

			if (siegeCount == t->tankSet.size()){

				flag = true;

				for (auto tank : t->tankSet){
					tank->self->move(goPos);
				}
			}
		}
	}
	else{
		frameCount = bw->getFrameCount();

		for (auto tank : t->tankSet) {
			vector<Unit> canAttackEnemys = getCanAttackEnemy(tank, UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange());
			Unitset notAttackEnemys = tank->self->getUnitsInRadius(tank->self->getType().groundWeapon().minRange(), Filter::IsEnemy);

			if (tank->self->isSieged()){
				if (t->marineSet.size() + unSigedSize < notAttackEnemys.size() * 1.5){
					tank->self->unsiege();
				}
				else{
					if (canAttackEnemys.size() == 0 && tank->maginNumber + (24 * 2) < bw->getFrameCount()){
						tank->self->unsiege();
					}
					else{
						if (canAttack(tank)){
							Unit target = getTargetUnit(tank, canAttackEnemys);
							tank->self->attack(target);
						}
					}
				}
			}
			else{
				if (t->marineSet.size() + unSigedSize < notAttackEnemys.size() * 1.5){
					if (canAttack(tank)){
						Unit target = getTargetUnit(tank, canAttackEnemys);
						tank->self->attack(target);
					}
					else{
						tank->self->rightClick(info->map->getMyStartBase()->getPosition());
					}
				}
				else{
					if (canAttackEnemys.size() == 0){
						tank->self->move(goPos);
					}
					else{
						auto chkPoint = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter();
						if (getDistance(chkPoint, tank->self->getPosition()) < 150){
							tank->self->move(goPos);
						}
						else{
							if (isSiegeTiming(tank, t)){
								tank->self->siege();

								flag = false;
								tank->maginNumber = bw->getFrameCount();
							}
							else{
								auto target = getNearEnemy(canAttackEnemys, tank->getPosition());
								if (canAttack(tank)){
									tank->self->attack(target);
								}
							}
						}
					}
				}
			}
		}
	}

	for (auto marine : t->marineSet){
		if (marine->self->isUnderStorm()) {
			Position asp = getAvoidStormPos(marine, t);
			marine->self->move(asp, false);
			continue;
		}

		auto chkPoint = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter();
		if (getDistance(chkPoint, marine->self->getPosition()) < 150){
						
			marine->self->move(goPos);
			continue;
		}

		if (getDistance(goPos, t->tankCenterPos) < getDistance(goPos, marine->getPosition())){
			auto target = getNearEnemy(marineEnemy, marine->getPosition());
			if (target == nullptr){
				marine->self->move(goPos);
			}
			else{
				if (isCanBuySteamPack(marine)) {
					marine->self->useTech(TechTypes::Stim_Packs, false);
				}
				else if (canAttack(marine)){
					marine->self->attack(target);
				}
			}
			
			continue;
		}

		if (0 < tankCantEnemys.size()){
			auto target = getNearEnemy(tankCantEnemys, marine->getPosition());
			if (isCanBuySteamPack(marine)) {
				marine->self->useTech(TechTypes::Stim_Packs, false);
			}
			else if (canAttack(marine)){
				marine->self->attack(target);
			}
		}
		else{
			if (flag){
				// 탱크들이 모두 시즈 안하고 있을 때
				if (enemyCount == 0 && getDistance(t->tankCenterPos, marine->getPosition()) < 320){
					marine->self->move(goPos);
				}
				else{
					auto target = getNearEnemy(marineEnemy, marine->getPosition());
					//auto target = getTargetUnit(marine, marineEnemy);
					if (enemyUnitCount == 0){
						maxRange = 250;
					}

					if (target == nullptr){
						if (maxRange < getDistance(t->tankCenterPos, marine->getPosition())){
							marine->self->move(t->tankCenterPos);
						}
						else{
							if (!marine->self->isHoldingPosition()){
								marine->self->holdPosition();
							}
						}
					}
					else{
						if (maxRange < getDistance(t->tankCenterPos, marine->getPosition())){
							marine->self->move(t->tankCenterPos);
						}
						else{
							if (isCanBuySteamPack(marine)) {
								marine->self->useTech(TechTypes::Stim_Packs, false);
							}
							else if (canAttack(marine)){
								marine->self->attack(target);
							}
						}
					}
				}
			}
			else{
				// 탱크들이 시즈를 하고 있을 때
				//auto target = getTargetUnit(marine, marineEnemy);

				if (maxRange < getDistance(t->tankCenterPos, marine->getPosition())){

					if (!marine->self->isHoldingPosition() || marine->self->isMoving()){
						marine->self->holdPosition();
					}
				}

				auto target = getTargetUnit(marine, marineEnemy);
				//auto target = getNearEnemy(marineEnemy, marine->getPosition());

				if (target == nullptr){

					if (marine->self->isSelected()){
						bw << "nullptr" << endl;
					}
					
					if (getDistance(t->tankCenterPos, marine->getPosition()) < 220){
						marine->self->move(goPos);
					}
				}
				else{
					// 탱크들이 시즈를 하고 있을 때
					if (isCanBuySteamPack(marine)) {
						marine->self->useTech(TechTypes::Stim_Packs, false);
					}
					else if (canAttack(marine)){
						marine->self->attack(target);
					}
				}
			}
		}
	}

	for (auto medic : t->medicSet) {
		if (medic->self->isUnderStorm()) {
			Position asp = getAvoidStormPos(medic, t);
			medic->self->move(asp, false);
			continue;
		}
		auto chkPoint = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter();
		if (getDistance(chkPoint, medic->self->getPosition()) < 150){
			medic->self->move(goPos);
			continue;
		}

		if (getDistance(goPos, t->tankCenterPos) < getDistance(goPos, medic->getPosition())){
			medic->self->move(goPos);
			continue;
		}

		Unit healTarget = getHealUnit(medic, t);

		if (healTarget == nullptr){
			auto near = getMyNearUnit(t->marineSet, medic, false);
			if (near != nullptr){
				medic->self->move(near->getPosition());
			}
			else{
				medic->self->move(t->teamCenterPos);
			}
		}
		else{
			if (medic->self->canUseTech(TechTypes::Healing, healTarget)){
				medic->self->useTech(TechTypes::Healing, healTarget);
			}
			else{
				medic->self->move(healTarget->getPosition());
			}
		}
	}
}

void CombatControl::bang3(TeamControl* t) {

	vector<MyUnit*> siegeMode;
	auto nearGroup = getNearestEnemyGroup(t->tankSet.front());

	Position goPos;

	if (t->goingSetting) {
		goPos = (Position)tightPos;
		if (info->enemy->enemyGroup.size() > 0) {
			float minDist = 99999;
			for (auto eg = info->enemy->enemyGroup.begin(); eg != info->enemy->enemyGroup.end(); eg++) {
				float dist = getDistance(eg->first, t->teamCenterPos);
				if (dist < minDist) {
					minDist = dist;
					goPos = eg->first;
				}
			}
		}
	}
	else {
		goPos = (Position)DefensePos[1];
		int mc = 0;
		int tc = 0;
		int waitZone = 260;
		for (auto mm : t->marineSet) {
			float dist = getDistance(goPos, mm->getPosition());
			if (dist < waitZone) {
				mc++;
			}
		}
		for (auto mm : t->tankSet) {
			float dist = getDistance(goPos, mm->getPosition());
			if (dist < waitZone) {
				tc++;
			}
		}

		float tdist = getDistance(t->teamCenterPos, goPos);
		if (t->team == 0) {
			if (tdist < 200 && tc > 5 && mc > 6) {
				t->goingSetting = true;
			}
		}
	}

	bang4(t);

	/*
	auto tankEnemys = getAllCanAttackEnemy(t->tankSet);
	auto tankCantEnemys = getAllCantEnemy(t->tankSet);

	static int frameCount = 0;
	int unSigedSize = 0;
	static bool flag = true;
	int enemyUnitCount = 0;
	int enemyCount = 0;
	int maxRange = 220;

	for (auto unit : bw->enemy()->getUnits()) {
		if ((unit->getType() != UnitTypes::Protoss_Photon_Cannon && unit->getType().isBuilding()) || unit->getType() == UnitTypes::Protoss_Probe) {

			continue;
		}

		if (unit->isVisible()) {
			enemyUnitCount++;
		}
	}

	for (auto unit : bw->enemy()->getUnits()) {
		if (unit->isVisible()) {
			enemyCount++;;
		}
	}

	// 시즈인애랑 아닌애 구분.
	for (auto tmp : t->tankSet) {
		if (tmp->self->isSieged()) {
			siegeMode.push_back(tmp);
		}
	}

	unSigedSize = t->tankSet.size() - siegeMode.size();
	int range = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() - 30;
	auto marineEnemy = getAllCanAttackEnemy(t->tankSet, range); // 팅 가능

																// TankLogic 모두 균등하게 있을 경우.
	if (tankEnemys.empty()) {
		if (frameCount + 24 * 2 < bw->getFrameCount()) {

			int siegeCount = 0;
			for (auto tank : t->tankSet) {
				if (tank->self->isSieged()) {
					tank->self->unsiege();
				}
				else {
					siegeCount++;
				}
			}

			if (siegeCount == t->tankSet.size()) {

				flag = true;

				for (auto tank : t->tankSet) {
					if(gameFrame % 12 == 0)
						tank->self->move(goPos);
				}
			}
		}
	}
	else {
		frameCount = bw->getFrameCount();

		for (auto tank : t->tankSet) {
			vector<Unit> canAttackEnemys = getCanAttackEnemy(tank, UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange());
			Unitset notAttackEnemys = tank->self->getUnitsInRadius(tank->self->getType().groundWeapon().minRange(), Filter::IsEnemy);

			if (tank->self->isSieged()) {
				if (t->marineSet.size() + unSigedSize < notAttackEnemys.size() * 1.5) {
					tank->self->unsiege();
				}
				else {
					if (canAttackEnemys.size() == 0 && tank->maginNumber + (24 * 2) < bw->getFrameCount()) {
						tank->self->unsiege();
					}
					else {
						if (canAttack(tank)) {
							Unit target = getTargetUnit(tank, canAttackEnemys);
							tank->self->attack(target);
						}
					}
				}
			}
			else {
				if (t->marineSet.size() + unSigedSize < notAttackEnemys.size() * 1.5) {
					if (canAttack(tank)) {
						Unit target = getTargetUnit(tank, canAttackEnemys);
						tank->self->attack(target);
					}
					else {
						tank->self->rightClick(info->map->getMyStartBase()->getPosition());
					}
				}
				else {
					if (canAttackEnemys.size() == 0) {
						tank->self->move(goPos);
					}
					else {
						auto chkPoint = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter();
						if (getDistance(chkPoint, tank->self->getPosition()) < 150) {
							tank->self->move(goPos);
						}
						else {
							if (isSiegeTiming(tank, t)) {
								tank->self->siege();

								flag = false;
								tank->maginNumber = bw->getFrameCount();
							}
							else {
								auto target = getNearEnemy(canAttackEnemys, tank->getPosition());
								if (canAttack(tank)) {
									tank->self->attack(target);
								}
							}
						}
					}
				}
			}
		}
	}

	for (auto marine : t->marineSet) {
		if (marine->self->isUnderStorm()) {
			Position asp = getAvoidStormPos(marine, t);
			marine->self->move(asp, false);
			continue;
		}

		auto chkPoint = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter();
		if (getDistance(chkPoint, marine->self->getPosition()) < 150) {
			marine->self->move(goPos);
			continue;
		}

		if (getDistance(goPos, t->tankCenterPos) < getDistance(goPos, marine->getPosition())) {
			marine->self->move(goPos);

			continue;
		}

		if (0 < tankCantEnemys.size()) {
			auto target = getNearEnemy(tankCantEnemys, marine->getPosition());
			if (isCanBuySteamPack(marine)) {
				marine->self->useTech(TechTypes::Stim_Packs, false);
			}
			else if (canAttack(marine)) {
				marine->self->attack(target);
			}
		}
		else {
			if (flag) {
				// 탱크들이 모두 시즈 안하고 있을 때
				if (enemyCount == 0 && getDistance(t->tankCenterPos, marine->getPosition()) < 320) {
					marine->self->move(goPos);
				}
				else {
					auto target = getNearEnemy(marineEnemy, marine->getPosition());
					//auto target = getTargetUnit(marine, marineEnemy);
					if (enemyUnitCount == 0) {
						maxRange = 300;
					}

					if (target == nullptr) {
						if (maxRange < getDistance(t->tankCenterPos, marine->getPosition())) {
							marine->self->move(t->tankCenterPos);
						}
						else {
							if (!marine->self->isHoldingPosition()) {
								marine->self->holdPosition();
							}
						}
					}
					else {
						if (maxRange < getDistance(t->tankCenterPos, marine->getPosition())) {
							marine->self->move(t->tankCenterPos);
						}
						else {
							if (isCanBuySteamPack(marine)) {
								marine->self->useTech(TechTypes::Stim_Packs, false);
							}
							else if (canAttack(marine)) {
								marine->self->attack(target);
							}
						}
					}
				}
			}
			else {
				// 탱크들이 시즈를 하고 있을 때
				//auto target = getTargetUnit(marine, marineEnemy);

				if (220 < getDistance(t->tankCenterPos, marine->getPosition())) {

					if (!marine->self->isHoldingPosition() || marine->self->isMoving()) {
						marine->self->holdPosition();
					}
				}

				auto target = getTargetUnit(marine, marineEnemy);
				//auto target = getNearEnemy(marineEnemy, marine->getPosition());

				if (target == nullptr) {

					if (marine->self->isSelected()) {
						bw << "nullptr" << endl;
					}

					if (getDistance(t->tankCenterPos, marine->getPosition()) < 220) {
						marine->self->move(goPos);
					}
				}
				else {
					// 탱크들이 시즈를 하고 있을 때
					if (isCanBuySteamPack(marine)) {
						marine->self->useTech(TechTypes::Stim_Packs, false);
					}
					else if (canAttack(marine)) {
						marine->self->attack(target);
					}
				}
			}
		}
	}

	for (auto medic : t->medicSet) {
		if (medic->self->isUnderStorm()) {
			Position asp = getAvoidStormPos(medic, t);
			medic->self->move(asp, false);
			continue;
		}
		auto chkPoint = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter();
		if (getDistance(chkPoint, medic->self->getPosition()) < 150) {
			medic->self->move(goPos);
			continue;
		}

		if (getDistance(goPos, t->tankCenterPos) < getDistance(goPos, medic->getPosition())) {
			medic->self->move(goPos);
			continue;
		}

		Unit healTarget = getHealUnit(medic, t);

		if (healTarget == nullptr) {
			auto near = getMyNearUnit(t->marineSet, medic, false);
			if (near != nullptr) {
				medic->self->move(near->getPosition());
			}
			else {
				medic->self->move(t->teamCenterPos);
			}
		}
		else {
			if (medic->self->canUseTech(TechTypes::Healing, healTarget)) {
				medic->self->useTech(TechTypes::Healing, healTarget);
			}
			else {
				medic->self->move(healTarget->getPosition());
			}
		}
	}
	*/
}

void CombatControl::bang4(TeamControl* t){

	if (isUnduck(t)) {
		onUNDUK(t);
		return;
	}

	vector<MyUnit*> siegeMode;
	auto nearGroup = getNearestEnemyGroup(t->tankSet.front());
	auto groupSize = info->enemy->enemyGroup.size(); 
	int range = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() - 30;

	Position goPos = t->attackPosition;   // 진출 방향
	Unitset enemy = getNearEnemyUnits(t); // 우리팀과 적군 사거리 내의 모든 유닛.
	auto tankEnemys = getAllCanAttackEnemy(t->tankSet);
	auto tankCantEnemys = getAllCantEnemy(t->tankSet);
	auto marineEnemy = getAllCanAttackEnemy(t->tankSet, range); // 팅 가능

	auto tSize = t->tankSet.size() + t->marineSet.size();
	static int frameCount = 0;
	int maxRange = 220;

	auto chkPoint = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter();

	// 우리팀이 든든하게 있다고 생각하고 싸우는 로직임.
	if (enemy.size() == 0){
		// 적군이 하나도 없을 때.
		// 탱크 - 다 시즈를 푼다.
		// 마린 - 앞으로 이동한다. (거리 유지)

		// 탱크
		for (auto tank : t->tankSet){
			if (tank->self->isSieged()){
				tank->self->unsiege();
			}
			else{
				// 여기 Enemy가 없더라도 때릴 대상은 있다!. 때려야대 마린도!
				auto target = getTargetUnit(tank, marineEnemy);
				if (target == nullptr){
					if (tank->self->isSelected()){
						bw << "move" << endl;
					}

					tank->self->move(goPos);
				}
				else{
					if (canAttack(tank)){
						if (tank->self->isSelected()){
							bw << "attack" << endl;
						}
						tank->self->attack(target);
					}
				}
			}
		}

		// 마린
		for (auto marine : t->marineSet){

			if (getDistance(chkPoint, marine->self->getPosition()) < 150){
				marine->self->move(goPos);
				continue;
			}

			if (marine->self->isUnderStorm()) {
				Position asp = getAvoidStormPos(marine, t);
				marine->self->move(asp, false);
				continue;
			}			

			auto target = getTargetUnit(marine, marineEnemy);
			if (target == nullptr){
				if (getDistance(goPos, t->tankCenterPos) < getDistance(goPos, marine->getPosition())){
					marine->self->move(goPos);
				}
				else{
					if (280 < getDistance(t->tankCenterPos, marine->getPosition())){
						if (!marine->self->isHoldingPosition())
							marine->self->holdPosition();
					}
					else{
						marine->self->move(goPos);
					}
				}
			}
			else{
				if (isCanBuySteamPack(marine)) {
					marine->self->useTech(TechTypes::Stim_Packs, false);
				}

				if (canAttack(marine)){
					marine->self->attack(target);
				}
			}
		}
	}
	else{
		if (enemy.size() < 3){
			// 시즈 ㄴㄴ 하고 다같이 밀고 ㄱㄱ

			// 탱크
			for (auto tank : t->tankSet){
				if (tank->self->isSieged()){
					tank->self->unsiege();
				}
				else{
					auto target = getTargetUnit(tank, marineEnemy);
					if (target == nullptr){
						if (tank->self->isSelected()){
							bw << "move" << endl;
						}

						tank->self->move(goPos);
					}
					else{
						if (canAttack(tank)){
							if (tank->self->isSelected()){
								bw << "attack" << endl;
							}

							tank->self->attack(target);
						}
					}
				}
			}

			// 마린
			for (auto marine : t->marineSet){

				if (getDistance(chkPoint, marine->self->getPosition()) < 150){
					marine->self->move(goPos);
					continue;
				}

				if (marine->self->isUnderStorm()) {
					Position asp = getAvoidStormPos(marine, t);
					marine->self->move(asp, false);
					continue;
				}

				if (getDistance(goPos, t->tankCenterPos) < getDistance(goPos, marine->getPosition())){
					auto target = getTargetUnit(marine, marineEnemy);
					if (target == nullptr){
						marine->self->move(goPos);
					}
					else{
						if (canAttack(marine)){
							marine->self->attack(target);
						}
					}
				}
				else{
					if (230 < getDistance(marine->self->getPosition(), t->tankCenterPos)){
						//marine->self->move(t->tankCenterPos);
						if (!marine->self->isHoldingPosition()){
							marine->self->holdPosition();
						}

						continue;
					}

					auto target = getTargetUnit(marine, marineEnemy);
					if (canAttack(marine)){
						marine->self->attack(target);
					}
				}
			}
		}
		else{
			if (enemy.size() < (tSize * 0.7)){
				// 때릴 애가 몇마리 이상 있을 때 시즈하기. ( 1마리 ㄴㄴ 해 )

				// 탱크
				for (auto tank : t->tankSet){
					auto targets = getCanAttackEnemy(tank);
					auto siegeTarget = getCanAttackEnemy(tank, UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange());

					if (tank->self->isSieged()){
						if (0 < targets.size()){
							auto target = getMassedUnit(tank, targets);
							tank->self->attack(target);
						}
						else{
							if (tank->maginNumber + 24 * 1 < bw->getFrameCount()){
								tank->self->unsiege();
							}
						}
					}
					else{
						if (getDistance(chkPoint, tank->self->getPosition()) < 150){
							tank->self->move(goPos);
							continue;
						}

						if (3 <= siegeTarget.size()){
							if (tank->self->canSiege()){
								tank->maginNumber = bw->getFrameCount();
								tank->self->siege();
							}
						}
						else{
							if (0 < targets.size()){
								//auto target = getNearEnemy(targets, tank->getPosition());
								auto target = getTargetUnit(tank, targets);
								if (canAttack(tank))
									tank->self->attack(target);
							}
							else{
								tank->self->move(goPos);
							}
						}
					}
				}

				// 마린
				for (auto marine : t->marineSet){

					if (getDistance(chkPoint, marine->self->getPosition()) < 150){
						marine->self->move(goPos);
						continue;
					}

					if (marine->self->isUnderStorm()) {
						Position asp = getAvoidStormPos(marine, t);
						marine->self->move(asp, false);
						continue;
					}

					if (getDistance(goPos, t->tankCenterPos) < getDistance(goPos, marine->getPosition())){
						auto target = getTargetUnit(marine, marineEnemy);
						if (target == nullptr){
							marine->self->move(goPos);
						}
						else{
							if (canAttack(marine)){
								marine->self->attack(target);
							}
						}
					}
					else{
						if (230 < getDistance(marine->self->getPosition(), t->tankCenterPos)){
							marine->self->move(t->tankCenterPos);
							continue;
						}

						if (190 < getDistance(marine->self->getPosition(), t->tankCenterPos)){
							if (!marine->self->isHoldingPosition()){
								marine->self->holdPosition();
							}

							continue;
						}

						if (isCanBuySteamPack(marine)) {
							marine->self->useTech(TechTypes::Stim_Packs, false);
						}

						auto target = getTargetUnit(marine, marineEnemy);
						if (canAttack(marine)){
							marine->self->attack(target);
						}
					}
				}
			}
			else{
				// 탱크
				for (auto tank : t->tankSet){

					if (getDistance(chkPoint, tank->self->getPosition()) < 150){
						tank->self->move(goPos);
						continue;
					}

					auto targets = getCanAttackEnemy(tank);
					auto siegeTarget = getCanAttackEnemy(tank, UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange());

					if (tank->self->isSieged()){
						if (0 < targets.size()){
							auto target = getMassedUnit(tank, targets);
							tank->self->attack(target);
						}
						else{
							if (tank->maginNumber + 24 * 2 < bw->getFrameCount()){
								tank->self->unsiege();
							}
						}
					}
					else{
						if (getDistance(chkPoint, tank->self->getPosition()) < 150){
							tank->self->move(goPos);
							continue;
						}

						int count = 1;
						if (t->state == TeamControl::STATE::BCN_GOING)
							count = 2;

						if (count <= siegeTarget.size()){
							if (tank->self->canSiege()){
								tank->maginNumber = bw->getFrameCount();
								tank->self->siege();
							}
						}
						else{
							if (0 < targets.size()){
								//auto target = getNearEnemy(targets, tank->getPosition());
								auto target = getTargetUnit(tank, targets);
								if (canAttack(tank))
									tank->self->attack(target);
							}
							else{
								tank->self->move(goPos);
							}
						}
					}
				}

				// 마린
				for (auto marine : t->marineSet){

					auto chkPoint = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition())->getCenter();
					if (getDistance(chkPoint, marine->self->getPosition()) < 150){
						marine->self->move(goPos);
						continue;
					}

					if (marine->self->isUnderStorm()) {
						Position asp = getAvoidStormPos(marine, t);
						marine->self->move(asp, false);
						continue;
					}

					if (getDistance(goPos, t->tankCenterPos) < getDistance(goPos, marine->getPosition())){
						auto target = getTargetUnit(marine, marineEnemy);
						if (target == nullptr){
							marine->self->move(goPos);
						}
						else{
							if (canAttack(marine)){
								marine->self->attack(target);
							}
						}
					}
					else{
						if (215 < getDistance(marine->self->getPosition(), t->tankCenterPos)){
							marine->self->move(t->tankCenterPos);
							continue;
						}

						if (190 < getDistance(marine->self->getPosition(), t->tankCenterPos)){
							if (!marine->self->isHoldingPosition()){
								marine->self->holdPosition();
							}

							continue;
						}

						if (isCanBuySteamPack(marine)) {
							marine->self->useTech(TechTypes::Stim_Packs, false);
						}

						auto target = getTargetUnit(marine, marineEnemy);
						if (canAttack(marine)){
							marine->self->attack(target);
						}
					}
				}
			}
		}
	}

	for (auto medic : t->medicSet) {

		if (getDistance(chkPoint, medic->self->getPosition()) < 150){
			medic->self->move(goPos);
			continue;
		}

		if (medic->self->isUnderStorm()) {
			Position asp = getAvoidStormPos(medic, t);
			medic->self->move(asp, false);
			continue;
		}

		if (getDistance(goPos, t->tankCenterPos) < getDistance(goPos, medic->getPosition())){
			medic->self->move(goPos);
			continue;
		}

		Unit healTarget = getHealUnit(medic, t);

		if (healTarget == nullptr){
			auto near = getMyNearUnit(t->marineSet, medic, false);
			if (near != nullptr){
				medic->self->move(near->getPosition());
			}
			else{
				medic->self->move(t->teamCenterPos);
			}
		}
		else{
			if (medic->self->canUseTech(TechTypes::Healing, healTarget)){
				medic->self->useTech(TechTypes::Healing, healTarget);
			}
			else{
				medic->self->move(healTarget->getPosition());
			}
		}
	}
}

void CombatControl::bangWait(TeamControl* t){

	auto halfSize = t->tankSet.size() / 2;

	auto chkPoint = getNearestChokepoint(t->attackPosition);
	auto enemyFront = getNearestBaseLocation(chkPoint->getCenter());
	auto marinePos = posMgr->getGasPosition(enemyFront->getTilePosition());

	for (int i = 0; i < t->tankSet.size(); i++){
		auto& tank = t->tankSet[i];

		if (i < halfSize){
			if (5 < getDistance(posMgr->getTilePosition(POS::TANK_FINISH_WAIT), tank->getTilePosition())){
				if (tank->self->isSieged()){
					tank->self->unsiege();
				}
				else{
					tank->self->move((Position)posMgr->getTilePosition(POS::TANK_FINISH_WAIT));
				}
			}
			else{
				if (!tank->self->isSieged()){
					tank->self->siege();
				}
			}
		}
		else{
			if (5 < getDistance(enemyFront->getTilePosition(), tank->getTilePosition())){
				if (tank->self->isSieged()){
					tank->self->unsiege();
				}
				else{
					tank->self->move(enemyFront->getPosition());
				}
			}
			else{
				if (!tank->self->isSieged()){
					tank->self->siege();
				}
			}
		}
	}

	for (auto marine : t->marineSet){
		marine->self->move((Position)marinePos);
	}
}

bool CombatControl::canGoHill(BWTA::BaseLocation* base, TeamControl* t){

	auto chkPoint = getNearestChokepoint(base->getTilePosition());
	auto enemyFront = getNearestBaseLocation(chkPoint->getCenter());

	int line = 8 * 32;

	int count = 0;
	for (auto unit : info->enemy->getAllUnits()){
		if (!unit->isVisible()){
			continue;
		}

		if (getDistance(enemyFront->getTilePosition(), unit->getTilePosition()) < 13){
			count++;
		}
	}

	for (auto unit : info->enemy->getAllBuildings()){
		if (!unit->isVisible()){
			continue;
		}

		if (getDistance(enemyFront->getTilePosition(), unit->getTilePosition()) < 13){
			count++;
		}
	}

	//if (count == 0 && 5 <= t->tankSet.size() && 10 <= t->marineSet.size()){
	if (count == 0 && (getDistance(enemyFront->getPosition(), t->teamCenterPos) < line || getDistance(enemyFront->getPosition(), t->tankCenterPos) < line ||
		getDistance(enemyFront->getPosition(), t->bionicCenterPos) < line)){
		return true;
	}

	return false;
}

void CombatControl::onSAMPLE2(TeamControl* t)
{
	Unitset enemy = getNearEnemyUnits(t);

	Position targetPos = t->attackPosition;
	Position goPos = (Position)tightPos;

	for (auto marine : t->marineSet) {
		Unit target = getTargetUnit(marine, enemy);
		if (target == NULL) {
			marine->self->move(goPos, false);
		}
		else {
			marine->self->attack(target, false);
		}
	}
	for (auto tank : t->tankSet) {
		Unit target = getTargetUnit(tank, enemy);
		if (target == NULL) {
			tank->self->move(goPos, false);
		}
		else {
			tank->self->attack(target, false);
		}
	}
	for (auto medic : t->medicSet) {
		Unit target = getTargetUnit(medic, enemy);
		if (target == NULL) {
			medic->self->move(goPos, false);
		}
		else {
			medic->self->useTech(TechTypes::Healing, target);
		}
	}
}

/////////////////////////////////
void CombatControl::onSAMPLE(TeamControl* t)
{

	Unitset enemy = getNearEnemyUnits(t);

	Position targetPos = t->attackPosition;
	Position goPos = t->attackPosition;

	int marineZone = 100;

	int stcount = 0;
	for (auto tank : t->tankSet) {
		if (tank->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
			stcount++;
		}
	}
	if (stcount * 3 < t->tankSet.size()) marineZone = 150;

	if (t->goingSetting) {
		goPos = (Position)tightPos;
		targetPos = info->map->getEnemyStartBase()->getPosition();
	}
	else if (t->goingDariSetting) {
		goPos = (Position)middleGoPos;
		targetPos = (Position)middlePos;

		float tdist = getDistance(t->teamCenterPos, targetPos);
		if (tdist < 100) {
			t->goingSetting = true;
		}
	}
	else {
		goPos = (Position)DefensePos[1];
		targetPos = (Position)DefensePos[1];

		int mc = 0;
		int tc = 0;
		for (auto mm : t->marineSet) {
			float dist = getDistance(targetPos, mm->getPosition());
			if (dist < 150) {
				mc++;
			}
		}
		for (auto mm : t->tankSet) {
			float dist = getDistance(targetPos, mm->getPosition());
			if (dist < 150) {
				tc++;
			}
		}

		float tdist = getDistance(t->teamCenterPos, targetPos);
		if (t->team == 0) {
			if (tdist < 100 && tc > 6 && mc > 6) {
				t->goingDariSetting = true;
			}
		}
		else {
			if (tdist < 100) {
				t->goingDariSetting = true;
			}
		}
	}

	t->fTank = getFrontTank(t, targetPos);
	t->fMarine = getFrontMarine(t, targetPos);

	if (info->enemy->enemyGroup.size() > 0 && t->fTank != NULL && t->team == 0) {
		goPos = getNearestEnemyGroup(t->fTank);
	}

	if (1) {
		if (gameFrame % 24 == 0 && t->fTank != NULL && t->fMarine != NULL) {
			float fmtDist = getDistance(t->fTank->getPosition(), t->fMarine->getPosition());	// 마린탱크거리
			float ttdist = getDistance(t->fTank->getPosition(), goPos);					// 탱크 타겟거리
			float mtdist = getDistance(t->fMarine->getPosition(), goPos);					// 마린 타겟거리
			if (enemy.size() > 0) {
				// 적이 있을 때
				if (fmtDist > marineZone) {
					for (auto marine : t->marineSet) {
						marine->self->move(t->tankCenterPos, false);
					}
				}
				else {
					for (auto marine : t->marineSet) {
						if (isCanBuySteamPack(marine)) {
							marine->self->useTech(TechTypes::Stim_Packs, false);
						}
						else {
							Unit targetUnit = getTargetUnit(marine, enemy);
							if (targetUnit == NULL) {
								marine->self->attack(goPos, false);
							}
							else {
								marine->self->attack(targetUnit, false);
							}
						}
					}
				}
			}
			else {
				// 적이 없을 때
				if (ttdist > mtdist) {
					for (auto marine : t->marineSet) {
						marine->self->holdPosition(false);
					}
				}
				else {
					for (auto marine : t->marineSet) {
						marine->self->move(goPos, false);
					}
				}
			}
		}

		/*for (auto marine : t->marineSet) {
		marine->maginNumber++;
		Unit target = getTargetUnit(marine, enemy);
		if (t->tankSet.size() > 0) {
		float ttdist = getDistance(fTank->getPosition(), targetPos);
		float mtdist = getDistance(marine->getPosition(), targetPos);

		if (ttdist + 90 > mtdist) {
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Orange, false);
		marine->self->move(t->tankCenterPos, false);
		}
		else if (ttdist > mtdist) {
		if (target != NULL) {
		if (isCanBuySteamPack(marine)) {
		marine->self->useTech(TechTypes::Stim_Packs, false);
		}
		else if (marine->self->isAttacking() == false) {
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Purple, false);
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Purple, false);
		marine->self->attack(target, false);
		}
		else if (marine->self->getOrder() == Orders::AttackMove) {
		if (marine->self->isHoldingPosition() == false) {
		marine->self->holdPosition();
		}
		}
		else {
		if (marine->self->isHoldingPosition() == false) {
		marine->self->holdPosition();
		}
		}
		}
		else {
		if (marine->self->isHoldingPosition() == false) {
		marine->self->holdPosition();
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Black, false);
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Black, false);
		}
		}
		}
		else {
		if (target != NULL) {
		if (isCanBuySteamPack(marine)) {
		marine->self->useTech(TechTypes::Stim_Packs, false);
		}
		else if (marine->self->isAttacking() == false && marine->self->isIdle()) {
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Green, false);
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Green, false);
		marine->self->attack(target, false);
		}
		}
		else {
		if (gameFrame % 12 == 0) {
		marine->self->move(goPos, false);
		}
		}
		}
		}
		}*/
		for (auto medic : t->medicSet) {
			medic->maginNumber++;
			Unit target = getHealUnit(medic, t);
			if (target != NULL) {
				float d = getDistance(target->getPosition(), medic->getPosition());
				if (medic->self->isIdle())
					medic->self->useTech(TechTypes::Healing, false);
			}
			else {
				Position mp = getNearestMarinePos(medic, t);
				if (mp.x > 0) {
					if (gameFrame % 12 == 0) {
						medic->self->move(mp, false);
					}
				}
				else {
					if (gameFrame % 12 == 0) {
						if (t->marineSet.size() > 0) {
							medic->self->move(t->bionicCenterPos, false);
						}
						else {
							medic->self->move(goPos, false);
						}
					}
				}
			}
			joiningTeam(medic, t);
		}
		for (auto tank : t->tankSet) {
			tank->maginNumber++;
			if (tank->self->isSieged()) {
				Unit target = getMassedUnit(tank, enemy);
				if (target == NULL) {
					target = getTargetBuilding(tank);
					if (target == NULL) {
						if (tank->maginNumber > 24) {
							tank->self->unsiege();
						}
					}
					else {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
						}
					}
				}
				else {
					if (isOnlyZealot(t) && tank->maginNumber > 24) {
						tank->self->unsiege();
					}
					else {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
						}
					}
				}
			}
			else {
				tank->maginNumber = 0;
				Unit target = getTargetUnit(tank, enemy);
				if (target == NULL)
				{
					target = getTargetBuilding(tank);
					if (target == NULL) {
						if (gameFrame % 20 == 0) {
							tank->self->move(goPos, false);
						}
					}
					else {
						if (getNearestChokePointDistance(tank) < 150) {
							if (gameFrame % 6 == 0) {
								tank->self->move(info->map->getEnemyStartBase()->getPosition(), false);
							}
						}
						else if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
				}
				else {
					if (isOnlyZealot(t)) {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
						}
						else {
							tank->self->move(info->map->getMyStartBase()->getPosition(), false);
						}
					}
					else {
						if (getNearestChokePointDistance(tank) < 150) {
							if (gameFrame % 6 == 0) {
								tank->self->move(info->map->getEnemyStartBase()->getPosition(), false);
							}
						}
						else {
							if (tank->self->canSiege()) {
								tank->self->siege();
							}
						}
					}
				}
			}
			joiningTeam(tank, t);
		}
	}
	else {
		for (auto marine : t->marineSet) {
			marine->maginNumber++;
			if (gameFrame % 24 == 0) {
				marine->self->move(goPos, false);
			}
		}
		for (auto medic : t->medicSet) {
			medic->maginNumber++;
			if (gameFrame % 24 == 0) {
				medic->self->move(goPos, false);
			}
		}
		for (auto tank : t->tankSet) {
			tank->maginNumber++;
			if (tank->self->isSieged() && tank->maginNumber > 24) {
				tank->self->unsiege();
			}
			else if (tank->self->isSieged() == false) {
				if (gameFrame % 12 == 0) {
					tank->self->move(goPos, false);
				}
			}
		}
	}
}

void CombatControl::onSTART(TeamControl* t)
{
	auto enemy = info->map->getCloseCount(UnitTypes::Protoss_Zealot);
	auto enemyCount = enemy + info->map->getCloseCount(UnitTypes::Protoss_Dragoon);

	if (EnemyOnce == false)
	{
		EnemyOnce = CheckEnemyOnce();	//false : 안왔거나 일꾼만, true : 왔었어
	}

	if (EnemyOnce == true)
	{
		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1))
		{
			setMarineHoldPosition(t);
			Position rpos;
			rpos.x = (info->map->getMyTerritorys()[0]->getPosition().x + (((Position)posMgr->getTilePosition(POS::SIM1_BARRACK)).x) * 2) / 3;
			rpos.y = (info->map->getMyTerritorys()[0]->getPosition().x + (((Position)posMgr->getTilePosition(POS::SIM1_BARRACK)).y) * 2) / 3;
			for (auto marine : t->marineSet)
			{
				if (getDistance(marine->self->getPosition(), rpos) < 3 * TILE_SIZE)
				{
					marineHold(marine);
				}
				else{
					if (gameFrame % 24 == 0)
					{
						marine->self->move(rpos);
					}
				}
			}
		}
		else{
			for (auto marine : t->marineSet)
			{
				if (!marine->self->isAttacking())
				{
					marine->self->attack(info->map->getMyTerritorys().front()->getPosition());
				}
			}
		}
	}
	else{
		if (enemy <= 0)
		{
			for (auto marine : t->marineSet)
			{
				if (!isAlmostReachedDest(marine->self->getPosition(), info->map->getMyTerritorys().back()->getPosition(), 3 * TILEPOSITION_SCALE, 3 * TILEPOSITION_SCALE))
				{
					if (gameFrame % 24 == 0)
					{
						marine->self->move(info->map->getMyTerritorys().back()->getPosition());
					}
				}
			}
		}
	}
}

void CombatControl::close(TeamControl* t){

	static bool isEnemyCome = false;
	vector<Unit>& enemy = info->map->getMyTerritorys().back()->getCloseEnemy();
	Unit photo = nullptr;

	if (0 < info->map->getCloseCount(UnitTypes::Protoss_Zealot) || 0 < info->map->getCloseCount(UnitTypes::Protoss_Dragoon) || 0 < info->map->getCloseCount(UnitTypes::Protoss_Dark_Templar)){
		isEnemyCome = true;
	}

	if (0 < info->map->getCloseCount(UnitTypes::Protoss_Photon_Cannon)){
		for (auto unit : enemy){
			if (unit->getType() == UnitTypes::Protoss_Photon_Cannon){
				photo = unit;
			}
		}

		if (photo->isCompleted()){
			isEnemyCome = true;
		}
	}

	if (!isEnemyCome){
		if (enemy.size() == 0){
			for (auto marine : t->marineSet){
				marine->self->move((Position)posMgr->getTilePosition(POS::SIM1_BARRACK));
			}			
		}
		else{
			for (auto marine : t->marineSet){
				if (marine->self->canAttack(marine)){
					marine->self->attack(getTargetUnit(marine, enemy));
				}
			}
		}		
	}
	else{
		if (enemy.size() == info->map->getCloseCount(UnitTypes::Protoss_Zealot) || enemy.size() == info->map->getCloseCount(UnitTypes::Protoss_Probe) + info->map->getCloseCount(UnitTypes::Protoss_Zealot)){
			// 질럿만 있을 경우. 혹은 질럿 + 프로브
			for (auto marine : t->marineSet){
				marineHold(marine);
			}
		}
		else if (enemy.size() == info->map->getCloseCount(UnitTypes::Protoss_Dragoon) || enemy.size() == info->map->getCloseCount(UnitTypes::Protoss_Probe) + info->map->getCloseCount(UnitTypes::Protoss_Dragoon)){
			// 드라군만 있을 경우. 혹은 드라군 + 프로브
			int dragoonCount = info->map->getMyTerritorys().back()->getCloseCount(UnitTypes::Protoss_Dragoon);
			if (dragoonCount == 1){
				if (3 < info->self->getUnitCount(UnitTypes::Terran_Marine)){
					for (auto marine : t->marineSet){
						if (getProgress(marine->self) < 40){
							marine->self->move((Position)posMgr->getTilePosition(POS::THIRD_FACTORY));
						}
						else{
							if (canAttack(marine))
								marine->self->attack(getTargetUnit(marine, enemy));
						}
					}
				}
				else{
					
					// 여기서 치고 빠지는 로직이 필요하다.			
					for (auto marine : t->marineSet){
						if (getProgress(marine->self) < 40){
							marine->self->move((Position)posMgr->getTilePosition(POS::THIRD_FACTORY));
						}
						else{
							marineHold(marine);
						}
					}
				}
			}
		}
		else if (enemy.size() == info->map->getCloseCount(UnitTypes::Protoss_Zealot) + info->map->getCloseCount(UnitTypes::Protoss_Dragoon) 
			|| enemy.size() == info->map->getCloseCount(UnitTypes::Protoss_Probe) + info->map->getCloseCount(UnitTypes::Protoss_Zealot) + info->map->getCloseCount(UnitTypes::Protoss_Dragoon)){

		}
	}
}

void CombatControl::onCLOSED(TeamControl* t) {
	vector<Unit> enemies = info->map->getCloseEnemy();

	int ZealotCount = info->map->getCloseCount(UnitTypes::Protoss_Zealot);
	int DragoonCount = info->map->getCloseCount(UnitTypes::Protoss_Dragoon);

	if (EnemyOnce == false)
	{
		EnemyOnce = CheckEnemyOnce();	//false : 안왔거나 일꾼만, true : 왔었어
	}

	int marineIndex = 0;
	for (auto marine : t->marineSet)
	{
		marine->isInTeam = true;
		Unit target = getTargetUnit(marine, enemies);
		if (EnemyOnce == true)
		{
			if (target == NULL)
			{
				//아예 없는거
				if (enemies.empty())
				{
					if (marineIndex <= 4)
					{
						marineHold(marine);
					}
					else{
						if (gameFrame % 12 == 0)
						{
							marine->self->move((Position)posMgr->getTilePosition(POS::SIM1_BARRACK));
						}
					}
				}
				//사정거리 밖에 있는 거
				else{
					if (t->tankSet.size() < 1)
					{
						if (gameFrame % 12 == 0)
						{
							marine->self->move(info->map->getMyTerritorys()[0]->getPosition());
						}
					}
					else{
						if (gameFrame % 24 == 0)
						{
							if (marineIndex <= 4)
							{
								marineHold(marine);
							}
							else{
								marine->self->move((Position)posMgr->getTilePosition(POS::SIM1_BARRACK));
							}
						}
					}
				}
			}
			//사정거리 안에 있는 것
			else
			{
				if (t->tankSet.size() < 1)
				{
					if (DragoonCount <= 0)
					{
						if (ZealotCount >= 0)
						{
							if (marineIndex > 4)
							{
								if (!isAlmostReachedDest(marine->self->getPosition(), (Position)posMgr->getTilePosition(POS::SIM1_BARRACK), 50, 50))
								{
									if (gameFrame % 24 == 0)
									{
										marine->self->move((Position)posMgr->getTilePosition(POS::SIM1_BARRACK));
									}
								}
							}
							else{
								marineHold(marine);
							}
						}
					}
					else{
						if (gameFrame % 24 == 0)
						{
							//marine->self->move((Position)posMgr->getTilePosition(POS::THIRD_FACTORY));// info->map->getMyTerritorys()[0]->getPosition());
							if (3 < getDistance(marine->self->getPosition(), info->map->getMyTerritorys()[0]->getPosition())){
								marine->self->move(info->map->getMyTerritorys()[0]->getPosition());
							}
							else{
								if (!marine->self->isHoldingPosition()){
									marine->self->holdPosition();
								}
							}
						}
					}
				}
				else{
					if (canAttack(marine))
					{
						if (marineIndex <= 4)
						{
							Position tpos = marine->targetPosition;
							tpos.x = tpos.x - 60;
							tpos.y = tpos.y - 60;
							if (isAlmostReachedDest(marine->self->getPosition(), tpos, 120, 120))
							{
								marine->self->attack(target);
							}
							else{
								marineHold(marine);
							}
						}
						else{
							Position mpos = (Position)posMgr->getTilePosition(POS::SIM1_BARRACK);
							mpos.x = mpos.x - 50;
							mpos.y = mpos.y - 50;
							if (isAlmostReachedDest(marine->self->getPosition(), mpos, 100, 100))
							{
								marine->self->attack(target);
							}
							else{
								if (gameFrame % 12 == 0){
									marine->self->move(mpos);
								}
							}
						}
					}
					else{
						if (marineIndex <= 4)
						{
							marineHold(marine);
						}
						else{
							if (gameFrame % 24 == 0)
							{
								marine->self->move((Position)posMgr->getTilePosition(POS::SIM1_BARRACK));
							}
						}
					}
				}
			}
		}
		//아직 적군이 안왔다
		else
		{
			if (target == NULL)
			{
				if (gameFrame % 12 == 0)
				{
					marine->self->move((Position)posMgr->getTilePosition(POS::FRONT_BARRACK));
				}
			}
		}

		marineIndex++;
		joiningTeam(marine, t);
	}

	for (auto medic : t->medicSet) {
		Unit healTarget = getHealUnit(medic, t);

		if (healTarget == NULL) {
			int mindis = 9999;
			Unit followTarget = NULL;
			for (auto marine : t->marineSet)
			{
				if (getDistance(marine->getTilePosition(), medic->getTilePosition()) < mindis)
				{
					mindis = getDistance(marine->getTilePosition(), medic->getTilePosition());
					followTarget = marine->self;
				}
			}
			if (gameFrame % 48 == 0) {
				medic->self->move(followTarget->getPosition(), false);
			}
		}
		else {
			if (gameFrame % 24 == 0)
				medic->self->useTech(TechTypes::Healing, healTarget);
		}
		joiningTeam(medic, t);
	}


	int tankIndex = 0;
	int repairCount = 0;
	int hillCount = 0;

	int dragoonRange = UnitTypes::Protoss_Dragoon.groundWeapon().maxRange() + 64;
	int tankRange = UnitTypes::Terran_Siege_Tank_Tank_Mode.groundWeapon().maxRange();
	printf("dr : %d tr %d\n", dragoonRange, tankRange);
	int tankCount = 0;
	if (enemies.size() > 0) {
		for (auto tank : t->tankSet) {
			if (BWAPI::Broodwar->self()->hasResearched(TechTypes::Tank_Siege_Mode)) {
				if (tankCount == 0) {
					float dist = getDistance(tank->getPosition(), (Position)defenseTankPos[0]);
					if (dist < 20) {
						if (tank->self->canSiege()) {
							tank->self->siege();
						}
					}
					else {
						tank->self->move((Position)defenseTankPos[0], false);
					}
				}
				else {
					float minDist = 99999;
					Unit target = NULL;
					if (DragoonCount > 0) {
						for (auto e : enemies) {
							if (e->getType() != UnitTypes::Protoss_Dragoon) continue;
							float dist = getDistance(e->getPosition(), tank->getPosition());
							if (dist < minDist) {
								minDist = dist;
								target = e;
							}
						}
					}
					else {
						for (auto e : enemies) {
							float dist = getDistance(e->getPosition(), tank->getPosition());
							if (dist < minDist) {
								minDist = dist;
								target = e;
							}
						}
					}
					float edist = getDistance(target->getPosition(), tank->getPosition());
					if (tank->self->getGroundWeaponCooldown() < 5 && (float)tank->self->getType().maxHitPoints() / (float)tank->self->getHitPoints() > 0.4) {
						if (edist < tankRange) {
							if (tank->self->getGroundWeaponCooldown() == 0) {
								tank->self->attack(target, false);
								printf("공격!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
							}
							else {
								if (tank->self->isHoldingPosition() == false) {
									tank->self->holdPosition(false);
									printf("홀드!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
								}
							}
						}
						else {
							tank->self->move((Position)posMgr->getTilePosition(POS::SIM1_BARRACK), false);
							printf("멀어!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
						}
					}
					else {
						if (target->getType() == UnitTypes::Protoss_Dragoon) {
							if (edist > dragoonRange + 18) {
								if (tank->self->isHoldingPosition() == false) {
									tank->self->holdPosition(false);
									printf("후홀!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
								}
							}
							else {
								tank->self->move(info->map->getMyStartBase()->getPosition(), false);
								printf("후퇴!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
							}
						}
						else {
							if (tank->self->isHoldingPosition() == false) {
								tank->self->holdPosition();
							}
						}
					}
				}
			}
			else {
				float minDist = 99999;
				Unit target = NULL;
				if (DragoonCount > 0) {
					for (auto e : enemies) {
						if (e->getType() != UnitTypes::Protoss_Dragoon) continue;
						float dist = getDistance(e->getPosition(), tank->getPosition());
						if (dist < minDist) {
							minDist = dist;
							target = e;
						}
					}
				}
				else {
					for (auto e : enemies) {
						float dist = getDistance(e->getPosition(), tank->getPosition());
						if (dist < minDist) {
							minDist = dist;
							target = e;
						}
					}
				}
				float edist = getDistance(target->getPosition(), tank->getPosition());
				if (tank->self->getGroundWeaponCooldown() < 5 && (float)tank->self->getHitPoints()/(float)tank->self->getType().maxHitPoints() > 0.5f) {
					if (edist < tankRange) {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
							printf("공격!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
						}
						else {
							if (tank->self->isHoldingPosition() == false) {
								tank->self->holdPosition(false);
								printf("홀드!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
							}
						}
					}
					else {
						tank->self->move(target->getPosition(), false);
						printf("멀어!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
					}
				}
				else {
					if (target->getType() == UnitTypes::Protoss_Dragoon) {
						if (edist > dragoonRange + 18) {
							if (tank->self->isHoldingPosition() == false) {
								tank->self->holdPosition(false);
								printf("후홀!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
							}
						}
						else {
							tank->self->move(info->map->getMyStartBase()->getPosition(), false);
							printf("후퇴!! tank cool down %d\n", tank->self->getGroundWeaponCooldown());
						}
					}
					else {
						if (tank->self->isHoldingPosition() == false) {
							tank->self->holdPosition();
						}
					}
				}
			}
			tankCount++;
		}
	}
	else {
		for (auto tank : t->tankSet) {
			tank->self->move((Position)defenseTankPos[0], false);
		}
	}
	
	/*
	/*for (auto tank : t->tankSet)
	{
		auto size = t->tankSet.size();
		Unit target = getTargetUnit(tank, enemies);
		//사정거리에 유닛이 없다.
		if (target == NULL)
		{
			Position pos = (Position)posMgr->getTilePosition(POS::SIM1_BARRACK);
			//보이는 유닛이 없다.
			if (enemies.empty())
			{
				if ((tankIndex < size - 1 && tankIndex < 2) || size == 1)
				{
					float dist = getDistance(tank->self->getPosition(), (Position)defenseTankPos[0]);
					if (dist <= 60 + (tankIndex * 20))
					{
						if (bw->self()->hasResearched(TechTypes::Tank_Siege_Mode) == true)
						{
							if (!tank->self->isSieged())
							{
								tank->self->siege();
							}
						}
						else{
							if (!tank->self->isHoldingPosition())
							{
								tank->self->holdPosition();
							}
						}
					}
					else
					{
						if (tank->self->isSieged())
						{
							tank->self->unsiege();
						}
						if (gameFrame % 24 == 0)
						{
							tank->self->move((Position)defenseTankPos[0]);
						}
					}
				}
				else
				{
					if (!isAlmostReachedDest(tank->self->getPosition(), pos, 4 * TILE_SIZE, 4 * TILE_SIZE))
					{
						if (tank->self->isSieged())
						{
							tank->self->unsiege();
						}
						if (gameFrame % 24 == 0)
						{
							tank->self->move(pos);
						}
					}
					else{
						if (tank->self->isHoldingPosition() == false)
						{
							tank->self->holdPosition();
						}
						if (tank->self->isSieged())
						{
							tank->self->unsiege();
						}
					}
				}
			}
			else{
				if (gameFrame % 24 == 0)
				{
					tank->self->move(pos);
				}
			}
		}
		//사정거리 안에 유닛이 있다.
		else
		{
			//시즈모드 할 유닛 설정
			if ((size == 2 && tankIndex == 1) || (size >= 3 && (tankIndex == 1 || tankIndex == 2)))
			{
				if (!isAlmostReachedDest(tank->self->getPosition(), (Position)defenseTankPos[0], 80, 80))
				{
					if (bw->self()->hasResearched(TechTypes::Tank_Siege_Mode) == true)
					{
						if (tank->self->isSieged() == false)
						{
							tank->self->siege();
						}
					}
					else{
						if (tank->self->isHoldingPosition() == false)
						{
							tank->self->holdPosition();
						}
					}
				}
				else{
					if (tank->self->getGroundWeaponCooldown() <= 5)
					{
						tank->self->attack((Position)defenseTankPos[0]);
					}
					else{
						if (gameFrame % 12 == 0)
						{
							tank->self->move((Position)defenseTankPos[0]);
						}
					}
				}
			}
			//배럭 앞에 있을 애들
			else
			{
				if (tank->self->getHitPoints() <= tank->self->getType().maxHitPoints() * 0.5)
				{
					if (gameFrame % 12 == 0)
					{
						tank->self->move((Position)posMgr->getTilePosition(POS::THIRD_FACTORY));
					}
				}
				else{
					//시즈모드 개발 된 상황
					if (bw->self()->hasResearched(TechTypes::Tank_Siege_Mode) == true)
					{
						if (getDistance(tank->self->getPosition(), target->getPosition()) == UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange())
						{
							if (tank->self->isSieged() == false)
							{
								tank->self->siege();
							}
						}
						else
						{
							if (tank->self->isSieged())
							{
								tank->self->unsiege();
							}
							else{
								if (gameFrame % 3 == 0)
								{
									tank->self->move(info->map->getMyTerritorys()[0]->getPosition());
								}
							}
						}
						// 추가
					}
					//아직 시즈모드 개발 안된 상황
					else{
						if (!bw->getUnitsInRadius(tank->self->getPosition(), UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange(), Filter::IsEnemy).empty())//groundWeapon().maxRange(), Filter::IsEnemy).empty())
						{
							//tank->self->move((Position)posMgr->getTilePosition(POS::SIM1_BARRACK));
							if (tankIndex < 1)
							{

							}
						}
						else{
							if (tank->self->isHoldingPosition() == false)
							{
								tank->self->holdPosition();
							}
						}
					}
				}
			}
		}
		tankIndex++;
	}
	}*/

	/*
	int minHp = 9999;
	MyUnit *target = NULL;
	for (auto tank : t->tankSet)
	{
		if ((tank->self->getHitPoints() < tank->self->getType().maxHitPoints()) && (tank->self->getHitPoints() < minHp))
		{
			target = tank;
			minHp = tank->self->getHitPoints();
		}
	}
	if (target != NULL)
	{
		for (auto scv : repairScvs){
			if (scv == nullptr){
				scv = info->map->getMyTerritorys()[0]->getNearWorker((Position)posMgr->getTilePosition(POS::SIM1_BARRACK), false);
			}

			if (!scv->self->exists()){
				scv = info->map->getMyTerritorys()[0]->getNearWorker((Position)posMgr->getTilePosition(POS::SIM1_BARRACK), false);
			}
		}

		for (auto scv : repairScvs){
			if (scv->self->isRepairing() == false)
			{
				scv->self->repair(target->self);
			}
		}
	}
	else{
		for (auto scv : repairScvs){
			if (scv != nullptr && scv->self->exists()){
				scv->state = MyUnit::STATE::IDLE;
			}
		}
	}
	*/

}

void CombatControl::onFRONT(TeamControl* t) 
{
	Unitset enemy = getNearEnemyUnits(t);

	/*int scvcC = 0;
	for (auto scv : t->scvSet) {
		if (scvcC > zealotCount + dragoonCount) {
			info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BUNKER))->addScv(scv);
		}
		if (repairBunker != NULL || repairTank != NULL) {
			if(repairTank != NULL){
				if (repairTank->getHitPoints() < UnitTypes::Terran_Siege_Tank_Siege_Mode.maxHitPoints() / 4 * 3) {
					if (scv->self->isRepairing() == false) {
						scv->self->repair(repairTank, false);
					}
					continue;
				}
			}
			if (repairBunker != NULL) {
				if (repairBunker->getHitPoints() < UnitTypes::Terran_Bunker.maxHitPoints() / 4 * 3) {
					if (scv->self->isRepairing() == false) {
						scv->self->repair(repairBunker, false);
					}
					continue;
				}
			}
		}
		else {
			if (scv->self->isIdle()) {
				float minDist = 9999;
				Unit minDistMineral = NULL;
				for (auto mineral : BWAPI::Broodwar->getMinerals()) {
					float dist = getDistance(scv->getPosition(), mineral->getPosition());
					if (minDist > dist) {
						minDist = dist;
						minDistMineral = mineral;
					}
				}

				if (minDistMineral != NULL) {
					scv->self->rightClick(minDistMineral, false);
				}
			}
		}
		scvcC++;
	}*/

	/*int seat = 0;
	for (auto bun : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		seat += 4;
		seat -= bun->self->getLoadedUnits().size();
	}
	*/

	Position ma = (Position)DefensePos[0];
	Position ma2 = (Position)DefensePos[0];
	Position mt = (Position)waitTankPos;

	int ev = 0;
	int edragoon = 0;
	for (auto q : info->map->getMyTerritorys().back()->getCloseEnemy()) {
		if (q->getType() == UnitTypes::Protoss_Zealot) ev += 4;
		if (q->getType() == UnitTypes::Protoss_Dragoon) {
			ev += 4;
			edragoon++;
		}
	}

	if (info->map->getMyTerritorys()[0]->getCloseEnemy().size() > 0) {
		if (info->map->getMyTerritorys()[0]->getCloseEnemy().size() == info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Observer)) {
			enemyBase = false;
		}
		else {
			enemyBase = true;
		}
	}
	if (t->tankSet.size() > 0 && t->tankSet.size() < 4) {
		enemyfront = isEnemyFront();
		enemyBase = false;
	}
	else {
		enemyfront = false;
		enemyBase = false;
	}

	int waitZone = 32*4;
	int holdZone = 75;

	if (t->tankSet.size() > 4) {
		waitSet = true;
	}
	
	if (enemyBase) {
		int maxSCVCount = 8;
		int minSCVCount = 2;

		int requiredSCV = (ev - (int)t->tankSet.size() * 4 - (int)t->marineSet.size()) / 2;
		if (requiredSCV > maxSCVCount) requiredSCV = maxSCVCount;
		if (requiredSCV < minSCVCount) requiredSCV = minSCVCount;

		if (t->scvSet.size() < requiredSCV) {
			MyUnit* scv = NULL;
			scv = info->map->getNearTerritory(info->map->getMyStartBase()->getTilePosition())->getWorker(false);
			if (scv != NULL)
				UnitControl::getInstance()->returnUnit(scv);
		}

		for (auto marine : t->marineSet) {
			Unit target = getTargetUnit(marine, enemy);
			if (target == NULL) {
				if (info->map->getMyTerritorys().back()->getCloseEnemy().size() > 0) {
					marine->self->move(info->map->getMyTerritorys().back()->getCloseEnemy().front()->getPosition(), false);
				}
			}
			else {
				float tmdist = getDistance(target->getPosition(), marine->getPosition());
				if (tmdist < 40 && target->getType() == UnitTypes::Protoss_Zealot) {
					marine->self->move(info->map->getMyStartBase()->getPosition(), false);
				}
				else if (canAttack(marine)) {
					marine->self->attack(target);
				}
			}
		}

		int scvCount = 0;

		for (auto scv : t->scvSet) {
			Unit rtarget = getRepairUnit(scv);
			if (scvCount < 2) {

				Unit ftank = NULL;
				ftank = t->tankSet.front()->self;
				if (ftank != NULL) {
					if (ftank->getType().maxHitPoints() - ftank->getHitPoints() > 0) {
						if (scv->self->isRepairing() == false) {
							scv->self->repair(ftank, false);
						}
					}
					else {
						if (gameFrame % 12 == 0)
							scv->self->move(ftank->getPosition(), false);
					}
				}
				else {
					if (canAttack(scv)) {
						scv->self->attack(info->map->getMyTerritorys()[0]->getCloseEnemy().front()->getPosition(), false);
					}
				}
			}
			else if (rtarget == NULL) {
				if (canAttack(scv)) {
					scv->self->attack(info->map->getMyTerritorys()[0]->getCloseEnemy().front()->getPosition(), false);
				}
			}
			else {
				if (scv->self->isRepairing() == false) {
					scv->self->repair(rtarget, false);
				}
			}
			scvCount++;
		}

		int tankIndex = 0;
		for (auto tank : t->tankSet) {
			if (tankIndex == 0) {
				float dist = getDistance(tank->getPosition(), (Position)defenseTankPos[0]);
				if (dist < 20) {
					if (tank->self->canSiege()) {
						tank->self->siege();
					}
				}
				else {
					Unit target = getTargetUnit(tank, enemy);
					if (target == NULL) {
						tank->self->move((Position)defenseTankPos[0], false);
					}
					else {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
						}
						else {
							tank->self->move((Position)defenseTankPos[0], false);
						}
					}
				}
			}
			else {
				Unit target = getTargetUnit(tank, enemy);
				if (target == NULL) {
					if (tank->self->isSieged()) tank->self->unsiege();
					else if (info->map->getMyTerritorys()[0]->getCloseEnemy().size() > 0) {
						tank->self->attack(info->map->getMyTerritorys()[0]->getCloseEnemy().front()->getPosition(), false);
					}
				}
				else {
					if (tank->self->getGroundWeaponCooldown() == 0) {
						tank->self->attack(target, false);
					}
					else {
						float dist = getDistance(target->getPosition(), tank->getPosition());
						if (dist > UnitTypes::Terran_Siege_Tank_Tank_Mode.groundWeapon().maxRange()) {
							tank->self->move(target->getPosition(), false);
						}
						else if (dist < 40) {
							tank->self->move(info->map->getMyTerritorys().back()->getPosition(), false);
						}
						else {
							if (canAttack(tank))
								tank->self->attack(target, false);
						}
					}
				}
			}
			tankIndex++;
		}
		return;
	}
	else if (enemyfront) {
		Position waitPos = (Position)PositionMgr::getInstance()->getTilePosition(POS::SIM1_BARRACK);
		waitPos.x += UnitTypes::Terran_Barracks.width();
		waitPos.y += UnitTypes::Terran_Barracks.height();
		Position waitSCVPos = BWTA::getNearestChokepoint(info->map->getMyStartBase()->getPosition())->getCenter();
		bool outTank = false;
		for (auto tank : t->tankSet) {
			if (BWTA::getRegion(info->map->getMyStartBase()->getPosition()) != BWTA::getRegion(tank->getPosition())) {
				outTank = true;
			}
		}
		if (outTank) {
			waitSCVPos = waitPos;
		}
		int maxSCVCount = 8;
		int minSCVCount = 2;

		int requiredSCV = (ev - (int)t->tankSet.size() * 4 - (int)t->marineSet.size()) / 2;
		if (requiredSCV > maxSCVCount) requiredSCV = maxSCVCount;
		if (requiredSCV < minSCVCount) requiredSCV = minSCVCount;

		if (t->scvSet.size() < requiredSCV) {
			MyUnit* scv = NULL;
			scv = info->map->getNearTerritory(info->map->getMyTerritorys().back()->getTilePosition())->getWorker(false);
			if (scv != NULL)
				UnitControl::getInstance()->returnUnit(scv);
		}

		printf("requiredscv : %d || scvSet Size : %d\n", requiredSCV, t->scvSet.size());

		if (!waitSet) {
			printf("밀어내기 준비중\n");
			bool check = true;
			for (auto marine : t->marineSet) {
				float dist = getDistance(marine->getPosition(), waitPos);
				if (dist > waitZone) {
					check = false;
				}

				if (dist > holdZone) {
					marine->self->move(waitPos, false);
				}
				else if (marine->self->isHoldingPosition() == false) {
					marine->self->holdPosition(false);
				}
			}
			int scvCount = 0;
			for (auto scv : t->scvSet) {
				if (scvCount < 2) {
					if (t->tankSet.size() > 0) {
						if (scv->self->isRepairing() == false) {
							scv->self->repair(t->tankSet.front()->self, false);
						}
					}
					else {
						Unit rtarget = getRepairUnit(scv);
						if (rtarget != NULL) {
							if (scv->self->isRepairing() == false) {
								scv->self->repair(rtarget, false);
							}
						}
						else if (canAttack(scv)) {
							scv->self->attack(info->map->getMyTerritorys().back()->getCloseEnemy().front()->getPosition(), false);
						}
					}
				}
				else {
					float dist = getDistance(scv->getPosition(), waitSCVPos);
					Unit rtarget = getRepairUnit(scv);

					if (dist > waitZone) {
						check = false;
					}
					if (dist > holdZone) {
						scv->self->move(waitSCVPos, false);
					}
					else if (rtarget != NULL) {
						if (scv->self->isRepairing() == false) {
							scv->self->repair(rtarget, false);
						}
					}
					else if (scv->self->isHoldingPosition() == false) {
						scv->self->holdPosition(false);
					}
				}
				scvCount++;
			}
			int tankIndex = 0;
			for (auto tank : t->tankSet) {
				if (tankIndex == 0) {
					float dist = getDistance(tank->getPosition(), (Position)defenseTankPos[0]);
					if (dist < 20) {
						if (tank->self->canSiege()) {
							tank->self->siege();
						}
					}
					else {
						if (dist > 40 && tank->self->isSieged()) {
							tank->self->unsiege();
						}
						Unit target = getTargetUnit(tank, enemy);
						if (target == NULL) {
							tank->self->move((Position)defenseTankPos[0], false);
						}
						else {
							float tdist = getDistance((Position)defenseTankPos[0], tank->getPosition());

							if (tdist > 20) {
								tank->self->move((Position)defenseTankPos[0], false);
							}
							else if (tank->self->getGroundWeaponCooldown() == 0) {
								tank->self->attack(target, false);
							}
							else {
								tank->self->move((Position)defenseTankPos[0], false);
							}
						}
					}
				}
				else {
					float dist = getDistance(tank->getPosition(), waitPos);
					if (dist > waitZone) {
						check = false;
					}
					if (dist > holdZone) {
						if (tank->self->isSieged()) {
							tank->self->unsiege();
						}
						else {
							tank->self->move(waitPos, false);
						}
					}
					else if (tank->self->isHoldingPosition() == false) {
						tank->self->holdPosition(false);
					}
				}
				tankIndex++;
			}
			if (check && t->tankSet.size() > 1) waitSet = true;
		}
		else {
			printf("밀어내기으아아아아아아아아\n");
			for (auto marine : t->marineSet) {
				Unit target = getTargetUnit(marine, enemy);
				if (target == NULL) {
					if (info->map->getMyTerritorys().back()->getCloseEnemy().size() > 0) {
						Unit target2 = NULL;
						for (auto eu : info->map->getMyTerritorys().back()->getCloseEnemy()) {
							if (eu->getType() == UnitTypes::Protoss_Observer) continue;
							else {
								target2 = eu;
								break;
							}
						}
						if (target2 == NULL) {
							if (gameFrame % 12 == 0) {
								marine->self->move(ma, false);
								if (getDistance(marine->getPosition(), (Position)ma) < 80) {
									marine->isInTeam = true;
								}
							}
						}
						else {
							marine->self->move(target2->getPosition(), false);
						}
					}
					else {
						marine->self->move(ma, false);
					}
				}
				else {
					float tmdist = getDistance(target->getPosition(), marine->getPosition());
					if (tmdist < 40 && target->getType() == UnitTypes::Protoss_Zealot) {
						marine->self->move(info->map->getMyStartBase()->getPosition(), false);
					}
					else if (canAttack(marine)) {
						marine->self->attack(target);
					}
				}
			}

			for (auto medic : t->medicSet) {
				Unit healTarget = getHealUnit(medic, t);
				if (healTarget == NULL) {
					if (gameFrame % 48 == 0) {
						medic->self->move((Position)ma2, false);
					}
				}
				else {
					if (gameFrame % 24 == 0)
						medic->self->useTech(TechTypes::Healing, healTarget);
				}
				joiningTeam(medic, t);
			}

			int scvCount = 0;

			for (auto scv : t->scvSet) {

				Unit rTarget = getRepairUnit(scv);
				if (canAttack(scv)) {
					scv->self->attack(info->map->getMyTerritorys().back()->getCloseEnemy().front()->getPosition(), false);
				}
				else if (info->map->getMyTerritorys().front()->getCloseEnemy().size() == 0 && info->map->getMyTerritorys().back()->getCloseEnemy().size() == 0) {
					
					if (scv->self->isRepairing() == false) {
						scv->self->repair(rTarget, false);
					}

					if (rTarget == NULL) {
						isClearing = true;
					}
				}
				scvCount++;
			}

			int tankIndex = 0;
			for (auto tank : t->tankSet) {
				if (tankIndex == 0) {
					float dist = getDistance(tank->getPosition(), (Position)defenseTankPos[0]);
					if (dist < 20) {
						if (tank->self->canSiege()) {
							tank->self->siege();
						}
					}
					else {
						Unit target = getTargetUnit(tank, enemy);
						if (target == NULL) {
							tank->self->move((Position)defenseTankPos[0], false);
						}
						else {
							if (tank->self->getGroundWeaponCooldown() == 0) {
								tank->self->attack(target, false);
							}
							else {
								tank->self->move((Position)defenseTankPos[0], false);
							}
						}
					}
				}
				else {
					Unit target = getTargetUnit(tank, enemy);
					float tttdist = getDistance((Position)waitTankPos, tank->self->getPosition());
					if (target == NULL) {
						if (tank->self->isSieged() && tttdist > waitZone) {
							tank->self->unsiege();
						}
						else if (info->map->getMyTerritorys().back()->getCloseEnemy().size() > 0) {
							tank->self->attack(info->map->getMyTerritorys().back()->getCloseEnemy().front()->getPosition(), false);
						}
					}
					else {
						float dist = getDistance(target->getPosition(), tank->getPosition());

						if (tank->self->isSieged()){
							if (tank->self->getGroundWeaponCooldown() == 0) {
								tank->self->attack(target, false);
							}
							else if (target->getType() == UnitTypes::Protoss_Zealot) {
								tank->self->unsiege();
							}
							else{}
						}
						else {
							if (tank->self->canSiege() && target->getType() == UnitTypes::Protoss_Dragoon) {
								if (dist > UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().minRange() && dist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
									tank->self->siege();
								}
							}
							else if (tank->self->getGroundWeaponCooldown() == 0) {
								tank->self->attack(target, false);
							}
							else {
								if (dist > UnitTypes::Terran_Siege_Tank_Tank_Mode.groundWeapon().maxRange()) {
									tank->self->move(target->getPosition(), false);
								}
								else if (dist < 40) {
									tank->self->move(info->map->getMyStartBase()->getPosition(), false);
								}
								else {
									if (tank->self->isHoldingPosition() == false)
										tank->self->holdPosition();
								}
							}
						}
					}
				}
				tankIndex++;
			}
		}
		return;
	}

	printf("세이프~~~~~\n");

	int maxSCVCount = 8;
	int minSCVCount = 2;

	int requiredSCV = (ev - (int)t->tankSet.size() * 4 - (int)t->marineSet.size()) / 2;
	if (requiredSCV > maxSCVCount) requiredSCV = maxSCVCount;
	if (requiredSCV < minSCVCount) requiredSCV = minSCVCount;

	if (t->scvSet.size() < requiredSCV) {
		MyUnit* scv = NULL;
		scv = info->map->getNearTerritory(info->map->getMyStartBase()->getTilePosition())->getWorker(false);
		if (scv != NULL)
			UnitControl::getInstance()->returnUnit(scv);
	}
	else if (isClearing) {
		int count = 0;
		for (auto scv : t->scvSet) {
			if (count >= t->scvSet.size() - requiredSCV) break;
			scv->state = MyUnit::STATE::IDLE;
			UnitControl::getInstance()->onUnitDestroy(scv->self);
			count++;
		}
		isClearing = false;
	}

	int scvCount = 0;
	for (auto scv : t->scvSet) {
		
		if (info->map->getMyTerritorys().back()->getCloseEnemy().size() > 0){
			if (canAttack(scv)) 
				scv->self->attack(info->map->getMyTerritorys().back()->getCloseEnemy().front()->getPosition(), false);
		}
		else if (info->map->getMyTerritorys().front()->getCloseEnemy().size() == 0 && info->map->getMyTerritorys().back()->getCloseEnemy().size() == 0) {
			Unit rTarget = getRepairUnit(scv);
			if (scv->self->isRepairing() == false) {
				scv->self->repair(rTarget, false);
			}

			if (rTarget == NULL) {
				isClearing = true;
				scv->state = MyUnit::STATE::IDLE;
				UnitControl::getInstance()->onUnitDestroy(scv->self);
			}
		}
		scvCount++;
	}

	if (info->enemy->enemyGroup.size() > 0) {
		Position ap;
		float minDist = 9999.9f;
		bool changePos = false;
		for (auto q = info->enemy->enemyGroup.begin(); q != info->enemy->enemyGroup.end(); q++) {
			bool check = false;
			for (auto terr : info->map->getMyTerritorys()) {
				if (BWTA::getRegion((*q).first) == BWTA::getRegion(terr->getBase()->getPosition())) {
					check = true;
				}
			}
			if (!check) continue;
			float dist = getDistance(t->tankCenterPos, (*q).first);
			if (minDist > dist) {
				minDist = dist;
				changePos = true;
				ap = (*q).first;
			}
		}
		if (changePos) {
			ma = ap;
			mt = ap;
			ma2 = ap;
		}
	}

	int marineCount = 0;
	bool scrab = false;
	vector<Position> mPos;
	if (info->enemy->getUnitCount(UnitTypes::Protoss_Reaver) > 0) {
		if (info->enemy->getUnits(UnitTypes::Protoss_Reaver).front()->isAttackFrame()) {
			scrab = true;
			mPos = getVGPosition(t->bionicCenterPos, info->enemy->getUnits(UnitTypes::Protoss_Scarab).front()->getPosition(), t->marineSet.size(), 2, 50, 50);
		}
	}
	for (auto marine : t->marineSet) {
		if (scrab) {
			if (mPos.size() > 0) {
				marine->self->move(mPos.back(), false);
				mPos.pop_back();
			}
			else {
				marine->self->move(info->map->getMyTerritorys().back()->getPosition(), false);
			}
		}
		if (marineCount < 4) {
			Unit target = getTargetUnit(marine, enemy);
			if (target == NULL) {
				if (gameFrame % 12 == 0) {
					marine->self->move(ma, false);
					if (getDistance(marine->getPosition(), (Position)ma) < 80) {
						marine->isInTeam = true;
					}
				}
			}
			else {
				if (target->getType() == UnitTypes::Protoss_Zealot && getDistance(target->getPosition(), marine->getPosition()) < 40) {
					marine->self->move(info->map->getMyStartBase()->getPosition(), false);
				}
				else if (canAttack(marine)) {
					marine->self->attack(target, false);
				}
			}
		}
		else {
			marine->isInTeam = true;
			Unit target = getTargetUnit(marine, enemy);
			if (target == NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 48 == 0)
					marine->self->move(ma2, false);
			}
			else {
				float dist = getDistance(marine->getPosition(), target->getPosition());
				if (dist < UnitTypes::Terran_Marine.groundWeapon().maxRange()) {
					if (target->getType() == UnitTypes::Protoss_Zealot && getDistance(target->getPosition(), marine->getPosition()) < 40) {
						marine->self->move(info->map->getMyStartBase()->getPosition(), false);
					}
					else if (canAttack(marine)) {
						marine->self->attack(target, false);
					}
				}
				else {
					if (canAttack(marine)) {
						marine->self->attack(target, false);
					}
				}
			}
		}
		marineCount++;
		joiningTeam(marine, t);
	}

	for (auto medic : t->medicSet) {
		Unit healTarget = getHealUnit(medic, t);
		if (healTarget == NULL) {
			if (gameFrame % 48 == 0) {
				medic->self->move((Position)ma2, false);
			}
		}
		else {
			if (gameFrame % 24 == 0)
				medic->self->useTech(TechTypes::Healing, healTarget);
		}
		joiningTeam(medic, t);
	}

	int tankIndex = 0;
	for (auto tank : t->tankSet) {
		Unit target = getTargetUnit(tank, enemy);
		if (target == NULL || BWAPI::Broodwar->self()->hasResearched(TechTypes::Tank_Siege_Mode)) {
			if ((tankIndex < 2 && info->enemy->enemyGroup.size() < 1) || (tankIndex < 1 && info->enemy->enemyGroup.size() > 0)) {
				float dist = getDistance(tank->getPosition(), (Position)defenseTankPos[0]);
				if (tank->self->isSieged() == false) {
					if (dist < tankIndex * 40 + 30) {
						if (target != NULL) {
							if (tank->self->getGroundWeaponCooldown() == 0) {
								tank->self->attack(target, false);
							}
							else {
								if (gameFrame % 6 == 0) {
									tank->self->move(info->map->getMyStartBase()->getPosition(), false);
								}
							}
						}
						else {
							tank->self->siege();
						}
					}
					else {
						if (target != NULL) {
							if (isOnlyZealot(t)) {
								if (tank->self->getGroundWeaponCooldown() == 0) {
									tank->self->attack(target, false);
								}
								else {
									if (gameFrame % 6 == 0) {
										tank->self->move(info->map->getMyStartBase()->getPosition(), false);
									}
								}
							}
							else {
								float udist = getDistance(target->getPosition(), tank->getPosition());
								if (udist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
									tank->self->siege();
								}
								else if (gameFrame % 24 == 0) {
									tank->self->move(target->getPosition(), false);
								}
							}
						}
						else {
							if (gameFrame % 24 == 0) {
								tank->self->move((Position)defenseTankPos[0], false);
							}
						}
					}
				}
				else {
					target = getMassedUnit(tank, enemy);
					if (target != NULL) {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
						}
					}
					else {
						if (dist > tankIndex * 40 + 30) {
							tank->self->unsiege();
						}
					}
				}
			}
			else {
				if (tank->self->isSieged() == false) {
					float dist = getDistance(tank->getPosition(), mt);
					if (dist < 60) {
						if (target != NULL) {
							if (tank->self->getGroundWeaponCooldown() == 0) {
								tank->self->attack(target, false);
							}
							else {
								if (gameFrame % 6 == 0) {
									tank->self->move(info->map->getMyStartBase()->getPosition(), false);
								}
							}
						}
						else {
							waitSet = true;
							if(info->enemy->getUnitCount(UnitTypes::Protoss_Shuttle) < 1)
								tank->self->siege();
						}
					}
					else {
						if (target != NULL) {
							if (isOnlyZealot(t)) {
								float ttdist = getDistance(target->getPosition(), tank->getPosition());
								if (tank->self->getGroundWeaponCooldown() == 0) {
									tank->self->attack(target, false);
								}
								else if (ttdist > UnitTypes::Terran_Siege_Tank_Tank_Mode.groundWeapon().maxRange()) {
									if (tank->self->isHoldingPosition() == false) {
										tank->self->holdPosition(false);
									}
								}
								else {
									tank->self->move(info->map->getMyStartBase()->getPosition(), false);
								}
							}
							else {
								float udist = getDistance(target->getPosition(), tank->getPosition());
								if (udist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() && target->getType() == UnitTypes::Protoss_Dragoon) {
									tank->self->siege();
								}
								else if (udist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
									if (tank->self->getGroundWeaponCooldown() == 0) {
										tank->self->attack(target, false);
									}
									else {
										if (tank->self->isHoldingPosition() == false) {
											tank->self->holdPosition(false);
										}
									}
								}
								else if (gameFrame % 24 == 0) {
									tank->self->move(target->getPosition(), false);
								}
							}
						}
						else {
							if (gameFrame % 24 == 0) {
								tank->self->move(mt, false);
							}
						}
					}
				}
				else {
					target = getMassedUnit(tank, enemy);
					if (target != NULL) {
						float dist = getDistance(target->getPosition(), tank->getPosition());
						if (dist > UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
							tank->self->unsiege();
						}
						else if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
						}
					}
					else {
						if (info->enemy->enemyGroup.size() > 0) {
							bool check = false;
							for (auto eg = info->enemy->enemyGroup.begin(); eg != info->enemy->enemyGroup.end(); eg++) {
								if (BWTA::getRegion(eg->first) == BWTA::getRegion(info->map->getMyTerritorys().front()->getPosition()) || 
									BWTA::getRegion(eg->first) == BWTA::getRegion(info->map->getMyTerritorys().back()->getPosition())){
									tank->self->unsiege();
								}
							}
						}
					}
				}
			}
		}
		else {
			if (tank->self->getGroundWeaponCooldown() == 0) {
				tank->self->attack(target, false);
			}
			else {
				tank->self->move(info->map->getMyStartBase()->getPosition(), false);
			}
		}
		tankIndex++;
		joiningTeam(tank, t);
	}

	if (t->team == 0) {
		if (tankIndex > 5 && marineCount > 9) {
			t->state = TeamControl::STATE::BCN_GOING;
			t->goingDariSetting = false;
			t->goingSetting = false;
		}
	}
	else if (t->team == 1) {
		if (tankIndex > 2 || marineCount > 12) {
			t->state = TeamControl::STATE::BCN_GOING;
			t->goingDariSetting = false;
			t->goingSetting = false;
		}
	}
}

void CombatControl::onGOING(TeamControl* t) {

	Unitset enemy = getNearEnemyUnits(t);

	/*int scvCount = 0;

	if (t->scvSet.size() < 3) {
		MyUnit* scv = NULL;

		scv = info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BUNKER))->getWorker(true);

		if (scv != NULL)
			t->scvSet.push_back(scv);
	}
	for (auto scv : t->scvSet) {
		if (scvCount > 2) {
			info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BUNKER))->addScv(scv);
			UnitControl::getInstance()->onUnitDestroy(scv->self);
		}
		else {
			Unit repairTarget = getRepairUnit(scv);

			if (repairTarget != NULL) {
				if (scv->self->isRepairing() == false) {
					scv->self->repair(repairTarget, false);
				}
			}
			else {
				scv->self->move(t->teamCenterPos, false);
			}
		}
		scvCount++;
		scv->targetPos = TilePosition(-1, -1);
	}

	for (auto bunker : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		if (bunker->self->getLoadedUnits().size() > 0) {
			bunker->self->unloadAll();
		}
	}*/


	Position targetPos;
	Position goPos;

	if (t->goingSetting) {
		goPos = (Position)tightPos;
		targetPos = info->map->getEnemyStartBase()->getPosition();
	}
	else if(t->goingDariSetting) {
		goPos = (Position)middleGoPos;
		targetPos = (Position)middlePos;

		float tdist = getDistance(t->teamCenterPos, targetPos);
		if (tdist < 100) {
			t->goingSetting = true;
		}
	}
	else {
		goPos = (Position)DefensePos[1];
		targetPos = (Position)DefensePos[1];

		int mc = 0;
		int tc = 0;
		for (auto mm : t->marineSet) {
			float dist = getDistance(targetPos, mm->getPosition());
			if (dist < 100) {
				mc++;
			}
		}
		for (auto mm : t->tankSet) {
			float dist = getDistance(targetPos, mm->getPosition());
			if (dist < 100) {
				tc++;
			}
		}

		float tdist = getDistance(t->teamCenterPos, targetPos);
		if (t->team == 0) {
			if (tdist < 100 && tc > 6 && mc > 6) {
				t->goingDariSetting = true;
			}
		}
		else {
			if (tdist < 100 && tc > 2 || mc > 12) {
				t->goingDariSetting = true;
			}
		}
	}

	Position bionicPos;
	vector<Position> bPos;
	if (info->enemy->enemyGroup.size() > 0) {
		float minDist = 99999;
		Position p;
		for (auto ep = info->enemy->enemyGroup.begin(); ep != info->enemy->enemyGroup.end(); ep++) {
			float dist = getDistance(ep->first, t->teamCenterPos);
			if (dist < minDist) {
				minDist = dist;
				p = ep->first;
			}
		}

		bPos = getVGPosition(t->tankCenterPos, p, 1, 2, 70.f, 50);
	}
	else {
		bPos = getVGPosition(t->tankCenterPos, goPos, 1, 2, 70.f, 50);
	}

	printf("bpos size %d\n", bPos.size());

	/*int marineCount = t->marineSet.size();
	int team1 = marineCount / 2;
	int team2 = marineCount - team1;
	vector<Position> pos = getVGPosition(t->tankCenterPos, (Position)tightPos, team1, 2, 150.0f, UnitTypes::Terran_Marine.width()*1.5f);
	vector<Position> pos3 = getVGPosition(t->tankCenterPos, (Position)tightPos, team2, 2, 100.0f, UnitTypes::Terran_Marine.width()*1.5f);

	vector<Position> mPos;
	mPos.clear();
	for (auto p : pos) {
		mPos.push_back(p);
	}
	for (auto p : pos3) {
		mPos.push_back(p);
	}*/

	/*if (t->team == 0 && t->marineSet.size() > 0) {
		for (auto marine : t->marineSet) {
			Unit target = getTargetUnit(marine, enemy);
			float dist = 0;
			if (mPos.size() > 0)
				dist = getDistance(mPos.back(), marine->getPosition());

			if (target == NULL) {
				if (gameFrame % 12 == 0) {
					if (mPos.size() > 0)
						marine->self->move(mPos.back(), false);
					else
						marine->self->move(goPos, false);
				}
			}
			else {
				if (dist > 80 && marine->isInTeam) {
					if (gameFrame % 12 == 0) {
						if (mPos.size() > 0)
							marine->self->move(mPos.back(), false);
						else
							marine->self->move(goPos, false);
					}
				}
				else if (isCanBuySteamPack(marine)) {
					marine->self->useTech(TechTypes::Stim_Packs, false);
				}
				else if (marine->self->isAttacking() == false && gameFrame % 48 == 0) {
					if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
						marine->self->attack(target, false);
					}
				}
			}
			joiningTeam(marine, t);
			mPos.pop_back();
		}


		for (auto medic : t->medicSet) {
			Unit healTarget = getHealUnit(medic, t);
			if (healTarget == NULL) {
				if (t->bionicCenterPos.x > 0) {
					medic->self->move(t->bionicCenterPos, false);
				}
				else if (t->teamCenterPos.x > 0) {
					medic->self->move(t->teamCenterPos, false);
				}
				else {
					medic->self->move(goPos, false);
				}
			}
			else {
				if (gameFrame % 24 == 0)
					medic->self->useTech(TechTypes::Healing, healTarget);
			}
			joiningTeam(medic, t);
		}
	}
	else {*/

	
	/*for (auto marine : t->marineSet) {
		marine->maginNumber++;
		Unit target = getTargetUnit(marine, enemy);
		int ts = t->tankSet.size();
		float dist = getDistance(t->tankCenterPos, marine->getPosition());

		if (target == NULL) {
			if (gameFrame % 12 == 0) {
				marine->self->move(goPos, false);
			}
		}
		else {
			if (isCanBuySteamPack(marine)) {
				marine->self->useTech(TechTypes::Stim_Packs, false);
			}
			else if (marine->self->isAttacking() == false && gameFrame % 48 == 0) {
				if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
					marine->self->attack(target, false);
				}
			}
		}
		joiningTeam(marine, t);
	}


	for (auto medic : t->medicSet) {
		Unit healTarget = getHealUnit(medic, t);
		if (healTarget == NULL) {
			if (t->bionicCenterPos.x > 0) {
				medic->self->move(t->bionicCenterPos, false);
			}
			else {
				medic->self->move(goPos, false);
			}
		}
		else {
			if (gameFrame % 24 == 0)
				medic->self->useTech(TechTypes::Healing, healTarget);
		}
		joiningTeam(medic, t);
	}
	

	int tankIndex = 0;
	for (auto tank : t->tankSet) {
		if (tank->self->isSieged()) {
			tank->maginNumber++;
			Unit target = getMassedUnit(tank, enemy);
			if (target == NULL) {
				if (tank->maginNumber > 12) {
					tank->self->unsiege();
				}
			}
			else {
				if (isOnlyZealot(t) && tank->maginNumber > 48) {
					tank->self->unsiege();
				}
				else {
					if (tank->self->getGroundWeaponCooldown() == 0) {
						tank->self->attack(target, false);
					}
				}
			}
		
		}
		else {
			tank->maginNumber = 0;
			Unit target = getTargetUnit(tank, enemy);
			if (target == NULL)
			{
				if (gameFrame % 5 == 0) {
					tank->self->move(goPos, false);
				}
			}
			else {
				if (isOnlyZealot(t)) {
					if (tank->self->getGroundWeaponCooldown() == 0) {
						tank->self->attack(target, false);
					}
					else {
						tank->self->move(info->map->getMyStartBase()->getPosition(), false);
					}
				}
				else {
					if (tank->self->canSiege()) {
						tank->self->siege();
					}
				}
			}
		}
		joiningTeam(tank, t);
	}
	*/

	if(enemy.size() > 0){
		for (auto marine : t->marineSet) {
			marine->maginNumber++;
			Unit target = getTargetUnit(marine, enemy);
			if ((isAlone(marine, t) && marine->maginNumber > 48 && marine->isInTeam)) {
				marine->maginNumber = 0;
				marine->self->move(t->tankCenterPos, false);
			}
			else if (isOnlyZealot(t) && marine->self->getHitPoints() < UnitTypes::Terran_Marine.maxHitPoints() / 3) {
				marine->self->move(info->map->getMyStartBase()->getPosition(), false);
			}
			else if (target == NULL) {
				if (info->enemy->enemyGroup.size() > 0 && bPos.size() > 0) {
					Position enemyPos = getNearestEnemyGroup(marine);
					float euDist = getDistance(enemyPos, marine->getPosition());
					float etDist = getDistance(bPos.front(), enemyPos);
					if (euDist < etDist && marine->maginNumber > 48) {
						marine->maginNumber = 0;
						marine->self->move(t->tankCenterPos, false);
					}
					else if(euDist > etDist){
						if (gameFrame % 48 == 0) {
							marine->self->attack(enemyPos, false);
						}
					}
				}
				else {
					if (isAlone(marine, t) && marine->isInTeam == false && marine->maginNumber > 48) {
						marine->maginNumber = 0;
						marine->self->move(t->tankCenterPos, false);
					}
					else if(marine->isInTeam == false && isAlone(marine, t) == false){
						marine->isInTeam = true;
					}
				}
			}
			else {
				if (isCanBuySteamPack(marine)) {
					marine->self->useTech(TechTypes::Stim_Packs, false);
				}
				else if (marine->self->isAttacking() == false) {
					marine->self->attack(target, false);
				}
			}
		}
		for (auto medic : t->medicSet) {
			medic->maginNumber++;
			Unit target = getHealUnit(medic, t);
			if (target != NULL) {
				float d = getDistance(target->getPosition(), medic->getPosition());
				if (d < 20) {
					if(medic->self->isIdle())
						medic->self->useTech(TechTypes::Healing, false);
				}
				else {
					if(gameFrame % 6 == 0)
						medic->self->move(target->getPosition(), false);
				}
			}
			else {
				if (bPos.size() > 0)
					medic->self->move(bPos.front(), false);
				else {
					Position mp = getNearestMarinePos(medic, t);
					if (mp.x > 0) {
						if (gameFrame % 12 == 0) {
							medic->self->move(mp, false);
						}
					}
					else {
						if (gameFrame % 12 == 0) {
							medic->self->move(t->tankCenterPos, false);
						}
					}
				}

			}
			joiningTeam(medic, t);
		}
		for (auto tank : t->tankSet) {
			tank->maginNumber++;
			if (tank->self->isSieged()) {
				Unit target = getMassedUnit(tank, enemy);
				if (target == NULL) {
					target = getTargetBuilding(tank);
					if (target == NULL) {
						if (tank->maginNumber > 48) {
							tank->self->unsiege();
						}
					}
					else {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
						}
					}
				}
				else {
					if (isOnlyZealot(t) && tank->maginNumber > 48) {
						tank->self->unsiege();
					}
					else {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
						}
					}
				}

			}
			else {
				tank->maginNumber = 0;
				Unit target = getTargetUnit(tank, enemy);
				if (target == NULL)
				{
					target = getTargetBuilding(tank);
					if (target == NULL) {
						if (gameFrame % 20 == 0) {
							tank->self->move(goPos, false);
						}
					}
					else {
						if (getNearestChokePointDistance(tank) < 150) {
							if (gameFrame % 6 == 0) {
								tank->self->move(info->map->getEnemyStartBase()->getPosition(), false);
							}
						}
						else if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
				}
				else {
					if (isOnlyZealot(t)) {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							tank->self->attack(target, false);
						}
						else {
							tank->self->move(info->map->getMyStartBase()->getPosition(), false);
						}
					}
					else {
						if (getNearestChokePointDistance(tank) < 150) {
							if (gameFrame % 6 == 0) {
								tank->self->move(info->map->getEnemyStartBase()->getPosition(), false);
							}
						}
						else {
							if (tank->self->canSiege()) {
								tank->self->siege();
							}
						}
					}
				}
			}
			joiningTeam(tank, t);
		}
	}
	else {
		for (auto marine : t->marineSet) {
			marine->maginNumber++;
			if (gameFrame % 24 == 0) {
				marine->self->move(goPos, false);
			}
		}
		for (auto medic : t->medicSet) {
			medic->maginNumber++;
			if (gameFrame % 24 == 0) {
				medic->self->move(goPos, false);
			}
		}
		for (auto tank : t->tankSet) {
			tank->maginNumber++;
			if (tank->self->isSieged() && tank->maginNumber > 48) {
				tank->self->unsiege();
			}
			else if(tank->self->isSieged() == false) {
				if (gameFrame % 12 == 0) {
					tank->self->move(goPos, false);
				}
			}
		}
	}
}

void CombatControl::onTIGHT(TeamControl* t) {
	
	
	
}

float CombatControl::getNearestTankDistance(MyUnit* unit) {

	float ret = 9999;

	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
		if (tank->isInTeam == false) continue;
		float dist = getDistance(tank->getPosition(), unit->getPosition());
		if (ret > dist) {
			ret = dist;
		}
	}

	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode)) {
		if (tank->isInTeam == false) continue;
		float dist = getDistance(tank->getPosition(), unit->getPosition());
		if (ret > dist) {
			ret = dist;
		}
	}

	return ret;
}

float CombatControl::getNearestChokePointDistance(MyUnit* unit) {

	float ret = 9999;

	for (auto c : BWTA::getChokepoints()) {
		if (c->getWidth() > 120) continue;
		float dist = getDistance(c->getCenter(), unit->getPosition());
		if (ret > dist) {
			ret = dist;
		}
	}

	return ret;
}

bool CombatControl::isCanBuySteamPack(MyUnit* unit) {
	int hp = unit->self->getHitPoints();
	int maxHp = unit->getType().maxHitPoints();

	if (maxHp / 2 > hp) {
		return false;
	}
	
	if (unit->self->isStimmed()) return false;
	if (isEnemyUnitComeTrue(unit)) return false;

	return true;
}

void CombatControl::onJOIN(TeamControl* t) {

	Unitset enemy = getNearEnemyUnits(t);

	for (auto marine : t->marineSet) {
		Unit target = getTargetUnit(marine, enemy);
		if (target == NULL) {
			if (BWAPI::Broodwar->getFrameCount() % 24 == 0)
				marine->self->move((Position)DefensePos[1], false);
		}
		else {
			if (marine->self->isAttackFrame() == false) {
				if (BWAPI::Broodwar->getFrameCount() % 12 == 0)
					marine->self->attack(target, false);
			}
		}
	}

	for (auto medic : t->medicSet) {
		Unit healTarget = getHealUnit(medic, t);
		if (healTarget == NULL) {
			if (gameFrame % 48 == 0) {
				medic->self->move((Position)DefensePos[1], false);
			}
		}
		else {
			if (gameFrame % 24 == 0)
				medic->self->useTech(TechTypes::Healing, healTarget);
		}
	}

	int tankIndex = 0;
	for (auto tank : t->tankSet) {
		Unit target = getTargetUnit(tank, enemy);
		if (tank->self->isSieged()) {
			tank->self->unsiege();
		}
		else {
			if (target != NULL) {
				if (tank->self->getGroundWeaponCooldown() == 0) {
					tank->self->attack(target, false);
				}
				else {
					if (tank->self->isSieged() == false) {
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
						tank->self->move(p, false);
					}
				}
			}
			else {
				if (gameFrame % 20 == 0) {
					tank->self->move((Position)DefensePos[1], false);
				}
			}
		}
	}

	int mr = 0;
	int tr = 0;

	for (auto marine : t->marineSet) {
		float dist = getDistance(marine->getPosition(), (Position)DefensePos[1]);
		if (dist < 60) {
			marine->isInTeam = true;
			mr++;
		}
	}

	for (auto medic : t->medicSet) {
		float dist = getDistance(medic->getPosition(), (Position)DefensePos[1]);
		if (dist < 60) {
			medic->isInTeam = true;
			mr++;
		}
	}

	for (auto tank : t->tankSet) {
		float dist = getDistance(tank->getPosition(), (Position)DefensePos[1]);
		if (dist < 60) {
			tank->isInTeam = true;
			tr++;
		}
	}

	if (tr > 0 && mr > 8) {
		t->state = TeamControl::STATE::BCN_GOING;
		t->goingSetting = true;

		UnitControl::getInstance()->TCV.push_back(new TeamControl(UnitControl::getInstance()->totalTeamIndex++));
		UnitControl::getInstance()->TCV[UnitControl::getInstance()->totalTeamIndex - 1]->state = TeamControl::STATE::BCN_JOIN;
	}
}

void CombatControl::onBJ(TeamControl* t) {

}

void CombatControl::onFINISH(TeamControl* t) {

	Unitset enemy = getNearEnemyUnits(t);

	Position goPos = t->attackPosition;

	/*int marineCount = t->marineSet.size();
	int team1 = marineCount / 2;
	int team2 = marineCount - team1;
	vector<Position> pos = getVGPosition(t->tankCenterPos, (Position)tightPos, team1, 2, 150.0f, UnitTypes::Terran_Marine.width()*1.5f);
	vector<Position> pos3 = getVGPosition(t->tankCenterPos, (Position)tightPos, team2, 2, 100.0f, UnitTypes::Terran_Marine.width()*1.5f);

	vector<Position> mPos;

	for (auto p : pos) {
	mPos.push_back(p);
	}
	for (auto p : pos3) {
	mPos.push_back(p);
	}*/



	for (auto marine : t->marineSet) {
		Unit target = getTargetUnit(marine, enemy);
		if (target == NULL) {
			target = getTargetBuilding(marine);
			if (target == NULL) {
				if (gameFrame % 12 == 0) {
					marine->self->move(goPos, false);
				}
			}
			else {
				if (marine->self->isAttacking() == false) {
					if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
						marine->self->attack(target, false);
					}
				}
			}
		}
		else {
			int ts = t->tankSet.size();
			float dist = getDistance(t->tankCenterPos, marine->getPosition());
			if (!marine->isInTeam) {
				if (gameFrame % 12 == 0) {
					marine->self->move(goPos, false);
				}
			}
			else if (isCanBuySteamPack(marine)) {
				marine->self->useTech(TechTypes::Stim_Packs, false);
			}
			else if (marine->self->isAttacking() == false && gameFrame % 48 == 0) {
				if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
					marine->self->attack(target, false);
				}
			}
		}
		joiningTeam(marine, t);
	}


	for (auto medic : t->medicSet) {
		Unit healTarget = getHealUnit(medic, t);
		if (healTarget == NULL) {
			if (t->bionicCenterPos.x > 0) {
				medic->self->move(t->bionicCenterPos, false);
			}
			else if (t->teamCenterPos.x > 0) {
				medic->self->move(t->teamCenterPos, false);
			}
			else {
				medic->self->move(goPos, false);
			}
		}
		else {
			if (gameFrame % 24 == 0)
				medic->self->useTech(TechTypes::Healing, healTarget);
		}
		joiningTeam(medic, t);
	}


	int tankIndex = 0;
	for (auto tank : t->tankSet) {
		if (tank->self->isSieged()) {
			tank->maginNumber++;
			Unit target = getMassedUnit(tank, enemy);
			if (target == NULL) {
				target = getTargetBuilding(tank);
				if (target == NULL) {
					if (tank->maginNumber > 96) {
						tank->self->unsiege();
					}
				}
				else {
					if (tank->self->getGroundWeaponCooldown() == 0) {
						tank->self->attack(target, false);
					}
				}
			}
			else {
				if (isOnlyZealot(t) && tank->maginNumber > 48) {
					tank->self->unsiege();
				}
				else {
					if (tank->self->getGroundWeaponCooldown() == 0) {
						tank->self->attack(target, false);
					}
				}
			}

		}
		else {
			tank->maginNumber = 0;
			Unit target = getTargetUnit(tank, enemy);
			if (target == NULL)
			{
				target = getTargetBuilding(tank);
				if (target == NULL) {
					if (gameFrame % 20 == 0) {
						tank->self->move(goPos, false);
					}
				}
				else {
					if (getNearestChokePointDistance(tank) < 150) {
						if (gameFrame % 6 == 0) {
							tank->self->move(info->map->getEnemyStartBase()->getPosition(), false);
						}
					}
					else if (tank->self->isAttacking() == false) {
						tank->self->attack(target, false);
					}
				}
			}
			else {
				if (isOnlyZealot(t)) {
					if (tank->self->getGroundWeaponCooldown() == 0) {
						tank->self->attack(target, false);
					}
					else {
						tank->self->move(info->map->getMyStartBase()->getPosition(), false);
					}
				}
				else {
					if (getNearestChokePointDistance(tank) < 150) {
						if (gameFrame % 6 == 0) {
							tank->self->move(info->map->getEnemyStartBase()->getPosition(), false);
						}
					}
					else {
						if (tank->self->canSiege()) {
							tank->self->siege();
						}
					}
				}
			}
			/*if (tank->self->isSieged()) {
			tank->self->unsiege();
			}
			else {
			if (target != NULL) {
			if (tank->self->getGroundWeaponCooldown() == 0) {
			tank->self->attack(target, false);
			}
			else if (isEnemyUnitComeTrue(tank)) {
			if (tank->self->isSieged() == false) {
			tank->self->move(info->map->getMyStartBase()->getPosition(), false);
			}
			}
			else {
			if (tank->self->isSieged() == false) {
			tank->self->move((Position)tightPos, false);
			}
			}
			}
			else {
			if (gameFrame % 20 == 0) {
			tank->self->move((Position)tightPos, false);
			}
			}
			}*/
		}
		joiningTeam(tank, t);
	}
}



void CombatControl::draw(TeamControl* t) {

	if (enemyChoke != NULL && t->team == 0) {
		if (isUnduck(t)) {
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 50, Colors::Purple, false);
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 100, Colors::Purple, false);
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 150, Colors::Purple, false);
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 200, Colors::Purple, false);
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 250, Colors::Purple, false);
		}
		else {
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 50, Colors::White, false);
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 100, Colors::White, false);
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 150, Colors::White, false);
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 200, Colors::White, false);
			BWAPI::Broodwar->drawCircleMap(enemyChoke->getCenter(), 250, Colors::White, false);
		}
		
		Position goPos;
		if (info->map->getEnemyDirection() == 0) goPos = (Position)TilePosition(2, 29);
		if (info->map->getEnemyDirection() == 1) goPos = (Position)TilePosition(95, 3);
		if (info->map->getEnemyDirection() == 2) goPos = (Position)TilePosition(32, 126);
		if (info->map->getEnemyDirection() == 3) goPos = (Position)TilePosition(125, 99);

		BWAPI::Broodwar->drawCircleMap((Position)undunkTankPos, 180, Colors::Teal, false);
		BWAPI::Broodwar->drawCircleMap((Position)undunkTankPos, 100, Colors::Cyan, false);
		BWAPI::Broodwar->drawCircleMap((Position)undunkTankPos, 150, Colors::Cyan, false);
	}

	for (auto enemy : info->map->getMyTerritorys().back()->getCloseEnemy()) {
		BWAPI::Broodwar->drawCircleMap(enemy->getPosition(), 17, Colors::Red, false);
		BWAPI::Broodwar->drawCircleMap(enemy->getPosition(), 16, Colors::Red, false);
		BWAPI::Broodwar->drawCircleMap(enemy->getPosition(), 15, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap(enemy->getPosition(), 14, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap(enemy->getPosition(), 13, Colors::Yellow, false);
		BWAPI::Broodwar->drawCircleMap(enemy->getPosition(), 12, Colors::Yellow, false);
		BWAPI::Broodwar->drawCircleMap(enemy->getPosition(), 11, Colors::Yellow, false);

	}
	if (t->team == 0) {
		if (t->fTank != NULL) {
			if (t->fTank->self->exists()) {
				BWAPI::Broodwar->drawCircleMap(t->fTank->getPosition(), 18, Colors::Orange, false);
				BWAPI::Broodwar->drawCircleMap(t->fTank->getPosition(), 19, Colors::Orange, false);
				BWAPI::Broodwar->drawCircleMap(t->fTank->getPosition(), 20, Colors::Orange, false);

				BWAPI::Broodwar->drawCircleMap(t->fTank->getPosition(), 100, Colors::Orange, false);
			}
		}

		if (t->fMarine != NULL) {
			BWAPI::Broodwar->drawCircleMap(t->fMarine->getPosition(), 18, Colors::Orange, false);
			BWAPI::Broodwar->drawCircleMap(t->fMarine->getPosition(), 19, Colors::Orange, false);
			BWAPI::Broodwar->drawCircleMap(t->fMarine->getPosition(), 20, Colors::Orange, false);
		}
	}

	for (auto tank : t->tankSet) {
		vector<Unit> canAttackEnemys = getCanAttackEnemy(tank);
		Unit u = getMassedUnit(tank, canAttackEnemys);
		BWAPI::Broodwar->drawCircleMap(tank->getPosition(), UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange(), Colors::Blue, false);
		if (tank->getType() != UnitTypes::Terran_Siege_Tank_Siege_Mode || tank->self->getGroundWeaponCooldown() > 11) continue;
		if (u != NULL) {
			BWAPI::Broodwar->drawLineMap(tank->getPosition(), u->getPosition(), Colors::Red);
			BWAPI::Broodwar->drawCircleMap(u->getPosition(), 30, Colors::Red, false);
			BWAPI::Broodwar->drawCircleMap(u->getPosition(), 20, Colors::Orange, false);
			BWAPI::Broodwar->drawCircleMap(u->getPosition(), 10, Colors::Yellow, false);
			BWAPI::Broodwar->drawCircleMap(u->getPosition(), 2, Colors::Blue, true);

			Position p[4];
			p[0] = Position(u->getPosition().x - 35, u->getPosition().y);
			p[1] = Position(u->getPosition().x + 35, u->getPosition().y);
			p[2] = Position(u->getPosition().x, u->getPosition().y + 35);
			p[3] = Position(u->getPosition().x, u->getPosition().y - 35);

			for (int x = 0; x < 4; x++) {
				BWAPI::Broodwar->drawLineMap(p[x], u->getPosition(), Colors::Red);
			}
		}
	}

	for (auto medic : t->medicSet) {
		Unit target = getHealUnit(medic, t);
		if (target != NULL) {
			BWAPI::Broodwar->drawLineMap(medic->getPosition(), target->getPosition(), Colors::Yellow);
		}
	}

	for (auto marine : t->marineSet) {
		/*if (isEnemyUnitComeTrue(marine)) {
		if (BWAPI::Broodwar->getFrameCount() % 8 == 0) {
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Red, false);
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 9, Colors::Red, false);
		}
		else {
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Black, false);
		BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 9, Colors::White, false);
		}
		}*/

		Unit target = getTargetUnit(marine, getNearEnemyUnits(t));
		if (target != NULL) {
			BWAPI::Broodwar->drawLineMap(marine->getPosition(), target->getPosition(), Colors::Teal);
		}
	}

	for (auto tank : t->tankSet) {
		if (isEnemyUnitComeTrue(tank)) {
			if (BWAPI::Broodwar->getFrameCount() % 8 == 0) {
				BWAPI::Broodwar->drawCircleMap(tank->getPosition(), 10, Colors::Red, false);
				BWAPI::Broodwar->drawCircleMap(tank->getPosition(), 9, Colors::Red, false);
			}
			else {
				BWAPI::Broodwar->drawCircleMap(tank->getPosition(), 10, Colors::Black, false);
				BWAPI::Broodwar->drawCircleMap(tank->getPosition(), 9, Colors::White, false);
			}
		}
	}

	//Unitset enemies = getNearEnemyUnits(t);

	/*int vcnt = 0;
	for (auto v : t->vessleList) {
	if (v->self->getEnergy() >= 100 && v->maginNumber > 48) {
	vcnt++;
	}
	if (v->preEnergy - v->self->getEnergy() > 50) {
	v->maginNumber = 0;
	}
	v->preEnergy = v->self->getEnergy();
	}
	vector<std::pair<Position, float>> vempPos = getEMPPositions(t, vcnt, enemies);
	vector<Position> vp = getVGPosition(t->teamCenterPos, t->attackPosition, t->vessleList.size(), 0, t->tankRage, UnitTypes::Terran_Vulture.width() * 8);

	for (auto q = vempPos.begin(); q != vempPos.end(); q++) {
	BWAPI::Broodwar->drawCircleMap((*q).first, 32 * 3, Colors::White, false);
	}

	for (auto v : t->vessleList) {
	if (vempPos.size() > 0) {
	if (v->self->getEnergy() >= 100 && getDistance(v->getPosition(), vempPos.back().first) < 256 * 2.5f && v->maginNumber > 48) {
	//if (BWAPI::Broodwar->getFrameCount() % 24 == 0) {
	v->self->useTech(TechTypes::EMP_Shockwave, vempPos.back().first);
	vempPos.pop_back();
	}
	}
	else {
	if (t->tankSet.size() > 0) {
	if (vp.size() > 0) {
	Position ppp = MapInformation::getInstance()->getAirNextAvoidMovePos(v->getPosition(), vp.back());
	vp.pop_back();
	v->self->move(ppp);
	}
	else {
	Position ppp = MapInformation::getInstance()->getAirNextAvoidMovePos(v->getPosition(), t->teamCenterPos);
	v->self->move(ppp);
	}
	}
	else {
	Position ppp = MapInformation::getInstance()->getAirNextAvoidMovePos(v->getPosition(), t->teamCenterPos);
	v->self->move(ppp);
	}
	}
	}
	*/

	/*for (auto p = minePos.begin(); p != minePos.end(); p++) {

	if ((*p).second.second == MINE_READY) {
	BWAPI::Broodwar->drawCircleMap((*p).first, 5, Colors::Orange, false);
	}
	else if ((*p).second.second == MINE_DONE) {
	BWAPI::Broodwar->drawCircleMap((*p).first, 5, Colors::Orange, false);
	BWAPI::Broodwar->drawCircleMap((*p).first, 7, Colors::Orange, false);
	}
	else if ((*p).second.first < 0) {
	BWAPI::Broodwar->drawCircleMap((*p).first, 5, Colors::Red, false);
	}
	else {
	BWAPI::Broodwar->drawCircleMap((*p).first, 5, Colors::Green, false);
	}
	}

	vector<Position> mineVec = getMinePos(5);
	for (auto m : mineVec) {
	BWAPI::Broodwar->drawCircleMap((m), 7, Colors::Yellow, false);
	BWAPI::Broodwar->drawCircleMap((m), 5, Colors::Yellow, true);
	}
	*/

	for (auto enemy : info->enemy->getAllUnits()) {
		Position expectUnitPos = enemy->getPosition();

		expectUnitPos.x += enemy->getVelocityX() * 18;
		expectUnitPos.y += enemy->getVelocityY() * 18;

		BWAPI::Broodwar->drawLineMap(enemy->getPosition(), expectUnitPos, Colors::Red);
	}

	if (info->map->getEnemyStartBase() != NULL && t->team == 0) {
		BWAPI::Broodwar->drawCircleMap((Position)DefensePos[0], 10, Colors::Blue, false);
		BWAPI::Broodwar->drawCircleMap((Position)DefensePos[0], 12, Colors::Green, false);

		BWAPI::Broodwar->drawCircleMap((Position)DefensePos[1], 10, Colors::Blue, false);
		BWAPI::Broodwar->drawCircleMap((Position)DefensePos[1], 12, Colors::Cyan, false);

		BWAPI::Broodwar->drawCircleMap((Position)middlePos, 10, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap((Position)middlePos, 12, Colors::Red, false);

		BWAPI::Broodwar->drawCircleMap((Position)middleGoPos, 10, Colors::Blue, false);
		BWAPI::Broodwar->drawCircleMap((Position)middleGoPos, 12, Colors::Cyan, false);

		BWAPI::Broodwar->drawCircleMap((Position)DefensePos[2], 100, Colors::Blue, false);

		BWAPI::Broodwar->drawCircleMap((Position)tightPos, 10, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap((Position)tightPos, 12, Colors::Red, false);

		BWAPI::Broodwar->drawCircleMap((Position)waitTankPos, 10, Colors::Green, false);
		BWAPI::Broodwar->drawCircleMap((Position)waitTankPos, 12, Colors::Teal, false);

		for (int x = 0; x < 1; x++) {
			BWAPI::Broodwar->drawCircleMap((Position)defenseTankPos[x], 10, Colors::Blue, false);
			BWAPI::Broodwar->drawCircleMap((Position)defenseTankPos[x], 11, Colors::Cyan, false);
			BWAPI::Broodwar->drawCircleMap((Position)defenseTankPos[x], 12, Colors::Blue, false);
		}

		/*int maxTankPos = -1;
		Position maxTank;
		for (auto tank : tightTankPos) {
		Position tankPos = (Position)tank;
		BWAPI::Broodwar->drawBoxMap(Position(tankPos.x - UnitTypes::Terran_Siege_Tank_Tank_Mode.width()/2, tankPos.y - UnitTypes::Terran_Siege_Tank_Tank_Mode.height() / 2),
		Position(tankPos.x + UnitTypes::Terran_Siege_Tank_Tank_Mode.width() / 2, tankPos.y + UnitTypes::Terran_Siege_Tank_Tank_Mode.height() / 2), Colors::Blue, false);

		if (maxTankPos < tightValue[tank]) {
		maxTankPos = tightValue[tank];
		maxTank = tankPos;
		}
		}

		if (maxTankPos != -1) {
		BWAPI::Broodwar->drawCircleMap(maxTank, 10, Colors::Purple, true);
		}

		int minBunkerPos = 99999;
		Position minBunker;
		for (auto bunker : tightBunkerPos) {
		Position bunkerPos = (Position)bunker;
		BWAPI::Broodwar->drawBoxMap(bunkerPos,
		Position(bunkerPos.x + 32*3, bunkerPos.y + 32*2), Colors::Teal, false);

		if (tightValue[bunker] < minBunkerPos) {
		minBunker = Position(bunkerPos.x + UnitTypes::Terran_Bunker.width()/2, bunkerPos.y + UnitTypes::Terran_Bunker.height()/2);
		minBunkerPos = tightValue[bunker];
		}
		}

		if (minBunkerPos != 99999) {
		BWAPI::Broodwar->drawCircleMap(minBunker, 10, Colors::Purple, true);
		}*/
	}

	if (t->tankCenterPos.x > 0) {
		int marineCount = t->marineSet.size();
		int team1 = marineCount / 2;
		int team2 = marineCount - team1;
		vector<Position> pos = getVGPosition(t->tankCenterPos, (Position)tightPos, team1, 2, 70.0f, UnitTypes::Terran_Marine.width()*1.5f);
		vector<Position> pos3 = getVGPosition(t->tankCenterPos, (Position)tightPos, team2, 2, 40.0f, UnitTypes::Terran_Marine.width()*1.5f);

		vector<Position> mPos;

		for (auto p : pos) {
			mPos.push_back(p);
		}
		for (auto p : pos3) {
			mPos.push_back(p);
		}
		for (auto p : mPos) {
			BWAPI::Broodwar->drawCircleMap(p, 6, Colors::Green, false);
			BWAPI::Broodwar->drawCircleMap(p, 8, Colors::Teal, false);
		}
	}
}


void CombatControl::onUnitCreate(Unit) {

}

void CombatControl::onUnitDestroy(Unit) {

}

void CombatControl::onUnitComplete(Unit) {

}

void CombatControl::onSTRONG_ATTACK(TeamControl* t) {
	if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
		if (info->enemy->enemyGroup.size() > 0) {
			if ((*t).tankSet.size() > 0 && info->enemy->enemyGroup.size() > 0) {
				if ((*t).vultureSet.size() + (*t).goliathSet.size() > 0) {
					vector <Position> gvp = getVGPosition((*t).tankCenterPos, info->enemy->enemyGroup.front().first, (*t).vultureSet.size() + (*t).goliathSet.size(), (*t).tR, (*t).tankRage, UnitTypes::Terran_Vulture.width());

					for (auto v = (*t).vultureSet.begin(); v != (*t).vultureSet.end(); v++) {
						if (gvp.size() <= 0)
							break;
						(*v)->self->attack(gvp.back(), false);
						gvp.pop_back();
					}
				}
			}
		}
	}
}

void CombatControl::onCRZAY(TeamControl* t) {
	Unitset enemies = getNearEnemyUnits(t);
	bool onlyZealot = isOnlyZealot(t);

	vector<MyUnit*> kingsMan;

	for (auto vulture : t->vultureSet) {
		//합류함?
		Unit target = getTargetUnit(vulture, enemies);
		if (vulture->isInTeam) {
			//탱크있냐?
			if (t->tankSet.size() != 0) {
				float dist = getDistance(vulture->getPosition(), t->tankCenterPos);
				//주변에 탱크 있냐
				if (dist < t->tankRage*1.0f) {
					//공격할애 있냐?
					if (target != NULL) {
						//질럿밖에 없냐?
						if (onlyZealot) {
							//카이팅 ㄱ
							if (vulture->self->getGroundWeaponCooldown() == 0) {
								if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
									if (gameFrame % 24 == 0)
										vulture->self->attack(target, false);
								}
								else {
									if (getDistance(target->getPosition(), vulture->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), (*t).attackPosition);
										vulture->self->move(p, false);
									}
									else
										vulture->self->attack(target, false);
								}
							}
							else {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), (*t).attackPosition);
								vulture->self->move(p, false);
							}
						}
						//질럿말고 딴애도 있냐
						else {
							//자리지키면서 싸워 ㄱㄱㄱㄱㄱㄱ
							kingsMan.push_back(vulture);
						}
					}
					//공격할애 없냐?
					else {
						//건물은 있냐?
						Unit targetB = getTargetBuilding(vulture);
						if (targetB != NULL) {
							//건물 쳐 ㄱㄱㄱㄱㄱㄱㄱ
							if (gameFrame % 24 == 0) {
								vulture->self->attack(targetB, false);
							}
						}
						else {
							//어택포지션으로 ㄱㄱㄱㄱㄱㄱ
							if (gameFrame % 24 == 0) {
								vulture->self->attack(t->attackPosition, false);
							}
						}
					}
				}
				//주변에 탱크 없냐
				else {
					//주변에 적있냐
					if (target != NULL) {
						//탱크쪽으로 카이팅ㄱ
						if (vulture->self->getGroundWeaponCooldown() == 0) {
							if (target->getType() == UnitTypes::Protoss_Photon_Cannon) {			
								vulture->self->move(t->tankCenterPos, false);
							}
							else {
								if (getDistance(target->getPosition(), vulture->getPosition()) < 80) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
									vulture->self->move(p, false);
								}
								else
									vulture->self->attack(target, false);
							}
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
							vulture->self->move(p, false);
						}
					}
					//주변에 적없냐
					else {
						//주변이 초크포인트임?
						if (isNearChokePoint(vulture)) {
							//attackPosition으로 무빙하셈ㄱㄱㄱㄱㄱ
							if (gameFrame % 12 == 0) {
								vulture->self->attack(t->attackPosition, false);
							}
						}
						else {
							//홀드하셈 ㄱㄱㄱㄱㄱㄱ
							float tdist = getDistance(t->attackPosition, t->tankCenterPos);
							float vdist = getDistance(vulture->getPosition(), t->attackPosition);
							if (tdist < vdist) {
								if (gameFrame % 12 == 0) {
									vulture->self->attack(t->attackPosition, false);
								}
							}
							else {
								if (gameFrame % 12 == 0) {
									vulture->self->holdPosition();
								}
							}
						}
					}
				}
			}
			//탱크1도없냐?
			else {
				//카이팅ㄱㄱㄱㄱㄱ
				if (target != NULL) {
					if (vulture->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							if (gameFrame % 24 == 0)
								vulture->self->attack(target, false);
						}
						else {
							if (getDistance(target->getPosition(), vulture->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), (*t).attackPosition);
								vulture->self->move(p, false);
							}
							else
								vulture->self->attack(target, false);
						}
					}
					else {
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), (*t).attackPosition);
						vulture->self->move(p, false);
					}
				}
				else {
					Unit targetB = getTargetBuilding(vulture);
					if (targetB != NULL) {
						if (gameFrame % 24 == 0) {
							vulture->self->attack(targetB, false);
						}
					}
					else {
						if (gameFrame % 24 == 0) {
							vulture->self->attack(t->attackPosition, false);
						}
					}
				}
			}
		}
		//합류안함?
		else {
			//합류하셈ㄱㄱㄱㄱㄱ
			if (vulture->self->getGroundWeaponCooldown() == 0) {
				if (target == NULL) {
					Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
					vulture->self->move(p, false);
				}
				else {
					if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
						if (gameFrame % 24 == 0)
							vulture->self->attack(target, false);
					}
					else {
						if (getDistance(target->getPosition(), vulture->getPosition()) < 80) {
							/*Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
							vulture->self->move(p, false);*/
							vulture->self->attack(target, false);
						}
						else
							vulture->self->attack(target, false);
					}
				}
			}
			else {
				Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
				vulture->self->move(p, false);
			}

			if (getDistance(vulture->getPosition(), t->tankCenterPos) < t->tankRage*2.f) {
				vulture->isInTeam = true;
			}
		}
	}

	vector <Position> gvp;
	if ((*t).tankSet.size() > 0) {
		gvp = getVGPosition((*t).tankCenterPos, t->attackPosition, kingsMan.size(), (*t).tR, (*t).tankRage, UnitTypes::Terran_Vulture.width());
	}

	for (auto k : kingsMan) {
		Unit target = getTargetUnit(k, enemies);
		if (k->self->getGroundWeaponCooldown() == 0) {
			k->self->attack(target, false);
			if (gvp.size() > 0) {
				gvp.pop_back();
			}
		}
		else {
			if (gvp.size() > 0) {
				k->self->move(gvp.back(), false);
				gvp.pop_back();
			}
		}
	}

	//printf("tankDanger : %d\n", t->tankDanger);
	/*for (auto q = (*t).vultureSet.begin(); q != (*t).vultureSet.end(); q++) {
		Unit target = getTargetUnit((*q), enemies);
		if ((*t).tankSet.size() == 0) {
			if (target == NULL) {
				target = getTargetBuilding((*q));
				if (target == NULL) {
					if (gameFrame % 24 == 0) {
						(*q)->self->attack((*t).attackPosition, false);
					}
				}
				else {
					if (gameFrame % 24 == 0)
						(*q)->self->attack(target, false);
				}
			}
			else {
				if ((*q)->self->getGroundWeaponCooldown() == 0) {
					if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
						if (gameFrame % 24 == 0)
							(*q)->self->attack(target, false);
					}
					else {
						if (getDistance(target->getPosition(), (*q)->getPosition()) < 80) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
							(*q)->self->move(p, false);
						}
						else
							(*q)->self->attack(target, false);
					}
				}
				else {
					Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
					(*q)->self->move(p, false);
				}
			}
		}
		else {
			if (isOnlyZealot(t) && enemies.size() > 0) {
				if (target == NULL) {
					target = getTargetBuilding((*q));
					if (target == NULL) {
						if (gameFrame % 24 == 0) {
							(*q)->self->attack((*t).attackPosition, false);
						}
					}
					else{
						if (gameFrame % 24 == 0)
							(*q)->self->attack(target, false);
					}
				}
				else {
					if ((*q)->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							if (gameFrame % 24 == 0)
								(*q)->self->attack(target, false);
						}
						else {
							if (getDistance(target->getPosition(), (*q)->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
								(*q)->self->move(p, false);
							}
							else
								(*q)->self->attack(target, false);
						}
					}
					else {
						Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
						(*q)->self->move(p, false);
					}
				}
			}
			else {
				if (target != NULL) {
					if ((*q)->self->getGroundWeaponCooldown() == 0) {
						(*q)->self->attack(target, false);
						if (gvp.size() > 0) {
							gvp.pop_back();
						}
					}
					else {
						if (gvp.size() > 0) {
							(*q)->self->move(gvp.back(), false);
							gvp.pop_back();
						}
					}
				}
				else {
					if(BWAPI::Broodwar->getFrameCount() % 12 == 0)
						if (gvp.size() > 0) {
							(*q)->self->move(gvp.back(), false);
							gvp.pop_back();
						}
				}
			}
		}
	}*/


	for (auto q = (*t).goliathSet.begin(); q != (*t).goliathSet.end(); q++) {
		Unit target = getTargetUnit((*q), enemies);
		if ((*t).tankSet.size() == 0) {
			if (target == NULL) {
				if (gameFrame % 24 == 0) {
					(*q)->self->attack((*t).attackPosition, false);
				}
			}
			else {
				if ((*q)->self->getGroundWeaponCooldown() == 0) {
					if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
						if (gameFrame % 24 == 0)
							(*q)->self->attack(target, false);
					}
					else {
						if (getDistance(target->getPosition(), (*q)->getPosition()) < 80) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
							(*q)->self->move(p, false);
						}
						else
							(*q)->self->attack(target, false);
					}
				}
				else {
					Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
					(*q)->self->move(p, false);
				}
			}
		}
		else {
			if (t->tankDanger < 1) {
				if (target == NULL) {
					if (gameFrame % 24 == 0) {
						if (gvp.size() > 0) {
							(*q)->self->attack(gvp.back(), false);
							gvp.pop_back();
						}
						else {
							(*q)->self->attack((*t).attackPosition, false);
						}
					}
				}
				else {
					if ((*q)->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							if (gameFrame % 24 == 0)
								(*q)->self->attack(target, false);
						}
						else {
							if (getDistance(target->getPosition(), (*q)->getPosition()) < 80) {
								if (gvp.size() > 0) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), gvp.back());
									gvp.pop_back();
									(*q)->self->attack(p, false);
								}
								else {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
									(*q)->self->attack(p, false);
								}
							}
							else
								(*q)->self->attack(target, false);
						}
					}
					else {
						if (gvp.size() > 0) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), gvp.back());
							gvp.pop_back();
							(*q)->self->move(p, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
							(*q)->self->move(p, false);
						}
					}
				}
			}
			else {
				if ((*q)->self->getGroundWeaponCooldown() == 0) {
					(*q)->self->attack(target, false);
				}
				else {
					if (gvp.size() > 0) {
						(*q)->self->move(gvp.back(), false);
						gvp.pop_back();
					}
				}
			}
		}
	}

	Unitset enemies2 = getNearEnemyUnits(t);

	/*for (auto tg : t->TG) {
		//셋다 시즈 상태 입니까???
		if (tg->isSieged()) {
			//타겟팅 사람이 있나여
			if (tg->isTargeting()) {
				//질럿밖에없나?
				if (onlyZealot) {
					//시즈 푸셈ㄱ
					for (auto tank : tg->tankgroup) {
						if (tank->self->isSieged()) {
							tank->self->unsiege();
						}
					}
				}
				//딴거도 있나여?
				else {
					//공격해 셋전부ㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetUnit(tank, enemies);
						if (target != NULL) {
							if (tank->self->isAttacking() == false) {
								tank->self->attack(target, false);
							}
						}
					}
				}
			}
			//암것도 음슴까
			else {
				//건물 공격?
				if (tg->isTargetingBuilding()) {
					//공격해염 ㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetBuilding(tank);
						if (target != NULL) {
							if (tank->self->isAttacking() == false) {
								tank->self->attack(target, false);
							}
						}
					}
				}
				//건물 ㄴㄴ ?
				else {
					//시즈 풀어염ㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱ
					for (auto tank : tg->tankgroup) {
						if (tank->self->isSieged()) {
							tank->self->unsiege();
						}
					}
				}
			}
		}
		//탱크 상태 입니까???
		else {
			//공격할 대상이 있나여
			if (tg->isTargeting()) {
				//질럿만??
				if (onlyZealot) {
					//카이팅 ㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetUnit(tank, enemies);
						if (tank->self->isSieged()) {
							tank->self->unsiege();
						}
						else {
							if (target != NULL) {
								if (tank->self->getGroundWeaponCooldown() == 0) {
									if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
										tank->self->move(p, false);
									}
									else
										tank->self->attack(target, false);
								}
								else {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
									tank->self->move(p, false);
								}
							}
							else {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
						}
					}
				}
				//딴거도?
				else{
					//시즈박아여ㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetUnit(tank, enemies);
						if (tank->self->isSieged()) {
							if (target != NULL) {
								if (tank->self->isAttacking() == false) {
									tank->self->attack(target, false);
								}
							}
						}
						else {
							float dist = getDistance(tank->getPosition(), tg->center);
							if (dist < 100) {
								tank->self->siege();
							}
							else {
								tank->self->attack(tg->center);
							}
						}
					}
				}
			}
			//없나여
			else {
				//건물 있음?
				if (tg->isTargetingBuilding()) {
					//공격 ㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetBuilding(tank);
						if (tank->self->isSieged()==false) {
							if (target != NULL) {
								if (tank->self->isAttacking() == false) {
									tank->self->attack(target, false);
								}
							}
						}
						else {
							tank->self->attack(t->attackPosition, false);
						}
					}
				}
				//음슴?
				else {
					//이동
					//서로 떨어져 있냐  그러면 뭉쳐 ㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱ
					bool farfaraway = false;
					for (auto tank : tg->tankgroup) {
						if (isNearChokePoint(tank)) {
							farfaraway = false;
							break;
						}
						for (auto tt : tg->tankgroup) {
							float dist = getDistance(tt->getPosition(), tank->getPosition());
							if (dist > 160) {
								farfaraway = true;
							}
						}
					}
					if (farfaraway) {
						for (auto tt : tg->tankgroup) {
							tt->self->move(tg->center, false);
							if (tt->self->isSieged()) {
								tt->self->unsiege();
							}
						}
					}
					//아니면 공격포지션 ㄱㄱ
					else {
						for (auto tt : tg->tankgroup) {
							tt->self->move(t->attackPosition, false);
							if (tt->self->isSieged()) {
								tt->self->unsiege();
							}
						}
					}
				}
			}
		}
	}*/
	
	for (auto tank : t->tankSet) {
		Unit target = getTargetUnit(tank, enemies);
		// 시즈모드냐
		if (tank->self->isSieged()) {
			//공격할 애가 있냐?
			if (target != NULL) {
				//질럿밖에없냐?
				if (onlyZealot) {
					// 시즈풀어ㄱㄱ
					//printf("시즈모드 -> 질럿밖에 없어서 시즈 풀져?\n");
					if (tank->self->canUnsiege()) {
						tank->self->unsiege();
					}
				}
				//딴애도 있냐?
				else {
					// 공격해ㄱㄱ
					//printf("시즈모드 -> 유닛 공격하져\n");
					if (tank->self->isAttacking() == false) {
						tank->self->attack(target);
					}
				}
			}
			//공격할 애가 없냐?
			else {
				Unit targetB = getTargetBuilding(tank);
				//공격할 건물은 있냐?
				if (targetB != NULL) {
					// 건물공격해ㄱㄱ
					//printf("시즈모드 -> 건물공격 들어가져?\n");
					if (tank->self->isAttacking() == false) {
						tank->self->attack(targetB);
					}
				}
				//공격할 건물도 없냐?
				else {
					// 시즈 풀어ㄱㄱ
					//printf("시즈모드 -> 암것도 없어서 시즈 품미다\n");
					if (tank->self->canUnsiege()) {
						tank->self->unsiege();
					}
				}
			}
		}
		// 탱크모드냐
		else {
			//공격할 애가 있냐?
			if (target != NULL) {
				if (getDistance(target->getPosition(), tank->getPosition()) > UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
					if (gameFrame % 24 == 0)
						tank->self->attack(target, false);
				}
				//질럿밖에 없냐?
				else if (onlyZealot) {
					//공격해ㄱㄱ
					//printf("탱크모드 -> 질럿밖에 없네여\n");
					if (tank->self->getGroundWeaponCooldown() == 0) {
						if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
							tank->self->move(p, false);
						}
						else
							tank->self->attack(target, false);
					}
					else {
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
						tank->self->move(p, false);
					}
				}
				//딴애들도 있냐?
				else {
					//printf("탱크모드 -> 딴애들도 있네여\n");
					float dist = getDistance(target->getPosition(), tank->getPosition());
					//거리가 가깝냐?
					if (dist < 100) {
						//printf("탱크모드 -> 가까우니까 걍 공격\n");
						//공격해ㄱㄱ
						if (gameFrame % 24 == 0)
							tank->self->attack(target, false);
					}
					//거리가 머냐?
					else {
						// 시즈박아ㄱㄱ
						//printf("탱크모드 -> 머니까 시즈박을게여\n");
						if (isNearChokePoint(tank)) {
							if (tank->self->getSpellCooldown() == 0) {
								tank->self->attack(target, false);
							}
							else {
								if(gameFrame%8 == 0)
									tank->self->move(t->attackPosition, false);
							}
						}
						else {
							if (tank->self->canSiege())
								tank->self->siege();
						}
					}
				}
			}
			//공격할 애가 없냐?
			else {
				//건물은 있냐?
				Unit targetB = getTargetBuilding(tank);
				if (targetB != NULL) {
					//시즈박아ㄱㄱ
					//printf("탱크모드 -> 건물공격할거니까 시즈할래여\n");
					
					if (isNearChokePoint(tank)) {
						if (tank->self->getSpellCooldown() == 0) {
							tank->self->attack(target, false);
						}
						else {
							if (gameFrame % 8 == 0)
								tank->self->move(t->attackPosition, false);
						}
					}
					else {
						if (tank->self->canSiege())
							tank->self->siege();
					}
				}
				//건물도 없냐?
				else {
					//합류는 했냐?
					if (tank->isInTeam) {
						//printf("탱크모드 -> 암것도 없어서 이동함미다\n");
						if(gameFrame % 24 == 0)
							tank->self->attack(t->attackPosition, false);
					}
					else {
						if (gameFrame % 24 == 0)
							tank->self->attack(t->tankCenterPos, false);
						float dist = getDistance(tank->getPosition(), t->tankCenterPos);
						if (dist < 250) {
							tank->isInTeam = true;
						}
					}
				}
			}
		}
	}
	//이거로함

	/*for (auto tank : t->tankSet) {
		// 팀에 합류했는가?
		Unit target = getMassedUnit(tank, enemies);
		if (!tank->isInTeam) {
			//공격할 대상이 있는가?
			if (target != NULL) {
				//질럿뿐인가?
				if (onlyZealot) {
					//카이팅 ㄱㄱ
					//printf("팀에 합류했고, 공격할 대상이 있고 질럿뿐이여서 카이팅을 할게요\n");
					if (tank->self->isSieged()) {
						if (tank->self->canUnsiege() && tank->self->siege()) {
							tank->self->unsiege();
						}
					}
					else {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
							else
								tank->self->attack(target, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
							tank->self->move(p, false);
						}
					}
				}
				//다른 유닛도 있는가?
				else {
					//시즈 ㄱㄱ
					//printf("팀에 합류했고, 공격할 대상이 있고 다른것도 있어서 시즈를 박을게요\n");
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
			}
			//공격할 대상이 없는가?
			else {
				Unit targetB = getTargetBuilding(tank, getNearEnemyBuildings(tank));
				//공격할 건물이라도 있는가?
				if (targetB != NULL) {
					// 시즈 ㄱㄱ
					//printf("팀에 합류했고, 공격할 대상이 없지만 건물이 있어서 시즈를 박을게요\n");
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(targetB, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
				//공격할 건물도 없는가?
				else {
					//printf("팀에 합류했고, 공격할 대상이 없고 건물도 없어서 이동 할게요\n");
					// 공격포지션 이동  ㄱㄱ
					if (tank->self->isSieged()) {
						if (tank->self->canUnsiege() && tank->self->isSieged()) {
							tank->self->unsiege();
						}
					}
					else {
						if (gameFrame % 24 == 0) {
							tank->self->attack(t->attackPosition, false);
						}
					}

				}
			}
		}
		// 팀에 합류하지 못했는가?
		else {
			//팀 센터랑 거리가 가까운가?
			float dist = getDistance(tank->getPosition(), t->tankCenterPos);
			if (dist < 350) {
				tank->isInTeam = true;
			}
			//공격할 대상이 있는가?
			if (target != NULL) {
				//질럿뿐인가?
				if (onlyZealot) {
					// 카이팅 ㄱㄱ
					//printf("팀에 합류하지 못 했고, 공격할 대상이 있고 질럿 뿐이여서 카이팅을할게여\n");
					if (tank->self->isSieged()) {
						if (tank->self->canUnsiege() && tank->self->siege()) {
							tank->self->unsiege();
						}
					}
					else {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
							else
								tank->self->attack(target, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
							tank->self->move(p, false);
						}
					}
				}
				//다른 유닛도 있는가?
				else {
					//printf("팀에 합류하지 못 했고, 공격할 대상이 있어 시즈를 할게여\n");
					// 시즈 ㄱㄱ
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
			}
			//공격할 대상이 없는가?
			else {
				Unit targetB = getTargetBuilding(tank, getNearEnemyBuildings(tank));
				//공격할 건물이라도 있는가?
				if (targetB != NULL) {
					//시즈 ㄱㄱ
					//printf("팀에 합류하지 못 했고, 공격할 대상이 없지만 건물이 있어서 시즈를 할게여\n");
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(targetB, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
				//공격할 건물도 없는가?
				else {
					// 팀의 무리로 이동 ㄱㄱ
					//printf("팀에 합류하지 못 했고, 공격할 대상이 없어서 팀의 무리로 이동 할게여\n");
					if (gameFrame % 24 == 0) {
						tank->self->attack(t->tankCenterPos, false);
					}
				}
			}
		}
	}*/
}

void CombatControl::onATTACK(TeamControl* t) {

}

void CombatControl::onTIGHTING(TeamControl* t) {

}

void CombatControl::onDEFENSE(TeamControl* t) {

}

void CombatControl::onSQUEEZE(TeamControl* t) {

	int mineCnt = 0;
	for (auto v : t->vultureSet) {
		if (v->maginNumber > 0 && !v->mining) {
			mineCnt++;
		}
	}

	int posIndex = 0;
	int defeneseIndex = 0;
	if (t->tankSet.size() > 10) {
		defeneseIndex = 1;
	}

	vector<Position> minPos = getMinePos(max(mineCnt, 5));
	Unitset enemies = getNearEnemyUnits(t);

	for (auto v : t->vultureSet) {
		Unit target = getTargetUnit(v, enemies);
		if (target != NULL) {
			if (getDistance(target->getPosition(), v->getPosition()) < 300) {
				if (t->tankSet.size() > 0) {
					if (v->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							v->self->move(t->attackPosition, false);
						}
						else {
							if (isOnlyZealot(t)) {
								if (getDistance(target->getPosition(), v->getPosition()) < 80) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
									v->self->attack(target, false);
								}
								else
									v->self->attack(target, false);
							}
							else {
								v->self->attack(target, false);
							}
						}
					}
					else {
						v->mining = false;
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
						v->self->move(p, false);
					}
				}
				else {
					if (v->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							v->self->move(t->attackPosition, false);
						}
						else {
							if (isOnlyZealot(t)) {
								if (getDistance(target->getPosition(), v->getPosition()) < 80) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).attackPosition);
									v->self->move(p, false);
								}
								else
									v->self->attack(target, false);
							}
							else {
								v->self->attack(target, false);
							}
						}
					}
					else {
						v->mining = false;
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
						v->self->move(p, false);
					}
				}
			}
			else {
				Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
				v->self->move(p, false);
			}
		}
		else {
			if (v->maginNumber > 0 && BWAPI::Broodwar->self()->hasResearched(TechTypes::Spider_Mines)) {
				if (v->mining) {
					float dist = getDistance(v->getPosition(), v->targetPosition);
					if (dist < 30) {
						//printf("도착했어여 마인을 박을게여\n");
						if (BWAPI::Broodwar->getFrameCount() % 24 == 0)
							v->self->useTech(TechTypes::Spider_Mines, v->targetPosition);
						if (v->maginNumber != v->self->getSpiderMineCount()) {
							//printf("마인을 박았느니라 다시 마인 모드 해제\n");
							v->maginNumber = v->self->getSpiderMineCount();
							v->mining = false;
						}
					}
					else {
						if (BWAPI::Broodwar->getFrameCount() % 24 == 0) {
							v->self->move(v->targetPosition, false);

							//printf("마인을 박으러 가는 중...\n");
						}
					}
				}
				else {
					if (minPos.size() > 0) {
						//printf("마인을 박으러 가려고 설정\n");
						float minDist = 9999.f;
						int eraseIndex = 0;
						Position ret = Position(-1, -1);
						for (auto p = minPos.begin(); p != minPos.end(); p++) {
							float dist = getDistance((*p), v->getPosition());
							if (minDist > dist) {
								minDist = dist;
								ret = (*p);
							}
						}
						setUseMineState(ret, MINE_READY);
						v->targetPosition = ret;
						auto iter = minPos.begin();
						for (iter; iter != minPos.end(); ++iter) {
							if (ret == (*iter)) {
								minPos.erase(iter);
								break;
							}
						}

						v->mining = true;
					}
					else {
						if (target != NULL) {
							if (v->self->getGroundWeaponCooldown() == 0) {
								if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
									if (gameFrame % 24 == 0)
										v->self->attack(target, false);
								}
								else {
									if (getDistance(target->getPosition(), v->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).attackPosition);
										v->self->move(p, false);
									}
									else
										v->self->attack(target, false);
								}
							}
							else {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).attackPosition);
								v->self->move(p, false);
							}
						}
						else {
							Unit targetB = getTargetBuilding(v);
							if (targetB != NULL) {
								if (gameFrame % 24 == 0) {
									v->self->attack(targetB, false);
								}
							}
							else {
								if (gameFrame % 24 == 0) {
									v->self->attack(t->attackPosition, false);
								}
							}
						}

					}
				}
			}
			else {
				if (target != NULL) {
					if (v->self->getGroundWeaponCooldown() == 0) {
						if (isOnlyZealot(t)) {
							if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
								v->self->attack(target, false);
							}
							else {
								if (t->tankSet.size() > 1) {
									if (getDistance(target->getPosition(), v->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->tankCenterPos);
										v->self->move(p, false);
									}
									else
										v->self->attack(target, false);
								}
								else {
									if (getDistance(target->getPosition(), v->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
										v->self->move(p, false);
									}
									else
										v->self->attack(target, false);
								}
							}

						}
						else {
							if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
								v->self->attack(target, false);
							}
							else {
								v->self->attack(target, false);
							}
						}
					}
					else {
						if (t->tankSet.size() > 1) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).tankCenterPos);
							v->self->move(p, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).attackPosition);
							v->self->move(p, false);
						}
					}
				}
				else {
					Unit targetB = getTargetBuilding(v);
					if (targetB != NULL) {
						if (gameFrame % 24 == 0) {
							v->self->attack(targetB, false);
						}
					}
					else {
						if (gameFrame % 24 == 0) {
							v->self->attack(t->attackPosition, false);
						}
					}
				}

			}
		}
	}

	int safeCount = 0;
	bool bangMissionCheck = false;
	for (auto tank : t->tankSet) {
		Unit target = getMassedUnit(tank, enemies);
		if (posIndex < 15) {
			float dist = getDistance(tank->getPosition(), (Position)defenseTankPos[posIndex]);
			if (dist < 45) {
				if (tank->self->canSiege() && !tank->self->isSieged()) {
					tank->self->siege();
					safeCount++;
				}
				else {
					if (target != NULL) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
					safeCount++;
				}
			}
			else {
				if (target != NULL) {
					float dist = getDistance(target->getPosition(), tank->getPosition());
					if (dist > UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
						target = NULL;
					}
				}
				if (target == NULL) {
					if (tank->self->isSieged()) {
						tank->self->unsiege();
					}
					else {
						if (gameFrame % 12 == 0) {
							tank->self->move((Position)defenseTankPos[posIndex], false);
						}
					}
				}
				else {
					if (isOnlyZealot(t)) {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
							else
								tank->self->attack(target, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
							tank->self->move(p, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
			}
			posIndex++;
		}
		else {
			float dist = getDistance((Position)DefensePos[defeneseIndex], (Position)tank->getTilePosition());
			if (dist < 100) {
				if (target != NULL) {
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
					else {
						if (isOnlyZealot(t)) {
							if (tank->self->getGroundWeaponCooldown() == 0) {
								if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
									tank->self->move(p, false);
								}
								else
									tank->self->attack(target, false);
							}
							else {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
						}
						else {
							if (tank->self->canSiege() && !tank->self->isSieged()) {
								tank->self->siege();
							}
						}
					}
				}
				else {
					if (tank->self->canSiege() && !tank->self->isSieged()) {
						tank->self->siege();
					}
				}
			}
			else {
				if (gameFrame % 24 == 0) {
					tank->self->attack((Position)DefensePos[defeneseIndex], false);
				}
			}
		}
	}
}

vector<Position> CombatControl::getVGPosition(Position tPos, Position ePos, int vgNum, int tR, float tankRange, int dist) {
	vector <Position> ret;
	ret.clear();

	float PI = 3.141592f;
	float theta = 0.0f;
	float r = 1;
	int px = 0;
	int py = 0;

	int R ;
	if (tR == 3) {
		R = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f;
	}
	else if (tR == 2) {
		R = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 2 - 75;
	}
	else if (tR == 1) {
		R = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 3 - 75;
	}
	else {
		R = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 4 - 75;
	}
																				  //R 길이 수정해줍시다.

	if (tR < 2) {
		R = max(tankRange + 80.0f, UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 2);
		R = min(R, (int)(UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 3));
	}
	else {
		R = max(tankRange + 80.0f, UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 1);
		R = min(R, (int)(UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 2));
	}


	theta = atan((float)(tPos.y - ePos.y) / (tPos.x - ePos.x));
	float l = (float)dist / ((float)R / 3);
	float sl = -l / 4 * (vgNum - 1);
	float el = l / 4 * (vgNum - 1);
	if (dist != UnitTypes::Terran_Vulture.width()) {
		sl = -l / 4 * (vgNum - 1) - l/3;
		el = l / 4 * (vgNum - 1) - l/3;
	}

	if (tPos.x < ePos.x && tPos.y > ePos.y || tPos.x < ePos.x && tPos.y < ePos.y) {  // 14사분면

		for (float z = sl; z <= el; z += l / 2) {
			px = (int)(R * cos(theta + (z*r))) + tPos.x;
			py = (int)(R * sin(theta + (z*r))) + tPos.y;
			ret.push_back(Position(px, py));
		}
	}
	else if (tPos.x > ePos.x && tPos.y < ePos.y || tPos.x > ePos.x && tPos.y > ePos.y) {  // 23사분면

		for (float z = sl; z <= el; z += l / 2) {
			px = (int)(R * cos(theta + PI + (z*r))) + tPos.x;
			py = (int)(R * sin(theta + PI + (z*r))) + tPos.y;
			ret.push_back(Position(px, py));
		}
	}

	return ret;
}
Unitset CombatControl::getNearEnemyBuildings(MyUnit* t) {
	Unitset ret;
	ret.clear();
	
	for (auto q = info->enemy->getAllBuildings().begin(); q != info->enemy->getAllBuildings().end(); q++) {
		int range = (int)(((*q)->getType().groundWeapon().maxRange()*1.0f));
		float dist = getDistance(t->getPosition(), info->enemy->getPosition((*q)));
		if (dist < range) {
			ret.insert((*q));
		}
	}
	return ret;
}

Unitset CombatControl::getNearEnemyUnits(TeamControl* t) {
	Unitset ret;
	ret.clear();
	vector<MyUnit*> teamUnits;
	teamUnits.clear();
	for (auto q = t->vultureSet.begin(); q != t->vultureSet.end(); q++) {
		teamUnits.push_back(*q);
	}
	for (auto q = t->tankSet.begin(); q != t->tankSet.end(); q++) {
		teamUnits.push_back(*q);
	}
	for (auto q = t->goliathSet.begin(); q != t->goliathSet.end(); q++) {
		teamUnits.push_back(*q);
	}
	for (auto q = t->vessleList.begin(); q != t->vessleList.end(); q++) {
		teamUnits.push_back(*q);
	}
	for (auto q = t->marineSet.begin(); q != t->marineSet.end(); q++) {
		teamUnits.push_back(*q);
	}
	
	for (auto q = info->enemy->getAllUnits().begin(); q != info->enemy->getAllUnits().end(); q++) {
		if ((*q)->isDetected() == false && (*q)->getType() == UnitTypes::Protoss_Observer) continue;
		if (t->state == TeamControl::STATE::BCN_START || t->state == TeamControl::STATE::BCN_CLOSED || t->state == TeamControl::STATE::BCN_FRONT) {
			for (auto terr : info->map->getMyTerritorys()) {
				if (BWTA::getRegion((*q)->getPosition()) == BWTA::getRegion(terr->getPosition())) {
					ret.insert((*q));
					break;
				}

				float dist = getDistance(terr->getBase()->getPosition(), (*q)->getPosition());
				if (dist < 360) {
					ret.insert((*q));
					break;
				}
			}
		}
		else {
			for (auto m = teamUnits.begin(); m != teamUnits.end(); m++) {
				int range = (int)(((*q)->getType().groundWeapon().maxRange()*1.2f + (*m)->getType().groundWeapon().maxRange()) * 1.2f);
				float dist = getDistance((*m)->getPosition(), (*q)->getPosition());
				if (dist < range) {
					ret.insert(*q);
					break;
				}
			}
		}
	}
	
	return ret;
}

Unit CombatControl::getRepairUnit(MyUnit* scv) {
	Unit ret = NULL;

	Unitset repairSet;
	repairSet.clear();

	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode)) {
		repairSet.insert(tank->self);
	}
	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
		repairSet.insert(tank->self);
	}
	for (auto bunker : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		repairSet.insert(bunker->self);
	}

	float maxValue = 0.0f;

	for (auto rs : repairSet) {
		float value = 0;
		if (rs->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || rs->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
			value = 1;
		}
		else {
			value = 1.5f;
		}
		float dist = (500.f - getDistance(scv->getPosition(), rs->getPosition()));
		dist /= 500.f;
		float hp = (float)(rs->getType().maxHitPoints() - rs->getHitPoints()) / (float)(rs->getType().maxHitPoints());

		value += dist + hp;

		if (hp == 0) {
			continue;
		}

		if (maxValue < value) {
			ret = rs;
			maxValue = value;
		}
	}

	return ret;
}

Unit CombatControl::getHealUnit(MyUnit* unit, TeamControl* t) {

	Unit ret = NULL;

	Unitset healSet;
	healSet.clear();

	for (auto marine : t->marineSet) {
		healSet.insert(marine->self);
	}
	for (auto medic : t->medicSet) {
		healSet.insert(medic->self);
	}
	for (auto scv : t->scvSet) {
		healSet.insert(scv->self);
	}

	float maxValue = 0.0f;

	for (auto hs : healSet) {
		float value = getTargetUnitValue(unit->getType(), hs->getType());
		float dist = (500.f - getDistance(unit->getPosition(), hs->getPosition()));
		dist /= 500.f;
		float hp = (float)(hs->getType().maxHitPoints() - hs->getHitPoints()) / (float)(hs->getType().maxHitPoints());

		value += dist + hp;

		if (hp == 0) {
			continue;
		}

		if (maxValue < value) {
			ret = hs;
			maxValue = value;
		}
	}

	return ret;
}

Unit CombatControl::getTargetUnit(MyUnit* unit, Unitset enemyUnits) {
	Unit ret = NULL;

	if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		ret = getMassedUnit(unit, enemyUnits);
		return ret;
	}
	
	float maxValue = 0; 
	for (auto q = enemyUnits.begin(); q != enemyUnits.end(); q++) {
		if (unit->getType() == UnitTypes::Unknown) continue;
		if ((*q)->getType() == UnitTypes::Protoss_Observer) continue;
		if (unit->getType().airWeapon().maxRange() < 1 && (*q)->isFlying()) continue;
		float value = getTargetUnitValue(unit->getType(), (*q)->getType());
		float dist = (unit->getType().groundWeapon().maxRange() - getDistance(unit->getPosition(), (*q)->getPosition()));
		float unitDist = getDistance((*q)->getPosition(), unit->getPosition());
		float tdist = getDistance(UnitControl::getInstance()->TCV[unit->team]->tankCenterPos, (*q)->getPosition());

		if (unit->getType() != UnitTypes::Terran_Marine) {
			if (unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
				if (unitDist < unit->getType().groundWeapon().minRange() || unitDist > UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) continue;
			}
			else if (unitDist < unit->getType().groundWeapon().minRange() || unitDist > unit->getType().groundWeapon().maxRange()) continue;
		}
		else{
			if (unitDist > unit->getType().groundWeapon().maxRange() + 20) continue;
		}

		if (UnitTypes::Terran_Siege_Tank_Tank_Mode == unit->getType()) {
			dist = (UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() - getDistance(unit->getPosition(), (*q)->getPosition()));
		}

		if (dist < 0) dist = 0;
		else {
			dist = dist / (float)unit->getType().groundWeapon().maxRange();
		}
		float hp = (((float)(*q)->getType().maxHitPoints() + (float)(*q)->getType().maxShields()) - ((float)(*q)->getHitPoints() + (float)(*q)->getShields())) / ((float)(*q)->getType().maxHitPoints() + (float)(*q)->getType().maxShields());
		value = value * 5.0f + dist * 8.0f + hp * 3.0f;

		float v = 0;
		if (unit->getType() == UnitTypes::Terran_Marine) {
			v = (500 - min(500.f, tdist)) / 500.f;
			value += v * 6.0f;
		}


		if (maxValue < value) {
			maxValue = value;
			ret = (*q);
		}
	}

	return ret;
}

Unit CombatControl::getTargetBuilding(MyUnit*unit) {
	Unit ret = NULL;
	float minDist = 9999;
	for (auto e = info->enemy->getAllBuildings().begin(); e != info->enemy->getAllBuildings().end(); e++) {
		float dist = getDistance((*e)->getPosition(), unit->getPosition());
		if (minDist > dist && (dist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() && dist > UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().minRange())) {
			minDist = dist;
			ret = (*e);
		}
	}
	return ret;
}
float CombatControl::getTargetUnitValue(UnitType my, UnitType enemy) {
	if (my == UnitTypes::Terran_Vulture) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 1.2f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 0.9f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 0.5f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 0.0f;
		}
	}
	else if (my == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.8f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 2.5f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 1.7f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 0.0f;
		}
		else {
			return 0.0f;
		}
	}
	else if (my == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.8f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 1.2f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 1.2f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 0.0f;
		}
	}
	else if (my == UnitTypes::Terran_Goliath) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 0.5f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Protoss_Interceptor) {
			return 0.5f;
		}
	}
	else if (my == UnitTypes::Terran_Science_Vessel) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.05f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 0.2f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.2f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 1.5f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.5f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 0.2f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 0.2f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 4.5f;
		}
		else if (enemy == UnitTypes::Protoss_Interceptor) {
			return 0.01f;
		}
	}
	else if (my == UnitTypes::Terran_Marine) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 1.2f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 0.9f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 1.5f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 1.5f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 0.0f;
		}
	}
	else if (my == UnitTypes::Terran_Medic) {
		if (enemy == UnitTypes::Terran_Marine) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Terran_SCV) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Terran_Medic) {
			return 0.7f;
		}
		return 0.0f;
	}

	return 0.0f;
}

bool CombatControl::isOnlyZealot(TeamControl* t) {
	Unitset enemies = getNearEnemyUnits(t);
	for (auto e = enemies.begin(); e != enemies.end(); e++) {
		if ((*e)->getType() == UnitTypes::Protoss_Probe) continue;
		if ((*e)->getType() != UnitTypes::Protoss_Zealot && !(*e)->getType().isFlyer() && (*e)->getType() != UnitTypes::Protoss_High_Templar) {
			return false;
		}
	}
	return true;
}

Unit CombatControl::getMassedUnit(MyUnit* unit, Unitset enemies) {
	// 밀집된 유닛 반환
	Unit ret = NULL;

	//시즈일떄
	if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		//시즈모드 일 때만
		float maxValue = -9999;

		for (auto e = enemies.begin(); e != enemies.end(); e++) {
			if ((*e)->isFlying()) continue;
			if ((*e)->getType() == UnitTypes::Unknown) continue;
			if ((*e)->getType() == UnitTypes::Protoss_High_Templar && (*e)->getEnergy() < 60) continue;
			//공격할 수 있는 애들 중에

			int minR = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().minRange();
			int maxR = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange();
			float distance = getDistance((*e)->getPosition(), unit->getPosition());
			if ( minR < distance && maxR > distance) {
				// 직접 타격하는 친구는 *8
				float value = getTargetUnitValue(unit->getType(), (*e)->getType()) * 8.0f;

				for (auto s = enemies.begin(); s != enemies.end(); s++) {
					float dist = getDistance((*e)->getPosition(), (*s)->getPosition());
					// 스플래시 해당 거리 따라 데미지에 맞춰 값 계산
					if (dist < 10) {
						value += getTargetUnitValue(UnitTypes::Terran_Siege_Tank_Siege_Mode, (*e)->getType()) * 4.0f;
					}
					else if (dist < 25) {
						value += getTargetUnitValue(UnitTypes::Terran_Siege_Tank_Siege_Mode, (*e)->getType()) * 2.0f;
					}
					else if (dist < 40) {
						value += getTargetUnitValue(UnitTypes::Terran_Siege_Tank_Siege_Mode, (*e)->getType()) * 1.0f;
					}
				}
				for (auto s : info->self->getAllUnits()) {
					float dist = getDistance((*e)->getPosition(), s->getPosition());
					// 스플래시 해당 거리 따라 데미지에 맞춰 값 계산
					if (dist < 10) {
						value -= 0.5f * 4.0f;
					}
					else if (dist < 25) {
						value -= 0.5f * 2.0f;
					}
					else if (dist < 40) {
						value -= 0.5f * 1.0f;
					}
				}
				if (maxValue < value) {
					ret = (*e);
					maxValue = value;
				}
			}
		}

		return ret;
	}

	return ret;
}

vector<std::pair<Position, float>> CombatControl::getEMPPositions(TeamControl* t, int vCount, Unitset enemies) {
	vector <pair<Position, float>> empValues;
	vector <pair <Position, float>> totalValues;
	//maxVal >= 4 && v->self->getEnergy() >= 100 && getDistance(v->getPosition(), ret) < 256 * 2.5f && v->maginNumber > 48
	for (auto e : info->enemy->getAllUnits()) {
		if (e->getType() == UnitTypes::Protoss_Arbiter && e->getEnergy() < 80) {
			continue;
		}
		else if (e->getType() == UnitTypes::Protoss_High_Templar && e->getEnergy() < 80) {
			continue;
		}
		else if (e->getShields() < 10) {
			continue;
		}
		float vx = e->getVelocityX() * 20.0f;
		float vy = e->getVelocityY() * 20.0f;
		Position p = Position(e->getPosition().x + vx, e->getPosition().y + vy);
		float val = getTargetUnitValue(UnitTypes::Terran_Science_Vessel, e->getType());
		BWAPI::Broodwar->drawLineMap(p, e->getPosition(), Colors::Red);
		empValues.push_back(pair<Position, float>(p, val));
	}


	// 예상 위치에 박아놓고 포지션당 값 계산
	float maxVal = 0;
	for (auto q = empValues.begin(); q != empValues.end(); q++) {
		float val = (*q).second;
		for (auto qq = empValues.begin(); qq != empValues.end(); qq++) {
			float dist = getDistance((*qq).first, (*q).first);
			// 근처에 있는 애들도 1
			if (dist < 32 * 3) {
				val += (*qq).second;
			}
		}
		if (maxVal < val) {
			maxVal = val;
			totalValues.push_back(pair<Position, float>((*q).first, val));
		}
	}

	vector<std::pair<Position, float>> ret;
	for (int x = 0; x < vCount; x++) {
		for (auto q = ret.begin(); q != ret.end(); q++) {
			auto iter = totalValues.begin();
			for (iter; iter != totalValues.end(); ++iter) {
				float dist = getDistance(iter->first, (*q).first);
				if (dist < 32 * 4) {
					totalValues.erase(iter);
					iter--;
				}
			}
		}
		
		float maxValue = 0;
		Position maxPos;
		for (auto q = totalValues.begin(); q != totalValues.end(); q++) {
			if (maxValue < (*q).second) {
				maxValue = (*q).second;
				maxPos = (*q).first;
			}
		}

		if (maxValue >= 4) {
			ret.push_back(std::pair<Position, float>(maxPos, maxValue));
		}
		else {
			break;
		}
	}

	return ret;

}

void CombatControl::setMinePos() {
	vector<Position> hongPos;
	hongPos.push_back(Position(271, 852));			hongPos.push_back(Position(472, 1113));			hongPos.push_back(Position(751, 1220));			hongPos.push_back(Position(1867, 313));
	hongPos.push_back(Position(1403, 306));			hongPos.push_back(Position(1229, 538));			hongPos.push_back(Position(1148, 894));			hongPos.push_back(Position(1252, 1489));
	hongPos.push_back(Position(943, 1201));			hongPos.push_back(Position(1249, 1674));		hongPos.push_back(Position(1232, 1912));		hongPos.push_back(Position(1297, 2250));
	hongPos.push_back(Position(677, 2054));			hongPos.push_back(Position(419, 2264));			hongPos.push_back(Position(928, 2183));			hongPos.push_back(Position(1462, 2577));
	hongPos.push_back(Position(1167, 2555));		hongPos.push_back(Position(309, 2561));			hongPos.push_back(Position(564, 2832));			hongPos.push_back(Position(748, 3049));
	hongPos.push_back(Position(1566, 2743));		hongPos.push_back(Position(1719, 3030));		hongPos.push_back(Position(1432, 3030));		hongPos.push_back(Position(2055, 3420));
	hongPos.push_back(Position(1145, 3314));		hongPos.push_back(Position(1167, 3442));		hongPos.push_back(Position(785, 3879));			hongPos.push_back(Position(1071, 3655));
	hongPos.push_back(Position(2237, 3688));		hongPos.push_back(Position(2644, 3770));		hongPos.push_back(Position(2009, 2799));		hongPos.push_back(Position(1927, 2451));
	hongPos.push_back(Position(2178, 2452));		hongPos.push_back(Position(2050, 2273));		hongPos.push_back(Position(2539, 2695));		hongPos.push_back(Position(2851, 3496));
	hongPos.push_back(Position(2987, 3149));		hongPos.push_back(Position(2825, 2593));		hongPos.push_back(Position(3189, 2937));		hongPos.push_back(Position(3445, 2920));
	hongPos.push_back(Position(3587, 2984));		hongPos.push_back(Position(3768, 3248));		hongPos.push_back(Position(2950, 2427));		hongPos.push_back(Position(2890, 2118));
	hongPos.push_back(Position(3401, 2027));		hongPos.push_back(Position(3666, 1810));		hongPos.push_back(Position(2879, 1720));		hongPos.push_back(Position(3729, 1533));
	hongPos.push_back(Position(3506, 1268));		hongPos.push_back(Position(3390, 1086));		hongPos.push_back(Position(2750, 1423));		hongPos.push_back(Position(2387, 1222));
	hongPos.push_back(Position(2141, 1656));		hongPos.push_back(Position(1905, 1663));		hongPos.push_back(Position(2053, 1875));		hongPos.push_back(Position(1495, 1536));
	hongPos.push_back(Position(1783, 1254));		hongPos.push_back(Position(2046, 590));			hongPos.push_back(Position(2939, 687));			hongPos.push_back(Position(2971, 824));
	hongPos.push_back(Position(2690, 1060));		hongPos.push_back(Position(3038, 463));			hongPos.push_back(Position(3299, 268));			hongPos.push_back(Position(1889, 911));
	hongPos.push_back(Position(2228, 926));

	for (auto p : hongPos) {
		Position p1[4];
		p1[0] = Position(p.x+30, p.y-30);
		p1[1] = Position(p.x+30, p.y+30);
		p1[2] = Position(p.x-30, p.y+30);
		p1[3] = Position(p.x-30, p.y-30);
		for (int x = 0; x < 4; x++) {
			if (BWAPI::Broodwar->isWalkable((WalkPosition)p1[x])) {
				std::pair<Position, std::pair<float, int>> t;
				t.first = p1[x];
				t.second.first = 0.0f;
				t.second.second = 0;
				minePos.push_back(t);
			}
		}
	}
	hongPos.clear();
	for (auto b : BWTA::getBaseLocations()) {
		std::pair<Position, std::pair<float, int>> t;
		t.first = b->getPosition();
		t.second.first = 0.0f;
		t.second.second = 0;
		minePos.push_back(t);
	}
}

void CombatControl::updateMinePosValue() {
	for (auto q = minePos.begin(); q != minePos.end(); q++) {
		(*q).second.first = 0.0f;
		if ((*q).second.second != MINE_READY) {
			bool isInMine = false;
			for (auto m : info->self->getUnits(UnitTypes::Terran_Vulture_Spider_Mine)) {
				if (m->getPosition() == (*q).first) {
					isInMine = true;
				}
			}
			if (isInMine) {
				(*q).second.second = MINE_DONE;
			}
			else {
				(*q).second.second = MINE_NONE;
			}
		}
		else {
			// 건물이 지어져 있거나 자원이 있는 곳은 애초에 세지 않을 거임 ㅅㄱ
			int gimV = MapInformation::getInstance()->getGIM((*q).first);
			if (gimV < 0) {
				(*q).second.first = -1.0f;
			}
		}
	}

	// 적군 그룹과 가까우면 마인을 박지 않을 거에여
	for (auto q = minePos.begin(); q != minePos.end(); q++) {
		//이미 마인을 박으러 갔거나, 안 박을 곳은 제외
		if ((*q).second.second >= 1 || (*q).second.first < 0) continue;
		for (auto e : info->enemy->getAllUnits()) {
			float dist = getDistance(info->enemy->getPosition(e), (*q).first);
			if (dist < 640) {
				(*q).second.first = -1.0f;
			}
		}
		for (auto e : info->self->getAllUnits()) {
			if (e->getType() == UnitTypes::Terran_Vulture_Spider_Mine) continue;
			float dist = getDistance(e->getPosition() , (*q).first);
			if (dist < 80) {
				(*q).second.first = -1.0f;
			}
		}
		for (auto e : info->self->getAllBuildings()) {
			float dist = getDistance(e->getPosition(), (*q).first);
			if (dist < 240) {
				(*q).second.first = -1.0f;
			}
		}
		for (auto e : info->enemy->getAllBuildings()) {
			float dist = getDistance(info->enemy->getPosition(e), (*q).first);
			if (dist < 640) {
				(*q).second.first = -1.0f;
			}
		}
		//시즈 주변에도 안 박을 거에요
		if ((*q).second.second >= 1 || (*q).second.first < 0) continue;
		for (auto t : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
			float dist = getDistance((*t).self->getPosition(), (*q).first);
			if (dist < 100) {
				(*q).second.first = -1.0f;
			}
		}
	}

	// 우리 그룹과 가까우면
	for (auto q = minePos.begin(); q != minePos.end(); q++) {
		//이미 마인을 박으러 갔거나, 안 박을 곳은 제외
		if ((*q).second.second >= 1 || (*q).second.first < 0) continue;
		for (auto g = info->self->myGroup.begin(); g != info->self->myGroup.end(); g++) {
			float dist = getDistance((*g).first, (*q).first);
			if (dist < 1800) {
				(*q).second.first += (1800 - dist)/1800.0*5;
			}
		}
		////적 그룹과 가까우면
		/*for (auto g = info->enemy->enemyGroup.begin(); g != info->enemy->enemyGroup.end(); g++) {
			float dist = getDistance((*g).first, (*q).first);
			if (dist < 1800 && dist > 300) {
				(*q).second.first += (1800 - dist)*30;
			}
		}*/
		// 적 베이스와 가까우면
		for (auto t = info->map->BLI.begin(); t != info->map->BLI.end(); t++) {
			if ((*t).second->player != 0) continue;
			float dist = getDistance((*t).first->getPosition(), (*q).first);
			if (dist < 3600) {
				(*q).second.first += (3600 - dist) / 3600.0 * 10;
			}
		}
		// 우리 베이스와 가까우면
		for (auto t : info->map->getMyTerritorys()) {
			float dist = getDistance((*t).getBase()->getPosition(), (*q).first);
			if (dist < 3600) {
				(*q).second.first += (3600 - dist) / 3600.0 * 8;
			}
		}

		for (auto b = BWTA::getBaseLocations().begin(); b != BWTA::getBaseLocations().end(); b++) {
			if ((*b)->getPosition() == (*q).first) {
				(*q).second.first += 50000;
			}
		}
	}

	
}

vector<Position> CombatControl::getMinePos(int count) {
	vector<Position> ret;
	while (ret.size() < count) {
		float maxValue = 0;
		Position maxPos = Position(-1, -1);
		for (auto q = minePos.begin(); q != minePos.end(); q++) {
			if ((*q).second.second != 0) continue;
			bool check = false;
			for (auto r : ret) {
				if (r == (*q).first) check = true;
			}
			if (check) continue;

			float value = (*q).second.first;
			if (maxValue < value) {
				maxValue = value;
				maxPos = (*q).first;
			}
		}

		if (maxValue > 0 && maxPos.x > 0) {
			ret.push_back(maxPos);
		}
		else {
			break;
		}
	}

	return ret;
}

void CombatControl::setUseMineState(Position p, int state) {
	for (auto q = minePos.begin(); q != minePos.end(); q++) {
		if ((*q).first == p) {
			(*q).second.second = state;
		}
	}
}

bool CombatControl::isNearChokePoint(MyUnit* unit) {
	float minDist = 9999;
	for (auto c : BWTA::getChokepoints()) {
		float dist = getDistance(c->getCenter(), unit->getPosition());
		if (minDist > dist) {
			minDist = dist;
		}
	}

	if (minDist < 128) {
		return true;
	}
	else {
		return false;
	}
}
void CombatControl::kitingMove(MyUnit* unit, Unit target, Position targetPos) {
	if (target != NULL) {
		if (unit->self->isAttacking() == false) {
			if (target->getType().isBuilding()) {
				if (gameFrame % 24 == 0)
					unit->self->attack(target, false);
			}
			else {
				if (getDistance(target->getPosition(), unit->getPosition()) < 80) {
					Position p = MapInformation::getInstance()->getNextAvoidMovePos(unit->getPosition(), targetPos);
					unit->self->attack(p, false);
				}
				else {
					if (unit->self->isAttacking() == false)
						unit->self->attack(target, false);
				}
			}
		}
		else {
			Position p = MapInformation::getInstance()->getNextAvoidMovePos(unit->getPosition(), targetPos);
			unit->self->move(p, false);
		}
	}
	else {
		Position p = MapInformation::getInstance()->getNextAvoidMovePos(unit->getPosition(), targetPos);
		unit->self->move(p, false);
	}
}

void CombatControl::settingDefenseObject() {
	if (info->map->getMyDirection() == 0) {

		defenseTankPos.push_back(TilePosition(21, 30));
		defenseTankPos.push_back(TilePosition(21, 30));

		bangMission = (TilePosition(4, 31));

		DefensePos[0] = TilePosition(11, 35);
		DefensePos[1] = TilePosition(36, 44);
		firstGoPos = TilePosition(38, 46);
		DefensePos[2] = TilePosition(27, 36);

		waitTankPos = TilePosition(20, 38);
		threeDragonPos = TilePosition(52, 7);
	}
	else if (info->map->getMyDirection() == 1) {

		defenseTankPos.push_back(TilePosition(101, 19));
		defenseTankPos.push_back(TilePosition(93, 17));

		bangMission = (TilePosition(93, 4));

		DefensePos[0] = TilePosition(92, 11);
		DefensePos[1] = TilePosition(86, 30);
		firstGoPos = TilePosition(84, 32);
		DefensePos[2] = TilePosition(96, 24);

		waitTankPos = TilePosition(93, 17);
		threeDragonPos = TilePosition(119, 53);
	}
	else if (info->map->getMyDirection() == 2) {

		defenseTankPos.push_back(TilePosition(25, 111));
		defenseTankPos.push_back(TilePosition(26, 106));		

		bangMission = (TilePosition(33, 123));

		DefensePos[0] = TilePosition(30, 113);
		DefensePos[1] = TilePosition(41, 98);
		firstGoPos = TilePosition(43, 96);
		DefensePos[2] = TilePosition(31, 104);

		waitTankPos = TilePosition(35, 110);
		threeDragonPos = TilePosition(9, 74);
	}
	else if (info->map->getMyDirection() == 3) {

		defenseTankPos.push_back(TilePosition(106, 98));
		defenseTankPos.push_back(TilePosition(105, 98));

		bangMission = (TilePosition(123, 96));

		DefensePos[0] = TilePosition(114, 92);
		DefensePos[1] = TilePosition(92, 85);
		firstGoPos = TilePosition(90, 83);
		DefensePos[2] = TilePosition(100, 94);

		waitTankPos = TilePosition(107, 92);
		threeDragonPos = TilePosition(78, 120);
	}

	if (info->map->getEnemyStartBase() != NULL) {

		if (info->map->getEnemyDirection() == 0) {
			tightPos = TilePosition(38, 46);
			tightBunkerPos.clear();
			tightTankPos.clear();
			undunkTankPos = TilePosition(17, 32);
			tightWaitBionicPos = TilePosition(29, 36);

			tightBunkerPos.push_back(TilePosition(26, 40));
			tightBunkerPos.push_back(TilePosition(26, 38));
			tightBunkerPos.push_back(TilePosition(23, 40));
			tightBunkerPos.push_back(TilePosition(23, 38));
			tightBunkerPos.push_back(TilePosition(23, 36));
			tightBunkerPos.push_back(TilePosition(23, 34));

			tightTankPos.push_back(TilePosition(27, 35));
			tightTankPos.push_back(TilePosition(30, 37));
			tightTankPos.push_back(TilePosition(30, 39));
			tightTankPos.push_back(TilePosition(30, 41));
			tightTankPos.push_back(TilePosition(28, 42));
			tightTankPos.push_back(TilePosition(30, 43));
			tightTankPos.push_back(TilePosition(32, 43));
			tightTankPos.push_back(TilePosition(34, 43));
			tightTankPos.push_back(TilePosition(33, 45));

			if (info->map->getMyDirection() == 1) {
				middlePos = TilePosition(63, 43);
				middleGoPos = TilePosition(61, 43);
			}
			else if (info->map->getMyDirection() == 2) {
				middlePos = TilePosition(37, 71);
				middleGoPos = TilePosition(37, 73);
			}
			else if (info->map->getMyDirection() == 3) {
				middlePos = TilePosition(65, 66);
				middleGoPos = TilePosition(65, 64);
			}

		}
		else if (info->map->getEnemyDirection() == 1) {
			tightPos = TilePosition(84, 32);

			tightBunkerPos.clear();
			tightTankPos.clear();
			undunkTankPos = TilePosition(98, 16);
			tightWaitBionicPos = TilePosition(98, 26);

			tightBunkerPos.push_back(TilePosition(94, 25));
			tightBunkerPos.push_back(TilePosition(94, 23));
			tightBunkerPos.push_back(TilePosition(88, 23));
			tightBunkerPos.push_back(TilePosition(91, 23));
			tightBunkerPos.push_back(TilePosition(88, 21));
			tightBunkerPos.push_back(TilePosition(91, 21));

			tightTankPos.push_back(TilePosition(87, 25));
			tightTankPos.push_back(TilePosition(89, 26));
			tightTankPos.push_back(TilePosition(91, 26));
			tightTankPos.push_back(TilePosition(93, 26));
			tightTankPos.push_back(TilePosition(87, 27));
			tightTankPos.push_back(TilePosition(89, 28));
			tightTankPos.push_back(TilePosition(91, 28));
			tightTankPos.push_back(TilePosition(93, 28));
			tightTankPos.push_back(TilePosition(95, 28));
			tightTankPos.push_back(TilePosition(97, 28));
			tightTankPos.push_back(TilePosition(99, 28));
			tightTankPos.push_back(TilePosition(100, 26));
			tightTankPos.push_back(TilePosition(98, 26));
			tightTankPos.push_back(TilePosition(98, 24));
			tightTankPos.push_back(TilePosition(85, 28));
			tightTankPos.push_back(TilePosition(87, 29));
			tightTankPos.push_back(TilePosition(89, 30));
			tightTankPos.push_back(TilePosition(91, 30));
			tightTankPos.push_back(TilePosition(83, 29));
			tightTankPos.push_back(TilePosition(85, 30));

			if (info->map->getMyDirection() == 0) {
				middlePos = TilePosition(65, 42);
				middleGoPos = TilePosition(67, 42);
			}
			else if (info->map->getMyDirection() == 2) {
				middlePos = TilePosition(65, 66);
				middleGoPos = TilePosition(65, 64);
			}
			else if (info->map->getMyDirection() == 3) {
				middlePos = TilePosition(90, 59);
				middleGoPos = TilePosition(90, 57);
			}
		}
		else if (info->map->getEnemyDirection() == 2) {
			tightPos = TilePosition(43, 96);

			tightBunkerPos.clear();
			tightTankPos.clear();
			undunkTankPos = TilePosition(28, 112);
			tightWaitBionicPos = TilePosition(30, 102);

			tightBunkerPos.push_back(TilePosition(39, 102));
			tightBunkerPos.push_back(TilePosition(36, 102));
			tightBunkerPos.push_back(TilePosition(33, 102));
			tightBunkerPos.push_back(TilePosition(34, 104));
			tightBunkerPos.push_back(TilePosition(37, 104));

			tightTankPos.push_back(TilePosition(42, 100));
			tightTankPos.push_back(TilePosition(44, 100));
			tightTankPos.push_back(TilePosition(44, 98));
			tightTankPos.push_back(TilePosition(42, 98));
			tightTankPos.push_back(TilePosition(40, 97));
			tightTankPos.push_back(TilePosition(40, 99));
			tightTankPos.push_back(TilePosition(40, 101));
			tightTankPos.push_back(TilePosition(38, 99));
			tightTankPos.push_back(TilePosition(38, 101));
			tightTankPos.push_back(TilePosition(36, 99));
			tightTankPos.push_back(TilePosition(36, 101));
			tightTankPos.push_back(TilePosition(34, 99));
			tightTankPos.push_back(TilePosition(34, 101));
			tightTankPos.push_back(TilePosition(32, 99));
			tightTankPos.push_back(TilePosition(32, 101));
			tightTankPos.push_back(TilePosition(30, 99));
			tightTankPos.push_back(TilePosition(30, 101));

			if (info->map->getMyDirection() == 0) {
				middlePos = TilePosition(37, 67);
				middleGoPos = TilePosition(37, 69);
			}
			else if (info->map->getMyDirection() == 1) {
				middlePos = TilePosition(65, 66);
				middleGoPos = TilePosition(67, 66);
			}
			else if (info->map->getMyDirection() == 3) {
				middlePos = TilePosition(66, 89);
				middleGoPos = TilePosition(64, 89);
			}
		}
		else if(info->map->getEnemyDirection() == 3) {
			tightPos = TilePosition(91, 83);

			tightBunkerPos.clear();
			tightTankPos.clear();
			undunkTankPos = TilePosition(109, 97);
			tightWaitBionicPos = TilePosition(99, 93);

			tightBunkerPos.push_back(TilePosition(99, 87));
			tightBunkerPos.push_back(TilePosition(99, 89));
			tightBunkerPos.push_back(TilePosition(102, 87));
			tightBunkerPos.push_back(TilePosition(102, 89));
			tightBunkerPos.push_back(TilePosition(102, 91));
			tightBunkerPos.push_back(TilePosition(102, 93));

			tightTankPos.push_back(TilePosition(95, 83));
			tightTankPos.push_back(TilePosition(93, 83));
			tightTankPos.push_back(TilePosition(91, 83));
			tightTankPos.push_back(TilePosition(90, 85));
			tightTankPos.push_back(TilePosition(92, 85));
			tightTankPos.push_back(TilePosition(94, 85));
			tightTankPos.push_back(TilePosition(96, 85));
			tightTankPos.push_back(TilePosition(98, 85));
			tightTankPos.push_back(TilePosition(98, 87));
			tightTankPos.push_back(TilePosition(96, 87));
			tightTankPos.push_back(TilePosition(94, 87));
			tightTankPos.push_back(TilePosition(92, 87));
			tightTankPos.push_back(TilePosition(94, 89));
			tightTankPos.push_back(TilePosition(96, 89));
			tightTankPos.push_back(TilePosition(98, 89));
			tightTankPos.push_back(TilePosition(98, 91));
			tightTankPos.push_back(TilePosition(96, 91));
			tightTankPos.push_back(TilePosition(94, 91));
			tightTankPos.push_back(TilePosition(94, 93));
			tightTankPos.push_back(TilePosition(96, 93));
			tightTankPos.push_back(TilePosition(98, 93));

			if (info->map->getMyDirection() == 0) {
				middlePos = TilePosition(65, 66);
				middleGoPos = TilePosition(65, 68);
			}
			else if (info->map->getMyDirection() == 1) {
				middlePos = TilePosition(85, 60);
				middleGoPos = TilePosition(87, 60);
			}
			else if (info->map->getMyDirection() == 2) {
				middlePos = TilePosition(62, 90);
				middleGoPos = TilePosition(64, 90);
			}
		}
		if (defenseTankPos.size() > 0) {
			enemyChoke = getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition());
			defenseSetting = true;
			for (auto pos : tightBunkerPos) {
				tightValue[pos] = 0;
				tightReady[pos] = 0;
			}
			for (auto pos : tightTankPos) {
				tightValue[pos] = 0;
				tightReady[pos] = 0;
			}
		}
	}
}

bool CombatControl::isEnemyUnitComeTrue(MyUnit* unit) {

	float minDist = 9999;
	float minUDist = 9999;
	bool isUniderAttack = false;
	float closeUnits = 0;
	for (auto enemy : info->enemy->getAllUnits()) {
		Position expectUnitPos = enemy->getPosition();

		expectUnitPos.x += enemy->getVelocityX() * 18;
		expectUnitPos.y += enemy->getVelocityY() * 18;

		float dist = getDistance(expectUnitPos, unit->getPosition());
		float udist = getDistance(enemy->getPosition(), unit->self->getPosition());

		if (dist < minDist) {
			minDist = dist;
		}
		if (udist < minUDist) {
			minUDist = udist;
		}
		if (unit->self->isUnderAttack()) {
			isUniderAttack = true;
		}
		if (udist < 16) {
			closeUnits++;
		}
	}

	if (closeUnits > 1) {
		return false;
	}
	if (minDist < 20 || minUDist < 20 || unit->self->isUnderAttack()) {
		return true;
	}

	return false;
}

void CombatControl::joiningTeam(MyUnit* m, TeamControl* t) {
	float dist = getDistance(m->getPosition(), t->teamCenterPos);
	if (dist < 200) {
		m->isInTeam = true;
	}
}

void CombatControl::setHelpMe(MyUnit* m) {
	if (m->getType() != UnitTypes::Terran_Marine) return;

	int fullp = UnitTypes::Terran_Marine.maxHitPoints();
	int unitp = m->self->getHitPoints();
	
	float percent = (float)unitp / fullp;

	if (percent < 0.4) m->helpMe = true;
	else if (m->helpMe && percent > 0.8) m->helpMe = false;
}


void CombatControl::update() {

	if (info->map->getEnemyStartBase() == NULL) return;

	// tankPos 
	for (auto tankPos : tightTankPos) {
		bool atPos = false;
		int nearTankCount = 0;
		int nearBuilding = 0;
		int nearUnit = 0;
		int cannonScore = 0;
		int value = 0;

		for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
			float dist = getDistance((Position)tankPos, tank->getPosition());
			// 탱크 있는지 점수
			if (dist < 30) {
				atPos = true;
			}
			// 근처에 다른 탱크 점수
			if (dist < 80) {
				nearTankCount++;
			}
		}

		for (auto eBuilding : info->enemy->getAllBuildings()) {
			float dist = getDistance(eBuilding->getPosition(), (Position)tankPos);
			if (dist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
				nearBuilding++;
			}
		}

		for (auto eUnit : info->enemy->getAllUnits()) {
			float dist = getDistance(eUnit->getPosition(), (Position)tankPos);
			if (dist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
				nearUnit++;
			}
		}

		for (auto cannon : info->enemy->getUnits(UnitTypes::Protoss_Photon_Cannon)) {
			float dist = getDistance(cannon->getPosition(), (Position)tankPos);

			if (dist < UnitTypes::Protoss_Photon_Cannon) {
				cannonScore -= 30;
			}
			else if (dist < UnitTypes::Terran_Siege_Tank_Siege_Mode) {
				cannonScore += 5;
			}
			else if(dist < UnitTypes::Terran_Siege_Tank_Siege_Mode && dist > UnitTypes::Protoss_Photon_Cannon){
				cannonScore += 40;
			}
		}

		if (atPos) {
			value = -1;
			tightReady[tankPos] = -1;
		}
		else value = max(100 - nearBuilding*30 - nearUnit*4 - nearTankCount*4 + cannonScore, 0);

		tightValue[tankPos] = value;
		tightReady[tankPos] = min(tightReady[tankPos], 1);
	}
	

	// BunkerPos
	for (auto bunkerPos : tightBunkerPos) {
		bool atPos = false;
		
		Position bunker = (Position)bunkerPos;
		bunker = Position(bunker.x + UnitTypes::Terran_Bunker.width(), bunker.y + UnitTypes::Terran_Bunker.height());

		for (auto b : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
			float bdist = getDistance(b->getPosition(), bunker);
			if (bdist < 30) atPos = true;
		}
		for (auto b : info->self->getImmediatelyBuilding(UnitTypes::Terran_Bunker)) {
			float bdist = getDistance(b->getPosition(), bunker);
			if (bdist < 30) atPos = true;
		}

		float dist = getDistance(bunker, (Position)tightPos);

		if (BWAPI::Broodwar->canBuildHere(bunkerPos, UnitTypes::Terran_Bunker, nullptr, false) == false) {
			atPos = true;
		}
		
		if (atPos) {
			tightValue[bunkerPos] = -1;
			tightReady[bunkerPos] = -1;
		}
		else {
			tightValue[bunkerPos] = (int)dist;
			tightReady[bunkerPos] = min(tightReady[bunkerPos], 1);
		}
	}

}

TilePosition CombatControl::getTightTankPosition() {
	TilePosition ret = TilePosition(-1, -1);

	int maxTankPos = -1;
	for (auto tank : tightTankPos) {
		if (maxTankPos < tightValue[tank] && tightValue[tank] >= 0 && tightReady[tank] == 0) {
			maxTankPos = tightValue[tank];
			ret = tank;
		}
	}

	//tightReady[ret] = 1;
	return ret;
}

TilePosition CombatControl::getTightBunkerPosition() {
	TilePosition ret = TilePosition(-1, -1);

	int minBunkerPos = -1;
	for (auto bunker : tightBunkerPos) {
		if (tightValue[bunker] > 0 && tightReady[bunker] == 0) {
			minBunkerPos = tightValue[bunker];
			ret = bunker;
			break;
		}
	}

	tightReady[ret] = 1;
	return ret;
}

void CombatControl::setMarinePos(){
	switch (info->map->getMyDirection())
	{
	case 0:
		marinePosition.push_back(tile2pos(TilePosition(11, 26)));
		marinePosition.push_back(tile2pos(TilePosition(7, 26)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(14, 27)) + Position(0, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 0));
		break;
	case 1:
		marinePosition.push_back(tile2pos(TilePosition(103, 8)));
		marinePosition.push_back(tile2pos(TilePosition(100, 6)));
		marinePosition.push_back(tile2pos(TilePosition(101, 6)));
		marinePosition.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
		marinePosition.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));
		break;
	case 2:
		marinePosition.push_back(tile2pos(TilePosition(24, 121)));
		marinePosition.push_back(tile2pos(TilePosition(23, 117)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(27, 123)));
		marinePosition.push_back(tile2pos(TilePosition(28, 123)));
		marinePosition.push_back(tile2pos(TilePosition(31, 121)) + Position(16, 16));
		break;

	case 3:
		marinePosition.push_back(tile2pos(TilePosition(118, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 99)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 100)) + Position(16, 16));
		break;
	}
}

void CombatControl::marineHold(MyUnit* unit)
{
	if (!isAlmostReachedDest(unit->self->getPosition(), unit->targetPosition, TILEPOSITION_SCALE, TILEPOSITION_SCALE))
	{
		if (gameFrame % 3 == 0)
		{
			unit->self->move(unit->targetPosition);
		}
	}
	else{
		if (unit->self->isHoldingPosition() == false)
		{
			unit->self->holdPosition();
		}
	}
}

void CombatControl::marineHold1(TeamControl *t)
{
	int count = 0;
	for (auto m = t->marineSet.begin(); m != t->marineSet.end(); m++)
	{
		if ((*m)->self->isCompleted() && (*m)->self->exists())
		{
			if (count < marinePosition.size())
			{
				if (isAlmostReachedDest((*m)->getPosition(), marinePosition[count], 2, 2))
				{
					if (!(*m)->self->isHoldingPosition())
					{
						(*m)->self->holdPosition();
					}
				}
				else
				{
					(*m)->self->move(marinePosition[count]);
				}
			}
		}
		count++;
	}
}
void CombatControl::setMarineHoldPosition(TeamControl *t)
{
	int count = 0;
	for (auto marine : t->marineSet)
	{
		if (marine->targetPosition == Position(-1, -1))
		{
			marine->targetPosition = marinePosition[count];
		}
		else{
			count++;
		}
		
		if (count > 5){
			break;
		}
	}
}

bool CombatControl::CheckEnemyOnce()
{
	auto enemy = info->map->getCloseEnemy();
	bool ArmyinGroup = false;

	for (auto e : enemy)
	{
		if (!e->getType().isWorker())
		{
			ArmyinGroup = true;
		}
	}

	if (ArmyinGroup == false)
	{
		//all worker : enemy
		return false;
	}
	else{
		return true;
	}

}

bool CombatControl::isAlone(MyUnit* unit, TeamControl* t) {
	bool ret = false;

	float tcdist = getDistance(unit->getPosition(), t->tankCenterPos);
	float dist = getNearestTankDistance(unit);

	if (dist < 57.5 || tcdist < 129.768) return false;
	
	return true;
}

Position CombatControl::getNearestEnemyGroup(MyUnit* unit) {
	Position ret = Position(-1, -1);

	if (info->enemy->enemyGroup.size() > 0) {
		float minDist = 99999;
		for (auto pos = info->enemy->enemyGroup.begin(); pos != info->enemy->enemyGroup.end(); pos++) {
			float dist = getDistance(unit->getPosition(), pos->first);
			if (minDist > dist) {
				minDist = dist;
				ret = pos->first;
			}
		}
	}

	return ret;
}

Position CombatControl::getNearestMarinePos(MyUnit* unit, TeamControl * t) {
	Position ret = Position(-1, -1);

	float minDist = 99999;
	for (auto pos : t->marineSet) {
		float dist = getDistance(unit->getPosition(), pos->getPosition());
		if (minDist > dist) {
			minDist = dist;
			ret = pos->getPosition();
		}
	}


	return ret;
}

MyUnit* CombatControl::getFrontTank(TeamControl* t, Position targetPos) {
	MyUnit* ret = NULL;

	float minDist = 9999;
	for (auto tank : t->tankSet) {
		float dist = getDistance(tank->getPosition(), targetPos);
		if (minDist > dist) {
			ret = tank;
			minDist = dist;
		}
	}

	return ret;
}

MyUnit* CombatControl::getFrontMarine(TeamControl* t, Position targetPos) {
	MyUnit* ret = NULL;

	float minDist = 9999;
	for (auto tank : t->marineSet) {
		float dist = getDistance(tank->getPosition(), targetPos);
		if (minDist > dist) {
			ret = tank;
			minDist = dist;
		}
	}

	return ret;
}

bool CombatControl::isEnemyFront() {

	int count = 0;

	for (auto enemy : info->map->getMyTerritorys().back()->getCloseEnemy()) {
		if (enemy->getType() == UnitTypes::Protoss_Observer) continue;
		if (enemy->getType() == UnitTypes::Protoss_Probe) continue;
		count++;
	}

	if (count > 0) return true;
	else return false;
}

bool CombatControl::isUnduck(TeamControl* t) {

	bool checkthisout = false;

	//-- 위치 ->  탱크 위치 unduckTankPos 이상함
	BWTA::BaseLocation* base1;
	for (auto base : BWTA::getStartLocations()) {
		Position p = UnitControl::getInstance()->finishNextPos;
		if (base->getPosition() == p) {
			checkthisout = true;
			base1 = base;
		}
	}

	if (checkthisout) {
		int index = info->map->getDirection(base1);
		switch (index)
		{
		case 0:
			undunkTankPos = TilePosition(17, 32);
			break;
		case 1:
			undunkTankPos = TilePosition(98, 16);
			break;
		case 2:
			undunkTankPos = TilePosition(28, 112);
			break;
		case 3:
			undunkTankPos = TilePosition(109, 97);
			break;
		default:
			break;
		}
	}
	else {
		return false;
	}
	float dist = getDistance(t->teamCenterPos, (Position)undunkTankPos);

	bool check = false;
	for (auto base : BWTA::getStartLocations()) {
		if (UnitControl::getInstance()->finishNextPos == base->getPosition()) {
			check = true;
		}
	}
	if(!check) return false;

	int totalCount = t->marineSet.size() + t->medicSet.size() + t->tankSet.size();
	int unduckCount = 0;
	for (auto marine : t->marineSet) {
		if (BWAPI::Broodwar->getGroundHeight(marine->getTilePosition()) == 2) {
			unduckCount++;
		}
	}
	for (auto medic : t->medicSet) {
		if (BWAPI::Broodwar->getGroundHeight(medic->getTilePosition()) == 2) {
			unduckCount++;
		}
	}
	for (auto tank : t->tankSet) {
		if (BWAPI::Broodwar->getGroundHeight(tank->getTilePosition()) == 2) {
			unduckCount++;
		}
	}

	if ((float)unduckCount / (float)totalCount > 0.65f) {
		return false;
	}
	else if (dist < 250 && BWAPI::Broodwar->getGroundHeight((TilePosition)t->teamCenterPos) != 2 && info->enemy->getBuildingCount(UnitTypes::Protoss_Nexus) < 2 && dist > 125) {
		if (unduckSetting == false) {
			unduckTanks.clear();
			unduckSetting = true;
		}
		return true;
	}
	else{
		return false;
	}
}

void CombatControl::onUNDUK(TeamControl* t) {
	Unitset enemy = getNearEnemyUnits(t);

	bool checkthisout = false;
	BWTA::BaseLocation* base1;
	for (auto base : BWTA::getStartLocations()) {
		Position p = UnitControl::getInstance()->finishNextPos;
		if (base->getPosition() == p) {
			BWAPI::Broodwar->drawCircleMap(p, 50, Colors::White, true);
			checkthisout = true;
			base1 = base;
		}
	}

	if (checkthisout) {
		int index = info->map->getDirection(base1);
		printf("index %d\n", index);
		switch (index)
		{
		case 0:
			undunkTankPos = TilePosition(17, 32);
			break;
		case 1:
			undunkTankPos = TilePosition(98, 16);
			break;
		case 2:
			undunkTankPos = TilePosition(28, 112);
			break;
		case 3:
			undunkTankPos = TilePosition(109, 97);
			break;
		default:
			break;
		}
	}
	BWAPI::Broodwar->drawCircleMap((Position)undunkTankPos, 50, Colors::White, true);
	int count = t->tankSet.size() / 2;
	unduckTanks.clear();
	while (count > 0) {
		float minDist = 9999;
		MyUnit* tu = NULL;
		for (auto tank : t->tankSet) {
			bool check = false;

			for (auto unduck : unduckTanks) {
				if (tank == unduck) {
					check = true;
				}
			}

			if (check)
				continue;

			float dist = getDistance(tank->getPosition(), (Position)undunkTankPos);
			if (dist < minDist) {
				minDist = dist;
				tu = tank;
			}
		}

		if (tu != NULL) {
			count--;
			unduckTanks.push_back(tu);
		}
		else {
			break;
		}
	}

	auto chkPoint = getNearestChokepoint(t->attackPosition);
	auto enemyFront = getNearestBaseLocation(chkPoint->getCenter())->getPosition();
	Position goPos;
	int tankZoen = 120;
	if (info->map->getEnemyDirection() == 0) goPos = (Position)TilePosition(10, 35);
	if (info->map->getEnemyDirection() == 1) {
		goPos = (Position)TilePosition(92, 10);
		tankZoen = 80;
	}
	if (info->map->getEnemyDirection() == 2) goPos = (Position)TilePosition(35, 119);
	if (info->map->getEnemyDirection() == 3) goPos = (Position)TilePosition(117, 94);

	int utankCount = 0;
	int uMarineCount = 0;
	int utank2Count = 0;


	for (auto tank : unduckTanks) {
		if (tank->getType() != UnitTypes::Terran_Siege_Tank_Siege_Mode) continue;
		float dist = getDistance(tank->getPosition(), (Position)undunkTankPos);
		if (dist < 180) utankCount++;
	}
	for (auto tank : t->tankSet) {
		float dist = getDistance(tank->getPosition(), enemyFront);
		if (dist < 180) utank2Count++;
	}
	for (auto marine : t->marineSet) {
		float dist = getDistance(marine->getPosition(), goPos);
		if (dist < 180) uMarineCount++;
	}

	if (!unduckWait && utankCount + utank2Count > 5 && uMarineCount > 10) {
		unduckWait = true;
	}

	if (unduckWait) {
		printf("올라간다아아아아아아아\n");
		for (auto tank : t->tankSet) {
			bool checkUT = false;

			for (auto t : unduckTanks) {
				if (tank == t) checkUT = true;
			}

			if (checkUT) continue;

			tank->self->move(info->map->getEnemyStartBase()->getPosition(), false);
		}
		for (auto marine : t->marineSet) {
			marine->self->move(info->map->getEnemyStartBase()->getPosition(), false);
		}
		for (auto medic : t->medicSet) {
			medic->self->move(info->map->getEnemyStartBase()->getPosition(), false);
		}
		return;
	}




	int utCount = 0;
	for (auto tank : unduckTanks) {
		float dist = getDistance((Position)undunkTankPos, tank->getPosition());
		if (dist < tankZoen && tank->self->isSieged()) {
			utCount++;
		}
	}
	

	for (auto tank : t->tankSet) {
		bool checkUT = false;

		for (auto t : unduckTanks) {
			if (tank == t) 
				checkUT = true;
		}

		if (checkUT) continue;

		Unit target = getTargetUnit(tank, enemy);
		if (tank->self->isSieged()) {
			tank->self->unsiege();
			continue;
		}
		if (target != NULL) {
			if (tank->self->getGroundWeaponCooldown() == 0) {
				tank->self->attack(target, false);
			}
			else {
				tank->self->move(enemyFront, false);
			}
		}
		else {
			if(gameFrame % 12 ==  0)
				tank->self->move(enemyFront, false);
		}
	}

	for (auto marine : t->marineSet) {
		Unit target = getTargetUnit(marine, enemy);

		float dist = getDistance(marine->getPosition(), goPos);
		
		if (target != NULL && canAttack(marine)) {
			marine->self->attack(target, false);
		}
		else if(target == NULL && canAttack(marine)) {
			marine->self->move(goPos, false);
		}
	}

	for (auto medic : t->medicSet) {
		medic->self->move(goPos, false);
	}

	for (auto tank : unduckTanks) {
		float dist = getDistance(tank->getPosition(), (Position)undunkTankPos);
		if (dist < tankZoen) {
			Unit target = getMassedUnit(tank, enemy);
			if (tank->self->isSieged() == false) {
				tank->self->siege();
			}
			else if (target != NULL && tank->self->getGroundWeaponCooldown() == 0) {
				tank->self->attack(target, false);
			}
		}
		else {
			if (tank->self->isSieged()) {
				tank->self->unsiege();
			}
			if (gameFrame % 12 == 0) {
				tank->self->move((Position)undunkTankPos, false);
			}
		}
	}
}

Position CombatControl::getAvoidStormPos(MyUnit* unit, TeamControl* t) {
	Position ret = t->teamCenterPos;


	float minDist = 999999;
	for (auto marine : t->marineSet) {
		if (marine->self->isUnderStorm()) continue;
		else {
			float dist = getDistance(unit->getPosition(), marine->getPosition());
			if (dist < minDist) {
				minDist = dist;
				ret = marine->getPosition();
			}
		}
	}
	for (auto marine : t->tankSet) {
		if (marine->self->isUnderStorm()) continue;
		else {
			float dist = getDistance(unit->getPosition(), marine->getPosition());
			if (dist < minDist) {
				minDist = dist;
				ret = marine->getPosition();
			}
		}
	}

	return ret;
}