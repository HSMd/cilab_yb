#include "PositionMgr.h"
#include "Instance.h"
#include "Work.h"
#include "Utility.h"

#define LEFT 0
#define RIGHT 1
#define DOWN 2
#define UP 3

PositionMgr* PositionMgr::getInstance(){

	static PositionMgr instance;
	return &instance;
}

void PositionMgr::init(){

	auto startBase = info->map->getMyStartBase();

	for (int i = 0; i<128; i++){
		for (int j = 0; j<128; j++){
			buildMap[i][j] = true;
		}
	}

	switch (info->map->getMyDirection()) {

	case 0:
		uploadPosition(POS::ENGINE_SIGHT_1, TilePosition(21, 36));

		uploadPosition(POS::SIM1_SUPPLY, TilePosition(11, 26));
		uploadPosition(POS::SIM1_SUPPLY2, TilePosition(8, 26));
		uploadPosition(POS::SIM1_BARRACK, TilePosition(4, 28));

		uploadPosition(POS::SECOND_CENTER, TilePosition(16, 25));
		uploadPosition(POS::THIRD_CENTER, TilePosition(21, 10));

		uploadPosition(POS::FRONT_BARRACK, TilePosition(18, 36));
		uploadPosition(POS::FRONT_SUPPLY, TilePosition(21, 34));
		uploadPosition(POS::FRONT_BUNKER, TilePosition(22, 38));
		uploadPosition(POS::FRONT_ENGINE, TilePosition(18, 40));

		uploadPosition(POS::FIRST_SUPPLY, info->map->getMyStartBase()->getTilePosition() + TilePosition(0, 4));
		uploadPosition(POS::FIRST_FACTORY, TilePosition(1, 18));
		uploadPosition(POS::SECOND_FACTORY, TilePosition(7, 18));
		uploadPosition(POS::THIRD_FACTORY, TilePosition(1, 22));

		uploadPosition(POS::START_TURRET, TilePosition(8, 9));
		uploadPosition(POS::HILL_TURRET, TilePosition(21, 25));
		uploadPosition(POS::SIM1_TURRET, TilePosition(5, 26));
		uploadPosition(POS::SIM2_TURRET, TilePosition(16, 36));
		uploadPosition(POS::FRONT_TURRET, TilePosition(8, 44));
		uploadPosition(POS::AROUND1_TURRET, TilePosition(26, 18));
		uploadPosition(POS::AROUND2_TURRET, TilePosition(24, 8));
		uploadPosition(POS::AROUND3_TURRET, TilePosition(24, 0));

		uploadPosition(POS::ACADEMY, TilePosition(1, 0));
		uploadPosition(POS::ARMORY, TilePosition(4, 0));
		uploadPosition(POS::ENGINEERING, TilePosition(1, 14));

		uploadPosition(POS::SIDE_BARRACK, startBase->getTilePosition() + TilePosition(4, 0));
		uploadPosition(POS::DOWN_SUPPLY, startBase->getTilePosition() + TilePosition(0, 3));

		uploadPosition(POS::TANK_FINISH_WAIT, TilePosition(17, 32));

		break;

	case 1:

		uploadPosition(POS::ENGINE_SIGHT_1, TilePosition(94, 17));

		uploadPosition(POS::SIM1_SUPPLY, TilePosition(97, 5));
		uploadPosition(POS::SIM1_SUPPLY2, TilePosition(100, 7));
		uploadPosition(POS::SIM1_BARRACK, TilePosition(102, 9));

		uploadPosition(POS::SECOND_CENTER, TilePosition(97, 1));
		uploadPosition(POS::THIRD_CENTER, TilePosition(118, 26));

		uploadPosition(POS::FRONT_BARRACK, TilePosition(89, 16));
		uploadPosition(POS::FRONT_SUPPLY, TilePosition(93, 18));
		uploadPosition(POS::FRONT_BUNKER, TilePosition(90, 20));
		uploadPosition(POS::FRONT_ENGINE, TilePosition(85, 17));

		uploadPosition(POS::FIRST_SUPPLY, info->map->getMyStartBase()->getTilePosition() + TilePosition(0, 4));
		uploadPosition(POS::THIRD_FACTORY, TilePosition(103, 2));
		uploadPosition(POS::SECOND_FACTORY, TilePosition(109, 2));
		uploadPosition(POS::FIRST_FACTORY, TilePosition(109, 6));

		uploadPosition(POS::START_TURRET, TilePosition(119, 10));
		uploadPosition(POS::HILL_TURRET, TilePosition(103, 16));
		uploadPosition(POS::SIM1_TURRET, TilePosition(101, 5));
		uploadPosition(POS::SIM2_TURRET, TilePosition(90, 16));
		uploadPosition(POS::FRONT_TURRET, TilePosition(80, 9));
		uploadPosition(POS::AROUND1_TURRET, TilePosition(109, 21));
		uploadPosition(POS::AROUND2_TURRET, TilePosition(115, 26));
		uploadPosition(POS::AROUND3_TURRET, TilePosition(123, 32));

		uploadPosition(POS::ACADEMY, TilePosition(121, 0));
		uploadPosition(POS::ARMORY, TilePosition(113, 23));

		uploadPosition(POS::ENGINEERING, TilePosition(110, 21));
		uploadPosition(POS::SIDE_BARRACK, startBase->getTilePosition() + TilePosition(-4, 0));
		uploadPosition(POS::DOWN_SUPPLY, startBase->getTilePosition() + TilePosition(0, 3));

		uploadPosition(POS::TANK_FINISH_WAIT, TilePosition(97, 16));

		break;

	case 2:

		uploadPosition(POS::ENGINE_SIGHT_1, TilePosition(30, 109));

		uploadPosition(POS::SIM1_SUPPLY, TilePosition(23, 118));
		uploadPosition(POS::SIM1_SUPPLY2, TilePosition(28, 121));
		uploadPosition(POS::SIM1_BARRACK, TilePosition(24, 120));

		uploadPosition(POS::SECOND_CENTER, TilePosition(29, 124));
		uploadPosition(POS::THIRD_CENTER, TilePosition(4, 99));

		uploadPosition(POS::FRONT_BARRACK, TilePosition(32, 110));
		uploadPosition(POS::FRONT_SUPPLY, TilePosition(32, 108));
		uploadPosition(POS::FRONT_BUNKER, TilePosition(35, 107));
		uploadPosition(POS::FRONT_ENGINE, TilePosition(38, 108));

		uploadPosition(POS::FIRST_SUPPLY, info->map->getMyStartBase()->getTilePosition() + TilePosition(0, 4));
		uploadPosition(POS::FIRST_FACTORY, TilePosition(14, 112));
		uploadPosition(POS::SECOND_FACTORY, TilePosition(14, 116));
		uploadPosition(POS::THIRD_FACTORY, TilePosition(24, 124));

		uploadPosition(POS::START_TURRET, TilePosition(11, 115));
		uploadPosition(POS::HILL_TURRET, TilePosition(22, 113));
		uploadPosition(POS::SIM1_TURRET, TilePosition(24, 123));
		uploadPosition(POS::SIM2_TURRET, TilePosition(34, 114));
		uploadPosition(POS::FRONT_TURRET, TilePosition(46, 116));
		uploadPosition(POS::AROUND1_TURRET, TilePosition(26, 105));
		uploadPosition(POS::AROUND2_TURRET, TilePosition(16, 102));
		uploadPosition(POS::AROUND3_TURRET, TilePosition(6, 97));

		uploadPosition(POS::ACADEMY, TilePosition(0, 123));
		uploadPosition(POS::ARMORY, TilePosition(3, 123));
		uploadPosition(POS::ENGINEERING, TilePosition(19, 105));

		uploadPosition(POS::SIDE_BARRACK, startBase->getTilePosition() + TilePosition(4, 0));
		uploadPosition(POS::DOWN_SUPPLY, startBase->getTilePosition() + TilePosition(0, 3));

		uploadPosition(POS::TANK_FINISH_WAIT, TilePosition(29, 112));

		break;

	case 3:

		uploadPosition(POS::ENGINE_SIGHT_1, TilePosition(105, 94));

		uploadPosition(POS::SIM1_SUPPLY, TilePosition(120, 97));
		uploadPosition(POS::SIM1_SUPPLY2, TilePosition(118, 99));
		uploadPosition(POS::SIM1_BARRACK, TilePosition(114, 101));

		uploadPosition(POS::SECOND_CENTER, TilePosition(123, 96));
		uploadPosition(POS::THIRD_CENTER, TilePosition(106, 120));

		uploadPosition(POS::FRONT_BARRACK, TilePosition(105, 92));
		uploadPosition(POS::FRONT_SUPPLY, TilePosition(109, 91));
		uploadPosition(POS::FRONT_BUNKER, TilePosition(103, 89));
		uploadPosition(POS::FRONT_ENGINE, TilePosition(106, 86));

		uploadPosition(POS::FIRST_SUPPLY, info->map->getMyStartBase()->getTilePosition() + TilePosition(0, 4));
		uploadPosition(POS::THIRD_FACTORY, TilePosition(122, 100));
		uploadPosition(POS::SECOND_FACTORY, TilePosition(122, 104));
		uploadPosition(POS::FIRST_FACTORY, TilePosition(122, 108));

		uploadPosition(POS::START_TURRET, TilePosition(117, 120));
		uploadPosition(POS::HILL_TURRET, TilePosition(104, 101));
		uploadPosition(POS::SIM1_TURRET, TilePosition(119, 101));
		uploadPosition(POS::SIM2_TURRET, TilePosition(111, 92));
		uploadPosition(POS::FRONT_TURRET, TilePosition(116, 82));
		uploadPosition(POS::AROUND1_TURRET, TilePosition(124, 95));
		uploadPosition(POS::AROUND2_TURRET, TilePosition(100, 114));
		uploadPosition(POS::AROUND3_TURRET, TilePosition(102, 123));

		uploadPosition(POS::ACADEMY, TilePosition(122, 124));
		uploadPosition(POS::ARMORY, TilePosition(125, 124));

		uploadPosition(POS::ENGINEERING, TilePosition(101, 107));
		uploadPosition(POS::SIDE_BARRACK, startBase->getTilePosition() + TilePosition(-4, 0));
		uploadPosition(POS::DOWN_SUPPLY, startBase->getTilePosition() + TilePosition(0, 3));

		uploadPosition(POS::TANK_FINISH_WAIT, TilePosition(108, 95));

		break;
	}
}

void PositionMgr::updateMarinePosition(){

	switch (info->map->getMyDirection())
	{
	case 0:
		marinePosition.push_back(tile2pos(TilePosition(11, 26)));
		marinePosition.push_back(tile2pos(TilePosition(7, 26)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(14, 27)) + Position(0, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 0));
		break;
	case 1:
		marinePosition.push_back(tile2pos(TilePosition(103, 8)));
		marinePosition.push_back(tile2pos(TilePosition(100, 6)));
		marinePosition.push_back(tile2pos(TilePosition(101, 6)));
		marinePosition.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
		marinePosition.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));
		break;
	case 2:
		marinePosition.push_back(tile2pos(TilePosition(24, 121)));
		marinePosition.push_back(tile2pos(TilePosition(23, 117)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(27, 123)));
		marinePosition.push_back(tile2pos(TilePosition(28, 123)));
		marinePosition.push_back(tile2pos(TilePosition(31, 121)) + Position(16, 16));
		break;

	case 3:
		marinePosition.push_back(tile2pos(TilePosition(118, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 99)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 100)) + Position(16, 16));
		break;
	}
}

void PositionMgr::show(){

}

void PositionMgr::draw(){

}

void PositionMgr::run(){

}

TilePosition PositionMgr::getProperPosition(UnitType type, bool upload){

	TilePosition pos;

	if (UnitTypes::Terran_Factory == type){
		pos = getFactoryPosition();
	}
	else if (UnitTypes::Terran_Supply_Depot == type){
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		pos = getSupplyPosition();
	}
	else{

	}

	if (upload)
	{
		planUpload(type, pos);
	}



	return pos;
}

TilePosition PositionMgr::getSupplyPosition(){

	TilePosition start = TilePosition(0, 0);
	TilePosition pos = TilePosition(0, 0);
	TilePosition next;

	auto sd = UnitTypes::Terran_Supply_Depot;
	int turn = 0;
	int turn_dir = 0;
	int count = 0;

	if (info->map->getMyDirection() == 0){
		start = TilePosition(14, 1);
		pos = TilePosition(14, 1);

		turn = 3;
		turn_dir = DOWN;
		next = TilePosition(sd.tileWidth(), 0);
	}
	else if (info->map->getMyDirection() == 1){
		start = TilePosition(117, 16);
		pos = TilePosition(117, 16);

		turn = 3;
		turn_dir = DOWN;
		next = TilePosition(sd.tileWidth(), 0);
	}
	else if (info->map->getMyDirection() == 2){
		start = TilePosition(1, 108);
		pos = TilePosition(1, 108);

		turn = 4;
		turn_dir = UP;
		next = TilePosition(sd.tileWidth(), 0);
	}
	else if (info->map->getMyDirection() == 3){
		start = TilePosition(113, 122);
		pos = TilePosition(113, 122);

		turn = 3;
		turn_dir = UP;
		next = TilePosition(-sd.tileWidth(), 0);
	}

	while (!canBuildHere(sd, pos)){
		pos = start;

		int temp = count / turn;
		int line = count % turn;

		if (0 < temp){
			for (int i = 0; i < temp; i++){

				switch (turn_dir){
				case LEFT:
					pos += TilePosition(-sd.tileWidth(), 0);
					break;
				case RIGHT:
					pos += TilePosition(sd.tileWidth(), 0);
					break;
				case DOWN:
					pos += TilePosition(0, sd.tileHeight());
					break;
				case UP:
					pos += TilePosition(0, -sd.tileHeight());
					break;
				}
			}
		}

		if (0 < line){
			for (int i = 0; i < line; i++){
				if (info->map->getMyDirection() == 3){
					if (i % 2 == 0){
						pos -= TilePosition(1, 0);
					}
					pos -= TilePosition(sd.tileWidth(), 0);
				}
				else{
					if (i % 2 == 0){
						pos += TilePosition(1, 0);
					}
					pos += TilePosition(sd.tileWidth(), 0);
				}
				
			}
		}

		count++;
	}

	return pos;
}



TilePosition PositionMgr::getFactoryPosition(){

	TilePosition start = TilePosition(0, 0);
	TilePosition pos = TilePosition(0, 0);
	TilePosition next;
	auto type = UnitTypes::Terran_Factory;

	int turn_dir = 0;
	int turn = 0;

	if (canBuildHere(type, tilePos[POS::FIRST_FACTORY])){
		return tilePos[POS::FIRST_FACTORY];
	}
	else if (canBuildHere(type, tilePos[POS::SECOND_FACTORY])){
		return tilePos[POS::SECOND_FACTORY];
	}
	else if (canBuildHere(type, tilePos[POS::THIRD_FACTORY])){
		return tilePos[POS::THIRD_FACTORY];
	}

	if (info->map->getMyDirection() == 0){

		start = TilePosition(16, 20);
		pos = TilePosition(16, 20);

		next = TilePosition(0, -type.tileHeight());
		turn = 3;
		turn_dir = RIGHT;
	}
	else if (info->map->getMyDirection() == 1){

		start = TilePosition(108, 11);
		pos = TilePosition(108, 11);

		next = TilePosition(0, type.tileHeight());
		turn = 3;
		turn_dir = RIGHT;
	}
	else if (info->map->getMyDirection() == 2){////////////////////////////////////////////////////////////////////////

		start = TilePosition(12, 124);
		pos = TilePosition(12, 124);

		next = TilePosition(0, -type.tileHeight());
		turn = 2 ;
		turn_dir = RIGHT;
	}
	else if (info->map->getMyDirection() == 3){

		start = TilePosition(106, 103);
		pos = TilePosition(106, 103);

		next = TilePosition(0, type.tileHeight());
		turn = 3;
		turn_dir = RIGHT;
	}

	int count = 0;

	while (!canBuildHere(type, pos)){
		pos = start;

		int temp = count / turn;
		int line = count % turn;

		if (0 < temp){
			for (int i = 0; i < temp; i++){

				switch (turn_dir){
				case LEFT:
					pos += TilePosition(-type.tileWidth(), 0);
					break;
				case RIGHT:
					pos += TilePosition(type.tileWidth(), 0);
					break;
				case DOWN:
					pos += TilePosition(0, type.tileHeight());
					break;
				case UP:
					pos += TilePosition(0, -type.tileHeight());
					break;
				}
			}
		}

		if (0 < line){
			for (int i = 0; i < line; i++){
				pos += next;
				//pos += TilePosition(type.tileWidth(), 0);
			}
		}		

		count++;
	}

	return pos;
}

void PositionMgr::createBuilding(Unit unit){

	int x = unit->getTilePosition().x;
	int y = unit->getTilePosition().y;

	int width = unit->getType().tileWidth();
	int height = unit->getType().tileHeight();

	for (int i = 0; i < height; i++){
		for (int j = 0; j < width; j++){
			buildMap[y + i][x + j] = false;
		}
	}
}

void PositionMgr::destroyBuilding(Unit unit){

	int x = unit->getTilePosition().x;
	int y = unit->getTilePosition().y;

	int width = unit->getType().tileWidth();
	int height = unit->getType().tileHeight();

	for (int i = 0; i < height; i++){
		for (int j = 0; j < width; j++){
			buildMap[y + i][x + j] = true;
		}
	}
}

bool PositionMgr::canBuildHere(UnitType type, TilePosition pos){

	if (buildMap[pos.y][pos.x] == true){
		return true;
	}

	return false;
}

void PositionMgr::planUpload(UnitType type, TilePosition pos) {

	int x = pos.x;
	int y = pos.y;

	int width = type.tileWidth();
	int height = type.tileHeight();

	if (1000 <= pos.x) {
		cout << type.c_str() << endl;
		cout << pos.x << " " << pos.y << endl;
	}

	for (int i = 0; i < height; i++){
		for (int j = 0; j < width; j++){
			buildMap[y + i][x + j] = false;
		}
	}
}

TilePosition PositionMgr::getGasPosition(Territory* territory){
	
	int minDis = 99999;
	TilePosition re;

	for (auto gas : info->resource->gases) {
		int dis = getDistance(gas->getTilePosition(), territory->getTilePosition());

		if (dis < minDis) {
			minDis = dis;
			re = gas->getTilePosition();
		}
	}

	return re;
}

TilePosition PositionMgr::getGasPosition(TilePosition pos){
	int minDis = 99999;
	TilePosition re;

	for (auto gas : info->resource->gases) {
		int dis = getDistance(gas->getTilePosition(), pos);

		if (dis < minDis) {
			minDis = dis;
			re = gas->getTilePosition();
		}
	}

	return re;
}