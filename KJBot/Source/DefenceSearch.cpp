#include "DefenceSearch.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

DefenceSearch::DefenceSearch(DEFENSE_TYPE type) : DefenceEvent(type)
{	
	
}

DefenceSearch::~DefenceSearch()
{

}

void DefenceSearch::chkFire(){

	auto probeCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Probe);
	auto zealotCount = info->map->getCloseCount(UnitTypes::Protoss_Zealot);
	auto DragoonCount = info->map->getCloseCount(UnitTypes::Protoss_Dragoon);
	if (0 < probeCount && (zealotCount == 0 || DragoonCount == 0)){
		fire = true;
	}
}

void DefenceSearch::update(){

	auto probeCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Probe);

	if (probeCount == 1){
		getUnits(UnitTypes::Terran_Marine, 2);

		if (defenceUnits[UnitTypes::Terran_Marine].size() == 0){
			getUnits(UnitTypes::Terran_SCV, probeCount);
		}
		else{
			eraseUnits(UnitTypes::Terran_SCV);
		}
	}
	else{
		getUnits(UnitTypes::Terran_Marine, 2);
		getUnits(UnitTypes::Terran_SCV, probeCount);
	}	
}

void DefenceSearch::run()
{	
	chkFire();
	turnOff();
	if (!fire){
		return;
	}	

	bw << defenceUnits[UnitTypes::Terran_SCV].size() << ", " << defenceUnits[UnitTypes::Terran_Marine].size() << endl;

	update();
	act();
}

void DefenceSearch::act(){
	
	auto& close = info->map->getCloseEnemy();
	Unit attackTarget = nullptr;

	// Ÿ�� ����.
	for (auto enemy : close){
		if (attackTarget == nullptr){
			attackTarget = enemy;
		}
		else{
			if (enemy->getHitPoints() < attackTarget->getHitPoints()){
				attackTarget = enemy;
			}
		}
	}

	if (attackTarget == nullptr){
		return;
	}

	for (auto iter : defenceUnits){
		UnitType type = iter.first;
		
		for (auto unit : defenceUnits[type]){

			if (!unit->self->exists() || !unit->self->isCompleted()){
				continue;
			}

			//if(!unit->self->isAttackFrame())
			if (unit->self->canAttackUnit())
			{
				unit->self->attack(attackTarget);
			}
		}
	}
}

void DefenceSearch::turnOff(){

	auto probeCount = info->map->getMyTerritorys()[0]->getCloseCount(UnitTypes::Protoss_Probe);

	if (probeCount == 0){
		eraseUnits(UnitTypes::Terran_Marine);
		eraseUnits(UnitTypes::Terran_SCV);

		fire = false;
	}	
}