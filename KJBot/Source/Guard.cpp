#include "Guard.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

#include "DefenceSearch.h"
#include "DefencePylon.h"
#include "DefenceGas.h"
#include "DefenceZealot.h"
#include "DefenceRepair.h"
#include "DefenceByScv.h"
#include "DefenceZealot2.h"
#include "TankRepair.h"

Guard::Guard()
{	
	events.push_back(new DefenceSearch(DEFENSE_TYPE::ENEMY_SEARCH));
	events.push_back(new DefenceGas(DEFENSE_TYPE::GAS_TERROR));
	events.push_back(new DefencePylon(DEFENSE_TYPE::PYLON_RUSH));
	events.push_back(new DefenceZealot(DEFENSE_TYPE::ZEALOT_RUSH));
	events.push_back(new DefenceZealot2(DEFENSE_TYPE::ZEALOT_RUSH2));
	events.push_back(new DefenceRepair(DEFENSE_TYPE::REPAIR_BUILDING));
	events.push_back(new TankRepair(DEFENSE_TYPE::TANK_REPAIR));
	//events.push_back(new DefenceByScv(DEFENSE_TYPE::SCV_GUARD));

}

Guard::~Guard()
{

}

Guard* Guard::getInstance() {

	static Guard instance;
	return &instance;
}

void Guard::init(){
	
	for (auto e : events){
		e->init();
	}
}

void Guard::show(){

	for (auto e : events){
		e->show();
	}
}

void Guard::update(){

}

void Guard::run(){

	for (auto e : events){
		/*
		if (e->getType() == DEFENSE_TYPE::ENEMY_SEARCH){
			continue;
		}
		*/
		
		e->run();
	}
}

bool Guard::isError(){
	
	bool re = false;

	for (auto e : events){
		if (e->isFired())
			re = true;
	}

	return re;
}
