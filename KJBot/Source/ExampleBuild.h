#pragma once
#include "BWAPI.h"
#include "MyUnit.h"
#include "Work.h"

#include <map>
#include <vector>

using namespace BWAPI;
using namespace std;

class ExampleBuild{

	enum FLAG{
		ATTAK,
	};

	vector<BuildWork> builds;
	vector<MyUnit*> moveBuildings;

	map<FLAG, bool> flags;

public: 
	ExampleBuild(){};
	~ExampleBuild(){};

	void init();
	void show();
	void draw();
	void run();
};