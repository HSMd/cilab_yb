#pragma once
#include <BWAPI.h>
#include <BWTA.h>

using namespace std;
using namespace BWAPI;

class Singletone
{
private:
	Singletone();
	~Singletone();
	static Singletone *s_Singletone;

public:
	static Singletone * create();
	static Singletone * getInstance();
	void init();
	void onFrame();
	void draw();
	void onUnitCreate(Unit);
	void onUnitDestroy(Unit);
	void onUnitComplete(Unit);

};

