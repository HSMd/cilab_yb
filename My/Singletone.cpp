#include "Singletone.h"

Singletone* Singletone::s_Singletone;
Singletone::Singletone() {
	init();
}

Singletone::~Singletone() {

}

Singletone* Singletone::create() {
	if (s_Singletone) {
		return s_Singletone;
	}

	s_Singletone = new Singletone();
	return s_Singletone;
}

Singletone* Singletone::getInstance() {
	return s_Singletone;
}

void Singletone::init() {

}
