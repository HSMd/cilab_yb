#include "CombatControl.h"
#include "Information.h"
#include "Instance.h"
#include "Self.h"
#include "Utility.h"

CombatControl* CombatControl::s_CombatControl;
CombatControl::CombatControl() {
	init();
}

CombatControl::~CombatControl() {

}

CombatControl* CombatControl::create() {
	static CombatControl instance;
	return &instance;
}

CombatControl* CombatControl::getInstance() {
	static CombatControl instance;
	return &instance;
}

void CombatControl::init() {
	p1 = 5.0f;
	p2 = 7.0f;
	p3 = 4.0f;

	tightPos = TilePosition(0, 0);

	//setMinePos();
}

void CombatControl::onFrame(TeamControl* t) {
	gameFrame = BWAPI::Broodwar->getFrameCount();
	//updateMinePosValue();
	if (!defenseSetting) {
		settingDefenseObject();
	}

	if ((*t).state == TeamControl::STATE::BCN_DOUBLE) {
		// 시작 후, 벙커 지어지기 전
		onDOUBLE(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_BUNKER) {
		// 벙커 지어진 후, 시즈 탱크가 나오기전
		onBUNKER(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_TANK) {
		// 탱크가 나온 후, 진출 전
		onTANK(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_GOING) {
		// 진출 후, 앞마당 까지 도달하기 전
		onGOING(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_TIGHT) {
		// 앞마당 도달 후, 조이기
		onTIGHT(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_JOIN) {
		// 후속 병력 합류
		onJOIN(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_BJ) {
		// 멀티                                    견제
		onBJ(t);
	}
	else if ((*t).state == TeamControl::STATE::BCN_FINISH) {
		// 마무으리
		onFINISH(t);
	}
}

void CombatControl::onDOUBLE(TeamControl* t) {
	int WorkerCount = 0;
	int ArmyCount = 0;
	
	/*cout << bw->getUnitsInRadius(info->map->getMyTerritorys().back()->getPosition(), info->map->getMyTerritorys().back()->getRadius()* TILEPOSITION_SCALE, Filter::IsEnemy).size() << endl;
	cout << bw->getUnitsInRadius(info->map->getMyTerritorys().front()->getPosition(), info->map->getMyTerritorys().front()->getRadius()* TILEPOSITION_SCALE, Filter::IsEnemy).size() << endl;*/
	
	vector<Territory*> tmp = info->map->getMyTerritorys();
	Unitset enemies;
	for (auto t : tmp)
	{
		enemies = bw->getUnitsInRadius(t->getPosition(), t->getRadius()*TILEPOSITION_SCALE, Filter::IsEnemy);
		if (!enemies.empty())
			break;
	}
	for (auto e : enemies)
	{
		if (e->getType().isWorker())
		{
			WorkerCount++;
		}
		else
		{
			if (!e->getType().isBuilding())
			{
				ArmyCount++;
			}
		}
	}

	//marine 있는 경우
	if (!t->marineSet.empty())
	{
		for (auto marine : t->marineSet)
		{
			Unit enemy = getTargetUnit(marine, enemies);
			if (enemy != NULL)
			{
				if (!marine->self->isAttacking())
				{
					marine->self->attack(enemy);
				}
			}
			else
			{
				if (gameFrame % 24 == 0)
				{
					marine->self->move(Position(DefensePos[0]));
				}
			}
		}
	}
	
	if (t->scvSet.empty())
	{
		if (ArmyCount <= 0 && WorkerCount > 0)
		{
			for (int i = 0; i < WorkerCount; i++)
			{
				MyUnit *temp = info->map->getMyTerritorys().front()->getWorker(true);
				t->scvSet.push_back(temp);
			}
		}
	}
	
	for (auto scv : t->scvSet)
	{
		int minenemyHP = 9999;
		Unit target = NULL;

		if (WorkerCount > 0 || ArmyCount > 0)
		{
			for (auto enemy : enemies)
			{
				if (enemy->getHitPoints() < minenemyHP)
				{
					minenemyHP = enemy->getHitPoints();
					target = enemy;
				}
			}

			if (target != NULL)
			{
				if (!scv->self->isAttacking())
				{
					scv->self->attack(target);
				}
			}
		}
		else{
			info->map->getNearTerritory(scv->getTilePosition())->addScv(scv);
			UnitControl::getInstance()->getUnit(scv);
		}
	}
}

void CombatControl::onBUNKER(TeamControl* t) {
	Unitset enemy = getNearEnemyUnits(t);

	int dragoonCount = 0;
	int zealotCount = 0;
	for (auto e : enemy) {
		if (e->getType() == UnitTypes::Protoss_Dragoon) {
			dragoonCount++;
		}
		else if (e->getType() == UnitTypes::Protoss_Zealot) {
			zealotCount++;
		}
	}

	Unit repairBunker = NULL;
	Unit repairTank = NULL;

	for (auto q : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		if(q->self->getHitPoints()  < q->self->getType().maxHitPoints())
			repairBunker = q->self;	
	}

	for (auto q : t->tankSet) {
		if (q->self->getHitPoints()  < q->self->getType().maxHitPoints())
			repairTank = q->self;
	}

	if (dragoonCount + zealotCount > t->scvSet.size() && t->scvSet.size() < 8) {
		MyUnit* scv = NULL;

		scv = info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BUNKER))->getWorker(true);
		
		if(scv != NULL)
			t->scvSet.push_back(scv);
	}

	int scvcC = 0;
	for (auto scv : t->scvSet) {
		if (repairBunker != NULL || repairTank != NULL) {
			if (repairBunker != NULL && dragoonCount + zealotCount >= scvcC) {
				if (repairBunker->getHitPoints() < UnitTypes::Terran_Bunker.maxHitPoints()) {
					if (scv->self->isRepairing() == false) {
						scv->self->repair(repairBunker, false);
					}
					continue;
				}
			}
			if (repairTank != NULL) {
				if (repairTank->getHitPoints() < UnitTypes::Terran_Siege_Tank_Siege_Mode.maxHitPoints()) {
					if (scv->self->isRepairing() == false) {
						scv->self->repair(repairTank, false);
					}
					continue;
				}
			}
		}
		else {
			if (scv->self->isIdle()) {
				float minDist = 9999;
				Unit minDistMineral = NULL;
				for (auto mineral : BWAPI::Broodwar->getMinerals()) {
					float dist = getDistance(scv->getPosition(), mineral->getPosition());
					if (minDist > dist) {
						minDist = dist;
						minDistMineral = mineral;
					}
				}

				if (minDistMineral != NULL) {
					scv->self->rightClick(minDistMineral, false);
				}
			}
		}
		scvcC++;
	}

	int seat = 0;
	for (auto bun : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		seat += 4;
		seat -= bun->self->getLoadedUnits().size();
	}

	for (auto marine : t->marineSet) {
		marine->isInTeam = true;
		Unit target = getTargetUnit(marine, enemy);
		if (seat > 0) {
			for (auto bunker : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
				if (bunker->self->getLoadedUnits().size() < 4) {
					if (BWAPI::Broodwar->getFrameCount() % 13 == 0)
						marine->self->load(bunker->self, false);
					break;
				}
			}
		} 
		else {
			if (target == NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 24 == 0)
					marine->self->move((Position)DefensePos[0], false);
			}
			else {
				if (isEnemyUnitComeTrue(marine)) {
					marine->self->move(info->map->getMyStartBase()->getPosition(), false);
				}
				else {

					float dist = getDistance((Position)DefensePos[0], marine->getPosition());

					if (dist < 120) {
						if (marine->self->isAttackFrame() == false) {
							if (BWAPI::Broodwar->getFrameCount() % 12 == 0)
								marine->self->attack(target, false);
						}
					}
					else {
						marine->self->move((Position)DefensePos[0], false);
					}
				}
			}
		}
	}

	for (auto medic : t->medicSet) {
		Unit healTarget = getHealUnit(medic, t);
		if (healTarget == NULL) {
			if (gameFrame % 48 == 0) {
				medic->self->move((Position)DefensePos[0], false);
			}
		}
		else {
			if(gameFrame % 24 == 0)
				medic->self->useTech(TechTypes::Healing, healTarget);
		}
		joiningTeam(medic, t);
	}

	int tankIndex = 0;
	for (auto tank : t->tankSet) {
		Unit target = getTargetUnit(tank, enemy);
		if (target == NULL || BWAPI::Broodwar->self()->hasResearched(TechTypes::Tank_Siege_Mode)) {
			if (tankIndex < 2) {
				float dist = getDistance(tank->getPosition(), (Position)defenseTankPos[tankIndex]);
				if (dist < 60) {
					if (tank->self->canSiege()) {
						tank->self->siege();
					}
				}
				else {
					tank->self->move((Position)defenseTankPos[tankIndex], false);
				}
			}
			else {
				tank->self->move((Position)DefensePos[0], false);
			}
			tankIndex++;
		}
		else {
			if (isEnemyUnitComeTrue(tank)) {
				if(gameFrame % 3 == 0)
					tank->self->move(info->map->getMyStartBase()->getPosition(), false);
			}
			else {
				float dist = getDistance((Position)defenseTankPos[0], tank->getPosition());
				if (dist < 160) {
					if (tank->self->getSpellCooldown() == 0) {
						tank->self->attack(target, false);
					}
					else {
						if (gameFrame % 12 == 0)
							tank->self->move((Position)defenseTankPos[0], false);
					}
				}
			}
		}
	}
}

void CombatControl::onTANK(TeamControl* t) {
	Unitset enemy = getNearEnemyUnits(t);

	int dragoonCount = 0;
	int zealotCount = 0;
	for (auto e : enemy) {
		if (e->getType() == UnitTypes::Protoss_Dragoon) {
			dragoonCount++;
		}
		else if (e->getType() == UnitTypes::Protoss_Zealot) {
			zealotCount += 1;
		}
	}

	Unit repairBunker = NULL;
	Unit repairTank = NULL;

	for (auto q : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		if (q->self->getHitPoints()  < q->self->getType().maxHitPoints())
			repairBunker = q->self;
	}

	for (auto q : t->tankSet) {
		if (q->self->getHitPoints() < q->self->getType().maxHitPoints())
			repairTank = q->self;
	}

	if (dragoonCount + zealotCount > t->scvSet.size() && t->scvSet.size() < 8) {
		MyUnit* scv = NULL;

		scv = info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BUNKER))->getWorker(true);

		if (scv != NULL)
			t->scvSet.push_back(scv);
	}

	int scvcC = 0;
	for (auto scv : t->scvSet) {
		if (scvcC > zealotCount + dragoonCount) {
			info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BUNKER))->addScv(scv);
		}
		if (repairBunker != NULL || repairTank != NULL) {
			if(repairTank != NULL){
				if (repairTank->getHitPoints() < UnitTypes::Terran_Siege_Tank_Siege_Mode.maxHitPoints() / 4 * 3) {
					if (scv->self->isRepairing() == false) {
						scv->self->repair(repairTank, false);
					}
					continue;
				}
			}
			if (repairBunker != NULL) {
				if (repairBunker->getHitPoints() < UnitTypes::Terran_Bunker.maxHitPoints() / 4 * 3) {
					if (scv->self->isRepairing() == false) {
						scv->self->repair(repairBunker, false);
					}
					continue;
				}
			}
		}
		else {
			if (scv->self->isIdle()) {
				float minDist = 9999;
				Unit minDistMineral = NULL;
				for (auto mineral : BWAPI::Broodwar->getMinerals()) {
					float dist = getDistance(scv->getPosition(), mineral->getPosition());
					if (minDist > dist) {
						minDist = dist;
						minDistMineral = mineral;
					}
				}

				if (minDistMineral != NULL) {
					scv->self->rightClick(minDistMineral, false);
				}
			}
		}
		scvcC++;
	}

	int seat = 0;
	for (auto bun : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		seat += 4;
		seat -= bun->self->getLoadedUnits().size();
	}

	for (auto marine : t->marineSet) {
		marine->isInTeam = true;
		Unit target = getTargetUnit(marine, enemy);
		if (seat > 0) {
			for (auto bunker : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
				if (bunker->self->getLoadedUnits().size() < 4) {
					if (BWAPI::Broodwar->getFrameCount() % 13 == 0)
						marine->self->load(bunker->self, false);
					break;
				}
			}
		}
		else {
			if (target == NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 24 == 0)
					marine->self->move((Position)DefensePos[0], false);
			}
			else {
				if (isEnemyUnitComeTrue(marine)) {
					marine->self->move(info->map->getMyStartBase()->getPosition(), false);
				}
				else {
					if (marine->self->isAttacking() == false && marine->self->isMoving() == false) {
						if (BWAPI::Broodwar->getFrameCount() % 12 == 0)
							marine->self->attack(target, false);
					}
				}
			}
		}
	}

	for (auto medic : t->medicSet) {
		Unit healTarget = getHealUnit(medic, t);
		if (healTarget == NULL) {
			if (gameFrame % 48 == 0) {
				medic->self->move((Position)DefensePos[0], false);
			}
		}
		else {
			if (gameFrame % 24 == 0)
				medic->self->useTech(TechTypes::Healing, healTarget);
		}
		joiningTeam(medic, t);
	}

	int tankIndex = 0;
	for (auto tank : t->tankSet) {
		Unit target = getTargetUnit(tank, enemy);
		if (target == NULL || BWAPI::Broodwar->self()->hasResearched(TechTypes::Tank_Siege_Mode)) {
			if (tankIndex < 2) {
				float dist = getDistance(tank->getPosition(), (Position)defenseTankPos[tankIndex]);
				if (dist < 60) {
					if (tank->self->canSiege()) {
						tank->self->siege();
					}
				}
				else {
					tank->self->move((Position)defenseTankPos[tankIndex], false);
				}
			}
			else {
				tank->self->move((Position)DefensePos[0], false);
			}
			tankIndex++;
		}
		else {
			if (isEnemyUnitComeTrue(tank)) {
				if (gameFrame % 3 == 0)
					tank->self->move(info->map->getMyStartBase()->getPosition(), false);
			}
			else {
				float dist = getDistance((Position)defenseTankPos[0], tank->getPosition());
				if (dist < 160) {
					if (tank->self->getSpellCooldown() == 0) {
						tank->self->attack(target, false);
					}
					else {
						if (gameFrame % 12 == 0)
							tank->self->move((Position)defenseTankPos[0], false);
					}
				}
			}
		}
	}
}

void CombatControl::onGOING(TeamControl* t) {
	Unitset enemy = getNearEnemyUnits(t);

	int scvCount = 0;

	if (t->scvSet.size() < 3) {
		MyUnit* scv = NULL;

		scv = info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BUNKER))->getWorker(true);

		if (scv != NULL)
			t->scvSet.push_back(scv);
	}
	for (auto scv : t->scvSet) {
		if (scvCount > 2) {
			info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BUNKER))->addScv(scv);
			UnitControl::getInstance()->onUnitDestroy(scv->self);
		}
		else {
			Unit repairTarget = getRepairUnit(scv);

			if (repairTarget != NULL) {
				if (scv->self->isRepairing() == false) {
					scv->self->repair(repairTarget, false);
				}
			}
			else {
				scv->self->move(t->teamCenterPos, false);
			}
		}
		scvCount++;
		scv->targetPos = TilePosition(-1, -1);
	}

	for (auto bunker : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		if (bunker->self->getLoadedUnits().size() > 0) {
			bunker->self->unloadAll();
		}
	}

	if (t->goingSetting) {
		int marineCount = t->marineSet.size();
		int team1 = marineCount / 2;
		int team2 = marineCount - team1;
		vector<Position> pos = getVGPosition(t->tankCenterPos, (Position)tightPos, team1, 2, 90.0f, UnitTypes::Terran_Marine.width()*1.5f);
		vector<Position> pos3 = getVGPosition(t->tankCenterPos, (Position)tightPos, team2, 2, 50.0f, UnitTypes::Terran_Marine.width()*1.5f);

		vector<Position> mPos;

		for (auto p : pos) {
			mPos.push_back(p);
		}
		for (auto p : pos3) {
			mPos.push_back(p);
		}

		for (auto marine : t->marineSet) {
			Unit target = getTargetUnit(marine, enemy);
			if (target == NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 12 == 0 && mPos.size() > 0) {
					marine->self->move(mPos.back(), false);
					mPos.pop_back();
				}
				else if (gameFrame % 12 == 0) {
					marine->self->move((Position)tightPos, false);
				}
			}
			else {
				if (marine->self->isAttacking() == false) {
					if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
						marine->self->attack(target, false);
						mPos.pop_back();
					}
				}
			}
			joiningTeam(marine, t);
		}
		for (auto medic : t->medicSet) {
			Unit healTarget = getHealUnit(medic, t);
			if (healTarget == NULL) {
				if (t->bionicCenterPos.x > 0) {
					medic->self->move(t->bionicCenterPos, false);
				}
				else if (t->teamCenterPos.x > 0) {
					medic->self->move(t->teamCenterPos, false);
				}
				else {
					medic->self->move((Position)DefensePos[1], false);
				}
			}
			else {
				if (gameFrame % 24 == 0)
					medic->self->useTech(TechTypes::Healing, healTarget);
			}
			joiningTeam(medic, t);
		}
		int tankIndex = 0;
		for (auto tank : t->tankSet) {
			Unit target = getTargetUnit(tank, enemy);
			if (tank->self->isSieged()) {
				tank->self->unsiege();
			}
			else {
				if (target != NULL) {
					if (tank->self->getGroundWeaponCooldown() == 0) {
						tank->self->attack(target, false);
					}
					else if (isEnemyUnitComeTrue(tank)) {
						if (tank->self->isSieged() == false) {
							tank->self->move(info->map->getMyStartBase()->getPosition(), false);
						}
					}
					else {
						if (tank->self->isSieged() == false) {
							tank->self->move((Position)tightPos, false);
						}
					}
				}
				else {
					if (gameFrame % 20 == 0) {
						tank->self->move((Position)tightPos, false);
					}
				}
			}
			joiningTeam(tank, t);
		}
	}
	else {
		int marineCount = t->marineSet.size();
		int team1 = marineCount / 2;
		int team2 = marineCount - team1;
		vector<Position> pos = getVGPosition(t->tankCenterPos, (Position)DefensePos[1], team1, 2, 90.0f, UnitTypes::Terran_Marine.width());
		vector<Position> pos3 = getVGPosition(t->tankCenterPos, (Position)DefensePos[1], team2, 2, 50.0f, UnitTypes::Terran_Marine.width());

		vector<Position> mPos;

		for (auto p : pos) {
			mPos.push_back(p);
		}
		for (auto p : pos3) {
			mPos.push_back(p);
		}

		for (auto marine : t->marineSet) {
			Unit target = getTargetUnit(marine, enemy);
			if (target == NULL) {
				if (BWAPI::Broodwar->getFrameCount() % 12 == 0 && mPos.size() > 0) {
					marine->self->move(mPos.back(), false);
					mPos.pop_back();
				}
				else if (gameFrame % 12 == 0) {
					marine->self->move((Position)tightPos, false);
				}
			}
			else {
				if (marine->self->isAttacking() == false) {
					if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
						marine->self->attack(target, false);
						mPos.pop_back();
					}
				}
			}
			joiningTeam(marine, t);
		}
		for (auto medic : t->medicSet) {
			Unit healTarget = getHealUnit(medic, t);
			if (healTarget == NULL) {
				if (gameFrame % 48 == 0) {
					if (t->bionicCenterPos.x > 0) {
						medic->self->move(t->bionicCenterPos, false);
					}
					else if (t->teamCenterPos.x > 0) {
						medic->self->move(t->teamCenterPos, false);
					}
					else {
						medic->self->move((Position)DefensePos[1], false);
					}
				}
			}
			else {
				if (gameFrame % 24 == 0)
					medic->self->useTech(TechTypes::Healing, healTarget);
			}
			joiningTeam(medic, t);
		}
		int tankIndex = 0;
		for (auto tank : t->tankSet) {
			Unit target = getTargetUnit(tank, enemy);
			if (tank->self->isSieged()) {
				tank->self->unsiege();
			}
			else {
				if (target != NULL) {
					if (tank->self->getGroundWeaponCooldown() == 0) {
						tank->self->attack(target, false);
					}
					else {
						if (tank->self->isSieged() == false) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
							tank->self->move(p, false);
						}
					}
				}
				else {
					if (gameFrame % 20 == 0) {
						tank->self->move((Position)DefensePos[1], false);
					}
				}
			}
			joiningTeam(tank, t);
		}

		float tdist = getDistance(t->teamCenterPos, (Position)DefensePos[1]);
		if (tdist < 60) {
			t->goingSetting = true;
			for (auto marine : t->marineSet) {
				if (isCanBuySteamPack(marine)) {
					marine->self->useTech(TechTypes::Stim_Packs, false);
				}
			}
		}
	}
}

void CombatControl::onTIGHT(TeamControl* t) {

	Unitset enemies = getNearEnemyUnits(t);

	int scvCount = 0;
	for (auto scv : t->scvSet) {
		if (scvCount > 2) {
			info->map->getNearTerritory(posMgr->getTilePosition(POS::FRONT_BUNKER))->addScv(scv);
			UnitControl::getInstance()->onUnitDestroy(scv->self);
		}
		scvCount++;
	}

	for (auto scv : t->scvSet) {
		if (scv->targetPos.x <= 0) {
			scv->targetPos = getTightBunkerPosition();
		}
		if (scv->state == MyUnit::STATE::BUILD) {
			scvControl->run(scv);
			if (scv->state != MyUnit::STATE::BUILD && scv->self->isIdle()) {
				scv->targetPos = TilePosition(-1, -1);
			}
		}
		else if (scv->self->isIdle()) {
			Unit target = getRepairUnit(scv);
			if (target != NULL) {
				scv->self->repair(target, false);
			}
			else {
				if (scv->targetPos.x <= 0) {
					scv->targetPos = getTightBunkerPosition();
				}
				else {
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Bunker, scv->targetPos, scv, 10, Scheduler::BUILD_SPEED::FAST));
					scv->state = MyUnit::STATE::BUILD;
					//zscv->self->build(UnitTypes::Terran_Bunker, scv->targetPos);
				}
			}
		}
		else {
			if (isEnemyUnitComeTrue(scv) && scv->self->getHitPoints() < scv->getType().maxHitPoints() / 2) {
				if (scv->self->isConstructing())
					scv->self->stop();
				else
					scv->self->move(info->map->getMyStartBase()->getPosition(), false);
			}
		}
	}

	for (auto tank : t->tankSet) {
		Unit target = getTargetUnit(tank, enemies);
		if (tank->self->isSieged()) {
			if (tank->self->getGroundWeaponCooldown() == 0) {
				target = getMassedUnit(tank, enemies);
				if (target != NULL)
					tank->self->attack(target, false);
			}
		}
		else {
			if (target != NULL) {
				float dist = getDistance(target->getPosition(), tank->getPosition());
				if (dist < 64 || isEnemyUnitComeTrue(tank)) {
					if (tank->self->getGroundWeaponCooldown() == 0) {
						tank->self->attack(target, false);
					}
				}
				else {
					TilePosition pos = getTightTankPosition();
					if (pos.x > 0) {
						float tdist = getDistance((Position)pos, tank->getPosition());
						if (tdist < 25) {
							if (tank->self->canSiege()) {
								tank->self->siege();
							}
						}
						else {
							tank->self->move((Position)pos, false);
						}
					}
					else {
						// 자리 배정 못받음
						printf("Notting better\n");
					}
				}
			}
			else {
				TilePosition pos = getTightTankPosition();
				if (pos.x > 0) {
					float tdist = getDistance((Position)pos, tank->getPosition());
					if (tdist < 10) {
						if (tank->self->canSiege()) {
							tank->self->siege();
						}
					}
					else {
						tank->self->move((Position)pos, false);
					}
				}
				else {
					// 자리 배정 못받음
					printf("Notting better\n");
				}
			}
		}
		joiningTeam(tank, t);
	}

	Unitset bunkers;
	bunkers.clear();

	for (auto bunker : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		if (bunker->getTilePosition() == posMgr->getTilePosition(POS::FRONT_BUNKER)) continue;
		bunkers.insert(bunker->self);
	}

	int seat = 0;


	for (auto bunker : bunkers) {
		seat += 4;
		seat -= bunker->getLoadedUnits().size();
	}
	printf("bunkers size %d :: seat %d\n", bunkers.size(), seat);

	for (auto marine : t->marineSet) {
		if (marine->isInTeam) {
			if (marine->self->isLoaded()) continue;
			if (seat > 0) {
				for (auto bunker : bunkers) {
					if (bunker->getLoadedUnits().size() < 4) {
						marine->self->load(bunker, false);
						seat--;
					}
				}
			}
			else {
				Unit target = getTargetUnit(marine, enemies);

				if (target == NULL) {
					TilePosition bPos = getTightBunkerPosition();
					TilePosition tPos = getTightTankPosition();

					float bDist = getDistance((Position)bPos, marine->getPosition());
					float tDist = getDistance((Position)tPos, marine->getPosition());

					if (bDist < 120 || tDist < 60) {
						if (gameFrame % 24 == 0) {
							marine->self->move((Position)tightPos, false);
						}
					}
					else if (tDist > 180) {
						if (gameFrame % 24 == 0) {
							if(t->tankCenterPos.x > 0)
								if (gameFrame % 24 == 0) {
									marine->self->move(t->tankCenterPos, false);
								}
							else
								if (gameFrame % 24 == 0)
									marine->self->move((Position)tightPos, false);
						}
					}
					else {
						if (gameFrame % 24 == 0) {
							marine->self->holdPosition();
						}
					}
				}
				else {
					if (isCanBuySteamPack(marine)) {
						marine->self->useTech(TechTypes::Stim_Packs);
					}
					else {
						if (marine->self->isAttacking() == false && marine->self->isMoving() == false) {
							marine->self->attack(target, false);
						}
						else {
							if (getNearestTankDistance(marine) > 180 && t->tankSet.size() > 0 && marine->self->getHitPoints() < UnitTypes::Terran_Marine.maxHitPoints() / 2) {
								marine->self->move((Position)tightPos, false);
							}
						}
					}
				}
			}
		}
		else {
			////////////////////////////////////////////////////////////////////////가다가 적 만나면
			if (gameFrame % 48 == 0) {
				marine->self->move(t->teamCenterPos, false);
			}
		}
		joiningTeam(marine, t);
	}

	for (auto medic : t->medicSet) {
		Unit healTarget = getHealUnit(medic, t);
		if (healTarget == NULL) {
			if (gameFrame % 48 == 0) {
				if (t->bionicCenterPos.x > 0)
					medic->self->move(t->bionicCenterPos, false);
				else if (t->tankCenterPos.x > 0)
					medic->self->move(t->tankCenterPos, false);
				else
					medic->self->move((Position)tightPos, false);
					
			}
		}
		else {
			if (gameFrame % 24 == 0)
				medic->self->useTech(TechTypes::Healing, healTarget);
		}
		joiningTeam(medic, t);
	}
}

float CombatControl::getNearestTankDistance(MyUnit* unit) {

	float ret = 9999;

	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
		float dist = getDistance(tank->getPosition(), unit->getPosition());
		if (ret > dist) {
			ret = dist;
		}
	}

	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode)) {
		float dist = getDistance(tank->getPosition(), unit->getPosition());
		if (ret > dist) {
			ret = dist;
		}
	}

	return ret;
}

bool CombatControl::isCanBuySteamPack(MyUnit* unit) {
	int hp = unit->self->getHitPoints();
	int maxHp = unit->getType().maxHitPoints();

	if (maxHp / 2 > hp) {
		return false;
	}
	
	if (unit->self->isStimmed()) return false;
	if (isEnemyUnitComeTrue(unit)) return false;

	return true;
}

void CombatControl::onJOIN(TeamControl* t) {

	Unitset enemy = getNearEnemyUnits(t);

	for (auto marine : t->marineSet) {
		Unit target = getTargetUnit(marine, enemy);
		if (target == NULL) {
			if (BWAPI::Broodwar->getFrameCount() % 24 == 0)
				marine->self->move((Position)DefensePos[1], false);
		}
		else {
			if (marine->self->isAttackFrame() == false) {
				if (BWAPI::Broodwar->getFrameCount() % 12 == 0)
					marine->self->attack(target, false);
			}
		}
	}

	for (auto medic : t->medicSet) {
		Unit healTarget = getHealUnit(medic, t);
		if (healTarget == NULL) {
			if (gameFrame % 48 == 0) {
				medic->self->move((Position)DefensePos[1], false);
			}
		}
		else {
			if (gameFrame % 24 == 0)
				medic->self->useTech(TechTypes::Healing, healTarget);
		}
	}

	int tankIndex = 0;
	for (auto tank : t->tankSet) {
		Unit target = getTargetUnit(tank, enemy);
		if (tank->self->isSieged()) {
			tank->self->unsiege();
		}
		else {
			if (target != NULL) {
				if (tank->self->getGroundWeaponCooldown() == 0) {
					tank->self->attack(target, false);
				}
				else {
					if (tank->self->isSieged() == false) {
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
						tank->self->move(p, false);
					}
				}
			}
			else {
				if (gameFrame % 20 == 0) {
					tank->self->move((Position)DefensePos[1], false);
				}
			}
		}
	}

	int mr = 0;
	int tr = 0;

	for (auto marine : t->marineSet) {
		float dist = getDistance(marine->getPosition(), (Position)DefensePos[1]);
		if (dist < 60) {
			marine->isInTeam = true;
			mr++;
		}
	}

	for (auto medic : t->medicSet) {
		float dist = getDistance(medic->getPosition(), (Position)DefensePos[1]);
		if (dist < 60) {
			medic->isInTeam = true;
			mr++;
		}
	}

	for (auto tank : t->tankSet) {
		float dist = getDistance(tank->getPosition(), (Position)DefensePos[1]);
		if (dist < 60) {
			tank->isInTeam = true;
			tr++;
		}
	}

	if (tr > 0 && mr > 8) {
		t->state = TeamControl::STATE::BCN_GOING;
		t->goingSetting = true;

		UnitControl::getInstance()->TCV.push_back(new TeamControl(UnitControl::getInstance()->totalTeamIndex++));
		UnitControl::getInstance()->TCV[UnitControl::getInstance()->totalTeamIndex - 1]->state = TeamControl::STATE::BCN_JOIN;
	}
}

void CombatControl::onBJ(TeamControl* t) {

}

void CombatControl::onFINISH(TeamControl* t) {

}



void CombatControl::draw(TeamControl* t) {

	Unitset enemy = getNearEnemyUnits(t);
	for (auto e : enemy) {
		BWAPI::Broodwar->drawCircleMap(e->getPosition(), 5, Colors::Red, true);
	}

	for (auto tank : t->tankSet) {
		Unit u = getMassedUnit(tank, enemy);
		if (u != NULL) {
			BWAPI::Broodwar->drawLineMap(tank->getPosition(), u->getPosition(), Colors::Red);
			BWAPI::Broodwar->drawCircleMap(u->getPosition(), 30, Colors::Red, false);
			BWAPI::Broodwar->drawCircleMap(u->getPosition(), 20, Colors::Orange, false);
			BWAPI::Broodwar->drawCircleMap(u->getPosition(), 10, Colors::Yellow, false);
			BWAPI::Broodwar->drawCircleMap(u->getPosition(), 2, Colors::Blue, true);

			Position p[4];
			p[0] = Position(u->getPosition().x - 35, u->getPosition().y);
			p[1] = Position(u->getPosition().x + 35, u->getPosition().y);
			p[2] = Position(u->getPosition().x, u->getPosition().y + 35);
			p[3] = Position(u->getPosition().x, u->getPosition().y - 35);

			for (int x = 0; x < 4; x++) {
				BWAPI::Broodwar->drawLineMap(p[x], u->getPosition(), Colors::Red);
			}
		}
	}

	for (auto medic : t->medicSet) {
		Unit target = getHealUnit(medic, t);
		if (target != NULL) {
			BWAPI::Broodwar->drawLineMap(medic->getPosition(), target->getPosition(), Colors::Yellow);
		}
	}

	for (auto marine : t->marineSet) {
		if (isEnemyUnitComeTrue(marine)) {
			if (BWAPI::Broodwar->getFrameCount() % 8 == 0) {
				BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Red, false);
				BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 9, Colors::Red, false);
			}
			else {
				BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 10, Colors::Black, false);
				BWAPI::Broodwar->drawCircleMap(marine->getPosition(), 9, Colors::White, false);
			}
		}
	}

	for (auto tank : t->tankSet) {
		if (isEnemyUnitComeTrue(tank)) {
			if (BWAPI::Broodwar->getFrameCount() % 8 == 0) {
				BWAPI::Broodwar->drawCircleMap(tank->getPosition(), 10, Colors::Red, false);
				BWAPI::Broodwar->drawCircleMap(tank->getPosition(), 9, Colors::Red, false);
			}
			else {
				BWAPI::Broodwar->drawCircleMap(tank->getPosition(), 10, Colors::Black, false);
				BWAPI::Broodwar->drawCircleMap(tank->getPosition(), 9, Colors::White, false);
			}
		}
	}

	//Unitset enemies = getNearEnemyUnits(t);

	/*int vcnt = 0;
	for (auto v : t->vessleList) {
		if (v->self->getEnergy() >= 100 && v->maginNumber > 48) {
			vcnt++;
		}
		if (v->preEnergy - v->self->getEnergy() > 50) {
			v->maginNumber = 0;
		}
		v->preEnergy = v->self->getEnergy();
	}
	vector<std::pair<Position, float>> vempPos = getEMPPositions(t, vcnt, enemies);
	vector<Position> vp = getVGPosition(t->teamCenterPos, t->attackPosition, t->vessleList.size(), 0, t->tankRage, UnitTypes::Terran_Vulture.width() * 8);

	for (auto q = vempPos.begin(); q != vempPos.end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q).first, 32 * 3, Colors::White, false);
	}

	for (auto v : t->vessleList) {
		if (vempPos.size() > 0) {
			if (v->self->getEnergy() >= 100 && getDistance(v->getPosition(), vempPos.back().first) < 256 * 2.5f && v->maginNumber > 48) {
				//if (BWAPI::Broodwar->getFrameCount() % 24 == 0) {
				v->self->useTech(TechTypes::EMP_Shockwave, vempPos.back().first);
				vempPos.pop_back();
			}
		}
		else {
			if (t->tankSet.size() > 0) {
				if (vp.size() > 0) {
					Position ppp = MapInformation::getInstance()->getAirNextAvoidMovePos(v->getPosition(), vp.back());
					vp.pop_back();
					v->self->move(ppp);
				}
				else {
					Position ppp = MapInformation::getInstance()->getAirNextAvoidMovePos(v->getPosition(), t->teamCenterPos);
					v->self->move(ppp);
				}
			}
			else {
				Position ppp = MapInformation::getInstance()->getAirNextAvoidMovePos(v->getPosition(), t->teamCenterPos);
				v->self->move(ppp);
			}
		}
	}
	*/
	
	/*for (auto p = minePos.begin(); p != minePos.end(); p++) {
		
		if ((*p).second.second == MINE_READY) {
			BWAPI::Broodwar->drawCircleMap((*p).first, 5, Colors::Orange, false);
		}
		else if ((*p).second.second == MINE_DONE) {
			BWAPI::Broodwar->drawCircleMap((*p).first, 5, Colors::Orange, false);
			BWAPI::Broodwar->drawCircleMap((*p).first, 7, Colors::Orange, false);
		}
		else if ((*p).second.first < 0) {
			BWAPI::Broodwar->drawCircleMap((*p).first, 5, Colors::Red, false);
		}
		else {
			BWAPI::Broodwar->drawCircleMap((*p).first, 5, Colors::Green, false);
		}
	}

	vector<Position> mineVec = getMinePos(5);
	for (auto m : mineVec) {
		BWAPI::Broodwar->drawCircleMap((m), 7, Colors::Yellow, false);
		BWAPI::Broodwar->drawCircleMap((m), 5, Colors::Yellow, true);
	}
	*/

	for (auto enemy : info->enemy->getAllUnits()) {
		Position expectUnitPos = enemy->getPosition();

		expectUnitPos.x += enemy->getVelocityX() * 18;
		expectUnitPos.y += enemy->getVelocityY() * 18;

		BWAPI::Broodwar->drawLineMap(enemy->getPosition(), expectUnitPos, Colors::Red);
	}

	if (info->map->getEnemyStartBase() != NULL && t->team == 0) {
		BWAPI::Broodwar->drawCircleMap((Position)DefensePos[0], 10, Colors::Blue, false);
		BWAPI::Broodwar->drawCircleMap((Position)DefensePos[0], 12, Colors::Green, false);

		BWAPI::Broodwar->drawCircleMap((Position)DefensePos[1], 10, Colors::Blue, false);
		BWAPI::Broodwar->drawCircleMap((Position)DefensePos[1], 12, Colors::Cyan, false);

		BWAPI::Broodwar->drawCircleMap((Position)tightPos, 10, Colors::Orange, false);
		BWAPI::Broodwar->drawCircleMap((Position)tightPos, 12, Colors::Red, false);

		for (int x = 0; x < 2; x++) {
			BWAPI::Broodwar->drawCircleMap((Position)defenseTankPos[x], 10, Colors::Blue, false);
			BWAPI::Broodwar->drawCircleMap((Position)defenseTankPos[x], 11, Colors::Cyan, false);
			BWAPI::Broodwar->drawCircleMap((Position)defenseTankPos[x], 12, Colors::Blue, false);
			if (x == 0) {
				BWAPI::Broodwar->drawCircleMap((Position)defenseTankPos[x], 159, Colors::Cyan, false);
				BWAPI::Broodwar->drawCircleMap((Position)defenseTankPos[x], 160, Colors::Blue, false);
				BWAPI::Broodwar->drawCircleMap((Position)defenseTankPos[x], 161, Colors::Cyan, false);
			}
		}

		int maxTankPos = -1;
		Position maxTank;
		for (auto tank : tightTankPos) {
			Position tankPos = (Position)tank;
			BWAPI::Broodwar->drawBoxMap(Position(tankPos.x - UnitTypes::Terran_Siege_Tank_Tank_Mode.width()/2, tankPos.y - UnitTypes::Terran_Siege_Tank_Tank_Mode.height() / 2), 
				Position(tankPos.x + UnitTypes::Terran_Siege_Tank_Tank_Mode.width() / 2, tankPos.y + UnitTypes::Terran_Siege_Tank_Tank_Mode.height() / 2), Colors::Blue, false);

			if (maxTankPos < tightValue[tank]) {
				maxTankPos = tightValue[tank];
				maxTank = tankPos;
			}
		}

		if (maxTankPos != -1) {
			BWAPI::Broodwar->drawCircleMap(maxTank, 10, Colors::Purple, true);
		}

		int minBunkerPos = 99999;
		Position minBunker;
		for (auto bunker : tightBunkerPos) {
			Position bunkerPos = (Position)bunker;
			BWAPI::Broodwar->drawBoxMap(bunkerPos,
				Position(bunkerPos.x + 32*3, bunkerPos.y + 32*2), Colors::Teal, false);

			if (tightValue[bunker] < minBunkerPos) {
				minBunker = Position(bunkerPos.x + UnitTypes::Terran_Bunker.width()/2, bunkerPos.y + UnitTypes::Terran_Bunker.height()/2);
				minBunkerPos = tightValue[bunker];
			}
		}

		if (minBunkerPos != 99999) {
			BWAPI::Broodwar->drawCircleMap(minBunker, 10, Colors::Purple, true);
		}
	}

	if (t->tankCenterPos.x > 0) {
		int marineCount = t->marineSet.size();
		int team1 = marineCount / 2;
		int team2 = marineCount - team1;
		vector<Position> pos = getVGPosition(t->tankCenterPos, (Position)tightPos, team1, 2, 70.0f, UnitTypes::Terran_Marine.width()*1.5f);
		vector<Position> pos3 = getVGPosition(t->tankCenterPos, (Position)tightPos, team2, 2, 40.0f, UnitTypes::Terran_Marine.width()*1.5f);

		vector<Position> mPos;

		for (auto p : pos) {
			mPos.push_back(p);
		}
		for (auto p : pos3) {
			mPos.push_back(p);
		}
		for (auto p : mPos) {
			BWAPI::Broodwar->drawCircleMap(p, 6, Colors::Green, false);
			BWAPI::Broodwar->drawCircleMap(p, 8, Colors::Teal, false);
		}
	}
}


void CombatControl::onUnitCreate(Unit) {

}

void CombatControl::onUnitDestroy(Unit) {

}

void CombatControl::onUnitComplete(Unit) {

}

void CombatControl::onSTRONG_ATTACK(TeamControl* t) {
	if (BWAPI::Broodwar->getFrameCount() % 12 == 0) {
		if (info->enemy->enemyGroup.size() > 0) {
			if ((*t).tankSet.size() > 0 && info->enemy->enemyGroup.size() > 0) {
				if ((*t).vultureSet.size() + (*t).goliathSet.size() > 0) {
					vector <Position> gvp = getVGPosition((*t).tankCenterPos, info->enemy->enemyGroup.front().first, (*t).vultureSet.size() + (*t).goliathSet.size(), (*t).tR, (*t).tankRage, UnitTypes::Terran_Vulture.width());

					for (auto v = (*t).vultureSet.begin(); v != (*t).vultureSet.end(); v++) {
						if (gvp.size() <= 0)
							break;
						(*v)->self->attack(gvp.back(), false);
						gvp.pop_back();
					}
				}
			}
		}
	}
}

void CombatControl::onCRZAY(TeamControl* t) {
	Unitset enemies = getNearEnemyUnits(t);
	bool onlyZealot = isOnlyZealot(t);

	vector<MyUnit*> kingsMan;

	for (auto vulture : t->vultureSet) {
		//합류함?
		Unit target = getTargetUnit(vulture, enemies);
		if (vulture->isInTeam) {
			//탱크있냐?
			if (t->tankSet.size() != 0) {
				float dist = getDistance(vulture->getPosition(), t->tankCenterPos);
				//주변에 탱크 있냐
				if (dist < t->tankRage*1.0f) {
					//공격할애 있냐?
					if (target != NULL) {
						//질럿밖에 없냐?
						if (onlyZealot) {
							//카이팅 ㄱ
							if (vulture->self->getGroundWeaponCooldown() == 0) {
								if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
									if (gameFrame % 24 == 0)
										vulture->self->attack(target, false);
								}
								else {
									if (getDistance(target->getPosition(), vulture->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), (*t).attackPosition);
										vulture->self->move(p, false);
									}
									else
										vulture->self->attack(target, false);
								}
							}
							else {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), (*t).attackPosition);
								vulture->self->move(p, false);
							}
						}
						//질럿말고 딴애도 있냐
						else {
							//자리지키면서 싸워 ㄱㄱㄱㄱㄱㄱ
							kingsMan.push_back(vulture);
						}
					}
					//공격할애 없냐?
					else {
						//건물은 있냐?
						Unit targetB = getTargetBuilding(vulture);
						if (targetB != NULL) {
							//건물 쳐 ㄱㄱㄱㄱㄱㄱㄱ
							if (gameFrame % 24 == 0) {
								vulture->self->attack(targetB, false);
							}
						}
						else {
							//어택포지션으로 ㄱㄱㄱㄱㄱㄱ
							if (gameFrame % 24 == 0) {
								vulture->self->attack(t->attackPosition, false);
							}
						}
					}
				}
				//주변에 탱크 없냐
				else {
					//주변에 적있냐
					if (target != NULL) {
						//탱크쪽으로 카이팅ㄱ
						if (vulture->self->getGroundWeaponCooldown() == 0) {
							if (target->getType() == UnitTypes::Protoss_Photon_Cannon) {			
								vulture->self->move(t->tankCenterPos, false);
							}
							else {
								if (getDistance(target->getPosition(), vulture->getPosition()) < 80) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
									vulture->self->move(p, false);
								}
								else
									vulture->self->attack(target, false);
							}
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
							vulture->self->move(p, false);
						}
					}
					//주변에 적없냐
					else {
						//주변이 초크포인트임?
						if (isNearChokePoint(vulture)) {
							//attackPosition으로 무빙하셈ㄱㄱㄱㄱㄱ
							if (gameFrame % 12 == 0) {
								vulture->self->attack(t->attackPosition, false);
							}
						}
						else {
							//홀드하셈 ㄱㄱㄱㄱㄱㄱ
							float tdist = getDistance(t->attackPosition, t->tankCenterPos);
							float vdist = getDistance(vulture->getPosition(), t->attackPosition);
							if (tdist < vdist) {
								if (gameFrame % 12 == 0) {
									vulture->self->attack(t->attackPosition, false);
								}
							}
							else {
								if (gameFrame % 12 == 0) {
									vulture->self->holdPosition();
								}
							}
						}
					}
				}
			}
			//탱크1도없냐?
			else {
				//카이팅ㄱㄱㄱㄱㄱ
				if (target != NULL) {
					if (vulture->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							if (gameFrame % 24 == 0)
								vulture->self->attack(target, false);
						}
						else {
							if (getDistance(target->getPosition(), vulture->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), (*t).attackPosition);
								vulture->self->move(p, false);
							}
							else
								vulture->self->attack(target, false);
						}
					}
					else {
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), (*t).attackPosition);
						vulture->self->move(p, false);
					}
				}
				else {
					Unit targetB = getTargetBuilding(vulture);
					if (targetB != NULL) {
						if (gameFrame % 24 == 0) {
							vulture->self->attack(targetB, false);
						}
					}
					else {
						if (gameFrame % 24 == 0) {
							vulture->self->attack(t->attackPosition, false);
						}
					}
				}
			}
		}
		//합류안함?
		else {
			//합류하셈ㄱㄱㄱㄱㄱ
			if (vulture->self->getGroundWeaponCooldown() == 0) {
				if (target == NULL) {
					Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
					vulture->self->move(p, false);
				}
				else {
					if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
						if (gameFrame % 24 == 0)
							vulture->self->attack(target, false);
					}
					else {
						if (getDistance(target->getPosition(), vulture->getPosition()) < 80) {
							/*Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
							vulture->self->move(p, false);*/
							vulture->self->attack(target, false);
						}
						else
							vulture->self->attack(target, false);
					}
				}
			}
			else {
				Position p = MapInformation::getInstance()->getNextAvoidMovePos(vulture->getPosition(), t->tankCenterPos);
				vulture->self->move(p, false);
			}

			if (getDistance(vulture->getPosition(), t->tankCenterPos) < t->tankRage*2.f) {
				vulture->isInTeam = true;
			}
		}
	}

	vector <Position> gvp;
	if ((*t).tankSet.size() > 0) {
		gvp = getVGPosition((*t).tankCenterPos, t->attackPosition, kingsMan.size(), (*t).tR, (*t).tankRage, UnitTypes::Terran_Vulture.width());
	}

	for (auto k : kingsMan) {
		Unit target = getTargetUnit(k, enemies);
		if (k->self->getGroundWeaponCooldown() == 0) {
			k->self->attack(target, false);
			if (gvp.size() > 0) {
				gvp.pop_back();
			}
		}
		else {
			if (gvp.size() > 0) {
				k->self->move(gvp.back(), false);
				gvp.pop_back();
			}
		}
	}

	//printf("tankDanger : %d\n", t->tankDanger);
	/*for (auto q = (*t).vultureSet.begin(); q != (*t).vultureSet.end(); q++) {
		Unit target = getTargetUnit((*q), enemies);
		if ((*t).tankSet.size() == 0) {
			if (target == NULL) {
				target = getTargetBuilding((*q));
				if (target == NULL) {
					if (gameFrame % 24 == 0) {
						(*q)->self->attack((*t).attackPosition, false);
					}
				}
				else {
					if (gameFrame % 24 == 0)
						(*q)->self->attack(target, false);
				}
			}
			else {
				if ((*q)->self->getGroundWeaponCooldown() == 0) {
					if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
						if (gameFrame % 24 == 0)
							(*q)->self->attack(target, false);
					}
					else {
						if (getDistance(target->getPosition(), (*q)->getPosition()) < 80) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
							(*q)->self->move(p, false);
						}
						else
							(*q)->self->attack(target, false);
					}
				}
				else {
					Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
					(*q)->self->move(p, false);
				}
			}
		}
		else {
			if (isOnlyZealot(t) && enemies.size() > 0) {
				if (target == NULL) {
					target = getTargetBuilding((*q));
					if (target == NULL) {
						if (gameFrame % 24 == 0) {
							(*q)->self->attack((*t).attackPosition, false);
						}
					}
					else{
						if (gameFrame % 24 == 0)
							(*q)->self->attack(target, false);
					}
				}
				else {
					if ((*q)->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							if (gameFrame % 24 == 0)
								(*q)->self->attack(target, false);
						}
						else {
							if (getDistance(target->getPosition(), (*q)->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
								(*q)->self->move(p, false);
							}
							else
								(*q)->self->attack(target, false);
						}
					}
					else {
						Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
						(*q)->self->move(p, false);
					}
				}
			}
			else {
				if (target != NULL) {
					if ((*q)->self->getGroundWeaponCooldown() == 0) {
						(*q)->self->attack(target, false);
						if (gvp.size() > 0) {
							gvp.pop_back();
						}
					}
					else {
						if (gvp.size() > 0) {
							(*q)->self->move(gvp.back(), false);
							gvp.pop_back();
						}
					}
				}
				else {
					if(BWAPI::Broodwar->getFrameCount() % 12 == 0)
						if (gvp.size() > 0) {
							(*q)->self->move(gvp.back(), false);
							gvp.pop_back();
						}
				}
			}
		}
	}*/


	for (auto q = (*t).goliathSet.begin(); q != (*t).goliathSet.end(); q++) {
		Unit target = getTargetUnit((*q), enemies);
		if ((*t).tankSet.size() == 0) {
			if (target == NULL) {
				if (gameFrame % 24 == 0) {
					(*q)->self->attack((*t).attackPosition, false);
				}
			}
			else {
				if ((*q)->self->getGroundWeaponCooldown() == 0) {
					if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
						if (gameFrame % 24 == 0)
							(*q)->self->attack(target, false);
					}
					else {
						if (getDistance(target->getPosition(), (*q)->getPosition()) < 80) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
							(*q)->self->move(p, false);
						}
						else
							(*q)->self->attack(target, false);
					}
				}
				else {
					Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
					(*q)->self->move(p, false);
				}
			}
		}
		else {
			if (t->tankDanger < 1) {
				if (target == NULL) {
					if (gameFrame % 24 == 0) {
						if (gvp.size() > 0) {
							(*q)->self->attack(gvp.back(), false);
							gvp.pop_back();
						}
						else {
							(*q)->self->attack((*t).attackPosition, false);
						}
					}
				}
				else {
					if ((*q)->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							if (gameFrame % 24 == 0)
								(*q)->self->attack(target, false);
						}
						else {
							if (getDistance(target->getPosition(), (*q)->getPosition()) < 80) {
								if (gvp.size() > 0) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), gvp.back());
									gvp.pop_back();
									(*q)->self->attack(p, false);
								}
								else {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
									(*q)->self->attack(p, false);
								}
							}
							else
								(*q)->self->attack(target, false);
						}
					}
					else {
						if (gvp.size() > 0) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), gvp.back());
							gvp.pop_back();
							(*q)->self->move(p, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos((*q)->getPosition(), (*t).attackPosition);
							(*q)->self->move(p, false);
						}
					}
				}
			}
			else {
				if ((*q)->self->getGroundWeaponCooldown() == 0) {
					(*q)->self->attack(target, false);
				}
				else {
					if (gvp.size() > 0) {
						(*q)->self->move(gvp.back(), false);
						gvp.pop_back();
					}
				}
			}
		}
	}

	Unitset enemies2 = getNearEnemyUnits(t);

	/*for (auto tg : t->TG) {
		//셋다 시즈 상태 입니까???
		if (tg->isSieged()) {
			//타겟팅 사람이 있나여
			if (tg->isTargeting()) {
				//질럿밖에없나?
				if (onlyZealot) {
					//시즈 푸셈ㄱ
					for (auto tank : tg->tankgroup) {
						if (tank->self->isSieged()) {
							tank->self->unsiege();
						}
					}
				}
				//딴거도 있나여?
				else {
					//공격해 셋전부ㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetUnit(tank, enemies);
						if (target != NULL) {
							if (tank->self->isAttacking() == false) {
								tank->self->attack(target, false);
							}
						}
					}
				}
			}
			//암것도 음슴까
			else {
				//건물 공격?
				if (tg->isTargetingBuilding()) {
					//공격해염 ㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetBuilding(tank);
						if (target != NULL) {
							if (tank->self->isAttacking() == false) {
								tank->self->attack(target, false);
							}
						}
					}
				}
				//건물 ㄴㄴ ?
				else {
					//시즈 풀어염ㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱ
					for (auto tank : tg->tankgroup) {
						if (tank->self->isSieged()) {
							tank->self->unsiege();
						}
					}
				}
			}
		}
		//탱크 상태 입니까???
		else {
			//공격할 대상이 있나여
			if (tg->isTargeting()) {
				//질럿만??
				if (onlyZealot) {
					//카이팅 ㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetUnit(tank, enemies);
						if (tank->self->isSieged()) {
							tank->self->unsiege();
						}
						else {
							if (target != NULL) {
								if (tank->self->getGroundWeaponCooldown() == 0) {
									if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
										tank->self->move(p, false);
									}
									else
										tank->self->attack(target, false);
								}
								else {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
									tank->self->move(p, false);
								}
							}
							else {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
						}
					}
				}
				//딴거도?
				else{
					//시즈박아여ㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetUnit(tank, enemies);
						if (tank->self->isSieged()) {
							if (target != NULL) {
								if (tank->self->isAttacking() == false) {
									tank->self->attack(target, false);
								}
							}
						}
						else {
							float dist = getDistance(tank->getPosition(), tg->center);
							if (dist < 100) {
								tank->self->siege();
							}
							else {
								tank->self->attack(tg->center);
							}
						}
					}
				}
			}
			//없나여
			else {
				//건물 있음?
				if (tg->isTargetingBuilding()) {
					//공격 ㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱ
					for (auto tank : tg->tankgroup) {
						Unit target = getTargetBuilding(tank);
						if (tank->self->isSieged()==false) {
							if (target != NULL) {
								if (tank->self->isAttacking() == false) {
									tank->self->attack(target, false);
								}
							}
						}
						else {
							tank->self->attack(t->attackPosition, false);
						}
					}
				}
				//음슴?
				else {
					//이동
					//서로 떨어져 있냐  그러면 뭉쳐 ㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱㄱ
					bool farfaraway = false;
					for (auto tank : tg->tankgroup) {
						if (isNearChokePoint(tank)) {
							farfaraway = false;
							break;
						}
						for (auto tt : tg->tankgroup) {
							float dist = getDistance(tt->getPosition(), tank->getPosition());
							if (dist > 160) {
								farfaraway = true;
							}
						}
					}
					if (farfaraway) {
						for (auto tt : tg->tankgroup) {
							tt->self->move(tg->center, false);
							if (tt->self->isSieged()) {
								tt->self->unsiege();
							}
						}
					}
					//아니면 공격포지션 ㄱㄱ
					else {
						for (auto tt : tg->tankgroup) {
							tt->self->move(t->attackPosition, false);
							if (tt->self->isSieged()) {
								tt->self->unsiege();
							}
						}
					}
				}
			}
		}
	}*/
	
	for (auto tank : t->tankSet) {
		Unit target = getTargetUnit(tank, enemies);
		// 시즈모드냐
		if (tank->self->isSieged()) {
			//공격할 애가 있냐?
			if (target != NULL) {
				//질럿밖에없냐?
				if (onlyZealot) {
					// 시즈풀어ㄱㄱ
					//printf("시즈모드 -> 질럿밖에 없어서 시즈 풀져?\n");
					if (tank->self->canUnsiege()) {
						tank->self->unsiege();
					}
				}
				//딴애도 있냐?
				else {
					// 공격해ㄱㄱ
					//printf("시즈모드 -> 유닛 공격하져\n");
					if (tank->self->isAttacking() == false) {
						tank->self->attack(target);
					}
				}
			}
			//공격할 애가 없냐?
			else {
				Unit targetB = getTargetBuilding(tank);
				//공격할 건물은 있냐?
				if (targetB != NULL) {
					// 건물공격해ㄱㄱ
					//printf("시즈모드 -> 건물공격 들어가져?\n");
					if (tank->self->isAttacking() == false) {
						tank->self->attack(targetB);
					}
				}
				//공격할 건물도 없냐?
				else {
					// 시즈 풀어ㄱㄱ
					//printf("시즈모드 -> 암것도 없어서 시즈 품미다\n");
					if (tank->self->canUnsiege()) {
						tank->self->unsiege();
					}
				}
			}
		}
		// 탱크모드냐
		else {
			//공격할 애가 있냐?
			if (target != NULL) {
				if (getDistance(target->getPosition(), tank->getPosition()) > UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
					if (gameFrame % 24 == 0)
						tank->self->attack(target, false);
				}
				//질럿밖에 없냐?
				else if (onlyZealot) {
					//공격해ㄱㄱ
					//printf("탱크모드 -> 질럿밖에 없네여\n");
					if (tank->self->getGroundWeaponCooldown() == 0) {
						if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
							tank->self->move(p, false);
						}
						else
							tank->self->attack(target, false);
					}
					else {
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
						tank->self->move(p, false);
					}
				}
				//딴애들도 있냐?
				else {
					//printf("탱크모드 -> 딴애들도 있네여\n");
					float dist = getDistance(target->getPosition(), tank->getPosition());
					//거리가 가깝냐?
					if (dist < 100) {
						//printf("탱크모드 -> 가까우니까 걍 공격\n");
						//공격해ㄱㄱ
						if (gameFrame % 24 == 0)
							tank->self->attack(target, false);
					}
					//거리가 머냐?
					else {
						// 시즈박아ㄱㄱ
						//printf("탱크모드 -> 머니까 시즈박을게여\n");
						if (isNearChokePoint(tank)) {
							if (tank->self->getSpellCooldown() == 0) {
								tank->self->attack(target, false);
							}
							else {
								if(gameFrame%8 == 0)
									tank->self->move(t->attackPosition, false);
							}
						}
						else {
							if (tank->self->canSiege())
								tank->self->siege();
						}
					}
				}
			}
			//공격할 애가 없냐?
			else {
				//건물은 있냐?
				Unit targetB = getTargetBuilding(tank);
				if (targetB != NULL) {
					//시즈박아ㄱㄱ
					//printf("탱크모드 -> 건물공격할거니까 시즈할래여\n");
					
					if (isNearChokePoint(tank)) {
						if (tank->self->getSpellCooldown() == 0) {
							tank->self->attack(target, false);
						}
						else {
							if (gameFrame % 8 == 0)
								tank->self->move(t->attackPosition, false);
						}
					}
					else {
						if (tank->self->canSiege())
							tank->self->siege();
					}
				}
				//건물도 없냐?
				else {
					//합류는 했냐?
					if (tank->isInTeam) {
						//printf("탱크모드 -> 암것도 없어서 이동함미다\n");
						if(gameFrame % 24 == 0)
							tank->self->attack(t->attackPosition, false);
					}
					else {
						if (gameFrame % 24 == 0)
							tank->self->attack(t->tankCenterPos, false);
						float dist = getDistance(tank->getPosition(), t->tankCenterPos);
						if (dist < 250) {
							tank->isInTeam = true;
						}
					}
				}
			}
		}
	}
	//이거로함

	/*for (auto tank : t->tankSet) {
		// 팀에 합류했는가?
		Unit target = getMassedUnit(tank, enemies);
		if (!tank->isInTeam) {
			//공격할 대상이 있는가?
			if (target != NULL) {
				//질럿뿐인가?
				if (onlyZealot) {
					//카이팅 ㄱㄱ
					//printf("팀에 합류했고, 공격할 대상이 있고 질럿뿐이여서 카이팅을 할게요\n");
					if (tank->self->isSieged()) {
						if (tank->self->canUnsiege() && tank->self->siege()) {
							tank->self->unsiege();
						}
					}
					else {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
							else
								tank->self->attack(target, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
							tank->self->move(p, false);
						}
					}
				}
				//다른 유닛도 있는가?
				else {
					//시즈 ㄱㄱ
					//printf("팀에 합류했고, 공격할 대상이 있고 다른것도 있어서 시즈를 박을게요\n");
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
			}
			//공격할 대상이 없는가?
			else {
				Unit targetB = getTargetBuilding(tank, getNearEnemyBuildings(tank));
				//공격할 건물이라도 있는가?
				if (targetB != NULL) {
					// 시즈 ㄱㄱ
					//printf("팀에 합류했고, 공격할 대상이 없지만 건물이 있어서 시즈를 박을게요\n");
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(targetB, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
				//공격할 건물도 없는가?
				else {
					//printf("팀에 합류했고, 공격할 대상이 없고 건물도 없어서 이동 할게요\n");
					// 공격포지션 이동  ㄱㄱ
					if (tank->self->isSieged()) {
						if (tank->self->canUnsiege() && tank->self->isSieged()) {
							tank->self->unsiege();
						}
					}
					else {
						if (gameFrame % 24 == 0) {
							tank->self->attack(t->attackPosition, false);
						}
					}

				}
			}
		}
		// 팀에 합류하지 못했는가?
		else {
			//팀 센터랑 거리가 가까운가?
			float dist = getDistance(tank->getPosition(), t->tankCenterPos);
			if (dist < 350) {
				tank->isInTeam = true;
			}
			//공격할 대상이 있는가?
			if (target != NULL) {
				//질럿뿐인가?
				if (onlyZealot) {
					// 카이팅 ㄱㄱ
					//printf("팀에 합류하지 못 했고, 공격할 대상이 있고 질럿 뿐이여서 카이팅을할게여\n");
					if (tank->self->isSieged()) {
						if (tank->self->canUnsiege() && tank->self->siege()) {
							tank->self->unsiege();
						}
					}
					else {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
							else
								tank->self->attack(target, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
							tank->self->move(p, false);
						}
					}
				}
				//다른 유닛도 있는가?
				else {
					//printf("팀에 합류하지 못 했고, 공격할 대상이 있어 시즈를 할게여\n");
					// 시즈 ㄱㄱ
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
			}
			//공격할 대상이 없는가?
			else {
				Unit targetB = getTargetBuilding(tank, getNearEnemyBuildings(tank));
				//공격할 건물이라도 있는가?
				if (targetB != NULL) {
					//시즈 ㄱㄱ
					//printf("팀에 합류하지 못 했고, 공격할 대상이 없지만 건물이 있어서 시즈를 할게여\n");
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(targetB, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
				//공격할 건물도 없는가?
				else {
					// 팀의 무리로 이동 ㄱㄱ
					//printf("팀에 합류하지 못 했고, 공격할 대상이 없어서 팀의 무리로 이동 할게여\n");
					if (gameFrame % 24 == 0) {
						tank->self->attack(t->tankCenterPos, false);
					}
				}
			}
		}
	}*/
}

void CombatControl::onATTACK(TeamControl* t) {

}

void CombatControl::onTIGHTING(TeamControl* t) {

}

void CombatControl::onDEFENSE(TeamControl* t) {

}

void CombatControl::onSQUEEZE(TeamControl* t) {

	int mineCnt = 0;
	for (auto v : t->vultureSet) {
		if (v->maginNumber > 0 && !v->mining) {
			mineCnt++;
		}
	}

	int posIndex = 0;
	int defeneseIndex = 0;
	if (t->tankSet.size() > 10) {
		defeneseIndex = 1;
	}

	vector<Position> minPos = getMinePos(max(mineCnt, 5));
	Unitset enemies = getNearEnemyUnits(t);

	for (auto v : t->vultureSet) {
		Unit target = getTargetUnit(v, enemies);
		if (target != NULL) {
			if (getDistance(target->getPosition(), v->getPosition()) < 300) {
				if (t->tankSet.size() > 0) {
					if (v->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							v->self->move(t->attackPosition, false);
						}
						else {
							if (isOnlyZealot(t)) {
								if (getDistance(target->getPosition(), v->getPosition()) < 80) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
									v->self->attack(target, false);
								}
								else
									v->self->attack(target, false);
							}
							else {
								v->self->attack(target, false);
							}
						}
					}
					else {
						v->mining = false;
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
						v->self->move(p, false);
					}
				}
				else {
					if (v->self->getGroundWeaponCooldown() == 0) {
						if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
							v->self->move(t->attackPosition, false);
						}
						else {
							if (isOnlyZealot(t)) {
								if (getDistance(target->getPosition(), v->getPosition()) < 80) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).attackPosition);
									v->self->move(p, false);
								}
								else
									v->self->attack(target, false);
							}
							else {
								v->self->attack(target, false);
							}
						}
					}
					else {
						v->mining = false;
						Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
						v->self->move(p, false);
					}
				}
			}
			else {
				Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
				v->self->move(p, false);
			}
		}
		else {
			if (v->maginNumber > 0 && BWAPI::Broodwar->self()->hasResearched(TechTypes::Spider_Mines)) {
				if (v->mining) {
					float dist = getDistance(v->getPosition(), v->targetPosition);
					if (dist < 30) {
						//printf("도착했어여 마인을 박을게여\n");
						if (BWAPI::Broodwar->getFrameCount() % 24 == 0)
							v->self->useTech(TechTypes::Spider_Mines, v->targetPosition);
						if (v->maginNumber != v->self->getSpiderMineCount()) {
							//printf("마인을 박았느니라 다시 마인 모드 해제\n");
							v->maginNumber = v->self->getSpiderMineCount();
							v->mining = false;
						}
					}
					else {
						if (BWAPI::Broodwar->getFrameCount() % 24 == 0) {
							v->self->move(v->targetPosition, false);

							//printf("마인을 박으러 가는 중...\n");
						}
					}
				}
				else {
					if (minPos.size() > 0) {
						//printf("마인을 박으러 가려고 설정\n");
						float minDist = 9999.f;
						int eraseIndex = 0;
						Position ret = Position(-1, -1);
						for (auto p = minPos.begin(); p != minPos.end(); p++) {
							float dist = getDistance((*p), v->getPosition());
							if (minDist > dist) {
								minDist = dist;
								ret = (*p);
							}
						}
						setUseMineState(ret, MINE_READY);
						v->targetPosition = ret;
						auto iter = minPos.begin();
						for (iter; iter != minPos.end(); ++iter) {
							if (ret == (*iter)) {
								minPos.erase(iter);
								break;
							}
						}

						v->mining = true;
					}
					else {
						if (target != NULL) {
							if (v->self->getGroundWeaponCooldown() == 0) {
								if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
									if (gameFrame % 24 == 0)
										v->self->attack(target, false);
								}
								else {
									if (getDistance(target->getPosition(), v->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).attackPosition);
										v->self->move(p, false);
									}
									else
										v->self->attack(target, false);
								}
							}
							else {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).attackPosition);
								v->self->move(p, false);
							}
						}
						else {
							Unit targetB = getTargetBuilding(v);
							if (targetB != NULL) {
								if (gameFrame % 24 == 0) {
									v->self->attack(targetB, false);
								}
							}
							else {
								if (gameFrame % 24 == 0) {
									v->self->attack(t->attackPosition, false);
								}
							}
						}

					}
				}
			}
			else {
				if (target != NULL) {
					if (v->self->getGroundWeaponCooldown() == 0) {
						if (isOnlyZealot(t)) {
							if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
								v->self->attack(target, false);
							}
							else {
								if (t->tankSet.size() > 1) {
									if (getDistance(target->getPosition(), v->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->tankCenterPos);
										v->self->move(p, false);
									}
									else
										v->self->attack(target, false);
								}
								else {
									if (getDistance(target->getPosition(), v->getPosition()) < 80) {
										Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), t->attackPosition);
										v->self->move(p, false);
									}
									else
										v->self->attack(target, false);
								}
							}

						}
						else {
							if (target->getType().isBuilding() && target->getType() != UnitTypes::Protoss_Photon_Cannon) {
								v->self->attack(target, false);
							}
							else {
								v->self->attack(target, false);
							}
						}
					}
					else {
						if (t->tankSet.size() > 1) {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).tankCenterPos);
							v->self->move(p, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(v->getPosition(), (*t).attackPosition);
							v->self->move(p, false);
						}
					}
				}
				else {
					Unit targetB = getTargetBuilding(v);
					if (targetB != NULL) {
						if (gameFrame % 24 == 0) {
							v->self->attack(targetB, false);
						}
					}
					else {
						if (gameFrame % 24 == 0) {
							v->self->attack(t->attackPosition, false);
						}
					}
				}

			}
		}
	}

	int safeCount = 0;
	bool bangMissionCheck = false;
	for (auto tank : t->tankSet) {
		Unit target = getMassedUnit(tank, enemies);
		if (posIndex < 15) {
			float dist = getDistance(tank->getPosition(), (Position)defenseTankPos[posIndex]);
			if (dist < 45) {
				if (tank->self->canSiege() && !tank->self->isSieged()) {
					tank->self->siege();
					safeCount++;
				}
				else {
					if (target != NULL) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
					safeCount++;
				}
			}
			else {
				if (target != NULL) {
					float dist = getDistance(target->getPosition(), tank->getPosition());
					if (dist > UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
						target = NULL;
					}
				}
				if (target == NULL) {
					if (tank->self->isSieged()) {
						tank->self->unsiege();
					}
					else {
						if (gameFrame % 12 == 0) {
							tank->self->move((Position)defenseTankPos[posIndex], false);
						}
					}
				}
				else {
					if (isOnlyZealot(t)) {
						if (tank->self->getGroundWeaponCooldown() == 0) {
							if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
							else
								tank->self->attack(target, false);
						}
						else {
							Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
							tank->self->move(p, false);
						}
					}
					else {
						if (tank->self->canSiege() && !tank->self->isSieged()) {
							tank->self->siege();
						}
					}
				}
			}
			posIndex++;
		}
		else {
			float dist = getDistance((Position)DefensePos[defeneseIndex], (Position)tank->getTilePosition());
			if (dist < 100) {
				if (target != NULL) {
					if (tank->self->isSieged()) {
						if (tank->self->isAttacking() == false) {
							tank->self->attack(target, false);
						}
					}
					else {
						if (isOnlyZealot(t)) {
							if (tank->self->getGroundWeaponCooldown() == 0) {
								if (getDistance(target->getPosition(), tank->getPosition()) < 80) {
									Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
									tank->self->move(p, false);
								}
								else
									tank->self->attack(target, false);
							}
							else {
								Position p = MapInformation::getInstance()->getNextAvoidMovePos(tank->getPosition(), target->getPosition());
								tank->self->move(p, false);
							}
						}
						else {
							if (tank->self->canSiege() && !tank->self->isSieged()) {
								tank->self->siege();
							}
						}
					}
				}
				else {
					if (tank->self->canSiege() && !tank->self->isSieged()) {
						tank->self->siege();
					}
				}
			}
			else {
				if (gameFrame % 24 == 0) {
					tank->self->attack((Position)DefensePos[defeneseIndex], false);
				}
			}
		}
	}
}

vector<Position> CombatControl::getVGPosition(Position tPos, Position ePos, int vgNum, int tR, float tankRange, int dist) {
	vector <Position> ret;
	ret.clear();

	float PI = 3.141592f;
	float theta = 0.0f;
	float r = 1;
	int px = 0;
	int py = 0;

	int R ;
	if (tR == 3) {
		R = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f;
	}
	else if (tR == 2) {
		R = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 2 - 75;
	}
	else if (tR == 1) {
		R = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 3 - 75;
	}
	else {
		R = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 4 - 75;
	}
																				  //R 길이 수정해줍시다.

	if (tR < 2) {
		R = max(tankRange + 80.0f, UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 2);
		R = min(R, (int)(UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 3));
	}
	else {
		R = max(tankRange + 80.0f, UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 1);
		R = min(R, (int)(UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f * 2));
	}


	theta = atan((float)(tPos.y - ePos.y) / (tPos.x - ePos.x));
	float l = (float)dist / ((float)R / 3);
	float sl = -l / 4 * (vgNum - 1);
	float el = l / 4 * (vgNum - 1);
	if (dist != UnitTypes::Terran_Vulture.width()) {
		sl = -l / 4 * (vgNum - 1) - l/3;
		el = l / 4 * (vgNum - 1) - l/3;
	}

	if (tPos.x < ePos.x && tPos.y > ePos.y || tPos.x < ePos.x && tPos.y < ePos.y) {  // 14사분면

		for (float z = sl; z <= el; z += l / 2) {
			px = (int)(R * cos(theta + (z*r))) + tPos.x;
			py = (int)(R * sin(theta + (z*r))) + tPos.y;
			ret.push_back(Position(px, py));
		}
	}
	else if (tPos.x > ePos.x && tPos.y < ePos.y || tPos.x > ePos.x && tPos.y > ePos.y) {  // 23사분면

		for (float z = sl; z <= el; z += l / 2) {
			px = (int)(R * cos(theta + PI + (z*r))) + tPos.x;
			py = (int)(R * sin(theta + PI + (z*r))) + tPos.y;
			ret.push_back(Position(px, py));
		}
	}

	return ret;
}
Unitset CombatControl::getNearEnemyBuildings(MyUnit* t) {
	Unitset ret;
	ret.clear();
	
	for (auto q = info->enemy->getAllBuildings().begin(); q != info->enemy->getAllBuildings().end(); q++) {
		int range = (int)(((*q)->getType().groundWeapon().maxRange()*1.0f));
		float dist = getDistance(t->getPosition(), info->enemy->getPosition((*q)));
		if (dist < range) {
			ret.insert((*q));
		}
	}
	return ret;
}

Unitset CombatControl::getNearEnemyUnits(TeamControl* t) {
	Unitset ret;
	ret.clear();
	vector<MyUnit*> teamUnits;
	teamUnits.clear();
	for (auto q = t->vultureSet.begin(); q != t->vultureSet.end(); q++) {
		teamUnits.push_back(*q);
	}
	for (auto q = t->tankSet.begin(); q != t->tankSet.end(); q++) {
		teamUnits.push_back(*q);
	}
	for (auto q = t->goliathSet.begin(); q != t->goliathSet.end(); q++) {
		teamUnits.push_back(*q);
	}
	for (auto q = t->vessleList.begin(); q != t->vessleList.end(); q++) {
		teamUnits.push_back(*q);
	}
	for (auto q = t->marineSet.begin(); q != t->marineSet.end(); q++) {
		teamUnits.push_back(*q);
	}
	
	for (auto q = info->enemy->getAllUnits().begin(); q != info->enemy->getAllUnits().end(); q++) {
		for (auto m = teamUnits.begin(); m != teamUnits.end(); m++) {
			int range = (int)(((*q)->getType().groundWeapon().maxRange()*1.2f + (*m)->getType().groundWeapon().maxRange()) * 1.2f);
			float dist = getDistance((*m)->getPosition(), (*q)->getPosition());
			if (dist < range) {
				ret.insert(*q);
				break;
			}
		}
	}
	
	return ret;
}

Unit CombatControl::getRepairUnit(MyUnit* scv) {
	Unit ret = NULL;

	Unitset repairSet;
	repairSet.clear();

	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode)) {
		repairSet.insert(tank->self);
	}
	for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
		repairSet.insert(tank->self);
	}
	for (auto bunker : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
		repairSet.insert(bunker->self);
	}

	float maxValue = 0.0f;

	for (auto rs : repairSet) {
		float value = 0;
		if (rs->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || rs->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
			value = 1;
		}
		else {
			value = 1.5f;
		}
		float dist = (500.f - getDistance(scv->getPosition(), rs->getPosition()));
		dist /= 500.f;
		float hp = (float)(rs->getType().maxHitPoints() - rs->getHitPoints()) / (float)(rs->getType().maxHitPoints());

		value += dist + hp;

		if (hp == 0) {
			continue;
		}

		if (maxValue < value) {
			ret = rs;
			maxValue = value;
		}
	}

	return ret;
}

Unit CombatControl::getHealUnit(MyUnit* unit, TeamControl* t) {

	Unit ret = NULL;

	Unitset healSet;
	healSet.clear();

	for (auto marine : t->marineSet) {
		healSet.insert(marine->self);
	}
	for (auto medic : t->medicSet) {
		healSet.insert(medic->self);
	}
	for (auto scv : t->scvSet) {
		healSet.insert(scv->self);
	}

	float maxValue = 0.0f;

	for (auto hs : healSet) {
		float value = getTargetUnitValue(unit->getType(), hs->getType());
		float dist = (500.f - getDistance(unit->getPosition(), hs->getPosition()));
		dist /= 500.f;
		float hp = (float)(hs->getType().maxHitPoints() - hs->getHitPoints()) / (float)(hs->getType().maxHitPoints());

		value += dist + hp;

		if (hp == 0) {
			continue;
		}

		if (maxValue < value) {
			ret = hs;
			maxValue = value;
		}
	}

	return ret;
}

Unit CombatControl::getTargetUnit(MyUnit* unit, Unitset enemyUnits) {
	Unit ret = NULL;

	if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		ret = getMassedUnit(unit, enemyUnits);
		return ret;
	}
	
	float maxValue = 0;
	for (auto q = enemyUnits.begin(); q != enemyUnits.end(); q++) {
		if (unit->getType() == UnitTypes::Unknown) continue;
		if (unit->getType().airWeapon().maxRange() < 1 && (*q)->isFlying()) continue;
		float value = getTargetUnitValue(unit->getType(), (*q)->getType());
		float dist = (unit->getType().groundWeapon().maxRange() - getDistance(unit->getPosition(), (*q)->getPosition()));
		if (UnitTypes::Terran_Siege_Tank_Tank_Mode == unit->getType()) {
			dist = (UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() - getDistance(unit->getPosition(), (*q)->getPosition()));
		}
		if (dist < 0) dist = 0;
		else {
			dist = dist / (float)unit->getType().groundWeapon().maxRange();
		}
		float hp = (((float)(*q)->getType().maxHitPoints() + (float)(*q)->getType().maxShields()) - ((float)(*q)->getHitPoints() + (float)(*q)->getShields())) / ((float)(*q)->getType().maxHitPoints() + (float)(*q)->getType().maxShields());
		value = value * 5.0f +dist * 10.0f + hp * 4.0f;

		if (maxValue < value) {
			maxValue = value;
			ret = (*q);
		}
	}

	if (ret != NULL) {
		float dst = getDistance(unit->getPosition(), ret->getPosition());

		if (dst < unit->getType().groundWeapon().minRange() && dst > unit->getType().groundWeapon().maxRange()) {
			ret = NULL;
		}
	}
	return ret;
}

Unit CombatControl::getTargetBuilding(MyUnit*unit) {
	Unit ret = NULL;
	float minDist = 9999;
	for (auto e = info->enemy->getAllBuildings().begin(); e != info->enemy->getAllBuildings().end(); e++) {
		float dist = getDistance((*e)->getPosition(), unit->getPosition());
		if (minDist > dist && (dist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() && dist > UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().minRange())) {
			minDist = dist;
			ret = (*e);
		}
	}
	return ret;
}
float CombatControl::getTargetUnitValue(UnitType my, UnitType enemy) {
	if (my == UnitTypes::Terran_Vulture) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 1.2f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 0.9f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 0.5f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 0.0f;
		}
	}
	else if (my == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.8f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 2.5f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 0.5f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 0.0f;
		}
	}
	else if (my == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.8f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.8f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 1.2f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 0.0f;
		}
	}
	else if (my == UnitTypes::Terran_Goliath) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 0.5f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Protoss_Interceptor) {
			return 0.5f;
		}
	}
	else if (my == UnitTypes::Terran_Science_Vessel) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.05f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 0.2f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.2f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 1.5f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.5f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 0.2f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 0.2f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 4.5f;
		}
		else if (enemy == UnitTypes::Protoss_Interceptor) {
			return 0.01f;
		}
	}
	else if (my == UnitTypes::Terran_Marine) {
		if (enemy == UnitTypes::Protoss_Probe) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Photon_Cannon) {
			return 0.3f;
		}
		else if (enemy == UnitTypes::Protoss_Zealot) {
			return 1.2f;
		}
		else if (enemy == UnitTypes::Protoss_Dragoon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_High_Templar) {
			return 0.9f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Templar) {
			return 0.6f;
		}
		else if (enemy == UnitTypes::Protoss_Archon) {
			return 0.4f;
		}
		else if (enemy == UnitTypes::Protoss_Dark_Archon) {
			return 0.1f;
		}
		else if (enemy == UnitTypes::Protoss_Reaver) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Protoss_Shuttle) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Protoss_Observer) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Scout) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Corsair) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Carrier) {
			return 0.0f;
		}
		else if (enemy == UnitTypes::Protoss_Arbiter) {
			return 0.0f;
		}
	}
	else if (my == UnitTypes::Terran_Medic) {
		if (enemy == UnitTypes::Terran_Marine) {
			return 1.0f;
		}
		else if (enemy == UnitTypes::Terran_SCV) {
			return 0.7f;
		}
		else if (enemy == UnitTypes::Terran_Medic) {
			return 0.7f;
		}
		return 0.0f;
	}
}

bool CombatControl::isOnlyZealot(TeamControl* t) {
	Unitset enemies = getNearEnemyUnits(t);
	for (auto e = enemies.begin(); e != enemies.end(); e++) {
		if ((*e)->getType() == UnitTypes::Protoss_Probe) continue;
		if ((*e)->getType() != UnitTypes::Protoss_Zealot && !(*e)->getType().isFlyer() && (*e)->getType() != UnitTypes::Protoss_High_Templar) {
			return false;
		}
	}
	return true;
}

Unit CombatControl::getMassedUnit(MyUnit* unit, Unitset enemies) {
	// 밀집된 유닛 반환
	Unit ret = NULL;

	//시즈일떄
	if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		//시즈모드 일 때만
		float maxValue = -9999;

		for (auto e = enemies.begin(); e != enemies.end(); e++) {
			if ((*e)->isFlying()) continue;
			if ((*e)->getType() == UnitTypes::Unknown) continue;
			if ((*e)->getType() == UnitTypes::Protoss_High_Templar && (*e)->getEnergy() < 60) continue;
			if ((*e)->getType() == UnitTypes::Protoss_Probe) continue;
			//공격할 수 있는 애들 중에
			int minR = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().minRange();
			int maxR = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange();
			if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
				maxR = UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() + 100;
			}
			float distance = getDistance((*e)->getPosition(), unit->getPosition());
			if ( minR <= distance && maxR >= distance) {
				// 직접 타격하는 친구는 *8
				float value = getTargetUnitValue(unit->getType(), (*e)->getType()) * 8.0f;

				for (auto s = enemies.begin(); s != enemies.end(); s++) {
					float dist = getDistance((*e)->getPosition(), (*s)->getPosition());
					// 스플래시 해당 거리 따라 데미지에 맞춰 값 계산
					if (dist < 10) {
						value += getTargetUnitValue(UnitTypes::Terran_Siege_Tank_Siege_Mode, (*e)->getType()) * 4.0f;
					}
					else if (dist < 25) {
						value += getTargetUnitValue(UnitTypes::Terran_Siege_Tank_Siege_Mode, (*e)->getType()) * 2.0f;
					}
					else if (dist < 40) {
						value += getTargetUnitValue(UnitTypes::Terran_Siege_Tank_Siege_Mode, (*e)->getType()) * 1.0f;
					}
				}
				for (auto s : info->self->getAllUnits()) {
					float dist = getDistance((*e)->getPosition(), s->getPosition());
					// 스플래시 해당 거리 따라 데미지에 맞춰 값 계산
					if (dist < 10) {
						value -= 0.5f * 4.0f;
					}
					else if (dist < 25) {
						value -= 0.5f * 2.0f;
					}
					else if (dist < 40) {
						value -= 0.5f * 1.0f;
					}
				}
				if (maxValue < value) {
					ret = (*e);
					maxValue = value;
				}
			}
		}

		return ret;
	}

	return ret;
}

vector<std::pair<Position, float>> CombatControl::getEMPPositions(TeamControl* t, int vCount, Unitset enemies) {
	vector <pair<Position, float>> empValues;
	vector <pair <Position, float>> totalValues;
	//maxVal >= 4 && v->self->getEnergy() >= 100 && getDistance(v->getPosition(), ret) < 256 * 2.5f && v->maginNumber > 48
	for (auto e : info->enemy->getAllUnits()) {
		if (e->getType() == UnitTypes::Protoss_Arbiter && e->getEnergy() < 80) {
			continue;
		}
		else if (e->getType() == UnitTypes::Protoss_High_Templar && e->getEnergy() < 80) {
			continue;
		}
		else if (e->getShields() < 10) {
			continue;
		}
		float vx = e->getVelocityX() * 20.0f;
		float vy = e->getVelocityY() * 20.0f;
		Position p = Position(e->getPosition().x + vx, e->getPosition().y + vy);
		float val = getTargetUnitValue(UnitTypes::Terran_Science_Vessel, e->getType());
		BWAPI::Broodwar->drawLineMap(p, e->getPosition(), Colors::Red);
		empValues.push_back(pair<Position, float>(p, val));
	}


	// 예상 위치에 박아놓고 포지션당 값 계산
	float maxVal = 0;
	for (auto q = empValues.begin(); q != empValues.end(); q++) {
		float val = (*q).second;
		for (auto qq = empValues.begin(); qq != empValues.end(); qq++) {
			float dist = getDistance((*qq).first, (*q).first);
			// 근처에 있는 애들도 1
			if (dist < 32 * 3) {
				val += (*qq).second;
			}
		}
		if (maxVal < val) {
			maxVal = val;
			totalValues.push_back(pair<Position, float>((*q).first, val));
		}
	}

	vector<std::pair<Position, float>> ret;
	for (int x = 0; x < vCount; x++) {
		for (auto q = ret.begin(); q != ret.end(); q++) {
			auto iter = totalValues.begin();
			for (iter; iter != totalValues.end(); ++iter) {
				float dist = getDistance(iter->first, (*q).first);
				if (dist < 32 * 4) {
					totalValues.erase(iter);
					iter--;
				}
			}
		}
		
		float maxValue = 0;
		Position maxPos;
		for (auto q = totalValues.begin(); q != totalValues.end(); q++) {
			if (maxValue < (*q).second) {
				maxValue = (*q).second;
				maxPos = (*q).first;
			}
		}

		if (maxValue >= 4) {
			ret.push_back(std::pair<Position, float>(maxPos, maxValue));
		}
		else {
			break;
		}
	}

	return ret;

}

void CombatControl::setMinePos() {
	vector<Position> hongPos;
	hongPos.push_back(Position(271, 852));			hongPos.push_back(Position(472, 1113));			hongPos.push_back(Position(751, 1220));			hongPos.push_back(Position(1867, 313));
	hongPos.push_back(Position(1403, 306));			hongPos.push_back(Position(1229, 538));			hongPos.push_back(Position(1148, 894));			hongPos.push_back(Position(1252, 1489));
	hongPos.push_back(Position(943, 1201));			hongPos.push_back(Position(1249, 1674));		hongPos.push_back(Position(1232, 1912));		hongPos.push_back(Position(1297, 2250));
	hongPos.push_back(Position(677, 2054));			hongPos.push_back(Position(419, 2264));			hongPos.push_back(Position(928, 2183));			hongPos.push_back(Position(1462, 2577));
	hongPos.push_back(Position(1167, 2555));		hongPos.push_back(Position(309, 2561));			hongPos.push_back(Position(564, 2832));			hongPos.push_back(Position(748, 3049));
	hongPos.push_back(Position(1566, 2743));		hongPos.push_back(Position(1719, 3030));		hongPos.push_back(Position(1432, 3030));		hongPos.push_back(Position(2055, 3420));
	hongPos.push_back(Position(1145, 3314));		hongPos.push_back(Position(1167, 3442));		hongPos.push_back(Position(785, 3879));			hongPos.push_back(Position(1071, 3655));
	hongPos.push_back(Position(2237, 3688));		hongPos.push_back(Position(2644, 3770));		hongPos.push_back(Position(2009, 2799));		hongPos.push_back(Position(1927, 2451));
	hongPos.push_back(Position(2178, 2452));		hongPos.push_back(Position(2050, 2273));		hongPos.push_back(Position(2539, 2695));		hongPos.push_back(Position(2851, 3496));
	hongPos.push_back(Position(2987, 3149));		hongPos.push_back(Position(2825, 2593));		hongPos.push_back(Position(3189, 2937));		hongPos.push_back(Position(3445, 2920));
	hongPos.push_back(Position(3587, 2984));		hongPos.push_back(Position(3768, 3248));		hongPos.push_back(Position(2950, 2427));		hongPos.push_back(Position(2890, 2118));
	hongPos.push_back(Position(3401, 2027));		hongPos.push_back(Position(3666, 1810));		hongPos.push_back(Position(2879, 1720));		hongPos.push_back(Position(3729, 1533));
	hongPos.push_back(Position(3506, 1268));		hongPos.push_back(Position(3390, 1086));		hongPos.push_back(Position(2750, 1423));		hongPos.push_back(Position(2387, 1222));
	hongPos.push_back(Position(2141, 1656));		hongPos.push_back(Position(1905, 1663));		hongPos.push_back(Position(2053, 1875));		hongPos.push_back(Position(1495, 1536));
	hongPos.push_back(Position(1783, 1254));		hongPos.push_back(Position(2046, 590));			hongPos.push_back(Position(2939, 687));			hongPos.push_back(Position(2971, 824));
	hongPos.push_back(Position(2690, 1060));		hongPos.push_back(Position(3038, 463));			hongPos.push_back(Position(3299, 268));			hongPos.push_back(Position(1889, 911));
	hongPos.push_back(Position(2228, 926));

	for (auto p : hongPos) {
		Position p1[4];
		p1[0] = Position(p.x+30, p.y-30);
		p1[1] = Position(p.x+30, p.y+30);
		p1[2] = Position(p.x-30, p.y+30);
		p1[3] = Position(p.x-30, p.y-30);
		for (int x = 0; x < 4; x++) {
			if (BWAPI::Broodwar->isWalkable((WalkPosition)p1[x])) {
				std::pair<Position, std::pair<float, int>> t;
				t.first = p1[x];
				t.second.first = 0.0f;
				t.second.second = 0;
				minePos.push_back(t);
			}
		}
	}
	hongPos.clear();
	for (auto b : BWTA::getBaseLocations()) {
		std::pair<Position, std::pair<float, int>> t;
		t.first = b->getPosition();
		t.second.first = 0.0f;
		t.second.second = 0;
		minePos.push_back(t);
	}
}

void CombatControl::updateMinePosValue() {
	for (auto q = minePos.begin(); q != minePos.end(); q++) {
		(*q).second.first = 0.0f;
		if ((*q).second.second != MINE_READY) {
			bool isInMine = false;
			for (auto m : info->self->getUnits(UnitTypes::Terran_Vulture_Spider_Mine)) {
				if (m->getPosition() == (*q).first) {
					isInMine = true;
				}
			}
			if (isInMine) {
				(*q).second.second = MINE_DONE;
			}
			else {
				(*q).second.second = MINE_NONE;
			}
		}
		else {
			// 건물이 지어져 있거나 자원이 있는 곳은 애초에 세지 않을 거임 ㅅㄱ
			int gimV = MapInformation::getInstance()->getGIM((*q).first);
			if (gimV < 0) {
				(*q).second.first = -1.0f;
			}
		}
	}

	// 적군 그룹과 가까우면 마인을 박지 않을 거에여
	for (auto q = minePos.begin(); q != minePos.end(); q++) {
		//이미 마인을 박으러 갔거나, 안 박을 곳은 제외
		if ((*q).second.second >= 1 || (*q).second.first < 0) continue;
		for (auto e : info->enemy->getAllUnits()) {
			float dist = getDistance(info->enemy->getPosition(e), (*q).first);
			if (dist < 640) {
				(*q).second.first = -1.0f;
			}
		}
		for (auto e : info->self->getAllUnits()) {
			if (e->getType() == UnitTypes::Terran_Vulture_Spider_Mine) continue;
			float dist = getDistance(e->getPosition() , (*q).first);
			if (dist < 80) {
				(*q).second.first = -1.0f;
			}
		}
		for (auto e : info->self->getAllBuildings()) {
			float dist = getDistance(e->getPosition(), (*q).first);
			if (dist < 240) {
				(*q).second.first = -1.0f;
			}
		}
		for (auto e : info->enemy->getAllBuildings()) {
			float dist = getDistance(info->enemy->getPosition(e), (*q).first);
			if (dist < 640) {
				(*q).second.first = -1.0f;
			}
		}
		//시즈 주변에도 안 박을 거에요
		if ((*q).second.second >= 1 || (*q).second.first < 0) continue;
		for (auto t : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
			float dist = getDistance((*t).self->getPosition(), (*q).first);
			if (dist < 100) {
				(*q).second.first = -1.0f;
			}
		}
	}

	// 우리 그룹과 가까우면
	for (auto q = minePos.begin(); q != minePos.end(); q++) {
		//이미 마인을 박으러 갔거나, 안 박을 곳은 제외
		if ((*q).second.second >= 1 || (*q).second.first < 0) continue;
		for (auto g = info->self->myGroup.begin(); g != info->self->myGroup.end(); g++) {
			float dist = getDistance((*g).first, (*q).first);
			if (dist < 1800) {
				(*q).second.first += (1800 - dist)/1800.0*5;
			}
		}
		////적 그룹과 가까우면
		/*for (auto g = info->enemy->enemyGroup.begin(); g != info->enemy->enemyGroup.end(); g++) {
			float dist = getDistance((*g).first, (*q).first);
			if (dist < 1800 && dist > 300) {
				(*q).second.first += (1800 - dist)*30;
			}
		}*/
		// 적 베이스와 가까우면
		for (auto t = info->map->BLI.begin(); t != info->map->BLI.end(); t++) {
			if ((*t).second->player != 0) continue;
			float dist = getDistance((*t).first->getPosition(), (*q).first);
			if (dist < 3600) {
				(*q).second.first += (3600 - dist) / 3600.0 * 10;
			}
		}
		// 우리 베이스와 가까우면
		for (auto t : info->map->getMyTerritorys()) {
			float dist = getDistance((*t).getBase()->getPosition(), (*q).first);
			if (dist < 3600) {
				(*q).second.first += (3600 - dist) / 3600.0 * 8;
			}
		}

		for (auto b = BWTA::getBaseLocations().begin(); b != BWTA::getBaseLocations().end(); b++) {
			if ((*b)->getPosition() == (*q).first) {
				(*q).second.first += 50000;
			}
		}
	}

	
}

vector<Position> CombatControl::getMinePos(int count) {
	vector<Position> ret;
	while (ret.size() < count) {
		float maxValue = 0;
		Position maxPos = Position(-1, -1);
		for (auto q = minePos.begin(); q != minePos.end(); q++) {
			if ((*q).second.second != 0) continue;
			bool check = false;
			for (auto r : ret) {
				if (r == (*q).first) check = true;
			}
			if (check) continue;

			float value = (*q).second.first;
			if (maxValue < value) {
				maxValue = value;
				maxPos = (*q).first;
			}
		}

		if (maxValue > 0 && maxPos.x > 0) {
			ret.push_back(maxPos);
		}
		else {
			break;
		}
	}

	return ret;
}

void CombatControl::setUseMineState(Position p, int state) {
	for (auto q = minePos.begin(); q != minePos.end(); q++) {
		if ((*q).first == p) {
			(*q).second.second = state;
		}
	}
}

bool CombatControl::isNearChokePoint(MyUnit* unit) {
	float minDist = 9999;
	for (auto c : BWTA::getChokepoints()) {
		float dist = getDistance(c->getCenter(), unit->getPosition());
		if (minDist > dist) {
			minDist = dist;
		}
	}

	if (minDist < 128) {
		return true;
	}
	else {
		return false;
	}
}
void CombatControl::kitingMove(MyUnit* unit, Unit target, Position targetPos) {
	if (target != NULL) {
		if (unit->self->isAttacking() == false) {
			if (target->getType().isBuilding()) {
				if (gameFrame % 24 == 0)
					unit->self->attack(target, false);
			}
			else {
				if (getDistance(target->getPosition(), unit->getPosition()) < 80) {
					Position p = MapInformation::getInstance()->getNextAvoidMovePos(unit->getPosition(), targetPos);
					unit->self->attack(p, false);
				}
				else {
					if (unit->self->isAttacking() == false)
						unit->self->attack(target, false);
				}
			}
		}
		else {
			Position p = MapInformation::getInstance()->getNextAvoidMovePos(unit->getPosition(), targetPos);
			unit->self->move(p, false);
		}
	}
	else {
		Position p = MapInformation::getInstance()->getNextAvoidMovePos(unit->getPosition(), targetPos);
		unit->self->move(p, false);
	}
}

void CombatControl::settingDefenseObject() {
	if (info->map->getMyDirection() == 0) {

		defenseTankPos.push_back(TilePosition(20, 39));
		defenseTankPos.push_back(TilePosition(21, 30));

		bangMission = (TilePosition(4, 31));

		DefensePos[0] = TilePosition(15, 35);
		DefensePos[1] = TilePosition(36, 44);
	}
	else if (info->map->getMyDirection() == 1) {

		defenseTankPos.push_back(TilePosition(92, 18));
		defenseTankPos.push_back(TilePosition(97, 24));

		bangMission = (TilePosition(93, 4));

		DefensePos[0] = TilePosition(95, 15);
		DefensePos[1] = TilePosition(86, 30);
	}
	else if (info->map->getMyDirection() == 2) {

		defenseTankPos.push_back(TilePosition(36, 110));
		defenseTankPos.push_back(TilePosition(26, 106));

		bangMission = (TilePosition(33, 123));

		DefensePos[0] = TilePosition(32, 111);
		DefensePos[1] = TilePosition(41, 98);
	}
	else if (info->map->getMyDirection() == 3) {

		defenseTankPos.push_back(TilePosition(107, 90));
		defenseTankPos.push_back(TilePosition(105, 98));

		bangMission = (TilePosition(123, 96));

		DefensePos[0] = TilePosition(110, 94);
		DefensePos[1] = TilePosition(92, 85);
	}

	if (info->map->getEnemyStartBase() != NULL) {

		if (info->map->getEnemyDirection() == 0) {
			tightPos = TilePosition(38, 46);
			tightBunkerPos.clear();
			tightTankPos.clear();

			tightBunkerPos.push_back(TilePosition(26, 40));
			tightBunkerPos.push_back(TilePosition(26, 38));
			tightBunkerPos.push_back(TilePosition(26, 36));
			tightBunkerPos.push_back(TilePosition(23, 40));
			tightBunkerPos.push_back(TilePosition(23, 38));
			tightBunkerPos.push_back(TilePosition(23, 36));
			tightBunkerPos.push_back(TilePosition(23, 34));

			tightTankPos.push_back(TilePosition(27, 35));
			tightTankPos.push_back(TilePosition(30, 37));
			tightTankPos.push_back(TilePosition(30, 39));
			tightTankPos.push_back(TilePosition(30, 41));
			tightTankPos.push_back(TilePosition(28, 42));
			tightTankPos.push_back(TilePosition(30, 43));
			tightTankPos.push_back(TilePosition(32, 43));
			tightTankPos.push_back(TilePosition(34, 43));
			tightTankPos.push_back(TilePosition(33, 45));

		}
		else if (info->map->getEnemyDirection() == 1) {
			tightPos = TilePosition(84, 32);

			tightBunkerPos.clear();
			tightTankPos.clear();

			tightBunkerPos.push_back(TilePosition(94, 25));
			tightBunkerPos.push_back(TilePosition(94, 23));
			tightBunkerPos.push_back(TilePosition(88, 23));
			tightBunkerPos.push_back(TilePosition(91, 23));
			tightBunkerPos.push_back(TilePosition(88, 21));
			tightBunkerPos.push_back(TilePosition(91, 21));

			tightTankPos.push_back(TilePosition(87, 25));
			tightTankPos.push_back(TilePosition(89, 26));
			tightTankPos.push_back(TilePosition(91, 26));
			tightTankPos.push_back(TilePosition(93, 26));
			tightTankPos.push_back(TilePosition(87, 27));
			tightTankPos.push_back(TilePosition(89, 28));
			tightTankPos.push_back(TilePosition(91, 28));
			tightTankPos.push_back(TilePosition(93, 28));
			tightTankPos.push_back(TilePosition(95, 28));
			tightTankPos.push_back(TilePosition(97, 28));
			tightTankPos.push_back(TilePosition(99, 28));
			tightTankPos.push_back(TilePosition(100, 26));
			tightTankPos.push_back(TilePosition(98, 26));
			tightTankPos.push_back(TilePosition(98, 24));
			tightTankPos.push_back(TilePosition(85, 28));
			tightTankPos.push_back(TilePosition(87, 29));
			tightTankPos.push_back(TilePosition(89, 30));
			tightTankPos.push_back(TilePosition(91, 30));
			tightTankPos.push_back(TilePosition(83, 29));
			tightTankPos.push_back(TilePosition(85, 30));

		}
		else if (info->map->getEnemyDirection() == 2) {
			tightPos = TilePosition(43, 96);

			tightBunkerPos.clear();
			tightTankPos.clear();

			tightBunkerPos.push_back(TilePosition(39, 102));
			tightBunkerPos.push_back(TilePosition(36, 102));
			tightBunkerPos.push_back(TilePosition(33, 102));
			tightBunkerPos.push_back(TilePosition(30, 102));
			tightBunkerPos.push_back(TilePosition(34, 104));
			tightBunkerPos.push_back(TilePosition(37, 104));

			tightTankPos.push_back(TilePosition(42, 100));
			tightTankPos.push_back(TilePosition(44, 100));
			tightTankPos.push_back(TilePosition(44, 98));
			tightTankPos.push_back(TilePosition(42, 98));
			tightTankPos.push_back(TilePosition(40, 97));
			tightTankPos.push_back(TilePosition(40, 99));
			tightTankPos.push_back(TilePosition(40, 101));
			tightTankPos.push_back(TilePosition(38, 99));
			tightTankPos.push_back(TilePosition(38, 101));
			tightTankPos.push_back(TilePosition(36, 99));
			tightTankPos.push_back(TilePosition(36, 101));
			tightTankPos.push_back(TilePosition(34, 99));
			tightTankPos.push_back(TilePosition(34, 101));
			tightTankPos.push_back(TilePosition(32, 99));
			tightTankPos.push_back(TilePosition(32, 101));
			tightTankPos.push_back(TilePosition(30, 99));
			tightTankPos.push_back(TilePosition(30, 101));
		}
		else if(info->map->getEnemyDirection() == 3) {
			tightPos = TilePosition(91, 83);

			tightBunkerPos.clear();
			tightTankPos.clear();

			tightBunkerPos.push_back(TilePosition(99, 87));
			tightBunkerPos.push_back(TilePosition(99, 89));
			tightBunkerPos.push_back(TilePosition(102, 87));
			tightBunkerPos.push_back(TilePosition(102, 89));
			tightBunkerPos.push_back(TilePosition(102, 91));
			tightBunkerPos.push_back(TilePosition(102, 93));

			tightTankPos.push_back(TilePosition(95, 83));
			tightTankPos.push_back(TilePosition(93, 83));
			tightTankPos.push_back(TilePosition(91, 83));
			tightTankPos.push_back(TilePosition(90, 85));
			tightTankPos.push_back(TilePosition(92, 85));
			tightTankPos.push_back(TilePosition(94, 85));
			tightTankPos.push_back(TilePosition(96, 85));
			tightTankPos.push_back(TilePosition(98, 85));
			tightTankPos.push_back(TilePosition(100, 86));
			tightTankPos.push_back(TilePosition(102, 86));
			tightTankPos.push_back(TilePosition(98, 87));
			tightTankPos.push_back(TilePosition(96, 87));
			tightTankPos.push_back(TilePosition(94, 87));
			tightTankPos.push_back(TilePosition(92, 87));
			tightTankPos.push_back(TilePosition(94, 89));
			tightTankPos.push_back(TilePosition(96, 89));
			tightTankPos.push_back(TilePosition(98, 89));
			tightTankPos.push_back(TilePosition(98, 91));
			tightTankPos.push_back(TilePosition(96, 91));
			tightTankPos.push_back(TilePosition(94, 91));
			tightTankPos.push_back(TilePosition(94, 93));
			tightTankPos.push_back(TilePosition(96, 93));
			tightTankPos.push_back(TilePosition(98, 93));
		}
		if (defenseTankPos.size() > 0) {
			defenseSetting = true;
			for (auto pos : tightBunkerPos) {
				tightValue[pos] = 0;
				tightReady[pos] = 0;
			}
			for (auto pos : tightTankPos) {
				tightValue[pos] = 0;
				tightReady[pos] = 0;
			}
		}
	}
}

bool CombatControl::isEnemyUnitComeTrue(MyUnit* unit) {

	float minDist = 9999;
	float minUDist = 9999;
	bool isUniderAttack = false;
	float closeUnits = 0;
	for (auto enemy : info->enemy->getAllUnits()) {
		Position expectUnitPos = enemy->getPosition();

		expectUnitPos.x += enemy->getVelocityX() * 18;
		expectUnitPos.y += enemy->getVelocityY() * 18;

		float dist = getDistance(expectUnitPos, unit->getPosition());
		float udist = getDistance(enemy->getPosition(), unit->self->getPosition());

		if (dist < minDist) {
			minDist = dist;
		}
		if (udist < minUDist) {
			minUDist = udist;
		}
		if (unit->self->isUnderAttack()) {
			isUniderAttack = true;
		}
		if (udist < 16) {
			closeUnits++;
		}
	}

	if (closeUnits > 1) {
		return false;
	}
	if (minDist < 20 || minUDist < 20 || unit->self->isUnderAttack()) {
		return true;
	}

	return false;
}

void CombatControl::joiningTeam(MyUnit* m, TeamControl* t) {
	float dist = getDistance(m->getPosition(), t->teamCenterPos);
	if (dist < 200) {
		m->isInTeam = true;
	}
}

void CombatControl::setHelpMe(MyUnit* m) {
	if (m->getType() != UnitTypes::Terran_Marine) return;

	int fullp = UnitTypes::Terran_Marine.maxHitPoints();
	int unitp = m->self->getHitPoints();
	
	float percent = (float)unitp / fullp;

	if (percent < 0.4) m->helpMe = true;
	else if (m->helpMe && percent > 0.8) m->helpMe = false;
}


void CombatControl::update() {

	if (info->map->getEnemyStartBase() == NULL) return;

	// tankPos 
	for (auto tankPos : tightTankPos) {
		bool atPos = false;
		int nearTankCount = 0;
		int nearBuilding = 0;
		int nearUnit = 0;
		int cannonScore = 0;
		int value = 0;

		for (auto tank : info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode)) {
			float dist = getDistance((Position)tankPos, tank->getPosition());
			// 탱크 있는지 점수
			if (dist < 30) {
				atPos = true;
			}
			// 근처에 다른 탱크 점수
			if (dist < 80) {
				nearTankCount++;
			}
		}

		for (auto eBuilding : info->enemy->getAllBuildings()) {
			float dist = getDistance(eBuilding->getPosition(), (Position)tankPos);
			if (dist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
				nearBuilding++;
			}
		}

		for (auto eUnit : info->enemy->getAllUnits()) {
			float dist = getDistance(eUnit->getPosition(), (Position)tankPos);
			if (dist < UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange()) {
				nearUnit++;
			}
		}

		for (auto cannon : info->enemy->getUnits(UnitTypes::Protoss_Photon_Cannon)) {
			float dist = getDistance(cannon->getPosition(), (Position)tankPos);

			if (dist < UnitTypes::Protoss_Photon_Cannon) {
				cannonScore -= 30;
			}
			else if (dist < UnitTypes::Terran_Siege_Tank_Siege_Mode) {
				cannonScore += 5;
			}
			else if(dist < UnitTypes::Terran_Siege_Tank_Siege_Mode && dist > UnitTypes::Protoss_Photon_Cannon){
				cannonScore += 40;
			}
		}

		if (atPos) {
			value = -1;
			tightReady[tankPos] = -1;
		}
		else value = max(100 - nearBuilding*30 - nearUnit*4 - nearTankCount*4 + cannonScore, 0);

		tightValue[tankPos] = value;
		tightReady[tankPos] = min(tightReady[tankPos], 1);
	}
	

	// BunkerPos
	for (auto bunkerPos : tightBunkerPos) {
		bool atPos = false;
		
		Position bunker = (Position)bunkerPos;
		bunker = Position(bunker.x + UnitTypes::Terran_Bunker.width(), bunker.y + UnitTypes::Terran_Bunker.height());

		for (auto b : info->self->getBuildings(UnitTypes::Terran_Bunker)) {
			float bdist = getDistance(b->getPosition(), bunker);
			if (bdist < 30) atPos = true;
		}
		for (auto b : info->self->getImmediatelyBuilding(UnitTypes::Terran_Bunker)) {
			float bdist = getDistance(b->getPosition(), bunker);
			if (bdist < 30) atPos = true;
		}

		float dist = getDistance(bunker, (Position)tightPos);
		
		if (atPos) {
			tightValue[bunkerPos] = -1;
			tightReady[bunkerPos] = -1;
		}
		else {
			tightValue[bunkerPos] = (int)dist;
			tightReady[bunkerPos] = min(tightReady[bunkerPos], 1);
		}
	}

}

TilePosition CombatControl::getTightTankPosition() {
	TilePosition ret = TilePosition(-1, -1);

	int maxTankPos = -1;
	for (auto tank : tightTankPos) {
		if (maxTankPos < tightValue[tank] && tightValue[tank] >= 0 && tightReady[tank] == 0) {
			maxTankPos = tightValue[tank];
			ret = tank;
		}
	}

	//tightReady[ret] = 1;
	return ret;
}

TilePosition CombatControl::getTightBunkerPosition() {
	TilePosition ret = TilePosition(-1, -1);

	int minBunkerPos = -1;
	for (auto bunker : tightBunkerPos) {
		if (tightValue[bunker] > 0 && tightReady[bunker] == 0) {
			minBunkerPos = tightValue[bunker];
			ret = bunker;
			break;
		}
	}

	tightReady[ret] = 1;
	return ret;
}