#include "VMap.h"
#include "Instance.h"
#include "Utility.h"

VMap::VMap(){

}

VMap::~VMap() {

}

VMap* VMap::getInstance() {

	static VMap instance;
	return &instance;
}

void VMap::init() {

	for (int i = 0; i < SIZE; i++){
		for (int j = 0; j < SIZE; j++){

			movePos[i][j] = MoveNode(j, i);
		}
	}

	for (int i = 0; i < SIZE; i++){
		for (int j = 0; j < SIZE; j++){
			if (!BWAPI::Broodwar->isWalkable((WalkPosition)movePos[i][j].getPosition())) {

				for (int y = -2; y <= 2 ; y++){
					for (int x = -2; x <= 2; x++){						

						if ((y == 0 && x == 0)){
							continue;
						}

						int xpos = j + x;
						int ypos = i + y;

						if (xpos < 0 || ypos < 0 || SIZE <= xpos || SIZE <= ypos){
							continue;
						}

						movePos[ypos][xpos].addPoint(-90);
					}
				}

				movePos[i][j].setPoint(-1);
			}
		}
	}
}

void VMap::show() {
	for (int i = 0; i < SIZE; i = i+4){
		for (int j = 0; j < SIZE; j = j+4){
			auto& pos = movePos[i][j];

			bw->drawTextMap(pos.getPosition(), "%d", pos.getPoint());
		}
	}
}

void VMap::update(){

	
}

void VMap::draw(){

}