#include "Information.h"

#include "Utility.h"

int nx[8] = { 1, 1, 1, 0, 0, -1, -1, -1 };
int ny[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };

Information::Information() {

}

Information::~Information() {

}

Information* Information::getInstance() {

	static Information instance;
	return &instance;
}

void Information::init() {

	self  = new Self();
	enemy = new Enemy();
	map = new Map();
	resource = new ResourceInfo();
}

void Information::create(Unit unit) {
	if(unit->getPlayer() == BWAPI::Broodwar->enemy())
		enemy->create(unit);
}

void Information::update() {

	self->update();
	enemy->update();
	map->update();
	resource->update();
}

void Information::run(){
	map->run();
}

void Information::show(){

	self->show(200, 40);
	enemy->show(300, 40);
	map->show(400, 40);
}

void Information::draw() {

	self->draw();
	enemy->draw();
	map->draw();
	resource->draw();
}

void Information::morph(Unit unit) {
	auto player = unit->getPlayer();

	if (player == Broodwar->self()) {
		self->morph(unit);
	}
	else {
		enemy->morph(unit);
	}
	
}

void Information::destroy(Unit unit){

	auto player = unit->getPlayer();

	if (player == Broodwar->self()){
		self->destroy(unit);
	}
	else{
		enemy->destroy(unit);
		map->destroy(unit);
	}
}

vector <pair<Position, pair<int, int>>> Information::getGroupArray(bool e, bool ground) {
	vector<pair<Position, pair<int, int>>> ret;
	vector<Position> posv;
	vector<int> unitCount;
	vector<int> unitPosX;
	vector<int> unitPosY;
	vector<int> unitValue;
	vector<int> maxUnitDist;
	bool check[IMSIZE][IMSIZE] = { false };

	vector<Unit> unitInfo;
	vector<MyUnit*> myUnitInfo;
	posv.clear();
	ret.clear();
	maxUnitDist.clear();
	unitCount.clear();
	unitInfo.clear();
	unitValue.clear();
	int count = 0;
	int range = 0;

	memset(posVal, 0, sizeof(posVal));

	if (e && !ground) {
		unitInfo = enemy->getAllUnits();
	}
	else if (e && ground) {
		unitInfo = enemy->getAllUnits();
	}
	else {
		for (auto q = self->getAllUnits().begin(); q != self->getAllUnits().end(); q++) {
			if ((*q)->getType().isBuilding()) continue;
			if ((*q)->getType() == UnitTypes::Terran_SCV) continue;
			unitInfo.push_back((*q)->self);
		}
	}
	
	//printf("enemyUntisInfo Size = %d\n", enemyUnitsInfo.size());
	for (auto q = unitInfo.begin(); q != unitInfo.end(); q++) {
		if (e) {
			if (enemy->getType((*q)).isFlyer()) continue;
		}
		if ((*q)->getType().isFlyer()) continue;
		if (e) {
			if (enemy->getType((*q)) == UnitTypes::Unknown || enemy->getType((*q)) == UnitTypes::Protoss_Photon_Cannon) continue;
		}
		else {
			if ((*q)->getType() == UnitTypes::Unknown) continue;
		}
		//printf("enemyUnitInfo for loop\n");
		if (e) {
			if (enemy->getType((*q)) == UnitTypes::Protoss_Probe) continue;
			if (enemy->getType((*q)) != UnitTypes::Protoss_Photon_Cannon && enemy->getType((*q)).isBuilding()) continue;
		}
		int _x;
		int _y;
		if (ground && e) {
			_x = enemy->getPosition((*q)).x;
			_y = enemy->getPosition((*q)).y;
		}
		else if (!ground & e) {
			_x = (*q)->getPosition().x;
			_y = (*q)->getPosition().y;
		}
		else {
			_x = (*q)->getPosition().x;
			_y = (*q)->getPosition().y;
		}
		_x /= TERM;
		_y /= TERM;
		if (_x < 0 || _y < 0 || _x>IMSIZE-1 || _y > IMSIZE-1) continue;
		if (e) {
			posVal[_y][_x] += getUnitValue(enemy->getType((*q)));
			check[_y][_x] = true;
		}
		else {
			posVal[_y][_x] += getUnitValue(((*q)->getType()));
			check[_y][_x] = true;
		}
		
	}
	int R = 36;
	if (!e) {
		R = 72;
	}
	for (int x = 0; x < IMSIZE; x++) {
		for (int y = 0; y < IMSIZE; y++) {
			for (int k = 0; k < 8; k++) {
				int _x = x + nx[k];
				int _y = y + ny[k];
				if (_x < 0 || _x > IMSIZE-1 || _y < 0 || _y > IMSIZE-1) continue;
				if (check[_y][_x]) {
					vector<pair<int, int>> queue;
					vector<pair<int, int>> dq;
					queue.push_back(pair<int, int>(_x, _y));
					dq.push_back(pair<int, int>(_x, _y));
					count = 0;
					int pos_x = 0;
					int pos_y = 0;
					float maxDist = 0;
					//printf("while loop\n");
					while (queue.size() > 0) {
						int xx = queue.front().first;
						int yy = queue.front().second;
						queue.erase(queue.begin());
						for (int i = -R; i <= R; i++) {
							for (int j = -R; j <= R; j++) {
								int _xx = xx + i;
								int _yy = yy + j;
								if (_xx < 0 || _xx > IMSIZE-1 || _yy < 0 || _yy > IMSIZE-1) continue;
								if (check[_yy][_xx]) {
									queue.push_back(pair<int, int>(_xx, _yy));
									dq.push_back(pair<int, int>(_xx, _yy));
									count++;
									pos_x += _xx*TERM;
									pos_y += _yy*TERM;
									check[_yy][_xx] = false;
								}
							}
						}
					}
					if (count != 0) {
						posv.push_back(Position(pos_x /= count, pos_y /= count));
						unitCount.push_back(count);
						unitPosX.push_back(pos_x);
						unitPosY.push_back(pos_y);
						int uv = 0;
						for (auto q = dq.begin(); q != dq.end(); q++) {
							uv += posVal[(*q).second][(*q).first];
							float dist = getDistance(Position((*q).first*TERM, (*q).second*TERM), Position(pos_x, pos_y));
							if (dist > maxDist) {
								maxDist = dist;
							}
						}
						maxUnitDist.push_back(maxDist);
						unitValue.push_back(uv);
					}
				}
			}
		}
	}

	if (unitCount.size() <= 0) {
		//printf("posv size = 0");
		return ret;
	}
	else {
		for (int x = 0; x < unitCount.size(); x++) {
			ret.push_back(pair<Position, pair<int, int>>(Position(unitPosX[x], unitPosY[x]), pair<int, int>(maxUnitDist[x], unitValue[x])));
		}
	}

	return ret;
}


int Information::getUnitValue(UnitType ut) {
	if (ut == UnitTypes::Protoss_Zealot) {
		return 3;
	}
	else if (ut == UnitTypes::Protoss_Dragoon) {
		return 4;
	}
	else if (ut == UnitTypes::Protoss_High_Templar) {
		return 8;
	}
	else if (ut == UnitTypes::Protoss_Dark_Templar) {
		return 4;
	}
	else if (ut == UnitTypes::Terran_Vulture) {
		return 3;
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode || ut == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		return 5;
	}
	else if (ut == UnitTypes::Terran_Goliath) {
		return 4;
	}
}