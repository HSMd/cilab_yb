#pragma once

#include <BWAPI.h>

#include <map>
#include <vector>

#include "Territory.h"

using namespace BWAPI;
using namespace std;

enum COMBAT_FLAG{
	GUARD_SIM_CITY_1, 
	GUARD_SIM_CITY_2,
	GUARD_SIM_CITY_3, 
	INVASION_PYLON,
	FORWARD_MARINE,
	ATTACK,

	SIEGE_MODE,
	STIM_PACKS,
	U_238_SHELLS,
};

enum ENEMY_BUILD{
	NONE,
	FORWARD_GATE,     // 전진게이트
	RAW_DOUBLE,       // 생더블
	ONE_GATE_DOUBLE,  // 원게이트 더블
	ONE_GATE_CORE,    // 원게이트 코어를 감.
	TWO_GATE,         // 투게이트.
	TWO_GATE_CORE,    // 투게이트 코어.
	PHOTO_RUSH,       // 포토 러쉬
	ZEALOT_ALLIN,      // 질럿 올인

	SHUTTLE_RIVER,    // 셔틀 리버
	SHUTTLE_DARK,     // 셔틀 다크
};

class FlagData{

public:
	bool fire;
	int frameCount;

	FlagData(){
		fire = false;
		frameCount = 0;
	}
};

class StateMgr
{
	map<COMBAT_FLAG, FlagData> combatFlag;
	map<COMBAT_FLAG, int> combatLog;		

	COMBAT_FLAG combat;
	ENEMY_BUILD enemyBuild;

	StateMgr();
	~StateMgr();

public:
	static StateMgr* getInstance();
		
	void init();
	void show();
	void draw();
	void run();

	COMBAT_FLAG getCombat() { return combat; }

	void setCombatFlag(COMBAT_FLAG flag, bool fire) { combatFlag[flag].fire = fire; };
	void setOppFlag(ENEMY_BUILD build) { enemyBuild = build; };

	bool getCombatFlag(COMBAT_FLAG flag) { return combatFlag[flag].fire; };
	ENEMY_BUILD getEnemyBuild(){ return enemyBuild; };
	void updateEnemyBuildByScout();
};

