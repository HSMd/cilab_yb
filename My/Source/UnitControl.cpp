#include "UnitControl.h"
#include "Information.h"
#include "Instance.h"
#include "Self.h"
#include "Utility.h"
#include "CombatControl.h"

void TeamControl::update() {
	//printf("Tank size  %d\n", tankSet.size());

	this->teamCenterPos = UnitControl::getInstance()->getTeamCenterPos(this);

	this->tankCenterPos = UnitControl::getInstance()->getTankCenterPos(this);

	this->bionicCenterPos = UnitControl::getInstance()->getBionicCenterPos(this);

	this->attackPosition = UnitControl::getInstance()->getAttackPosition(this);

	this->state = UnitControl::getInstance()->getTeamState(this);
	
	CombatControl::getInstance()->onFrame(this);

}

void TeamControl::resetTG() {
}

TeamControl::STATE UnitControl::getTeamState(TeamControl* t) {
	TeamControl::STATE ret = t->state;
	if (t->state == TeamControl::STATE::IDLE) {
		return TeamControl::STATE::BCN_DOUBLE;
	}
	if (t->state == TeamControl::STATE::BCN_DOUBLE) {
		if (info->self->getBuildingCount(UnitTypes::Terran_Bunker, false) > 0) {
			return TeamControl::STATE::BCN_BUNKER;
		}
		else {
			return ret;
		}
	}
	if (t->state == TeamControl::STATE::BCN_BUNKER) {
		if (info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode, false) > 0 || info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode, false) > 1) {
			return TeamControl::STATE::BCN_TANK;
		}
		else {
			return ret;
		}
	}
	if (t->state == TeamControl::STATE::BCN_TANK) {
		if (isReadyGoing()) {
			return TeamControl::STATE::BCN_GOING;
		}
		else {
			return ret;
		}
	}
	if (t->state == TeamControl::STATE::BCN_GOING) {
		if (isReadyTight(t->team)) {
			return TeamControl::STATE::BCN_TIGHT;
		}
		else {
			return ret;
		}
	}
	if (t->state == TeamControl::STATE::BCN_TIGHT) {
		if (isReadyBJ()) {
			return TeamControl::STATE::BCN_BJ;
		}
		else if (isReadyFinish()) {
			return TeamControl::STATE::BCN_FINISH;
		}
		else {
			return ret;
		}
	}
	if (t->state == TeamControl::STATE::BCN_FINISH) {
		if (isUnReadyFinish()) {
			return TeamControl::STATE::BCN_TIGHT;
		}
		else if (isReadyBJ()) {
			return TeamControl::STATE::BCN_BJ;
		}
		else {
			return ret;
		}
	}

	return ret;
}

UnitControl* UnitControl::s_UnitControl;
UnitControl::UnitControl() {

}

UnitControl::~UnitControl() {

}

UnitControl* UnitControl::create() {
	static UnitControl instance;
	return &instance;
}

UnitControl* UnitControl::getInstance() {
	static UnitControl instance;
	return &instance;
}

void UnitControl::init() {
	CombatControl::create();
	TCV.clear();
	TCV.push_back(new TeamControl(0));
	totalTeamIndex = 1;
}

void UnitControl::onFrame() {
	
	CombatControl::getInstance()->update();
	
	for (int x = 0; x < totalTeamIndex; x++) {
		TCV[x]->update();
		//printf("team -> %d : marine %d medic %d tank %d\n", TCV[x]->team, TCV[x]->marineSet.size(), TCV[x]->medicSet.size(), TCV[x]->tankSet.size());
	}
	//setTeamDicision();
}

void UnitControl::draw() {
	
	for (auto q = info->self->getUnits(UnitTypes::Terran_Marine).begin(); q != info->self->getUnits(UnitTypes::Terran_Marine).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Teal, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Medic).begin(); q != info->self->getUnits(UnitTypes::Terran_Medic).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Yellow, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Vulture).begin(); q != info->self->getUnits(UnitTypes::Terran_Vulture).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Orange, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode).begin(); q != info->self->getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Blue, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).begin(); q != info->self->getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Cyan, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Goliath).begin(); q != info->self->getUnits(UnitTypes::Terran_Goliath).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Purple, true);
	}
	for (auto q = info->self->getUnits(UnitTypes::Terran_Science_Vessel).begin(); q != info->self->getUnits(UnitTypes::Terran_Science_Vessel).end(); q++) {
		BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::White, true);
	}

	Position sp = Position(480, 270);
	Position np = Position(sp.x - 50, 270);

	for (int x = 0; x < totalTeamIndex; x++) {
		BWAPI::Color color = Colors::Red;
		if (x == 1) color = Colors::Yellow;
		if (x == 2) color = Colors::Green;
		if (x == 3) color = Colors::Blue;
		if (x == 4) color = Colors::Purple;
		if (x == 5) color = Colors::White;
		if (x == 6) color = Colors::Brown;
		if (x == 7) color = Colors::Black;
		if (x == 8) color = Colors::Orange;
		if (x == 9) color = Colors::Cyan;
		if (x == 10) color = Colors::Grey;
		if (x == 11) color = Colors::Teal;

		//printf("marineSet size = %d\n", TCV[x]->marineSet.size());
		for (auto q = TCV[x]->scvSet.begin(); q != TCV[x]->scvSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 5, Colors::Cyan, true);
		}
		for (auto q = TCV[x]->marineSet.begin(); q != TCV[x]->marineSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->medicSet.begin(); q != TCV[x]->medicSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->vultureSet.begin(); q != TCV[x]->vultureSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->tankSet.begin(); q != TCV[x]->tankSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->goliathSet.begin(); q != TCV[x]->goliathSet.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}
		for (auto q = TCV[x]->vessleList.begin(); q != TCV[x]->vessleList.end(); q++) {
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 7, color, false);
			BWAPI::Broodwar->drawCircleMap((*q)->getPosition(), 8, color, false);
		}

		BWAPI::Broodwar->drawCircleMap(TCV[x]->teamCenterPos, 10, color, true);

		CombatControl::getInstance()->draw(TCV[x]);

		BWAPI::Broodwar->drawBoxScreen(Position(np.x - 5, np.y - 4), Position(sp.x + 80, sp.y + 16), color, true);
		BWAPI::Broodwar->drawBoxScreen(Position(np.x - 3, np.y - 2), Position(sp.x + 78, sp.y + 14), Colors::Black, true);
		BWAPI::Broodwar->drawTextScreen(np, "Team ");
		BWAPI::Broodwar->drawTextScreen(sp, getControlString(TCV[x]));
		np.y += 20;
		sp.y += 20;
	}

}

char* UnitControl::getControlString(TeamControl* t) {
	if (t->state == TeamControl::STATE::BCN_DOUBLE) {
		return "Bacanic_Double";
	}
	else if (t->state == TeamControl::STATE::BCN_BUNKER) {
		return "Bacanic_Bunker";
	}
	else if (t->state == TeamControl::STATE::BCN_FINISH) {
		return "Bacanic_Finish";
	}
	else if (t->state == TeamControl::STATE::BCN_GOING) {
		return "Bacanic_Going";
	}
	else if (t->state == TeamControl::STATE::BCN_JOIN) {
		return "Bacanic_Join";
	}
	else if (t->state == TeamControl::STATE::BCN_TANK) {
		return "Bacanic_Tank";
	}
	else if (t->state == TeamControl::STATE::BCN_TIGHT) {
		return "Bacanic_Tight";
	}
	else if (t->state == TeamControl::STATE::BCN_BJ) {
		return "Bacanic_BJ";
	}
	else {
		return "NONE";
	}
}

void UnitControl::changeTeam(MyUnit* unit, int team) {

	// 바꿀 팀의 리스트가 있는지 확인해여
	if (team >= totalTeamIndex) {
		return;
	}

	//팀을 바꿔줘요
	if (unit->getType() == UnitTypes::Terran_Marine) {
		onUnitDestroy(unit->self);
		unit->team = team;
		TCV[team]->marineSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Medic) {
		auto iter = TCV[unit->team]->medicSet.begin();
		auto iter2 = TCV[unit->team]->medicSet.begin();
		for (iter; iter != TCV[unit->team]->medicSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->medicSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->medicSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Vulture) {
		auto iter = TCV[unit->team]->vultureSet.begin();
		auto iter2 = TCV[unit->team]->vultureSet.begin();
		for (iter; iter != TCV[unit->team]->vultureSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->vultureSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->vultureSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		auto iter = TCV[unit->team]->tankSet.begin();
		auto iter2 = TCV[unit->team]->tankSet.begin();
		for (iter; iter != TCV[unit->team]->tankSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->tankSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->tankSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Goliath) {
		auto iter = TCV[unit->team]->goliathSet.begin();
		auto iter2 = TCV[unit->team]->goliathSet.begin();
		for (iter; iter != TCV[unit->team]->goliathSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->goliathSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->goliathSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Science_Vessel) {
		auto iter = TCV[unit->team]->vessleList.begin();
		auto iter2 = TCV[unit->team]->vessleList.begin();
		for (iter; iter != TCV[unit->team]->vessleList.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->vessleList.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->vessleList.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_SCV) {
		auto iter = TCV[unit->team]->scvSet.begin();
		auto iter2 = TCV[unit->team]->scvSet.begin();
		for (iter; iter != TCV[unit->team]->scvSet.end(); iter++) {
			iter2++;
			if ((*iter) == unit) {
				TCV[unit->team]->scvSet.erase(iter);
				break;
			}
		}
		unit->team = team;
		TCV[team]->scvSet.push_back(unit);
	}
}
MyUnit* UnitControl::getUnit(UnitType ut) {
	// 유닛을 빼갈 수 있게 도와줘요
	MyUnit* ret = NULL;
	for (int x = 0; x < totalTeamIndex; x++) {
		if (ut == UnitTypes::Terran_Marine) {
			auto iter = TCV[x]->marineSet.begin();
			auto iter2 = TCV[x]->marineSet.begin();
			for (iter; iter != TCV[x]->marineSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->marineSet.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_Medic) {
			auto iter = TCV[x]->medicSet.begin();
			auto iter2 = TCV[x]->medicSet.begin();
			for (iter; iter != TCV[x]->medicSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->medicSet.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_Vulture) {
			auto iter = TCV[x]->vultureSet.begin();
			auto iter2 = TCV[x]->vultureSet.begin();
			for (iter; iter != TCV[x]->vultureSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->vultureSet.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode || ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
			auto iter = TCV[x]->tankSet.begin();
			auto iter2 = TCV[x]->tankSet.begin();
			for (iter; iter != TCV[x]->tankSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->tankSet.erase(iter);
				return ret;
			}

		}
		else if (ut == UnitTypes::Terran_Goliath) {
			auto iter = TCV[x]->goliathSet.begin();
			auto iter2 = TCV[x]->goliathSet.begin();
			for (iter; iter != TCV[x]->goliathSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->goliathSet.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_Science_Vessel) {
			auto iter = TCV[x]->vessleList.begin();
			auto iter2 = TCV[x]->vessleList.begin();
			for (iter; iter != TCV[x]->vessleList.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->vessleList.erase(iter);
				return ret;
			}
		}
		else if (ut == UnitTypes::Terran_SCV) {
			auto iter = TCV[x]->scvSet.begin();
			auto iter2 = TCV[x]->scvSet.begin();
			for (iter; iter != TCV[x]->scvSet.end(); iter = iter2) {
				iter2++;
				ret = (*iter);
				TCV[x]->scvSet.erase(iter);
				return ret;
			}
		}
	}

	return ret;
}

void UnitControl::getUnit(MyUnit* unit) {
	// 유닛을 빼갈 수 있게 도와줘요

	UnitType ut = unit->getType();

	for (int x = 0; x < totalTeamIndex; x++) {
		if (ut == UnitTypes::Terran_Marine) {
			auto iter = TCV[x]->marineSet.begin();
			auto iter2 = TCV[x]->marineSet.begin();
			for (iter; iter != TCV[x]->marineSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->marineSet.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_Medic) {
			auto iter = TCV[x]->medicSet.begin();
			auto iter2 = TCV[x]->medicSet.begin();
			for (iter; iter != TCV[x]->medicSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->medicSet.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_Vulture) {
			auto iter = TCV[x]->vultureSet.begin();
			auto iter2 = TCV[x]->vultureSet.begin();
			for (iter; iter != TCV[x]->vultureSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->vultureSet.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode || ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
			auto iter = TCV[x]->tankSet.begin();
			auto iter2 = TCV[x]->tankSet.begin();
			for (iter; iter != TCV[x]->tankSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->tankSet.erase(iter);
					return;
				}
			}

		}
		else if (ut == UnitTypes::Terran_Goliath) {
			auto iter = TCV[x]->goliathSet.begin();
			auto iter2 = TCV[x]->goliathSet.begin();
			for (iter; iter != TCV[x]->goliathSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->goliathSet.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_Science_Vessel) {
			auto iter = TCV[x]->vessleList.begin();
			auto iter2 = TCV[x]->vessleList.begin();
			for (iter; iter != TCV[x]->vessleList.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->vessleList.erase(iter);
					return;
				}
			}
		}
		else if (ut == UnitTypes::Terran_SCV) {
			auto iter = TCV[x]->scvSet.begin();
			auto iter2 = TCV[x]->scvSet.begin();
			for (iter; iter != TCV[x]->scvSet.end(); iter = iter2) {
				iter2++;
				if ((*iter) == unit) {
					TCV[x]->scvSet.erase(iter);
					return;
				}
			}
		}
	}
}

void UnitControl::returnUnit(MyUnit* unit) {
	// 팀을 먼저 분배해 줘요
	int teamIndex = 0;

	if (unit->getType() == UnitTypes::Terran_Marine) {
		TCV[teamIndex]->marineSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Medic) {
		TCV[teamIndex]->medicSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Vulture) {
		TCV[teamIndex]->vultureSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode || unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		TCV[teamIndex]->tankSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Goliath) {
		TCV[teamIndex]->goliathSet.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Science_Vessel) {
		TCV[teamIndex]->vessleList.push_back(unit);
	}
	else if (unit->getType() == UnitTypes::Terran_SCV) {
		TCV[teamIndex]->scvSet.push_back(unit);
	}
}

void UnitControl::onUnitCreate(Unit unit) {
	if (unit == nullptr) return;
}

void UnitControl::onUnitComplete(Unit unit) {
	//printf("유닛 추가해줄거에염\n");

	if (unit == nullptr) return;
	// 유닛이 완성되면 팀에 넣어줘요

	//일단 0번팀에 넣어줘요
	UnitType ut = unit->getType();
	if (ut == UnitTypes::Terran_SCV) return;
	int teamIndex = totalTeamIndex-1;

	//조이기에 들어갔으면

	if (ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		for (int x = 0; x < totalTeamIndex; x++) {
			for (auto t = TCV[x]->tankSet.begin(); t != TCV[x]->tankSet.end(); t++) {
				if ((*t)->self->getID() == unit->getID()) {
					return;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		for (int x = 0; x < totalTeamIndex; x++) {
			for (auto t = TCV[x]->tankSet.begin(); t != TCV[x]->tankSet.end(); t++) {
				if ((*t)->self->getID() == unit->getID()) {
					return;
				}
			}
		}
	}

	if (ut == UnitTypes::Terran_Marine) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->marineSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Medic) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->medicSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Vulture) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->vultureSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->tankSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Goliath) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->goliathSet.push_back(m);
	}
	else if (ut == UnitTypes::Terran_Science_Vessel) {
		MyUnit *m = new MyUnit(unit);
		m->team = teamIndex;
		TCV[teamIndex]->vessleList.push_back(m);
	}
}

void UnitControl::onUnitDestroy(Unit unit) {
	// 애가 뒤지면 리스트에서 없애줘요
	UnitType ut = unit->getType();
	if (ut == UnitTypes::Terran_SCV) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->scvSet.begin();
			for (iter; iter != TCV[x]->scvSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->scvSet.erase(iter);
					iter--;
				}
			}
		}
	}
	if (ut == UnitTypes::Terran_Marine) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->marineSet.begin();
			for (iter; iter != TCV[x]->marineSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->marineSet.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Medic) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->medicSet.begin();
			for (iter; iter != TCV[x]->medicSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->medicSet.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Vulture) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->vultureSet.begin();
			for (iter; iter != TCV[x]->vultureSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->vultureSet.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Siege_Tank_Tank_Mode || ut == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->tankSet.begin();
			for (iter; iter != TCV[x]->tankSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->tankSet.erase(iter);
					iter--;
				}
			}
		}

	}
	else if (ut == UnitTypes::Terran_Goliath) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->goliathSet.begin();
			for (iter; iter != TCV[x]->goliathSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->goliathSet.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_Science_Vessel) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->vessleList.begin();
			for (iter; iter != TCV[x]->vessleList.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->vessleList.erase(iter);
					iter--;
				}
			}
		}
	}
	else if (ut == UnitTypes::Terran_SCV) {
		for (int x = 0; x < totalTeamIndex; x++) {
			auto iter = TCV[x]->scvSet.begin();
			for (iter; iter != TCV[x]->scvSet.end(); ++iter) {
				if (isSame((*iter)->self, unit)) {
					iter = TCV[x]->scvSet.erase(iter);
					iter--;
				}
			}
		}
	}
}

int UnitControl::getTeamTankDanger(TeamControl* t) {
	int ret = 0;
	Unitset enemies = CombatControl::getInstance()->getNearEnemyUnits(t);

	float mindist = 99999;
	for (auto q = (*t).tankSet.begin(); q != (*t).tankSet.end(); q++) {
		for (auto e = enemies.begin(); e != enemies.end(); e++) {
			float dist = getDistance((*q)->getPosition(), (*e)->getPosition());
			if (dist < mindist) {
				mindist = dist;
			}
		}
	}
	//printf("3 - %.2f // 2 - %.2f // 1 - %.2f\n", (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 1.0f), (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 2.0f), (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 3.0f));
	//printf("minDist : %.2f\n", mindist);
	if (mindist < (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 1.0f)) {
		ret = 3;
	}
	else if (mindist < (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 2.0f)) {
		ret = 2;
	}
	else if (mindist < (((float)UnitTypes::Terran_Siege_Tank_Siege_Mode.groundWeapon().maxRange() / 4.0f) * 3.0f)) {
		ret = 1;
	}
	return ret;
}

void UnitControl::updateTeam() {


}


Position UnitControl::getTeamCenterPos(TeamControl* t) {

	int tx = 0;
	int ty = 0;

	int count = 0;

	for (auto q : t->marineSet) {
		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;
		count++;
	}
	for (auto q : t->medicSet) {
		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;
		count++;
	}
	for (auto q : t->tankSet) {
		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;

		count++;
	}
	if (count == 0) {
		return info->map->getMyStartBase()->getPosition();
	}
	else {
		return Position(tx / count, ty / count);
	}
}

Position UnitControl::getBionicCenterPos(TeamControl* t) {

	Position ret = Position(0, 0);

	int tx = 0;
	int ty = 0;

	int count = 0;

	for (auto q : t->marineSet) {
		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;

		count++;
	}
	if (count != 0) {
		ret = Position(tx / count, ty / count);
	}

	return ret;
}

Position UnitControl::getTankCenterPos(TeamControl* t) {

	Position ret = Position(0, 0);

	int tx = 0;
	int ty = 0;

	int count = 0;

	for (auto q : t->tankSet) {
		if (q->isInTeam == false) continue;
		tx += q->getPosition().x;
		ty += q->getPosition().y;

		count++;
	}
	if (count != 0) {
		ret = Position(tx / count, ty / count);
	}

	return ret;
}

Position UnitControl::getAttackPosition(TeamControl* t) {
	Position p = info->map->getMyStartBase()->getPosition();

	if (t->state == TeamControl::STATE::SQUEEZE) {
		if (t->tankSet.size() > 10) {
			p = (Position)CombatControl::getInstance()->DefensePos[1];
		}
		else {
			p = (Position)CombatControl::getInstance()->DefensePos[0];
		}
	}
	else {
		if (info->enemy->enemyGroup.size() > 0) {
			Position ap;
			float minDist = 9999.9f;
			for (auto q = info->enemy->enemyGroup.begin(); q != info->enemy->enemyGroup.end(); q++) {
				float dist = getDistance(t->tankCenterPos, (*q).first);
				if (minDist > dist) {
					minDist = dist;
					ap = (*q).first;
				}
			}
			p = ap;
		}
		else {
			if (info->map->getEnemyStartBase() != NULL) {
				p = info->map->getEnemyStartBase()->getPosition();
			}
			else {
				p = info->map->getMyStartBase()->getPosition();;
			}

			BWTA::BaseLocation* base = NULL;
			int maxValue = 0;
			for (auto q = info->map->BLI.begin(); q != info->map->BLI.end(); q++) {
				if ((*q).second->player == 0) {
					int value = (*q).second->value;
					if (value > maxValue) {
						maxValue = value;
						base = (*q).first;
					}
				}
			}

			if (base == NULL) {
				p = (Position)CombatControl::getInstance()->DefensePos[1];
			}
			else {
				p = base->getPosition();
			}
		}
	}

	return p;
}

void UnitControl::setTeamDicision() {

}

// 충분한 병력이 모였는?
bool UnitControl::isReadyGoing() {
	bool ret = false;

	int marineCount = 0;
	int tankCount = 0;
	
	marineCount = info->self->getUnitCount(UnitTypes::Terran_Marine);
	tankCount = info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode) + info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode);
	
	if (marineCount > 9 && tankCount > 1) ret = true;
	return ret;
}

// 적 다리에 도착했는지?
bool UnitControl::isReadyTight(int teamNumber) {
	bool ret = false;

	float dist = getDistance((Position)CombatControl::getInstance()->tightPos, TCV[teamNumber]->teamCenterPos);

	if (dist < 60) {
		ret = true;
	}
	
	if (ret && teamNumber == totalTeamIndex-1 && teamNumber != 0) {
		TCV.push_back(new TeamControl(totalTeamIndex++));
		TCV[totalTeamIndex - 1]->state = TeamControl::STATE::BCN_JOIN;
	}
	return ret;
}

//병준형을 견제하는 방법
bool UnitControl::isReadyBJ() {
	bool ret = false;
	
	int count = 0;
	for (auto base = info->map->BLI.begin(); base != info->map->BLI.end(); base++) {
		if (base->second->player == 0) {
			if (base->first != info->map->getEnemyStartBase()) {
				count++;
			}
		}
	}

	if (TCV.size() > 2) {
		// 이미 견제 팀이 만들어 진 경우
		ret = false;
	}
	if (count > 1) {
		ret = true;
	}

	return ret;
}

// 용사여 적을 끝낼 준비가 되었나!!!!!
bool UnitControl::isReadyFinish() {
	bool ret = false;

	if (info->map->getEnemyStartBase() == NULL) {
		ret = false;
	}
	else {
		for (auto eb : info->enemy->getAllBuildings()) {
			if (BWTA::getNearestBaseLocation(eb->getPosition()) != info->map->getMyStartBase()) {
				ret = false;
				break;
			}
			ret = true;
		}

		if (ret) {
			// 여기서 유불리 판단 불리하면 false 유리하면 true
		}
	}
	return ret;
}

// 밀다가 불리할 때?
bool UnitControl::isUnReadyFinish() {
	bool ret = false;

	// 여기서 유불리 판단 불리하면 true 유리하면 false

	return ret;
}