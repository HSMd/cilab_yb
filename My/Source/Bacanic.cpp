#include "Bacanic.h"
#include "Instance.h"
#include "Work.h"
#include "Utility.h"

void Bacanic::init(){

	auto front = info->map->getCandidateLocation();
	Territory* territory = new Territory(front);
	info->map->addMyTerritory(territory);

	performer = nullptr;
	spare = nullptr;
	frameCount = 0;
	
	progress = BUILD_START;
}

void Bacanic::show(){

}

void Bacanic::draw(){

}

void Bacanic::run(){

	// 빌드진행.
	buildProcess();

	// 자동 유닛.
	autoUnit();	

	// 서치.
	search();

	// 앞마당 확장
	expand();

	// 빌드 진행.
	runBuild();

	// SCV 생산.
	scvCreate();	

	// 가스 컨트롤
	gasControl();

	// 심시티 짓고 나간 SCV 처리
	simOut();
	
	// 건물 옮기기.
	move();
}

void Bacanic::autoUnit(){
	/*
	if (1 <= info->self->getBuildingCount(UnitTypes::Terran_Factory)){
		flags[AUTO_TANK] = true;
	}

	if (4 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		flags[AUTO_SUPPLY] = true;
	}

	if (flags[BUILD_MID_MOVE]){
		if (flags[INVASION]){
			flags[AUTO_MARINE] = true;
		}
		else if(4 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks), true){
			flags[AUTO_MARINE] = true;
		}		
	}

	if (flags[AUTO_TANK]){
		if (scheduler->canAddUnitType(UnitTypes::Terran_Siege_Tank_Tank_Mode)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode), Scheduler::BUILD_SPEED::NORMAL);
		}
	}

	if (flags[AUTO_MARINE]){

		int marineCount = info->self->getUnitCount(UnitTypes::Terran_Marine, true);
		int medicCount = info->self->getUnitCount(UnitTypes::Terran_Medic, true);

		if (7 <= marineCount && medicCount * 4 < marineCount && scheduler->canAddUnitType(UnitTypes::Terran_Medic)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Medic, 2), Scheduler::BUILD_SPEED::SLOW);
		}

		if (scheduler->canAddUnitType(UnitTypes::Terran_Marine)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Marine), Scheduler::BUILD_SPEED::SLOW);
		}
	}
	*/

	/*
	if (flags[AUTO_SUPPLY]){
		if (Broodwar->self()->supplyTotal() < 200 * 2 && frameCount + 900 < bw->getFrameCount()){

			if (info->self->getBuildingCount(UnitTypes::Terran_Barracks) < 4){
				if (scheduler->getSupply() <= 5 * 2){
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
					frameCount = bw->getFrameCount();
				}
			}
			else{
				if (scheduler->getSupply() <= 8 * 2){
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
					frameCount = bw->getFrameCount();
				}
			}			
		}
	}
	*/
}

void Bacanic::search(){

	// 정찰.
	if (!flags[SEARCH] && 11 <= info->self->getUnitCount(UnitTypes::Terran_SCV) && 70 <= bw->self()->minerals()){

		auto worker = info->map->getMyTerritorys()[0]->getWorker(true);
		scouting->setSpy(worker);

		if (worker != nullptr)
			flags[SEARCH] = true;
	}
}

void Bacanic::gasControl(){

	auto startTerritory = info->map->getMyTerritorys().front();

	if (!flags[GAS_CONTROL]){
		if (88 <= bw->self()->gas()){
			startTerritory->setGasScvCount(2);
		}

		if (96 <= bw->self()->gas()){
			startTerritory->setGasScvCount(1);
		}
	}
	else{
		int gasCount = 3;
		startTerritory->setGasScvCount(3);
	}
}

void Bacanic::scvCreate(){

	auto& cnters = info->self->getBuildings(UnitTypes::Terran_Command_Center);
	auto& scvs = info->self->getImmediatelyUnit(UnitTypes::Terran_SCV);

	// SCV 뽑기.
	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center)){
		/*
		for (auto territory : info->map->getMyTerritorys()){


			int require = territory->getRequireSCVCount();
			auto cnter = territory->getCmdCenter()->self;

			if (1 <= require && scheduler->getReadyTrain(cnter) && scheduler->enoughResource(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV))
				cnter->train(UnitTypes::Terran_SCV);
		}
		*/
	}
	else{		
		if (progress == BUILD_START){
			if (scvs.size() == 15){
				if (1 <= info->self->getBuildingCount(UnitTypes::Terran_Factory, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			else if (scvs.size() == 19){
				if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}
			else if (scvs.size() == 20){
				if (3 <= info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true)){
					if (scheduler->getReadyTrain(cnters.front()->self))
						cnters.front()->self->train(UnitTypes::Terran_SCV);
				}
			}			
			else{
				if (scheduler->getReadyTrain(cnters.front()->self))
					cnters.front()->self->train(UnitTypes::Terran_SCV);
			}			
		}
	}
}

void Bacanic::expand(){

	

	/*
	if (!flags[CENTER_MOVE] && 2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center)){

		MyUnit* cnter = info->self->getBuildings(UnitTypes::Terran_Command_Center).back();
		cnter->targetPos = info->map->getMyTerritorys().back()->getTilePosition();

		moveBuildings.push_back(cnter);

		flags[CENTER_MOVE] = true;
	}
	*/
	
	/*
	if (!flags[ADD_CENTER] && 2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center, true)){
		info->map->getMyTerritorys().back()->init(info->map->getMyTerritorys().back()->getBase());
		info->map->getMyTerritorys().back()->setCmdCenter(info->self->getImmediatelyBuilding(UnitTypes::Terran_Command_Center).back());
		info->map->getMyTerritorys().back()->setFitScvCount(7);

		info->map->getMyTerritorys().back()->getReady() = false;

		flags[ADD_CENTER] = true;
	}

	if (spare == nullptr){
		spare = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::FRONT_BUNKER));
	}
	else{
		if (!flags[IMM_SPARE]){

			if (spare->state == MyUnit::STATE::MINERAL || spare->state == MyUnit::STATE::IDLE){
				info->map->getMyTerritorys().front()->delWorker(spare);
				info->map->getMyTerritorys().back()->addScv(spare);

				flags[IMM_SPARE] = true;
			}
		}
		
		if (spare->self->isCarryingMinerals() && info->self->getBuildingCount(UnitTypes::Terran_Command_Center) < 2){
			spare->self->move(info->map->getMyTerritorys().back()->getPosition());
		}
	}	

	if (flags[ADD_CENTER] && 85 <= getProgress(info->self->getImmediatelyBuilding(UnitTypes::Terran_Command_Center).back()->self)){
		
		if (!flags[IMM_PERFORMER]){

			info->map->getMyTerritorys().front()->delWorker(performer);
			info->map->getMyTerritorys().back()->addScv(performer);

			flags[IMM_PERFORMER] = true;
		}

		info->map->getMyTerritorys().back()->getReady() = true;
		info->map->getMyTerritorys().front()->immigrant(info->map->getMyTerritorys().back(), Territory::WORK::OVER);
	}	
	*/
}

void Bacanic::runBuild(){

	// 저장된 빌드들을 수행.
	for (auto iter = builds.begin(); iter != builds.end(); ++iter){
		BuildWork& build = (*iter);
		auto work = build.getWork();

		if (!build.getFire() && build.run()){

			if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_BARRACK)){
				auto performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_SUPPLY));
				work->setPerformer(performer);
			}
			else if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_SUPPLY2)){
				auto performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_BARRACK));
				work->setPerformer(performer);
			}

			scheduler->addSchedule(build.getWork(), build.getSpeed());

			iter = builds.erase(iter);
			iter--;
		}
	}
}

void Bacanic::buildProcess(){

	if (0 < info->self->getBuildingCount(UnitTypes::Terran_Factory, true)){
		//progress = BUILD_MID;
	}

	if (progress == BUILD_START){
		buildStart();
	}
	else if (progress == BUILD_MID){
		buildMid();
	}
}

void Bacanic::buildStart(){

	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Supply_Depot, true) && 1 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, true);
	}

	if (2 <= info->self->getBuildingCount(UnitTypes:: Terran_Command_Center, true)){
		flags[GAS_CONTROL] = true;
	}

	if (!flags[BUILD_START]){

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 8, false, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY), nullptr, 60));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 10, true, UnitTypes::Terran_Barracks, posMgr->getTilePosition(POS::SIM1_BARRACK)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 12, true, UnitTypes::Terran_Refinery));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 14, true, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY2), nullptr));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 12, true, UnitTypes::Terran_Marine));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 14, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, 50));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 14, true, UnitTypes::Terran_Machine_Shop));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Marine));

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Command_Center, posMgr->getTilePosition(POS::SECOND_CENTER), nullptr, 60));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Siege_Tank_Tank_Mode, 2));

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)));
		builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Supply_Depot, 3, true, TechTypes::Tank_Siege_Mode));

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, 3, true, UnitTypes::Terran_Engineering_Bay, posMgr->getTilePosition(POS::ENGINEERING)));

		flags[BUILD_START] = true;
	}
}

void Bacanic::simOut(){

	if (!state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1) || flags[ERROR_OUT]){
		return;
	}

	auto startTerritory = info->map->getMyTerritorys().front();
	auto chkPoint = getNearestChokepoint(startTerritory->getTilePosition());
	auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
	bool safe = info->map->getCloseEnemy().size() == 0;

	if (performer == nullptr){
		performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_SUPPLY2));
	}
	else{
		if (!performer->self->isConstructing()){
			if (!brk->self->isLifted()){
				if (performer->self->exists() && getDistance(chkPoint->getCenter(), performer->getPosition()) < 55){
					performer->state = MyUnit::STATE::OUT;

					if (brk->self->isIdle()){
						brk->self->lift();
					}					
				}
			}
			else{
				if (performer->self->exists() && getDistance(startTerritory->getTilePosition(), performer->getTilePosition()) <= 18){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}

				if (brk->self->isIdle()){
					performer->state = MyUnit::STATE::MINERAL;
				}
			}
		}
	}
}

void Bacanic::buildMid(){

	int closeDragoonCount = info->map->getCloseCount(UnitTypes::Protoss_Dragoon);

	if (1 < closeDragoonCount){
		flags[INVASION] = true;
	}

	if (!flags[BUILD_MID]){

		bw << "BUILD_MID" << endl;

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Academy, posMgr->getTilePosition(POS::ACADEMY), nullptr, 60));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 2, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));

		builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, TechTypes::Tank_Siege_Mode));

		flags[BUILD_MID] = true;
	}

	// 드라군 견제가 온다면 컴셋을 짓지 않는다.
	if (3 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		if (!flags[INVASION]){
			if (!flags[BUILD_MID_NO_INVASION]){

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 3, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 4, true, UnitTypes::Terran_Comsat_Station));

				flags[BUILD_MID_NO_INVASION] = true;
			}
		}
		else{
			if (!flags[BUILD_MID_INVASION]){

				if (1 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode, true)){
					builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 3, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));

					flags[BUILD_MID_INVASION] = true;
				}				
			}
		}
	}

	if (!flags[BUILD_MID_MOVE] && 3 <= info->self->getBuildingCount(UnitTypes::Terran_Barracks, true)){
		auto& firstBrk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
		firstBrk->targetPos = posMgr->getProperPosition(UnitTypes::Terran_Factory, true);

		moveBuildings.push_back(firstBrk);
		flags[BUILD_MID_MOVE] = true;
	}

	if (!flags[BUILD_MID_STIM_PACKS] && state->getCombatFlag(COMBAT_FLAG::SIEGE_MODE) && info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode, true)){
		builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Barracks, 4, true, TechTypes::Stim_Packs));
		flags[BUILD_MID_STIM_PACKS] = true;
	}

	if (!flags[BUILD_MID_U_238_SHELLS]){
		if (2 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode, true) && state->getCombatFlag(COMBAT_FLAG::STIM_PACKS)){
			builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Barracks, 4, true, UpgradeTypes::U_238_Shells, 1, nullptr, Scheduler::BUILD_SPEED::FAST));
			flags[BUILD_MID_U_238_SHELLS] = true;
		}		
	}
}

void Bacanic::move(){

	for (auto iter = moveBuildings.begin(); iter != moveBuildings.end(); iter++){

		auto unit = (*iter);

		if (unit->self->isLifted()){
			if (5 < getDistance(unit->getTilePosition(), unit->targetPos)){
				unit->self->move((Position)unit->targetPos);
			}
			else{
				if (unit->self->isIdle())
					unit->self->land(unit->targetPos);
			}
		}
		else{
			if (getDistance(unit->getTilePosition(), unit->targetPos) < 5){
				if (unit->self->isIdle()){
					iter = moveBuildings.erase(iter);
					iter--;
				}
			}
			else{
				unit->self->lift();
			}
		}
	}
}