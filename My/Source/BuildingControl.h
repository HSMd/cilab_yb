#pragma once
#include <BWAPI.h>
#include "MyUnit.h"

using namespace BWAPI;

class BuildingControl{

	BuildingControl();
	~BuildingControl();

public:
	static BuildingControl* getInstance();

	void run(MyUnit* unit);
};