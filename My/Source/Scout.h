#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include <vector>
#include "MyUnit.h"

using namespace std;
using namespace BWAPI;
using namespace BWTA;


class ChkPoint{
public:
	Position pos;
	bool chk;

	ChkPoint(Position pos){
		this->pos = pos;
		chk = false;
	}
};

class Scout
{

	enum S_FLAGS{
		JUDGE_M,
		JUDGE_TIME,
		JUDGE_WATAGATA1,
		JUDGE_WATAGATA2
	};

	Scout();
	~Scout();

	bool find;
	bool isIn;

	bool canUseData;

	int searchCount;
	int enemyDirection;

	int centerMoveCount;
	int centerMoveJudge = 0;

	MyUnit* performer;
	BaseLocation* target;
	BaseLocation* enemyFront;
	ChkPoint* point;
	
	map<S_FLAGS, bool> s_flags;

	vector<BaseLocation*> startBases;
	vector<ChkPoint*> chkPoints;

public:
	static Scout* getInstance();

	void init();
	void show();
	void draw();
	void run();

	void scvScout();

	void changeTarget();
	ChkPoint* getNextPoint();
	void endMission();

	void seeEverything();
	void hinder();
	void centerRotate();
	void findEnemyStart(BaseLocation* base);

	void setSpy(MyUnit* unit){ performer = unit; };
	MyUnit* getSpy(){ return performer; };

	int getSearchCount(){ return searchCount; };
	bool getUseData(){ return canUseData; };
	bool isFind(){ return find; };
};