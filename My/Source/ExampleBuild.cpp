#include "ExampleBuild.h"
#include "Instance.h"
#include "Work.h"

void ExampleBuild::init(){

	int s1 = 8;
	int s2 = 14;
	int gas = 12;
	int b1 = 10;
		
	builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_SCV, 1, false, TechTypes::Spider_Mines));
	builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_SCV, 1, false, UpgradeTypes::Ion_Thrusters));

	switch (info->map->getMyDirection()) {

	case 0:
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s1, true, UnitTypes::Terran_Supply_Depot, TilePosition(11, 26)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, b1, true, UnitTypes::Terran_Barracks, TilePosition(4, 28)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, gas, true, UnitTypes::Terran_Refinery, TilePosition(0, 0)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s2, true, UnitTypes::Terran_Supply_Depot, TilePosition(8, 26)));

		break;

	case 1:
		
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s1, true, UnitTypes::Terran_Supply_Depot, TilePosition(97, 5)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, b1, true, UnitTypes::Terran_Barracks, TilePosition(102, 9)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, gas, true, UnitTypes::Terran_Refinery, TilePosition(0, 0)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s2, true, UnitTypes::Terran_Supply_Depot, TilePosition(100, 7)));

		break;

	case 2:

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s1, true, UnitTypes::Terran_Supply_Depot, TilePosition(23, 118)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, b1, true, UnitTypes::Terran_Barracks, TilePosition(24, 120)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, gas, true, UnitTypes::Terran_Refinery, TilePosition(0, 0)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s2, true, UnitTypes::Terran_Supply_Depot, TilePosition(28, 121)));

		break;

	case 3:

		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s1, true, UnitTypes::Terran_Supply_Depot, TilePosition(120, 97)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, b1, true, UnitTypes::Terran_Barracks, TilePosition(114, 101)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, gas, true, UnitTypes::Terran_Refinery, TilePosition(0, 0)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s2, true, UnitTypes::Terran_Supply_Depot, TilePosition(118, 99)));

		break;
	}

	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 12, false, UnitTypes::Terran_Marine));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 14, true, UnitTypes::Terran_Factory, TilePosition(0, 0)));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 14, true, UnitTypes::Terran_Factory, TilePosition(0, 0)));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Machine_Shop, TilePosition(0, 0)));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 20, true, UnitTypes::Terran_Supply_Depot, TilePosition(0, 0)));
}

void ExampleBuild::show(){

}

void ExampleBuild::draw(){

}

void ExampleBuild::run(){

	/*
	 *   현재는 정적 빌드만으로 구성된 빌드.
	 *   추후 state에 상태들을 정의( 질럿 러쉬, 올 프로브, 등등 ) 해서 아래와 같이 이후 상황에 대한 대처를 만들어줘야됨.
	 *   따라서 이것은 예시일뿐 builds에 미리 들어가있는 것은 3개정도 혹은 미리 들어가 있어도 되는 것들에 지나지 않아야 된다.
	 *   (ex. MachineShop - 넣어둬도 Factory에 의존적이기 때문에 Factory만 잘 지어주면 됨)
	 *   현재 예시를 통해 Scheduler 에 대한 어느정도 감을 익히기 바람.
	 *
	 *   과제 1.
	 *   자신의 Build를 하나 구현해보도록 하자. (새파일 만들어서 꼭.) ( Youtube 영상을 통해 빌드 하나를 보고 구현 해 볼 것  - 최적화도 할것.)
	 *
	 *   [미구현 상황]
	 *   현재 Upgrade / Research 에 대한것들은 아직 Scheduler에 미구현 상태. ( 귀찮았다. ) - 코드로 구현하셈.
	 *	 Territory에 죽은 SCV 안빠짐. (우측 상단 Count 보면 알꺼임.)  
	 */

	// 자주 사용하는 유닛타입.
	auto tankType = UnitTypes::Terran_Siege_Tank_Tank_Mode;
	auto barrakType = UnitTypes::Terran_Barracks;

	// 공격 플래그 온. (example. 내 탱크가 2기 이상일 경우)
	if (!state->getCombatFlag(COMBAT_FLAG::ATTACK) && 2 <= info->self->getUnitCount(tankType)){

		// 가스 컨트롤 예시. 3 -> 1
		//info->map->getMyTerritorys()[0]->setGasScvCount(1);

		if (0 < info->self->getBuildingCount(barrakType)){
			if (info->self->getBuildings(barrakType)[0]->self->lift()){
				state->setCombatFlag(COMBAT_FLAG::ATTACK, true);
			}
		}
		else{
			state->setCombatFlag(COMBAT_FLAG::ATTACK, true);
		}		
	}

	// 머신샵이 지어지고난 이후부터 탱크 지속적인 생산.
	if (0 < info->self->getBuildingCount(UnitTypes::Terran_Machine_Shop)){
		if (scheduler->canAddUnitType(tankType)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, tankType), Scheduler::BUILD_SPEED::SLOW);
		}
	}

	// 부족한 SCV를 뽑는다.
	for (auto territory : info->map->getMyTerritorys()){

		int require = territory->getRequireSCVCount();

		if (territory->isRequireScv() && scheduler->canAddUnitType(UnitTypes::Terran_SCV)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV), Scheduler::BUILD_SPEED::NORMAL);
		}
	}

	// 저장된 빌드들을 수행.
	for (auto iter = builds.begin(); iter != builds.end(); ++iter){
		BuildWork& build = (*iter);

		if (!build.getFire() && build.run()){

			scheduler->addSchedule(build.getWork());
			build.onFire();
		}
	}
}
