#include "Guard.h"
#include "Utility.h"
#include "Instance.h"
#include "CombatControl.h"

Guard::Guard()
{	
	trace = false;
	frontSight = true;
}

Guard::~Guard()
{

}

Guard* Guard::getInstance() {
	static Guard instance;
	return &instance;
}

void Guard::show()
{

}

void Guard::init()
{
	switch (info->map->getMyDirection())
	{
	case 0:
		marinePosition.push_back(tile2pos(TilePosition(11, 26)));
		marinePosition.push_back(tile2pos(TilePosition(7, 26)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 27)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(14, 27)) + Position(0, 16));
		marinePosition.push_back(tile2pos(TilePosition(6, 31)) + Position(16, 0));
		break;
	case 1:
		marinePosition.push_back(tile2pos(TilePosition(103, 8)));
		marinePosition.push_back(tile2pos(TilePosition(100, 6)));
		marinePosition.push_back(tile2pos(TilePosition(101, 6)));
		marinePosition.push_back(tile2pos(TilePosition(96, 6)) + Position(16, 0));
		marinePosition.push_back(tile2pos(TilePosition(103, 12)) + Position(16, 0));
		break;
	case 2:
		marinePosition.push_back(tile2pos(TilePosition(24, 121)));
		marinePosition.push_back(tile2pos(TilePosition(23, 117)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(27, 123)));
		marinePosition.push_back(tile2pos(TilePosition(28, 123)));
		marinePosition.push_back(tile2pos(TilePosition(31, 121)) + Position(16, 16));
		break;

	case 3:
		marinePosition.push_back(tile2pos(TilePosition(118, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 101)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(119, 102)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 99)) + Position(16, 16));
		marinePosition.push_back(tile2pos(TilePosition(121, 100)) + Position(16, 16));
		break;
	}

	if (info->map->getMyDirection() == 0) {
		tankPosition.push_back(TilePosition(21, 30));
		tankPosition.push_back(TilePosition(4, 31));
		tankPosition.push_back(TilePosition(15, 36));
		tankPosition.push_back(TilePosition(20, 29));		
		tankPosition.push_back(info->map->getMyStartBase()->getTilePosition() + TilePosition(5, 3));
		tankPosition.push_back(TilePosition(14, 35));

		/*
		tankPosition.push_back(TilePosition(25, 34));
		tankPosition.push_back(TilePosition(29, 31));
		tankPosition.push_back(TilePosition(36, 33));
		tankPosition.push_back(TilePosition(26, 41));
		tankPosition.push_back(TilePosition(32, 39));

		//bangMission = (TilePosition(4, 31));

		DefensePos[0] = TilePosition(30, 37);
		DefensePos[1] = TilePosition(38, 47);
		*/
	}
	else if (info->map->getMyDirection() == 1) {
		tankPosition.push_back(TilePosition(100, 18));
		tankPosition.push_back(TilePosition(93, 4));
		tankPosition.push_back(TilePosition(93, 15));
		tankPosition.push_back(TilePosition(101, 19));				
		tankPosition.push_back(info->map->getMyStartBase()->getTilePosition() + TilePosition(0, 3));
		tankPosition.push_back(TilePosition(92, 14));

		/*
		tankPosition.push_back(TilePosition(94, 20));
		tankPosition.push_back(TilePosition(97, 24));
		tankPosition.push_back(TilePosition(97, 29));
		tankPosition.push_back(TilePosition(87, 25));
		tankPosition.push_back(TilePosition(98, 32));

		bangMission = (TilePosition(93, 4));

		DefensePos[0] = TilePosition(93, 26);
		DefensePos[1] = TilePosition(92, 34);
		*/
	}
	else if (info->map->getMyDirection() == 2) {
		tankPosition.push_back(TilePosition(25, 110));
		tankPosition.push_back(TilePosition(33, 123));
		tankPosition.push_back(TilePosition(33, 114));
		tankPosition.push_back(TilePosition(23, 109));
		tankPosition.push_back(info->map->getMyStartBase()->getTilePosition() + TilePosition(5, 0));
		tankPosition.push_back(TilePosition(34, 114));

		/*
		tankPosition.push_back(TilePosition(30, 103));
		tankPosition.push_back(TilePosition(39, 105));
		tankPosition.push_back(TilePosition(30, 97));
		tankPosition.push_back(TilePosition(33, 99));
		tankPosition.push_back(TilePosition(37, 102));

		bangMission = (TilePosition(33, 123));

		DefensePos[0] = TilePosition(34, 101);
		DefensePos[1] = TilePosition(45, 94);
		*/
	}
	else if (info->map->getMyDirection() == 3) {
		tankPosition.push_back(TilePosition(105, 97));
		tankPosition.push_back(TilePosition(123, 96));
		tankPosition.push_back(TilePosition(112, 93));
		tankPosition.push_back(TilePosition(106, 98));
		tankPosition.push_back(info->map->getMyStartBase()->getTilePosition() + TilePosition(-1, 0));
		tankPosition.push_back(TilePosition(113, 94));

		/*
		tankPosition.push_back(TilePosition(99, 95));
		tankPosition.push_back(TilePosition(101, 89));
		tankPosition.push_back(TilePosition(91, 92));
		tankPosition.push_back(TilePosition(91, 90));
		tankPosition.push_back(TilePosition(97, 89));

		bangMission = (TilePosition(123, 96));

		DefensePos[0] = TilePosition(96, 91);
		DefensePos[1] = TilePosition(88, 91);
		*/
	}
}

void Guard::update(){
	getUnits(UnitTypes::Terran_Marine);
	getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode, 5);
}

void Guard::run()
{	
	update();
	guardSim1Defense();
	tankHold();
}

void Guard::guardSim1Defense(){

	auto& allClose = info->map->getCloseEnemy();
	auto& startClose = info->map->getMyTerritorys()[0]->getCloseEnemy();

	static bool enemySearch = false;
	static bool probeRush = false;
	
	if (allClose.size() - getCloseCount(UnitTypes::Protoss_Probe) == 0){
		if (startClose.size() == 0){
			eraseUnits(UnitTypes::Terran_SCV);
		}
		
		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2)){
			marineFront();
		}	
		else{
			probeDefense();
		}
	}
	//probe + zealot rush
	else if (allClose.size() == getCloseCount(UnitTypes::Protoss_Probe) + getCloseCount(UnitTypes::Protoss_Zealot)){

		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2)){
			marineFront();
		}
		else{
			int count = 0;
			auto chkPoint = BWTA::getNearestChokepoint(info->map->getMyStartBase()->getTilePosition());
			frontSight = false;

			for (auto unit : info->map->getMyTerritorys()[0]->getCloseEnemy()){
				if (getDistance(unit->getPosition(), chkPoint->getCenter()) < 200){
					count++;
				}
			}

			if (info->map->getMyTerritorys()[0]->getCloseEnemy().size() - count == 0){
				if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1))
					marineHold();
				else{
					//bw << "No Guard Zealot" << endl;
				}
			}
			else{
				//bw << "IN" << endl;
				// 좆됨.. SCV데리고 같이, 생산 추가.
			}
		}
	}
	else if (0 < getCloseCount(UnitTypes::Protoss_Pylon)){

		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1))
		{
			if (!info->map->getCloseEnemy().empty())
			{
				pylonDefense();
			}
			else{
				eraseUnits(UnitTypes::Terran_SCV);
			}
			
		}
		else{
			pylonDefense();
		}
		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2))
		{
			if (!info->map->getCloseEnemy().empty())
			{
				pylonDefense();
			}
		}
	}
	else if (0 < getCloseCount(UnitTypes::Protoss_Dragoon)){
		int dragoonCount = getCloseCount(UnitTypes::Protoss_Dragoon);
		frontSight = false;		
		
		if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2)){
			Unit target = nullptr;
			int minHit = 9999;

			for (auto unit : allClose){
				if (unit->getHitPoints() < minHit){
					target = unit;
					minHit = unit->getHitPoints();
				}
			}

			for (auto unit : guardUnits[UnitTypes::Terran_Marine]){
				if (target != nullptr){
					unit->self->rightClick(target);
				}
			}
		}
		else{
			scvRepair(dragoonCount);
			marineHold();
		}
	}
}

void Guard::getUnits(UnitType type, int count){

	auto unitController = UnitControl::getInstance()->getInstance();

	while (true){
		MyUnit* unit = unitController->getUnit(type);
		if (unit == nullptr || (count != -1 && count <= guardUnits[type].size())){
			return;
		}

		unit->state = MyUnit::STATE::IDLE;
		guardUnits[type].push_back(unit);
	}
}

void Guard::eraseUnits(UnitType type){

	auto& units = guardUnits[type];

	for (auto unit : units){
		unit->state = MyUnit::STATE::IDLE;
	}

	units.clear();
}

int Guard::getCloseCount(UnitType type){

	return info->map->getCloseCount(type);
}

////////////

void Guard::probeDefense(){
	
	// 유닛 할당.
	if (!state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
		noSimGuard();
	}
	else{
		eraseUnits(UnitTypes::Terran_SCV);

		if (info->map->getCloseEnemy().size() == 0)
		{
			if (frontSight){
				marineFront();
			}
			else{
				marineHold();
			}
		}
		else
		{
			noSimGuard();
		}
	}
}

void Guard::pylonDefense()
{
	// 파일런 - 입구방해 혹은 포토러쉬 본진 게이트 등.
	auto& close = info->map->getCloseEnemy();
	int probeCount = info->map->getCloseCount(UnitTypes::Protoss_Probe);
	int pylonCount = info->map->getCloseCount(UnitTypes::Protoss_Pylon);

	state->setCombatFlag(COMBAT_FLAG::INVASION_PYLON, true);

	//int photoCount = info->map->getCloseCount(UnitTypes::Protoss_Photon_Cannon);
	//int gateCount = info->map->getCloseCount(UnitTypes::Protoss_Gateway);

	auto& scvs = guardUnits[UnitTypes::Terran_SCV];

	int scvGetSize = probeCount + (pylonCount * 2) - scvs.size();
	int minHit = 9999;

	// 파일런이 있을 경우.
	Unit pylon = nullptr;
	Unit probe = nullptr;

	for (auto unit : close){
		if (unit->getType() == UnitTypes::Protoss_Pylon){
			if (pylon == nullptr){
				pylon = unit;
			}
		}
		else{
			if (unit->getHitPoints() < minHit){
				minHit = unit->getHitPoints();
				probe = unit;
			}
		}
	}

	if (0 < scvGetSize){
		for (int i = 0; i < scvGetSize; i++){
			auto worker = info->map->getMyTerritorys()[0]->getWorker();
			if (worker != nullptr){
				scvs.push_back(worker);
			}
		}
	}

	auto probeTracer = scvs.front();
	if (probe != nullptr){
		probeTracer->self->attack(probe);
	}
	else{
		probeTracer->self->attack(pylon);
	}

	for (auto scv : scvs){
		if (scv == probeTracer)
			continue;

		scv->self->rightClick(pylon);
	}

	for (auto& unit : guardUnits[UnitTypes::Terran_Marine]){
		if (unit->self->exists()){
			unit->self->rightClick(pylon);
		}		
	}
}

void Guard::scvRepair(int count)
{
	auto& scvs = guardUnits[UnitTypes::Terran_SCV];
	int maxValue = -1;
	MyUnit *temp = nullptr;

	for (auto building = info->self->getAllBuildings().begin(); building != info->self->getAllBuildings().end(); building++)
	{
		if ((*building)->self->getType().maxHitPoints() - (*building)->self->getHitPoints() > 0)
		{
			if ((*building)->self->getType().maxHitPoints() - (*building)->self->getHitPoints() > maxValue)
			{
				temp = *building;
			}
		}
		                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
	}

	if (temp != nullptr)
	{
		for (int i = scvs.size(); i < count; i++)
		{
			auto scv = info->map->getNearTerritory(temp->getTilePosition())->getWorker(false);
			if (scv != nullptr)
			{
				scvs.push_back(scv);
			}
		}
		
		for (auto scv : scvs)
		{
			if (scv->self->exists()) 
			{
				scv->self->repair(temp->self);
			}
		}
	}
	else
	{
		eraseUnits(UnitTypes::Terran_SCV);
		/*for (auto scv = scvs.begin(); scv != scvs.end();)
		{
			if ((*scv)->self->exists())
			{
				(*scv)->state = MyUnit::STATE::IDLE;
				scv = scvs.erase(scv);
			}
		}*/
	}
}

void Guard::noSimGuard(){

	auto& close = info->map->getMyTerritorys()[0]->getCloseEnemy();
	int probeCount = getCloseCount(UnitTypes::Protoss_Probe);

	auto& scvs = guardUnits[UnitTypes::Terran_SCV];
	Unit target = nullptr;
	int scvGetSize = probeCount - scvs.size();
	int minHit = 9999;

	if (0 == info->self->getUnitCount(UnitTypes::Terran_Marine))
	{
		// 입구가 안막히고 마린이 없을 때.
		for (auto unit : close){
			if (unit->isStartingAttack() || unit->isAttacking()){
				trace = true;
			}
		}

		if (close.size() == 0){
			trace = false;
		}

		if (trace == true){
			// SCV 숫자 관리.
			if (0 <= scvGetSize){
				for (int i = 0; i < scvGetSize; i++){
					auto worker = info->map->getMyTerritorys()[0]->getWorker();
					if (worker != nullptr){
						scvs.push_back(worker);
					}
				}
			}
			else{
				int over = 1.5 * probeCount;

				for (auto iter = scvs.begin(); over < scvs.size(); iter++){
					(*iter)->state = MyUnit::STATE::IDLE;

					iter = scvs.erase(iter);
					iter--;
				}
			}
		}
	}
	else{
		// 입구가 안막히고 마린이 있을 경우
		if (probeCount < 2){
			eraseUnits(UnitTypes::Terran_SCV);
		}
	}

	// 타겟을 선정한다.
	for (auto unit : close){
		if (unit->getHitPoints() < minHit){
			minHit = unit->getHitPoints();
			target = unit;
		}
	}

	// SCV 공격.
	for (auto iter = scvs.begin(); iter != scvs.end(); iter++){
		auto scv = (*iter);

		if (!scv->self->exists()){
			iter = scvs.erase(iter);
			iter--;
			continue;
		}

		if (target != nullptr){
			//scv->self->rightClick(target);
			scv->self->attack(target);
		}
	}

	// Marine 공격
	for (auto unit : guardUnits[UnitTypes::Terran_Marine]){
		if (!unit->self->exists()){
			continue;
		}

		if (target != nullptr){
			unit->self->rightClick(target);
			//unit->self->attack(target);
		}
	}
}

void Guard::marineHold()
{
	auto& Marine = guardUnits[UnitTypes::Terran_Marine];

	int count = 0;
	for (auto m = Marine.begin(); m != Marine.end(); m++)
	{
		if ((*m)->self->isCompleted() && (*m)->self->exists())
		{
			if (count < marinePosition.size())
			{
				if (isAlmostReachedDest((*m)->getPosition(), marinePosition[count], 2, 2))
				{
					if (!(*m)->self->isHoldingPosition())
					{
						(*m)->self->holdPosition();
					}
				}
				else
				{
					(*m)->self->move(marinePosition[count]);
				}
			}
		}

		count++;
	}
}

void Guard::marineFront(){

	auto& allClose = info->map->getCloseEnemy();
	//auto p = info->map->getMyTerritorys().back()->getPosition();
	auto p = (Position)posMgr->getTilePosition(POS::FRONT_SUPPLY);

	Unit target = nullptr;

	for (auto unit : guardUnits[UnitTypes::Terran_Marine]){
		if (!unit->self->exists()){
			continue;
		}		

		if (120 < getDistance(p, unit->getPosition())){
			unit->self->attack(p);
		}
		else{
			unit->self->holdPosition();
		}
	}	
}

void Guard::tankHold(){

	// 탱크 배치.
	for (auto pos : tankPosition){
		if (holdTanks[pos]){
			continue;
		}

		auto tank = getIdleUnit(guardUnits[UnitTypes::Terran_Siege_Tank_Tank_Mode]);
		if (tank == nullptr){
			break;
		}

		tank->state = MyUnit::STATE::HOLD;
		tank->targetPos = pos;
		holdTanks[pos] = true;
	}

	for (auto iter = guardUnits[UnitTypes::Terran_Siege_Tank_Tank_Mode].begin(); iter != guardUnits[UnitTypes::Terran_Siege_Tank_Tank_Mode].end(); ++iter){
		auto unit = (*iter);
		if (!unit->self->exists()){

			holdTanks[unit->targetPos] = false;

			iter = guardUnits[UnitTypes::Terran_Siege_Tank_Tank_Mode].erase(iter);
			iter--;
		}

		if (unit->self->isCompleted() && unit->state == MyUnit::STATE::HOLD){
			holdTankControl(unit);
		}
	}
}

MyUnit* Guard::getIdleUnit(vector<MyUnit*>& units){
	for (auto unit : units){
		if (!unit->self->exists()){
			continue;
		}

		if (unit->state == MyUnit::STATE::IDLE){
			return unit;
		}
	}

	return nullptr;
}

void Guard::holdTankControl(MyUnit* unit){

	bool safe = true;

	if (safe){
		if (2 < getDistance(unit->targetPos, unit->self->getTilePosition())){
			unit->self->move((Position)unit->targetPos);
		}
		else{
			if (unit->self->canSiege())
				unit->self->siege();
			else{
				if (!unit->self->isAttacking()){
					Unitset closeSet;
					for (auto unit : info->map->getCloseEnemy()){
						closeSet.insert(unit);
					}

					auto target = CombatControl::getInstance()->getTargetUnit(unit, closeSet);

					unit->self->attack(target);
				}
			}
		}
	}
	else{

	}	
}