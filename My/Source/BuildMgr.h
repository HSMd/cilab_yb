#pragma once
#include "BWAPI.h"
#include "MyUnit.h"
#include "ExampleBuild.h"
#include "StudyBuild.h"
#include "OneFacDouble.h"
#include "LastBuild.h"
#include "Bacanic.h"

#include <vector>

using namespace BWAPI;
using namespace std;

class BuildMgr {

	ExampleBuild example;
	StudyBuild study;
	OneFacDouble oneFacDouble;
	LastBuild last;
	Bacanic bacanic;

	BuildMgr(){};
	~BuildMgr(){};

public:
	
	static BuildMgr* getInstance();

	void init();
	void show();
	void draw();
	void run();
};