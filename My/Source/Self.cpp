#include "Self.h"
#include "Utility.h"
#include "Instance.h"

Self::Self(){

}

Self::~Self(){

}

void Self::update(){
	
}

void Self::show(int startX, int startY){

	Broodwar->drawTextScreen(startX, startY, "ALL_SCV : %d", getUnitCount(UnitTypes::Terran_SCV, true));
}

void Self::draw(){

	for (auto unit : getAllUnits()){
		drawUnit(unit->self, DRAW_MY, DRAW_UNIT);
	}

	for (auto unit : getAllBuildings()){
		drawUnit(unit->self, DRAW_MY, DRAW_BUILDING);
	}
}

void Self::destroy(Unit unit){

	UnitType type = unit->getType();

	if (type.isBuilding()){
		delData(allBuildings, unit);
		delData(completedBuildings[type], unit);
		delData(createBuildings[type], unit);
	}
	else{

		delData(allUnits, unit);
		delData(createUnits[type], unit);
		delData(completedUnits[type], unit);		
	}
}

void Self::morph(Unit unit){

}

void Self::delData(vector <MyUnit*>& data, Unit unit){

	for (auto iter = data.begin(); iter != data.end(); iter++) {
		Unit find = (*iter)->self;

		if (isSame(find, unit)){
			data.erase(iter);
			break;
		}
	}
}

void Self::addUnit(BWAPI::Unit unit){
	if (unit->getType() == UnitTypes::Terran_Siege_Tank_Tank_Mode) {
		/*for (auto q = getAllUnits().begin(); q != getAllUnits().end(); q++) {
			if (isSame(unit, (*q)->self)) {
				allUnits.erase(q);
				break;
			}
		}
		for (auto q = getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode).begin(); q != getUnits(UnitTypes::Terran_Siege_Tank_Siege_Mode).end(); q++) {
			if (isSame(unit, (*q)->self)) {
				allUnits.erase(q);
				break;
			}
		}*/
		delData(allUnits, unit);
		delData(createUnits[UnitTypes::Terran_Siege_Tank_Siege_Mode], unit);
		delData(completedUnits[UnitTypes::Terran_Siege_Tank_Siege_Mode], unit);
	}
	else if (unit->getType() == UnitTypes::Terran_Siege_Tank_Siege_Mode) {
		/*for (auto q = getAllUnits().begin(); q != getAllUnits().end(); q++) {
			if (isSame(unit, (*q)->self)) {
				allUnits.erase(q);
				break;
			}
		}
		for (auto q = getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).begin(); q != getUnits(UnitTypes::Terran_Siege_Tank_Tank_Mode).end(); q++) {
			if (isSame(unit, (*q)->self)) {
				allUnits.erase(q);
				break;
			}
		}*/
		delData(allUnits, unit);
		delData(createUnits[UnitTypes::Terran_Siege_Tank_Tank_Mode], unit);
		delData(completedUnits[UnitTypes::Terran_Siege_Tank_Tank_Mode], unit);
	}

	completedUnits[unit->getType()].push_back(new MyUnit(unit));
	allUnits.push_back(new MyUnit(unit));
}

void Self::addUnitImmediately(BWAPI::Unit unit){
	
	createUnits[unit->getType()].push_back(new MyUnit(unit));
}

void Self::addBuilding(BWAPI::Unit building){

	completedBuildings[building->getType()].push_back(new MyUnit(building));
	allBuildings.push_back(new MyUnit(building));

	if (building->getType() == UnitTypes::Terran_Refinery){
		auto territory = info->map->getNearTerritory(building->getTilePosition());
		territory->setGas(building);
	}
}

void Self::addBuildingImmediately(BWAPI::Unit building){

	createBuildings[building->getType()].push_back(new MyUnit(building));
}

vector<MyUnit*>& Self::getUnits(UnitType type)
{
	return completedUnits[type];
}

vector<MyUnit*>& Self::getImmediatelyUnit(UnitType type){
	return createUnits[type];
}

vector<MyUnit*>& Self::getBuildings(UnitType type){
	return completedBuildings[type];
}

vector<MyUnit*>& Self::getImmediatelyBuilding(UnitType type){
	return createBuildings[type];
}

vector<MyUnit*>& Self::getAllUnits(){
	return allUnits;
}

vector<MyUnit*>& Self::getAllBuildings(){
	return allBuildings;
}

int Self::getUnitCount(UnitType type, bool immediately){

	if (immediately)
		return createUnits[type].size();

	return completedUnits[type].size();
}

int Self::getBuildingCount(UnitType type, bool immediately){

	if (immediately)
		return createBuildings[type].size();

	return completedBuildings[type].size();
}