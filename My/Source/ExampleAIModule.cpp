#include "ExampleAIModule.h"
#include "CombatControl.h"
#include "Instance.h"


using namespace BWAPI;
using namespace Filter;


ExampleAIModule::ExampleAIModule(){
}

ExampleAIModule::~ExampleAIModule(){

}

void ExampleAIModule::onStart()
{
	// Check if this is a replay
	if (Broodwar->isReplay())
	{
		Broodwar << "The following players are in this replay:" << std::endl;

		Playerset players = Broodwar->getPlayers();
		for (auto p : players)
		{
			// Only print the player if they are not an observer
			if (!p->isObserver())
				Broodwar << p->getName() << ", playing as " << p->getRace() << std::endl;
		}
	}
	else // if this is not a replay
	{
		if (Broodwar->enemy()) // First make sure there is an enemy
			Broodwar << "The matchup is " << Broodwar->self()->getRace() << " vs " << Broodwar->enemy()->getRace() << std::endl;
	}

	cmd->init();
	cmd->start();
	//Broodwar->sendText("black sheep wall");

	printf("mineral size %d\n", BWAPI::Broodwar->getMinerals().size());

}

void ExampleAIModule::onEnd(bool isWinner)
{
	// Called when the game ends
	if (isWinner)
	{
		// Log your win here!
	}
	printf("frameCount = %d\n", BWAPI::Broodwar->getFrameCount());
}

void ExampleAIModule::onFrame()
{
	cmd->show();

	if (Broodwar->isReplay() || Broodwar->isPaused() || !Broodwar->self())
		return;
	if (Broodwar->getFrameCount() % Broodwar->getLatencyFrames() != 0)
		return;

	cmd->run();

}

void ExampleAIModule::onSendText(std::string text)
{
	// Send the text to the game if it is not being processed.
	Broodwar->sendText("%s", text.c_str());

	if (text == "m") {
		Broodwar->sendText("show me the money");
	}
	else if (text == "o") {
		Broodwar->sendText("operation cwal");
	}
	else if (text == "b") {
		Broodwar->sendText("black sheep wall");
	}
	else if (text == "p") {
		Broodwar->sendText("power overwhelming");
	}
	else if (text == "modify") {
		Broodwar->sendText("modify the pahse variance");
	}
	else if (text == "g") {
		Broodwar->sendText("the gathering");
	}
	else if (text == "f") {
		Broodwar->sendText("food for thought");
	}
	else if (text == "0") {
		Broodwar->setLocalSpeed(0);
		Broodwar->sendText("speed change 0");
	}
	else if (text == "1") {
		Broodwar->setLocalSpeed(1);
		Broodwar->sendText("speed change 1");
	}
	else if (text == "5") {
		Broodwar->setLocalSpeed(5);
		Broodwar->sendText("speed change 5");
	}
	else if (text == "10") {
		Broodwar->setLocalSpeed(10);
		Broodwar->sendText("speed change 10");
	}
	else if (text == "15") {
		Broodwar->setLocalSpeed(15);
		Broodwar->sendText("speed change 15");
	}
	else if (text == "30") {
		Broodwar->setLocalSpeed(30);
		Broodwar->sendText("speed change 30");
	}
	else if (text == "60") {
		Broodwar->setLocalSpeed(60);
		Broodwar->sendText("speed change 60");
	}
	else if (text == "120") {
		Broodwar->setLocalSpeed(120);
		Broodwar->sendText("speed change 120");
	}
	else if (text == "2") {
		Broodwar->setLocalSpeed(-1);
		Broodwar->sendText("speed change default speed");
	}
}

void ExampleAIModule::onReceiveText(BWAPI::Player player, std::string text)
{
	// Parse the received text
	Broodwar << player->getName() << " said \"" << text << "\"" << std::endl;
}

void ExampleAIModule::onPlayerLeft(BWAPI::Player player)
{
	// Interact verbally with the other players in the game by
	// announcing that the other player has left.
	Broodwar->sendText("Goodbye %s!", player->getName().c_str());
}

void ExampleAIModule::onNukeDetect(BWAPI::Position target)
{

	// Check if the target is a valid position
	if (target)
	{
		// if so, print the location of the nuclear strike target
		Broodwar << "Nuclear Launch Detected at " << target << std::endl;
	}
	else
	{
		// Otherwise, ask other players where the nuke is!
		Broodwar->sendText("Where's the nuke?");
	}

	// You can also retrieve all the nuclear missile targets using Broodwar->getNukeDots()!
}

void ExampleAIModule::onUnitDiscover(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitEvade(BWAPI::Unit unit)
{
	//printf("%s 이/가 evade 발동 했어여 id 는 %d\n", unit->getType().c_str(), unit->getID());
}

void ExampleAIModule::onUnitShow(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitHide(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitCreate(BWAPI::Unit unit)
{
	if (Broodwar->isReplay())
	{
		// if we are in a replay, then we will print out the build order of the structures
		if (unit->getType().isBuilding() && !unit->getPlayer()->isNeutral())
		{
			int seconds = Broodwar->getFrameCount() / 24;
			int minutes = seconds / 60;
			seconds %= 60;
			Broodwar->sendText("%.2d:%.2d: %s creates a %s", minutes, seconds, unit->getPlayer()->getName().c_str(), unit->getType().c_str());
		}
	}
	else{
		if (unit->getPlayer() == Broodwar->self()){
			if (unit->getType().isBuilding()){
				cmd->structureCreate(unit);
			}
			else if (!unit->getType().isSpell()){
				cmd->unitCreate(unit);
			}
		}
		else {
			cmd->unitCreate(unit);
		}
	}
	//if(unit->getPlayer() == BWAPI::Broodwar->enemy())
		//printf("%s 이/가 create 발동 했어여 id 는 %d\n", unit->getType().c_str(), unit->getID());
}

void ExampleAIModule::onUnitDestroy(BWAPI::Unit unit)
{
	cmd->destroy(unit);
}

void ExampleAIModule::onUnitMorph(BWAPI::Unit unit)
{
	if (Broodwar->isReplay())
	{
		// if we are in a replay, then we will print out the build order of the structures
		if (unit->getType().isBuilding() && !unit->getPlayer()->isNeutral())
		{
			int seconds = Broodwar->getFrameCount() / 24;
			int minutes = seconds / 60;
			seconds %= 60;
			Broodwar->sendText("%.2d:%.2d: %s morphs a %s", minutes, seconds, unit->getPlayer()->getName().c_str(), unit->getType().c_str());
		}
	}
	else{
		if (unit->getPlayer() == Broodwar->self() && unit->getType() == UnitTypes::Terran_Refinery){
			cmd->structureCreate(unit);
		}
		else {
			cmd->unitMorph(unit);
		}
	}

	//printf("%s 이/가 morph 발동 했어여 id 는 %d\n", unit->getType().c_str(), unit->getID());
}

void ExampleAIModule::onUnitRenegade(BWAPI::Unit unit)
{
	//printf("%s 이/가 renegade 발동 했어여 id 는 %d\n", unit->getType().c_str(), unit->getID());
}

void ExampleAIModule::onSaveGame(std::string gameName)
{
	Broodwar << "The game was saved to \"" << gameName << "\"" << std::endl;
}

void ExampleAIModule::onUnitComplete(BWAPI::Unit unit)
{
	if (!Broodwar->isReplay()){
		if (unit->getPlayer() == Broodwar->self()){
			if (unit->getType().isBuilding()){

				cmd->structureComplete(unit);
			}
			else if (!unit->getType().isSpell()){

				cmd->unitComplete(unit);
			}
		}
	}
	//if (unit->getPlayer() == BWAPI::Broodwar->enemy())
		//printf("%s 이/가 completed 발동 했어여 id 는 %d\n", unit->getType().c_str(), unit->getID());
}
