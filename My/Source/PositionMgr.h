#pragma once
#include "BWAPI.h"
#include "MyUnit.h"
#include "Work.h"
#include "Territory.h"

#include <map>
#include <vector>

using namespace BWAPI;
using namespace std;

enum POS{
	ENGINE_SIGHT_1,
	ENGINE_SIGHT_2,

	FIRST_SUPPLY,

	SIM1_SUPPLY,
	SIM1_BARRACK,
	SIM1_SUPPLY2,

	SIDE_BARRACK,
	DOWN_SUPPLY,

	SECOND_CENTER,
	THIRD_CENTER,

	FRONT_BARRACK,
	FRONT_SUPPLY,
	FRONT_BUNKER,
	FRONT_ENGINE,

	FIRST_FACTORY,
	SECOND_FACTORY,
	THIRD_FACTORY,

	START_TURRET,
	HILL_TURRET,
	SIM1_TURRET,
	SIM2_TURRET,
	FRONT_TURRET,
	AROUND1_TURRET,
	AROUND2_TURRET,
	AROUND3_TURRET,
	AROUND4_TURRET,

	ACADEMY,
	ARMORY,
	ENGINEERING
};

class PositionMgr{

	bool buildMap[128][128];

	map<POS, TilePosition> tilePos;
	map<POS, Position> normalPos;

	vector<Position> marinePosition;

	PositionMgr(){};
	~PositionMgr(){};

public:
	static PositionMgr* getInstance();

	void init();
	void show();
	void draw();
	void run();

	void updateMarinePosition();

	void uploadPosition(POS key, TilePosition pos){ tilePos[key] = pos; };
	void uploadPosition(POS key, Position pos){ normalPos[key] = pos; };

	TilePosition getTilePosition(POS key){ return tilePos[key]; };
	Position getPosition(POS key){ return normalPos[key]; };

	TilePosition getProperPosition(UnitType type, bool upload = false);

	TilePosition getFactoryPosition();
	TilePosition getSupplyPosition();
	TilePosition getGasPosition(Territory* territory);

	void planUpload(UnitType type, TilePosition pos);
	void createBuilding(Unit unit);
	void destroyBuilding(Unit unit);
	bool canBuildHere(UnitType type, TilePosition pos);
};