#pragma once

#include <BWAPI.h>
#include <BWTA.h>
#include <math.h>

#include "Territory.h"

#define DRAW_MY    1
#define DRAW_ENEMY 0

#define DRAW_UNIT     0
#define DRAW_BUILDING 1

using namespace BWAPI;

Position tile2pos(TilePosition tile);
TilePosition pos2tile(Position pos);
void drawTerrainData();
void drawTerritory(Territory* territory);
void drawUnit(Unit u, bool my, bool building);
void drawUnitText(Unit u, string str);

float getDistance(TilePosition cmp1, TilePosition cmp2);
float getDistance(Position cmp1, Position cmp2);
float getDistance(BWTA::BaseLocation* cmp1, BWTA::BaseLocation* cmp2);

bool isAlmostReachedDest(Position dest, Position comparator, int xTerm, int yTerm);
bool isContainOneTerm(TilePosition cmp1, TilePosition cmp2, int term = 7);
bool isContainTwoTerm(TilePosition cmp1, TilePosition cmp2, int xTerm, int yTerm);
bool isSame(Unit a, Unit b);
int getProgress(Unit unit);

