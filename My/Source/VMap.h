#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <vector>
#include <stdio.h>
#include <list>
#include <map>

#define SCALE 4
#define TILE_SIZE 32 / SCALE
#define SIZE 128 * SCALE

using namespace std;
using namespace BWAPI;

class MoveNode{

	enum TYPE{
		BLOCK,
		ROAD,
	};

	Position pos;
	int point;
	TYPE type;

public:
	MoveNode(){ point = 0; type = ROAD; };
	MoveNode(int x, int y){ pos = Position(x * TILE_SIZE, y * TILE_SIZE); point = 100; type = ROAD; };

	Position& getPosition(){ return pos; };
	TYPE getType(){ return type; };
	void setPoint(int point){ this->point = point; if (point == -1){ type = BLOCK; } };
	void addPoint(int add){ this->point += add; };
	int getPoint(){ return point; };
};

class VMap
{
private:
	VMap();
	~VMap();

	MoveNode movePos[SIZE][SIZE];

public:
	static VMap* getInstance();

	void init();
	void show();
	void update();
	void draw();
};

