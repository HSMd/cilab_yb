#pragma once
#include <BWAPI.h>
#include <vector>
#include "MyUnit.h"

using namespace BWAPI;
using namespace std;

class Self{

	map<UnitType, vector <MyUnit*>> createUnits;
	map<UnitType, vector <MyUnit*>> createBuildings;

	map<UnitType, vector <MyUnit*>> completedUnits;
	map<UnitType, vector <MyUnit*>> completedBuildings;
	
	vector<MyUnit*> allUnits;
	vector<MyUnit*> allBuildings;

public:
	Self();
	~Self();

	void show(int startX, int startY);
	void update();
	void draw();
	void destroy(Unit unit);
	void morph(Unit unit);

	void addUnit(BWAPI::Unit unit);
	void addUnitImmediately(BWAPI::Unit unit);
	void addBuilding(BWAPI::Unit building);
	void addBuildingImmediately(BWAPI::Unit building);	

	int getUnitCount(UnitType type, bool immediately = false);
	int getBuildingCount(UnitType type, bool immediately = false);

	vector<MyUnit*>& getUnits(UnitType type);
	vector<MyUnit*>& getImmediatelyUnit(UnitType type);

	vector<MyUnit*>& getBuildings(UnitType type);
	vector<MyUnit*>& getImmediatelyBuilding(UnitType type);

	vector<MyUnit*>& getAllUnits();
	vector<MyUnit*>& getAllBuildings();

	vector<pair<Position, pair<int, int>>> myGroup;						//[GroupPosition, [Range, GroupLevel]]
	vector<pair<Position, pair<int, int>>> myGroundGroup;				//[GroupPosition, [Range, GroupLevel]]

	void delData(vector <MyUnit*>& data, Unit unit);
};