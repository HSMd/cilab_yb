#include "Commander.h"
#include "BWTA.h"
#include "Instance.h"

using namespace BWAPI;
using namespace Filter;
using namespace std;

MyUnit* t_scv = nullptr;
Unit test_enemy = nullptr;

Commander::Commander(){

}

Commander::~Commander(){

	BWTA::cleanMemory();
}

Commander* Commander::getInstance(){

	static Commander instance;
	return &instance;
}

// OnFunc
void Commander::init(){

	bw << "hello I'm MJ Bot!" << endl;

	bw << "The map is " << bw->mapName() << "!" << std::endl;
	bw->enableFlag(Flag::UserInput);

	// Speed 0 - 10
	bw->setLocalSpeed(10);
	bw->setCommandOptimizationLevel(0);

	BWTA::readMap();
	BWTA::analyze();
		
	info->init();
	mInfo->init();
	control->init();
	posMgr->init();
	buildMgr->init();
	scouting->init();

	guard->init();
	defense->init();

	//vmap->init();
}

void Commander::start(){

}

void Commander::show(){
	
	info->show();
	scheduler->show();
	state->show();
	scouting->show();
	buildMgr->show();
	//vmap->show();

	info->draw();
	
	//mInfo->draw();
	//control->draw();
	/*scheduler->draw();
	//defense->show();
	*/
	//scouting->draw();
}

void Commander::run(){

	// Test	
	
	/*
	if (test_enemy == nullptr){
		for (auto unit : bw->enemy()->getUnits()){
			if (unit->getType() == UnitTypes::Protoss_Probe){
				test_enemy = unit;
				break;
			}
		}
	}
	else{
		if (test_enemy->isVisible()){
			bw << "visible" << endl;
		}
		else{
			bw << "not-visible" << endl;
		}
	}
	
	*/
		
	/*
	info->update();

	if (t_scv == nullptr && !info->map->getMyTerritorys().empty()){
		t_scv = info->map->getMyTerritorys()[0]->getScvs(Territory::WORK::MINERAL).back();
	}
	
	bw << info->map->getMyTerritorys()[0]->getScore(t_scv) << endl;
	
	info->run();

	return;
	*/

	/*
	 * On Frame Main Loop
	 */

	
	// Update
	mInfo->onFrame();
	info->update();   // Player Information Update

	state->run();
	//guard->run();
	//defense->run();
	scouting->run();

	// Build
	
	buildMgr->run();  // run Builds		
	scheduler->run(); // run Schedule	
	info->run();      // work Scv

	control->onFrame();
}

void Commander::structureCreate(BWAPI::Unit unit){

	info->self->addBuildingImmediately(unit);

	if (bw->getFrameCount() < 50){
		auto cnter = info->self->getImmediatelyBuilding(UnitTypes::Terran_Command_Center).back();
		info->map->getMyTerritorys()[0]->setCmdCenter(cnter);
	}
	else{
		scheduler->buildingCreate(unit);
	}

	if (unit->getType().isBuilding()){
		posMgr->createBuilding(unit);
	}
}

void Commander::structureComplete(BWAPI::Unit unit){

	info->self->addBuilding(unit);
}

void Commander::unitCreate(BWAPI::Unit unit){

	if (unit->getPlayer() == BWAPI::Broodwar->enemy()) {
		info->create(unit);
	}
	else{
		info->self->addUnitImmediately(unit);
		control->onUnitCreate(unit);
	}
}

void Commander::unitComplete(BWAPI::Unit unit){

	if (unit->getType() == UnitTypes::Terran_SCV){
		info->map->addScv(unit);
	}

	info->self->addUnit(unit);	
	control->onUnitComplete(unit);
}

void Commander::destroy(BWAPI::Unit unit) {
	
	info->destroy(unit);
	control->onUnitDestroy(unit);

	if (unit->getType().isBuilding()){
		posMgr->destroyBuilding(unit);
	}
}

void Commander::unitMorph(BWAPI::Unit unit){
	info->morph(unit);
}