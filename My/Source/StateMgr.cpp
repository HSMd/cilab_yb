#include "StateMgr.h"

#include "Instance.h"
#include "Utility.h"

StateMgr::StateMgr(){
	enemyBuild = ENEMY_BUILD::NONE;
};

StateMgr::~StateMgr(){

};

StateMgr* StateMgr::getInstance(){

	static StateMgr instance;
	return &instance;
}

void StateMgr::init(){

}

void StateMgr::show(){

	int xpos = 50;
	switch (enemyBuild){
	case ENEMY_BUILD::NONE:
		bw->drawTextScreen(xpos, 100, "NONE");
		break;

	case ENEMY_BUILD::FORWARD_GATE:
		bw->drawTextScreen(xpos, 100, "FORWARD_GATE");
		break;

	case ENEMY_BUILD::ONE_GATE_CORE:
		bw->drawTextScreen(xpos, 100, "ONE_GATE_CORE");
		break;

	case ENEMY_BUILD::ONE_GATE_DOUBLE:
		bw->drawTextScreen(xpos, 100, "ONE_GATE_DOUBLE");
		break;

	case ENEMY_BUILD::PHOTO_RUSH:
		bw->drawTextScreen(xpos, 100, "PHOTO_RUSH");
		break;

	case ENEMY_BUILD::RAW_DOUBLE:
		bw->drawTextScreen(xpos, 100, "RAW_DOUBLE");
		break;

	case ENEMY_BUILD::SHUTTLE_DARK:
		bw->drawTextScreen(xpos, 100, "SHUTTLE_DARK");
		break;

	case ENEMY_BUILD::SHUTTLE_RIVER :
		bw->drawTextScreen(xpos, 100, "SHUTTLE_RIVER");
		break;

	case ENEMY_BUILD::TWO_GATE:
		bw->drawTextScreen(xpos, 100, "TWO_GATE");
		break;

	case ENEMY_BUILD::TWO_GATE_CORE:
		bw->drawTextScreen(xpos, 100, "TWO_GATE_CORE");
		break;

	case ENEMY_BUILD::ZEALOT_ALLIN:
		bw->drawTextScreen(xpos, 100, "ZEALOT_ALLIN");
		break;
	}
}

void StateMgr::draw(){

}

void StateMgr::run(){

	updateEnemyBuildByScout();
}

void StateMgr::updateEnemyBuildByScout(){

	if (scouting->isFind()){
		if (scouting->getSearchCount() == 1){
			if (!scouting->getUseData()){
				return;
			}

			enemyBuild = ENEMY_BUILD::ONE_GATE_CORE;

			/*
			if (info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway) == 0){
				if (2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Nexus)){
					enemyBuild = ENEMY_BUILD::RAW_DOUBLE;
				}
				else if (1 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Forge)){
					enemyBuild = ENEMY_BUILD::PHOTO_RUSH;
				}
				else{
					enemyBuild = ENEMY_BUILD::FORWARD_GATE;
				}
			}
			else if (1 == info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway)){
				if (2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Nexus)){
					enemyBuild = ENEMY_BUILD::ONE_GATE_DOUBLE;
				}
				if (1 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Cybernetics_Core)){
					enemyBuild = ENEMY_BUILD::ONE_GATE_CORE;
				}
			}
			else if (2 == info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway)){
				if (1 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Cybernetics_Core)){
					enemyBuild = ENEMY_BUILD::TWO_GATE_CORE;
				}
				else{
					enemyBuild = ENEMY_BUILD::TWO_GATE;
				}
			}
			else if (3 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway)){
				enemyBuild = ZEALOT_ALLIN;
			}
		}
		else{
			if (!scouting->getUseData()){
				return;
			}

			if (info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway) == 0){
				if (2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Nexus)){
					enemyBuild = ENEMY_BUILD::RAW_DOUBLE;
				}
				else if (1 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Forge)){
					enemyBuild = ENEMY_BUILD::PHOTO_RUSH;
				}
				else{
					enemyBuild = ENEMY_BUILD::FORWARD_GATE;
				}
			}
			else if (1 == info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway)){
				if (2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Nexus)){
					enemyBuild = ENEMY_BUILD::ONE_GATE_DOUBLE;
				}
				if (1 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Cybernetics_Core)){
					enemyBuild = ENEMY_BUILD::ONE_GATE_CORE;
				}
			}
			else if (2 == info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway)){
				if (1 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Cybernetics_Core)){
					enemyBuild = ENEMY_BUILD::TWO_GATE_CORE;
				}
				else{
					enemyBuild = ENEMY_BUILD::TWO_GATE;
				}
			}
			else if (3 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway)){
				enemyBuild = ZEALOT_ALLIN;
			}
		*/
		}
		
	}
}
