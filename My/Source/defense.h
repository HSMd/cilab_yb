#pragma once
#include <BWAPI.h>
#include <BWTA.h>

#include "MyUnit.h"
#include "Territory.h"

#include <map>

using namespace std;

class Defense
{
	public:
		static Defense* getInstance();
		void init();
		void show();
		void run();

		void run2();
		void cleanGarbage();

		//defense func
		void probeSearch();
		void pylonDefeat();
		void MarineHold();

		void blockdefense();
		void IdleMarine();
		void getUnits(UnitType type, int count = -1);

		//////////////////
		void scvRepair();				
		bool ScanControl(TilePosition tp);

	private:
		Defense();
		~Defense();
		
		vector<Position> marinePosition;

		Unitset myunit;
		Unit probe;
		MyUnit* repairtarget;
		
		map<TilePosition, bool> TurretPos;
		

		//�� ���� ���� ����
		map<UnitType, vector<MyUnit*>> guardUnits;

		vector<MyUnit*>SCV;
		vector<MyUnit*>Scan;
		vector<MyUnit*>Marine;

		vector<TilePosition>Turret;

		bool FrontSight;
};