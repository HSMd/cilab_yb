#include "Work.h"
#include "Instance.h"

bool BuildWork::run(){

	int cmp = 0;

	if (comparison.isBuilding()){
		if (immediatly){
			cmp = info->self->getImmediatelyBuilding(comparison).size();
		}
		else{
			cmp = info->self->getBuildings(comparison).size();
		}
	}
	else{
		if (immediatly){
			cmp = info->self->getImmediatelyUnit(comparison).size();
		}
		else{
			cmp = info->self->getUnits(comparison).size();
		}
	}

	return cmpCount <= cmp;
}