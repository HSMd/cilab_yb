#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <vector>
#include <stdio.h>
#include <list>
#include <map>

#define SCALE 4
#define TERM 32 / SCALE
#define IMSIZE 128 * SCALE
#define MMSIZE 128
#define MMTERM 32

using namespace std;
using namespace BWAPI;



struct Node {
	BWTA::Chokepoint* choke;
	BWTA::Region* region;
	int num;
	bool regionCheck;

	Position getCenter() {
		if (regionCheck) {
			return region->getCenter();
		}
		else {
			return choke->getCenter();
		}
	}

	bool isRegion() {
		return regionCheck;
	}

	int getNumber() {
		return num;
	}

	Node(BWTA::Region* r, int n) {
		num = n;
		regionCheck = true;
		region = r;
	}

	Node(BWTA::Chokepoint* c, int n) {
		num = n;
		regionCheck = false;
		choke = c;
	}
};

class MapInformation
{
private:
	MapInformation();
	~MapInformation();
	static MapInformation *s_MapInformation;

	Position IMpos[IMSIZE][IMSIZE];
	int GIM[IMSIZE][IMSIZE]; // 걸을수 있는곳
	int GIMBackup[IMSIZE][IMSIZE];
	int AIM[IMSIZE][IMSIZE]; // 공중
	int AIMBackup[IMSIZE][IMSIZE];
	int SIM[IMSIZE][IMSIZE]; // 
	int SIMBackup[IMSIZE][IMSIZE];
	
	int nGraph[51][51];
	int nsGraph[51][51];
	int nGraphBU[51][51];
	int nsGraphBU[51][51];
	map <int, Node*> index2Node;
	map <BWTA::Region*, vector<BWTA::Chokepoint*>> injupChokesList;
	bool isGumNodes(Node*, Node*);
	int tni = 0;

public:
	static MapInformation * create();
	static MapInformation * getInstance();
	void init();

	void onFrame();
	void draw();
	void onUnitCreate(Unit);
	void onUnitDestroy(Unit);
	void onUnitComplete(Unit);

	Position getNextAvoidMovePos(Position currentPos, Position targetPos);
	TilePosition getNextAvoidMoveTilePos(TilePosition currentPos, TilePosition targetPos);
	Position getNextMovePos(Position currentPos, Position targetPos);
	TilePosition getNextMoveTilePos(TilePosition currentPos, TilePosition targetPos);
	Position getAirNextAvoidMovePos(Position currentPos, Position targetPos);
	TilePosition getAirNextAvoidMoveTilePos(TilePosition currentPos, TilePosition targetPos);
	Position getAirNextMovePos(Position currentPos, Position targetPos);
	TilePosition getAirNextMoveTilePos(TilePosition currentPos, TilePosition targetPos);

	void updateIM();
	float getDistance(Position p1, Position p2);
	int Floyd(Position c, Position t, bool strong);
	Position getFloydPosition(Position c, Position t, bool strong);
	int getGIM(Position p);
};

