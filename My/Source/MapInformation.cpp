#include "MapInformation.h"
#include "Instance.h"
#include "Utility.h"

int cx[4] = { 10, 10, -10, -10 };
int cy[4] = { 10, -10, -10, 10 };

MapInformation* MapInformation::s_MapInformation;
MapInformation::MapInformation() {

}

MapInformation::~MapInformation() {

}

MapInformation* MapInformation::create() {
	if (s_MapInformation) {
		return s_MapInformation;
	}

	s_MapInformation = new MapInformation();
	return s_MapInformation;
}

MapInformation* MapInformation::getInstance() {

	static MapInformation instance;
	return &instance;
}

void MapInformation::init() {

	index2Node.clear();

	for (int x = 0; x < IMSIZE; x++) {
		for (int y = 0; y < IMSIZE; y++) {
			IMpos[y][x] = Position(x * TERM, y * TERM);
			GIM[y][x] = 0;
			GIMBackup[y][x] = 0;
			AIM[y][x] = 0;
			AIMBackup[y][x] = 0;

			if (!BWAPI::Broodwar->isWalkable((WalkPosition)IMpos[y][x])) {
				GIM[y][x] = -1;
				GIMBackup[y][x] = -1;
			}
		}
	}

	//벽 근처 페널티
	for (int x = 0; x < IMSIZE; x++) {
		for (int y = 0; y < IMSIZE; y++) {

			bool check = false;
			if (GIM[y][x] < 0) continue;

			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {

					int _x = x + i;
					int _y = y + j;
					if (_x < 0 || _y < 0 || _x > IMSIZE - 1 || _y > IMSIZE - 1) {
						continue;
					}

					if (GIM[_y][_x] == -1) {
						check = true;
					}
				}
			}
			if (check) {
				GIM[y][x] = 16;
				GIMBackup[y][x] = 16;
			}
		}
	}

	for (int x = 0; x < IMSIZE; x++) {
		for (int y = 0; y < IMSIZE; y++) {
			bool check = false;
			if (GIM[y][x] < 0 || GIM[y][x] == 16) continue;
			for (int i = -2; i <= 2; i++) {
				for (int j = -2; j <= 2; j++) {
					int _x = x + i;
					int _y = y + j;
					if (_x < 0 || _y < 0 || _x > IMSIZE - 1 || _y > IMSIZE - 1) {
						continue;
					}

					if (GIM[_y][_x] == -1) {
						check = true;
					}
				}
			}
			if (check) {
				GIM[y][x] = 8;
				GIMBackup[y][x] = 8;
			}
		}
	}

	// 노드 생성
	tni = 0;
	index2Node.clear();
	for (auto q = BWTA::getRegions().begin(); q != BWTA::getRegions().end(); q++) {
		index2Node.insert(map<int, Node*>::value_type(tni, (new Node(*q, tni))));
		tni++;
	}
	for (auto q = BWTA::getChokepoints().begin(); q != BWTA::getChokepoints().end(); q++) {
		index2Node.insert(map<int, Node*>::value_type(tni, (new Node(*q, tni))));
		tni++;
	}
	
	injupChokesList.clear();
	for (auto q = index2Node.begin(); q != index2Node.end(); q++) {
		if (!(*q).second->isRegion()) {
			Position p = (*q).second->getCenter();
			for (int x = 0; x < 4; x++) {
				Position n = p;
				n.x += cx[x];
				n.y += cy[x];
				BWAPI::Broodwar->drawCircleMap(n, 4, Colors::Red, true);
				for (auto w = index2Node.begin(); w != index2Node.end(); w++) {
					if ((*w).second->isRegion()) {
						if (BWTA::getRegion(n) == (*w).second->region) {
							injupChokesList[(*w).second->region].push_back((*q).second->choke);
						}
					}
				}
			}
		}
	}

	//그래프 생성, air는 나중에 추가할 것(고작 배슬을 위해 혹은 드랍십이나)
	for (int x = 0; x < tni; x++) {
		Node* n = index2Node[x];
		for (int y = 0; y < tni; y++) {
			if (index2Node[x] == NULL || index2Node[y] == NULL) {
				//printf("Null Pointer excetion where CombatInfo setting Node\n");
				continue;
			}
			if (x == y) {
				nGraph[y][x] = -2;
				nsGraph[y][x] = -2;
				nGraphBU[y][x] = -2;
				nsGraphBU[y][x] = -2;

			}
			else if (isGumNodes(index2Node[x], index2Node[y])) {
				float dist = BWTA::getGroundDistance((TilePosition)(index2Node[x]->getCenter()), (TilePosition)(index2Node[y]->getCenter()));
				nGraph[y][x] = (int)dist;
				nsGraph[y][x] = (int)dist;
				nGraphBU[y][x] = (int)dist;
				nsGraphBU[y][x] = (int)dist;
			}
			else {
				nGraph[y][x] = 99999;
				nsGraph[y][x] = 99999;
				nGraphBU[y][x] = 99999;
				nsGraphBU[y][x] = 99999;
			}
		}
	}
	

}

void MapInformation::onFrame() {
	
	updateIM();
	//printf("enemyAllUnit size %d\n", info->enemy->getAllUnits().size());
}

void MapInformation::draw() {

	int sx = 0;
	int sy = 0;
	if (info->map->getMyDirection() == 1) {
		sx = IMSIZE / 4 * 3;
	}
	else if (info->map->getMyDirection() == 2) {
		sy = IMSIZE / 4 * 3;
	}
	else if (info->map->getMyDirection() == 3) {
		sx = IMSIZE / 4 * 3;
		sy = IMSIZE / 4 * 3;
	}
	

	for (int x = sx; x < IMSIZE/4 + sx; x += SCALE/4) {
		for (int y = sy; y < IMSIZE/4 + sy; y += SCALE/4) {
			int drawX = IMpos[y][x].x ;// SCALE;
			int drawY = IMpos[y][x].y ;// SCALE;
			Position drawPos = Position(drawX, drawY);
			if (GIM[y][x] == -1) {		// 못가는 지형
				BWAPI::Broodwar->drawCircleMap(drawPos, 1, Colors::Red, true);
			} 
			else if (GIM[y][x] == -2) {		// 유닛이 있음
				BWAPI::Broodwar->drawCircleMap(drawPos, 1, Colors::Black, true);
			}
			else if (GIM[y][x] == -3) {		// 자원이 있음
				BWAPI::Broodwar->drawCircleMap(drawPos, 1, Colors::Cyan, true);
			}
			else if (GIM[y][x] >= 40) {		// 
				BWAPI::Broodwar->drawCircleMap(drawPos, 1, Colors::Red, true);
			}
			else if (GIM[y][x] >= 32) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 1, Colors::Orange, true);
			}
			else if (GIM[y][x] >= 24) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 1, Colors::Yellow, true);
			}
			else if (GIM[y][x] >= 16) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 1, Colors::Green, true);
			}
			else if (GIM[y][x] >= 8) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 1, Colors::Cyan, true);
			}
			else  {
				BWAPI::Broodwar->drawCircleMap(drawPos, 1, Colors::Blue, true);
			}
		}
	}

	/*for (int x = 0; x < MMSIZE; x++) {
		for (int y = 0; y < MMSIZE; y++) {
			Position drawPos = Position(x * 32, y * 32);
			if (MMAP[y][x] == -3) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Red, true);
			}
			else if (MMAP[y][x] == -2) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Grey, true);
			}
			else if (MMAP[y][x] == -1) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2 , Colors::Purple, true);
			}
			else if (MMAP[y][x] == 0) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Green, true);
			}
			else if (MMAP[y][x] == 1) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Blue, true);
			}
			else if (MMAP[y][x] == 2) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Blue, true);
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Blue, false);
			}
			else if (MMAP[y][x] == 3) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Orange, false);
			}
			else if (MMAP[y][x] == 4) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Orange, true);
			}
			else if (MMAP[y][x] == 5) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Yellow, true);
			}
			else if (MMAP[y][x] == 6) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Yellow, true);
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Yellow, false);
			}
		}
	}*/

	/*for (int x = 0; x < MMSIZE; x++) {
		for (int y = 0; y < MMSIZE/2; y++) {
			Position drawPos = Position(x * 32, y * 32);
			if (TurretMAP[y][x] == -3) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Red, false);
			}
			else if (TurretMAP[y][x] == -2) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Purple, false);
			}
			else if (TurretMAP[y][x] == -1) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Green, false);
			}
			else if (TurretMAP[y][x] == 0) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Grey, false);
			}
			else if (TurretMAP[y][x] == 1) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Orange, false);
			}
			else if (TurretMAP[y][x] == 2) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Blue, false);
			}
			else if (TurretMAP[y][x] == 3) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Orange, false);
			}
			else if (TurretMAP[y][x] == 4) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Orange, false);
			}
			else if (TurretMAP[y][x] == 5) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Yellow, false);
			}
			else if (TurretMAP[y][x] == 6) {
				BWAPI::Broodwar->drawCircleMap(drawPos, 2, Colors::Yellow, false);
			}
		}
	}*/
	

	/*for (auto q = injupChokesList.begin(); q != injupChokesList.end(); q++) {
		for (auto p = (*q).second.begin(); p != (*q).second.end(); p++) {
			BWAPI::Broodwar->drawLineMap((*q).first->getCenter(), (*p)->getCenter(), Colors::White);
		}
	}*/

}

void MapInformation::updateIM() {

	memcpy(GIM, GIMBackup, sizeof(GIMBackup));
	memcpy(AIM, AIMBackup, sizeof(AIMBackup));

	for (auto q = info->enemy->getAllUnits().begin(); q != info->enemy->getAllUnits().end(); q++) {
		if ((*q)->exists() == false) continue;
		if ((*q) == NULL) continue;
		if ((*q)->isCompleted() == false) continue;

		Unit unit = (*q);
		Position p = unit->getPosition();

		float eRange = (*q)->getType().groundWeapon().maxRange() + 140;
		if ((*q)->getType() == UnitTypes::Protoss_Probe)
			eRange = (*q)->getType().groundWeapon().maxRange() * 2;
		
		int sx = (p.x - eRange) / 8;
		int sy = (p.y - eRange) / 8;
		int ex = (p.x + eRange) / 8;
		int ey = (p.y + eRange) / 8;
				 

		//printf("eRange %d\tUnitTypes %s\n", eRange, (*q).second.first.c_str());
		if (unit->getType() != UnitTypes::Protoss_Arbiter) {
			for (int i = sx; i <= ex; i++) {
				for (int j = sy; j <= ey; j++) {
					if (i < 0 || j < 0 || i > IMSIZE - 1 || j > IMSIZE - 1) {
						continue;
					}
					if (GIM[j][i] < 0) continue;
					float dist = getDistance(Position(i*TERM, j*TERM), p);
					if (dist < eRange) {
						GIM[j][i] += (1.0 - (dist / eRange)) * (float)(*q)->getType().groundWeapon().damageAmount() * 2.0f;
					}
					//printf("erange : %d\tdist : %d\t (eRange-dist)/eRange*100 : %d\n", eRange, dist, (eRange - dist) / eRange * 100);
				}
			}
		}

		if (unit->getType() == UnitTypes::Protoss_Dragoon || unit->getType() == UnitTypes::Protoss_Photon_Cannon || unit->getType() == UnitTypes::Protoss_Scout || unit->getType() == UnitTypes::Protoss_Corsair) {
			for (int i = sx; i <= ex; i++) {
				for (int j = sy; j <= ey; j++) {
					if (i < 0 || j < 0 || i > IMSIZE - 1 || j > IMSIZE - 1) {
						continue;
					}
					if (AIM[j][i] < 0) continue;
					float dist = getDistance(Position(i*TERM, j*TERM), p);
					if (dist < eRange) {
						AIM[j][i] += (1.0 - (dist / eRange)) * (float)(*q)->getType().groundWeapon().damageAmount() * 2.0f;
					}
					//printf("erange : %d\tdist : %d\t (eRange-dist)/eRange*100 : %d\n", eRange, dist, (eRange - dist) / eRange * 100);
				}
			}
		}
	}

	for (auto q = info->enemy->getAllBuildings().begin(); q != info->enemy->getAllBuildings().end(); q++) {
		if ((*q)->exists() == false) continue;
		if ((*q) == NULL) continue;
		Unit unit = (*q);
		Position p = unit->getPosition();
		p.x /= 8;
		p.y /= 8;
		int width = unit->getType().width() / 16;
		int height = unit->getType().height() / 16;

		Position leftUp = Position(p.x - width, p.y - height);
		Position rightBottom = Position(p.x + width, p.y + height);

		int range = unit->getType().width() / 8.f / 2 * 2;
		float rr = unit->getType().width() / 2.f * 1.25f;

		for (int i = p.x - (range); i <= p.x + (range); i++) {
			for (int j = p.y - (range); j <= p.y + (range); j++) {
				if (i < 0 || j < 0 || i >IMSIZE - 1 || j > IMSIZE - 1) {
					continue;
				}
				float dist = getDistance(Position(8 * i, 8 * j), unit->getPosition());
				//printf("dist %.2f rr %.2f\n", dist, rr);
				if (dist < rr) {
					GIM[j][i] += 15.f * (rr - dist);
				}
			}
		}

		for (int i = leftUp.x; i <= rightBottom.x; i++) {
			for (int j = leftUp.y; j <= rightBottom.y; j++) {
				if (i < 0 || j < 0 || i >IMSIZE - 1 || j > IMSIZE - 1) {
					continue;
				}
				GIM[j][i] = -2;
			}
		}
	}

	for (auto q = info->self->getAllUnits().begin(); q != info->self->getAllUnits().end(); q++) {
		if ((*q)->self->exists() == false) continue;
		if ((*q) == NULL) continue;
		if ((*q)->self->isCompleted() == false) continue;
		Unit unit = (*q)->self;
		Position p = unit->getPosition();

		float w = unit->getType().width() / 2;
		float h = unit->getType().height() / 2;

		int sx = (p.x - w) / 8;
		int sy = (p.y - h) / 8;
		int ex = (p.x + w) / 8;
		int ey = (p.y + h) / 8;


		//printf("eRange %d\tUnitTypes %s\n", eRange, (*q).second.first.c_str());
		for (int i = sx; i <= ex; i++) {
			for (int j = sy; j <= ey; j++) {
				if (i < 0 || j < 0 || i > IMSIZE - 1 || j > IMSIZE - 1) {
					continue;
				}
				GIM[j][i] = -2;
				//printf("erange : %d\tdist : %d\t (eRange-dist)/eRange*100 : %d\n", eRange, dist, (eRange - dist) / eRange * 100);
			}
		}

	}
	
	for (auto q = info->self->getAllBuildings().begin(); q != info->self->getAllBuildings().end(); q++) {
		if ((*q)->self->exists() == false) continue;
		if ((*q) == NULL) continue;
		Unit unit = (*q)->self;
		Position p = unit->getPosition();
		p.x /= 8;
		p.y /= 8;
		int width = unit->getType().width() / 16;
		int height = unit->getType().height() / 16;

		Position leftUp = Position(p.x - width, p.y - height);
		Position rightBottom = Position(p.x + width, p.y + height);

		for (int i = leftUp.x; i <= rightBottom.x; i++) {
			for (int j = leftUp.y; j <= rightBottom.y; j++) {
				if (i < 0 || j < 0 || i >IMSIZE - 1 || j > IMSIZE - 1) {
					continue;
				}
				GIM[j][i] = -2;
			}
		}
	}	

	for (auto q = BWAPI::Broodwar->getMinerals().begin(); q != BWAPI::Broodwar->getMinerals().end(); q++) {
		if ((*q)->exists() == false) continue;
		if ((*q) == NULL) continue;
		Unit unit = *q;
		Position p = unit->getPosition();
		p.x /= 8;
		p.y /= 8;
		int width = unit->getType().width() / 16;
		int height = unit->getType().height() / 16;

		Position leftUp = Position(p.x - width, p.y - height);
		Position rightBottom = Position(p.x + width, p.y + height);

		int range = unit->getType().width() / 8.f / 2 * 2;
		float rr = unit->getType().width() / 2.f * 1.5f;

		for (int i = p.x - (range); i <= p.x + (range); i++) {
			for (int j = p.y - (range); j <= p.y + (range); j++) {
				if (i < 0 || j < 0 || i >IMSIZE - 1 || j > IMSIZE - 1) {
					continue;
				}
				float dist = getDistance(Position(8 * i, 8 * j), unit->getPosition());
				//printf("dist %.2f rr %.2f\n", dist, rr);
				if (dist < rr) {
					GIM[j][i] += 15.f * (rr - dist);
				}
			}
		}

		for (int i = leftUp.x; i <= rightBottom.x; i++) {
			for (int j = leftUp.y; j <= rightBottom.y; j++) {
				if (i < 0 || j < 0 || i >IMSIZE - 1 || j > IMSIZE - 1) {
					continue;
				}
				GIM[j][i] = -3;
			}
		}
	}

	for (auto q = BWAPI::Broodwar->getGeysers().begin(); q != BWAPI::Broodwar->getGeysers().end(); q++) {
		if ((*q)->exists() == false) continue;
		if ((*q) == NULL) continue;
		Unit unit = *q;
		Position p = unit->getPosition();
		p.x /= 8;
		p.y /= 8;
		int width = unit->getType().width() / 16;
		int height = unit->getType().height() / 16;

		Position leftUp = Position(p.x - width, p.y - height);
		Position rightBottom = Position(p.x + width, p.y + height);

		int range = unit->getType().width() / 8.f / 2 * 2;
		float rr = unit->getType().width() / 2.f * 1.2f;

		for (int i = p.x - (range); i <= p.x + (range); i++) {
			for (int j = p.y - (range); j <= p.y + (range); j++) {
				if (i < 0 || j < 0 || i >IMSIZE - 1 || j > IMSIZE - 1) {
					continue;
				}
				float dist = getDistance(Position(8 * i, 8 * j), unit->getPosition());
				//printf("dist %.2f rr %.2f\n", dist, rr);
				if (dist < rr) {
					GIM[j][i] += 15.f * (rr - dist);
				}
			}
		}

		for (int i = leftUp.x; i <= rightBottom.x; i++) {
			for (int j = leftUp.y; j <= rightBottom.y; j++) {
				if (i < 0 || j < 0 || i >IMSIZE - 1 || j > IMSIZE - 1) {
					continue;
				}
				GIM[j][i] = -3;
			}
		}
	}

	// graph 최신화
	memcpy(nGraph, nGraphBU, sizeof(nGraphBU));

	for (auto q = info->enemy->getAllUnits().begin(); q != info->enemy->getAllUnits().end(); q++) {
		for (int x = 0; x < tni; x++) {
			float dist = getDistance((*q)->getPosition(), index2Node[x]->getCenter());
			int damage = (*q)->getType().groundWeapon().damageAmount();
			if (damage == 0) {
				(*q)->getType().airWeapon().damageAmount();
			}
			if (dist < 1000 && dist > 1) {
				for (int y = 0; y < tni; y++) {
					if (nGraph[y][x] > 0 || nGraph[y][x] < 10000) {

						nGraph[y][x] += damage * 500000 / dist;
						nGraph[x][y] += damage * 500000 / dist;
					}
				}
			}
			else if (dist >= 0) {
				for (int y = 0; y < tni; y++) {
					if (nGraph[y][x] > 0 || nGraph[y][x] < 10000) {
						nGraph[y][x] += damage * 500000;
						nGraph[x][y] += damage * 500000;
					}
				}
			}
		}
	}

}

void MapInformation::onUnitCreate(Unit unit) {

}

void MapInformation::onUnitDestroy(Unit unit) {

}

void MapInformation::onUnitComplete(Unit unit) {

}

float MapInformation::getDistance(Position p1, Position p2) {
	float ret;
	float x = p1.x, y = p1.y, x2 = p2.x, y2 = p2.y;

	ret = (x - x2)*(x - x2) + (y - y2)*(y - y2);
	ret = sqrt(ret);

	return ret;
}

bool MapInformation::isGumNodes(Node* n1, Node* n2) {
	if (n1 == NULL || n2 == NULL) return false;
	if (n1->isRegion() && n2->isRegion()) {
		return false;
	}
	else if (!n1->isRegion() && !n2->isRegion()) {
		//printf("index2Node Size = %d\n", index2Node.size());
		for (auto q = index2Node.begin(); q != index2Node.end(); q++) {
			if ((*q).second == NULL) {
				//printf("이게왜 널이야");
				return false;
			}
			if ((*q).second->isRegion()) {
				bool check1 = false;
				bool check2 = false;
				for (auto r = injupChokesList[(*q).second->region].begin(); r != injupChokesList[(*q).second->region].end(); r++) {
					if ((*r) == n1->choke) check1 = true;
					if ((*r) == n2->choke) check2 = true;
				}
				if (check1 && check2) return true;
			}
		}
	}
	else {
		BWTA::Region* r; BWTA::Chokepoint* c;
		if (n1->isRegion()) {
			r = n1->region;
			c = n2->choke;
		}
		else {
			r = n2->region;
			c = n1->choke;
		}
		for (auto q = injupChokesList[r].begin(); q != injupChokesList[r].end(); q++) {
			if ((*q) == c) {
				return true;
			}
		}
	}
	return false;
}

int MapInformation::Floyd(Position currentPos, Position targetPos, bool strong) {
	int via[102][102] = { 0 };
	int d[102][102] = { 0 };
	int graph[51][51];
	int currentIndex = 0;
	int targetIndex = 0;

	if (strong) {
		memcpy(graph, nsGraph, sizeof(nsGraph));
	}
	else {
		memcpy(graph, nGraph, sizeof(nGraph));
	}


	float minC = 99999;
	float minT = 99999;
	for (int x = 0; x < tni; x++) {
		float distC = getDistance(currentPos, index2Node[x]->getCenter());
		float distT = getDistance(targetPos, index2Node[x]->getCenter());
		if (distC < minC) {
			minC = distC;
			currentIndex = x;
		}
		if (distT < minT) {
			minT = distT;
			targetIndex = x;
		}
	}

	for (int x = 0; x < tni; x++) {
		for (int y = 0; y < tni; y++) {
			d[y][x] = graph[y][x];
			via[y][x] = -1;
		}
	}
	for (int k = 0; k < tni; k++) {
		for (int x = 0; x < tni; x++) {
			for (int y = 0; y < tni; y++) {
				if (d[y][x] < 0) continue;
				if (d[y][k] < 0 || d[k][x] < 0) continue;
				if (d[y][x] > d[y][k] + d[k][x]) {
					d[y][x] = d[y][k] + d[k][x];
					via[y][x] = k;
				}
			}
		}
	}
	if (targetIndex == via[currentIndex][targetIndex]) {
		int minIndex = 999999;
		int min = -1;
		for (int x = 0; x < tni; x++) {
			if (graph[currentIndex][x] < minIndex && graph[currentIndex][x] > 0) {
				minIndex = graph[currentIndex][x];
				min = x;
			}
		}
		return min;
	}
	else {
		while (true) {
			if (isGumNodes(index2Node[currentIndex], index2Node[targetIndex])) {
				return targetIndex;
			}
			else {
				if (via[currentIndex][targetIndex] < 0) return targetIndex;
				targetIndex = via[currentIndex][targetIndex];
			}
		}
	}
}

Position MapInformation::getFloydPosition(Position c, Position t, bool strong) {
	Position ret;
	int n = Floyd(c, t, strong);
	ret = index2Node[n]->getCenter();
	float dist = getDistance(t, ret);
	if (BWTA::getRegion(ret) == BWTA::getRegion(t) || dist < 320) {
		return t;
	}
	return ret;
}

Position MapInformation::getNextAvoidMovePos(Position currentPos, Position targetPos) {
	Position ret = currentPos;
	int minValue = 9999;
	int ux = currentPos.x / 8;
	int uy = currentPos.y / 8;
	int nx = 0;
	int ny = 0;
	// 3x3 -> 3x3 
	int range = 4;
	//for (int x = -range; x <= range; x++) {
	//	for (int y = -range; y <= range; y++) {
	//		int _x = ux + x;
	//		int _y = uy + y;
	//		if (_y < 0 || _x < 0 || _x > IMSIZE-1 || _y > IMSIZE-1) continue;
	//		if (GIM[_y][_x] < minValue && GIM[_y][_x] >= 0) {
	//			minValue = GIM[_y][_x];
	//			nx = _x;
	//			ny = _y;
	//		}
	//	}
	//}
	minValue = 9999;
	for (int x = -range + nx; x <= range + nx; x++) {
		for (int y = -range + ny; y <= range + ny; y++) {
			int _x = ux + x;
			int _y = uy + y;
			if (_y < 0 || _x < 0 || _x > IMSIZE - 1 || _y > IMSIZE - 1) continue;
			if (GIM[_y][_x] < minValue && GIM[_y][_x] >= 0) {
				minValue = GIM[_y][_x];
				nx = _x;
				ny = _y;
			}
		}
	}
	
	float minDisttoTarget = 99999;

	for (int x = -range*2; x <= range*2; x++) {
		for (int y = -range*2; y <= range*2; y++) {
			int _x = x + ux;
			int _y = y + uy;
			if (_y < 0 || _x < 0 || _x > IMSIZE-1 || _y > IMSIZE-1) continue;
			if (GIM[_y][_x] == minValue) {
				float dist = getDistance(Position(_x * TERM, _y * TERM), targetPos);
				if (dist < minDisttoTarget) {
					minDisttoTarget = dist;
					ret = Position(_x * TERM, _y * TERM);
				}
			}
		}
	}
	
	return ret;
}

TilePosition MapInformation::getNextAvoidMoveTilePos(TilePosition currentPos, TilePosition targetPos) {
	return (TilePosition)getNextAvoidMovePos((Position)currentPos, (Position)targetPos);
}

Position MapInformation::getNextMovePos(Position currentPos, Position targetPos) {
	return getFloydPosition(currentPos, targetPos, true);
}

TilePosition MapInformation::getNextMoveTilePos(TilePosition currentPos, TilePosition targetPos) {
	return (TilePosition)getNextMovePos((Position)currentPos, (Position)targetPos);
}


Position MapInformation::getAirNextAvoidMovePos(Position currentPos, Position targetPos) {
	Position ret = currentPos;
	int minValue = 9999;
	int ux = currentPos.x / 8;
	int uy = currentPos.y / 8;
	int range = 8;
	for (int x = -8; x <= 8; x++) {
		for (int y = -8; y <= 8; y++) {
			int _x = ux + x;
			int _y = uy + y;
			if (_y < 0 || _x < 0 || _x > IMSIZE - 1 || _y > IMSIZE - 1) continue;
			if (AIM[_y][_x] < minValue && AIM[_y][_x] >= 0) {
				minValue = AIM[_y][_x];
			}
		}
	}

	float minDisttoTarget = 99999;

	for (int x = -8; x <= 8; x++) {
		for (int y = -8; y <= 8; y++) {
			int _x = x + ux;
			int _y = y + uy;
			if (_y < 0 || _x < 0 || _x > IMSIZE - 1 || _y > IMSIZE - 1) continue;
			if (AIM[_y][_x] == minValue) {
				float dist = getDistance(Position(_x * TERM, _y * TERM), targetPos);
				if (dist < minDisttoTarget) {
					minDisttoTarget = dist;
					ret = Position(_x * TERM, _y * TERM);
				}
			}
		}
	}

	return ret;
}

TilePosition MapInformation::getAirNextAvoidMoveTilePos(TilePosition currentPos, TilePosition targetPos) {
	return (TilePosition)getAirNextAvoidMovePos((Position)currentPos, (Position)targetPos);
}

Position MapInformation::getAirNextMovePos(Position currentPos, Position targetPos) {
	return targetPos;
}

TilePosition MapInformation::getAirNextMoveTilePos(TilePosition currentPos, TilePosition targetPos) {
	return (TilePosition)getAirNextMovePos((Position)currentPos, (Position)targetPos);
}

int MapInformation::getGIM(Position p) {
	int px = p.x / 8;
	int py = p.y / 8;

	return GIM[py][px];
}