#include "LastBuild.h"
#include "Instance.h"
#include "Work.h"
#include "Utility.h"

void LastBuild::init(){

	auto startBase = info->map->getMyStartBase();	
	auto front = info->map->getCandidateLocation();
	Territory* territory = new Territory(front);
	info->map->addMyTerritory(territory);
	progressBuild = PROGRESS_BUILD::NONE;

	performer = nullptr;
	frameCount = 0;

	int s1 = 8;
	int s2 = 14;
	int gas = 12;
	int b1 = 10;

	/*
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s1, false, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY)));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, b1, true, UnitTypes::Terran_Barracks, posMgr->getTilePosition(POS::SIM1_BARRACK), nullptr, 80));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, gas, true, UnitTypes::Terran_Refinery));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, s2, true, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::SIM1_SUPPLY2)));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Barracks, 1, true, UnitTypes::Terran_Marine));
	*/

	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 5, true, UnitTypes::Terran_Barracks));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 5, true, UnitTypes::Terran_Factory));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 5, true, UnitTypes::Terran_Engineering_Bay));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 5, true, UnitTypes::Terran_Marine));
	builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 5, true, UnitTypes::Terran_Engineering_Bay));
}

void LastBuild::show(){

	//auto chk = BWTA::getNearestChokepoint(info->map->getMyStartBase()->getTilePosition());
	//BWAPI::Broodwar->drawCircleMap(chk->getCenter(), 400, Colors::Yellow, false);

	auto pos = info->map->getMyTerritorys().back()->getPosition();
	
	switch (info->map->getMyDirection()){
	case 0:
		pos += Position(-60, 0);
		BWAPI::Broodwar->drawCircleMap(pos, 320, Colors::Yellow, false);

		break;

	case 1:
		pos += Position(-40, 0);
		BWAPI::Broodwar->drawCircleMap(pos, 320, Colors::Yellow, false);

		break;

	case 2:
		pos += Position(-20, 50);
		BWAPI::Broodwar->drawCircleMap(pos, 320, Colors::Yellow, false);

		break;

	case 3:

		pos += Position(60, 0);
		BWAPI::Broodwar->drawCircleMap(pos, 320, Colors::Yellow, false);
		break;
	}
	
}

void LastBuild::draw(){

}

void LastBuild::buildPush(){

	if (!builds.empty()){
		return;
	}

	auto enemyBuild = state->getEnemyBuild();

	if (progressBuild == PROGRESS_BUILD::NONE){
		if (enemyBuild == NONE){ // 1��ġ �������� �ƹ��� �ǽɰ��� ������� �ȿ°� ( EX. ���䷯��, ��������Ʈ �� )
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 15, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Marine, 2));
			progressBuild = PROGRESS_BUILD::FIRST_FAC;
		}
	}
	else if (progressBuild == PROGRESS_BUILD::FIRST_FAC){

		if (250 <= scheduler->getMineral()){
			// ����ġ ����
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Command_Center, posMgr->getTilePosition(POS::SECOND_CENTER), nullptr, 100));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 1, true, UnitTypes::Terran_Machine_Shop));
			progressBuild = SECOND_CENTER;
		}
		else{
			// ����ġ ����
			//enemyBuild == ENEMY_BUILD::TWO_GATE || 
			if (enemyBuild == ENEMY_BUILD::ZEALOT_ALLIN || enemyBuild == ENEMY_BUILD::FORWARD_GATE ||
				enemyBuild == ENEMY_BUILD::PHOTO_RUSH || (enemyBuild == ENEMY_BUILD::RAW_DOUBLE && !info->map->isDiagonal())){

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Machine_Shop));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Machine_Shop, 1, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Machine_Shop, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -30));
				builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, TechTypes::Spider_Mines));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true, UnitTypes::Terran_Machine_Shop));

				flags[AUTO_TANK] = true;

				progressBuild = TWO_FACTORY;
			}
		}
	}
	else if (progressBuild == PROGRESS_BUILD::SECOND_CENTER){
		//enemyBuild == ENEMY_BUILD::TWO_GATE || 
		if (enemyBuild == ENEMY_BUILD::ZEALOT_ALLIN || enemyBuild == ENEMY_BUILD::FORWARD_GATE ||
			enemyBuild == ENEMY_BUILD::PHOTO_RUSH || (enemyBuild == ENEMY_BUILD::RAW_DOUBLE && !info->map->isDiagonal())){

			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 1, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot)));
			builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, TechTypes::Tank_Siege_Mode));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot)));

			flags[AUTO_TANK] = true;

			progressBuild = EXPAND_GATE;
		}
		else{
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Engineering_Bay, posMgr->getTilePosition(POS::ENGINEERING)));
			builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, TechTypes::Tank_Siege_Mode));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, false, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::HILL_TURRET)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, false, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::SIM1_TURRET)));

			flags[AUTO_TANK] = true;

			progressBuild = EXPAND_CORE;
		}
	}
	else if (progressBuild == PROGRESS_BUILD::TWO_FACTORY){
		if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Factory) || 2 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode)){

			//builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot)));

			flags[AUTO_VULTURE] = true;
		}
	}
	else if (progressBuild == EXPAND_CORE){
		if (flags[FLAG::EXPAND_CMD]){
			if (!flags[TEST]){

				//builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));

				/*
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Engineering_Bay, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true, UnitTypes::Terran_Machine_Shop));
				builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Machine_Shop, 2, true, TechTypes::Spider_Mines));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true, UnitTypes::Terran_Refinery));

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 4, true, UnitTypes::Terran_Academy, posMgr->getTilePosition(POS::ACADEMY)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, false, UnitTypes::Terran_Comsat_Station));
				builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Factory, 4, false, UpgradeTypes::Ion_Thrusters));

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true , UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 3, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -40));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 4, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -40));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 5, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -40));
				*/

				/*
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 4, true, UnitTypes::Terran_Comsat_Station));

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -50));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -50));

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 4, true, UnitTypes::Terran_Armory, posMgr->getTilePosition(POS::ARMORY)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Armory, 1, false, UpgradeTypes::Terran_Vehicle_Weapons));

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Comsat_Station, 1, false, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -75));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Comsat_Station, 1, false, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true), nullptr, -75));
				*/
				flags[AUTO_VULTURE] = true;
				flags[AUTO_SUPPLY] = true;
				flags[TEST] = true;
			}				
		}
	}
}

void LastBuild::bacanicPush(){

	if (!builds.empty()){
		return;
	}

	auto enemyBuild = state->getEnemyBuild();

	if (progressBuild == PROGRESS_BUILD::NONE){
		if (enemyBuild == NONE){ // 1��ġ �������� �ƹ��� �ǽɰ��� ������� �ȿ°� ( EX. ���䷯��, ��������Ʈ �� )
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 15, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory)));
			builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Marine, 2));
			progressBuild = PROGRESS_BUILD::FIRST_FAC;
		}
	}
	else if (progressBuild == PROGRESS_BUILD::FIRST_FAC){
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 1, true, UnitTypes::Terran_Command_Center, posMgr->getTilePosition(POS::SECOND_CENTER), nullptr, 100));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 1, true, UnitTypes::Terran_Machine_Shop));
		progressBuild = SECOND_CENTER;
	}
	else if (progressBuild == PROGRESS_BUILD::SECOND_CENTER){
		
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Command_Center, 2, true, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot)));
		builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, UnitTypes::Terran_Engineering_Bay, posMgr->getTilePosition(POS::ENGINEERING)));
		builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Siege_Tank_Tank_Mode, 1, true, TechTypes::Tank_Siege_Mode));

		flags[AUTO_TANK] = true;

		progressBuild = EXPAND_CORE;
	}
	else if (progressBuild == EXPAND_CORE){
		if (flags[FLAG::EXPAND_CMD]){
			if (!flags[TEST]){

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 15, true, UnitTypes::Terran_Factory, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true, UnitTypes::Terran_Refinery));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Factory, 2, true, UnitTypes::Terran_Academy, posMgr->getTilePosition(POS::ACADEMY)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Factory, 2, true, UpgradeTypes::Terran_Infantry_Armor));
				builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Factory, 2, true, UpgradeTypes::Terran_Infantry_Weapons));
				builds.push_back(BuildWork(Work::WORK_TYPE::TECH, UnitTypes::Terran_Factory, 2, true, TechTypes::Stim_Packs));				
				builds.push_back(BuildWork(Work::WORK_TYPE::UPGRADE, UnitTypes::Terran_Factory, 2, true, UpgradeTypes::U_238_Shells));

				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Machine_Shop));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
				builds.push_back(BuildWork(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Academy, 1, true, UnitTypes::Terran_Barracks, posMgr->getProperPosition(UnitTypes::Terran_Factory, true)));
				


				flags[AUTO_MARINE] = true;
				flags[AUTO_SUPPLY] = true;

				flags[TEST] = true;
			}
		}
	}
}

void LastBuild::run(){

	auto startTerritory = info->map->getMyTerritorys().front();

	// ���� ����ϴ� ����Ÿ��.
	auto tankType = UnitTypes::Terran_Siege_Tank_Tank_Mode;
	auto vultureType = UnitTypes::Terran_Vulture;
	auto barrakType = UnitTypes::Terran_Barracks;
	auto marineType = UnitTypes::Terran_Marine;

	//buildPush();

	gasControl();
	search();
	guardChk();
	simOutScv();		
	move();

	if (progressBuild == EXPAND_CORE){
		expand_4();
		
		if (!flags[ATTACK] && 3 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Siege_Mode) && 3 <= info->self->getUnitCount(UnitTypes::Terran_Vulture) && info->map->getCloseEnemy().size() < 5){
			info->self->getBuildings(UnitTypes::Terran_Barracks).front()->self->lift();
			flags[ATTACK] = true;
		}

		/*
		if (!flags[GAS_CONTROL_2]){
			if (3 <= info->self->getBuildingCount(UnitTypes::Terran_Factory)){
				for (auto territory : info->map->getMyTerritorys()){					
					territory->setGasScvCount(1);
				}

				flags[GAS_CONTROL_2] = true;
			}		
		}
		*/
	}

	if (flags[AUTO_MARINE]){
		if (scheduler->canAddUnitType(marineType)){
			if (0 < info->self->getBuildingCount(UnitTypes::Terran_Academy) && 4 <= info->self->getUnitCount(UnitTypes::Terran_Marine) && info->self->getUnitCount(UnitTypes::Terran_Medic) * 2 <= info->self->getUnitCount(UnitTypes::Terran_Marine)){
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Medic), Scheduler::BUILD_SPEED::NORMAL);
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Medic), Scheduler::BUILD_SPEED::NORMAL);
			}

			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, marineType), Scheduler::BUILD_SPEED::NORMAL);			
		}
	}

	if (flags[AUTO_VULTURE]){
		if (scheduler->canAddUnitType(vultureType)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, vultureType), Scheduler::BUILD_SPEED::NORMAL);
		}
	}

	if (flags[AUTO_TANK]){
		if (scheduler->canAddUnitType(tankType)){
			scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, tankType), Scheduler::BUILD_SPEED::NORMAL);
		}
	}

	if (flags[AUTO_SUPPLY]){
		if (Broodwar->self()->supplyTotal() < 200 * 2 && frameCount + 700 < bw->getFrameCount()){
			if (info->self->getBuildingCount(UnitTypes::Terran_Factory, true) + info->self->getBuildingCount(UnitTypes::Terran_Barracks, true) < 4){
				if (scheduler->getSupply() <= 8 * 2){
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
					frameCount = bw->getFrameCount();
				}					
			}
			//else if(info->self->getBuildingCount(UnitTypes::Terran_Factory, true) < 5){
			//else if (info->self->getBuildingCount(UnitTypes::Terran_Factory, true) + info->self->getBuildingCount(UnitTypes::Terran_Barracks, true) < 5){
			else{
				if (scheduler->getSupply() <= 12 * 2){
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getProperPosition(UnitTypes::Terran_Supply_Depot, true)), Scheduler::BUILD_SPEED::SLOW);
					frameCount = bw->getFrameCount();
				}				
			}
		}
	}

	// ����� ������� ����.
	for (auto iter = builds.begin(); iter != builds.end(); ++iter){
		BuildWork& build = (*iter);
		auto work = build.getWork();

		if (!build.getFire() && build.run()){
			if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_BARRACK)){
				auto performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_SUPPLY));
				work->setPerformer(performer);
			}
			else if (build.getWork()->pos == posMgr->getTilePosition(POS::SIM1_SUPPLY2)){
				auto performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_BARRACK));
				work->setPerformer(performer);
			}
			
			//scheduler->addSchedule(build.getWork());
			scheduler->addSchedule(build.getWork(), Scheduler::BUILD_SPEED::NORMAL);

			iter = builds.erase(iter);
			iter--;
		}
	}	

	// ������ SCV�� �̴´�.
	for (auto territory : info->map->getMyTerritorys()){
		/*
		if (progressBuild == PROGRESS_BUILD::FIRST_FAC && !flags[INSERT_SCV] && 15 == info->self->getUnitCount(UnitTypes::Terran_SCV, true)){
			//scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV, 1, startTerritory->getCmdCenter()), Scheduler::BUILD_SPEED::NORMAL);
			if (startTerritory->getCmdCenter()->self->train(UnitTypes::Terran_SCV))
				flags[INSERT_SCV] = true;
		}
		*/

		int require = territory->getRequireSCVCount();

		if (territory->isRequireScv() && scheduler->canAddUnitType(UnitTypes::Terran_SCV)){
			if (1 <= info->self->getBuildingCount(UnitTypes::Terran_Factory)){
				if (3 * 2 <= scheduler->getSupply()){
					scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV), Scheduler::BUILD_SPEED::NORMAL);
				}				
			}			
			else{
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_SCV), Scheduler::BUILD_SPEED::NORMAL);
			}
		}
	}
}

void LastBuild::gasControl(){

	auto startTerritory = info->map->getMyTerritorys().front();

	// ���� ��Ʈ��
	if (!flags[GAS_CONTROL]){
		for (auto run : scheduler->getRunning()){
			if (run->type == UnitTypes::Terran_Command_Center){
				flags[GAS_CONTROL] = true;
				break;
			}
		}

		if (progressBuild == TWO_FACTORY){
			flags[GAS_CONTROL] = true;
		}
	}

	if (!flags[GAS_CONTROL]){
		if (88 <= bw->self()->gas()){
			startTerritory->setGasScvCount(2);
		}

		if (96 <= bw->self()->gas()){
			startTerritory->setGasScvCount(1);
		}
	}
	else{
		int gasCount = 3;
		startTerritory->setGasScvCount(3);
		//territory->setGasScvCount(gasCount);
		/*
		if (400 <= scheduler->getGas()){
			gasCount = 2;
		}		

		for (auto territory : info->map->getMyTerritorys()){
			territory->setGasScvCount(gasCount);
		}
		*/
	}
}

void LastBuild::simOutScv(){

	if (!state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1) || flags[ERROR_OUT]){
		return;
	}

	auto startTerritory = info->map->getMyTerritorys().front(); 
	auto chkPoint = getNearestChokepoint(startTerritory->getTilePosition());
	auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
	bool safe = info->map->getCloseEnemy().size() == 0;

	if (performer == nullptr){
		performer = scheduler->getPerformerInSchedule(posMgr->getTilePosition(POS::SIM1_SUPPLY2));
	}

	if (!flags[OUT_SCV] && performer->state == MyUnit::STATE::MINERAL){
		if (performer->self->exists() && getDistance(startTerritory->getTilePosition(), performer->getTilePosition()) < 16){
			flags[ERROR_OUT] = true;
			return;
		}

		if (performer->self->exists() && getDistance(chkPoint->getCenter(), performer->getPosition()) < 55){
			performer->state = MyUnit::STATE::OUT;	
			flags[OUT_SCV] = true;
		}		
	}

	if (flags[OUT_SCV]){
		if (!safe){
			flags[ERROR_OUT] = true;
		}

		if (performer->state == MyUnit::STATE::OUT){
			if (!brk->self->isLifted()){
				if (brk->self->isIdle())
					brk->self->lift();
			}
			else{
				performer->state = MyUnit::MINERAL;
			}
		}

		if (performer->state == MyUnit::STATE::MINERAL){
			if (performer->self->exists() && getDistance(startTerritory->getTilePosition(), performer->getTilePosition()) <= 20){
				if (brk->self->isLifted()){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}	
				else{
					if (brk->self->isIdle()){
						flags[ERROR_OUT] = true;
						performer = nullptr;
					}
				}
			}
		}
	}
}

void LastBuild::guardChk(){

	if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1)){
		return;
	}

	int count = 0;

	for (auto unit : info->self->getImmediatelyBuilding(UnitTypes::Terran_Supply_Depot)){
		if (unit->getTilePosition() == posMgr->getTilePosition(POS::SIM1_SUPPLY)){
			count++;
		}

		if (unit->getTilePosition() == posMgr->getTilePosition(POS::SIM1_SUPPLY2)){
			count++;
		}
	}

	for (auto unit : info->self->getImmediatelyBuilding(UnitTypes::Terran_Barracks)){
		if (unit->getTilePosition() == posMgr->getTilePosition(POS::SIM1_BARRACK)){
			count++;
		}
	}

	if (3 <= count){
		state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1, true);
	}
}

void LastBuild::search(){

	// ����.
	if (!flags[SEARCH] && 12 <= info->self->getUnitCount(UnitTypes::Terran_SCV)){

		auto worker = info->map->getMyTerritorys()[0]->getWorker(true);
		scouting->setSpy(worker);

		if (worker != nullptr)
			flags[SEARCH] = true;
	}
}

void LastBuild::move(){
	
	for (auto iter = moveBuildings.begin(); iter != moveBuildings.end(); iter++){

		auto unit = (*iter);

		if (unit->self->isLifted()){
			if (5 < getDistance(unit->getTilePosition(), unit->targetPos)){
				unit->self->move((Position)unit->targetPos);
			}
			else{
				if(unit->self->isIdle())
					unit->self->land(unit->targetPos);
			}
		}
		else{
			if (getDistance(unit->getTilePosition(), unit->targetPos) < 5){
				if (unit->self->isIdle()){
					iter = moveBuildings.erase(iter);
					iter--;
				}
			}
			else{
				unit->self->lift();
			}
		}
	}
}

void LastBuild::expand_3(){

	if (flags[EXPAND_CMD]){
		
		return;
	}

	auto pos = info->map->getMyTerritorys().back()->getPosition();
	int radius = 320;

	switch (info->map->getMyDirection()){
	case 0:
		pos += Position(-60, 0);
		break;

	case 1:
		pos += Position(-40, 0);
		break;

	case 2:
		pos += Position(-20, 50);
		break;

	case 3:
		pos += Position(60, 0);
		break;
	}

	auto engine = info->self->getBuildings(UnitTypes::Terran_Engineering_Bay);
	auto close = info->map->getCloseEnemy();
	auto cnter = info->self->getBuildings(UnitTypes::Terran_Command_Center).back();
	int count = 0;
	static bool isGoal = false;

	for (auto unit : close){
		if (getDistance(unit->getPosition(), pos) < radius){
			count++;
		}
	}

	if (0 < engine.size()){
		int progress = getProgress(engine.front()->self);

		if (engine.front()->self->isLifted()){
			
			if (count - info->map->getCloseCount(UnitTypes::Protoss_Probe) == 0){
				if (12 < getDistance(engine.front()->self->getTilePosition(), posMgr->getTilePosition(POS::ENGINE_SIGHT_1))){
					engine.front()->self->move((Position)posMgr->getTilePosition(POS::ENGINE_SIGHT_1));
				}
				else{
					isGoal = true;
				}
			}
			else{
				if (3 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) && info->self->getUnitCount(UnitTypes::Protoss_Zealot) < 7){
					engine.front()->self->move((Position)posMgr->getTilePosition(POS::ENGINE_SIGHT_1));
				}
				else{
					if (80 < progress){
						engine.front()->self->move((Position)posMgr->getTilePosition(POS::ENGINE_SIGHT_1));
					}
					else{
						if (5 < getDistance(engine.front()->self->getTilePosition(), posMgr->getTilePosition(POS::ENGINEERING))){
							engine.front()->self->move((Position)posMgr->getTilePosition(POS::ENGINEERING));
						}
						else{
							engine.front()->self->stop();
						}
					}
				}				
			}
		}
		else{
			engine.front()->self->lift();
		}
	}

	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center)){
		auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
		auto chk = BWTA::getNearestChokepoint(info->map->getMyStartBase()->getTilePosition());
		auto next = info->map->getMyTerritorys().back()->getBase();

		if (brk->self->isLifted())
			state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2, true);

		if (cnter->self->isLifted()){
			if (count == 0){
				if (getDistance(cnter->self->getTilePosition(), next->getTilePosition()) < 4){
					cnter->self->land(next->getTilePosition());
				}
				else{
					cnter->self->move(next->getPosition());
				}
			}
			else{
				int progress = getProgress(cnter->self);

				if (3 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) && info->self->getUnitCount(UnitTypes::Protoss_Zealot) < 7){
					state->setCombatFlag(COMBAT_FLAG::FORWARD_MARINE, true);

					if (getDistance(cnter->self->getTilePosition(), next->getTilePosition()) < 4){
						cnter->self->land(next->getTilePosition());
					}
					else{
						cnter->self->move(next->getPosition());
					}

					brk->self->lift();
					brk->targetPos = posMgr->getTilePosition(POS::FRONT_BARRACK);
					moveBuildings.push_back(brk);
				}
				else{
					if (85 < progress){
						if (6 < getDistance(cnter->self->getTilePosition(), next->getTilePosition())){
							cnter->self->move(next->getPosition());
						}
						else{
							cnter->self->stop();
						}
					}
					else{
						cnter->self->move((Position)posMgr->getTilePosition(POS::SECOND_CENTER));
					}
				}
			}
		}
		else{
			if (getDistance(cnter->self->getTilePosition(), next->getTilePosition()) < 4 && 0 < engine.size()){

				brk->targetPos = posMgr->getTilePosition(POS::FRONT_BARRACK);
				moveBuildings.push_back(brk);

				engine.front()->targetPos = posMgr->getTilePosition(POS::FRONT_ENGINE);
				moveBuildings.push_back(engine.front());

				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::SIM2_TURRET)), Scheduler::BUILD_SPEED::NORMAL);
				//scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::FRONT_SUPPLY)), Scheduler::BUILD_SPEED::SLOW);

				info->map->getMyTerritorys().back()->setCmdCenter(cnter);
				info->map->getMyTerritorys().back()->init(next);

				info->map->getMyTerritorys().front()->immigrant(info->map->getMyTerritorys().back(), Territory::WORK::OVER);

				flags[EXPAND_CMD] = true;
			}
			else{
				cnter->self->lift();
			}
		}

		/*
		if (isGoal){
			
		}
		else{
			if (cnter->self->isLifted()){
				cnter->self->move(next->getPosition());
			}
			else{
				cnter->self->lift();				
			}			
		}
		*/
	}
}

void LastBuild::expand_4(){

	if (flags[EXPAND_CMD]){
		
		auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
		brk->targetPos = posMgr->getTilePosition(POS::THIRD_FACTORY);
		moveBuildings.push_back(brk);

		return;
	}

	auto pos = info->map->getMyTerritorys().back()->getPosition();
	int radius = 320;

	switch (info->map->getMyDirection()){
	case 0:
		pos += Position(-60, 0);
		break;

	case 1:
		pos += Position(-40, 0);
		break;

	case 2:
		pos += Position(-20, 50);
		break;

	case 3:
		pos += Position(60, 0);
		break;
	}

	auto engine = info->self->getBuildings(UnitTypes::Terran_Engineering_Bay);
	auto close = info->map->getCloseEnemy();
	auto cnter = info->self->getBuildings(UnitTypes::Terran_Command_Center).back();
	int count = 0;
	static bool isGoal = false;

	for (auto unit : close){
		if (getDistance(unit->getPosition(), pos) < radius){
			count++;
		}
	}

	if (0 < engine.size()){
		int progress = getProgress(engine.front()->self);

		if (engine.front()->self->isLifted()){

			if (count - info->map->getCloseCount(UnitTypes::Protoss_Probe) == 0){
				if (12 < getDistance(engine.front()->self->getTilePosition(), posMgr->getTilePosition(POS::ENGINE_SIGHT_1))){
					engine.front()->self->move((Position)posMgr->getTilePosition(POS::ENGINE_SIGHT_1));
				}
				else{
					isGoal = true;
				}
			}
			else{
				if (3 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) && info->self->getUnitCount(UnitTypes::Protoss_Zealot) < 7){
					engine.front()->self->move((Position)posMgr->getTilePosition(POS::ENGINE_SIGHT_1));
				}
				else{
					if (80 < progress){
						engine.front()->self->move((Position)posMgr->getTilePosition(POS::ENGINE_SIGHT_1));
					}
					else{
						if (5 < getDistance(engine.front()->self->getTilePosition(), posMgr->getTilePosition(POS::ENGINEERING))){
							engine.front()->self->move((Position)posMgr->getTilePosition(POS::ENGINEERING));
						}
						else{
							engine.front()->self->stop();
						}
					}
				}
			}
		}
		else{
			posMgr->destroyBuilding(engine.front()->self);
			engine.front()->self->lift();			
		}
	}

	if (2 <= info->self->getBuildingCount(UnitTypes::Terran_Command_Center)){
		auto brk = info->self->getBuildings(UnitTypes::Terran_Barracks).front();
		auto chk = BWTA::getNearestChokepoint(info->map->getMyStartBase()->getTilePosition());
		auto next = info->map->getMyTerritorys().back()->getBase();

		if (brk->self->isLifted())
			state->setCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_2, true);

		if (cnter->self->isLifted()){
			if (count == 0){

				if (brk->self->isIdle()){
					brk->self->lift();
				}

				if (getDistance(brk->self->getTilePosition(), posMgr->getTilePosition(POS::THIRD_FACTORY)) < 4){
					if (brk->self->isIdle()){
						brk->self->land(next->getTilePosition());
						posMgr->planUpload(UnitTypes::Terran_Barracks, posMgr->getTilePosition(POS::THIRD_FACTORY));
					}					
				}
				else{
					brk->self->move((Position)posMgr->getTilePosition(POS::THIRD_FACTORY));
				}

				if (getDistance(cnter->self->getTilePosition(), next->getTilePosition()) < 4){
					cnter->self->land(next->getTilePosition());
				}
				else{
					cnter->self->move(next->getPosition());
				}
			}
			else{
				int progress = getProgress(cnter->self);

				if (brk->self->isIdle()){
					brk->self->land(posMgr->getTilePosition(POS::SIM1_BARRACK));
				}				

				if (3 <= info->self->getUnitCount(UnitTypes::Terran_Siege_Tank_Tank_Mode) && info->self->getUnitCount(UnitTypes::Protoss_Zealot) < 7){
					state->setCombatFlag(COMBAT_FLAG::FORWARD_MARINE, true);

					if (getDistance(cnter->self->getTilePosition(), next->getTilePosition()) < 4){
						cnter->self->land(next->getTilePosition());
					}
					else{
						cnter->self->move(next->getPosition());
					}

					brk->self->lift();
					brk->targetPos = posMgr->getTilePosition(POS::FRONT_BARRACK);
					moveBuildings.push_back(brk);
				}
				else{
					if (85 < progress){
						if (6 < getDistance(cnter->self->getTilePosition(), next->getTilePosition())){
							cnter->self->move(next->getPosition());
						}
						else{
							cnter->self->stop();
						}
					}
					else{
						cnter->self->move((Position)posMgr->getTilePosition(POS::SECOND_CENTER));
					}
				}
			}
		}
		else{
			if (getDistance(cnter->self->getTilePosition(), next->getTilePosition()) < 4 && 0 < engine.size()){
				
				info->map->getMyTerritorys().back()->setCmdCenter(cnter);
				info->map->getMyTerritorys().back()->init(next);

				info->map->getMyTerritorys().front()->immigrant(info->map->getMyTerritorys().back(), Territory::WORK::OVER);

				engine.front()->targetPos = posMgr->getTilePosition(POS::FRONT_ENGINE);
				moveBuildings.push_back(engine.front());
				
				//posMgr->planUpload(UnitTypes::Terran_Factory, posMgr->getTilePosition(POS::FRONT_ENGINE));
				
				scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Missile_Turret, posMgr->getTilePosition(POS::SIM2_TURRET)), Scheduler::BUILD_SPEED::NORMAL);
				//scheduler->addSchedule(new Work(Work::WORK_TYPE::UNIT, UnitTypes::Terran_Supply_Depot, posMgr->getTilePosition(POS::FRONT_SUPPLY)), Scheduler::BUILD_SPEED::SLOW);
				

				flags[EXPAND_CMD] = true;
			}
			else{
				cnter->self->lift();
			}
		}
	}
}