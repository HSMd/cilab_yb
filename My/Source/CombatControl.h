#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include "MyUnit.h"
#include "UnitControl.h"
#define MINE_NONE 0
#define MINE_READY 1
#define MINE_DONE 2


using namespace std;
using namespace BWAPI;

class CombatControl
{
private:
	CombatControl();
	~CombatControl();
	static CombatControl *s_CombatControl;
	bool fighting;
	bool forward;
	int gameFrame;

public:

	static CombatControl * create();
	static CombatControl * getInstance();
	void init();
	void onFrame(TeamControl *t);
	void draw(TeamControl* t);
	void update();

	void onUnitCreate(Unit);
	void onUnitDestroy(Unit);
	void onUnitComplete(Unit);

	void onSTRONG_ATTACK(TeamControl* t);
	void onCRZAY(TeamControl* t);
	void onATTACK(TeamControl* t);
	void onTIGHTING(TeamControl* t);
	void onDEFENSE(TeamControl* t);
	void onSQUEEZE(TeamControl* t);

	void onDOUBLE(TeamControl* t);
	void onBUNKER(TeamControl* t);
	void onTANK(TeamControl* t);
	void onGOING(TeamControl* t);
	void onTIGHT(TeamControl* t);
	void onJOIN(TeamControl* t);
	void onBJ(TeamControl* t);
	void onFINISH(TeamControl* t);


	vector<std::pair<Position, std::pair<float, int>>> minePos;
	vector<std::pair<Position, std::pair<float, int>>> turretPos;
	void setMinePos();
	void updateMinePosValue();
	vector<Position> getMinePos(int count);
	void setUseMineState(Position p, int state);

	float p1, p2, p3;

	int totalTeamIndex = 0;

	vector<Position> getVGPosition(Position tPos, Position ePos, int vgNum, int tR, float tankRange, int dist);

	Unit getTargetUnit(MyUnit* unit, Unitset enemyUnits);
	Unit getHealUnit(MyUnit* unit, TeamControl* t);
	Unit getRepairUnit(MyUnit* unit);
	Unit getTargetBuilding(MyUnit* unit);
	float getTargetUnitValue(UnitType my, UnitType enemy);
	Unitset getNearEnemyUnits(TeamControl* t);
	Unitset getNearEnemyBuildings(MyUnit* t);
	bool isOnlyZealot(TeamControl* t);

	Unit getMassedUnit(MyUnit* unit, Unitset enemies);
	vector<std::pair<Position, float>> getEMPPositions(TeamControl* t, int vCount, Unitset enemies);
	bool isNearChokePoint(MyUnit *);

	void kitingMove(MyUnit*, Unit target, Position targetPos);

	TilePosition DefensePos[2];
	TilePosition tightPos;
	vector<TilePosition> defenseTankPos;
	TilePosition bangMission;
	bool defenseSetting = false;
	void settingDefenseObject();

	bool isEnemyUnitComeTrue(MyUnit*);
	void joiningTeam(MyUnit*, TeamControl* t);
	void setHelpMe(MyUnit*);

	vector<TilePosition> tightTankPos;
	vector<TilePosition> tightBunkerPos;

	map<TilePosition, int> tightValue;
	map<TilePosition, int> tightReady;

	TilePosition getTightTankPosition();
	TilePosition getTightBunkerPosition();

	bool isCanBuySteamPack(MyUnit*);
	float getNearestTankDistance(MyUnit*);
};
