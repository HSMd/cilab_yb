#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <vector>

#include "Territory.h"
#include "Resource.h"

using namespace BWAPI;
using namespace BWTA;
using namespace std;


class UnitInfo {
private:

public:
	int player;
	int id;
	UnitType type;
	Position pos;
};


class BaseLocationInfo {
private:

public:
	enum TYPE {
		스타팅,
		앞마당,
		멀티
	};
	
	BWTA::BaseLocation* base;
	vector<UnitInfo*> myUnits;
	vector<UnitInfo*> enemyUnits;
	vector<UnitInfo*> myBuildings;
	vector<UnitInfo*> enemyBuildings;
	vector<Unit> mineralVec;
	vector<Unit> gasVec;

	TYPE type;
	int defense;
	int value;
	int mineralCount;
	int minerals;
	int gases;
	int workerCount;
	int player;

	BaseLocationInfo(BWTA::BaseLocation* b) {
		base = b;
		defense = 0;
		value = 0;
		mineralCount = 0;
		minerals = 0;
		gases = 0;
		workerCount = 0;
		player = 0;
	}

	void update();
	void draw();
	
	vector<Unit> getMinerals() { return mineralVec; };
	vector<Unit> getGases() { return gasVec; };
	int getDefense() { return defense; };
	int getValue() { return value; };
};


class Map{

	int startDirection;
	int enemyDirection;

	BaseLocation* myStartBase;
	BaseLocation* enemyStartBase;

	BaseLocation* allBase;

	vector<Territory*> myTerritorys;
	vector<Territory*> enemyTerritorys;

	vector<Unit> close;

public:
	Map();
	~Map();

	void show(int startX, int startY);
	void update();
	void run();
	void draw();

	void destroy(Unit unit);
	bool isAlready(Territory* territory, Unit unit);

	int getDirection(BaseLocation* base);		
	void addScv(Unit unit);
	BaseLocation* getCandidateLocation();
	Territory* getNearTerritory(TilePosition dst);
	
	int getMyDirection(){ return startDirection; };
	int getEnemyDirection(){ return enemyDirection; };
	void addMyTerritory(Territory* territory){ myTerritorys.push_back(territory); };

	void setEnemyStartBase(BaseLocation* base){ enemyStartBase = base; enemyDirection = getDirection(base); };

	BaseLocation* getMyStartBase(){ return myStartBase; };
	BaseLocation* getEnemyStartBase(){ return enemyStartBase; };

	vector<Territory*>& getMyTerritorys(){ return myTerritorys; };
	vector<Territory*>& getEnemyTerritorys(){ return enemyTerritorys; };

	int getCloseCount(UnitType type);
	vector<Unit>& getCloseEnemy();

	map<BWTA::BaseLocation*, BaseLocationInfo*> BLI;

	void setBaseLocationInfo();
	void updateBaseLocationInfo();
	void drawBaseLocationInfo();
	bool isDiagonal();
};