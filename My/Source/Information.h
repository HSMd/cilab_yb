#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <vector>
#include <list>
#include <map>

#include "MapInformation.h"
#include "Self.h"
#include "Enemy.h"
#include "Map.h"
#include "Resource.h"

using namespace std;
using namespace BWAPI;

class Information
{
private:
	Information();
	~Information();
	int posVal[IMSIZE][IMSIZE];

public:

	Self* self;   // Me
	Enemy* enemy; // Enemy
	Map* map;     // Map
	ResourceInfo* resource;
	
public:
	static Information * getInstance();

	vector<pair<Position, pair<int, int>>> getGroupArray(bool, bool);
	int getUnitValue(UnitType ut);
	// On Func
	void init();
	void update();
	void show();
	void morph(Unit unit);
	void draw();
	void run();
	void destroy(Unit unit);
	void create(Unit unit);
};

