#include "Scout.h"
#include "Utility.h"
#include "Instance.h"

Scout::Scout()
{
	find = false;
	isIn = false;
	canUseData = false;

	point = nullptr;
	target = nullptr;
	enemyFront = nullptr;

	searchCount = 0;
}

Scout::~Scout()
{

}

Scout* Scout::getInstance() {

	static Scout instance;
	return &instance;
}

void Scout::init(){

	// 들르는 순서 정리.
	auto myStartBase = BWTA::getStartLocation(bw->self());
	vector<BaseLocation*> startLocations;
	int maxDis = 0;

	BaseLocation* secondLocation;
	vector<BaseLocation*>::iterator delIter;

	for (auto location : BWTA::getStartLocations()){
		startLocations.push_back(location);
	}

	for (auto iter = startLocations.begin(); iter != startLocations.end(); iter++){
		BaseLocation* location = (*iter);

		if (location->getTilePosition() == myStartBase->getTilePosition()){
			iter = startLocations.erase(iter);
			iter--;

			continue;
		}

		auto dis = getDistance(myStartBase, location);
		if (maxDis < dis){
			maxDis = dis;
			delIter = iter;
			secondLocation = location;
		}
	}

	startLocations.erase(delIter);

	startBases.push_back(startLocations.front());
	startBases.push_back(secondLocation);
	startBases.push_back(startLocations.back());
}

void Scout::show(){

	//drawUnitText(performer->self, "Spy");
}

void Scout::draw(){

	for (auto point : chkPoints){

		if (!point->chk){
			bw->drawCircleMap(point->pos, 7, Colors::Purple, false);
		}
		else{
			bw->drawCircleMap(point->pos, 7, Colors::Purple, true);
		}

	}
}

void Scout::run(){

	if (performer == nullptr){
		return;
	}

	if (performer->getType() == UnitTypes::Terran_SCV)
		scvScout();
}

void Scout::scvScout(){

	if (!performer->self->exists()){
		canUseData = true;
		return;
	}

	if (!find){  //적 스타팅위치 찾기전까지
		if (target == nullptr){
			changeTarget();
		}

		if (8 < getDistance(target->getTilePosition(), performer->getTilePosition())){
			performer->self->move(target->getPosition());
		}
		else{
			changeTarget();
		}

		if (searchCount == 3){   //막서치일때는 들어가지 못할 가능성이 있으므로 바로 적위치로 때려버린다.
			find = true;
			info->map->setEnemyStartBase(target);
			findEnemyStart(target);
			point = getNextPoint();
		}

		if (1 < info->enemy->getAllBuildings().size() || 0 < info->enemy->getBuildingCount(UnitTypes::Protoss_Nexus)){
			find = true;

			auto starts = BWTA::getStartLocations();
			for (auto base : starts){
				if (getDistance((TilePosition)info->enemy->getPosition(info->enemy->getAllBuildings().front()), base->getTilePosition()) < 30){
					target = base;
					break;
				}
			}

			info->map->setEnemyStartBase(target);
			findEnemyStart(target);
			point = getNextPoint();
		}


	}
	else{  //적 스타팅 위치 알았을때 
		if (point == nullptr){
			// 추후 수정.
			//hinder();
			//seeEverything();
			centerRotate();
			canUseData = true;
			return;
		}

		if (!isIn && 4 < getDistance(target->getTilePosition(), performer->getTilePosition())){
			performer->self->move(target->getPosition());
			isIn = true;
		}
		else{
			if (TILE_SIZE * 3 < getDistance(point->pos, performer->getPosition())){
				performer->self->move(point->pos);
			}
			else{
				point = getNextPoint();
			}
		}
	}
}

void Scout::changeTarget(){

	searchCount++;
	target = startBases.back();
	startBases.pop_back();
}

void Scout::centerRotate(){
	if (!s_flags[JUDGE_M]){  //한바퀴돌고 중간으로 찍기
		performer->self->move((Position)TilePosition(64, 64));
	}
	else{
		if (!s_flags[JUDGE_TIME]){  //가운데 돌리기 용도 

			if (searchCount == 1){
				centerMoveCount = 3;
			}
			else if (searchCount == 2){
				centerMoveCount = 2;
			}
			else{
				centerMoveCount = 1;
			}

			if (centerMoveJudge < centerMoveCount){
				if (centerMoveJudge % 2 == 0){
					performer->self->move((Position)TilePosition(64, 42));
					if (performer->self->getTilePosition().x - TilePosition(64, 42).x < 2 && performer->self->getTilePosition().y - TilePosition(64, 42).y < 2 && performer->self->getTilePosition().x - TilePosition(64, 42).x > -2 && performer->self->getTilePosition().y - TilePosition(64, 42).y > -2 && !s_flags[JUDGE_WATAGATA1]){
						centerMoveJudge++;
						s_flags[JUDGE_WATAGATA1] = true;
						s_flags[JUDGE_WATAGATA2] = false;
					}
				}
				else if (centerMoveJudge % 2 == 1){
					performer->self->move((Position)TilePosition(77, 80));
					if (performer->self->getTilePosition().x - TilePosition(77, 80).x < 1 && performer->self->getTilePosition().y - TilePosition(77, 80).y < 1 && performer->self->getTilePosition().x - TilePosition(77, 80).x > -2 && performer->self->getTilePosition().y - TilePosition(88, 80).y > -2 && !s_flags[JUDGE_WATAGATA2] && s_flags[JUDGE_WATAGATA1]){
						centerMoveJudge++;
						s_flags[JUDGE_WATAGATA2] = true;
						s_flags[JUDGE_WATAGATA1] = false;
					}
				}
			}
			else{
				s_flags[JUDGE_TIME] = true;
			}
		}
		else{
			performer->self->move(target->getPosition());
		}
	}

	//중앙에 왔을때 플래그값 트루로 주기
	if (performer->self->getTilePosition().x - TilePosition(64, 64).x < 3 && performer->self->getTilePosition().y - TilePosition(64, 64).y < 3){
		s_flags[JUDGE_M] = true;
	}
}

void Scout::endMission(){

	if (performer->getType() == UnitTypes::Terran_SCV){

		int minScvCount = 9999;
		Territory* territory;

		for (auto t : info->map->getMyTerritorys()){
			if (!t->getReady()){
				continue;
			}

			int count = t->getScvCount();

			if (count < minScvCount){
				count = minScvCount;
				territory = t;
			}
		}

		territory->addScv(performer);
		performer = nullptr;
	}
	else{

	}
}

void Scout::findEnemyStart(BaseLocation* base){

	auto enemyFront = BWTA::getNearestBaseLocation(base->getTilePosition());
	enemyDirection = info->map->getEnemyDirection();

	switch (enemyDirection){

	case 0:
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-170, 300)));
		chkPoints.push_back(new ChkPoint((Position)TilePosition(0, 0)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(350, -170)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(330, 300)));

		break;

	case 1:

		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-330, -100)));
		chkPoints.push_back(new ChkPoint((Position)TilePosition(126, 1)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(250, 300)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-350, 300)));

		break;

	case 2:

		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(330, 100)));
		chkPoints.push_back(new ChkPoint((Position)TilePosition(1, 124)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-170, -150)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(330, -150)));

		break;

	case 3:

		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(250, -330)));
		chkPoints.push_back(new ChkPoint((Position)TilePosition(126, 126)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-400, 170)));
		chkPoints.push_back(new ChkPoint(base->getPosition() + Position(-330, -400)));

		break;
	}
}

ChkPoint* Scout::getNextPoint(){

	if (chkPoints.empty()){
		return nullptr;
	}

	if (point != nullptr){
		point->chk = true;
	}

	for (auto p : chkPoints){
		if (!p->chk){
			return p;
		}
	}

	return nullptr;
}

void Scout::seeEverything(){

	auto chkPoint = BWTA::getNearestChokepoint(target->getTilePosition());
	auto center = TilePosition(128 / 2, 128 / 2);

	if (2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Gateway) || 2 <= info->enemy->getBuildingCount(UnitTypes::Protoss_Nexus) ||
		0 < info->enemy->getBuildingCount(UnitTypes::Protoss_Cybernetics_Core)){
	}

	if (info->enemy->getAllUnits().size() == info->enemy->getUnitCount(UnitTypes::Protoss_Probe)){
		bool avoid = false;

		for (auto unit : bw->enemy()->getUnits()){
			if (unit->getType().isBuilding()){
				continue;
			}

			if (getDistance(performer->getTilePosition(), unit->getTilePosition()) < 6){
				avoid = true;
				break;
			}
		}

		if (avoid){
			if (getDistance(performer->getTilePosition(), target->getTilePosition()) <= 21){

				if (getDistance(performer->getPosition(), chkPoint->getCenter()) <= 60){
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), (Position)center));
				}
				else{
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), chkPoint->getCenter()));
				}
			}
			else{
				if (getDistance(center, performer->getTilePosition()) < 15){
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), target->getPosition()));
				}
				else{
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), (Position)center));
				}
			}
		}
		else{
			if (7 < getDistance(performer->getTilePosition(), target->getTilePosition())){
				performer->self->move(target->getPosition());
			}
		}
	}
	else{

		bool avoid = false;

		for (auto unit : bw->enemy()->getUnits()){
			if (unit->getType().isBuilding()){
				continue;
			}

			if (getDistance(performer->getTilePosition(), unit->getTilePosition()) < 13){
				avoid = true;
				break;
			}
		}

		if (avoid){
			if (getDistance(performer->getTilePosition(), target->getTilePosition()) <= 21){
				if (getDistance(performer->getPosition(), chkPoint->getCenter()) <= 60){
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), (Position)center));
				}
				else{
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), chkPoint->getCenter()));
				}
			}
			else{
				if (getDistance(center, performer->getTilePosition()) < 15){
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), target->getPosition()));
				}
				else{
					performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), (Position)center));
				}
			}
		}
		else{
			if (7 < getDistance(performer->getTilePosition(), target->getTilePosition())){
				performer->self->move(target->getPosition());
			}
		}
	}
}

void Scout::hinder(){

	bool avoid = false;
	auto chkPoint = BWTA::getNearestChokepoint(info->map->getEnemyStartBase()->getTilePosition());

	for (auto unit : bw->enemy()->getUnits()){
		if (unit->getType().isBuilding()){
			continue;
		}

		if (getDistance(unit->getPosition(), performer->getPosition()) < TILEPOSITION_SCALE * 6){
			avoid = true;
			break;
		}
	}

	if (!avoid){
		Unit attackTarget = nullptr;
		int minLife = 9999;

		for (auto unit : bw->enemy()->getUnits()){

			if (!unit->getType().isBuilding()){
				continue;
			}

			if (attackTarget == nullptr){
				attackTarget = unit;
			}
			else{
				if (attackTarget->getType() != UnitTypes::Protoss_Pylon){
					attackTarget = unit;
					minLife = attackTarget->getHitPoints();
				}
				else{
					if (unit->getHitPoints() < minLife){
						attackTarget = unit;
						minLife = attackTarget->getHitPoints();
					}
				}
			}
		}

		performer->self->attack(attackTarget);
	}
	else{
		if (getDistance(chkPoint->getCenter(), performer->getPosition()) < 150){

			endMission();
			return;
		}

		performer->self->move(mInfo->getNextAvoidMovePos(performer->getPosition(), chkPoint->getCenter()));
	}
}