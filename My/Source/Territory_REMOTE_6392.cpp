#include "Territory.h"
#include "Instance.h"
#include "Utility.h"

int mineralDirection;

Territory::Territory(){

	cmdCenter = nullptr;
	gas = nullptr;

	if (base->isStartLocation())
		radius = 23;
	else
		radius = 17;

	ready = false;
}

Territory::Territory(BaseLocation* base){

	cmdCenter = nullptr;
	gas = nullptr;
	this->base = base;

	if (base->isStartLocation())
		radius = 23;
	else
		radius = 17;

	ready = false;
	//init(base);
}

Territory::Territory(BaseLocation* base, MyUnit* cmdCenter){

	this->cmdCenter = cmdCenter;
	this->gas = nullptr;

	if (base->isStartLocation())
		radius = 23;
	else
		radius = 17;

	ready = false;
	init(base);
}

Territory::~Territory(){

}

void Territory::init(BaseLocation* base){

	this->base = base;

	for (auto m : base->getMinerals()){
		this->minerals.push_back(new CountUnit(m));
	}

	setMineralDir();
	sort(minerals.begin(), minerals.end(), Territory::orderByDistance);

	fitnessScvCount = (base->getMinerals().size() * 1) + 5;
	gasScvCount = 3;

	ready = true;
}

void Territory::show(int startX, int startY){

	Broodwar->drawTextScreen(startX, startY, "ScvCount : %d", getScvCount());
	Broodwar->drawTextScreen(startX, startY + 20, "MineralScv : %d", scvs[WORK::MINERAL].size());
	Broodwar->drawTextScreen(startX, startY + 40, "GasScv : %d", scvs[WORK::GAS].size());
	Broodwar->drawTextScreen(startX, startY + 60, "RequireSCV : %d", getRequireSCVCount());
}

void Territory::draw(){

	drawTerritory(this);

	for (auto scv : scvs[WORK::MINERAL]){
		
		drawUnitText(scv->self, getScvStateString(scv));
	}

	for (auto scv : scvs[WORK::GAS]){

		drawUnitText(scv->self, getScvStateString(scv));
	}

	for (auto scv : scvs[WORK::OVER]){

		drawUnitText(scv->self, getScvStateString(scv));
	}
}

void Territory::update(){
	
	int frame = bw->getFrameCount();

	for (auto iter = closeCount.begin(); iter != closeCount.end(); iter++){
		UnitType type = (*iter).first;
		closeCount[type] = 0;
	}

	for (auto iter = closeEnemy.begin(); iter != closeEnemy.end(); ++iter){
		Unit unit = (*iter);

		if (radius < getDistance(getTilePosition(), unit->getTilePosition())){
			iter = closeEnemy.erase(iter);
			iter--;
		}
	}

	for (auto unit : bw->enemy()->getUnits()){
		bool already = false;

		for (int i = 0; i < info->map->getMyTerritorys().size() - 1 ; i++){
			if (info->map->getMyTerritorys()[i]->isAlready(unit)){
				//info->map->getMyTerritorys()[i]->destroy(unit);
				already = true;
				break;
			}
		}
		
		if (!already && !isAlready(unit) && getDistance(getTilePosition(), unit->getTilePosition()) <= radius){
			closeEnemy.push_back(unit);
		}

		/*
		if (!isAlready(unit) && getDistance(getTilePosition(), unit->getTilePosition()) <= radius){
			closeEnemy.push_back(unit);
		}
		*/
	}

	for (auto unit : closeEnemy){
		closeCount[unit->getType()]++;
	}

	for (auto& scv : scvs[WORK::MINERAL]){
		scv->updateOrderTime();
	}

	for (auto& scv : scvs[WORK::GAS]){
		scv->updateOrderTime();
	}

	for (auto& scv : scvs[WORK::OVER]){
		scv->updateOrderTime();
	}
}

void Territory::run(){
	
	for (auto iter = scvs[WORK::MINERAL].begin(); iter != scvs[WORK::MINERAL].end(); ++iter){
		MyUnit* scv = (*iter);

		if (!scv->self->exists()){
			subResource(scv->target);

			iter = scvs[WORK::MINERAL].erase(iter);
			iter--;
		}

		if (scv->state == MyUnit::STATE::IDLE){

			scv->buildMoney = false;

			if (scv->targetType == UnitTypes::Terran_Refinery){

				orderGas(scv, gas, true); // ?
				iter = scvs[WORK::MINERAL].erase(iter);
				iter--;

				continue;
			}			

			auto work = getIdleMineral();
			orderMineral(scv, work);
		}

		scvControl->run(scv);
	}

	for (auto iter = scvs[WORK::OVER].begin(); iter != scvs[WORK::OVER].end(); ++iter){
		MyUnit* scv = (*iter);

		if (!scv->self->exists()){
			
			subResource(scv->target);

			iter = scvs[WORK::OVER].erase(iter);
			iter--;
		}

		if (scv->state == MyUnit::STATE::IDLE){
			auto work = getIdleMineral();
			orderMineral(scv, work);
		}

		scvControl->run(scv);
	}

	for (auto iter = scvs[WORK::GAS].begin(); iter != scvs[WORK::GAS].end(); ++iter){
		MyUnit* scv = (*iter);

		if (!scv->self->exists()){
			subResource(gas->unit, false);

			iter = scvs[WORK::GAS].erase(iter);
			iter--;		
		}

		scvControl->run(scv);
	}

	if (gas != nullptr){
		int require = gasScvCount - gas->count;

		if (0 < require){
			for (int i = 0; i < require; i++){
				MyUnit* scv = getWorker(true);
				if (scv == nullptr){
					break;
				}

				orderGas(scv, gas, true);
			}
		}
		else if (require < 0){

			for (int i = require; i < 0; i++){
				MyUnit* scv = getGasWorker(true);
				if (scv == nullptr){
					bw << "hello" << endl;
					break;
				}

				addScv(scv);				
			}
		}
	}
}

//
void Territory::orderMineral(MyUnit* scv, CountUnit* work, bool add){

	work->count++;
	scv->target = work->unit;
	scv->state = MyUnit::STATE::MINERAL;

	if (add)
		scvs[WORK::MINERAL].push_back(scv);
}

void Territory::orderGas(MyUnit* scv, CountUnit* work, bool add){
	
	work->count++;
	scv->target = work->unit;
	scv->state = MyUnit::STATE::GAS;

	if (add)
		scvs[WORK::GAS].push_back(scv);
}

void Territory::addScv(MyUnit* scv){

	if (getScvCount() < fitnessScvCount){
		scv->state = MyUnit::STATE::IDLE;
		scvs[WORK::MINERAL].push_back(scv);
	}
	else{
		scv->state = MyUnit::STATE::IDLE;
		scvs[WORK::OVER].push_back(scv);
	}
}

MyUnit* Territory::getGasWorker(bool except){

	MyUnit* re = nullptr;
	vector<MyUnit*>::iterator delIter;

	for (auto iter = scvs[WORK::GAS].begin(); iter != scvs[WORK::GAS].end(); ++iter){
		MyUnit* scv = (*iter);

		if (!scv->self->exists() || !scv->self->isCompleted() || scv->self->isBeingGathered()){
			continue;
		}
		
		if (re != nullptr){
			if (!scv->self->isCarryingGas()){
				re = scv;
				delIter = iter;
			}
		}
		else{
			re = scv;
			delIter = iter;
		}
	}

	if (re != nullptr) {

		subResource(re->target, false);
		re->state = MyUnit::STATE::OTHER;

		if (except) {
			scvs[WORK::GAS].erase(delIter);
		}
	}

	return re;
}

MyUnit* Territory::getLastWorker(bool except){

	if (except){
		scvs[WORK::MINERAL].pop_back();
	}

	auto scv = scvs[WORK::MINERAL].back();
	scv->state = MyUnit::STATE::OTHER;
	scv->self->stop();

	return scv;
}

MyUnit* Territory::getWorker(bool except, TilePosition pos){

	MyUnit* re = nullptr;
	vector<MyUnit*>::iterator delIter;
	int maxScore = -1;

	for (auto iter = scvs[WORK::MINERAL].begin(); iter != scvs[WORK::MINERAL].end(); ++iter){
		MyUnit* scv = (*iter);

		if (!scv->self->exists() || !scv->self->isCompleted()){
			continue;
		}

		if (scv->state == MyUnit::STATE::MINERAL){
			if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1) && 18 < getDistance(scv->getTilePosition(), info->map->getMyStartBase()->getTilePosition())){
				continue;
			}

			int score = getScore(scv);

			if (maxScore <= score){
				maxScore = score;
				re = scv;
				delIter = iter;
			}
			else if (maxScore == score){
				if (getDistance(scv->getTilePosition(), pos) <= getDistance(re->getTilePosition(), pos)){

					maxScore = score;
					re = scv;
					delIter = iter;
				}
			}
		}
	}

	if (re != nullptr) {

		subResource(re->target);
		re->state = MyUnit::STATE::OTHER;

		if (except) {
			scvs[WORK::MINERAL].erase(delIter);
		}
	}

	return re;
}

void Territory::delWorker (MyUnit* worker){

	for (auto iter = scvs[WORK::MINERAL].begin(); iter != scvs[WORK::MINERAL].end(); ++iter){
		MyUnit* scv = (*iter);

		if (scv == worker){
			scvs[WORK::MINERAL].erase(iter);
			break;
		}
	}
}

MyUnit* Territory::getWorkerEx(bool except){

	MyUnit* re = nullptr;
	vector<MyUnit*>::iterator delIter;
	int maxScore = -1;

	for (auto iter = scvs[WORK::MINERAL].begin(); iter != scvs[WORK::MINERAL].end(); ++iter){
		MyUnit* scv = (*iter);

		if (!scv->self->exists() || !scv->self->isCompleted()){
			continue;
		}

		if (scv->state == MyUnit::STATE::MINERAL){
			if (state->getCombatFlag(COMBAT_FLAG::GUARD_SIM_CITY_1) && 18 < getDistance(scv->getTilePosition(), info->map->getMyStartBase()->getTilePosition())){
				continue;
			}

			int score = getScore(scv);
			if (maxScore < score){
				maxScore = score;
				re = scv;
				delIter = iter;

				if (score == 7){
					break;
				}
			}
		}
	}

	if (re != nullptr) {

		subResource(re->target);
		re->state = MyUnit::STATE::OTHER;

		if (except) {
			scvs[WORK::MINERAL].erase(delIter);
		}
	}

	return re;
}

int Territory::getScore(MyUnit* unit){

	int score = 0;
	int size = 7;

	if (unit->self->isCarryingMinerals()){
		return 0;
	}

	if (getProgress(unit->self) < 60){
		return 0;
	}

	bool soonEnd = false;
	bool startZero = false;
	int zeroCount = 0;
	int otherCount = 0;

	for (int i = 0; i < size; i++){
		if (unit->orderTimes[0] == 0){
			startZero = true;
		}

		if (unit->orderTimes[i] == 0){
			zeroCount++;
		}
		else{
			otherCount++;
		}
	}	

	if (startZero){
		if (zeroCount == size){
			score = 2000;
		}
		else{
			for (int i = 0; i < size; i++){
				if (unit->orderTimes[i] == 0){
					score += 200;
				}
				else{
					score = score + 100 + unit->orderTimes[i];
				}
			}
		}
	}
	else{
		if (0 < zeroCount){
			score = 0;
		}
		else{
			for (int i = 0; i < size; i++){
				score = score + 100 + unit->orderTimes[i];
			}
		}
	}

	return score;
}

CountUnit* Territory::getIdleMineral(){
	CountUnit* re = nullptr;
	int minCount = 9999;

	for (auto m : minerals){
		if (m->count == 0){
			return m;
		}

		if (m->count < minCount){
			re = m;
			minCount = m->count;
		}
	}

	return re;
}

void Territory::subResource(Unit resource, bool isMineral){

	if (isMineral){
		for (auto m : minerals){
			if (isSame(m->unit, resource)){
				m->count--;
				break;
			}

		}
	}
	else{
		gas->count--;
	}
}

void Territory::setMineralDir(){

	int midX = 0, midY = 0;

	for (auto m : minerals){
		auto pos = m->unit->getTilePosition();

		midX += pos.x;
		midY += pos.y;
	}

	midX = midX / minerals.size();
	midY = midY / minerals.size();

	if (2 < abs(midY - base->getTilePosition().y)){
		if (base->getTilePosition().y < midY){
			mineralDirection = M_DIR::DOWN;
		}
		else{
			mineralDirection = M_DIR::UP;
		}
	}
	else{
		if (base->getTilePosition().x < midX){
			mineralDirection = M_DIR::RIGHT;
		}
		else{
			mineralDirection = M_DIR::LEFT;
		}
	}
}

string Territory::getScvStateString(MyUnit* scv){
	
	string re = "";

	switch (scv->state){
	case MyUnit::STATE::IDLE:

		re += "IDLE";
		break;

	case MyUnit::STATE::MINERAL:

		re += "MINERAL";
		break;

	case MyUnit::STATE::GAS:

		re += "GAS";
		break;

	case MyUnit::STATE::BUILD:

		re += "BUILD";
		break;

	case MyUnit::STATE::OTHER:

		re += "OTHER";
		break;

	case MyUnit::STATE::OUT:

		re += "OUT";
		break;
	}
	return re;
}

bool Territory::orderByDistance(CountUnit* aUnit, CountUnit* bUnit){

	auto a = aUnit->unit;
	auto b = bUnit->unit;

	float dis1, dis2;
	auto base = BWTA::getStartLocation(bw->self());

	switch (mineralDirection){
	case M_DIR::LEFT:
		dis1 = getDistance(base->getPosition() + Position(-16, 0), a->getPosition() + Position(+32, 0));
		dis2 = getDistance(base->getPosition() + Position(-16, 0), b->getPosition() + Position(+32, 0));

		break;
	
	case M_DIR::RIGHT:
		dis1 = getDistance(base->getPosition() + Position(16, 0), a->getPosition() + Position(-32, 0));
		dis2 = getDistance(base->getPosition() + Position(16, 0), b->getPosition() + Position(-32, 0));

		break;

	case M_DIR::UP:
		dis1 = getDistance(base->getPosition() + Position(0, -8), a->getPosition() + Position(0, 16));
		dis2 = getDistance(base->getPosition() + Position(0, -8), b->getPosition() + Position(0, 16));

		break;

	case M_DIR::DOWN:
		dis1 = getDistance(base->getPosition() + Position(0, 8), a->getPosition() + Position(0, -16));
		dis2 = getDistance(base->getPosition() + Position(0, 8), b->getPosition() + Position(0, -16));

		break;
	}

	if (dis1 < dis2){
		return true;
	}

	return false;
}

bool Territory::isAlready(Unit unit){
	for (auto close : closeEnemy){
		if (unit == close){
			return true;
		}
	}

	return false;
}

int Territory::getCloseCount(UnitType type){

	return closeCount[type];
}

void Territory::destroy(Unit unit){

	for (auto iter = closeEnemy.begin(); iter != closeEnemy.end(); iter++){
		Unit close = (*iter);

		if (isSame(unit, close)){
			iter = closeEnemy.erase(iter);
			iter--;
		}
	}
}

void Territory::immigrant(Territory* territory, WORK work){

	for (auto iter = scvs[work].begin(); iter != scvs[work].end(); ++iter){
		auto scv = (*iter);
		
		if (2000 <= getScore(scv)){
			territory->addScv(scv);

			iter = scvs[work].erase(iter);
			iter--;
		}
	}
}