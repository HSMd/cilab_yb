#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <map>
#include <vector>

#include "MyUnit.h"

using namespace std;

class Guard
{
	bool trace;
	bool frontSight;

	map<UnitType, vector<MyUnit*>> guardUnits;
	vector<Position> marinePosition;
	vector<TilePosition> tankPosition;
	map<TilePosition, bool> holdTanks;

	Guard();
	~Guard();

public:
	static Guard* getInstance();
	void init();
	void show();
	void update();
	void run();

	void getUnits(UnitType type, int count = -1);
	void eraseUnits(UnitType type);
	int getCloseCount(UnitType type);

	void guardSim1Defense();

	void noSimGuard();
	void probeDefense();
	void pylonDefense();
	void scvRepair(int count);

	void marineFront();
	void marineHold();
	void tankHold();

	void holdTankControl(MyUnit* unit);

	MyUnit* getIdleUnit(vector<MyUnit*>& units);
};

