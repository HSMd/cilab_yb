#pragma once
#include "BWAPI.h"
#include "MyUnit.h"
#include "Work.h"

#include <map>
#include <vector>

using namespace BWAPI;
using namespace std;

class LastBuild{

	enum FLAG{
		INSERT_SCV,
		ERROR_OUT,
		OUT_SCV, // SIM_1 �ϼ��ϰ� �i�ܳ� �ֵ�.
		SEARCH,
		GAS_CONTROL,
		GAS_CONTROL_2,

		AUTO_MARINE,
		AUTO_TANK,
		AUTO_VULTURE,
		AUTO_SUPPLY,

		TEST,
		SAFE_EXPAND,
		CAREFUL_EXPAND,

		EXPAND_CMD,
		EXPAND_FRONT,
		EXPAND_BRK,

		ATTACK,
	};

	enum PROGRESS_BUILD{
		NONE,
		FIRST_FAC,
		SECOND_CENTER,
		FAC_CENTER,
		TWO_FACTORY,

		EXPAND_GATE,
		EXPAND_CORE,
	};

	vector<BuildWork> builds;
	vector<MyUnit*> moveBuildings;

	map<FLAG, bool> flags;
	PROGRESS_BUILD progressBuild;

	MyUnit* performer;
	int frameCount;

public: 
	LastBuild(){};
	~LastBuild(){};

	void init();
	void show();
	void draw();
	void run();

	void buildPush();
	void bacanicPush();

	void search();
	void simOutScv();
	void guardChk();
	void gasControl();

	void expand_4();
	void expand_3();
	void move();
};